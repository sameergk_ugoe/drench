#Sameer Kulkarni

CODE REPO SETUP:
git init
git remote add origin https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git
git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git

Commiting changes from local repo:
git add <file>
git commit -m "commit message"
git push origin master

SAMPLE COMMANDS:
git add contributors.txt
git commit -m 'Initial commit with contributors'
git push -u origin master

#git pull
git fetch --all; git reset --hard origin/master

CODE/WORKSPACE ORGANIZATION:
Expected Base folder (Home directory):
e.g. '/home/mininet/'
Rest are subdirectories in the 'homefolder/drench/'

SDN Controller:
pox/

Topologies:
topologies/

Mininet Setp:
mn_scripts/

Ultility and other Misscelaneous Scripts:
scripts/
    testcases/
    topo_gens/
    result_extractors/
    plot_scripts/
    other_utils/

Results folder:
results/


INSTALLATION AND SETUP RQUIREMENTS:
1. INSTALL MININET:
    Refer: http://mininet.org/download/
    Use Option 2
    git clone git://github.com/mininet/mininet
    branch: 2.2.1
    mininet/util/install.sh -a

    verify:
    sudo mn --test pingall
    
2. PYTHON LIBRARIES:
   pip install netifaces
   pip install scapy
   pip install networkx
   
POST GIT DOWNLOAD: 
Provide Executable access for all scripts and python files in mn_scripts, pox and scripts folders.
chmod -R +x *.sh
chmod -R +x *.py

VERIFICATION:
1. setup Topology and Link files:
     Edit mn_scripts/rftop_sk.py to select desired topo and link files 

2. Launch Pox controller
cd pox
./rpox_def.sh /home/mininet/drench/nfv_rsrc_files/list3.csv

3. Launch Mininet:
cd mn_scripts
./run_rftopo_sk.py

4. Launch Desired Flows in mininet CLI 'mininet>'
source ../scripts/testcases/n5_5f.py
