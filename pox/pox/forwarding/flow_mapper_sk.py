from pox.core import core
log = core.getLogger()
import pox.openflow.discovery_sk as disc

base_flow_info_tbl = {}
flow_info_tbl = {} #{ 'key=match_fileds': value=FlowMapper Object}
flow_rel_tbl = {}
##Function for Extracting Key for Flows
def get_realted_flows_key_from_match(match):
    flow_key = (match.nw_src, match.nw_proto, match.nw_tos, match.tp_src, match.nw_dst)
    #flow_key = (match.dl_src, match.nw_src, match.nw_proto, match.nw_tos, match.tp_src, match.nw_dst, match.dl_dst)
    #flow_key = (match.dl_src, match.nw_src, match.nw_proto, match.nw_tos, match.tp_src, match.nw_dst)
    return flow_key

def get_flow_base_key_from_match(match):
    #flow_key = (match.dl_src, match.nw_proto, match.nw_tos, match.tp_src) #[SRC_MAC, SRC_IP, IP_PROTO, DST_IP, DST_MAC]
    flow_key = (match.dl_src, match.nw_proto, match.nw_tos, match.tp_src, match.tp_dst, match.nw_dst, match.dl_dst) #[SRC_MAC, SRC_IP, IP_PROTO, DST_IP, DST_MAC]
    return flow_key
def get_flow_key_from_match(match):
    #flow_key = (match.dl_src, match.nw_src,  match.nw_proto, match.nw_tos, match.tp_src, match.tp_dst, match.nw_dst, match.dl_dst) #[SRC_MAC, SRC_IP, IP_PROTO, DST_IP, DST_MAC]
    flow_key = (match.dl_src, match.nw_proto, match.nw_tos, match.tp_src, match.tp_dst, match.dl_dst) #[SRC_MAC, SRC_IP, IP_PROTO, DST_IP, DST_MAC]
    return flow_key
def get_rev_flow_key_from_match(match):
    #flow_key = (match.dl_dst, match.nw_dst, match.tp_dst, match.tp_src, match.nw_tos, match.nw_proto, match.nw_src, match.dl_src) #[SRC_MAC, SRC_IP, IP_PROTO, DST_IP, DST_MAC]
    flow_key = (match.dl_dst, match.tp_dst, match.tp_src, match.nw_tos, match.nw_proto, match.dl_src) #[SRC_MAC, SRC_IP, IP_PROTO, DST_IP, DST_MAC]
    return flow_key

### Helper functions for extracting Links of Path and RTT info
def get_links_of_path(path):
    if path is None:
        return None
    
    links_of_path = []
    for index, (sw,in_port,out_port) in enumerate(path): 
        if index == 0:
            #prev_sw = sw
            #prev_outp = out_port
            pass
        else:
            link = disc.get_link_from_sw_and_port(sw_dpid=sw.dpid, port=in_port)
            if link is None:
                log.debug("Retrieved Null link For (%s)", (sw,in_port,out_port))
                break
            else:
                links_of_path.append(link)
    #log.warning("Retrieved links: (%s) For Path: (%s)",links_of_path, path )
    return links_of_path

class RelatedFlows():
    def __init__(self, in_key, match, path=None, svc_id=None, svc_mac_addr=None, src_mac_addr=None,dst_path_len=None):
        self.flow_id = len(flow_rel_tbl) + 1
        self.key = in_key #get_realted_flows_key_from_match(match)
        self.src_mac_addr = None
        self.dst_mac_addr = None 
        self.src_ip = None
        self.dst_ip = None
        self.nw_tos = None
        self.src_port = None
        self.dst_port = None
        self.match = None
        self.svc_list = []
        self.path_map = {} # list of [svc_id, svc_mac_addr, len(path), path, src_mac_addr]
        self.ttl_plen = 0
        self.dst_plen = 0
        
        if dst_path_len:
            self.set_dst_path_len(dst_path_len)
        if match:
            self.set_params_from_match(match)
        if path:
            self.set_path_map(svc_id,svc_mac_addr,path,src_mac_addr)
    
    def pop_svc_flow(self, svc_id,path):
            if svc_id not in self.svc_list:
                log.warning("Requested svc_id [%d] is not in the related Flow Table[%s]!]", svc_id,self.svc_list)
                return len(self.svc_list)
            index = self.svc_list.index(svc_id)
            self.svc_list.pop(index)
            item = self.path_map.pop(svc_id)
            self.ttl_plen -= item[2]
            return len(self.svc_list)
    def pop_svc_flow_by_path(self,path):
            svc_id = None
            for key, path_info in  self.path_map.items(): #enumerate(self.path_map):
                if path == path_info[3]: svc_id = path_info[0]
                break
            if svc_id is not None:
                self.pop_svc_flow(svc_id,path)
                
    def set_path_map(self,svc_id, svc_mac_addr, path, src_mac_addr):
        if svc_id in self.svc_list: self.pop_svc_flow(svc_id,path)
        self.svc_list.append(svc_id)
        self.path_map[svc_id] = [svc_id, svc_mac_addr, len(path), path, src_mac_addr]
        self.ttl_plen+=len(path)
        return len(self.svc_list)
        return
    def set_params_from_match(self, match):
        self.src_mac_addr = match.dl_src
        self.dst_mac_addr = match.dl_dst 
        self.src_ip = match.nw_src
        self.dst_ip = match.nw_dst
        self.nw_tos = match.nw_tos
        self.src_port = match.tp_src
        self.dst_port = match.tp_dst
        #self.svc_list.append(svc_id)
        self.match = match
        
    def set_dst_path_len(self,dst_path_len):
        self.dst_plen = dst_path_len #+1
    def get_key(self):
        return self.key
    def get_match(self):
        return self.match
    def get_l2_src_and_dst(self):
        return(self.src_mac_addr, self.dst_mac_addr)
    def get_l3_src_and_dst(self):
        return(self.src_ip, self.dst_ip)    
class FlowMapper():
    def __init__(self, in_key, match, path=None, svc_id=None, svc_mac_addr=None, src_mac_addr=None, dst_mac_addr=None,p_to_dest=None):
        self.in_key = in_key
        self.key = None
        self.rev_key = None
        self.match = None
        self.path = None
        self.actions = None
        self.src_sw_dpid = None
        self.src_sw_in_port = None
        self.svc_id = None
        self.svc_mac_addr = None
        self.src_mac_addr = None
        self.dst_mac_addr = None
        self.path_to_dest = None
        if match:
            self.set_key_from_match(match)
        if path:
            self.set_path(path)
        if svc_id is not None:
            self.svc_id = svc_id
        if svc_mac_addr:
            self.svc_mac_addr = svc_mac_addr
        if src_mac_addr:
            self.src_mac_addr = src_mac_addr
        if dst_mac_addr:
            self.dst_mac_addr = dst_mac_addr
        if p_to_dest:
            self.path_to_dest = p_to_dest
        return
    def get_key(self):
        return self.key
    def get_rev_key(self):
        return self.rev_key
    def get_match(self):
        return self.match
    def get_path(self):
        return self.path
    def get_path_to_destination(self):
        return self.path_to_dest
    def get_svc_mac_addr(self):
        return self.svc_mac_addr
    def get_l2_info(self):
        src_mac = self.match.dl_src
        dst_mac = self.match.dl_dst
        dl_type = self.match.dl_type
        return(src_mac,dst_mac,dl_type)
    def get_l3_info(self):
        src_ip = self.match.nw_src
        dst_ip = self.match.nw_dst
        nw_proto = self.match.nw_proto
        return(src_ip,dst_ip,nw_proto)
    def get_l4_info(self):
        sport = self.match.tp_src
        dport = self.match.tp_dst
        return(sport, dport)
    def get_src_mac_and_ip(self):
        src_mac = self.match.dl_src
        src_ip =  self.match.nw_src
        return(src_mac,src_ip)
    def get_dst_mac_and_ip(self):
        dst_mac = self.match.dl_dst
        dst_ip =  self.match.nw_dst
        return(dst_mac,dst_ip)
    def get_nw_proto_src_and_dst_ports(self):
        proto = self.match.nw_proto
        sport = self.match.tp_src
        dport = self.match.tp_dst
        return(proto, sport, dport)
    def set_path(self,path):
        self.path = path
        for index, (sw,in_port,out_port) in enumerate(path):
            self.src_sw_dpid = sw.dpid
            self.src_sw_in_port = in_port
            break
        return
    def set_key_from_match(self,match):
        self.match = match
        self.key = get_flow_key_from_match(match)
        self.rev_key = get_rev_flow_key_from_match(match)
        if self.key != self.in_key:
            log.error("Key in Dictionary[%s] and Key extracted from match [%s] differ!!", self.in_key, self.key)
        return

##Flow Info table Mgt functions
def add_flow_to_flow_info_table(match, path=None, event=None, svc_id=None, svc_mac_addr=None, src_mac_addr=None, dst_mac_addr=None, p_to_dest=None, dst_path_len=None):
    global flow_info_tbl, base_flow_info_tbl
    
    #Add the base_key to base_flow_info_tbl
    #base_key = get_flow_base_key_from_match(match)
    #if base_flow_info_tbl.has_key(base_key): base_flow_info_tbl[base_key]+=1
    #base_flow_info_tbl[base_key] = 0
    ##flow_obj = FlowMapper(in_key=base_key, match=None, path=path, svc_id=svc_id, svc_mac_addr=svc_mac_addr)
    
    if match is None: return False
    
    flow_key = get_flow_key_from_match(match)
    #log.info("<add_flow_to_flow_info_table>Flow Key is: %s", flow_key)
    
    if flow_info_tbl.has_key(flow_key):
        log.warning("Flow Info Table already has entry for key(%s):(%s)", flow_key, flow_info_tbl[flow_key])
        return False
    
    #rev_flow_key = get_rev_flow_key_from_match(match)
    #if flow_info_tbl.has_key(rev_flow_key): # and (match.nw_proto == ipv4.TCP_PROTOCOL or match.nw_proto == ipv4.UDP_PROTOCOL):
    #    log.info("Skipped Adding the flow <add_flow_to_flow_info_table>Flow Key is: %s", flow_key)
    #    return False
    
    #Add the Info to the Flow_info_table
    flow_obj = FlowMapper(in_key=flow_key, match=match, path=path, svc_id=svc_id, svc_mac_addr=svc_mac_addr,src_mac_addr=src_mac_addr, dst_mac_addr=dst_mac_addr,p_to_dest=p_to_dest)
    flow_info_tbl[flow_key] = flow_obj
    #log.info("New Flow entry with key(%s) is added to table!", flow_key)
    
    #Check for related flows and add
    fr_key = get_realted_flows_key_from_match(match)
    if flow_rel_tbl.has_key(fr_key):
        cur_related_flows = flow_rel_tbl[fr_key].set_path_map(svc_id,svc_mac_addr,path,src_mac_addr)
        log.warning("Set of current related flows for flow_key(%s) = %d", fr_key, cur_related_flows)
    else:
        fr_obj = RelatedFlows(in_key=fr_key, match=match, path=path, svc_id=svc_id, svc_mac_addr=svc_mac_addr, src_mac_addr=src_mac_addr, dst_path_len=dst_path_len)
        flow_rel_tbl[fr_key] = fr_obj
        log.warning("Set of current related flows for New flow_key(%s) = %d", fr_key, 1)
    return True

def del_flow_from_flow_info_table(match,event=None):
    global flow_info_tbl, base_flow_info_tbl
    
    #Remove the base_key to base_flow_info_tbl
    #base_key = get_flow_base_key_from_match(match)
    #if base_flow_info_tbl.has_key(base_key): base_flow_info_tbl[base_key]-=1
    #if base_flow_info_tbl[base_key] <= 0: base_flow_info_tbl.pop(base_key)

    if match is None:
        log.error("Invalid Input! Cannot remove the flow from flow_info_table")
        return
    
    flow_key = get_flow_key_from_match(match)
    log.info("<del_flow_from_flow_info_table> Flow Key is: %s", flow_key)
    
    del_flow_from_flow_info_table_by_key(flow_key)

    return
def del_flow_from_flow_info_table_by_key(flow_key):
    global flow_info_tbl
    
    if not flow_info_tbl.has_key(flow_key):
        log.warning("<del_flow_from_flow_info_table_by_key> Flow Key:%s is not found in flow_info_table", flow_key)
        return
    
    flow_info = flow_info_tbl.pop(flow_key)
    #log.debug("<del_flow_from_flow_info_table_by_key> Flow Key: %s removed from flow_info_table!", flow_key)
    
    
    fr_key = get_realted_flows_key_from_match(flow_info.match)
    if flow_rel_tbl.has_key(fr_key):
        sts = flow_rel_tbl[fr_key].pop_svc_flow(flow_info.svc_id,flow_info.path)
        log.warning("deleted sub-flow for keys(%s, %s), remaining[%s]", flow_key, fr_key, sts)    
        if sts is 0:flow_rel_tbl.pop(fr_key)
    else: log.warning("Failed to delete sub-flow for keys(%s, %s)", flow_key, fr_key)    
    return
    
def check_flow_in_flow_info_table(match):
    if match is None: return False
    
    #base_key = get_flow_base_key_from_match(match)
    #log.warning("base_key: [%s], match =[%s]", base_key, match)
    #if base_flow_info_tbl.has_key(base_key) is False:
    #    base_flow_info_tbl[base_key] = 1
    #    return False
    #return base_flow_info_tbl.has_key(base_key)
    
    flow_key = get_flow_key_from_match(match)
    #log.info("<check_flow_in_flow_info_table>Flow Key is: %s", flow_key)
    #if flow_info_tbl.has_key(flow_key): log.warning ("Input Flow present with key: %s", flow_key)
    return flow_info_tbl.has_key(flow_key)
    
def get_total_flow_entries_count():
    return len(flow_info_tbl)
    
## Functions to extract Params from FlowMapper
def get_svc_mac_addr_of_flow_from_flow_info_table(flow_key):
    if flow_info_tbl.has_key(flow_key) and flow_info_tbl[flow_key] is not None:
        return flow_info_tbl[flow_key].get_svc_mac_addr()
    return None 
def get_path_of_flow_from_flow_info_table(flow_key):
    if flow_info_tbl.has_key(flow_key) and flow_info_tbl[flow_key] is not None:
        return flow_info_tbl[flow_key].get_path()
    return None
def get_match_of_flow_from_flow_info_table(flow_key):
    if flow_info_tbl.has_key(flow_key) and flow_info_tbl[flow_key] is not None:
        return flow_info_tbl[flow_key].get_match()
    return None
def get_src_mac_and_src_ip_from_flow_key(flow_key):
    global flow_info_tbl
    if flow_info_tbl.has_key(flow_key) and flow_info_tbl[flow_key] is not None:
        return flow_info_tbl[flow_key].get_src_mac_and_ip()
    return (None,None)
def get_dst_mac_and_ip_from_flow_key(flow_key):
    if flow_info_tbl.has_key(flow_key) and flow_info_tbl[flow_key] is not None:
        return flow_info_tbl[flow_key].get_dst_mac_and_ip()
    return (None,None)
def get_proto_src_and_dst_ports_from_flow_key(flow_key):
    if flow_info_tbl.has_key(flow_key) and flow_info_tbl[flow_key] is not None:
        return flow_info_tbl[flow_key].get_nw_proto_src_and_dst_ports()
    return (None,None,None)
def get_dstination_path_of_flow_from_flow_info_table(flow_key):
    if flow_info_tbl.has_key(flow_key) and flow_info_tbl[flow_key] is not None:
        return flow_info_tbl[flow_key].get_path_to_destination()
    return None

#Additional Functions for Service Specific Handling: < Better IP based as cloudlab IP is configurable but not MAC >
def get_total_flow_count_for_svc_id(svc_id):
    fcount = sum([1 for k,v in flow_info_tbl.items() if v.svc_id == svc_id ])
    return fcount
def get_all_flows_for_svc_id(svc_id):
    svc_id_flows_dict = { k:v for k,v in flow_info_tbl.items() if v.svc_id == svc_id }
    return svc_id_flows_dict

def get_total_flow_count_for_svc_id_at_svc_mac(svc_id, svc_mac_addr):
    fcount = sum([1 for k,v in flow_info_tbl.items() if v.svc_id == svc_id and v.svc_mac_addr == svc_mac_addr])
    return fcount
def get_all_flows_for_svc_id_at_svc_mac(svc_id, svc_mac_addr):
    svc_id_flows_dict = { k:v for k,v in flow_info_tbl.items() if v.svc_id == svc_id and v.svc_mac_addr == svc_mac_addr}
    
    ##Debug:
    #for k,v in flow_info_tbl.items():
    #    if v.svc_id == svc_id:
    #        log.warning("SVC=[%s], input_mac=[%s] and dict_mac=[%s]", svc_id, svc_mac_addr, v.svc_mac_addr)
    
    return svc_id_flows_dict

def get_total_flow_count_for_svc_id_from_src_mac(svc_id, src_mac_addr):
    fcount = sum([1 for k,v in flow_info_tbl.items() if v.svc_id == svc_id and v.src_mac_addr == src_mac_addr])
    return fcount
def get_all_flows_for_svc_id_from_src_mac(svc_id, src_mac_addr):
    svc_id_flows_dict = { k:v for k,v in flow_info_tbl.items() if v.svc_id == svc_id and v.src_mac_addr == src_mac_addr}
    return svc_id_flows_dict

def get_total_svc_instance_list_for_svc_id(svc_id):
    svc_inst_list = []
    [svc_inst_list.append(v.svc_mac_addr) for k,v in flow_info_tbl.items() if v.svc_id == svc_id and v.svc_mac_addr not in svc_inst_list]
    return svc_inst_list
def get_total_svc_instance_count_for_svc_id(svc_id):
    svc_inst_list = []
    [svc_inst_list.append(v.svc_mac_addr) for k,v in flow_info_tbl.items() if v.svc_id == svc_id and v.svc_mac_addr not in svc_inst_list]
    #svc_inst_list = [v.svc_mac_addr for k,v in flow_info_tbl.items() if v.svc_id == svc_id]
    return len(svc_inst_list)

#Functions to alter the Flowmap
def update_flow_map_on_svc_instance_removal(svc_id,svc_mac_addr):
    global flow_info_tbl
    pop_keys_list = [k for k,v in flow_info_tbl.items() if v.svc_id == svc_id and v.svc_mac_addr == svc_mac_addr]
    log.info("Removing svc[%d] at svc_instance[%s] results in eviction of [%d] flows: [%s]", svc_id, svc_mac_addr,len(pop_keys_list),pop_keys_list)
    for key in pop_keys_list:
        #flow_info_tbl.pop(key)
        del_flow_from_flow_info_table_by_key(key)
    return

def get_related_flow_table():
    return flow_rel_tbl

import csv
def log_relate_flow_info(cur_time, log_file_func):
    
    with open(log_file_func, 'ab') as csvfile:
        logWriter = csv.writer(csvfile,delimiter=',')
        ttl_valid_flows, ttl_avg_dst_len, ttl_avg_plen,ttl_avg_deviation,ttl_valid_sub_flows = 0,0,0,0,0
        datax=""
        for key,val in flow_rel_tbl.items():
            data,data1,data2,data3  = "","","",""
            
            #data += str('Flow:') + str(val.flow_id) + '@' + str(val.src_ip) + '_' + str(val.nw_tos) + '_' + str(val.dst_ip)
            data = str(val.flow_id)
            #data1 += str('DstLen:') + str(val.dst_plen)
            data1 = str(val.dst_plen)
            
            #data2 += str('PathLen:') + str(val.ttl_plen)
            data2 = str(val.ttl_plen)
            
            #data += str('Flow:') + str(val.flow_id) + '@' + str(val.src_ip) + '_' + str(val.nw_tos) + '_' + str(val.dst_ip) + '=>'
            #data += str('DstLen:') + str(val.dst_plen) + ':' + str('PathLen:') + str(val.ttl_plen) + '::' 
            
            data3 = str('SubFlowsInfo:') + str(key)
            ttl_sub_flows = 0
            path_till_end_dst = 0
            for svc_id, path_info in val.path_map.items():
                svc_p = (path_info[0], path_info[1], path_info[2])
                data3 += str(svc_p) + ','
                if path_info[0] == 0: path_till_end_dst = 1
                ttl_sub_flows+=1
            data3 = data3.rstrip(',')
            datax += data3
            
            if ttl_sub_flows >=2 and (val.ttl_plen >= val.dst_plen) and path_till_end_dst:
            #if ttl_sub_flows ==4 and (val.ttl_plen >= val.dst_plen):
                #logWriter.writerows([[int(cur_time), str(data), str(data1),str(data2),str(data3)]])
                ttl_valid_flows+=1
                ttl_avg_dst_len +=val.dst_plen
                ttl_avg_plen +=val.ttl_plen
                ttl_avg_plen -= (ttl_sub_flows -1)
                ttl_valid_sub_flows +=ttl_sub_flows

        if log_relate_flow_info.old_data and log_relate_flow_info.old_data == datax: return
        log_relate_flow_info.old_data = datax
        
        if ttl_valid_flows and ttl_avg_dst_len and ttl_avg_plen >= ttl_avg_dst_len:
            ttl_avg_deviation = 100*(ttl_avg_plen - ttl_avg_dst_len)/(ttl_avg_dst_len*ttl_valid_flows)
            logWriter.writerows([[int(cur_time), str(ttl_avg_dst_len), str(ttl_avg_plen), str(ttl_avg_deviation), str(ttl_valid_flows), str(ttl_valid_sub_flows)]])
        #logWriter.writerows([[int(cur_time), str(ttl_avg_dst_len),str(ttl_avg_plen),str(ttl_avg_deviation), str(ttl_valid_flows), str(ttl_valid_sub_flows), str(datax)]])
    return
log_relate_flow_info.old_data = None