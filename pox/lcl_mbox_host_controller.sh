#!/bin/bash
#usage: drench_controller.sh <base_directiory> <resource file path in base_dir/nfv_rsrc_files> <svc chain file in base_dir/svc_chains>
# ./drench_controller.sh /home/mininet/drench list3.csv svc_chain_dict.csv

#From Windows Machine (Try this directly instead of a script)
#python pox.py openflow.util_sk --base_dir=C:\Users\skulkar\Academics\bitbucket\drench --rsrc_file=C:\Users\skulkar\Academics\bitbucket\drench\nfv_rsrc_files\list3.csv --svc_chain_info_file=C:\Users\skulkar\Academics\bitbucket\drench\svc_chains\svc_chain_dict.csv openflow.discovery_sk forwarding.l2_multi_sk_lcl samples.flow_stats_sk_lcl
#./lcl_mbox_host_controller.sh /home/mininet/drench list3.csv svc_chain_dict.csv h1-eth0 00:00:00:00:00:01
#

echo " Arguments are:" $array, $len
echo "Args: $@"
echo "Number_of_args: $#"
echo " Argments are : $1, $2, $3, $4, $5"
#base_dir=`pwd`
base_dir=$HOME"/drench"
if [ $# -ge 1 ];
then
  base_dir=$1
  echo "updated base_dir: $base_dir"
fi
rsrc_file=$base_dir"/nfv_rsrc_files/list3.csv"
if [ $# -ge 2 ];
then
  rsrc_file=$base_dir/nfv_rsrc_files/$2
  echo "updated resource file: $rsrc_file"
fi
policy_file=$base_dir/svc_chains/svc_chain_dict.csv
if [ $# -ge 3 ];
then
  policy_file=$base_dir/svc_chains/$3
  echo "updated policy file: $policy_file"
fi
if [ $# -ge 4 ];
then
  host_if_name=$4
  echo "updated host_if_name: $host_if_name"
fi
if [ $# -ge 5 ];
then
  host_mac_addr=$5
  echo "updated host_mac_addr: $host_mac_addr"
fi

cleanup() {
#    sudo killall -9 python
#    sudo fuser -k 6633/tcp
    cp controller_logs.txt /home/mininet/drench/results/
    rm controller_logs.txt
    cp *.csv /home/mininet/drench/results/
    #rm *.csv
    #rm -rf /home/mininet/dre/results/* 
    echo "*** cleanup completed ****"
}
ctrlc() {
    cleanup
    echo "*** Exitting!! ****"
    exit
}
trap ctrlc SIGINT

ctrl_dir=$base_dir/pox
#sudo ./pox.py samples.topo_sk log.level --WARNING --openflow.discovery_sk=WARNING --forwarding.l2_multi_sk=WARNING --samples.flow_stats_sk=INFO log --file=controller_logs.txt,w
#sudo ./pox.py samples.topo_sk log.level --WARNING --openflow.discovery_sk=WARNING --forwarding.l2_multi_sk=WARNING --samples.flow_stats_sk=WARNING log --file=controller_logs.txt,w
#sudo ./pox.py samples.topo_sk log.level --WARNING --openflow.discovery_sk=WARNING --forwarding.l2_multi_sk=WARNING --samples.flow_stats_sk=INFO log --file=controller_logs.txt,w
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log log.level --INFO
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log log.level --ERROR
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log log.level --INFO

#cp -rf ~/pox_wm/pox ~/pox_d/
#rename "s/\.csv$/\_$(date +%H-%M-%S)\.csv/" *.csv
#mv *.csv bkp/

#sudo ./pox.py openflow.discovery_sk --base_dir=$base_dir, --rsrc_file=$rsrc_file forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log openflow.keepalive log.level --WARNING log --file=controller_logs.txt,w
sudo python $ctrl_dir/pox.py openflow.util_sk  --base_dir=$base_dir --rsrc_file=$rsrc_file --svc_chain_info_file=$policy_file openflow.discovery_sk_lcl forwarding.l2_multi_sk_lcl --iname=$host_if_name --imacaddr=$host_mac_addr samples.flow_stats_sk_lcl --iname=$host_if_name samples.pretty_log openflow.keepalive log.level --WARNING log --file=$ctrl_dir/lcl_controller_logs_${host_if_name}.txt,w

