#!/bin/bash
#NumSwitches="$1"
#NumSwitches=5
NumSwitches="$1"
if [ $NumSwitches == 0 ]
then
    $NumSwitches = 5
fi
for(( s=1; s<=$NumSwitches; s++ ))
do
    echo "Dumping Switch flow for s${s}:"
    sudo ovs-ofctl dump-flows s${s}
done
