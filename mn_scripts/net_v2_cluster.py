#!/usr/bin/python
#usage
#sudo python net_v2.py base_dir_path conf_file_name svc_chain_file other_options
#sudo python net_v2.py -b /home/mininet/drench -c demotopo_conf.csv -s svc_chain_dict_3_6.csv
#sudo python net_v2.py -b /home/mininet/drench -c demotopo_conf.csv -s svc_chain_dict.csv -cip 172.23.0.33 -cpt 6633 -n 20

 
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.topolib import TreeTopo
from mininet.node import RemoteController
from mininet.log import setLogLevel
from mininet.examples.cluster import MininetCluster, SwitchBinPlacer
from mininet.examples.clustercli import ClusterCLI as CLI
#from mininet.cli import CLI
from mininet.link import TCLink
from mininet.node import CPULimitedHost
from mininet.util import custom, quietRun
import csv
import os.path
import random
import sys
import time
import argparse
from threading import Thread
from os.path import expanduser
import ovs_rule_dumper as ovsrd
hdir_p = expanduser('~')
#hdir_p = '/local'

base_dir = os.path.join(hdir_p, 'drench')
rdir_p = os.path.join(base_dir,'results')
topo_dir = os.path.join(base_dir,'topologies')
test_dir = os.path.join(base_dir,'code')
code_dir = os.path.join(base_dir,'code')
tscr_dir = os.path.join(base_dir,'scripts')
svch_dir = os.path.join(base_dir,'svc_chains')
scrp_dir = os.path.join(base_dir,'mn_scripts')

MAX_SWITCHES = 5
MAX_HOSTS    = 6

#Configuration File <Total Switches, Total Hosts>
conf_file = os.path.join(topo_dir, 'demotopo_conf.csv')
#conf_file = topo_dir + 'gfc1_conf.csv'
#conf_file = topo_dir + 'gfc2_conf.csv'

# Switch-Switch Links File <Hid, Swid, Weight, TotalHosts, TotalLinks>
slink_file = os.path.join(topo_dir, 'demotopo.csv')
#slink_file = topo_dir + 'gfc1_slink.csv'

#Host to Switch Link File <Hid, Swid, Weight, TotalHosts, TotalLinks>
hlink_file = os.path.join(topo_dir, 'host_link_demotopo.csv')
#hlink_file = topo_dir + 'gfc1_hlink.csv'

#Service Chain File:
svc_ch_file = os.path.join(svch_dir , 'svc_chain_dict.csv')
#Test Script
test_file = None

#ecn_mode
ecn_mode = False

sched_type = 'cfs' # 'rt'
host_cpu_limit = 1
#controlelr Settings
#controller_ip = '134.76.249.236'
controller_ip = '172.23.0.33'
controller_port = 6633

num_of_nf_hosts=20
#cluster_ip_list
cluster_ip_list = []
def setup_cluster_list(servers):
    global cluster_ip_list
    servers_list = servers.rstrip('\n')
    servers_list = servers_list.split(',')
    cluster_ip_list = [str(s).rstrip(' ') for s in servers_list]
    print "Cluster List:%s" %cluster_ip_list

def update_paths():
  global hdir_p, base_dir, topo_dir, tscr_dir, svch_dir, scrp_dir, test_dir, code_dir
  
  rdir_p = os.path.join(base_dir,'results')
  topo_dir = os.path.join(base_dir,'topologies')
  test_dir = os.path.join(base_dir,'code')
  code_dir = os.path.join(base_dir,'code')
  tscr_dir = os.path.join(base_dir,'scripts')
  svch_dir = os.path.join(base_dir,'svc_chains')
  scrp_dir = os.path.join(base_dir,'mn_scripts')

def parse_named_args():
    global base_dir, conf_file, svc_ch_file, test_file, ecn_mode, controller_ip,controller_port, num_of_nf_hosts
    is_parsed = False
    
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--base_dir', '-b', required=True,help='Base Directory of execution')
    parser.add_argument('--cfg', '-c', required=True, help='Config file with description of Topology')
    parser.add_argument('--svch', '-s', help='Service Chain File')
    parser.add_argument('--cip', '-cip', help='Controller IP')
    parser.add_argument('--cpt', '-cpt', type=int, help='Controller Port')
    parser.add_argument('--cluster_list', '-cllst', help='Cluster_ip_list')
    parser.add_argument('--test', '-t', help='Script of test file to execute')
    parser.add_argument('--ecn', '-e', type=int, help='ECN Mode')
    parser.add_argument('--num_nfv', '-n', type=int, help='Number of Hosts treated as NFs')
    args = parser.parse_args()
    
    if args.base_dir:
        base_dir = args.base_dir
        update_paths()
        is_parsed = True
    if args.cfg:
        conf_file = os.path.join(topo_dir, args.cfg)
        is_parsed = True
    if args.svch:
        svc_ch_file = os.path.join(svch_dir, args.svch)
    if args.test:
        test_file = os.path.join(tscr_dir, args.test)
    if args.ecn:
        ecn_mode = args.ecn
    if args.cip:
        controller_ip = args.cip
    if args.cpt:
        controller_port = args.cpt
    if args.cluster_list:
        setup_cluster_list(args.cluster_list)
    if args.num_nfv:
        num_of_nf_hosts=args.num_nfv
    return is_parsed
    
def parse_args():
    global conf_file
    global test_file
    global ecn_mode
    #global slink_file
    #global hlink_file
    if parse_named_args():
        return
    print "Python Program Args: ", sys.argv
    #Note: sys.argv[0] will be the program name "rftopo_sk.py" subsequent will be arguements to program
    try:
        i = 1
        if(len(sys.argv)>=i):
            cf_name = str(sys.argv[i])
            if cf_name:
                conf_file = topo_dir + cf_name
            #edst  = "00:00:00:00:00:" + "{:0>2d}".format(dst_id)
            i +=1
        if(len(sys.argv)>=i):
            cf_name = str(sys.argv[i])
            if cf_name:
                test_file = tscr_dir + cf_name
            i +=1
        if(len(sys.argv)>=i):
            cf_name = int(sys.argv[i])
            if cf_name:
                ecn_mode = True
            i +=1
    except Exception,e:
        print "Failed to parse the args!"
    print "conf_file:", conf_file, "test_file", test_file
    return

def progress(t=0):
    """Report progress of time""
    while t > 0:
        #cprint('  %3d seconds left  \r' % (t), 'cyan', cr=False)
        t = t-1
        #sys.stdout.flush()
        time.sleep(float(1))
    """
    print "sleeing for:", t
    time.sleep(float(t))
    return
def get_test_duration_from_cmd(cmd):
    duration = 0
    if cmd:
        lst1 = cmd.split(' ')
        lst = [ x for x in lst1 if x and len(x) > 0 and x.isdigit() == True]
        if len(lst) >= 4:
            duration = lst[3]
        pass
    print "Test Duration:", duration
    return duration

def parse_cmd_from_test_line(line):
    node = None
    cmd = None
    if not line:
        return (node,cmd)
    key = 'net.get(\''
    tsy = '\''
    if key in line:
        of = line.find(key)
        ln = len(key)
        node = line[of+ln:line.find(tsy,of+ln)]
        key = '.cmd(\''
        if key in line:
            of = line.find(key)
            ln = len(key)
            cmd = line[of+ln:line.find(tsy,of+ln)]
    else:
        key = 'time.sleep('
        tsy = ')'
        if key in line:
            of = line.find(key)
            ln = len(key)
            node = line[of+ln:line.find(tsy,of+ln)]
        else:
            key = 'py '
            if key in line:
                of = line.find(key)
                ln = len(key)
                cmd = line[of+ln:]
        pass
    print "node:", node, "cmd:", cmd
    return node,cmd

def read_and_launch_test(net):
    test_duration = 0
    if os.path.isfile(test_file) is False:
        return test_duration
    f = open(test_file, 'r')
    for line in f:
        h1, cmd = parse_cmd_from_test_line(line)
        if h1 is None and cmd is None:
            pass
        elif h1 is not None and cmd is None:
            time.sleep(float(h1))
        elif h1 is None and cmd is not None:
            os.system(cmd)
        else:
            h1 = net.getNodeByName(h1) #net.getNodeByName('h%s' % h1)
            if h1:
                h1.cmdPrint(cmd)
                if test_duration == 0:
                    test_duration = get_test_duration_from_cmd(cmd)
    return test_duration

def perform_test(net):
    test_duration = read_and_launch_test(net)
    progress(test_duration)
    return

def get_params_from_conf_file():
    global conf_file, slink_file, hlink_file
    global MAX_HOSTS, MAX_SWITCHES
    
    if(os.path.isfile(conf_file)):
        pass
    else:
        print conf_file, "Conf File Not Found! Return!!"
        return True

    #Get the Switch Count and Host Count from CSV File Header
    slk_file = None
    hlk_file = None
    with open(conf_file, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        index = 0
        for row in reader:
            try:
                if index == 0 and len(row) >=2:
                    MAX_SWITCHES = int(row[0])
                if index == 0 and len(row) >=2:
                    MAX_HOSTS    = int (row[1])
                if index == 0 and len(row) >=3:
                    slk_file = row[2]
                    if slk_file is not None:
                        slink_file = topo_dir + '/' + slk_file
                        if(os.path.isfile(slink_file)): pass
                        else: return True
                if index == 0 and len(row) >=4:
                    hlk_file = row[3]
                    if hlk_file is not None:
                        hlink_file = topo_dir + '/' + hlk_file
                break
            except Exception,e:
                print " Failed to read configuration header,", str(e), "Defaults for Max Switches [%d] and Max Hosts [%d], Link files [%s,%s]" %(MAX_SWITCHES, MAX_HOSTS, slk_file,hlk_file)
                return True
    return False

def setup_list_from_file(topo, link_file, src_node_list, dst_node_list, linkopts=None):
    return
def export_conn_list(conn_list, conn_type, conn_file):
    conn_str = ""
    for i, conn in enumerate(conn_list):
        c_str = conn[0] + ':' + conn[1]
        conn_str += c_str
        if i+1 < len(conn_list): conn_str += ','
    if conn_file:
        new_csv_file = conn_file.split('.')[0] + 'cloudlab_links_' + conn_type + conn_file.split('.')[1]
    else:
        new_csv_file = 'cloudlab_links_' + conn_type + '.csv'
        new_csv_file =  os.path.join(topo_dir, new_csv_file)
    
    with open(new_csv_file, 'w') as f:
        f.write(conn_str)
    return
    
def build_topo():
    "Construct Topology from CSV File (Use Small Scale and Large Scale)"
    global conf_file, slink_file, hlink_file
    global MAX_HOSTS, MAX_SWITCHES
    global host_cpu_limit
    if get_params_from_conf_file():
        return None
    
    topo = Topo(controller=None)

    # Add Switches
    switchlist = [topo.addSwitch('s%i' %i) for i in range(1,MAX_SWITCHES+1)]
    print "\n", switchlist, "\n"
  
    # Add Hosts
    hostlist = [topo.addHost('h%i' %i, ip='10.0.0.%i' %i ) for i in range(1,MAX_HOSTS+1)]
    print "\n", hostlist, "\n"

    #Set Default Link Options
    #linkopts = dict(delay='1ms')
    linkopts = dict(bw = 1000, delay='1ms')
    #linkopts = dict(bw=10, delay='1ms', enable_ecn=True, enable_red =True, max_queue_size=1000) #loss=1
    #linkopts = dict(bw=10, delay='1ms', max_queue_size=1000)
    #if ecn_mode is True:
    #    linkopts = dict(bw=10, delay='1ms', max_queue_size=75, enable_ecn=True)

    #Add Host to Switch Links
    link_count = 0
    if(os.path.isfile(hlink_file)):
        connlist = []
        with open(hlink_file, 'rb') as csvfile:
            reader = csv.DictReader(csvfile)
            link_count = 0
            for row in reader:
                sw1 = int(row['Host'])
                sw2 = int(row['Switch'])
        
                if 'Delay' in row.keys():
                    delay = int(row['Delay'])
                elif 'Weight' in row.keys():
                    delay  = int(row['Weight'])
                else:
                    delay  = 5
                linkopts['delay'] = str(delay)+ 'ms'
        
                if 'Bandwidth' in row.keys():
                    bwd = int(row['Bandwidth'])
                    linkopts['bw'] = bwd
                else: 
                    bwd = 10000  #10Gbps
        
                #print "ECN: Host:%i, Switch:%i, Delay:%i, BandWidth:%i " %(sw1,sw2,delay,bwd)
                s1 = 'h%i' %sw1
                s2 = 's%i' %sw2
                if( (s1 in hostlist) and (s2 in switchlist) and ((s1,s2) not in connlist) ):
          
                    topo.addLink(s1,s2)
                    #topo.addLink(s1,s2,**linkopts)
                    #topo.addLink('h%i'%sw1,'s%i' %sw2, delay='%ims' %delay)
                    #topo.addLink('h%i'%sw1,'s%i' %sw2, bw=wt)
          
                    link_count = link_count+1
                    connlist.append((s1,s2))
                    #print(" Added: Host:%d, Switch:%d, linkopts:%s",sw1,sw2,linkopts)
                else:
                    #print(" Skipped: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
                    pass
            print "[%i] Number of Host to Switch Links Created." %link_count
    else:
        connlist = []
        #Randomly insert the links 
        sws = range(1,MAX_SWITCHES+1)
        #random.shuffle(sws)
        for i in range(1,MAX_HOSTS+1):
            sw1 = 'h%i' %i
            sw2 = 's%i'%sws[((i-1)%MAX_SWITCHES)]
            topo.addLink(sw1, sw2)
            #topo.addLink(sw1, sw2,**linkopts)
            link_count = link_count+1
            connlist.append((sw1,sw2))
    
    
    #Export Host_to_Switch connection list as string format to CSV file
    export_conn_list(connlist, 'H_S', hlink_file)
    
    #set the CPU fraction for each host:
    frac = float(1/len(hostlist))
    host_cpu_limit = float("{0:.2f}".format(frac))
    
    #linkopts = dict(delay='1ms')
    linkopts = dict(bw = 1000, delay='1ms')
    #linkopts = dict(bw=10, delay='1ms', max_queue_size=75)
    #if ecn_mode is True:
    #    linkopts = dict(bw=10, delay='1ms', max_queue_size=75, enable_ecn=True)

    # Add Switch to Switch Links
    link_count = 0
    connlist = []
    with open(slink_file, 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        link_count = 0
        for row in reader:
            sw1 = int(row['Source'])
            sw2 = int(row['Destination'])
      
            if 'Delay' in row.keys():
                delay = int(row['Delay'])
            elif 'Weight' in row.keys():
                delay  = int(row['Weight'])
            else:
                delay  = 5
            linkopts['delay'] = str(delay)+ 'ms'
        
            if 'Bandwidth' in row.keys():
                bwd = int(row['Bandwidth'])
                linkopts['bw'] = bwd
            else:
                bwd = 10  #10Mbps

        
            #print "Sw1:%i, Sw2:%i, Delay:%i, BandWidth:%i " %(sw1,sw2,delay,bwd)
            s1 = 's%i' %sw1
            s2 = 's%i' %sw2
            if( ( s1 in switchlist) and ( s2 in switchlist) and 
                (((s1,s2) not in connlist) and ((s2,s1) not in connlist))):
        
                topo.addLink(s1,s2 )
                #topo.addLink(s1,s2,**linkopts)
                #topo.addLink('s%i'%sw1,'s%i' %sw2, delay='%ims' %delay)
                #topo.addLink('s%i'%sw1,'s%i' %sw2, bw=wt)

                link_count = link_count+1
                connlist.append((s1,s2))
                #print(" Added: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
                #print("\n Added: Sw1:%d, Sw2:%d, linkopts:%s",sw1,sw2,linkopts)
            else:
                #print(" Skipped: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
                pass
        print "[%i] Number of Switch to Switch Links Created." %link_count
    
    #Export Switch_to_Switch connection list as string format to CSV file
    export_conn_list(connlist, 'S_S', slink_file)
    
    return topo
def stop_flow_count_monitor(num_switches):
    ovsrd.stopLogging()
    return
def start_flow_counter(num_switches):
    fcount_file = rdir_p + 'ovs_rules_count.csv'
    #ovsrd.startLogging(num_of_iterations=0, interval=5, num_of_switches=num_switches, log_file=fcount_file)
    return
def launch_flow_count_monitor(net):
    try:
        t = Thread(group=None,target=start_flow_counter,name="FCOUNTER", args=(), kwargs={'num_switches':len(net.switches)})
        t.start()
        #t.join()
        pass
    except Exception, e:
        print "Exception:", str(e)
    return
    
def launch_udp_flows(net):
    print "Launching All Flows from all hosts!"
    py_scrpt = os.path.join(scrp_dir, 'client_task.py')
    host_cmd = 'sudo python -u ' + py_scrpt + ' -b ' + base_dir + ' -s ' + svc_ch_file # + ' >/dev/null 2>&1 '
    num_cnt=0
    for h in net.hosts:
        num_cnt+=1
        if num_cnt<= num_of_nf_hosts: continue
        h.cmdPrint(host_cmd,'&')
        #h.cmdPrint('sudo python -u client_task.py >/dev/null 2>&1', '&')
        #h.cmdPrint('sudo python -u client_task.py')
        #h.cmdPrint('sudo python -u client_task.py', '&')
        time.sleep(0.5)
    print "Finished Triggering All Flows from all hosts!"
    time.sleep(float(5))
    return

def enable_spanning_tree_on_all_switches(net):
    #s = net.getNodeByName(sw)
    for s in net.switches:
        #cmd = 'ovs-vsctl set bridge %s stp-enable=true' %s.name
        cmd = 'ovs-vsctl set bridge %s stp-enable=true' %s # Both are same %s => __str(self)__ == self.name
        s.cmd(cmd)
    return
# For performance boost! Reference: https://floodlight.atlassian.net/wiki/display/floodlightcontroller/How+to+Use+OpenFlow+Meters 
def disable_all_port_gro_and_tso(net):
    nodes = net.hosts + net.switches
    for node in nodes:
        for port in node.ports:
            if str.format('{}', port) != 'lo':
                node.cmdPrint(str.format('ethtool --offload {} gro off gso off tso off', port))
    return
def disable_all_node_ipv6(net):
    nodes = net.hosts + net.switches
    for node in nodes:
        node.cmd("sysctl -w net.ipv6.conf.all.disable_ipv6=1")
        node.cmd("sysctl -w net.ipv6.conf.default.disable_ipv6=1")
        node.cmd("sysctl -w net.ipv6.conf.lo.disable_ipv6=1")
        node.cmd("sysctl -p")
    return
def save_all_results(net):
    dest_path = 'mininet@' + controller_ip + ':' + rdir_p
    for ip_addr in cluster_ip_list:
        cmd = 'scp -r '
        src_path = 'mininet@' + ip_addr + ':' + os.path.join(rdir_p, '*')
        cmd = cmd + src_path + ' ' + dest_path
        os.system(cmd) # '"%s" " %s" " %s"' % (cmd, src_path, dest_path)
        
        # or try ssh user@remote sudo scp /path/to/file user@local.machine.com:/local/path
        cmd = 'ssh mininet@' + ip_addr
        if ip_addr =='localhost': cmd = ''
        cmd += ' sudo scp -r '
        src_path = os.path.join(rdir_p, '*')
        dest_path = 'mininet@' + controller_ip + ':' + rdir_p
        cmd = cmd + src_path + ' ' + dest_path
        os.system(cmd)
        
        # if this works then cleanup the directories
        cmd = 'ssh  mininet@' + ip_addr
        cmd += ' sudo rm -rf ' + os.path.join(rdir_p, '*')
        os.system(cmd)
        
    return
def process_all_results(net):
    host_cmd = 'ITGDec '
    opts = ' -f t -c 10 -c 500'
    for h in net.hosts:
        log_file_name = h.IP() + '_flow_log.txt'
        log_file_name = os.path.join(rdir_p,log_file_name)
        out_file_name = h.IP() + '_combined_flow_out.dat'
        out_file_name = os.path.join(rdir_p,out_file_name)
        host_cmd = host_cmd + log_file_name + opts + out_file_name
        h.cmdPrint(host_cmd)
    
    #Gnu Plot 1
    cmd = 'gnuplot -p < '
    plot_script = os.path.join(scrp_dir, 'nw_util_plot.gnu')
    plot1_cmd = cmd + plot_script
    os.system(plot1_cmd)

    #Gnu Plot 2
    cmd = 'gnuplot -p < '
    plot_script = os.path.join(scrp_dir, 'nfv_util_plot.gnu')
    plot1_cmd = cmd + plot_script
    os.system(plot1_cmd)

    return
def cleanup_bkgd_procs(net):
    host_cmd = 'killall python'
    for h in net.hosts:
        h.cmdPrint(host_cmd)
def demo():

    topo = build_topo()
    #net = Mininet(topo=topo, controller=None, link=TCLink, autoSetMacs = False, autoStaticArp = False, cleanup = True)
    
    #net = MininetCluster(topo=topo, controller=None, link=TCLink, autoSetMacs = False, autoStaticArp = False, cleanup = True, servers=cluster_ip_list, placement=SwitchBinPlacer)
    net = MininetCluster(topo=topo, controller=None, autoSetMacs = False, autoStaticArp = False, cleanup = True, servers=cluster_ip_list, placement=SwitchBinPlacer)
    c1 = net.addController(name = 'c1', controller = RemoteController, ip = controller_ip, port = controller_port )

    for i, h in enumerate(net.hosts):
        mac = '00:00:00:00:00:00'
        if i <99:
            mac = "00:00:00:00:00:" + "{:0>2d}".format(i+1)
            #mac = "00:00:00:00:00:" + "{:0>2x}".format(i+1) # hex mode
        else:
            k = (i+1)/100
            j = ((i+1)%100)/10
            l = ((i+1)%100)%10
            mac = '00:00:00:00:0%i:%i%i' %(k,j,l)
        h.setMAC(mac)
        print "\n Host ", h.name, "has IP address", h.IP(), "and MAC address", h.MAC(), mac
    
    #Start network
    #net.build()
    c1.start()
    #print "\n Switches:"
    #for s in net.switches:
        #print s
        #s.start([c1])
  
    net.start()
    net.staticArp()
    #net.pingAll()
    time.sleep(float(5))
    
    #Safe Warning Disable
    disable_all_port_gro_and_tso(net)
    
    #Disable IPV6
    disable_all_node_ipv6(net)
    
    #Enable Spanning Tree on all switches:
    #enable_spanning_tree_on_all_switches(net)
    
    #print "\n Hosts:"
    py_scrpt = os.path.join(scrp_dir, 'host_task_cluster.py')
    host_cmd = 'sudo python -u ' + py_scrpt + ' -b ' + base_dir + ' -s ' + svc_ch_file + ' >/dev/null 2>&1 '
    
    py_scrpt = os.path.join(scrp_dir, 'host_nfv_task_cluster.py')
    nfv_cmd = 'sudo python -u ' + py_scrpt + ' -b ' + base_dir + ' -s ' + svc_ch_file + ' >/dev/null 2>&1 '
    
    num_cnt=0
    for h in net.hosts:
        if num_cnt< num_of_nf_hosts: h.cmdPrint(nfv_cmd,'&')
        else: h.cmdPrint(host_cmd,'&')
        num_cnt+=1
        #h.cmdPrint(host_cmd,'&')
    
    
    #print " wait for 30 seconds before launching the flows!"
    time.sleep(float(20))
    time.sleep(float(20))
    time.sleep(float(20))
    
    #Start Monitor and Flows
    #launch_flow_count_monitor(net)
    launch_udp_flows(net)
    
    #if test_file:
    #    perform_test(net)
    CLI( net )
    #Altenate Logic: check for rule count to become zero and then exit
    stop_flow_count_monitor(net)
    #Process all results
    process_all_results(net)
    #Copy Result files to controlelr node
    #save_all_results(net)
    #do_cleanup
    #cleanup_bkgd_procs(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' ) # Other Options 'debug', 'info', 'output','warning','error','critical'
    parse_args()
    demo()
