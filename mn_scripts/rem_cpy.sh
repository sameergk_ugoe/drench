scp -r mininet@c1:/home/mininet/drench/results/* /home/mininet/drench/results/
scp -r mininet@c2:/home/mininet/drench/results/* /home/mininet/drench/results/
scp -r mininet@c3:/home/mininet/drench/results/* /home/mininet/drench/results/
scp -r mininet@c4:/home/mininet/drench/results/* /home/mininet/drench/results/

ssh c1 'rm -rf /home/mininet/drench/results/*'
ssh c2 'rm -rf /home/mininet/drench/results/*'
ssh c3 'rm -rf /home/mininet/drench/results/*'
ssh c4 'rm -rf /home/mininet/drench/results/*'
