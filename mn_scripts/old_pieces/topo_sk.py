# Copyright 2011 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Fires up topology, discovery_sk, and a l2_multi_sk switch controller
"""

def launch (**kw):
  import pox.samples.pretty_log
  pox.samples.pretty_log.launch(**kw)
  import pox.openflow.discovery_sk
  pox.openflow.discovery_sk.launch()
  import pox.forwarding.l2_multi_sk
  pox.forwarding.l2_multi_sk.launch()
  #import pox.topology
  #pox.topology.launch()
  #import pox.openflow.topology
  #pox.openflow.topology.launch()
