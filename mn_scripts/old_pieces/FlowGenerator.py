#!/usr/bin/python

#
# Program to generate cli command list
#

#from mininet.log import setLogLevel
from random import randint
import sys
import time
import random

#aChain = ['1011', '1101', '1111']
#aChain = ['1011', '1010', '1101', '1110','1111']
aChain = ['0110', '0111', '1000', '1001', '1010', '1011', '1100', '1101', '1110','1111']
MAX_HOST = 30
FLOW = 10
PING = 10
INTERVAL = .01  # @10ms => 100 packets per second => 100*100*8 = 80Kbps~100Kbps
def returnZipfDistribution(zipfExponent,numberOfServices):
  probabilityDistribution  = [0.0]*numberOfServices
  cdfOfDistribution        = 0.0
  for serviceIndex in xrange(numberOfServices):
    nominator  = 1/(pow(float(serviceIndex+1),zipfExponent))
    probabilityDistribution[serviceIndex]  = nominator
    cdfOfDistribution  += nominator
  for serviceIndex in xrange(numberOfServices):
    probabilityDistribution[serviceIndex]/=cdfOfDistribution
  #probability distribution
  #print 'Zipf distribution'
  #print probabilityDistribution
  return probabilityDistribution
def serviceChainChoice(serviceChainLength,probabilityDistribution):
  probability  = {}
  for index in xrange(len(probabilityDistribution)):
    probability[index]  = probabilityDistribution[index]
  serviceChain  = [None]*serviceChainLength
  for serviceIndex in xrange(serviceChainLength):
    randomNumber  = random.random()
    #print '---------'
    #print 'service choice iteration '+str(serviceIndex)
    #print 'random number: '+str(randomNumber)
    #print 'updated probability space'
    #print probability
    serviceCDF     =  0.0
    serviceChosen  = -1
    for element in probability:
      if serviceCDF>randomNumber:
        break
      serviceCDF    += probability[element]
      serviceChosen  = element
    serviceChain[serviceIndex]  = serviceChosen
    #print 'chosen service: '+str(serviceChosen)
    updateProbabilitySpace(probability,serviceChosen)
  print 'Service chain selected'
  print serviceChain
  return serviceChain
def updateProbabilitySpace(probability,serviceChosen):
  del probability[serviceChosen]
  sumOfProbabilitiesOfNewSpace  = 0.0
  for element in probability:
    sumOfProbabilitiesOfNewSpace  += probability[element]
  #estimate new probabilities
  for element in probability:
    probability[element]/=sumOfProbabilitiesOfNewSpace

#select number of flows
def getNumFlows():
  try:
    print "No of flows: %s"  %str(sys.argv[1])
    nFlow = int(sys.argv[1])
    return nFlow
  except:
    print "Please enter valid number of flows"

#select number of pings per flow
def getPingLen():
  try:
    print "No of packets per flow: %s"  %str(sys.argv[2])
    pings = int(sys.argv[2])
    return pings
  except:
    print "Please enter valid number of packet per flow"

#select number of pings per flow
def getPingInterval():
  try:
    print "packet_rate: %s"  %str(sys.argv[3])
    interval = float(sys.argv[3])
    return interval
  except:
    print "Please enter valid number of packet per flow"

#select flow schedule rate
def getFlowInterval():
  try:
    print "flow_schedule_rate: %s"  %str(sys.argv[4])
    interval = float(sys.argv[4])
    return interval
  except:
    print "Please enter valid number of flow_schedule_Rate"
    
    
#select service chain
def getServiceChain(min_len=1, max_len=3, fixed_len=3):
  numberOfServices         = len(aChain)
  serviceChainLength       =  fixed_len
  zipfExponent             =  0.5
  #zipf distribution
  probabilityDistribution  = returnZipfDistribution(zipfExponent,numberOfServices)
  sc_index = serviceChainChoice(serviceChainLength,probabilityDistribution)
  sc = [aChain[x] for x in sc_index]
  return sc
  
  lenth = randint(1,len(aChain)) #3
  sc = aChain[0:]
  sch = 0
  random.shuffle(sc)
  #sc = random.sample(aChain,lenth)
  #return aChain
  
  #sc.append(',servicelist')
  
  #for x in range(lenth):
  #  sc.append(aChain[randint(0,len(aChain)-1)])
  #print "service chain: %s" %sc
  
  if not sc:
    sc.append('1101')
  #return sc[0:randint(1,max_len)]
  #return sc[0:min(max_len,len(sc))]
  sc[0] = '1101'
  return sc[0:1]


#select source and destination
def selectHosts(nFlow=10, nPing=100,nInterval=.01, nFlowInterval=.1):
  #num = randint(1, len(network.hosts))
  nElastic = randint(0,1)
  #nInterval2 = min(0.5, nInterval*10)
  #nInterval2_max = 1 #max(2,nInterval*nPing/float(nFlow))
  #nInterval2_min = .01 #min(0.1, nInterval*10)
  #nInterval_step = 10*(nInterval2_max - nInterval2_min)/float(nFlow)
  #nInterval2 = nInterval2_max
  #print nInterval2_max, nInterval2_min, nInterval_step, nInterval2
  nInterval2 = 5*nFlowInterval
  for x in range (nFlow):
    src=randint(1,MAX_HOST)
    dst=''
    dstn=0
    if x and x%5 == 0:
      nInterval2 = 5*nFlowInterval
    else:
      nInterval2 = .05
    #if x and x%10 == 0:
    #  nInterval2 = abs(min(nInterval2-nInterval_step, nInterval2_max))
    sChain=getServiceChain()
    sc=','.join(map(str, sChain))
    while (dstn==src or dstn==0):
      dstn=randint(1,MAX_HOST)
    
    print "src: h%s" %str(src)
    print "dst: %s" %str(dstn)
    print "schain: %s" %sc
    command = "py net.get('h"+str(src)+"').cmd('sudo python gn_sniff_sk.py "+ str(sc) + " " + str(src) + " " + str(dstn) + " " + str(nPing) + " " + str(nFlow) + " " + str(nInterval) + " " + str(nElastic) + " -q &')"
    print command
    #with open('/home/mininet/flowlist.py', 'ab') as flist:
    fname = 'n4_' + str(nFlow) + 'f_' + str(nPing)+ 'p_' + str(nInterval*1000) +'Pms_' + str(nFlowInterval*1000) + 'Fms.py'
    #with open('flowlist.py', 'ab') as flist:
    with open(fname, 'ab') as flist:
      if x == 0:
        flist.write("sh ./launch_flow_count_capture.sh &" + "\n")
      flist.write(command+'\n')
      #flist.write("py time.sleep(nInterval)\n")
      flist.write("py time.sleep(" + str(nInterval2) + ")" + "\n")


def initiate():
  global FLOW
  global PING
  global INTERVAL
  FLOW = getNumFlows()
  PING = getPingLen()
  INTERVAL =getPingInterval()
  FLOWINTERVAL = getFlowInterval()
  selectHosts(FLOW, PING, INTERVAL, FLOWINTERVAL)
    
if __name__ == '__main__':
  #setLogLevel( 'info' )
  initiate()