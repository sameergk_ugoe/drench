#!/usr/bin/python

import subprocess
import commands
from scapy.all import sendp, sniff, IP, ICMP, Dot1Q, Ether, Raw
import csv
import netifaces
import time
import sys
ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
payload = ",svclist,"
svc_list = []
host_interface = "Hx-eth0"
host_mac_addr  = "00:00:00:00:00:00"
host_ip_addr   = "10.0.0.0"
svc_rx_file = '/home/mininet/my_code/svc_rx.csv'
rx_file = '/home/mininet/my_code/rx.csv'
pkt_counter = 0
def getID(list):
  #print " List:", list
  id = filter(lambda x: 'id' in x, list)
  #print " ID:", id
  if id and len(id) > 0:
    list = id[0].split('id ')
    return int(list[1])

def getVLAN(list):
  try:
    vlan = filter(lambda x: 'vlan' in x, list)
    list = vlan[0].split('vlan ')
    print "VLAN ID: %d" %int(list[1])
    return int(list[1])
  except:
    return 0
  
def modVLAN(ovlan):
  nvlan = ovlan >> 4
  if nvlan == 0:
    return 0
  else:
    return nvlan
  
def getSEQ(list):
  id = filter(lambda x: 'seq' in x, list)
  list = id[0].split('seq ')
  return int(list[1])

def getSrc(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[0].split(' ')
  #print "SRC MAC: %s" %str(mac[1])
  return str(mac[1])
  
def getDst(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[1].split(' ')
  #print "DST MAC: %s" %str(mac[0])
  return str(mac[0])
  
def getDestIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[1].split(':')
  #print "DST IP: %s" %str(ip[0])
  return str(ip[0])
  
def getSrcIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[0].split(':')
  #print "SRC IP: %s" %str(ip[0])
  return str(ip[0])

def getSVCList(list):
  #print " Payload: %s", list
  id = filter(lambda x: 'svclist' in x, list)
  #print " LIST ID: %s", id
  svc_list = id[0].split('svclist=')
  #print " SVC_LIST: %s", svc_list
  return svc_list[1]
  
def getNextVlan(vlan = 0,svc_list=0):
  if vlan == 0 or svc_list == 0:
    return 0
  #print "CUR_VLAN= %d, Total VLAN=%d" %(vlan,svc_list)
  mask = 0x0F
  cur_vlan = svc_list & mask
  while cur_vlan != vlan:
    mask <<= 4
    cur_vlan = svc_list & mask
  return ((svc_list & mask <<4)>>4)

def getSVCList2(list):
    global svc_list
    svc_list = []
    #print " Payload: %s" %str(list)
    id = filter(lambda x: 'svclist' in x, list)
    #print " getSVCList2 LIST ID: %s", id
    slst = id[0].split('svclist,')
    #print "slst:",slst
    #print " SLIST[1]:", slst[1]
    #print "After: SLIST[1]:", slst[1]
    if(slst[1] and len(slst[1])>2):
        svc_list = slst[1].split(',')
        svc_list = [x.rsplit()[0] for x in svc_list]
    else:
        svc_list = []

def getNextVlan2():
    global svc_list
    sv = 0
    if not svc_list or len(svc_list) == 0:
        sv = 0
    else:
        sv = int(svc_list.pop(0),2)
    return sv

def write_pkt(vlan=0, id=1, seq=1):
  svc_rx_file_func = svc_rx_file.split('.')[0] + '_' + str(vlan) +'.'+svc_rx_file.split('.')[1]
  with open(svc_rx_file_func, 'ab') as csvfile:
        rxWriter = csv.writer(csvfile,delimiter=',')
        timest = time.time()
        rxWriter.writerows([[str(timest), str(id), str(seq), str(vlan), str(host_ip_addr), str(host_interface)]])
  return

def createPkt3(nvlan=0, nid=1, nseq=1, ipsrc=ipsrc, ipdst=ipdst, ndst=edst, nsrc=esrc, npayload=payload):
  if nvlan>0:
    #sendp(Ether(src=nsrc,dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
    sendp(Ether(dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
  else:
    #sendp(Ether(src=nsrc,dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
    sendp(Ether(dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
def parseDump(op):
  list = op[0]
  list = list.split(', ')
  #print "List is:", list
  vlan = getVLAN(list)
  if vlan>0:
    id = getID(list)
    seq = getSEQ(list)
    ovlan= modVLAN(vlan)
    dst = getDst(list)
    src = getSrc(list)
    dstip = getDestIP(list)
    srcip = getSrcIP(list)
    svc_list = getSVCList(op)
    vid = int(svc_list)
    vid = vid >> 4
    ovlan2 = vid & 0x0F
    data = payload+str(vid)
    #print "current_vlan= %d, next_vlan= %d, data=%s" %(vlan,ovlan2,data)
    #createPkt(ovlan, id, seq)
    #createPkt2(nvlan=ovlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=src)
    createPkt3(nvlan=ovlan2, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=host_mac_addr,npayload=data)
  else:
    #print str(list)
    id = getID(list)
    seq = getSEQ(list)
    timest = time.time()
    ip = getDestIP(list)
    # Write only if this host is destination
    if ip == host_ip_addr:
      with open(rx_file, 'ab') as csvfile:
        rxWriter = csv.writer(csvfile,delimiter=',')
        rxWriter.writerows([[str(id), str(seq), str(timest), str(host_interface)]])
    return

def parseDump2(op):
  list = op[0]
  list = list.split(', ')
  #print "op is:", op
  #print "List is:", list
  vlan = getVLAN(list)
  if vlan>0:
    #print "VLAN Pkt#[%d]" %(pkt_counter)
    
    id = getID(list)
    seq = getSEQ(list)
    ovlan= modVLAN(vlan)
    dst = getDst(list)
    src = getSrc(list)
    dstip = getDestIP(list)
    srcip = getSrcIP(list)
    
    global svc_list
    getSVCList2(op)
    ovlan2 = getNextVlan2()
    new_data = ""
    if(svc_list and len(svc_list) != 0):
        new_data = ','.join(svc_list)
    #print "new data:", new_data
    data = ",svclist,"+new_data
    #print "data:", data
    #print "current_vlan= %d, next_vlan= %d, data=%s" %(vlan,ovlan2,data)
    #print "current_vlan= %d, next_vlan= %d, data=%s" %(vlan,ovlan2,data)
    createPkt3(nvlan=ovlan2, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=host_mac_addr,npayload=data)
    write_pkt(vlan,id,seq)
  else:
    #print str(list)
    global pkt_counter
    #print "No VLAN for the Pkt#[%d]" %(pkt_counter)
    pkt_counter +=1
    id = getID(list)
    seq = getSEQ(list)
    timest = time.time()
    ip = getDestIP(list)
    # Write only if this host is destination
    if ip == host_ip_addr:
      with open(rx_file, 'ab') as csvfile:
        rxWriter = csv.writer(csvfile,delimiter=',')
        rxWriter.writerows([[str(timest), str(id), str(seq), str(ip), str(host_interface)]])
    return

def getPacket():
  # Get interface name
  interface_list = netifaces.interfaces()
  interface = filter(lambda x: 'eth0' in x,interface_list)
  if len(interface) == 0:
    print "No interface found"
    return
  
  global host_interface
  global host_mac_addr
  global host_ip_addr
  
  addrs      = netifaces.ifaddresses(interface[0])
  link_addrs = addrs[netifaces.AF_LINK]
  net_addrs  = addrs[netifaces.AF_INET]
  host_interface  = interface[0]
  host_mac_addr   = link_addrs[0]['addr']
  host_ip_addr    = net_addrs[0]['addr']

  print "host_interface:", host_interface, "host_mac_addr:", host_mac_addr, "host_ip_addr:", host_ip_addr
  
  # Create tcpdump command
  #comTcpDump = "sudo tcpdump icmp[icmptype] == 8 -e -l -i " + interface[0]
  #comTcpDump = "sudo tcpdump icmp[icmptype] == 8 -A -e -l -i " + interface[0]
  #comTcpDump = "sudo tcpdump icmp[icmptype] == 8 -A -nnvvXSs 1514 -i " + interface[0]
  #comTcpDump = "sudo tcpdump -l -i " + interface[0] + " -Uw - | tcpdump -l  -en -r - vlan"
  comTcpDump = "sudo tcpdump icmp[icmptype] == 8 -A -e -Ul -i " + interface[0]
  # Create subprocess to execute command
  p = subprocess.Popen(comTcpDump, stdout=subprocess.PIPE, shell=True)
  lines = iter(p.stdout.readline, b'')
  packet = []
  for line in lines:
    if line == '\n':
      print "Caught New Line and Skipped!"
      continue
    packet.append(line)
    if(',svclist,' in line):
      #print "Parse Packet:", packet
      #parseDump(packet)
      parseDump2(packet)
      packet = []
  #parseDump(lines)
  #sys.stdout.flush()
  
if __name__ == '__main__':
    getPacket()