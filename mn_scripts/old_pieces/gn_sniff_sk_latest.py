#!/usr/bin/python

#
# Program to generate packets with vlan
#
import subprocess
import commands
import logging
from scapy.all import sendp, sniff, ICMP, Dot1Q, Ether, Raw, IP, srp1, TCP
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
#from scapy.layers.inet import IP
import csv
import netifaces
import time
import sys
import threading
import thread
import sys
from random import randint, normalvariate, gauss
from threading import Thread
from os.path import expanduser
host_interface = "hx-eth0"
host_mac_addr = "00:00:00:00:00:00"
host_ip_addr = "00.00.00.00"

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
payload = ",svclist,"
nvlan = 0
#nvlan = 251 #[1111 1011] (BC)
#nvlan = 4029 #[1111 1011 1101] (BCA)
#nvlan = 3069 #[1011 1111 1101] (CBA)
#nvlan = 3579 #[1101 1111 1011] (ABC)
#nvlan = 13 #(A)
#nvlan = 253 #[1111 1101] (BA)

sChain = []
nid = 1
nseq = 1
interval = 0.5
count = 10
flow_id = 0
path_cost = 0
done = False
nElastic = 0
flow_rate = 0
hdir_p = expanduser('~')
tx_file = hdir_p + '/my_code/tx.csv' #'/home/mininet/my_code/tx.csv'
svc_tx_file = hdir_p + '/my_code/svc_tx.csv' # '/home/mininet/my_code/svc_tx.csv'
svc_ts_file = hdir_p + '/my_code/svc_ts.csv' #'/home/mininet/my_code/svc_ts.csv'
tx_flows_file = hdir_p + '/my_code/tx_flows.csv' #'/home/mininet/my_code/tx.csv'

def writeFlowData(id=nid, seq=nseq, idst=ipdst, dst=edst, flow=flow_id, flow_rate=0, rtt=0,schain=[]):
    with open(tx_flows_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), id, seq, flow_id, ipsrc,str(schain),idst,flow_rate,rtt]])
        
def writeData(id=nid, seq=nseq, idst=ipdst, dst=edst, flow=flow_id, flow_rate=0, rtt=0):
    with open(tx_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), id, seq, flow_id, flow_rate,rtt, ipsrc,idst]])
        #txWriter.writerows([[str(time.time()), id, seq, flow_id, ipsrc,esrc, idst,edst]])

def write_pkt(vlan=0, id=1, seq=1):
  svc_tx_file_func = svc_tx_file.split('.')[0] + '_' + str(vlan) +'.'+svc_tx_file.split('.')[1]
  with open(svc_tx_file_func, 'ab') as csvfile2:
        tx2Writer = csv.writer(csvfile2,delimiter=',')
        tx2Writer.writerows([[str(time.time()), str(id), str(seq), str(vlan), str(host_ip_addr), str(host_interface)]])
  return

def log_latency_data(id=0, seq=0,cost=0,ts_list=[]):
  try:
    for i, x in enumerate(ts_list):
      data = x.split(':')
      vlid,ipid = data[0].split('-')[0], data[0].split('-')[1]
      #vlan-IP = data[0], TS = data[1]
      svc_ts_file_func = svc_ts_file.split('.')[0] + '_' + str(data[0]) + '.' + svc_ts_file.split('.')[1]
      with open(svc_ts_file_func, 'ab') as csvfile3:
          tx3Writer = csv.writer(csvfile3,delimiter=',')
          #tx3Writer.writerows([[ str(id), str(seq), str(cost), str(data[0]), str(data[1]), str(host_ip_addr)]])
          tx3Writer.writerows([[ str(id), str(seq), str(cost), str(vlid), str(ipid), str(data[1]) ]])
  except Exception,e:
    print "Exception logging ping response packet!!", str(e)
  return

def getFunction():

    interface_list = netifaces.interfaces()
    interface = filter(lambda x: 'eth0' in x,interface_list)
    if len(interface) == 0:
        print "No interface found"
        return
  
    global ipsrc
    global esrc
    global host_interface
    global host_mac_addr
    global host_ip_addr
    global flow_rate
    addrs      = netifaces.ifaddresses(interface[0])
    link_addrs = addrs[netifaces.AF_LINK]
    net_addrs  = addrs[netifaces.AF_INET]
    host_interface  = interface[0]
    host_mac_addr   = link_addrs[0]['addr']
    host_ip_addr    = net_addrs[0]['addr']
    esrc = host_mac_addr
    ipsrc = host_ip_addr
    
    global nvlan
    global sChain
    global nid
    global nseq
    global interval
    global flow_id
    global count
    global ipdst
    global edst
    global nElastic
    nid = randint(0,99999)
    nseq = 1
    src_id = 0
    dst_id = 0
    try:
        #parse arguments <svc_chain, esrc, edst, count, flow_id, ping_interval, elastic_or_cbr>
        #i = 7
        nElastic = 0
        #if(len(sys.argv)>i) and sys.argv[i].isdigit() is True:
        #    nElastic = int(sys.argv[i])
        #i -=1
        i = 6
        if(len(sys.argv)>i):
            ival = float(sys.argv[i])
            if ival > 0:
              interval = ival
        i -=1
        
        if(len(sys.argv)>i):
            flow_id = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            count = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            dst_id = int(sys.argv[i])
            ipdst = "10.0.0."+ sys.argv[i]
            edst  = "00:00:00:00:00:" + "{:0>2d}".format(dst_id)
            #if dst_id < 10:
            #    edst = "00:00:00:00:00:0"+sys.argv[i]
            #else:
            #    edst = "00:00:00:00:00:"+sys.argv[i]
        i -=1
        
        if(len(sys.argv)>i):
            src_id = int(sys.argv[i])
            #print src
        i-=1
        
        if(len(sys.argv)>i):
            sChain = sys.argv[1].split(",")
            sChain = [x for x in sChain if x.isdigit() is True]
            #nvlan = int(sChain.pop(0),2)
            nvlan = int(sChain[0],2)
            print "sChain:", sChain, "vlan: %d" %nvlan 
            #print "vlan: %s" %str(nvlan)
        print "Generator: %s to_Dst:%s(%d), Service Chain:%s, First Service: %d , num_packets:%d and Flow_ID:%d, ping_interval:%f, Elastic:%d " %(host_ip_addr,edst,dst_id,sChain,nvlan,count,flow_id,interval, nElastic)
        flow_rate = 1/float(interval) #packets per second
    except Exception,e:
        print "Exception parsing input args!!", str(e)
        exit()

MAX_WAIT_TIME_FOR_RESP = 0.5
def sendPings(count=0):
  global nseq
  global nid
  global flow_id
  global payload
  global flow_rate
  global MAX_WAIT_TIME_FOR_RESP
  fr = flow_rate
  ret = 0
  rtt_ms = 25
  rtt_s = 0
  if not sChain:
    d=',end,'
    payload = ",svclist," + d
    #print "payload without Schain:", payload
  else:
    d= ','.join(map(str, sChain)) +',end,' + '{0:03d}'.format(0) + ','
    payload = ",svclist," + d 
  # Append the Origin Details <SVC_ID-IP:SP:Time>,
  payload += '{0:03d}'.format(0) + '-' + host_ip_addr + ":" + '{0:03d}'.format(0) + ":"+ str(time.time()) +','
  
  try:
    #FOR TCP #payload2 = ',' + str(nid) + ',' + str(nseq) + payload
    #FOR TCP #payload = payload2.lstrip(' ').rstrip(' ')
    #FOR TCP #print "Pkt Payload:", payload
    #FOR TCP #pkt = Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/TCP(dport=7)/payload
    pkt = Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload
    pkt2 = Ether(src=esrc, dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)
    sendp(pkt, verbose=0)
    #try with response for some packets:
    if count == 0 or count%50 == 0:
      stime = time.time()
      #resp = srp1(pkt2,timeout=MAX_WAIT_TIME_FOR_RESP,verbose=0)
      etime = time.time()
      rtt_s = int(etime-stime)
      rtt_ms = (etime - stime - rtt_s)*1000
      if rtt_s:
        ret = 0 #rtt_s
        if rtt_s >= MAX_WAIT_TIME_FOR_RESP:
          fr = 1/float(MAX_WAIT_TIME_FOR_RESP)
          MAX_WAIT_TIME_FOR_RESP +=1
      if rtt_ms < 100:
        ret = 0 #-int(rtt_ms)
        MAX_WAIT_TIME_FOR_RESP = 1
      #print " count=", count,"rtt_s:", rtt_s, "rtt_ms", rtt_ms 
    #working version
    #pkt = Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload
    #sendp(pkt, verbose=0)
    
    #new Trial version
    #pkt = Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload
    #srp1(pkt,verbose=1),#srp1(pkt)
    #srp1(pkt,timeout=interval,verbose=0)
    
    #new Trial version2
    #pkt = Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/TCP(dport=7)/ICMP(id=nid, seq=nseq)/payload
    #srp1(pkt,verbose=1),#srp1(pkt)
    #srp1(pkt,timeout=interval,verbose=0)
    #Ether(src="00:00:00:00:00:05", dst="00:00:00:00:00:06")/Dot1Q(vlan=13)/IP(src="10.0.0.5", dst="10.0.0.6")/TCP(dport=7)/ICMP(id=1001, seq=1)/",svclist,13,15,11,end,0,MY TCP PACKET AT PORT 7"
    
    #old version
    #sendp(Ether(dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload, verbose=0)
    #pkt = Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload
    #sendp(pkt,verbose=0,)
  except Exception,e:
    print "Failed to Send Packets with Exception:", str(e)
    #exit()
    return -1
  writeData(id=str(nid), seq=str(nseq), idst=str(ipdst),dst=str(edst),flow=flow_id,flow_rate=fr,rtt=rtt_s*1000+rtt_ms )
  #if nvlan != 0:
    #write_pkt(vlan=nvlan, id=nid, seq=nseq)
    #pass
  if nseq == 1:
    writeFlowData(id=str(nid), seq=str(nseq), idst=str(ipdst),dst=str(edst),flow=flow_id,flow_rate=fr,rtt=rtt_s*1000+rtt_ms, schain=sChain)
  nseq = nseq+1
  return ret
AVG_TOPOLOGY_LATENCY = .5 #500ms (6.6*87 =575ms)
def startPings():
  global count
  MAX_RETRY_COUNT = 0
  print "START: At[%s], StartPings[%d]" %(time.time(),count)
  ret = 0
  retry_count = 0
  while retry_count <= MAX_RETRY_COUNT:
    for x in range(count):
      new_interval = interval
      try:
        ret = sendPings(count=x)
        if ret == -1:
          print "Aborting!!"
          raise Exception('sendPing() Failed!!')
          return
        if ret > 0:
          new_interval = ret
        if nElastic:
          #new_interval = abs(normalvariate(interval, interval))
          #new_interval = abs(gauss(interval, interval))
          delay_ms = randint(min(interval*1000,AVG_TOPOLOGY_LATENCY*1000), max(interval*1000,AVG_TOPOLOGY_LATENCY*1000))
          new_interval = delay_ms/float(1000)
        time.sleep(new_interval)
      except Exception,e:
        print " SendPings() Exception:",str(e)
        break
    retry_count +=1
    if ret == 0:
      break
    else:
      time.sleep(max(3,interval*5))
      print "Retry attempt [%d]" %retry_count
  print "End At[%s], StartPings[%d]" %(time.time(),count)
  return 

def isIncoming(pkt):
  try:
    if pkt[IP].src == host_ip_addr:
      return False 
    if pkt.haslayer(Dot1Q):
      return False
    if pkt.haslayer(ICMP) is False:
      return False
    if pkt[ICMP].payload is None:
      return False
    
    #print "Packet is:", pkt.show()
    #print a,pkt[ICMP].type
    if pkt[IP].dst==host_ip_addr and pkt[ICMP].type==0:
      if pkt[ICMP].payload and ',' == str(pkt[ICMP].payload)[-1]:
        return True
  except:
    pass
  return False

def PacketHandler(pkt):
  global path_cost
  payload = str(pkt[ICMP].payload)
  
  try:
    payload.rstrip()
    id=pkt[ICMP].id
    seq=pkt[ICMP].seq
    of = payload.find(',end,')
    if of < 0:
      raise Exception('Incorrect Payload', ',end, not found!!')
    ln = len(',end,')
    cost = payload[of+ln:payload.find(',', of+ln)]
    if cost is None or len(cost) == 0 or False == cost.isdigit():
      raise Exception('Incorrect Payload', 'cost is not a number!!')
      pass
    else:
      path_cost = int(cost)
      ln += len(cost) + len(',')
      ts_list = payload[of+ln:].split(',')
      ts_list=[x for x in ts_list if ':' in x] 
      log_latency_data(id=id, seq=seq, cost=path_cost, ts_list=ts_list )
  except Exception, e:
    print "Parse for Payload data Failed to find the Path cost!!", str(e)
    #time.sleep(10)
  return

def startSniffer(name=None,**args):
  #print " In Sniffer: for ", host_interface
  global done
  while not done:
    #time.sleep(5)
    comTcpDump = sniff(iface=host_interface, lfilter = isIncoming, prn = PacketHandler, store=0,count =1,timeout=max(1, interval*5))
  print "Exiting sniffer thread!!"
  
def startSnifferThread_and_Pings():
  global done
  try:
    #print "Launching Thread"
    #thread.start_new_thread( startSniffer, ("SNIFF", 10)) #OLD MODE
    #New Approach
    #t = Thread(group=None,target=startSniffer,name="SNIFF", args=(), kwargs={})
    #t.start()
    startPings()
    #time.sleep(max(5, interval*10))
    done = True
    #time.sleep(max(10, interval*10))
    #t.join()
  except Exception, e:
    print str(e)
    return

if __name__ == '__main__':
    getFunction()
    startSnifferThread_and_Pings()
    #startPings()
    #time.sleep(max(10, interval*10))
