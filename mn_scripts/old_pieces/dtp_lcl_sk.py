#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.topolib import TreeTopo
from mininet.node import RemoteController
from mininet.log import setLogLevel
from mininet.cli import CLI
from mininet.link import TCLink
import time

def demo():
  "Simple Demo for FCSC"
  
  #net = Mininet(controller=RemoteController, link=TCLink, autoSetMacs = True, autoStaticArp = True, cleanup = True, xterms = True)
  net = Mininet(controller=RemoteController, link=TCLink, autoSetMacs = True, autoStaticArp = True, cleanup = True)
  
  #c1 = net.addController(name = 'c1', controller = RemoteController, ip = '172.23.0.33', port = 6633 )
  c1 = net.addController(name = 'c1', controller = RemoteController, port = 6633 )
  
  # Add switches
  s1 = net.addSwitch('s1')
  s2 = net.addSwitch('s2')
  s3 = net.addSwitch('s3')
  s4 = net.addSwitch('s4')
  s5 = net.addSwitch('s5')
  
  # Add nodes
  h1 = net.addHost ('h1')#, ip='10.0.0.1')
  h2 = net.addHost ('h2')#, ip='10.0.0.2')
  h3 = net.addHost ('h3')#, ip='10.0.0.3')
  h4 = net.addHost ('h4')#, ip='10.0.0.4')
  h5 = net.addHost ('h5')#, ip='10.0.0.5')
  h6 = net.addHost ('h6')#, ip='10.0.0.6')
  
  # Add links
  linkopts = {'delay':'6ms', 'use_htb': True }
  #Siwtch to Host
  net.addLink(s1, h1)
  net.addLink(s2, h2)
  net.addLink(s3, h3)
  net.addLink(s4, h4)
  net.addLink(s5, h5)
  net.addLink(s1, h6)
  #switch to Switch
  net.addLink(s1, s2, **linkopts)
  net.addLink(s1, s5, **linkopts)
  net.addLink(s2, s4, **linkopts)
  net.addLink(s4, s3, **linkopts)
  net.addLink(s4, s5, **linkopts)
  
  
  # Start network
  net.build()
  c1.start()
  
  #for s in net.switches:
  #  s.start([c1])

  net.start()
  #net.pingAll()
  for h in net.hosts:
    #h.pexec(['xterm', 'python', '-u', 'hostprocess_sk.py'])
    h.cmdPrint('sudo python -u hostprocess_sk.py &')
    #h.sendCmd('sudo python -u hostprocess_sk.py &')
    pass
  CLI( net )
  net.stop()

if __name__ == '__main__':
    #setLogLevel( 'warning' )
    setLogLevel( 'info' )
    #setLogLevel( 'debug' )
    demo()
