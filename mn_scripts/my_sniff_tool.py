import socket, struct, os, array
from scapy.all import ETH_P_ALL, ETH_P_IP
from scapy.all import select
from scapy.all import MTU
from scapy.all import Ether, ICMP, UDP
import sys
UDP_PORT_LOW=10000
from struct import *
###
#Using TCPReplay tools
### a) tcpdump -i eth0 -s0 -w /tmp/dump -U
### b) tcprewrite --portmap=80:8080 --infile=/tmp/dump.cap --outfile=/tmp/dump.rewrite
### c) tcpreplay --intf1=eth1 /tmp/dump.rewrite
### abc) tcpdump -i eth0 -s0 -w - -U | tcprewrite --portmap=80:8080 --infile=- --outfile=- | tcpreplay --intf1=eth1 -
### *** tcpdump -l -n -i pub dst port 53 and inbound

def macnumstr_to_macaddrformat(bytesmac):
    return ':'.join(s.encode('hex') for s in bytesmac.decode('hex'))
def macaddrformat_to_macnumstr(macaddr):
    return   ''.join(macaddr.split(':'))

def eth_addr (a) :
  b = "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x" % (ord(a[0]) , ord(a[1]) , ord(a[2]), ord(a[3]), ord(a[4]) , ord(a[5]))
  return b
def bytes_to_mac(bytesmac):
    return ":".join("{:02x}".format(x) for x in bytesmac)

def parse_ethernet(frame):
    header_length = 14
    header = frame[:header_length]
    print " Eth Header: %s " %(frame[0:14])
    
    dmac = eth_addr(frame[0:6])
    smac = eth_addr(frame[6:12])
    
    prot = frame[12:14]
    print 'Destination MAC: ' + dmac + ' Source MAC : ' + smac + ' Protocol : ' + str(prot)
    
    smac = dmac
    
    #dst, src, type_code = struct.unpack("!6s6sH", header)
    eth = struct.unpack("!6s6sH", header)
    src = eth[0]
    dst = eth[1]
    type_code = eth[2]
    print "%s, %s %s " %(src, dst, type_code)
    
    if type_code == 0x8100:  # Encountered an 802.1Q tag, compensate.
        print "Encountered 802.1Q Packet!!"
        header_length = 18
        header = frame[:header_length]
        type_code = struct.unpack("!16xH", header)
    payload = frame[header_length:]
    return src, dst, type_code, payload

def parse_ip(packet):
    header_length_in_bytes = 20
    #header_length_in_bytes = (int(packet[0],16) & 0x0F) * 4
    header = packet[:header_length_in_bytes]
    iph = struct.unpack('!BBHHHBBH4s4s' , header)
    
    version_ihl = iph[0]
    version = version_ihl >> 4
    ihl = version_ihl & 0xF
    iph_length = ihl * 4
    
    tos = iph[1]
    ttl = iph[5]
    protocol = iph[6]
    src = socket.inet_ntoa(iph[8]);
    dst = socket.inet_ntoa(iph[9]);
    payload = packet[iph_length:]
    
    return tos,ttl,protocol,src,dst,payload
    
    #header_length_in_bytes = (packet[0] & 0x0F) * 4
    #header = packet[:header_length_in_bytes]
    #payload = packet[header_length_in_bytes:]
    #(total_length, protocol, src, dst) = struct.unpack_from("!2xH5xB2x4s4s", header)
    #src = socket.inet_ntoa(src)
    #dst = socket.inet_ntoa(dst)
    #return header_length_in_bytes, total_length, protocol, src, dst, payload

def parse_udp(packet):
    header_length = 8
    header = packet[:header_length]
    payload = packet[header_length:]
    src_port, dst_port, data_length, checksum = struct.unpack("!HHHH", header)
    return src_port, dst_port, data_length, checksum, payload


def process_raw_pkt(pkt):
    print "Raw Packet Length: %s:%s" %(len(pkt), pkt[0:20])
    eth_src, eth_dst, eth_type, eth_payload = parse_ethernet(pkt)
    if eth_type != 0x0800:  # IP is type 0x0800
        #print("Frame with ethernet type {} received; skipping...".format(eth_type))
        return
    eth_hdr = (eth_src, eth_dst, eth_type)
    print "Ethernet Header Data: (%s:%s:%s)" %(eth_src,eth_dst,eth_type)
    
    (ip_tos, ip_ttl, ip_protocol, ip_src, ip_dst,ip_payload) = parse_ip(eth_payload)
    if ip_protocol != 17:  # UDP is protocol 17
        #print("Packet with protocol nr. {} received; skipping...".format(ip_protocol))
        return
    ip_hdr = (ip_tos, ip_ttl, ip_protocol, ip_src, ip_dst)
    print "IP Header Data: (%s, %s, %s, %s, %s)" %(ip_tos, ip_ttl, ip_protocol, ip_src, ip_dst)
    
    (udp_src_port, udp_dst_port, udp_data_length,udp_checksum, udp_payload) = parse_udp(ip_payload)
    udp_hdr = (udp_src_port, udp_dst_port, udp_data_length, udp_checksum)
    print "UDP Header Data: (%s, %s )" %(udp_src_port, udp_dst_port)

    return
    
def process_scapy_pkt(pkt):
    print len(pkt)
    pkt_e = Ether(pkt)
    if pkt_e.haslayer(ICMP) and pkt_e[ICMP].type == 8:
        #print pkt_e.show()
        return True, pkt_e
    if pkt_e.haslayer(UDP) and (pkt_e[UDP].dport == 8999 or pkt_e[UDP].dport >= UDP_PORT_LOW):
        print pkt_e.show()
        return True, pkt_e
    if pkt_e.haslayer(TCP) and (pkt_e[TCP].dport == 8999 or pkt_e[TCP].dport >= UDP_PORT_LOW):
        print pkt_e.show()
        return True, pkt_e
    return False, pkt_e
    
class IPSniff:
 
    def __init__(self, interface_name, on_ip_incoming, on_ip_outgoing):
 
        self.interface_name = interface_name
        self.on_ip_incoming = on_ip_incoming
        self.on_ip_outgoing = on_ip_outgoing
 
        # The raw in (listen) socket is a L2 raw socket that listens
        # for all packets going through a specific interface.
        self.ins = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_IP))
        self.ins.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 2**30)
        self.ins.bind((self.interface_name, ETH_P_IP))
        
        #self.outs = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_IP))
        #self.outs.setsockopt(socket.SOL_SOCKET, socket.SNDBUF, 2**30)
        #self.outs.bind((self.interface_name, 0))
        ##self.outs.sendto(packet, (self.interface_name,0))
        
    def __process_ipframe(self, pkt_type, pkt):

        #eth_header = struct.unpack("!6s6sH", pkt[0:14])
        #dummy_eth_protocol = socket.ntohs(eth_header[2])
        #if eth_header[2] != 0x800 :
        #    continue
        #ip_header = pkt[14:34]
        #payload = pkt[14:]
        ## Extract the 20 bytes IP header, ignoring the IP options
        #fields = struct.unpack("!BBHHHBBHII", ip_header)
        #dummy_hdrlen = fields[0] & 0xf
        #iplen = fields[2]
        #ip_src = payload[12:16]
        #ip_dst = payload[16:20]
        #ip_frame = payload[0:iplen]

        if pkt_type == socket.PACKET_OUTGOING: return
        #    if self.on_ip_outgoing is not None:
        #        self.on_ip_outgoing(ip_src, ip_dst, ip_frame)
 
        #else:
        #process_scapy_pkt(pkt)
        #process_raw_pkt(pkt)
        if self.on_ip_incoming is not None:
            process_sts,scapy_pkt = process_scapy_pkt(pkt)
            if process_sts is True:
                self.on_ip_incoming(scapy_pkt)
 
    def recv(self):
        while True:
 
            pkt, sa_ll = self.ins.recvfrom(MTU)
 
            #if type == socket.PACKET_OUTGOING and self.on_ip_outgoing is None:
            #    continue
            #elif self.on_ip_outgoing is None:
            #    continue
            
            if len(pkt) <= 0:
                break

            self.__process_ipframe(sa_ll[2], pkt )
            #process_scapy_pkt(pkt)
    
    def sendPkt(raw_packet):
        if self.outs:
            self.outs.send(raw_packet)
        return
    def edit_raw_pkt(raw_packet, ip_ttl, udp_sport):
        return
#Example code to use IPSniff
def test_incoming_callback(scapy_pkt):
    pass
    #print("incoming - src=%s, dst=%s, frame len = %d" %(socket.inet_ntoa(src), socket.inet_ntoa(dst), len(frame)))
    return

def test_outgoing_callback(scapy_pkt):
    pass
    #print("outgoing - src=%s, dst=%s, frame len = %d" %(socket.inet_ntoa(src), socket.inet_ntoa(dst), len(frame)))
    return

ip_sniff_obj = None
def setup_l2sniff(interface_name, callback_func):
    global ip_sniff_obj
    ip_sniff_obj = IPSniff(interface_name, callback_func, None)
    ip_sniff_obj.recv()
    return ip_sniff_obj

if __name__ == '__main__':
    iface = 'h1-eth0'
    if sys.argv[1]: iface = sys.argv[1]
    setup_l2sniff(iface, test_incoming_callback)
    #ip_sniff = IPSniff('eth0', test_incoming_callback, test_outgoing_callback)
    #ip_sniff = IPSniff('eth0', test_incoming_callback, test_outgoing_callback)
    #ip_sniff.recv()
    #return
