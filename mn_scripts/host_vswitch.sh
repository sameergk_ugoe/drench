#Script to create per host ovs-db and open-v-switch\
#Require input: 1. Node name ('h1'), 2. Node-Interface name ('h1-eth0') 3. Node IP ('10.0.0.1')
#To terminate and cleanup: 1. Node name ('h1'), 2. Node-Interface name ('h1-eth0') 3. Node IP ('10.0.0.1') 4. Flag=1
#host_vswitch.sh h1 h1-eth0 10.0.0.1 1
#Globals desired as input args
mynode="h1"
mybridge=br_h1
myiface=$mynode-eth0
myip="10.0.0.1"
mycontroller_info="tcp:127.0.0.1:6633"
cleanup_bridge=0
#Parse and setup input arguments
if [ $# -ge 1 ];
then
  mynode=$1
  mybridge=br_$1
  echo "updated node_name: $mynode"
fi

if [ $# -ge 2 ];
then
  myiface=$2
  echo "Host interface updated to: $myiface"
fi
if [ $# -ge 3 ];
then
  myip=$3
  echo "Host IP updated to: $myip"
fi

if [ $# -ge 4 ];
then
  cleanup_bridge=1
  echo "Host celanup mode:"
fi

#Derived Globals for ovs-setup
ovs_dir=/tmp/mininet_$mynode

#ovs-db conf and schema files
ovs_conf=$ovs_dir/conf.db
ovs_schema=/usr/share/openvswitch/vswitch.ovsschema
if [ ! -f $ovs_schema ]; 
then
  #try local path
  ovs_schema=/usr/local/share/openvswitch/vswitch.ovsschema
fi

#ovs-db server files
ovs_db=$ovs_dir/db.sock
ovs_pids=$ovs_dir/ovsdb_server.pid
ovs_logs=$ovs_dir/ovsdb_server.log

#ovs-vswitchd files
ovsd_pids=$ovs_dir/ovs_vswitchd.pid
ovsd_logs=$ovs_dir/ovs_vswitchd.log

delete_bridge_and_reset() {
    
    #Delete the Port from bridge and setup default routing
    sudo ovs-vsctl --db=unix:$ovs_db del-port $mybridge $myiface
    sudo ifconfig $myiface $myip
    sudo ip route add 10.0.0.0/24 dev $myiface
    
    # Delete the bridge
    sudo ovs-vsctl --db=unix:$ovs_db del-br $mybridge
    
    # Stop the ovsdb-server
    sudo kill ` cat $ovsd_pids $ovs_pids`
    #sudo kill `cd /usr/local/var/run/openvswitch && cat ovsdb-server.pid ovs-vswitchd.pid`
    
    #clean the directory
    sudo rm -rf $ovs_dir
    
    #exit 0
}
if [ $cleanup_bridge -eq 1 ]; 
then
  echo "cleaning up the ovs bridge"
  delete_bridge_and_reset
  exit 0
fi
#For Safety Kill the service before starting new one
delete_bridge_and_reset

create_ovs_dir() {
    sudo rm -rf $ovs_dir
    sudo mkdir -p $ovs_dir
    #check and create ovs-conf files
    if [ -e $ovs_conf ]; then
        echo "OVS-DB exists!!"
    else
        sudo ovsdb-tool create $ovs_conf $ovs_schema
    fi
}
create_ovs_dir

create_ovsdb_server() {
    #Launch ovsdb-server
    sudo ovsdb-server $ovs_conf \
        --remote=punix:$ovs_db \
        --private-key=db:Open_vSwitch,SSL,private_key \
        --certificate=db:Open_vSwitch,SSL,certificate \
        --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert \
        --no-chdir \
        --pidfile=$ovs_pids \
        --log-file=$ovs_logs \
        --detach \
        --monitor
    
    #Intialize the Openvswitch server
    sudo ovs-vsctl --db=unix:$ovs_db --no-wait init

    #start the openvswitch with custom ovs-db provided\
    sudo ovs-vswitchd unix:$ovs_db  --pidfile=$ovsd_pids --log-file=$ovsd_logs --no-chdir --mlockall --detach --monitor
}
create_ovsdb_server

configure_ovsswitch_on_private_db() {
    #Create OVS-brdige and add the interface to the bridge
    sudo ovs-vsctl --db=unix:$ovs_db add-br $mybridge
    sudo ovs-vsctl --db=unix:$ovs_db add-port $mybridge $myiface
    sudo ovs-vsctl --db=unix:$ovs_db set-fail-mode $mybridge secure
    sudo ovs-vsctl --db=unix:$ovs_db set bridge $mybridge protocol=OpenFlow10
    sudo ovs-vsctl --db=unix:$ovs_db set-controller $mybridge $mycontroller_info
    sudo ovs-vsctl --db=unix:$ovs_db show
}
configure_ovsswitch_on_private_db

configure_ovsswitch_on_global_db() {
    #Create OVS-brdige and add the interface to the bridge
    sudo ovs-vsctl add-br $mybridge
    sudo ovs-vsctl add-port $mybridge $myiface
    sudo ovs-vsctl set-fail-mode $mybridge secure
    sudo ovs-vsctl set bridge $mybridge protocol=OpenFlow10
    sudo ovs-vsctl set-controller $mybridge $mycontroller_info
    sudo ovs-vsctl show
}
#configure_ovsswitch_on_global_db

#reset existing interface details and push IP to vSwitch
sudo ifconfig  $myiface 0
sudo ip route del 10.0.0.0/24 dev $myiface
sudo ifconfig $mybridge $myip
sudo ip route add 10.0.0.0/24 dev $mybridge
#sudo systemctl restart network
