#!/usr/bin/gnuplot
reset

#set terminal windows size 800, 600
set terminal png size 2400, 600

script_name=ARG0 
#script_name=$#

#set output 'nfv_utlization.png'
set output script_name.".png"


#set xdata Duration
set xlabel "Time in Seconds"
#set xtics 5
#set xrange [50:100]
#set yrange [0:*]
#set y2range [-1:*]
#set y2tics 1

set xrange auto
set y2range auto
set yrange auto

set autoscale
#set offsets <left>, <right>, <top>, <bottom>
set offsets graph 0.5, 0.5, 0.5, 0.5

set ylabel "Network Utilization in Mbps"
set y2label "Redirections and Path-Stretch Deviations"

set ytics nomirror
set y2tics

set title "Service Instance Re-location Scenario"
set datafile separator ","

set autoscale fix
set key inside top right

set style data lines
set style fill solid

set style data lines
set style fill solid

set palette defined (0 "blue",1 "green", 2 "red")
set pointsize 0.7

set style line 1 lc rgb "black" lt 2 lw 2 pt 1 pi -1 ps 1.5
set style line 2 lc rgb "blue" lt 2 lw 2 pt 6 pi -1 ps 1.5
set style line 3 lc rgb "green" pt 11 pi -1 ps 1.5
set style line 4 lc rgb "pink" pt 16 pi -1 ps 1.5
set style line 5 lc rgb "red" pt 18 pi -1 ps 1.5

set style line 6 lc rgb '#0060ad' lt 2 lw 2 pt 24 pi -1 ps 1.5
set style line 7 lc rgb '#06ad60' lt 2 lw 2 pt 22 pi -1 ps 1.5
set style line 8 lc rgb '#6060ad' lt 2 lw 2 pt 18 pi -1 ps 1.5
set style line 9 lc rgb 'red' lt 2 lw 2 pt 16 pi -1 ps 1.5
set pointintervalbox 3


plot "../results/log_nw_utilization_info_lcl_h1-eth0.csv" using 1:2 w linespoints  title "NF1 Utilization" axes x1y1 ls 9, \
"../results/log_nw_utilization_info_lcl_h2-eth0.csv" using 1:2 w linespoints  title "NF2 Utilization" axes x1y1 ls 8, \
"../results/log_nw_utilization_info_lcl_h3-eth0.csv" using 1:2 w linespoints  title "NF3 Utilization" axes x1y1 ls 7, \
"../results/log_nw_utilization_info_lcl_h4-eth0.csv" using 1:2 w linespoints  title "NF4 Utilization" axes x1y1 ls 6, \
"../results/log_nw_utilization_info_lcl_h5-eth0.csv" using 1:2 w linespoints  title "NF5 Utilization" axes x1y1 ls 5, \
"../results/log_nw_utilization_info_lcl_h6-eth0.csv" using 1:2 w linespoints  title "NF6 Utilization" axes x1y1 ls 4, \

#"../results/related_flows_info.csv" using 1:($4/100) with linespoints title "Path Deviation" axes x1y2 ls 6 , \
#"../results/rdir_log.csv" using 1:($2 == 0 ? 1/0 : $2) w lp  title "Redirection Count" axes x1y2 ls 8, \


set terminal x11
set output
replot

#set ytics ('s1-1' 1.1, 's1-2' 1.2, 's2-1' 2.1, 's2-2' 2.2, 's3-1' 3.1, 's3-2' 3.2)
#YTICS = "set ytics ('s1-1' 1.1, 's1-2' 1.2, 's2-1' 2.1, 's2-2' 2.2, 's3-1' 3.1, 's3-2' 3.2);
#         set ylabel Switch ID with Active NFInstances"


