test_dir="80f"
if [ $# -ge 1 ];
then
  test_dir=$1
  echo "updated base_dir: $base_dir"
fi

rm -rf /home/mininet/drench/results/*
ssh c1 'rm -rf /home/mininet/drench/results/*'
ssh c2 'rm -rf /home/mininet/drench/results/*'
ssh c3 'rm -rf /home/mininet/drench/results/*'
ssh c4 'rm -rf /home/mininet/drench/results/*'

ssh c1 'rm -rf /home/mininet/drench/testcases/*.txt'
ssh c2 'rm -rf /home/mininet/drench/testcases/*.txt'
ssh c3 'rm -rf /home/mininet/drench/testcases/*.txt'
ssh c4 'rm -rf /home/mininet/drench/testcases/*.txt'

scp -r /home/mininet/drench/testcases/fattree_topo/master/mixed/TCP/$test_dir/* mininet@c1:/home/mininet/drench/testcases/ 
scp -r /home/mininet/drench/testcases/fattree_topo/master/mixed/TCP/$test_dir/* mininet@c2:/home/mininet/drench/testcases/
scp -r /home/mininet/drench/testcases/fattree_topo/master/mixed/TCP/$test_dir/* mininet@c3:/home/mininet/drench/testcases/
scp -r /home/mininet/drench/testcases/fattree_topo/master/mixed/TCP/$test_dir/* mininet@c4:/home/mininet/drench/testcases/
