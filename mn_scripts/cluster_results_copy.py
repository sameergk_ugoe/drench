import argparse
import os
controller_ip = '192.168.56.5'
cluster_ip_list = ['192.168.56.1', '192.168.56.2','192.168.56.3','192.168.56.4']
rdir_p = '/home/mininet/drench/results'
   
def setup_cluster_list(servers):
    global cluster_ip_list
    servers_list = servers.rstrip('\n')
    servers_list = servers_list.split(',')
    cluster_ip_list = [str(s).rstrip(' ') for s in servers_list]
    print "Cluster List:%s" %cluster_ip_list

def parse_args():
    global base_dir, controller_ip

    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--base_dir', '-b', required=True,help='Base Directory of execution')
    parser.add_argument('--cip', '-cip', help='Controller IP')
    parser.add_argument('--cluster_list', '-cllst', help='Cluster_ip_list')
    args = parser.parse_args()
    
    if args.base_dir:
        base_dir = args.base_dir
        rdir_p = os.path.join(base_dir,'results')
    if args.cip:
        controller_ip = args.cip
    if args.cluster_list:
        setup_cluster_list(args.cluster_list)
    return
    
def save_all_results(cluster_ip_list, controller_ip, rdir_p):
    dest_path = 'mininet@' + controller_ip + ':' + rdir_p
    for ip_addr in cluster_ip_list:
        cmd = 'scp -r '
        src_path = 'mininet@' + ip_addr + ':' + os.path.join(rdir_p, '*')
        cmd = cmd + src_path + ' ' + dest_path
        print "executing: %s" %cmd
        os.system(cmd) # '"%s" " %s" " %s"' % (cmd, src_path, dest_path)
    return
def run():
    parse_args()
    save_all_results(cluster_ip_list,controller_ip,rdir_p)
if __name__ == '__main__':
    run()
