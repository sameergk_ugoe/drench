import socket
import sys
import subprocess
import commands

from scapy.all import conf
conf.use_pcap = True
#import scapy.arch.pcapdnet 

from scapy.all import sendp, sniff, IP, ICMP, Dot1Q, Ether, Raw, srp, srp1, sr1, send, TCP, UDP, wrpcap
from scapy.utils import PcapWriter
import csv
import netifaces
import time
import sys
from os.path import expanduser
import os.path
from threading import Thread
import my_sniff_tool as mysniff
import argparse

pktdumper = None
host_interface = "Hx-eth0"
host_mac_addr  = "00:00:00:00:00:00"
host_ip_addr   = "10.0.0.0"
hdir_p = expanduser('~')

base_dir = os.path.join(hdir_p, 'drench')
rdir_p = os.path.join(base_dir,'results')
svch_dir = os.path.join(base_dir,'svc_chains')
fdir_p = os.path.join(base_dir,'testcases')
ctrl_dir = os.path.join(base_dir,'pox')
code_dir = os.path.join(base_dir,'mn_scripts')
svc_ch_file = os.path.join(svch_dir, 'svc_chain_dict.csv')
rx_file = os.path.join(rdir_p, 'rx.csv')
xx_file = os.path.join(rdir_p, 'xx.csv')
xy_file = os.path.join(rdir_p, 'xy.csv')
rx_flows_file = os.path.join(rdir_p, 'rx_flows.csv')
of_usage_log_file = None 
payload = ",svclist,"
pkt_counter = 0
l2_socket = None
UDP_SERVER_PORT = 10001
TCP_SERVER_PORT = 10002
TCP_CLIENT_PORT = 10003
UDP_CLIENT_PORT = 10004
MAX_WAIT_TIME_FOR_RESP = 1
TCP_PACKET_SIZE = 4096
UDP_DGRAM_SIZE  = 4096
start_link_monitor = False

# State information for Host services and flows:
host_svc_list_r = []
host_flows_dict_r = {}
host_svc_list_p = []

def get_list_of_svcs_from_string(in_str):
    str_items = in_str.split(',')
    num_list = []
    for str in str_items:
        val = int(''.join(ele for ele in str if ele.isdigit())) # float(''.join(ele for ele in x if ele.isdigit() or ele == '.'))
        num_list.append(val)
    return num_list
def read_to_dict_from_csv(dict_file_name='dict.csv'):
    global host_svc_list_r
    reader = csv.reader(open(dict_file_name, 'rb'))
    for row in reader:
        key = str(row[0])
        vals = str(row[1])
        if str(key) == str(host_ip_addr):
            host_svc_list_r = get_list_of_svcs_from_string(vals) #[int(s) for s in vals.split(,) if s.isdigit() and s not in [\", \", \[, \]]
            break

def check_for_svc_in_host_list(svc):
    if svc in host_svc_list_r: return True
    else:
        # could be newly instantited (check again by upating now)
        svc_tracker()
        if svc in host_svc_list_r: return True
    return False
        
def update_paths():
  global  base_dir, rdir_p, svch_dir, ctrl_dir, code_dir
  global rx_file, xx_file, xy_file, rx_flows_file
  
  rdir_p = os.path.join(base_dir,'results')
  svch_dir = os.path.join(base_dir,'svc_chains')
  ctrl_dir = os.path.join(base_dir,'pox')
  code_dir = os.path.join(base_dir,'mn_scripts')
  
  svc_ch_file = os.path.join(svch_dir, 'svc_chain_dict.csv')
  rx_file = os.path.join(rdir_p, 'rx.csv')
  xx_file = os.path.join(rdir_p, 'xx.csv')
  xy_file = os.path.join(rdir_p, 'xy.csv')
  rx_flows_file = os.path.join(rdir_p, 'rx_flows.csv')

def parse_named_args():
    global base_dir, conf_file, svc_ch_file, test_file, ecn_mode
    is_parsed = False
    
    parser = argparse.ArgumentParser(description='Agrs for Host Tasks')
    parser.add_argument('--base_dir', '-b', help='Base Directory of execution')
    parser.add_argument('--svch', '-s', help='Service Chain File')
    #parser.add_argument('--cfg', '-c', required=True, help='Config file with description of Topology')
    args = parser.parse_args()
    
    if args.base_dir:
        base_dir = args.base_dir
        update_paths()
        is_parsed = True
    if args.svch:
        svc_ch_file = args.svch # svch_dir + args.svch
    return is_parsed

def parse_args():
    return parse_named_args()
    
#import pox.misc.svcgen_sk as SvcGen
import SvcChainGenerator as SvcGen

def load_svc_chain_info(svc_chain_file=svc_ch_file):
    SvcGen.read_to_dict_from_csv(svc_chain_file)
    #log.info ("Loaded Service Chain with length [%d]!", get_svc_chain_len_for_key(8))

def get_svc_chain_len_for_key(key):
    svc = SvcGen.get_svc_chian_len_for_tos(ip_tos=key)
    if svc is None:
        #log.error("get_svc_chain_len_for_key():: SVC for key[%d] is not Available!", key)
        svc = -1
    #log.error("get_svc_chain_len_for_key():: for key[%d] len=[%d]!", key, svc)
    return svc
  
def get_svc_for_key_at_index(key,index):
    svc = SvcGen.get_svc_at_index_for_tos(ip_tos=key, index=index)
    if svc is None:
        #log.error("SVC for key[%d], index[%d] is not Available!", key, index)
        svc = -1
    #log.error("get_svc_for_key_at_index():: for key[%d] at index [%d] SVC=[%d]!", key, index, svc)
    return svc

def get_ttl_svs_to_process(key, index):
    chain = SvcGen.get_svc_chain_for_tos(key)
    svc_count = 0
    if index < len(chain): 
        for svc in chain[index:]:
            if svc in host_svc_list_r: svc_count +=1
            else: break
    else: return index
    if svc_count == 0:
        #potentially first time the service is instantiated??
        check_for_svc_in_host_list(svc)
        svc_count = 1
    return index+svc_count
def getInterfaceInfo():
    # Get interface name
    #print "start reading interface details!!"
    interface_list = netifaces.interfaces()
    interface = filter(lambda x: 'eth0' in x,interface_list)
    if len(interface) == 0:
      print "No interface found"
      return
    
    global host_interface
    global host_mac_addr
    global host_ip_addr
    global l2_socket
    
    addrs      = netifaces.ifaddresses(interface[0])
    link_addrs = addrs[netifaces.AF_LINK]
    net_addrs  = addrs[netifaces.AF_INET]
    host_interface  = interface[0]
    host_mac_addr   = link_addrs[0]['addr']
    host_ip_addr    = net_addrs[0]['addr']
    
    l2_socket =  conf.L2socket(iface=host_interface)
    #print "host_interface:", host_interface, "host_mac_addr:", host_mac_addr, "host_ip_addr:", host_ip_addr
    return host_interface, host_mac_addr, host_ip_addr

def disable_arp():
    #Drop ARP Packets:
    cmd = 'arptables -P INPUT DROP'
    os.system(cmd)
    cmd = 'arptables -P OUTPUT DROP'
    os.system(cmd)
    #Disable ARP
    #cmd = "sudo ip link set dev " + str(host_interface) + " arp off"
    #os.system(cmd)
    
    return

def switch_route_del_and_add(br_name, f):
    ip_addr = host_ip_addr
    mc_addr = host_mac_addr
    ip_addr_b = ip_addr[0:ip_addr.rfind('.')] + '.0'
    
    cmd = 'ifconfig ' + f + ' 0'
    os.system(cmd)
    
    cmd = 'sudo ip route del ' + ip_addr_b + '/24 dev ' + f
    os.system(cmd)
    
    if br_name:
        cmd = 'sudo ifconfig ' + br_name + ' ' + ip_addr
        os.system(cmd)
        cmd = 'sudo ip route add ' + ip_addr_b + '/24 dev ' + br_name
        os.system(cmd)
    
    return

def configure_host_local_ovs_and_controller():
    
    #Launch the local node controller now
    #cmd1 = 'cd ' + ctrl_dir
    
    script = 'lcl_mbox_host_controller.sh' #'start_local_node_controller.sh'
    script = os.path.join(ctrl_dir, script)
    
    arg1 = base_dir # base_dir
    arg2 = 'list3.csv' # nfv_rsrc_file
    arg3 = os.path.basename(svc_ch_file) # policy spec file
    arg4 = host_interface
    arg5 = host_mac_addr
    cmd = script + ' ' + arg1 + ' ' + arg2 + ' ' + arg3 + ' ' + arg4 +  ' ' + arg5 + ' &'
    #cmd = cmd1 + ' && ' + cmd2 + ' &'
    os.system(cmd)
    
    #Configure the Openvswitch first for the existing host interface
    script = 'host_vswitch.sh'
    script = os.path.join(code_dir, script)
    node_name =  host_interface.split('-')[0]
    #node_iface = host_interface
    #node_ipaddr = host_ip_addr
    cmd = script + ' ' + node_name + ' ' + host_interface + ' ' + host_ip_addr 
    os.system(cmd)
    
    return
    
def teardown_host_local_ovs_and_controller():
    
    #To kill the controller:
    cmd = 'sudo fuser -k 6633/tcp'
    os.system(cmd)
    
    # To delete the openvswitch #
    script = 'host_vswitch.sh'
    script = os.path.join(code_dir, script)
    node_name =  host_interface.split('-')[0]
    #node_iface = host_interface
    #node_ipaddr = host_ip_addr
    cmd = script + ' ' + node_name + ' ' + host_interface + ' ' + host_ip_addr + ' 1'
    os.system(cmd)
    
    return
    
def configure_local_nfv_host_ovs_and_controller_interface_details():
    
    #Harcode the controlelr node in cluster to bypass this configuration
    #if '192.168.56.5' == 
    
    br_name = 'lbr' + str(host_interface)
    
    cmd = 'sudo ovs-vsctl add-br ' + br_name 
    os.system(cmd)
    
    # add all interfaces as ports to ovs-switch
    switch_route_del_and_add(br_name, host_interface)
    #cmd = 'sudo ifconfig ' + host_interface + ' 0'
    #os.system(cmd)
    cmd = 'sudo  ovs-vsctl add-port ' + br_name + ' ' + host_interface 
    os.system(cmd)

    #Update Bridge control settings:
    controller_ip_port = "127.0.0.1:6633" #get_controller_ip_and_port()
    cmd = 'sudo ovs-vsctl set-controller ' + br_name + ' tcp:' + controller_ip_port + ' &'
    os.system(cmd)
    
    #Set OF1.0 Protocol
    cmd = 'sudo ovs-vsctl set bridge ' + br_name + ' protocol=OpenFlow10'
    os.system(cmd)

    #Set mode secure, so that we can track for all flows
    #cmd = 'sudo ovs-vsctl set-fail-mode ' + br_name + ' standalone '
    cmd = 'sudo ovs-vsctl set-fail-mode ' + br_name + ' secure '
    os.system(cmd)

    cmd = 'sudo ovs-vsctl show'
    os.system(cmd)
    
    #Launch Local controller
    
    return

def optimize_nw_stack():
    #http://engineering.chartbeat.com/2014/01/02/part-1-lessons-learned-tuning-tcp-and-nginx-in-ec2/
    #http://www.nateware.com/linux-network-tuning-for-2013.html#.Vyxs054jl-Y
    # Make room for more TIME_WAIT sockets due to more clients,
    # and allow them to be reused if we run out of sockets
    # Also increase the max packet backlog
    
    #net.core.netdev_max_backlog = 50000
    cmd = "sudo sysctl -w net.core.netdev_max_backlog=50000"
    os.system(cmd)
    
    #net.ipv4.tcp_max_syn_backlog = 30000
    cmd = "sudo sysctl -w net.ipv4.tcp_max_syn_backlog=30000"
    os.system(cmd)
    
    #net.ipv4.tcp_max_tw_buckets = 2000000
    cmd = "sudo sysctl -w net.ipv4.tcp_max_tw_buckets=2000000"
    os.system(cmd)
    
    #net.ipv4.tcp_tw_reuse = 1
    cmd = "sudo sysctl -w net.ipv4.tcp_tw_reuse=1"
    os.system(cmd)
    
    #net.ipv4.tcp_fin_timeout = 10
    cmd = "sudo sysctl -w net.ipv4.tcp_fin_timeout=10"
    os.system(cmd)

    # Disable TCP slow start on idle connections
    #sudo sed -e "s/^1$/0/" /proc/sys/net/ipv4/tcp_slow_start_after_idle
    #net.ipv4.tcp_slow_start_after_idle = 0
    cmd = "sudo sysctl -w net.ipv4.tcp_slow_start_after_idle=0"
    os.system(cmd)
    
    #Increase the initcwnd to 16
    cmd = 'ip route | while read p; do ip route change $p initcwnd 16 initrwnd 16; done'
    os.system(cmd)
    
    

def configure_host():
    global host_interface
    global host_ip_addr
    global pktdumper
    global rx_flows_file
    global of_usage_log_file
    
    #Disable IPv6:
    cmd = "sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1"
    os.system(cmd)
    cmd = "sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1"
    os.system(cmd)
    cmd = "sudo sysctl -w net.ipv6.conf.lo.disable_ipv6=1"
    os.system(cmd)
    cmd = "ip6tables -P INPUT DROP"
    os.system(cmd)
    cmd = "ip6tables -P OUTPUT DROP"
    os.system(cmd)
    
    #Disable ARP
    #cmd = "sudo ip link set dev " + str(host_interface) + " arp off"
    #os.system(cmd)
    
    #Configure IP Tables
    #iptables -A OUTPUT -p tcp --tcp-flags RST RST -j DROP
    cmd = 'iptables -A OUTPUT -p tcp --tcp-flags RST RST -j DROP'
    os.system(cmd)
    cmd = 'iptables -L'
    os.system(cmd)
    
    #Disable ECN Capabiliy
    cmd = 'echo 0 > /proc/sys/net/ipv4/tcp_ecn'
    os.system(cmd)
    cmd = 'sudo sysctl -w net.ipv4.tcp_ecn=0'
    os.system(cmd)
    
    #TCP Optimizations
    optimize_nw_stack()

    
    #Finalize the effect of above commands:
    cmd = 'sudo sysctl -p'
    os.system(cmd)
    
    #setup the PCAP FILES for CAPTURE
    pcap_dump = 'pcap_dump' + str(host_ip_addr[-2:]) + '-' + str(host_interface) + '.pcap'
    pcap_dump = os.path.join(rdir_p, pcap_dump)
    

    #cmd = 'cp -rf ' + pcap_dump + ~/pox_d/'
    #cmd 'rename "s/\.pcap$/\_$(date +%H-%M-%S)\.pcap/" *.pcap'
    #cmd 'mv *.pcap bkp/'

    
    #TCPDUMP Options <generating truncated files:: using scapy instead (but scapy impacts CPU utlization)
    #cmd = 'sudo tcpdump -i ' + str(host_interface) + ' -w ' + pcap_dump + ' portrange ' + str(UDP_SERVER_PORT) + '-' + str(TCP_SERVER_PORT)
    #cmd = 'tcpdump -w ' + pcap_dump + ' dst port ' + str(TCP_SERVER_PORT) + ' or dst port ' + str(UDP_SERVER_PORT) + ' &'
    #os.system(cmd)
    
    # OR SCAPY PCAP WRITER
    #pcap_dump= rdir_p + 'sp_pcap_dump' + str(host_ip_addr[-2:]) + '-' + str(host_interface) + '.pcap'
    #pktdumper = PcapWriter(pcap_dump, append=True, sync=True)
    
    # Load the SVC Chain File
    load_svc_chain_info(svc_ch_file)
    
    # Setup the Log Files 
    rx_flows_file = 'rx_flows' +  host_ip_addr + '.csv'
    rx_flows_file = os.path.join(rdir_p, rx_flows_file)
    of_usage_log_file = 'iface_' + str(host_ip_addr) +  '_usage_rlog.csv'
    of_usage_log_file = os.path.join(rdir_p, of_usage_log_file)

    pass
    return
    
def advertise_host():
    global host_interface
    global host_mac_addr
    global host_ip_addr
 
    #Send Dummy packet to help discover the host in the controller
    dnum =  int(host_ip_addr[(host_ip_addr.rfind('.')+1):])
    if dnum == 1 or dnum == 0: 
        dnum = 2
    else:
        dnum -= 1
    dst_ip_addr = host_ip_addr[0:host_ip_addr.rfind('.')] + '.' + str(dnum)
    dst_mac_addr = host_mac_addr[0:host_mac_addr.rfind(':')]+':'+ "{:0>2d}".format(dnum)
    
    #if dnum < 10:
    #   dst_mac_addr = host_mac_addr[0:host_mac_addr.rfind(':')]+':0'+str(dnum)
    #else:
    #   dst_mac_addr = host_mac_addr[0:host_mac_addr.rfind(':')]+':'+str(dnum)
    
    #print "sending Packet to Neighbour:", dst_ip_addr, dst_mac_addr
    #sendp(Ether(src=host_mac_addr,dst=dst_mac_addr)/Dot1Q(vlan=0)/IP(src=host_ip_addr,dst=dst_ip_addr))
    
    #ans,unans=srp(Ether(src=host_mac_addr,dst=dst_mac_addr)/Dot1Q(vlan=0)/IP(src=host_ip_addr,dst=dst_ip_addr),timeout=1)
    icmp_pkt = Ether(src=host_mac_addr,dst=dst_mac_addr)/IP(src=host_ip_addr,dst=dst_ip_addr)/ICMP(type=4,code=0)
    #ans,unans=srp(icmp_pkt,timeout=1)
    sendp(icmp_pkt)
    #if ans is not None:
    #   print ans.show()
    #ans,unans=srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst="192.168.1.0/24"),timeout=2)

def setup_local_ovs_switch_and_controller():
    #configure_local_nfv_host_ovs_and_controller_interface_details()
    configure_host_local_ovs_and_controller()
    return
def isIncoming(pkt):
   
    try:
        #if pkt.haslayer(TCP) and (pkt[TCP].dport == TCP_SERVER_PORT or pkt[TCP].dport == TCP_CLIENT_PORT): #7 and not pkt.haslayer(Dot1Q):
            #print "Got True TCP Packet! "
        #    return True 
        #if pkt.haslayer(UDP) and (pkt[UDP].dport == UDP_SERVER_PORT or pkt[UDP].dport == UDP_CLIENT_PORT):
            #print "Got True UDP Packet! "
        #    return True
        #Any Other: Ignore Outgoing
        if pkt.haslayer(Dot1Q):
            return False
            #print "Packets Has Dot1Q:"
            #print "Dot1Q Pkt:", pkt.show()
            #print "Packets Dot1Q:%d" %pkt[Dot1Q].vlan

        if pkt.haslayer(ICMP) and pkt[ICMP].type==8: #and pkt[Ether].dst == host_mac_addr:
            #print "Packet with ICMP [8]!!"
            return True

        if pkt.haslayer(Ether) and pkt[Ether].src == host_mac_addr:
            return False
        if pkt.haslayer(IP) and pkt[IP].src == host_ip_addr:
            return False

        if pkt.haslayer(Ether) and pkt[Ether].dst == host_mac_addr:
        #if pkt[IP].dst == host_ip_addr: #and pkt[Ether].dst == host_mac_addr:
            # FOR TCP
            if pkt.haslayer(TCP) and (pkt[TCP].dport == TCP_SERVER_PORT or pkt[TCP].dport == TCP_CLIENT_PORT): #7 and not pkt.haslayer(Dot1Q):
                return True #a = True#
            if pkt.haslayer(UDP) and (pkt[UDP].dport == UDP_SERVER_PORT or pkt[UDP].dport == UDP_CLIENT_PORT):
                #print "Got True UDP Packet! "
                return True
            if pkt.haslayer(TCP) and (pkt[TCP].dport == (TCP_SERVER_PORT+20) or pkt[TCP].dport == (TCP_CLIENT_PORT+20)): #7 and not pkt.haslayer(Dot1Q):
                return True #a = True#
            if pkt.haslayer(UDP) and (pkt[UDP].dport == (UDP_SERVER_PORT+20) or pkt[UDP].dport == (UDP_CLIENT_PORT+20)):
                #print "Got True UDP Packet! "
                return True
    except:
        pass
    return False

def getinstantaneous_sp(vlan =0, mac_id=host_mac_addr):
  isp = 0
  found = False
  global host_mac_addr
  
  fname = str(vlan) + '_' + str(mac_id[mac_id.rfind(":")+1:])
  fname = rdir_p + fname + '.csv'  # /home/mininet/drench/results/13_1.csv
  #print "Trying to open file:",fname
  if ( False == os.path.isfile(fname)):
    return isp
  
  with open(fname, 'rb') as csvfile0:
    for line in csvfile0:
      if line is None or len(line) == 0 or False == line.isdigit():
        return isp
      line.rstrip('\n')
      line.rstrip(',')
      isp = int(line)
      #print "From File:",fname, "Read isp", isp
      break

  return isp

def log_pkt_icmp(id=1, seq=1,srcip="", data=""):
    rx_file_func = rx_file.split('.')[0] + '_' + str(id) +'.'+rx_file.split('.')[1]
    with open(rx_file_func, 'ab') as csvfile3:
        rxWriter = csv.writer(csvfile3,delimiter=',')
        #rxWriter.writerows([[str(id), str(seq), str(srcip), str(data)]])
        rxWriter.writerows([[str(id), str(seq), str(data)]])
    return
def log_garbage_pkt_icmp(id=1, seq=1,srcip="",dstip=""):
    with open(xx_file, 'ab') as csvfile2:
        xxWriter = csv.writer(csvfile2,delimiter=',')
        xxWriter.writerows([[str(time.time()), str(id), str(seq), str(srcip), str(dstip),str(host_interface),str(host_ip_addr)]])
    return

SAFE_TIMEOUT =0.5 #10
svc_info_tbl = {}
#Information about flows utilizing some service at this host node
class FlowInfo:
    def __init__(self, svc_id, in_key):
        self.in_key = in_key
        self.svc_id = svc_id
        self.start_time = time.time()
        self.end_time = self.start_time
        self.ttl_packets = 0
        self.avg_pkt_rate =0
        self.last_logged_time = 0
        return
    def increment_pkt_count(self):
        self.end_time = time.time()
        self.ttl_packets+=1
        self.last_logged_time = 0
        return
    def check_flow_expired(self,cur_time):
        if self.end_time + 10 < cur_time: return True
        return False
#Information regarding the services that are getting utlizied on this host node
class mySvcInfo:
    def __init__(self, svc_id):
        self.svc_id = svc_id
        self.start_time = time.time()
        self.end_time = self.start_time
        self.ttl_packets = 0
        self.avg_pkt_rate =0
        self.last_logged_time = self.start_time
        self.last_logged_pkt_count = 0
        self.flow_tbl = {}
        self.num_of_flows = 0
        #self.mean_pkt_rate = []
        return
    
    def update_avg_pkt_rate(self):
        #if self.end_time != self.start_time: self.avg_pkt_rate = float(self.ttl_packets/(self.end_time-self.start_time))
        ttl_duration = self.end_time - self.start_time
        if ttl_duration:
            self.avg_pkt_rate = float(self.ttl_packets/ttl_duration)
        return
    def get_svcinfo_details(self):
        #return 100
        cur_time = time.time()
        cur_ival_pkt_rate = 0
        active_status = True
        if self.end_time + 10 < cur_time: active_status = False
        cur_ival_pkt_rate = self.get_current_interval_packet_rate(cur_time)
        
        self.update_avg_pkt_rate()

        return(cur_ival_pkt_rate, self.avg_pkt_rate,len(self.flow_tbl),active_status)
        #return(self.svc_id,self.start_time,self.end_time,self.ttl_packets,self.avg_pkt_rate)
        
    def get_current_interval_packet_rate(self,cur_time):
        cur_ival_pkt_rate = 0
        if self.end_time > self.last_logged_time:
            cur_ival = self.end_time - self.last_logged_time
            cur_ival_pkts = self.ttl_packets - self.last_logged_pkt_count
            if cur_ival_pkts <=0: return 0
            cur_ival_pkt_rate = float(cur_ival_pkts/cur_ival)
        self.set_logged_time(cur_time)
        return cur_ival_pkt_rate
    def get_end_time(self):
        return self.end_time
    def set_logged_time(self,log_time):
        self.last_logged_time = log_time
        self.last_logged_pkt_count = self.ttl_packets
        return
    def get_logged_time(self):
        return self.last_logged_time
    def increment_pkt_count(self, flow_key):
        self.end_time = time.time()
        self.ttl_packets+=1
        #self.last_logged_time = 0
        self.update_flow_counter(flow_key)
        return
    def update_flow_counter(self,flow_key):
        flow_obj = None
        if self.flow_tbl.has_key(flow_key):
            flow_obj = self.flow_tbl[flow_key]
        else:
            flow_obj = FlowInfo(self.svc_id,flow_key)
            self.flow_tbl[flow_key] = flow_obj
        
        if flow_obj is None: return
        flow_obj.increment_pkt_count()
        return
    def check_for_expired_flows(self,cur_time):
        # Need logic to expire the flows from flow_tbl
        for flow in self.flow_tbl.keys():
            if self.flow_tbl[flow].check_flow_expired(cur_time):
                self.flow_tbl.pop(flow)
        return
def log_svc_pkt(svc, srcip, dstip, tos, ttl):
    global svc_info_tbl
    if svc_info_tbl.has_key(svc):
        svc_obj = svc_info_tbl[svc]
    else:
        svc_obj = mySvcInfo(svc_id=svc)
        svc_info_tbl[svc] = svc_obj
    
    if svc_obj:
        flow_key = (str(srcip),str(tos),str(dstip))
        svc_obj.increment_pkt_count(flow_key)
    return

# This needs to be periodically called to get the pkt rate observed at each service
def write_svc_utility_to_file(cur_time):
    rx_f = rx_file.split('.')
    t_time = time.time()
    for svc_key in svc_info_tbl.keys():
        svc_info_tbl[svc_key].check_for_expired_flows(t_time)
        #svc_info = svc_info_tbl[svc_key].get_svcinfo_details()
        cur_pkt_rate, avg_pkt_rate, cur_flows,sts = svc_info_tbl[svc_key].get_svcinfo_details()
        rx_file_func = rx_f[0] + '_' + str(svc_key) + '.' + rx_f[1]
        with open(rx_file_func, 'ab') as csvfile3:
            rxWriter = csv.writer(csvfile3,delimiter=',')
            #rxWriter.writerows([[str(cur_time), str(host_mac_addr), str(svc_info)]])
            rxWriter.writerows([[str(cur_time), str(host_mac_addr), str(cur_pkt_rate), str(avg_pkt_rate), str(cur_flows), str(sts)]])
    return

#Information regarding the Flows reaching this Host as end destination
class SFlow_Info:
    def __init__(self, in_key):
        self.in_key = in_key
        self.start_time = time.time()
        self.end_time = self.start_time
        self.ttl_packets = 1
        self.avg_pkt_rate =0
        self.last_logged_time = 0
        #self.mean_pkt_rate = []
        return
    def get_flow_details(self):
        ttl_duration = (self.end_time - self.start_time)
        if ttl_duration:
            self.avg_pkt_rate = float(self.ttl_packets/ttl_duration)
        print" FLow Details [%s:%s:%s]" %(ttl_duration, self.avg_pkt_rate, (self.in_key,self.start_time,self.end_time,self.ttl_packets))
        return (ttl_duration, self.ttl_packets, self.avg_pkt_rate, (self.in_key,self.start_time,self.end_time))
    def increment_pkt_count(self):
        self.end_time = time.time()
        self.ttl_packets+=1
        self.last_logged_time = 0
        return
    def get_end_time(self):
        return self.end_time
    def set_logged_time(self,log_time):
        self.last_logged_time = log_time
        return
    def get_logged_time(self):
        return self.last_logged_time
flow_info_tbl = {}
def log_pkt_udp(svc=0, srcip="", dstip="", tos=0, ttl=0):
    global flow_info_tbl
    flow_key = (str(srcip),str(tos),str(dstip))
    if flow_info_tbl.has_key(flow_key):
        flow_info_tbl[flow_key].increment_pkt_count()
    else:
        flow_obj = SFlow_Info(in_key=flow_key)
        flow_info_tbl[flow_key] = flow_obj
    return
def write_streams_to_file():
    #rx_file_func = rx_file.split('.')[0] + '_' + str(host_ip_addr) + '_' + rx_file.split('.')[1]
    rx_file_func = 'rx_file_' + str(host_ip_addr) + '_.csv'
    rx_file_func = os.path.join(rdir_p,rx_file_func)
    cur_time = time.time()
    with open(rx_file_func, 'ab') as csvfile2:
        rxWriter = csv.writer(csvfile2,delimiter=',')
        #rxWriter.writerows([[cur_time, str("Hello!!")]])
        for flow_key in flow_info_tbl.keys():
            if flow_info_tbl[flow_key].get_logged_time(): continue
            if (flow_info_tbl[flow_key].get_end_time() + SAFE_TIMEOUT) < cur_time: #Safety time
                flow_fct, flow_pkts, flow_rate, flow_info = flow_info_tbl[flow_key].get_flow_details()
                #print "Values are %s:%s:%s " %(flow_fct, flow_rate, flow_info)
                flow_info_tbl[flow_key].set_logged_time(cur_time)
                rxWriter.writerows([[cur_time, str(flow_fct), str(flow_pkts),str(flow_rate), str(flow_info)]])
            else: pass # assume flow is still active
    return
    
def log_pkt_udp__old(svc=0, srcip="", dstip="", tos=0, ttl=0):
    rx_file_func = rx_file.split('.')[0] + '_' + str(srcip) + '_' + str(tos) + '_' + str(dstip) + '.' + rx_file.split('.')[1]
    with open(rx_file_func, 'ab') as csvfile3:
        rxWriter = csv.writer(csvfile3,delimiter=',')
        #rxWriter.writerows([[str(time.time()), str(svc), str(tos), str(srcip), str(ttl)]])
        rxWriter.writerows([[str(time.time()), str(svc), str(tos), str(ttl)]])
    return
def log_garbage_pkt_udp(svc=0, tos=0, ttl=0, srcip="",dstip=""):
    with open(xx_file, 'ab') as csvfile2:
        xxWriter = csv.writer(csvfile2,delimiter=',')
        xxWriter.writerows([[str(time.time()), str(srcip), str(tos), str(ttl), str(dstip), str(host_interface), str(host_ip_addr)]])
    return
def log_udp_cap(srcip,dstip,tos,ttl):
    rx_file_func = "/home/mininet/drench/results/udp_cap.csv"
    with open(rx_file_func, 'ab') as csvfile3:
        rxWriter = csv.writer(csvfile3,delimiter=',')
        #rxWriter.writerows([[str(time.time()), str(svc), str(tos), str(srcip), str(ttl)]])
        rxWriter.writerows([[str(time.time()), str(host_ip_addr), str(srcip), str(dstip), str(tos), str(ttl)]])
    return
def log_sniff_pkt(vlan=0, id=1, seq=1,srcip="",dstip="",payload=""):
    xy_file_func = xy_file.split('.')[0] + '_' + str(host_ip_addr[host_ip_addr.rfind('.'):]) +'.'+xy_file.split('.')[1]
    with open(xy_file_func, 'ab') as csvfile4:
        xyWriter = csv.writer(csvfile4,delimiter=',')
        xyWriter.writerows([[str(time.time()), str(id), str(seq), str(srcip), str(dstip),str(host_ip_addr), str(payload)]])
    return

  
def getdata_fromPayload_icmp(pkt):
    #set_nid_nseq(pkt)
    vlan = 0
    nvlan = 0
    #npayload = pkt[0:pkt.find(',svclist')] + ',svclist,end,'
    npayload = ',svclist,end,'
    svlan = None
    snvlan = None
    sp = 0
    try:
        pkt = pkt.rstrip()
        of = pkt.find(',svclist,')
        ln = len(',svclist,')
        svlan = pkt[of+ln: pkt.find(',',of+ln)]
        if svlan is None or len(svlan) == 0 or False == svlan.isdigit():
            vlan = 0
            nvlan = 0
            # Append the DST Details <SVC_ID-IP:SP:Time.>,
            npayload = pkt + '{0:03d}'.format(0) + '-' + str(host_ip_addr) +':' '{0:03d}'.format(0) + ':' + str(time.time()) + '.'
        else:
            #print "svlan", svlan, "pkt=", pkt
            vlan = int(svlan,2)
            snvlan = pkt[of+ln+len(svlan+','):pkt.find(',',of+ln+len(svlan+','))]
            #print "snvlan:", snvlan
            sp = getinstantaneous_sp(vlan = vlan,  mac_id=host_mac_addr)
            if snvlan is None or len(snvlan) == 0 or False == snvlan.isdigit():
                nvlan = 0
            #print "set nvlan to Zero"
            else:
                #print "Extracting snvlan"
                nvlan = int(snvlan,2)
            #npayload = ',svclist,'+ pkt[of+ln+len(svlan +','):]
            #npayload = pkt[0:pkt.find(',svclist')] +',svclist,'+ pkt[of+ln+len(svlan +','):pkt.find(',end,')] + ',end,'
            npayload = ',svclist,'+ pkt[of+ln+len(svlan +','):pkt.find(',end,')] + ',end,'
      
            # Stitch the Maximum SP at the front and also append the Timestamp, instance SP for Function processing
            of = pkt.find(',end,')
            ln = len(',end,')
            scsp = pkt[of+ln: pkt.find(',', of+ln)]
            pkt_sp = 0
            #print "0 sp = ", sp, "scsp=",scsp
            if scsp is None or len(scsp) == 0:
                pkt_sp = sp
                #npayload = npayload + str(sp) +','
                #npayload = npayload + '{0:03d}'.format(sp) + '-' +
            else:
                #pkt_sp = str(scsp)
                pkt_sp = int(scsp)
                #print "1 sp = ", sp, "csp=",csp
                if sp > pkt_sp:
                    pkt_sp = sp
                #print " sp = ", sp, "csp=",csp, "np=",npayload
                #npayload = npayload + str(pkt_sp) + ','
            
            #Stitch MAX SP
            spkt_sp = '{0:03d}'.format(pkt_sp)
            npayload = npayload + spkt_sp + ','
          
            #Now append the Previous Service Name and TS values in the Packet (if any)
            npayload = npayload + pkt[of+ln+len(spkt_sp)+len(','):]
            #append the Service Name and TS
            #npayload = npayload + str(vlan) + '-' + str(host_ip_addr) + ':' + str(time.time()) + ','
            #append the Service Name, Current Instance Service Cost and TS
            #npayload = npayload + str(vlan) + '-' + str(host_ip_addr) + ':' + str(sp) + ':' + str(time.time()) + ','
            npayload = npayload + '{0:03d}'.format(vlan) + '-' + str(host_ip_addr) + ':' + '{0:03d}'.format(sp) + ':' + str(time.time()) + ','

    except Exception, e:
        print "Parse for Payload data Failed!!", str(e)
        #print "New Payload:", npayload
        
    return vlan, nvlan, npayload
icmp_pkt = Ether()/Dot1Q()/IP()/ICMP()
def createPkt3_icmp(nvlan=0, nid=1, nseq=1, ipsrc=None, ipdst=None, ndst=None, nsrc=None, npayload=payload):
    
    global l2_socket, icmp_pkt
    #pkt = Ether(dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload
    icmp_pkt[Ether].dst=ndst
    icmp_pkt[Dot1Q].vlan=nvlan
    icmp_pkt[IP].src=ipsrc
    icmp_pkt[IP].dst=ipdst
    icmp_pkt[ICMP].id=nid
    icmp_pkt[ICMP].seq=nseq
    icmp_pkt[ICMP].payload=npayload
    
    if l2_socket is not None:
        l2_socket.send(icmp_pkt)
        #print "Sent via L2 Socket!"
    else:
        sendp(icmp_pkt, verbose=0)
        print "Sent via Sendp()"
    return

def get_new_udp_pkt(udp_pkt):
    u_pkt = Ether(src=host_mac_addr,dst=udp_pkt[Ether].dst,type=udp_pkt[Ether].type)/IP(src=udp_pkt[IP].src,dst=udp_pkt[IP].dst,tos=udp_pkt[IP].tos,ttl=udp_pkt[IP].ttl)/UDP(sport=udp_pkt[UDP].sport,dport=udp_pkt[UDP].dport)
    return u_pkt
def createPkt_udp(udp_ipkt, ttl=0, insert_vlan=False):
    global l2_socket 
    #pkt = Ether(dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload
    
    udp_ipkt[Ether].src=host_mac_addr
    
    #udp_ipkt[Ether].dst=ndst
    #udp_ipkt[Dot1Q].vlan=nvlan
    #udp_ipkt[IP].src=ipsrc
    #udp_ipkt[IP].dst=ipdst
    
    udp_ipkt[IP].ttl=ttl
    
    if udp_ipkt.haslayer(UDP):
        #Approach 2: Modify udp.sport = ((ttl << 12 )| (udp_ipkt[UDP].sport & 0x0FFF))
        udp_ipkt[UDP].sport = ((ttl << 12 )| (udp_ipkt[UDP].sport & 0x0FFF))
    elif udp_ipkt.haslayer(TCP):
        udp_ipkt[TCP].sport = ((ttl << 12 )| (udp_ipkt[TCP].sport & 0x0FFF))
    #if insert_vlan: udp_ipkt = udp_ipkt/Dot1Q(vlan=insert_vlan)
    
    if len(udp_ipkt) > 1500:
        print " Packet Size exceeds 1500 bytes! [%d]" %len(udp_ipkt)
        #udp_ipkt = udp_ipkt[0:1200]
        udp_ipkt = get_new_udp_pkt(udp_ipkt)
        #return
        print " Packet Size changed to [%d]" %len(udp_ipkt)
    if l2_socket is not None:
        l2_socket.send(udp_ipkt)
        #print "Sent UDP PKT via L2 Socket!"
    else:
        sendp(udp_ipkt, verbose=0)
        #print "Sent UDP PKT via Sendp()"
    return
def PacketHandler_icmp(pkt):
    #id = int(pkt[ICMP].id)
    #seq = int(pkt[ICMP].seq)
    #cpayload = str(pkt[Raw].load)
    cpayload = str(pkt[ICMP].payload)
    srcip = str(pkt[IP].src)
    dstip = str(pkt[IP].dst)
    src = str(pkt[Ether].src)
    dst = str(pkt[Ether].dst)
    vlan,nvlan,npayload = getdata_fromPayload_icmp(cpayload)
    if pkt.haslayer(ICMP):
        id = int(pkt[ICMP].id)
        seq = int(pkt[ICMP].seq)
    else:
        global nid
        global nseq
        id=nid
        seq=nseq
  
    #print "vlan:%d, nvlan:%d id:%d, seq:%d, src:%s, dst:%s, srcip:%s, dstip:%s, host_ip_addr:%s" %(vlan,nvlan,id,seq,src,dst,srcip,dstip,host_ip_addr)
    #print "cpayload=",cpayload, "npayload=",npayload
    #log_sniff_pkt(vlan= vlan, id=id, seq=seq,srcip=srcip,dstip=dstip,payload=cpayload)
  
    if vlan > 0:
        createPkt3_icmp(nvlan=nvlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst, nsrc=src, npayload=npayload)
        #write_pkt(vlan,id,seq)
    
        # sniff mechanism fails, as the in coming packets get intercepted directly with Dot1Q headers and fails to differentiate as incoming or outgoing packet.
        if nvlan == 0 and dstip == host_ip_addr:
            data = npayload[npayload.find(',end,')+len(',end,'):] + '0-' + str(host_ip_addr) +':' + str(time.time()) + '.'
            log_pkt_icmp(id=id, seq=seq, srcip=srcip, data=data)
    else:
        if dstip == host_ip_addr:
            data = npayload[npayload.find(',end,')+len(',end,'):]
            log_pkt_icmp(id=id, seq=seq, srcip=srcip, data=data)
            #createPkt4(nid=id, nseq=seq, ipdst=srcip, ndst=dst, npayload=npayload)
            pass
        else:
            # Send as this could be the last service in chain
            #createPkt3_icmp(nvlan=vlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst, nsrc=src, npayload=npayload)
            log_garbage_pkt_icmp(id=id, seq=seq, srcip=srcip, dstip=dstip)
    return
def PacketHandler_udp(pkt):
    end_of_chain_len = 1
    max_chain_len = 63
    srcip = str(pkt[IP].src)
    dstip = str(pkt[IP].dst)
    tos   = pkt[IP].tos
    ttl   = pkt[IP].ttl
    
    src = str(pkt[Ether].src)
    dst = str(pkt[Ether].dst)
    #print pkt.show()
    
    end_of_chain_len = get_svc_chain_len_for_key(tos)
    insert_vlan = 0
    
    #log_udp_cap(srcip,dstip,tos,ttl)
    if pkt.haslayer(Dot1Q):
        vlan = pkt[Dot1Q].vlan
        pcp  = pkt[Dot1Q].prio
        insert_vlan = True
        #print "Packets Has Dot1Q:"
        #print "Dot1Q Pkt:", pkt.show()
        #print "Packets Dot1Q:%d" %pkt[Dot1Q].vlan
    
    ttl = end_of_chain_len
    
    if ttl > max_chain_len:
        ttl = 0
    
    if ttl < end_of_chain_len:
        new_ttl = ttl +1
        svc_id = get_svc_for_key_at_index(tos,ttl)
        log_svc_pkt(svc=svc_id, srcip=srcip, dstip=dstip, tos=tos, ttl=ttl)
        createPkt_udp(udp_ipkt=pkt, ttl=new_ttl)
        
        ##new_ttl = get_ttl_svs_to_process(tos,ttl)
        ##if new_ttl == end_of_chain_len and dstip == host_ip_addr: # this checks for all svc's at this node, including if this is the intended destination
        ##    log_pkt_udp(svc=0, srcip=srcip, dstip=dstip, tos=tos,ttl=ttl)
        ##else: # intermediate srv or headed to some other destination
            #if src == host_mac_addr: insert_vlan = get_svc_for_key_at_index(tos,ttl)
            #createPkt_udp(udp_ipkt=pkt, ttl=new_ttl, insert_vlan = get_svc_for_key_at_index(tos,new_ttl))
        ##    createPkt_udp(udp_ipkt=pkt, ttl=new_ttl)
    
    elif ttl == end_of_chain_len: # <reached chain length, now it must be destination >
        if dstip == host_ip_addr:
            log_pkt_udp(svc=0, srcip=srcip, dstip=dstip, tos=tos,ttl=ttl)
            #log_pkt_udp__old(svc=0, srcip=srcip, dstip=dstip, tos=tos,ttl=ttl)
            pass
        else:
            log_garbage_pkt_udp(svc=0, srcip=srcip, tos=tos,ttl=ttl, dstip=dstip)
    else:
        log_garbage_pkt_udp(svc=999, srcip=srcip, tos=tos,ttl=ttl, dstip=dstip)
        pass
    
    #if vlan > 0:
    #    pass
    #else:
    #    pass
    return

UDP_PORT_LOW=10000
UDP_PORT_HIGH=15000
def PacketHandler(pkt):
    global host_interface
    global host_ip_addr
    global pktdumper
    
    # ICMP Echo request or source Quench messages
    if pkt.haslayer(ICMP) and (pkt[ICMP].type == 8 or pkt[ICMP].type == 16):
        PacketHandler_icmp(pkt) #print "Got True ICMP Packet! ", pkt.show()
        return
    
    # ICMP Echo reply  (ignore the replies)
    if pkt.haslayer(ICMP) and (pkt[ICMP].type == 0):
        #PacketHandler_icmp(pkt) #print "Got True ICMP Packet! ", pkt.show()
        return
    #if pkt.haslayer(UDP) and (pkt[UDP].sport >= UDP_PORT_LOW or pkt[UDP].dport >= UDP_PORT_LOW):
    if pkt.haslayer(UDP) and pkt[IP].tos !=0 and (pkt[UDP].dport == 8999 or (pkt[UDP].dport > UDP_PORT_LOW and pkt[UDP].dport <= UDP_PORT_HIGH )):
        PacketHandler_udp(pkt)
        return
    if pkt_e.haslayer(TCP) and pkt[IP].tos !=0 and (pkt_e[TCP].dport == 8999 or pkt_e[TCP].dport >= UDP_PORT_LOW):
        PacketHandler_udp(pkt)
    
    # Log packet to pcap
    #wrpcap(pcap_dump,pkt)
    #if pktdumper is not None:
    #    pktdumper.write(pkt)
    #pass
    
    return

def start_sniffer():
    global host_interface
    global host_ip_addr
    
    mysniff.setup_l2sniff(host_interface,PacketHandler)
    
    #comTcpDump = sniff(iface=host_interface, lfilter = isIncoming, prn = PacketHandler) #,store=0
    
    #time.sleep(1)
    #Start the SNIFF and Forward/Log Sniffed Packets
    #comTcpDump = sniff(offline=pcap_dump, iface=host_interface, lfilter = isIncoming, prn = PacketHandler) #,store=0
    
    
    pass
    return

def launch_sniffer():
    try:
        t = Thread(group=None,target=start_sniffer,name="SNIFFER", args=(), kwargs={})
        t.start()
        #t.join()
        pass
    except Exception, e:
        print "Exception:", str(e)
    return

def start_itg_recv():
    global host_interface
    global host_ip_addr
    log_file = host_ip_addr + '_recv_itglog.txt'
    log_file = os.path.join(rdir_p,log_file)
    #log_file  = rdir_p + host_ip_addr + '_recv_itglog.txt'
    #cmd = 'ITGRecv ' + '-l ' + log_file
    cmd = 'ITGRecv '
    #if not os.path.isfile(flows_file):
    #    return -1
    #./ITGSend <script_file> [log_opts]
    #print cmd
    os.system(cmd)
    return 0
def launch_itg_recv():
    try:
        t = Thread(group=None,target=start_itg_recv,name="ITGRECV", args=(), kwargs={})
        t.start()
        #t.join()
        pass
    except Exception, e:
        print "Exception:", str(e)
    return
def writeRecvdDataInfo(dlen=0,from_addr=""):
    global rx_flows_file
    with open(rx_flows_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), dlen, from_addr]])
    return

def start_udp_sock_server():
    global host_ip_addr
    
    # Create a UDP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the socket to the port
    server_address = (host_ip_addr, UDP_SERVER_PORT)
    print >>sys.stderr, 'starting up on %s port %s' % server_address
    sock.bind(server_address)

    while True:
        print >>sys.stderr, '\nwaiting to receive message'
        data, address = sock.recvfrom(UDP_DGRAM_SIZE)
    
        writeRecvdDataInfo(dlen=len(data),from_addr=address)
        #print >>sys.stderr, '[%s] received %s bytes from %s' % ( time.time(), len(data), address)
        print >>sys.stderr, data
    
        #if data:
            #data = "Hi" + data + '!'
            #sent = sock.sendto(data, address)
            #print >>sys.stderr, 'sent %s bytes back to %s' % (sent, address)
            #pass
        
        #if not data: 
        #    break
    
    return

def launch_udp_server():
    try:
        t = Thread(group=None,target=start_udp_sock_server,name="UDPSERVER", args=(), kwargs={})
        t.start()
        #t.join()
        pass
    except Exception, e:
        print "Exception:", str(e)
    return

def start_nc_server():
    global host_interface
    global host_ip_addr
    NC_UDP_SERVER_PORT = UDP_SERVER_PORT + 10
    NC_TCP_SERVER_PORT = TCP_SERVER_PORT + 10
    ncs_dump= rdir_p + 'nc_dump' + str(host_ip_addr) + '-' + str(host_interface)
    ncs_dump_udp = ncs_dump + str(NC_UDP_SERVER_PORT) + '.rawtxt'

    cmd = 'nc -k -u -l ' + str(NC_UDP_SERVER_PORT) + ' > ' + ncs_dump_udp + ' &'
    print cmd
    os.system(cmd)

    ncs_dump_tcp = ncs_dump + str(NC_TCP_SERVER_PORT) + '.rawtxt'
    cmd = 'nc -k -l ' + str(NC_TCP_SERVER_PORT) + ' > ' + ncs_dump_tcp + ' &'
    print cmd
    os.system(cmd)

def start_iperf_server():
    global host_interface
    global host_ip_addr
    IP_UDP_SERVER_PORT = UDP_SERVER_PORT + 20
    IP_TCP_SERVER_PORT = TCP_SERVER_PORT + 20
    ncs_dump= rdir_p + 'ipfs_dump' + str(host_ip_addr) + '-' + str(host_interface)
    ncs_dump_udp = ncs_dump + str(IP_UDP_SERVER_PORT) + '.rawtxt'

    cmd = 'iperf -s -u -p ' + str(IP_UDP_SERVER_PORT) + ' > ' + ncs_dump_udp + ' &'
    print cmd
    os.system(cmd)

    ncs_dump_tcp = ncs_dump + str(IP_TCP_SERVER_PORT) + '.rawtxt'
    cmd = 'iperf -s -p ' + str(IP_TCP_SERVER_PORT) + ' > ' + ncs_dump_tcp + ' &'
    print cmd
    os.system(cmd)
    
def start_tcp_sock_server():
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Bind the socket to the port
    server_address = (host_ip_addr, TCP_SERVER_PORT)
    print >>sys.stderr, 'starting up on %s port %s' % server_address
    sock.bind(server_address)
    
    # Listen for incoming connections
    sock.listen(1)
    
    while True:
        # Wait for a connection
        print >>sys.stderr, 'waiting for a connection'
        connection, client_address = sock.accept()
        try:
            print >>sys.stderr, 'connection from', client_address
            # Receive the data in small chunks and retransmit it
            while True:
                data = connection.recv(TCP_PACKET_SIZE)
                #print >>sys.stderr, 'received "%s"' % data
                if data:
                    #print >>sys.stderr, 'sending data back to the client'
                    writeRecvdDataInfo(dlen=len(data),from_addr=client_address)
                    #connection.sendall(data)
                    pass
                else:
                    print >>sys.stderr, 'no more data from', client_address
                    break
        finally:
            # Clean up the connection
            connection.close()

def launch_tcp_server():
    try:
        #print "Launching UDP server Thread"
        #thread.start_new_thread( start_tcp_sock_server, ("UDPSERVER", 10)) #OLD MODE
        #New Approach
        t = Thread(group=None,target=start_tcp_sock_server,name="TCPSERVER", args=(), kwargs={})
        t.start()
        #t.join()
        pass
    except Exception, e:
        print "Exception:", str(e)
    return
        
def example_tcp_scapy_web_server():
    # https://akaljed.wordpress.com/2010/12/12/scapy-as-webserver/
    #!/usr/bin/python
    #from scapy.all import *

    # Interacts with a client by going through the three-way handshake.
    # Shuts down the connection immediately after the connection has been established.
    # Akaljed Dec 2010, http://www.akaljed.wordpress.com

    # Wait for client to connect.
    a=sniff(count=1,filter="tcp and host 192.168.1.1 and port 80")

    # some variables for later use.
    ValueOfPort=a[0].sport
    SeqNr=a[0].seq
    AckNr=a[0].seq+1

    # Generating the IP layer:
    ip=IP(src="192.168.1.1", dst="192.168.1.2")
    # Generating TCP layer:
    TCP_SYNACK=TCP(sport=80, dport=ValueOfPort, flags="SA", seq=SeqNr, ack=AckNr, options=[('MSS', 1460)])

    #send SYNACK to remote host AND receive ACK.
    ANSWER=sr1(ip/TCP_SYNACK)

    # Capture next TCP packets with dport 80. (contains http GET request)
    GEThttp = sniff(filter="tcp and port 80",count=1,prn=lambda x:x.sprintf("{IP:%IP.src%: %TCP.dport%}"))
    AckNr=AckNr+len(GEThttp[0].load)
    SeqNr=a[0].seq+1

    # Print the GET request
    # (Sanity check: size of data should be greater than 1.)
    if len(GEThttp[0].load)>1: print GEThttp[0].load

    # Generate custom http file content.
    html1="HTTP/1.1 200 OK\x0d\x0aDate: Wed, 29 Sep 2010 20:19:05 GMT\x0d\x0aServer: Testserver\x0d\x0aConnection: Keep-Alive\x0d\x0aContent-Type: text/html; charset=UTF-8\x0d\x0aContent-Length: 291\x0d\x0a\x0d\x0a<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\"><html><head><title>Testserver</title></head><body bgcolor=\"black\" text=\"white\" link=\"blue\" vlink=\"purple\" alink=\"red\"><p><font face=\"Courier\" color=\"blue\">-Welcome to test server-------------------------------</font></p></body></html>"

    # Generate TCP data
    data1=TCP(sport=80, dport=ValueOfPort, flags="PA", seq=SeqNr, ack=AckNr, options=[('MSS', 1460)])

    # Construct whole network packet, send it and fetch the returning ack.
    ackdata1=sr1(ip/data1/html1)
    # Store new sequence number.
    SeqNr=ackdata1.ack

    # Generate RST-ACK packet
    Bye=TCP(sport=80, dport=ValueOfPort, flags="FA", seq=SeqNr, ack=AckNr, options=[('MSS', 1460)])

    send(ip/Bye)
    # The End
    return

def svc_tracker():
    dict_file_name= rdir_p + 'ip_svc_info.csv'
    read_to_dict_from_csv(dict_file_name)
    return
def start_svc_tracker():
    counter = 0
    c_time = 0
    while True:
        #svc_tracker()
        time.sleep(5)
        advertise_host()
        #write_svc_utility_to_file(c_time)
        #write_streams_to_file()
        c_time +=2
        #counter+=1
        #if counter == 5:
          #write_streams_to_file()
          ##advertise_host()
          #counter = 0
    return
    
def launch_svc_tracker():
    try:
        t = Thread(group=None,target=start_svc_tracker,name="SVCTRACK", args=(), kwargs={})
        t.start()

        pass
    except Exception, e:
        print "Exception:", str(e)
    return
    
import iface_usage_details as if_usage
def start_link_monitor():
    #global host_interface
    #global host_ip_addr
    
    #Start the monitor for the link of host_interface
    #subprocess.Popen(['./link_bandwidth_monitor.sh', str(host_interface)], shell=False)
    #log_file = rdir_p + str(host_interface) + "iface_usage_details.csv"
    if_usage.start_if_monitor(iface_name=host_interface, interval_in_sec=5,log_file=of_usage_log_file)
    pass
    return

def launch_link_monitor():
    try:
        disable_arp()
       # while(start_link_monitor is False): time.sleep(1)
        start_link_monitor()
        time.sleep(300)
        pass
    except Exception, e:
        print "Exception:", str(e)
    return

import signal
import sys
def signal_handler(signal, frame):
    print "Exitting Local Swith and controlelr"
    teardown_host_local_ovs_and_controller()
    #print('You pressed Ctrl+C!')
    sys.exit(0)
def setup_singal_handler():
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    signal.signal(signal.SIGABRT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    #signal.signal(signal.SIGKILL, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)
    #print('Press Ctrl+C')
    #signal.pause()
if __name__ == '__main__':
    
    parse_args()
    time.sleep(2)
    getInterfaceInfo()
    time.sleep(2)
    configure_host()
    time.sleep(2)
    advertise_host()
    time.sleep(2)
    setup_singal_handler()
    time.sleep(2)
    #setup_local_ovs_switch_and_controller()
    #time.sleep(2)
    
    #launch_sniffer()
    #time.sleep(2)
    
    launch_itg_recv()
    time.sleep(2)
    
    #start_nc_server()
    #start_iperf_server()
    #launch_tcp_server()
    #launch_udp_server()
    
    #launch_svc_tracker()
    #launch_link_monitor()
    
    start_svc_tracker()


