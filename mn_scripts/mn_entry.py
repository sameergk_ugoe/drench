#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.topolib import TreeTopo
from mininet.node import RemoteController
from mininet.log import setLogLevel
from mininet.cli import CLI
from mininet.link import TCLink
import csv
import os.path
import random
from os.path import expanduser
hdir_p = expanduser('~')
hdir_p = hdir_p + '/drench/topologies'

MAX_SWITCHES = 5 #128 #23 #315 #127 #200 #79 #40 #319  #5
MAX_HOSTS    = 6 #10 #5 #128 #5 #10 #5 #46   #125

# Small Default Testbed Topology
topo_file = hdir_p + '/demotopo.csv'
#topo_file = hdir_p + '/demotopo2.csv'  #no loop version

# Topology for service reversal
#topo_file = hdir_p + '/demotopo3.csv'

#Med EU RocketFuel Topology  50 Hosts
#topo_file = hdir_p + '/latencies_1755_topo_tree.csv'

#Med EU RocketFuel Topology  30 Hosts
#topo_file = hdir_p + '/latencies_1755_topo_tree_30.csv'

#DataCenter Tree Topology 30 Hosts
#topo_file = hdir_p + '/tree_topo.csv'

#Argyrios Evaualtions for Probabilisitc Vs Real <Note to revert the # at Random.shuffle
#topo_file = hdir_p + '/demotopo_argi.csv'

#Large RF Topology 1239
#topo_file = hdir_p + '/latencies_1239_topo_tree.csv'

#Garcia Topology
#topo_file = hdir_p + '/garcia_topo_topo_tree.csv'

#topo_file = hdir_p + '/sm_eu_rftopo.csv'
#topo_file = hdir_p + '/rftopo_tree.csv'

#Host to Switch Link File <Hid, Swid>
# Small Default Testbed Topology
link_file = hdir_p + '/host_link_demotopo.csv'

# Topology for service reversal
#link_file = hdir_p + '/host_link_demotopo3.csv'

#Med EU RocketFuel Topology  30 Hosts
#link_file = hdir_p + '/host_link_latencies_1755_topo_tree_30.csv'

#DataCenter Tree Topology 30 Hosts
#link_file = hdir_p + '/host_link_tree_topo.csv'

#Argyrios Evaualtions for Probabilisitc Vs Real <Note to revert the # at Random.shuffle
#link_file = hdir_p + '/host_link_demotopo_argi.csv'

def build_topo():
    "Construct Topology from CSV File (Use Small Scale and Large Scale)"
    topo = Topo(controller=None)

  if(os.path.isfile(topo_file)):
    pass
  else:
    print "No File Found! Return!!"
    return

  #Get the Switch Count and Host Count from CSV File Header
  with open(topo_file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    index = 0
    for row in reader:
      try:
        if index == 0 and len(row) >=4:
          MAX_SWITCHES = int(row[3].split(':')[1])
        if index == 0 and len(row) >=5:
          MAX_HOSTS    = int (row[4].split(':')[1])
        break
      except Exception,e:
        print " Failed to read header,", str(e), "Defaults for Max Switches [%d] and Max Hosts [%d]" %(MAX_SWITCHES, MAX_HOSTS)

  # Add switches
  switchlist = [topo.addSwitch('s%i' %i) for i in range(1,MAX_SWITCHES+1)]
  print "\n", switchlist, "\n"
  
  # Add nodes
  hostlist = [topo.addHost('h%i' %i, ip='10.0.0.%i' %i ) for i in range(1,MAX_HOSTS+1)]
  print "\n", hostlist, "\n"

  # Add Switch to Host Links
  if(os.path.isfile(link_file)):
    connlist = []
    with open(link_file, 'rb') as csvfile:
      reader = csv.DictReader(csvfile)
      #linkopts = {'delay':'6ms', 'use_htb':True}
      link_count = 0
      for row in reader:
        sw1 = int(row['Host'])
        sw2 = int(row['Switch'])
        wt  = int(row['Weight'])
        print "Host:%i, Switch:%i, Wt:%i" %(sw1,sw2,wt)
        if( ('h%i' %sw1 in hostlist) and ('s%i' %sw2 in switchlist) and 
            (((sw1,sw2) not in connlist) and ((sw2,sw1) not in connlist))):
          
          topo.addLink('h%i'%sw1,'s%i' %sw2)
          #topo.addLink('s%i'%sw1,'s%i' %sw2,**linkopts)
          #topo.addLink('s%i'%sw1,'s%i' %sw2, delay='%ims' %wt)
          #topo.addLink('s%i'%sw1,'s%i' %sw2, bw=wt)

          link_count = link_count+1
          connlist.append((sw1,sw2))
          print(" Added: Host:%d, Switch:%d, Wt:%d",sw1,sw2,wt)
        else:
          #print(" Skipped: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
          pass
      print "[%i] Number of Host to Switch Links Created" %link_count
  else:
    #Randomly insert the links 
    sws = range(1,MAX_SWITCHES+1)
    #random.shuffle(sws)
    for i in range(1,MAX_HOSTS+1):
      topo.addLink('s%i'%sws[((i-1)%MAX_SWITCHES)],'h%i' %i)
    
  # Add Switch to Switch Links
  connlist = []
  with open(topo_file, 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    linkopts = {'delay':'6ms', 'use_htb':True}
    link_count = 0
    for row in reader:
      sw1 = int(row['Source'])
      sw2 = int(row['Destination'])
      wt  = int(row['Weight'])
      print "SW1:%i, SW2:%i, Wt:%i" %(sw1,sw2,wt)
      if( ('s%i' %sw1 in switchlist) and ('s%i' %sw2 in switchlist) and 
          (((sw1,sw2) not in connlist) and ((sw2,sw1) not in connlist))):
        
        #topo.addLink('s%i'%sw1,'s%i' %sw2,**linkopts)
        topo.addLink('s%i'%sw1,'s%i' %sw2, delay='%ims' %wt)
        #topo.addLink('s%i'%sw1,'s%i' %sw2, bw=wt)

        link_count = link_count+1
        connlist.append((sw1,sw2))
        print(" Added: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
      else:
        #print(" Skipped: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
        pass
    print "[%i] Number of Unique Links Created" %link_count
    return topo
    
def demo():
  "Construct Mininet topo "
  
  topo = build_topo()
  net = Mininet(topo=topo, controller=None, link=TCLink, autoSetMacs = True, autoStaticArp = True, cleanup = True)
  c1 = net.addController(name = 'c1', controller = RemoteController, port = 6633 )
  #c1 = net.addController(name = 'c1', controller = RemoteController, ip = '172.23.0.33', port = 6633 )

  for i, h in enumerate(net.hosts):
    mac = '00:00:00:00:00:00'
    if i <9: 
      mac = '00:00:00:00:00:0%i' %(i+1) 
    elif i < 99: 
      mac = '00:00:00:00:00:%i' %(i+1)
    else:
      k = (i+1)/100
      j = ((i+1)%100)/10
      l = ((i+1)%100)%10
      mac = '00:00:00:00:0%i:%i%i' %(k,j,l)
    h.setMAC(mac)
    print "Host ", h.name, "has IP address", h.IP(), "and MAC address", h.MAC(), mac
    
  # Start network
  #net.build()
  c1.start()
  
  #for s in net.switches:
    #print s
    #s.start([c1])
  
  net.start()
  #net.pingAll()
  
  #print "\n Hosts:"
  for h in net.hosts[0:MAX_HOSTS]:
    h.cmd("sysctl -w net.ipv6.conf.all.disable_ipv6=1")
    h.cmd("sysctl -w net.ipv6.conf.default.disable_ipv6=1")
    h.cmd("sysctl -w net.ipv6.conf.lo.disable_ipv6=1")
    h.cmdPrint('sudo python -u hp_sniff_sk.py >/dev/null 2>&1', '&')
    
  CLI( net )
  net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' ) # Other Optioms 'debug', 'info','output','warning','error','critical'
    demo()
