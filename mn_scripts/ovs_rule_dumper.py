import subprocess
import commands
import csv
import time
import sys
import os
from os.path import expanduser
hdir_p = expanduser('~')
MAX_ITERATIONS = 5
INTERVAL_IN_SECONDS = 1
MAX_SWITCHES = 87
flow_counter_logfile = hdir_p + '/drench/results/ovs_rules_count.csv'
STOP_FLAG = False
start_time = None
def process_output_and_log(log_file=flow_counter_logfile):
  global start_time
  def write_pkt(cur_time = 0, total_num_of_flows=0,total_vlan_flows=0,vlan_rule_count=0):
    with open(log_file, 'ab') as csvfile:
      rxWriter = csv.writer(csvfile,delimiter=',')
      rxWriter.writerows([[str(cur_time), str(total_num_of_flows), str(total_vlan_flows), str(vlan_rule_count)]])
    return
  if not os.path.isfile('all_flowcount.txt'): return
  
  cur_time = int(time.time())
  if start_time is None: start_time = cur_time
  cur_time = cur_time - start_time
  try:
      with open('all_flowcount.txt', "r") as f:
        total_rules = int(f.readline().strip('\n'))
        os.remove('all_flowcount.txt')
      with open('all_vlan_flowcount.txt', "r") as f:
        total_vlan_rules = int(f.readline().strip('\n'))
        os.remove('all_vlan_flowcount.txt')
      with open('only_vlan_flow_count.txt', "r") as f:
        only_vlan_rules = int(f.readline().strip('\n'))
        os.remove('only_vlan_flow_count.txt')
      write_pkt(cur_time = cur_time, total_num_of_flows=total_rules,total_vlan_flows=total_vlan_rules,vlan_rule_count=only_vlan_rules)
  except: pass
  return

def stopLogging():
    global STOP_FLAG
    STOP_FLAG = True
def startLogging(num_of_iterations=MAX_ITERATIONS, interval=INTERVAL_IN_SECONDS, num_of_switches=MAX_SWITCHES, log_file=flow_counter_logfile):
  
  # Create subprocess to execute command
  #comTcpDump = "./log_switch_flow_count.sh" + str(num_of_switches)
  #p = subprocess.Popen(comTcpDump, stdout=subprocess.PIPE, shell=True)
  if os.path.isfile(log_file):
    os.remove(log_file)
    os.mknod(log_file)
    
  if num_of_iterations: 
    for x in range(num_of_iterations):
      subprocess.Popen(['./log_switch_flow_count.sh', str(num_of_switches)], shell=False)
      time.sleep(interval)
      process_output_and_log(log_file)
  else: 
    while True:
      if STOP_FLAG is True: return
      subprocess.Popen(['./log_switch_flow_count.sh', str(num_of_switches)], shell=False)
      time.sleep(interval)
      process_output_and_log(log_file)
      if STOP_FLAG is True: return
  
  return
  
if __name__ == '__main__':
  if(len(sys.argv)>1):
    num_switches = int(sys.argv[1])
  else:
    num_switches = MAX_SWITCHES
  startLogging(num_of_iterations=100, interval=5, num_of_switches=num_switches)
