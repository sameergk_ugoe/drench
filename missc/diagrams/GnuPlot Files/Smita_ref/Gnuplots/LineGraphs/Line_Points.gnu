set terminal windows size 1200, 800 enhanced font ",16"
#set terminal png size 1200, 800 enhanced font ",16"
script_name=ARG0
set output script_name."_fig.png"

set datafile separator comma
set key right bottom autotitle columnhead
set key box lt -1 lw 2
#set key left bottom Left title 'Legend' box 3
#set autoscale fix
set title 'TCP UDP mix'
set ylabel 'Throughput in Mbps'
set y2label 'Throughput in Mbps'
set xlabel 'Time in seconds'


set xrange [0:6]
set xtics (0,1,2,3,4,5,6)
#set log y
unset logscale y
set yrange [0:1]
set y2range [0:1]
set ytics (0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1)
set y2tics (0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1)
#set xrange auto
#set yrange auto

#color definitions
set border linewidth 1.5
set style line 1 lc rgb '#99FF33' lt 1 lw 2 pt 7 ps 1.5 # --- green
set style line 2 lc rgb '#dd181f' lt 1 lw 2 pt 7 ps 1.5 # --- red
set style line 3 lc rgb '#3366ff' lt 1 lw 2 pt 7 ps 1.5 # --- blue

plot 'Line_PointsCSV.csv' using 1:2 with lines ls 1, \
     '' using 1:3 with lines ls 2, \
     '' using 1:4 with lines ls 3, \
     'Line_PointsCSV.csv' using 1:5 with points axes x1y2 ls 1, \
     'Line_PointsCSV.csv' using 1:6 with points axes x1y2 ls 2, \
     'Line_PointsCSV.csv' using 1:7 with points axes x1y2 ls 3