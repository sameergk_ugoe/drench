set terminal windows size 1200, 800 enhanced font ",16"
#set terminal png size 1200, 800 enhanced font ",16"
script_name=ARG0
set output script_name."_fig.png"

set datafile separator comma
set key right bottom autotitle columnhead
set key box lt -1 lw 2
#set key left bottom Left title 'Legend' box 3
#set autoscale fix
set title 'TCP UDP mix'
set ylabel 'Throughput in Mbps'
set xlabel 'Time in seconds'


set xrange [5:60]
set xtics (5,10,15,20,25,30,35,40,45,50,55,60)
set log y
set yrange [1:10000]
set ytics ("1" 1,"10" 10,"100" 100,"1000" 1000,"10000" 10000)
#set xrange auto
#set yrange auto

#color definitions
set border linewidth 1.5
set style line 1 lc rgb '#99FF33' lt 1 lw 2 pt 5 ps 1.5 # --- green
set style line 2 lc rgb '#dd181f' lt 1 lw 2 pt 1 ps 1.5 # --- red
set style line 3 lc rgb '#3366ff' lt 1 lw 2 pt 2 ps 1.5 # --- blue
set style line 4 lc rgb '#ff6600' lt 1 lw 2 pt 3 ps 1.5 # --- orange

plot 'MultilineCSV.csv' using 1:2 with linespoints ls 1, \
     '' using 1:3 with linespoints ls 2,    \
     '' using 1:4 with linespoints ls 3,    \
     'MultilineCSV.csv' using 1:5 with linespoints ls 4