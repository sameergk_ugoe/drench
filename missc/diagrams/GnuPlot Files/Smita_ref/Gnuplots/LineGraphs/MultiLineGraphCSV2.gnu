set terminal windows size 1200, 800 enhanced font ",16"
#set terminal png size 1200, 800 enhanced font ",16"
script_name=ARG0
set output script_name."_fig.png"

set datafile separator comma
set key right bottom autotitle columnhead
set key box lt -1 lw 2
#set key left bottom Left title 'Legend' box 3
#set autoscale fix
set title 'Normalized Throughput Plots'
set ylabel 'Normalized Throughput'
set xlabel 'Number of Multiplexed Flows per NF'


set xrange [1:6]
set xtics (1,2,3,4,5,6)
#set log y
unset logscale y
set yrange [0:1]
set ytics (0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1)
#set xrange auto
#set yrange auto

#color definitions
set border linewidth 1.5
set style line 1 lc rgb '#3366ff' lt 1 lw 2 pt 7 ps 1.5 # --- blue
set style line 2 lc rgb '#99FF33' lt 1 lw 2 pt 7 ps 1.5 # --- green
set style line 3 lc rgb '#ffff00' lt 1 lw 2 pt 7 ps 1.5 # --- yellow
set style line 4 lc rgb '#ff6600' lt 1 lw 2 pt 7 ps 1.5 # --- orange
set style line 5 lc rgb '#dd181f' lt 1 lw 2 pt 7 ps 1.5 # --- red
set style line 6 lc rgb '#660066' lt 1 lw 2 pt 7 ps 1.5 # --- violet

#plot 'MultilineCSV2.csv' using 1:2 smooth csplines with points ls 1, \
 #    '' using 1:3 smooth csplines with linespoints ls 2,    \
  #   '' using 1:4 smooth csplines with linespoints ls 3,    \
   #  '' using 1:5 smooth csplines with linespoints ls 4, \
    # '' using 1:6 smooth csplines with linespoints ls 5, \
     #'MultilineCSV2.csv' using 1:7 smooth csplines with linespoints ls 6
     
     
     
     plot 'MultilineCSV2.csv' using 1:2 notitle with points ls 1, \
     "" using 1:2 notitle smooth csplines with lines ls 1, \
     1 / 0 title "CSV" with linespoints ls 1, \
     '' using 1:3 notitle with points ls 2, \
     "" using 1:3 notitle smooth csplines with lines ls 2, \
     1 / 0 title "BATCH" with linespoints ls 2, \
     '' using 1:4 notitle with points ls 3, \
     "" using 1:4 notitle smooth csplines with lines ls 3, \
     1 / 0 title "RR" with linespoints ls 3, \
     '' using 1:5 notitle with points ls 4, \
     "" using 1:5 notitle smooth csplines with lines ls 4, \
     1 / 0 title "CFS-BKPR" with linespoints ls 4, \
     '' using 1:6 notitle with points ls 5, \
     "" using 1:6 notitle smooth csplines with lines ls 5, \
     1 / 0 title "BATCH-BKPR" with linespoints ls 5, \
     'MultilineCSV2.csv' using 1:7 notitle with points ls 6, \
     "" using 1:7 notitle smooth csplines with lines ls 6, \
     1 / 0 title "RR-BKPR" with linespoints ls 6, \
     'MultilineCSV2.csv' using 1:8 title "CSF-THRTL" with points ls 1, \
     'MultilineCSV2.csv' using 1:9 title "BATCH-THRTL" with points ls 2, \
     'MultilineCSV2.csv' using 1:10 title "RR-THRTL" with points ls 3
     
     
     
     
     
 #    plot "file" using 1:3 notitle with points linestyle 1, \
  #   "" using 1:3 notitle smooth csplines with lines linestyle 1, \
   #  1 / 0 title "title" with linespoints linestyle 1
