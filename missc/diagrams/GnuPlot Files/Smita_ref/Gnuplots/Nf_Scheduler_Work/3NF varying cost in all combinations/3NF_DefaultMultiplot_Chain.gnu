reset
#set terminal windows size 1200, 800 enhanced font ",12"
set datafile separator comma 	

set multiplot layout 2,3 title "Throughput Plots"
set xtics rotate
set key left top autotitle columnhead
#set key box lt -1 lw 3 
set bmargin 3


alerts = 'CFS CFS-BKPR BATCH BATCH-BKPR RR/FIFO RR/FIFO-BKPR'
index(s) = words(substr(alerts, 0, strstrt(alerts, s)-1)) + 1


#set linetype 1 fc rgb 'orange'
#set linetype 2 fc rgb 'red'
#set linetype 3 fc rgb 'yellow'
#set linetype 4 fc rgb 'green'
#set linetype 5 fc rgb 'blue'
#set linetype 6 fc rgb 'pink'

#plot 'alerts.txt' using 0:2:(index(strcol(1))):xtic(1) with boxes linecolor variable


set title "No-Med-High"
set style data histogram
set style fill solid 1.0 border -1
set boxwidth 2
set ylabel 'Throughput in Mpps'
unset xlabel
unset xtics
set ytic 0.5
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot '3NF_DefaultMultiplot_ChainCSV.csv' using 2:xtic(1) notitle fc rgb '#ff6600'
#
#
set title "No-High-Med"
set style data histogram
set style fill solid 1.0 border -1
set boxwidth 2
unset xtics
unset ylabel
unset xlabel
#unset ytics
set ytic 0.5
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot '3NF_DefaultMultiplot_ChainCSV.csv' using 3:xtic(1) notitle fc rgb '#80ff00'
#
#
set title "High-No-Med"
set style data histogram
set style fill solid 1.0 border -1
set boxwidth 2
unset xtics
unset ylabel
unset xlabel
#unset ytics
set ytic 0.5
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot '3NF_DefaultMultiplot_ChainCSV.csv' using 4:xtic(1) notitle fc rgb '#3333cc'
#
#
set title "High-Med-No"
set style data histogram
set style fill solid 1.0 border -1
set boxwidth 2
set xtics rotate
set ytic 0.5
set ylabel 'Throughput in Mpps'
set xlabel 'Different Schedulers'
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot '3NF_DefaultMultiplot_ChainCSV.csv' using 5:xtic(1) notitle fc rgb '#660033'
#
#
set title "Med-No-High"
set style data histogram
set style fill solid 1.0 border -1
set boxwidth 2
set xtics rotate
unset ylabel
set xlabel 'Different Schedulers'
#unset ytics
set ytic 0.5
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot '3NF_DefaultMultiplot_ChainCSV.csv' using 6:xtic(1) notitle fc rgb '#ff80d5'
#
#
set title "Med-High-No"
set style data histogram
set style fill solid 1.0 border -1
set boxwidth 2
set xtics rotate
unset ylabel
set xlabel 'Different Schedulers'
#unset ytics
set ytic 0.5
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot '3NF_DefaultMultiplot_ChainCSV.csv' using 7:xtic(1) notitle fc rgb '#ffff00'
#
unset multiplot