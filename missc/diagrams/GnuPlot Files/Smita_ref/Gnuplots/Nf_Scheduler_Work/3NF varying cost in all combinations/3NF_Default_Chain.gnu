set terminal windows size 1200, 800 enhanced font ",16"
#set terminal png size 1200, 800 enhanced font ",16"
#script_name=ARG0
#set output script_name."_fig.png"

set datafile separator comma
set key right bottom autotitle columnhead title 'Legend'
set key box lt -1 lw 2 
set key font ",14"
#set key left bottom Left title 'Legend' box 2
#set autoscale fix
set title 'Throughput Plots'
set ylabel 'Throughput'
set xlabel 'Chain Types'


#set xrange [1:128]
#set xtics (1,2,10,50,100,128)
#set log y
unset logscale y
set yrange [0:3]
set ytics (0,1,2,3)
set auto x

#set yrange auto

#color definitions
set border linewidth 1.5
set style line 1 lc rgb '#3366ff' lt 1 lw 2 pt 7 ps 1.5 # --- blue
set style line 2 lc rgb '#99FF33' lt 1 lw 2 pt 7 ps 1.5 # --- green
set style line 3 lc rgb '#ffff00' lt 1 lw 2 pt 7 ps 1.5 # --- yellow
set style line 4 lc rgb '#ff6600' lt 1 lw 2 pt 7 ps 1.5 # --- orange
set style line 5 lc rgb '#dd181f' lt 1 lw 2 pt 7 ps 1.5 # --- red
set style line 6 lc rgb '#660066' lt 1 lw 2 pt 7 ps 1.5 # --- violet

plot '3NF_Default_ChainCSV.csv' using 2:xticlabels(1) with linespoints ls 1, \
     '' using 3:xticlabels(1) with linespoints ls 2, \
     '' using 4:xticlabels(1) with linespoints ls 3, \
     '' using 5:xticlabels(1) with linespoints ls 4, \
     '' using 6:xticlabels(1) with linespoints ls 5, \
     '' using 7:xticlabels(1) with linespoints ls 6, \
     
     

     
     
     
     
     
 #    plot "file" using 1:3 notitle with points linestyle 1, \
  #   "" using 1:3 notitle smooth csplines with lines linestyle 1, \
   #  1 / 0 title "title" with linespoints linestyle 1
