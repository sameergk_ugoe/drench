reset
set terminal windows size 1200, 800 enhanced font ",16"
set datafile separator comma
set key right bottom autotitle columnhead title 'Legend'
set key box lt -1 lw 2 
set key font ",14"
#set key left bottom Left title 'Legend' box 2
#set autoscale fix
set title 'Throughput Plots'
set ylabel 'Throughput'
set xlabel 'Chain Types'

unset logscale y
#set yrange [0:3]
#set ytics (0,1,2,3)
set auto x
set auto y

set style data histogram
#set style histogram cluster gap 2
set style fill solid 1.0 border -1
set boxwidth 0.5
#set xtic scale 0
#set ytics (0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7)
set ytic 0.2
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
#plot 'BarGraphHistogramData2.csv' using 2:xtic(1) ti col fc rgb '#0099ff', '' u 3 ti col fc rgb '#009900'


plot '3NF_Default_ChainCSV.csv' using 2:xtic(1) ti col fc rgb '#dfeaf1', \
     '' using 3 ti col fc rgb '#87a7f8', \
     '' using 4 ti col fc rgb '#002794', \
     '' using 5 ti col fc rgb '#f1e6df', \
     '' using 6 ti col fc rgb '#730d62', \
     '' using 7 ti col fc rgb '#45083b'