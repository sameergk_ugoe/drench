set terminal windows size 1200, 800 enhanced font ",16"
#set terminal png size 1200, 800 enhanced font ",16"
#script_name=ARG0
#set output script_name."_fig.png"

set datafile separator comma
set key right bottom autotitle columnhead title 'Legend'
set key box lt -1 lw 2 
set key font ",14"
#set key left bottom Left title 'Legend' box 3
set autoscale fix
set title 'AES Encrypt and Decrypt'
set ylabel 'Mpps'
set y2label 'Mbps'
set xlabel 'Packet Size (bytes)'
set key opaque
#set style fill transparent solid 0.5

set xrange [64:1500]
set xtics (64,128,256,512,1024,1500)
set log y
#unset logscale y
set yrange [0.01:1]
set y2range [0:550]
set ytics (0.01,0.1,1)
set y2tics (0,50,100,150,200,250,300,350,400,450,500,550)
#set xrange auto
#set yrange auto

set style data histogram
set style histogram cluster gap 2
set style fill solid 1.0 border -1
set boxwidth 0.9

#color definitions
#set border linewidth 1.5
set style line 1 lc rgb '#99FF33' lt 1 lw 2 pt 7 ps 1.5 # --- green
set style line 2 lc rgb '#dd181f' lt 1 lw 2 pt 7 ps 1.5 # --- red
set style line 3 lc rgb '#3366ff' lt 1 lw 2 pt 7 ps 1.5 # --- blue

plot '1NF_AES_Encrypt_DecryptCSV.csv' using 1:2 with linespoints ls 3, \
     '1NF_AES_Encrypt_DecryptCSV.csv' using 3:xtic(1) ti col axes x1y2 fc rgb '#99FF33'


#plot 'Line_BarCSV.csv' using 1:2 with lines ls 1, \
 #    '' using 1:3 with lines ls 2, \
  #   '' using 1:4 with lines ls 3, \
   #  'Line_BarCSV.csv' using 5:xtic(1) ti col axes x1y2 fc rgb '#3366ff', '' u 6 ti col axes x1y2 fc rgb '#dd181f'
     #'Line_BarCSV.csv' using 1:5 with boxes axes x1y2 ls 1, \
     #'Line_BarCSV.csv' using 1:6 with boxes axes x1y2 ls 2, \
     #'Line_BarCSV.csv' using 1:7 with boxes axes x1y2 ls 3
     