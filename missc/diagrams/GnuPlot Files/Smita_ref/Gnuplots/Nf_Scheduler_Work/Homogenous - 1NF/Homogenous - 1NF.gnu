reset
#set terminal windows size 1200, 800 enhanced font ",16"
#set datafile separator comma
#set key right bottom autotitle columnhead
#set key box lt -1 lw 2
#set autoscale fix
#set key noinvert box
#set title "Homogenous - 1NF"
#set key invert reverse Left outside
#set key autotitle columnheader
#set yrange [0:60]
#set ytics 1 nomirror
#set auto x

#unset xtics
#set xtics nomirror
#set style data histogram
#set style histogram columnstacked
#set style fill solid 1.0 border -1
#set boxwidth 0.5


#plot 'Homogenous - 1NFCSV.csv' using 2 ti col, '' u 3 ti col, '' u 4:key(1) ti col

set terminal windows size 1200,800
set title "Schedulers and CFS Tuning with Back-pressure"
#W/o Bkpressure = "#0099ff"; With Backpressure = "#009900"
set datafile separator comma
set key left top autotitle columnhead
set key box lt -1 lw 3

set auto x

unset logscale y
#set yrange [0:7]
set auto y
set style data histogram
set style histogram cluster gap 2
set style fill solid 1.0 border -1
set boxwidth 0.9
set xtic scale 0
#set ytics (0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7)
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot 'Homogenous - 1NFCSV.csv' using 2:xtic(1) ti col fc rgb '#0099ff', '' u 3 ti col fc rgb '#009900', ''u 4 ti col fc rgb '#ff0000'