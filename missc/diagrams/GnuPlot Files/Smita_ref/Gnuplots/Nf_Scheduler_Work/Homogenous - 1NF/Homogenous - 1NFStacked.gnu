reset
set terminal windows size 1200,800
set title "Homogenous NFs with multiple NFs and multiple Flows"
#W/o Bkpressure = "#0099ff"; With Backpressure = "#009900"
set xlabel 'Number of Flows'
set ylabel 'Throughput in Mpps'
set datafile separator comma
set key left top 
#autotitle columnhead
#set key box lt -1 lw 3

set auto x

unset logscale y
#set yrange [0:7]
set auto y
set style data histogram
set style histogram rowstacked gap 2
set style fill solid 1.0 border -1
set boxwidth 0.5
set xtic scale 0
set ytic 0.5
#set ytics (0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7)
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'

#plot 'Homogenous - 1NFStackedCSV.csv' using 2:xtic(1) ti col fc rgb '#0099ff', '' u 3 ti col fc rgb '#009900', ''u 4 ti col fc rgb '#ff0000'

plot newhistogram "CFS", \
'Homogenous - 1NFStackedCSV.csv' using 2:xtic(1) title "NF1" fc rgb '#0099ff', '' u 3 title "NF2" fc rgb '#009900', '' u 4 title "NF3" fc rgb '#ff0000', \
newhistogram "Batch", \
'Homogenous - 1NFStackedCSV.csv' using 5:xtic(1) notitle fc rgb '#0099ff', '' u 6 notitle fc rgb '#009900', '' u 7 notitle fc rgb '#ff0000', \
newhistogram "RoundRobin", \
'Homogenous - 1NFStackedCSV.csv' using 8:xtic(1) notitle fc rgb '#0099ff', '' u 9 notitle fc rgb '#009900', '' u 10 notitle fc rgb '#ff0000'
