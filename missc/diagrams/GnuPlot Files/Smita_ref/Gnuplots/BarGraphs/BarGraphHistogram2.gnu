set terminal windows size 1200,800
set title "Schedulers and CFS Tuning with Back-pressure"
#W/o Bkpressure = "#0099ff"; With Backpressure = "#009900"

set key left top autotitle columnhead
set key box lt -1 lw 3

set auto x
unset logscale y
set yrange [0:7]
set style data histogram
set style histogram cluster gap 2
set style fill solid 1.0 border -1
set boxwidth 0.9
set xtic scale 0
set ytics (0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7)
# 2, 3 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot 'BarGraphHistogramData2.csv' using 2:xtic(1) ti col fc rgb '#0099ff', '' u 3 ti col fc rgb '#009900'