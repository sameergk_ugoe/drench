set terminal windows size 1200,800
set title "Benchmarks"
C = "#99ffff"; Cpp = "#4671d5"; Java = "#ff0000"; Python = "#f36e00"
set auto x
set yrange [1:15]
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
set xtic scale 0
set ytics (1,2,15)
# 2, 3, 4, 5 are the indexes of the columns; 'fc' stands for 'fillcolor'
plot 'BarGraphHistogramData.csv' using 2:xtic(1) ti col fc rgb C, '' u 3 ti col fc rgb Cpp, '' u 4 ti col fc rgb Java, '' u 5 ti col fc rgb Python