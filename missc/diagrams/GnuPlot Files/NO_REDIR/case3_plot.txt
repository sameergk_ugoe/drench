#!/usr/bin/gnuplot
reset
#set terminal windows size 2400, 600
set terminal png size 2400, 600
#set terminal png size 1024,768
set output 'case3_.png'
#set terminal pdf
#set output 'case3.pdf'

#set term postscript
#set output "case3.ps"

#set xdata Duration
set xlabel "Time in Seconds"
set xtics 5
#set y2tics 100000

set y2range[-1:10]
#set yrange[-10:150]

set ylabel "Network Utilization in Kbps"
set y2label "Redirections-Path-StretchDeviations"

set ytics nomirror
set y2tics

set title "Service Instance Re-location Scenario"
set datafile separator ","
set autoscale fix
set key outside right center

set style data lines
set style fill solid

set pointsize 0.7

set style line 8 lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
set style line 9 lc rgb 'red' lt 1 lw 2 pt 7 pi -1 ps 1.5
set pointintervalbox 3

set style line 1 lc rgb "red" pt 7
set style line 2 lc rgb "blue" pt 7
set style line 3 lc rgb "green" pt 7
set style line 4 lc rgb "pink" pt 7
set style line 5 lc rgb "black" pt 7

plot "./UDP/log_nw_utilization_info.csv" using 1:($2 < 30 ? 1/0 : $2) w linespoints  title "Network Utilization(Base UDP)" axes x1y1 ls 9, \
"./TCP/log_nw_utilization_info.csv" using 1:($2 < 30 ? 1/0 : $2) w linespoints  title "Network Utilization(Base TCP)" axes x1y1 ls 7, \
"../with_REDIR/UDP/log_nw_utilization_info.csv" using 1:($2 < 30 ? 1/0 : $2) w linespoints  title "Network Utilization(Drench UDP)" axes x1y1 ls 10, \
"../with_REDIR/TCP/log_nw_utilization_info.csv" using 1:($2 < 30 ? 1/0 : $2) w linespoints title "Network Utilization(DrenchTCP)" axes x1y1 ls 3, \
"../with_REDIR/UDP/rdir_log.csv" using 1:($2 == 0 ? 1/0 : $2) w lp  title "Redirection (UDP)" axes x1y2 ls 4, \
"../with_REDIR/TCP/rdir_log.csv" using 1:($2 == 0 ? 1/0 : $2) w lp  title "Redirection (TCP)" axes x1y2 ls 5, \

#plot "./UDP/log_nw_utilization_info.csv" using 1:($2 == 0 ? 1/0 : $2) w linespoints  title "Network Utilization(Base UDP)" axes x1y1 ls 9, \
#"./TCP/log_nw_utilization_info.csv" using 1:($2 == 0 ? 1/0 : $2) w linespoints  title "Network Utilization(Base TCP)" axes x1y1 ls 7, \
#"../with_REDIR/UDP/log_nw_utilization_info.csv" using 1:($2 == 0 ? 1/0 : $2) smooth csplines  title "Network Utilization(Drench)" axes x1y1 ls 10, \
#"../with_REDIR/TCP/log_nw_utilization_info.csv" using 1:($2 == 0 ? 1/0 : $2) smooth csplines  title "Network Utilization(Drench)" axes x1y1 ls 3, \
#"./UDP/related_flows_info.csv" using 1:4 with linespoints title "Path Deviation" axes x1y2 ls 2 , \
#"./UDP/rdir_log.csv" using 1:2 w lp  title "Redirection" axes x1y2 ls 4, \

#convert case1.ps case1.jpg
