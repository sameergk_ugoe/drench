#!/usr/bin/gnuplot
reset
#set terminal windows size 2400, 600
#set terminal png size 2400, 600
#set terminal png size 1024,768

#set terminal png size 800, 600
#set output 'nwl_vs_ps_tcp.pdf'

#set terminal pdf
#set output 'nw_utlization_tcp.pdf'
#set term postscript eps enhanced color 
##set output '|ps2pdf - outputfile.pdf'

set terminal pdf enhanced font ",16"
set output 'Fig_5a.pdf'

#set term postscript
#set output "case3.ps"

#set xdata Duration
set xlabel "Time in Seconds"
set xtics 5 nomirror font ",14" 
set y2tics 1 nomirror font ",14" 

set xrange[60:110]
set y2range[0:2.25]
set yrange[0:45]

set ylabel "Network Load in Mbps"
set y2label "Path Deviation"
 
set ytics nomirror
set y2tics 0,0.25,2.25


#set title "Optimal Service Instance Placement Convergence"
set datafile separator ","
set autoscale fix	
set key inside top left

set style data lines
set style fill solid

set style data lines
set style fill solid

set pointsize 0.7

set style line 1 lc rgb "black" lt 2 lw 2 pt 1 pi -1 ps 1.5
set style line 2 lc rgb "blue" lt 2 lw 2 pt 6 pi -1 ps 1.5
set style line 3 lc rgb "green" pt 11 pi -1 ps 1.5
set style line 4 lc rgb "pink" pt 16 pi -1 ps 1.5
set style line 5 lc rgb "brown" pt 18 pi -1 ps 1.5

set style line 6 lc rgb '#0060ad' lt 2 lw 2 pt 24 pi -1 ps 1.5
set style line 7 lc rgb '#0060ad' lt 2 lw 2 pt 22 pi -1 ps 1.5
set style line 8 lc rgb '#06ad60' lt 2 lw 2 pt 18 pi -1 ps 1.5
set style line 9 lc rgb 'brown' lt 2 lw 2 pt 16 pi -1 ps 1.5
set pointintervalbox 3

samples(x) = $0 > 4 ? 5 : ($0+1)
avg2(x) = (shift2(x), (back1+back2)/samples($0))
avg5(x) = (shift5(x), (back1+back2+back3+back4+back5)/samples($0))
shift5(x) = (back5 = back4, back4 = back3, back3 = back2, back2 = back1, back1 = x)
shift2(x) = (back2 = back1, back1 = x)

#
# Initialize a running sum
init(x) = (back1 = back2 = back3 = back4 = back5 = sum = 0)

# Plot data, running average and cumulative average
xshift(x) = ($0 > 50 ? ($0-10): $0 )


#datafile = 'silver.dat'
#set xrange [0:57]

#plot sum = init(0), \
#     datafile using 0:2 title 'data' lw 2 lc rgb 'forest-green', \
#     '' using 0:(avg5($2)) title "running mean over previous 5 points" pt 7 ps 0.5 lw 1 lc rgb "blue", \
#     '' using 0:(sum = sum + $2, sum/($0+1)) title "cumulative mean" pt 1 lw 1 lc rgb "dark-red"


plot sum = init(0),\
"./DEFAULT_TCP/log_nw_utilization_info.csv" using 1:(avg2($2)) smooth csplines w lines title "Network Load (Baseline)" axes x1y1 ls 9, \
sum = init(0),\
"./DRENCH_TCP/log_nw_utilization_info.csv" using ($1):(avg2($2)) smooth csplines w lines title "Network Load (DRENCH TCP)" axes x1y1 ls 7, \
"./DEFAULT_TCP/related_flows_info.csv" using 1:($4/100) with linespoints title "Path Deviation (Baseline)" axes x1y2 ls 5 , \
"./DRENCH_TCP/related_flows_info.csv" using ($1):($4/100) with linespoints title "Path Deviation (DRENCH)" axes x1y2 ls 6 , \


#"./DRENCH_TCP/rdir_log.csv" using 1:($2 == 0 ? 1/0 : $2) w lp  title "Redirection (TCP)" axes x1y2 ls 8, \


#set terminal x11
#set output
#replot

