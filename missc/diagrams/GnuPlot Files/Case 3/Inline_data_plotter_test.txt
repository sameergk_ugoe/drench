#!/usr/bin/gnuplot
reset
#set terminal windows size 800, 600
#set terminal png size 2400, 600
set terminal png size 1024,768
set output 'inline_case3_.png'

set xlabel "Time in Seconds"
set ylabel "Switch ID with Active NFInstances"
set y2label "Service Placement/Redirections"

set title "Service Instance Optimal Placement/Relocation Scenario"
set datafile separator ","
set autoscale fix
#set key outside right center
set key inside top center

set ytics nomirror
set ytics 1
set y2tics
set xtics 1

set style data lines
set style fill solid

set pointsize 0.7

set style line 1 lc rgb "black" lt 2 lw 2 pt 1 pi -1 ps 1.5
set style line 2 lc rgb "blue" lt 2 lw 2 pt 6 pi -1 ps 1.5
set style line 3 lc rgb "green" pt 11 pi -1 ps 1.5
set style line 4 lc rgb "pink" pt 16 pi -1 ps 1.5
set style line 5 lc rgb "red" pt 18 pi -1 ps 1.5

set style line 6 lc rgb '#0060ad' lt 2 lw 2 pt 24 pi -1 ps 1.5
set style line 7 lc rgb '#06ad60' lt 2 lw 2 pt 22 pi -1 ps 1.5
set style line 8 lc rgb '#6060ad' lt 2 lw 2 pt 18 pi -1 ps 1.5
set style line 9 lc rgb 'red' lt 2 lw 2 pt 16 pi -1 ps 1.5
set pointintervalbox 3


ttl=0
set xrange [57:76]
set yrange [0:4]
set y2range [-1:15]

plot "multi_data.csv" index 0 using 1:(floor($2)) with linespoints title "Network Function A" axes x1y1 ls 9, \
"multi_data.csv" index 1 using 1:(floor($2)) with linespoints title "Network Function B" axes x1y1 ls 8, \
"multi_data.csv" index 2 using 1:(floor($2)) with linespoints title "Network Function C" axes x1y1 ls 7, \
"case_related_flowsinfo.csv" using 1:($4/100) with linespoints title "Avg. Path Deviation" axes x1y2 ls 1 , \
"case_rdir_log.csv" using 1:2 w lp  title "Total # of Redirections" axes x1y2 ls 2, \




