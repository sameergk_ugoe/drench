#!/usr/bin/gnuplot
reset

#set terminal png size 2400, 600
set terminal png size 800, 600
#set terminal png size 1024,768
set output 'redirections_tcp.png'

#set title "Service Instance Optimal Placement/Relocation Scenario"
set datafile separator ","
set autoscale fix
#set key outside right center
set key inside top right

set style data lines
set style fill solid

#set xdata Duration
set xlabel "Time in Seconds"
set xtics 5
set y2tics 1

set xrange[60:90]
set y2range[0:8]
set yrange[0:8]


set pointsize 0.7

set palette defined (0 "blue",1 "green", 2 "red")

set style line 1 lc rgb "black" lt 2 lw 2 pt 1 pi -1 ps 1.5
set style line 2 lc rgb "blue" lt 2 lw 2 pt 6 pi -1 ps 1.5
set style line 3 lc rgb "green" pt 11 pi -1 ps 1.5
set style line 4 lc rgb "pink" pt 16 pi -1 ps 1.5
set style line 5 lc rgb "red" pt 18 pi -1 ps 1.5

set style line 6 lc rgb '#0060ad' lt 2 lw 2 pt 24 pi -1 ps 1.5
set style line 7 lc rgb '#06ad60' lt 2 lw 2 pt 22 pi -1 ps 1.5
set style line 8 lc rgb '#6060ad' lt 2 lw 2 pt 18 pi -1 ps 1.5
set style line 9 lc rgb 'red' lt 2 lw 2 pt 16 pi -1 ps 1.5
set pointintervalbox 3

set xlabel "Time in Seconds"
set ylabel "Service Instance Placement "
set y2label "Path-Stretch Deviation(%) and # of Redirections"

set ytics nomirror
#set ytics 1
set y2tics
set xtics 1
set xtics 5

set grid ytics

ttl=0
set ytics ('s1-1' 1, 's1-2' 2, 's2-1' 3, 's2-2' 4, 's3-1' 5, 's3-2' 6)
#YTICS = "set ytics ('s1-1' 1.1, 's1-2' 1.2, 's2-1' 2.1, 's2-2' 2.2, 's3-1' 3.1, 's3-2' 3.2); \
#         set ylabel Switch ID with Active NFInstances"


#plot lines(h=2) 
#plot lines(h=4) 
#plot lines(h=6) 

plot "./DRENCH_TCP/multi_data_drench.csv" index 0 using 1:2 with linespoints title "Network Function A" axes x1y1 ls 9, \
"./DRENCH_TCP/multi_data_drench.csv" index 1 using 1:2 with linespoints title "Network Function B" axes x1y1 ls 8, \
"./DRENCH_TCP/multi_data_drench.csv" index 2 using 1:2 with linespoints title "Network Function C" axes x1y1 ls 7, \
"./DRENCH_TCP/related_flows_info.csv" using 1:($4/100) with linespoints title "Avg. Path Deviation" axes x1y2 ls 1 , \
"./DRENCH_TCP/rdir_log2.csv" using 1:2 w lp  title "Total # of Redirections" axes x1y2 ls 2, \
#"./DRENCH_TCP/case_related_flowsinfo.csv" using 1:($4/100) with linespoints title "Avg. Path Deviation" axes x1y2 ls 1 , \
#"./DRENCH_TCP/case_rdir_log.csv" using 1:2 w lp  title "Total # of Redirections" axes x1y2 ls 2, \


set terminal x11
set output
replot



