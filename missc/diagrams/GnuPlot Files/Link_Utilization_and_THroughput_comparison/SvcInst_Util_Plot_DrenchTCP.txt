#!/usr/bin/gnuplot
reset
#set terminal windows size 2400, 600

set terminal png size 2400, 600
#set terminal png size 1024,768
set output 'svc_utlization_tcp_drench.png'
#set terminal pdf
#set output 'case3.pdf'

#set term postscript
#set output "case3.ps"

#set xdata Duration
set xlabel "Time in Seconds"
set xtics 5
set y2tics 1

set xrange[60:110]
set y2range[-1:10]
set yrange[0.5:8]

set ylabel "Instance Utilization in Mbps"
#set y2label "Redirections-Path-StretchDeviations"

set ytics nomirror
set y2tics

set title "Service Instance Utilization"
set datafile separator ","
set autoscale fix
set key inside top right

set style data lines
set style fill solid

set style data lines
set style fill solid

set pointsize 0.7
set style line 1 lc rgb "black" lt 1 lw 2 
set style line 2 lc rgb "blue" lt 2 lw 2 
set style line 3 lc rgb "green" lt 3 lw 2
set style line 4 lc rgb "pink" lt 4 lw 2
set style line 5 lc rgb "red" lt 5 lw 2

set style line 6 lc rgb '#0060ad' lt 6 lw 2 
set style line 7 lc rgb '#06ad60' lt 7 lw 2 
set style line 8 lc rgb '#6060ad' lt 8 lw 2 
set style line 9 lc rgb '#303030' lt 9 lw 2 
set style line 10 lc rgb '#edf0ad' lt 10 lw 2 
set style line 11 lc rgb '#0af0ed' lt 11 lw 2 
set style line 12 lc rgb '#adf0ad' lt 12 lw 2
set style line 13 lc rgb '#feef00' lt 13 lw 2 
set pointintervalbox 3


plot "./DRENCH_REDIR_CASE/TCP/new/log_nw_utilization_info_lcl_h1-eth0.csv" using 1:($2 <= 0 ? 1/0 : $2) w linespoints  title "NF-1 Utilization(Drench TCP)" axes x1y1 ls 8, \
"./DRENCH_REDIR_CASE/TCP/new/log_nw_utilization_info_lcl_h2-eth0.csv" using 1:($2 <= 0 ? 1/0 : $2) w linespoints  title "NF-2 Utilization(Drench TCP)" axes x1y1 ls 6, \
"./DRENCH_REDIR_CASE/TCP/new/log_nw_utilization_info_lcl_h3-eth0.csv" using 1:($2 <= 0 ? 1/0 : $2) w linespoints  title "NF-3 Utilization(Drench TCP)" axes x1y1 ls 4, \
"./DRENCH_REDIR_CASE/TCP/new/log_nw_utilization_info_lcl_h4-eth0.csv" using 1:($2 <= 0 ? 1/0 : $2) w linespoints  title "NF-4 Utilization(Drench TCP)" axes x1y1 ls 2, \
"./DRENCH_REDIR_CASE/TCP/new/log_nw_utilization_info_lcl_h5-eth0.csv" using 1:($2 <= 0 ? 1/0 : $2) w linespoints  title "NF-5 Utilization(Drench TCP)" axes x1y1 ls 10, \
"./DRENCH_REDIR_CASE/TCP/new/log_nw_utilization_info_lcl_h6-eth0.csv" using 1:($2 <= 0 ? 1/0 : $2) w linespoints  title "NF-6 Utilization(Drench TCP)" axes x1y1 ls 12




#convert case1.ps case1.jpg
