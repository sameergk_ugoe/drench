#To extract column 2 data and get the avg
awk '{ total += $2 } END { print total/NR }'
perl -F, -lane '$total += $F[1]; END{print $total/$.}' file

#To append a fixed column to the each line of all files in a directory
for file in *.txt; do gawk -i inplace '{print $0 " -m rttm"}' $file; done

#To swap a content of column with new value (say 10240) for each line of all files in a directory
for file in *.txt; do gawk -i inplace '{$10=10240}1' $file; done

#To truncate file to first x lines 
for file in *.txt; do sed -i '2, $max_lines p' $file; done
for file in *.txt; do sed -i '$max_lines+1,$ d' $file; done

#To swap a content of column with new value (say 10240) for each line of all files in a directory
for file in *.txt; do gawk -i inplace '{$10=10240}1' $file; done
for file in *.txt; do gawk -i inplace '{$2="TCP"}1' $file; done
find . -name "*.txt" -type f -print | xargs gawk -i inplace '{$2="TCP"}1'

#To replace a content of column for every 8th row wih a new value for each line of all files in a directory
for file in *.txt; do gawk -i inplace '{!(NR%8) && $10=10240}1' $file; done

#count total lines in all files recursively
find . -type f -exec wc -l {} \; | awk '{total += $1} END{print total}'
or 
wc -l `find . -type f`

#Recursively find .bak files and delete
find . -name "*.bak" -type f -print | xargs /bin/rm -f

#Recursively find .bak files and move to home/.old.files directory
find . -name "*.bak" -print0 | xargs -0 -I {} mv {} ~/old.files
 
#Shuffle the lines of files inplace in a given directory
for file in *.txt; do shuf < $file -o $file ; done

#find all 0 byte files in a directory
find . -name "*.txt" -type f -size 0 -delete
find . -name 'file*' -size 0 -print0 | xargs -0 rm
find . -name 'file*' -size 0 -delete

#Find Avg of Row value:
awk 'BEGIN{FS=",";} { if ($1 > 0) {sum+=$1; count+=1}; } END { {print FILENAME,  "Total Records=" NR "Total Fields=" NF  "Sum=" sum "Count=" count  "Avg= "sum/count, max} }' text.csv
find . -name "log_nw_utilization_info.csv" -type f -print | xargs awk 'BEGIN{FS=",";} { if ($1 > 0) {sum+=$1; count+=1}; } END { {print FILENAME,  "Total Records=" NR "Total Fields=" NF  "Sum=" sum "Count=" count  "Avg= "sum/count, max} }'
awk 'BEGIN{FS=",";} { if ($2 > 0) {sum+=$2; count+=1}; } END { {print FILENAME,  "Total Records=" NR "Total Fields=" NF  "Sum=" sum "Count=" count  "Avg= "sum/count, max} }'

awk 'BEGIN{FS=",";} { if ($1 > 0) {sum+=$1; count+=1}; } END { {print FILENAME,  "Total Records=" NR "Total Fields=" NF  "Sum=" sum "Count=" count  "Avg= "sum/count, max} }' log_nw_utilization_info.csv

awk '{sum=0; for(i=1; i<=NF; i++){sum+=$i}; sum/=NF; print sum}' file


find . -name "log_nw_utilization_info.csv" -type f -print | xargs awk 'BEGIN{FS=",";} { for(i=1; i<=NF; i++){ if ($i > 0) {sum[i]+=$i; rec[i]+=1} }; } END { for(i=1; i<=NF; i++) {print FILENAME, sum[i]/rec[i], max} }'

awk '{for(i=1; i<=NF; i++){sum[i]+=$i}} END {for(i=1; i<=NF; i++){printf sum[i]/NR "\t"}}' file
#Find Avg of all Columns
awk '{for(i=1; i<=NF; i++){sum[i]+=$i}} END {for(i=1; i<=NF; i++){printf sum[i]/NR "\t"}}' files

#Refer:
http://www.catonmat.net/blog/ten-awk-tips-tricks-and-pitfalls/
http://www.grymoire.com/Unix/Awk.html
http://lorance.freeshell.org/csvutils/

#some git and Awk quickies
#add all files that have status changed
git status -s|awk '{ print $2 }'|xargs git add
#reset changes
git status -s|awk '{ print $2 }'|xargs git reset HEAD


#Find line with max fields and Count 
awk '{ if (NF > max) max = NF } END { print max }'
find . -name "splog*.csv" -type f -print | xargs awk '{ if (NF > max) max = NF } END { print FILENAME, max }'
find . -name "splog*.csv" -type f -print | xargs awk 'BEGIN{FS=","} if ((NF-2)/2 > max) max = (NF-2)/2 } END { print FILENAME, max }'
#Prepend to file some detail
awk 'BEGIN{FS=","} {print NF+1 "," $0}' splog_0.csv 

#To remove first field column 
awk 'BEGIN{FS=OFS="\t"}{$1="";}1' file
#also the first FS
awk 'BEGIN{FS=OFS="\t"}{$1="";sub("\t","")}1'  file
#for inplace try: #Dangerous wipes out full file
awk 'BEGIN{FS=OFS="\t"}{$1="";sub(",","")}1'  file
find . -name "splog*.csv" -type f -print | xargs gawk -i inplace 'BEGIN{FS=OFS=","}{$1="";sub(",","")}1'




find . -name "splog*.csv" -type f -print | xargs awk 'BEGIN{FS=","} if ((NF-2)/2 > max) max = (NF-2)/2 } END { print FILENAME, max }'

#get-mean-min-max.awk
#You must define COL with option -v of awk
{
if(min==”")
{
min=max=$COL
};

if($COL>max)
{
max=$COL
maxIdx=NR
};

if($COL< min)
{
min=$COL
minIdx=NR
};

total+=$COL;
count+=1
}
END {print total/count "\n" min " – " minIdx "\n" max " – " maxIdx}

awk ‘{if(min==””)min=max=$1}; if($1>max) {max=$1}; if($1< min) {min=$1}; total+=$1; count+=1} END {print total/count, min, max}’ data.txt
awk -F ‘;’ ‘{if(min==””){min=max=$2}; if($2>max) {max=$2}; if($2< min) {min=$2}; total+=$2; count+=1} END {print total/count, min, max}’ data.txt

$ awk 'BEGIN {FS=","} min=="" { min=max=$3 ; minday=maxday=$4}{if ($3 > max) {max = $3; maxday = $4};if ($3 < min) {min = $3; minday = $4};total += $3count += 1}END {print "minimum hours:" min"("minday")";print "maximum hours:" max"("maxday")";print "average:" total/count;}' report.txt


#!/usr/bin/awk -f
# NOTE: This example takes only CPU load as input. Use it like this:
#
#    cat log.txt | awk '$1 == "1923" {print $9}' | awk -f rolling-average.awk

BEGIN {
    freq = 10; sum = 0; max = 0
}

{
    i = NR % freq
    if (NR > freq)
        sum -= data[i]
    sum += (data[i] = $1)
}

NR >= freq {
    avg = sum/freq
    print "average for " NR-freq+1 ".." NR " = " avg
    if (avg > max) {
        max = avg
        pos = NR
    }
}

END {
    print "peak " max " at " pos-freq+1 ".." pos
}

____________________________________________________________________________________________________________________________________________________________________________________________________________
#For extracting Avg Link Utilization:
awk 'BEGIN{FS=",";} { if ($1 > 0) {sum+=$1; count+=1}; } END { {print FILENAME,  "Total Records=" NR "Total Fields=" NF  "Sum=" sum "Count=" count  "Avg= "sum/count, max} }' log_nw_utilization_info.csv
awk 'BEGIN{FS=",";} { if ($1 > 0) {sum+=$1; count+=1}; } END { {print FILENAME,  "Total Records=" NR "Total Fields=" NF  "Sum=" sum "Count=" count  "Avg= "sum/count, max} }' log_nw_utilization_info.csv
for file in log_nw_utilization_info_lcl_*.csv; do awk 'BEGIN{FS=",";} { if ($1 > 0) {sum+=$1; count+=1}; } END { {print FILENAME,  "Total Records=" NR "Total Fields=" NF  "Sum=" sum "Count=" count  "Avg= "sum/count, max} }' $file ; done

