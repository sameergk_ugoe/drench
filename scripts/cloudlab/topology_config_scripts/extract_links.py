#!/usr/bin/python
#usage
#sudo python net_v2.py base_dir_path conf_file_name other_options
#sudo python net_v2.py -b /home/mininet/drench -c demotopo_conf.csv

 
import csv
import os.path
import random
import sys
import time
import argparse
from os.path import expanduser
hdir_p = expanduser('~')
#hdir_p = '/local'
base_dir = hdir_p   + '/drench'
topo_dir = base_dir + '/topologies'

MAX_SWITCHES = 5
MAX_HOSTS    = 6

#Configuration File <Total Switches, Total Hosts>
conf_file = topo_dir + '/demotopo_conf.csv'
#conf_file = topo_dir + '/gfc1_conf.csv'
#conf_file = topo_dir + '/gfc2_conf.csv'

# Switch-Switch Links File <Hid, Swid, Weight, TotalHosts, TotalLinks>
slink_file = topo_dir + '/demotopo.csv'
#slink_file = topo_dir + '/gfc1_slink.csv'

#Host to Switch Link File <Hid, Swid, Weight, TotalHosts, TotalLinks>
hlink_file = topo_dir + '/host_link_demotopo.csv'
#hlink_file = topo_dir + '/gfc1_hlink.csv'

def update_paths():
  global hdir_p, base_dir, topo_dir, tscr_dir
  topo_dir = base_dir + '/topologies'
  
def parse_named_args():
    global base_dir, conf_file, test_file, ecn_mode
    is_parsed = False
    
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--base_dir', '-b', required=True,help='Base Directory of execution')
    parser.add_argument('--cfg', '-c', required=True, help='Config file with description of Topology')
    
    args = parser.parse_args()
    
    if args.base_dir:
        base_dir = args.base_dir
        update_paths()
        is_parsed = True
    if args.cfg:
        conf_file = topo_dir + '/' + args.cfg
        #conf_file = args.cfg
        is_parsed = True
    return is_parsed
    
def get_params_from_conf_file():
    global conf_file, slink_file, hlink_file
    global MAX_HOSTS, MAX_SWITCHES
    
    if(os.path.isfile(conf_file)):
        pass
    else:
        print conf_file, "Conf File Not Found! Return!!"
        return True

    #Get the Switch Count and Host Count from CSV File Header
    slk_file = None
    hlk_file = None
    with open(conf_file, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        index = 0
        for row in reader:
            try:
                if index == 0 and len(row) >=2:
                    MAX_SWITCHES = int(row[0])
                if index == 0 and len(row) >=2:
                    MAX_HOSTS    = int (row[1])
                if index == 0 and len(row) >=3:
                    slk_file = row[2]
                    if slk_file is not None:
                        slink_file = topo_dir + '/' + slk_file
                        if(os.path.isfile(slink_file)): pass
                        else: return True
                if index == 0 and len(row) >=4:
                    hlk_file = row[3]
                    if hlk_file is not None:
                        hlink_file = topo_dir + '/' + hlk_file
                break
            except Exception,e:
                print " Failed to read configuration header,", str(e), "Defaults for Max Switches [%d] and Max Hosts [%d], Link files [%s,%s]" %(MAX_SWITCHES, MAX_HOSTS, slk_file,hlk_file)
                return True
    return False

def export_conn_list(conn_list, conn_type, conn_file):
    conn_str = ""
    for i, conn in enumerate(conn_list):
        c_str = conn[0] + ':' + conn[1]
        conn_str += c_str
        if i+1 < len(conn_list): conn_str += ','
    if conn_file:
        new_csv_file = conn_file.split('.')[0] + '_cloudlab_links_' + conn_type + '.' + conn_file.split('.')[1]
    else:
        new_csv_file =  topo_dir + '/cloudlab_links_' + conn_type + '.csv'
    
    with open(new_csv_file, 'w') as f:
        f.write(conn_str)
    return
    
def build_topo():
    "Construct Topology from CSV File (Use Small Scale and Large Scale)"
    global conf_file, slink_file, hlink_file
    global MAX_HOSTS, MAX_SWITCHES
    
    if get_params_from_conf_file():
        return None
    
    # Add Switches
    switchlist = [('s%i' %i) for i in range(1,MAX_SWITCHES+1)]
    print "\n", switchlist, "\n"
  
    # Add Hosts
    hostlist = [('h%i' %i) for i in range(1,MAX_HOSTS+1)]
    print "\n", hostlist, "\n"

    # Add Host to Switch Links
    link_count = 0
    if(os.path.isfile(hlink_file)):
        connlist = []
        with open(hlink_file, 'rb') as csvfile:
            reader = csv.DictReader(csvfile)
            link_count = 0
            for row in reader:
                sw1 = int(row['Host'])
                sw2 = int(row['Switch'])

                s1 = 'h%i' %sw1
                s2 = 's%i' %sw2
                if( (s1 in hostlist) and (s2 in switchlist) and ((s1,s2) not in connlist) ):
                    connlist.append((s1,s2))
                    link_count = link_count+1
                    #print(" Added: Host:%d, Switch:%d, linkopts:%s",sw1,sw2,linkopts)
                else:
                    #print(" Skipped: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
                    pass
            print "[%i] Number of Host to Switch Links Created." %link_count
    else:
        #Randomly insert the links 
        sws = range(1,MAX_SWITCHES+1)
        #random.shuffle(sws)
        for i in range(1,MAX_HOSTS+1):
            sw1 = 'h%i' %i
            sw2 = 's%i'%sws[((i-1)%MAX_SWITCHES)]
            connlist.append((sw1,sw2))
            link_count = link_count+1
    
    #Export Host_to_Switch connection list as string format to CSV file
    export_conn_list(connlist, 'H_S', hlink_file)
    
    # Add Switch to Switch Links
    link_count = 0
    connlist = []
    with open(slink_file, 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        link_count = 0
        for row in reader:
            sw1 = int(row['Source'])
            sw2 = int(row['Destination'])
      
            s1 = 's%i' %sw1
            s2 = 's%i' %sw2
            if( ( s1 in switchlist) and ( s2 in switchlist) and 
                (((s1,s2) not in connlist) and ((s2,s1) not in connlist))):
                link_count = link_count+1
                connlist.append((s1,s2))
                #print(" Added: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
                #print("\n Added: Sw1:%d, Sw2:%d, linkopts:%s",sw1,sw2,linkopts)
            else:
                #print(" Skipped: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
                pass
        print "[%i] Number of Switch to Switch Links Created." %link_count
    
    #Export Switch_to_Switch connection list as string format to CSV file
    export_conn_list(connlist, 'S_S', slink_file)
    return
    
if __name__ == '__main__':
    parse_named_args()
    build_topo()
