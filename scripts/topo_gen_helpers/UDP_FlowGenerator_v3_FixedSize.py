#!/usr/bin/python

#
# Program to generate cli command list
#

#from mininet.log import setLogLevel
from random import randint
import sys
import csv
import time
import random
import argparse

#small_flows:
#Fized size(200KB): #python UDP_FlowGenerator_v3_FixedSize.py -c 3 -n 500 -t 0 -b 10.0.0.0 -bh 21 -mh 16 -fd 10
#Fixed size (4KB) python UDP_FlowGenerator_v3_FixedSize.py -c 3 -n 500 -t 0 -s 1 -b 10.0.0.0 -bh 21 -mh 16 -fd 5
#Fixed size (32KB) python UDP_FlowGenerator_v3_FixedSize.py -c 3 -n 500 -t 0 -s 1 -b 10.0.0.0 -bh 21 -mh 16 -fd 5

#python UDP_FlowGenerator_v3.py -c 3 -n 25 -t 0 -b 10.0.0.0 -bh 21 -mh 16 -fd 5

num_flows = 10
flow_type = 0
out_file = "flows.txt"
distribution = 0
ecn_mode = 0
base_ip_addr = "10.0.0.0"
max_hosts = 30
base_host_id = 1
base_rp_port = 10001
rp_next_offset =10
inter_flow_delay_ms = 10
small_flow_sizes = [4, 8, 16, 32, 64, 128, 256, 512, 1024] # in KB
large_flow_sizes = [10, 25, 50, 100, 150, 250, 500] # in MB
size_index = 0
def parse_named_args():
    global num_flows, flow_type, out_file, base_ip_addr, distribution, ecn_mode, max_hosts, base_host_id, inter_flow_delay_ms, size_index
    is_parsed = False
    chain_length = 3
    max_functions = 6

    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--chain_length', '-c' ,'-l', required=True,type=int, help='Service Chain Length')
    parser.add_argument('--num_flows', '-n' ,'-f', required=True, type=int, help='Number of Flows')
    parser.add_argument('--flow_type', '-t', required=True, type=int, help='Type of Flows <Mice=0/Elephant=1/Mix=2>')
    parser.add_argument('--flow_size_index', '-s', type=int, help='Size Index to choose for Flow i.e. flows total size in bytes based on type')

    parser.add_argument('--base_ip_addr', '-b', help='base_ip_addr')
    parser.add_argument('--base_host_id', '-bh', type=int, help='Start ID of Host that acts as src/dst for traffic generation')
    parser.add_argument('--distribution', '-d', type=int, help='Distribution Mode')
    parser.add_argument('--max_hosts', '-mh', type=int, help='Maximum Hosts in the system')
    parser.add_argument('--inter_flow_delay', '-fd', type=int, help='Delay in ms between flows i.e. flow scheduling rate')

    parser.add_argument('--max_functions', '-mf', type=int, help='Maximum Number of Service Chain Functions')
    parser.add_argument('--out_file', '-o', help='Output to File')
    parser.add_argument('--ecn', '-e', type=int, help='ECN Mode')

    args = parser.parse_args()

    if args.chain_length:
        chain_length = args.chain_length
        is_parsed = True
    if args.num_flows:
        num_flows = args.num_flows
        is_parsed = True
    if args.out_file:
        out_file = args.out_file
        is_parsed = True
    if args.out_file:
        out_file = args.out_file
        is_parsed = True
    if args.flow_size_index:
        size_index = args.flow_size_index

    if args.max_functions and args.max_functions > 0 and args.max_functions <15:
        max_functions = args.max_functions
    if args.base_ip_addr:
        base_ip_addr = args.base_ip_addr
    if args.base_host_id:
        base_host_id = args.base_host_id
    if args.distribution:
        distribution = args.distribution
    if args.max_hosts:
        max_hosts = args.max_hosts
    if args.inter_flow_delay:
        inter_flow_delay_ms = args.inter_flow_delay
    if args.ecn:
        ecn_mode = args.ecn

    return is_parsed

rp_count = 0
def get_next_rport():
    global rp_count
    rport = base_rp_port + rp_next_offset*rp_count
    rp_count+=1
    return rport

def get_svc_chain_keys():
    keys = [ i for i in xrange(1,256) if  not (i == 0 or i&0x01 or i&0x02==2) ]
    return keys
def get_small_flow_size():
    if size_index == 0: return 200
    else:
        if size_index <= len (small_flow_sizes): return small_flow_sizes[size_index-1]
    return 200
def get_large_flow_size():
    if size_index == 0: return 200
    else:
        if size_index <= len (large_flow_sizes): return large_flow_sizes[size_index-1]
    return 200

def get_flow_size_inKB():
    flow_size, multiplier = 1,1
    if flow_type == 0:
        flow_size = get_small_flow_size()
        #multiplier = randint(1,20) # 10KB to 4 MB
        #flow_size = 10
    elif flow_type == 1:
        flow_size = get_large_flow_size()
        multiplier = 1024
        #multiplier = randint(200,50000) # 5MB to 500MB+
        #flow_size = 10
    else:
        # change to adapt to mix of traffic pattern
        #multiplier = randint(1,10000) % provide 80% small and 20% large flows
        multiplier = random.triangular(1, 400, 50000)
        flow_size = 10
    flow_size = flow_size*multiplier
    return flow_size

def get_packet_size():
    return 1400
    pkt_size = 10
    if flow_type == 0:
        multiplier = randint(25,100)
    elif flow_type == 1:
        multiplier = randint(125,150) #150
    else:
        # change to adapt to mix of traffic pattern
        multiplier = randint(10,150)
    pkt_size = pkt_size*multiplier
    return pkt_size
def get_packets_per_sec():
    return 0
    multiplier = randint(1,10)
    pps = 100*multiplier
    return pps
def generate_flows_for_host(host_id, host_file, host2_file,max_flows_per_host=0):
    hf = open(host_file, "wb")
    hf2 = open(host2_file, "wb")

    base_inter_flow_delay = inter_flow_delay_ms
    nflows = 0
    tos_keys = get_svc_chain_keys()
    hosts = [h for h in xrange(base_host_id,base_host_id+max_hosts) if h!= host_id]
    random.shuffle(hosts)
    for h in hosts: #xrange(1,max_hosts+1):
        if h == host_id: continue
        d_ip = base_ip_addr[0:base_ip_addr.rfind('.')] + '.' + str(h)
        d2_ip = '10.0.' + str(h) + '.1'

        flows_per_dst = max(1, int(max_flows_per_host/len(hosts)))
        j = 0
        random.shuffle(tos_keys)
        for key in tos_keys:
            tos=int(key)
            fsize = get_flow_size_inKB()
            psize = get_packet_size()
            pktps = get_packets_per_sec()
            rport = get_next_rport() #10000 + host_id*100 + h*10 + j

            cmd = '-T UDP -a ' + str(d_ip) + ' -rp ' + str(rport)
            cmd2 = '-T UDP -a ' + str(d2_ip) + ' -rp ' + str(rport)

            opts = ' -b ' + str(tos) + ' -k ' + str(fsize) + ' -c ' + str(psize)
            #opts += ' -C ' + str(pktps)
            #opts += ' -m ' + 'owdm' # 'rttm'
            opts += ' -d ' + str(base_inter_flow_delay)
            opts += ' \n'

            cmd += opts
            cmd2 += opts

            hf.write(cmd)
            hf2.write(cmd2)


            nflows +=1
            base_inter_flow_delay +=inter_flow_delay_ms
            j+=1
            if j == flows_per_dst:
                break
            if max_flows_per_host and nflows >= max_flows_per_host:
                break
        if max_flows_per_host and nflows >= max_flows_per_host: break
    hf.close()
    return nflows
def generate_all_flows():
    global max_hosts
    max_flows_per_host = 0
    if num_flows <= max_hosts:
        max_flows_per_host = 1
    else:
        max_flows_per_host = int((num_flows+max_hosts)/max_hosts)
    generated_flows = 0
    #for h in xrange(base_host_id,base_host_id+max_hosts):
    hlist = [h for h in xrange(base_host_id,base_host_id+max_hosts)]
    random.shuffle(hlist)
    for h in hlist:
        h_ip = base_ip_addr[0:base_ip_addr.rfind('.')] + '.' + str(h)
        h2_ip = '10.0.' + str(h) + '.1'
        h_file = str(h_ip) + "_flows.txt"
        h2_file = str(h2_ip) + "_flows.txt"
        generated_flows+= generate_flows_for_host(h, h_file, h2_file, max_flows_per_host)
        if generated_flows >= num_flows: break

if __name__ == '__main__':
  parse_named_args()
  generate_all_flows()
