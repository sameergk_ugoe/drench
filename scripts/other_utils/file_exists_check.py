#!/usr/bin/python
 
import subprocess
import pipes
 
ssh_host = 'xmodulo@remote_server'
file = '/var/run/test.pid'
 
resp = subprocess.call(
    ['ssh', ssh_host, 'test -e ' + pipes.quote(file)])
 
if resp == 0:
    print ('%s exists' % file)
else:
    print ('%s does not exist' % file)