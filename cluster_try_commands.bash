RUN FatTree single Script: <drench/mn_scripts>
./xnew_launch_script.sh /home/mininet/drench ftreetopo_conf.csv list_ftree.csv svc_chain_dict.csv 172.23.0.33 6633
./new_launch_script.sh /home/mininet/drench ftreetopo_conf.csv list_ftree.csv svc_chain_dict.csv 127.0.0.1 6633


__________________________________________________________________________________________________________________________________
BENCHMARK
GREEDY:
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c full_ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2,192.168.56.3,192.168.56.4 -n 20
cd ~/drench/pox && ./stdsdn_drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv 1

DRENCH:
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c full_ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2,192.168.56.3,192.168.56.4 -n 20
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv

STDSDN-no_redir
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c full_ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2,192.168.56.3,192.168.56.4 -n 20
cd ~/drench/pox && ./stdsdn_drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv 

 


______________________________________________________________________________________________________________________________
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv

cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c full_ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2,192.168.56.3,192.168.56.4 -n 20

Cloudlab cluster Setup:

Drench Controller ( on last node 192.168.56.5):
#FatTree
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv
#DemoTopo
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv

Mininet cluster (on first node 192.168.56.1):
#FatTree
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2 -n 20
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2,192.168.56.3,192.168.56.4 -n 20
#FullFatTree
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c full_ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2,192.168.56.3,192.168.56.4 -n 20

#SVCSWAP 
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c svc_swap_topo_conf.csv -s svc_chain_dict_svc_swap.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.4,192.168.56.3 -n 6
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_svcswap.csv svc_chain_dict_svc_swap.csv

#DemoTopo
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c demotopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2 -n 5


Local Setup
DRENCH COntroller:
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv 0 0, 0
MININET:
sudo python /home/mininet/drench/mn_scripts/net_v2.py -b /home/mininet/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 172.23.0.33 -cpt 6633 -n 20
FullFatTree
sudo python /home/mininet/drench/mn_scripts/net_v2.py -b /home/mininet/drench -c full_ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 172.23.0.33 -cpt 6633 -n 20

Hexaon Topology
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_hexagontopo.csv svc_chain_dict_hexatopo.csv 0 0, 0
sudo python /home/mininet/drench/mn_scripts/net_v2.py -b /home/mininet/drench -c hexagontopo_conf.csv -s svc_chain_dict_hexatopo.csv  -cip 172.23.0.33 -cpt 6633 -n 8


MININET_CLUSTER (Linux Localbox):
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 172.23.0.33 -cpt 6633 -cllst localhost,192.168.56.102 -n 20

cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c svc_swap_topo_conf.csv -s svc_chain_dict_svc_swap.csv  -cip 172.23.0.33 -cpt 6633 -cllst localhost,192.168.56.102 -n6
Controller:
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv

#SVC SWAP USECASE
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_svcswap.csv svc_chain_dict_svc_swap.csv 1 
sudo python /home/mininet/drench/mn_scripts/net_v2.py -b /home/mininet/drench -c svc_swap_topo_conf.csv -s svc_chain_dict_svc_swap.csv  -cip 172.23.0.33 -cpt 6633 -n 6

#Hexagon Topo
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_hexagontopo.csv svc_chain_dict_single_chain.csv
sudo python /home/mininet/drench/mn_scripts/net_v2.py -b /home/mininet/drench -c hexagontopo_conf.csv -s svc_chain_dict_single_chain.csv  -cip 172.23.0.33 -cpt 6633 -n 8

git fetch --all; git reset --hard origin/master
______________________________________________________________________________________________________________________________________


#CLUSTER RELATED:
sudo apt-get install build-essential fakeroot
sudo wget http://openvswitch.org/releases/openvswitch-2.5.0.tar.gz
sudo tar zxvf openvswitch-2.5.0.tar.gz
sudo cd openvswitch-2.5.0
sudo ./boot.sh
sudo ./configure --with-linux=/lib/modules/`uname -r`/build
sudo make 
sudo make install
#Load the OVS Kernel Module
sudo insmod datapath/linux/openvswitch.ko
sudo touch /usr/local/etc/ovs-vswitchd.conf
sudo mkdir -p /usr/local/etc/openvswitch
sudo ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
sudo ovsdb-server /usr/local/etc/openvswitch/conf.db \
    --remote=punix:/usr/local/var/run/openvswitch/db.sock \
    --remote=db:Open_vSwitch,manager_options \
    --private-key=db:SSL,private_key \
    --certificate=db:SSL,certificate \
    --bootstrap-ca-cert=db:SSL,ca_cert --pidfile --detach --log-file
sudo ovs-vsctl --no-wait init
sudo ovs-vswitchd --pidfile --detach
sudo insmod datapath/linux/openvswitch.ko
sudo ovs-vsctl show
sudo ovs-vsctl --version


SSH SETUP:
ssh-keygen -t rsa
cat ~/.ssh/id_rsa.pub | ssh mininet@192.168.56.2 "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"

ssh-keygen -t rsa
cat ~/.ssh/id_rsa.pub | ssh mininet@192.168.56.1 "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"


sudo git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git
sudo chmod +x /drench/scripts/cloudlab/system_setup_script.sh
. /drench/scripts/cloudlab/system_setup_script.sh

128.104.222.150
chmod +x drench/pox/pox.py
./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv
sudo python /home/mininet/drench/mn_scripts/net_v2.py -b /home/mininet/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv -t inc_flows1.py -e 0 


sudo -E mn --topo tree,3,3 --cluster localhost,128.104.222.148 --controller remote
sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv -t inc_flows1.py -cllst localhost,128.104.222.148 -e 0

sudo python /local/drench/mn_scripts/net_v2_cluster.py -b /local/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2 

Exception: ('makeTunnel:\n', 'Tunnel setup failed for', 's1:None', 'to', 's10:mininet@128.104.222.148\n', 'command was:', ['ssh', '-n', '-o', 'Tunnel=Ethernet', '-w', '9:9', 'mininet@128.104.222.148', 'echo @'], '\n')



#Launching Mininet cluster
cd ~/drench/mn_scripts && ./xcluster_new_launch_script.sh /home/mininet/drench ftreetopo_conf.csv list_ftree.csv svc_chain_dict.csv 192.168.56.5 6633 localhost,192.168.56.2,192.168.56.3,192.168.56.4

#FatTree
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c ftreetopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2

#DemoTopo
cd ~/drench/mn_scripts &&  sudo python /home/mininet/drench/mn_scripts/net_v2_cluster.py -b /home/mininet/drench -c demotopo_conf.csv -s svc_chain_dict.csv  -cip 192.168.56.5 -cpt 6633 -cllst localhost,192.168.56.2

#Launching Controller
cd ~/drench/pox && ./drench_controller.sh /home/mininet/drench list_ftree.csv svc_chain_dict.csv
