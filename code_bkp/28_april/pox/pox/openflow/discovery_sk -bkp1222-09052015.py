# Copyright 2011-2013 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is loosely based on the discovery component in NOX.

"""
This module discovers the connectivity between OpenFlow switches by sending
out LLDP packets. To be notified of this information, listen to LinkEvents
on core.openflow_discovery.

It's possible that some of this should be abstracted out into a generic
Discovery module, or a Discovery superclass.
"""

from pox.lib.revent import *
from pox.lib.recoco import Timer
from pox.lib.util import dpid_to_str, str_to_bool
from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt
import csv                                      #EM030315
import os.path                                  #SK

import struct
import time
from collections import namedtuple
from random import shuffle, random

log = core.getLogger()
####################################                        #EM030315
function_map = []                                 #EM030315
abc = 0
svc_dict = {}  #SK DictionaArray of ['Service name':[(Host Switch MacID, Shadow Price)]]
list_file_ln = '/home/mininet/list.csv'
list_file2_ln = '/home/mininet/list2.csv'
list_file_wn = 'list.csv'
list_file2_wn = 'list2.csv'
####################################                        #EM030315

class LLDPSender (object):
  """
  Sends out discovery packets
  """

  SendItem = namedtuple("LLDPSenderItem", ('dpid','port_num','packet'))

  #NOTE: This class keeps the packets to send in a flat list, which makes
  #      adding/removing them on switch join/leave or (especially) port
  #      status changes relatively expensive. Could easily be improved.

  # Maximum times to run the timer per second
  #_sends_per_sec = 15
  _sends_per_sec = 15       #EM040415

  def __init__ (self, send_cycle_time, ttl = 120):
    """
    Initialize an LLDP packet sender

    send_cycle_time is the time (in seconds) that this sender will take to
      send every discovery packet.  Thus, it should be the link timeout
      interval at most.

    ttl is the time (in seconds) for which a receiving LLDP agent should
      consider the rest of the data to be valid.  We don't use this, but
      other LLDP agents might.  Can't be 0 (this means revoke).
    """
    # Packets remaining to be sent in this cycle
    self._this_cycle = []

    # Packets we've already sent in this cycle
    self._next_cycle = []

    # Packets to send in a batch
    self._send_chunk_size = 1

    self._timer = None
    self._ttl = ttl
    self._send_cycle_time = send_cycle_time
    core.listen_to_dependencies(self)

  def _handle_openflow_PortStatus (self, event):
    """
    Track changes to switch ports
    """
    if event.added:
      self.add_port(event.dpid, event.port, event.ofp.desc.hw_addr)
    elif event.deleted:
      self.del_port(event.dpid, event.port)

  def _handle_openflow_ConnectionUp (self, event):
    self.del_switch(event.dpid, set_timer = False)

    ports = [(p.port_no, p.hw_addr) for p in event.ofp.ports]

    for port_num, port_addr in ports:
      self.add_port(event.dpid, port_num, port_addr, set_timer = False)

    self._set_timer()

  def _handle_openflow_ConnectionDown (self, event):
    self.del_switch(event.dpid)

  def del_switch (self, dpid, set_timer = True):
    self._this_cycle = [p for p in self._this_cycle if p.dpid != dpid]
    self._next_cycle = [p for p in self._next_cycle if p.dpid != dpid]
    if set_timer: self._set_timer()

  def del_port (self, dpid, port_num, set_timer = True):
    if port_num > of.OFPP_MAX: return
    self._this_cycle = [p for p in self._this_cycle
                        if p.dpid != dpid or p.port_num != port_num]
    self._next_cycle = [p for p in self._next_cycle
                        if p.dpid != dpid or p.port_num != port_num]
    if set_timer: self._set_timer()

  def add_port (self, dpid, port_num, port_addr, set_timer = True):
    if port_num > of.OFPP_MAX: return
    self.del_port(dpid, port_num, set_timer = False)
    #SK      
    self._next_cycle.append(LLDPSender.SendItem(dpid, port_num,
          self.create_discovery_packet(dpid, port_num, port_addr)))
    
    custom_lldp_pkts = []
    custom_lldp_pkts = self.create_discovery_packet_sk(dpid, port_num, port_addr)
    for custom_lldp_pkt in custom_lldp_pkts:
      self._next_cycle.append(LLDPSender.SendItem(dpid, port_num,custom_lldp_pkt))
    
    #if custom_lldp_pkts != None:
    #  self._next_cycle.append(LLDPSender.SendItem(dpid, port_num,custom_lldp_pkts))
    #if(self.ChecksendCustomLLPD_sk(dpid, port_num, port_addr) is True):
      #self._next_cycle.append(LLDPSender.SendItem(dpid, port_num,
      #    self.create_discovery_packet_sk(dpid, port_num, port_addr)))
      #pass
    #else:
    #  log.info("No LLPD created!")
    #  pass
    #SK
    
    if set_timer: self._set_timer()

  def _set_timer (self):
    if self._timer: self._timer.cancel()
    self._timer = None
    num_packets = len(self._this_cycle) + len(self._next_cycle)

    if num_packets == 0: return

    self._send_chunk_size = 1 # One at a time
    interval = self._send_cycle_time / float(num_packets)
    if interval < 1.0 / self._sends_per_sec:
      # Would require too many sends per sec -- send more than one at once
      interval = 1.0 / self._sends_per_sec
      chunk = float(num_packets) / self._send_cycle_time / self._sends_per_sec
      self._send_chunk_size = chunk

    self._timer = Timer(interval,
                        self._timer_handler, recurring=True)

    ###############################################                 #EM030315
  def clearFlows(self):                               #EM030415
      for c in core.openflow.connections:                     #EM030415
        d = of.ofp_flow_mod(command = of.OFPFC_DELETE)              #EM030415
        c.send(d)                               #EM030415
      log.info("flows cleared on %s" % c)                     #EM030415

  def function_handler (self):                            #EM030415
  #####################################                     #EM030415
  # Code to insert function value as TTL in LLDP packet             #EM030415
  #####################################                     #EM030415
    global function_map                               #EM030415
    csv_file = ''
    if(os.path.isfile(list_file_ln)):
      csv_file = list_file_ln
      log.debug("ListFile is:  %s ", csv_file)
    elif (os.path.isfile(list_file_wn)):
      csv_file = list_file_wn
      log.debug("ListFile is:  %s ", csv_file)
    else:
      log.warn("ListFile: %s is not Found ", csv_file)
      return
      
    with open(csv_file, 'rb') as csvfile:             #EM030415
      reader = csv.DictReader(csvfile)                      #EM030415
      for row in reader:                              #EM030415
        fnc = int(row['function'])                        #EM030415
        mac = row['mac']                            #EM030415
        iFlag = 1                               #EM030415
        for x in range (0, len(function_map)):                  #EM030415
          if (function_map[x]['FUNCTION'] == fnc and \
            function_map[x]['MAC'] == mac):                 #EM030415
            iFlag = 0                           #EM030415
      if iFlag == 1:                              #EM030415
        self.insert_function(fnc, mac)                    #EM030415

  def insert_function(self, func, macad):                     #EM030415
    dpid = 1                                    #EM030415
    port_num = 1                                  #EM030415
    #port_addr = "b6:68:50:ec:35:95"                        #EM030415
    port_addr = "xx:xx:xx:xx:xx:xx"                         #EM030415
    chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)         #EM030415
    chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])

    port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(port_num))

    ttl = pkt.ttl(ttl = self._ttl)

    sysdesc = pkt.system_description()
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])

    discovery_packet = pkt.lldp()
    discovery_packet.tlvs.append(chassis_id)
    discovery_packet.tlvs.append(port_id)
  #####################################                     #EM030315
  # Code to insert function value as TTL in LLDP packet             #EM030315
  #####################################                     #EM030315
    #print "Insert function: %i" %func                      #EM030315
    ttl = pkt.ttl(ttl = func)             #EM030315
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1] +'\n mac:'+macad) #EM240315

    discovery_packet.tlvs.append(ttl)
    discovery_packet.tlvs.append(sysdesc)

    discovery_packet.tlvs.append(pkt.end_tlv())                   #EM030415

    eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)                 #EM030415
    eth.src = port_addr                               #EM030415
    eth.dst = pkt.ETHERNET.NDP_MULTICAST                      #EM030415
    eth.payload = discovery_packet                          #EM030415

    po = of.ofp_packet_out(action = of.ofp_action_output(port=port_num))      #EM030415
    po.data = eth.pack()
    core.openflow.sendToDPID(dpid, po.pack())                   #EM030415

  def function_handler_sk (self, dpid = 1, port_num = 1, port_addr = "xx:xx:xx:xx:xx:xx", mode = 0):                            #EM030415
  #####################################                     #EM030415
  # Code to insert function value as TTL in LLDP packet             #EM030415
  #####################################                     #EM030415
    ####TEST
    ####return self.create_discovery_packet(dpid,port_num,port_addr)
    
    global svc_dict                               #EM030415
    custom_packets_list = []
    log.debug("Gather packets for dpid=%d, port_num=%d, port_addr=%s",dpid,port_num,port_addr)
    csv2_file = 'none'
    if(os.path.isfile(list_file2_ln)):
      csv2_file = list_file2_ln
      log.debug("ListFile2 is:  %s ", csv2_file)
    elif (os.path.isfile(list_file2_wn)):
      csv2_file = list_file2_wn
      log.debug("ListFile2 is:  %s ", csv2_file)
    else:
      global abc
      if abc == 0:
        log.warn("ListFile2: %s is not Found ", csv2_file)
        abc = 1
      else:
        #pass
        return None

    with open(csv2_file, 'rb') as csvfile:             #EM030415
      reader = csv.DictReader(csvfile)                      #EM030415
      for row in reader:                              #EM030415
        svc = int(row['function'],2)                        #EM030415
        mac = row['mac']                            #EM030415
        sp  = int(row['sp'])
        fnc = str(row['function'])
        sw_num = int(row['switch'])
        #log.info("#####sw_num:%d Fnc:%s mac:%s sp:%d svc:%d", sw_num, fnc, mac, sp, svc)
        iFlag = 1
        #log.info("##### %s:%d, %d",svc_dict.keys(),len(svc_dict.keys()),svc)
        #log.info("#### svc_dict = {%s}",svc_dict)
        
        if ( svc_dict.has_key(svc)):
        #if svc in svc_dict.keys:
          for x in range(0,len(svc_dict[svc])):
            isp,imac = int(svc_dict[svc][x][0]),svc_dict[svc][x][1]
            log.info("##### isp:%d imac:%s sp:%d mac:%s", isp, imac, sp, mac)
            if(isp == sp and imac == mac):
              iFlag = 0                           #EM030415
        else:
          log.info("#### Key:[%d] not found in svc_dict:%s",svc,svc_dict)

        if iFlag == 1:                              #EM030415
          custom_packets_list.append(self.insert_function_sk( svc=svc,macad=mac,sp=sp,dpid=dpid, port_num = port_num, port_addr = port_addr, mode=mode))
          #return self.insert_function_sk( svc=svc,macad=mac,sp=sp,dpid=dpid, port_num = port_num, port_addr = port_addr, mode=mode)                    #EM030415
        else:
          log.info("**** Skipped duplicate packet for Key:[%d] in svc_dict:%s ****",svc,svc_dict.keys())
          pass
          #return self.create_discovery_packet(dpid,port_num,port_addr)
          #return None
    log.info('Number of Custom LLDP Packets to be sent: %d', len(custom_packets_list))
    return custom_packets_list

  def insert_function_sk(self,svc, macad,sp, dpid = 1, port_num = 1, port_addr = "xx:xx:xx:xx:xx:xx", mode=0):                     #EM030415
    
    chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)         #EM030415
    chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])

    port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(port_num))

    ttl = pkt.ttl(ttl = svc)
    log.debug("Initial TTL:%s",ttl)

    sysdesc = pkt.system_description()
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])

    discovery_packet = pkt.lldp()
    discovery_packet.tlvs.append(chassis_id)
    discovery_packet.tlvs.append(port_id)
    
    #####################################                     #EM030315
    # Code to insert function value as TTL in LLDP packet             #EM030315
    #####################################                     #EM030315
    #print "Insert function: %i" %func                      #EM030315

    #sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1] +'\n mac:'+macad + '\n sp:'+sp) #EM240315
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1] +'\n mac:'+macad + '\n sp:'+str(sp)) #EM240315
    #log.info("Setting packet with TTL: %s, val:%d", ttl,svc)
    
    discovery_packet.tlvs.append(ttl)
    discovery_packet.tlvs.append(sysdesc)

    discovery_packet.tlvs.append(pkt.end_tlv())                   #EM030415

    eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)                 #EM030415
    eth.src = port_addr                               #EM030415
    eth.dst = pkt.ETHERNET.NDP_MULTICAST                      #EM030415
    eth.payload = discovery_packet                          #EM030415

    #log.debug('SK: Send Specific LLDP for MAC: [%s] ', str(macad)) #SK
    #log.debug('SK: Send Specific LLDP with SP: [%s] ', str(sp)) #SK
    #log.debug('SK: Send Specific LLDP with TTL: [%s]', str(ttl)) #SK
    
    po = of.ofp_packet_out(action = of.ofp_action_output(port=port_num))      #EM030415
    po.data = eth.pack()
    if mode == 0:
      #core.openflow.sendToDPID(dpid, po.pack())                   #EM030415
      log.debug('SK: Send Specific LLDP for MAC:[%s] with SP:[%s] and TTL:[%s]', str(macad),str(sp),str(svc)) #SK
    else:
      log.debug('SK: Prepared and Return Specific LLDP for MAC:[%s] with SP:[%s] and TTL:[%s]', str(macad),str(sp),str(svc)) #SK
      pass
    return po.pack()

  def _timer_handler (self):
    """
    Called by a timer to actually send packets.

    Picks the first packet off this cycle's list, sends it, and then puts
    it on the next-cycle list.  When this cycle's list is empty, starts
    the next cycle.
    """
    num = int(self._send_chunk_size)
    fpart = self._send_chunk_size - num
    if random() < fpart: num += 1

    #self.function_handler()                             #EM030415
    #self.function_handler_sk(mode=0)                    #SK

    for _ in range(num):
      if len(self._this_cycle) == 0:
        self._this_cycle = self._next_cycle
        self._next_cycle = []
        #shuffle(self._this_cycle)
      item = self._this_cycle.pop(0)
      self._next_cycle.append(item)
      core.openflow.sendToDPID(item.dpid, item.packet)
      #log.info("Regular packets dpid:%d eth_src:%s",item.dpid, item.packet.data.eth_src)

  def create_discovery_packet (self, dpid, port_num, port_addr):
    """
    Build discovery packet
    """
    chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)
    chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])
    # Maybe this should be a MAC.  But a MAC of what?  Local port, maybe?

  #############################################################################
    #print "dpid: %s" %dpid
    #print "port_num: %s" %str(port_num)
    #print "port_addr: %s" %str(port_addr)
    #log.info("PortAddr: %s",port_addr)

    port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(port_num))

    ttl = pkt.ttl(ttl = self._ttl)
    
    #log.info("Creating default discovery LLDP packet: TTL %s", ttl)
    
    sysdesc = pkt.system_description()
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])

    discovery_packet = pkt.lldp()
    discovery_packet.tlvs.append(chassis_id)
    discovery_packet.tlvs.append(port_id)
  #####################################                     #EM030315
  # Code to insert function value as TTL in LLDP packet             #EM030315
  #####################################                     #EM030315
    #print "Insert function dpid: %i" %dpid                     #EM030315
    #with open('/home/mininet/list.csv', 'rb') as csvfile:              #EM030315
  # reader = csv.DictReader(csvfile)                      #EM030315
  # for row in reader:                              #EM030315
  #   if dpid == int(row['switch']):                      #EM030315
  #     ttl = pkt.ttl(ttl = int(row['function']))             #EM030315
  #     sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1] +'\n mac:'+row['mac']) #EM240315
    # Below is attempt to insert function in custom TVL field         #EM030315
    #function = pkt.organizationally_specific()                 #EM030315
    #function.payload = bytes('101')                      #EM030315
    #discovery_packet.tlvs.append(function)                   #EM030315
  #####################################                     #EM030315
    discovery_packet.tlvs.append(ttl)
    discovery_packet.tlvs.append(sysdesc)

    discovery_packet.tlvs.append(pkt.end_tlv())

    eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)
    eth.src = port_addr
    eth.dst = pkt.ETHERNET.NDP_MULTICAST
    eth.payload = discovery_packet

    po = of.ofp_packet_out(action = of.ofp_action_output(port=port_num))
    po.data = eth.pack()
    return po.pack()

  def create_discovery_packet_sk (self, dpid, port_num, port_addr):
    """
    Build custom discovery packet
    """
    return self.function_handler_sk(dpid,port_num,port_addr,mode=1)


  def ChecksendCustomLLPD_sk(self, dpid, port_num, port_addr):
    """" 
    Function checks if we need to send the custom LLDP packet 
    returns None if not else returns the LLDP packet
    """
    return True
    #return False
    
class LinkEvent (Event):
  """
  Link up/down event
  """
  def __init__ (self, add, link):
    Event.__init__(self)
    self.link = link
    self.added = add
    self.removed = not add

  def port_for_dpid (self, dpid):
    if self.link.dpid1 == dpid:
      return self.link.port1
    if self.link.dpid2 == dpid:
      return self.link.port2
    return None


class Link (namedtuple("LinkBase",("dpid1","port1","dpid2","port2"))):
  @property
  def uni (self):
    """
    Returns a "unidirectional" version of this link

    The unidirectional versions of symmetric keys will be equal
    """
    pairs = list(self.end)
    pairs.sort()
    return Link(pairs[0][0],pairs[0][1],pairs[1][0],pairs[1][1])

  @property
  def end (self):
    return ((self[0],self[1]),(self[2],self[3]))

  def __str__ (self):
    return "%s.%s -> %s.%s" % (dpid_to_str(self[0]),self[1],
                               dpid_to_str(self[2]),self[3])

  def __repr__ (self):
    return "Link(dpid1=%s,port1=%s, dpid2=%s,port2=%s)" % (self.dpid1,
        self.port1, self.dpid2, self.port2)


class Discovery (EventMixin):
  """
  Component that attempts to discover network toplogy.

  Sends out specially-crafted LLDP packets, and monitors their arrival.
  """

  _flow_priority = 65000     # Priority of LLDP-catching flow (if any)
  _link_timeout = 10         # How long until we consider a link dead
  _timeout_check_period = 5  # How often to check for timeouts
  #_link_timeout = 100         # How long until we consider a link dead
  #_timeout_check_period = 50  # How often to check for timeouts

  _eventMixin_events = set([
    LinkEvent,
  ])

  _core_name = "openflow_discovery" # we want to be core.openflow_discovery

  Link = Link

  def __init__ (self, install_flow = True, explicit_drop = True,
                link_timeout = None, eat_early_packets = False):
    self._eat_early_packets = eat_early_packets
    self._explicit_drop = explicit_drop
    self._install_flow = install_flow
    if link_timeout: self._link_timeout = link_timeout

    self.adjacency = {} # From Link to time.time() stamp
    self._sender = LLDPSender(self.send_cycle_time)

    # Listen with a high priority (mostly so we get PacketIns early)
    core.listen_to_dependencies(self,
        listen_args={'openflow':{'priority':0xffffffff}})

    Timer(self._timeout_check_period, self._expire_links, recurring=True)

  @property
  def send_cycle_time (self):
    return self._link_timeout / 2.0

  def install_flow (self, con_or_dpid, priority = None):
    if priority is None:
      priority = self._flow_priority
    if isinstance(con_or_dpid, (int,long)):
      con = core.openflow.connections.get(con_or_dpid)
      if con is None:
        log.warn("Can't install flow for %s", dpid_to_str(con_or_dpid))
        return False
    else:
      con = con_or_dpid

    match = of.ofp_match(dl_type = pkt.ethernet.LLDP_TYPE,
                          dl_dst = pkt.ETHERNET.NDP_MULTICAST)
    msg = of.ofp_flow_mod()
    msg.priority = priority
    msg.match = match
    msg.actions.append(of.ofp_action_output(port = of.OFPP_CONTROLLER))
    con.send(msg)
    return True

  def _handle_openflow_ConnectionUp (self, event):
    if self._install_flow:
      # Make sure we get appropriate traffic
      log.debug("Installing flow for %s", dpid_to_str(event.dpid))
      log.info("Discovery: ConnectionUp: Installing flow for %s", dpid_to_str(event.dpid))
      self.install_flow(event.connection)

  def _handle_openflow_ConnectionDown (self, event):
    # Delete all links on this switch
    self._delete_links([link for link in self.adjacency
                        if link.dpid1 == event.dpid
                        or link.dpid2 == event.dpid])

  def _expire_links (self):
    """
    Remove apparently dead links
    """
    now = time.time()

    expired = [link for link,timestamp in self.adjacency.iteritems()
               if timestamp + self._link_timeout < now]
    if expired:
      for link in expired:
        log.info('link timeout: %s', link)

      self._delete_links(expired)

  def _handle_openflow_PacketIn (self, event):
    """
    Receive and process LLDP packets
    """
    packet = event.parsed
    log.debug("LLDP Packet IN: %s", str(packet))

    if (packet.effective_ethertype != pkt.ethernet.LLDP_TYPE
        or packet.dst != pkt.ETHERNET.NDP_MULTICAST):
      if not self._eat_early_packets: return
      if not event.connection.connect_time: return
      enable_time = time.time() - self.send_cycle_time - 1
      if event.connection.connect_time > enable_time:
        return EventHalt
      return

    if self._explicit_drop:
      if event.ofp.buffer_id is not None:
        log.debug("Dropping LLDP packet %i", event.ofp.buffer_id)
        msg = of.ofp_packet_out()
        msg.buffer_id = event.ofp.buffer_id
        msg.in_port = event.port
        event.connection.send(msg)

    lldph = packet.find(pkt.lldp)
    if lldph is None or not lldph.parsed:
      log.error("LLDP packet could not be parsed")
      return EventHalt
    if len(lldph.tlvs) < 3:
      log.error("LLDP packet without required three TLVs")
      return EventHalt
    if lldph.tlvs[0].tlv_type != pkt.lldp.CHASSIS_ID_TLV:
      log.error("LLDP packet TLV 1 not CHASSIS_ID")
      return EventHalt
    if lldph.tlvs[1].tlv_type != pkt.lldp.PORT_ID_TLV:
      log.error("LLDP packet TLV 2 not PORT_ID")
      return EventHalt
    if lldph.tlvs[2].tlv_type != pkt.lldp.TTL_TLV:
      log.error("LLDP packet TLV 3 not TTL")
      return EventHalt

    def lookInSysDesc ():
      r = None
      for t in lldph.tlvs[3:]:
        if t.tlv_type == pkt.lldp.SYSTEM_DESC_TLV:
          # This is our favored way...
          for line in t.payload.split('\n'):
            if line.startswith('dpid:'):
              try:
                return int(line[5:], 16)
              except:
                pass
          if len(t.payload) == 8:
            # Maybe it's a FlowVisor LLDP...
            # Do these still exist?
            try:
              return struct.unpack("!Q", t.payload)[0]
            except:
              pass
          return None

    def getMAC ():                            #EM240315
      r = None                              #EM240315
      for t in lldph.tlvs[3:]:                      #EM240315
        if t.tlv_type == pkt.lldp.SYSTEM_DESC_TLV:            #EM240315
          # This is our favored way...                  #EM240315
          for line in t.payload.split('\n'):              #EM240315
            if line.startswith(' mac:'):                #EM240315
              try:                            #EM240315
                return (line[6:])                   #EM240315
              except:                         #EM240315
                pass                          #EM240315
          return None

    def getSP ():                            #EM240315
      r = None                              #EM240315
      for t in lldph.tlvs[3:]:                      #EM240315
        if t.tlv_type == pkt.lldp.SYSTEM_DESC_TLV:            #EM240315
          # This is our favored way...                  #EM240315
          for line in t.payload.split('\n'):              #EM240315
            #log.info("Line:[%s]", line)
            if line.startswith(' sp:'):                #EM240315
              try:                            #EM240315
                log.debug("SP Value: [%s]", line[4:])
                return (line[4:])                   #EM240315
              except:                         #EM240315
                pass                          #EM240315
          return None

    originatorDPID = lookInSysDesc()

    if originatorDPID == None:
      # We'll look in the CHASSIS ID
      if lldph.tlvs[0].subtype == pkt.chassis_id.SUB_LOCAL:
        if lldph.tlvs[0].id.startswith('dpid:'):
          # This is how NOX does it at the time of writing
          try:
            originatorDPID = int(lldph.tlvs[0].id[5:], 16)
          except:
            pass
      if originatorDPID == None:
        if lldph.tlvs[0].subtype == pkt.chassis_id.SUB_MAC:
          # Last ditch effort -- we'll hope the DPID was small enough
          # to fit into an ethernet address
          if len(lldph.tlvs[0].id) == 6:
            try:
              s = lldph.tlvs[0].id
              originatorDPID = struct.unpack("!Q",'\x00\x00' + s)[0]
            except:
              pass

    if originatorDPID == None:
      log.warning("Couldn't find a DPID in the LLDP packet")
      return EventHalt

    if originatorDPID not in core.openflow.connections:
      log.info('Received LLDP packet from unknown switch')
      return EventHalt

  ###############################################                     #EM030315
    def clearFlows():                                   #EM030415
        for c in core.openflow.connections:                         #EM030415
            d = of.ofp_flow_mod(command = of.OFPFC_DELETE)                  #EM030415
            c.send(d)                                   #EM030415
        log.info("flows cleared on %s" % c)                         #EM030415

    def storeFunction():                                  #EM030315
      """
      Stores function code for correct dpid in a list of arrays
      """
      func = lldph.tlvs[2].ttl                              #EM030315
      iFlag = 1                                     #EM030315
      if func != 120:                                   #EM030315
        global function_map                               #EM030315
        for x in range (0, len(function_map)):                      #EM030315
          if (function_map[x]['FUNCTION'] == func and \
            function_map[x]['MAC'] == getMAC()):                  #EM030315
            iFlag = 0                               #EM030315
        if iFlag == 1:                                  #EM030315
          function_map.append({'FUNCTION':func, 'MAC':getMAC()})            #EM030315
          #print 'Function Detected: %s' %func                     #EM030315
        clearFlows()
    #for t in lldph.tlvs[126:]:                             #EM030315
    # if t.tlv_type == pkt.lldp.ORGANIZATIONALLY_SPECIFIC_TLV:            #EM030315
    #   function_map.append({"FUNCTION":t.payload, "DPID":originatorDPID})      #EM030315
    #   print 'Function Detected: '+ t.payload                    #EM030315
  ################################################                    #EM030315
    #storeFunction()                                     #EM030315

    def storeFunction_sk():                                  #SK
      """
      Stores svc code for correct dpid in a list of arrays
      """
      func = lldph.tlvs[2].ttl                              #EM030315
      log.debug("LLDP Packet IN2: %s, %d", str(packet),func)
      iFlag = 1                                     #EM030315
      if func != 120:                                   #EM030315
      #if func == 120:                                   #EM030315
        global svc_dict                               #SK
        #svc = str(func)
        svc = func
        mac = getMAC()
        sp = getSP()
        iFlag = 1                               #EM030415
        #log.debug ('Key of LLD Packet:[%d], MAC:[%s] Shadow Price:[%s]', svc, str(mac),str(sp))
        if (svc_dict.has_key(svc)):
        #if svc in svc_dict.keys:
          for x in range(0,len(svc_dict[svc])):                 #SK
            isp,imac = svc_dict[svc][x][0],svc_dict[svc][x][1]
            if(isp == sp and imac == mac):
              iFlag = 0                           #SK
              log.debug("SKIPPED!(sp=%s,mac=%s),already in Dict:%s", sp,mac,isp,imac,svc_dict)
          if iFlag == 1:
            svc_dict[svc].append((sp,mac))
            #log.debug ('Existing Service:%d Detected at %s, %s',svc,str(mac),str(sp))
            clearFlows()
        else:
          svc_dict[svc] = [(sp,mac)]
          #log.debug ('New Service:%s Detected at:%s, %s', svc,str(mac),str(sp))
          clearFlows()
        #log.info('Store_Func::ServiceDictionary is: %s', svc_dict)

    storeFunction_sk()                                     #SK
    
    # Get port number from port TLV
    if lldph.tlvs[1].subtype != pkt.port_id.SUB_PORT:
      log.warning("Thought we found a DPID, but packet didn't have a port")
      return EventHalt
    originatorPort = None
    if lldph.tlvs[1].id.isdigit():
      # We expect it to be a decimal value
      originatorPort = int(lldph.tlvs[1].id)
    elif len(lldph.tlvs[1].id) == 2:
      # Maybe it's a 16 bit port number...
      try:
        originatorPort  =  struct.unpack("!H", lldph.tlvs[1].id)[0]
      except:
        pass
    if originatorPort is None:
      log.warning("Thought we found a DPID, but port number didn't " +
                  "make sense")
      return EventHalt

    if (event.dpid, event.port) == (originatorDPID, originatorPort):
      log.warning("Port received its own LLDP packet; ignoring")
      return EventHalt

    link = Discovery.Link(originatorDPID, originatorPort, event.dpid,
                          event.port)

    if link not in self.adjacency:
      self.adjacency[link] = time.time()
      log.info('link detected: %s', link)
      self.raiseEventNoErrors(LinkEvent, True, link)
    else:
      # Just update timestamp
      self.adjacency[link] = time.time()

    return EventHalt # Probably nobody else needs this event

  def _delete_links (self, links):
    for link in links:
      self.raiseEventNoErrors(LinkEvent, False, link)
    for link in links:
      self.adjacency.pop(link, None)

  def is_edge_port (self, dpid, port):
    """
    Return True if given port does not connect to another switch
    """
    for link in self.adjacency:
      if link.dpid1 == dpid and link.port1 == port:
        return False
      if link.dpid2 == dpid and link.port2 == port:
        return False
    return True

  def getFunctionList (self):                             #EM030315
    """                                         #EM030315
    Return function list                                #EM030315
    """
    global function_map
    return function_map                                 #EM030315


  def getFunctionList_sk (self):                  #SK
    """                                             #SK
    Return SV_MAP                                   #SK
    """
    global svc_dict
    return svc_dict                                 #SK


def launch (no_flow = False, explicit_drop = True, link_timeout = None,
            eat_early_packets = False):
  explicit_drop = str_to_bool(explicit_drop)
  eat_early_packets = str_to_bool(eat_early_packets)
  install_flow = not str_to_bool(no_flow)
  if link_timeout: link_timeout = int(link_timeout)

  core.registerNew(Discovery, explicit_drop=explicit_drop,
                   install_flow=install_flow, link_timeout=link_timeout,
                   eat_early_packets=eat_early_packets)
