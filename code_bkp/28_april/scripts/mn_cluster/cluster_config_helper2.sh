#!/bin/bash

#Swith to user mininet (Unfortunately there is no way to get around this, have to manually run it on individual terminals)
#su - mininet

#Cluster setup (only after logging into user mininet)
cluster_nodes="192.168.56.2 192.168.56.3 192.168.56.4"
#./drench/mininet/util/clustersetup.sh -p $cluster_nodes
./local/drench/mininet/util/clustersetup.sh -p $cluster_nodes
