#!/usr/bin/python

#
# Program to generate packets with vlan
#
import subprocess
import commands
import logging
from scapy.all import sendp, sniff, ICMP, Dot1Q, Ether, Raw, IP, srp1, TCP, conf, socket
import time
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
#from scapy.layers.inet import IP
import csv
import netifaces
import time
import sys
import threading
import thread
import sys
from random import randint, normalvariate, gauss
from threading import Thread
from os.path import expanduser
host_interface = "hx-eth0"
host_mac_addr = "00:00:00:00:00:00"
host_ip_addr = "00.00.00.00"
l2_socket = None
ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
payload = ",svclist,"
nvlan = 0
#nvlan = 251 #[1111 1011] (BC)
#nvlan = 4029 #[1111 1011 1101] (BCA)
#nvlan = 3069 #[1011 1111 1101] (CBA)
#nvlan = 3579 #[1101 1111 1011] (ABC)
#nvlan = 13 #(A)
#nvlan = 253 #[1111 1101] (BA)

sChain = []
nid = 1
nseq = 1
interval = 0.5
count = 10
flow_id = 0
path_cost = 0
done = False
nElastic = 0
flow_rate = 0
hdir_p = expanduser('~')
hdir_p = hdir_p + '/drench/results'

def getFunction():

    interface_list = netifaces.interfaces()
    interface = filter(lambda x: 'eth0' in x,interface_list)
    if len(interface) == 0:
        print "No interface found"
        return
  
    global ipsrc
    global esrc
    global host_interface
    global host_mac_addr
    global host_ip_addr
    global flow_rate
    global l2_socket
    
    addrs      = netifaces.ifaddresses(interface[0])
    link_addrs = addrs[netifaces.AF_LINK]
    net_addrs  = addrs[netifaces.AF_INET]
    host_interface  = interface[0]
    host_mac_addr   = link_addrs[0]['addr']
    host_ip_addr    = net_addrs[0]['addr']
    esrc = host_mac_addr
    ipsrc = host_ip_addr
    
    l2_socket = conf.L2socket(iface=host_interface)
    
    global nvlan
    global sChain
    global nid
    global nseq
    global interval
    global flow_id
    global count
    global ipdst
    global edst
    global nElastic
    nid = randint(0,99999)
    nseq = 1
    src_id = 0
    dst_id = 0
    try:
        #parse arguments <svc_chain, esrc, edst, count, flow_id, ping_interval, elastic_or_cbr>
        #i = 7
        nElastic = 0
        #if(len(sys.argv)>i) and sys.argv[i].isdigit() is True:
        #    nElastic = int(sys.argv[i])
        #i -=1
        i = 6
        if(len(sys.argv)>i):
            ival = float(sys.argv[i])
            if ival > 0:
              interval = ival
        i -=1
        
        if(len(sys.argv)>i):
            flow_id = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            count = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            dst_id = int(sys.argv[i])
            ipdst = "10.0.0."+ sys.argv[i]
            edst  = "00:00:00:00:00:" + "{:0>2d}".format(dst_id)
            #if dst_id < 10:
            #    edst = "00:00:00:00:00:0"+sys.argv[i]
            #else:
            #    edst = "00:00:00:00:00:"+sys.argv[i]
        i -=1
        
        if(len(sys.argv)>i):
            src_id = int(sys.argv[i])
            #print src
        i-=1
        
        if(len(sys.argv)>i):
            sChain = sys.argv[1].split(",")
            sChain = [x for x in sChain if x.isdigit() is True]
            #nvlan = int(sChain.pop(0),2)
            nvlan = int(sChain[0],2)
            print "sChain:", sChain, "vlan: %d" %nvlan 
            #print "vlan: %s" %str(nvlan)
        print "Generator: %s to_Dst:%s(%d), Service Chain:%s, First Service: %d , num_packets:%d and Flow_ID:%d, ping_interval:%f, Elastic:%d " %(host_ip_addr,edst,dst_id,sChain,nvlan,count,flow_id,interval, nElastic)
        flow_rate = 1/float(interval) #packets per second
    except Exception,e:
        print "Exception parsing input args!!", str(e)
        exit()

def sendpings_1():
    pe=Ether()/IP(dst="192.168.56.1")/ICMP()
    #sendp(pe, loop=True)
    sendp(pe,verbose=0)
    return 1

pe_g=Ether()/IP(dst="192.168.56.1")/ICMP()
def sendpings_2():
    global pe_g
    #sendp(pe_g, loop=True)
    sendp(pe_g,verbose=0)
    return 1

def sendpings_3():
    global pe_g
    #sendp(pe_g, loop=True)
    sendp(pe_g, verbose=0)
    return 1

def sendpings_4():
    s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
    s.bind(("eth0", 0))
    pe=Ether()/IP(dst="192.168.56.1")/ICMP()
    data = pe.build()
    s.send(data)
    return 1

def sendpings_5():
    global l2_socket
    pe=Ether()/IP(dst="192.168.56.1")/ICMP()
    #data = pe.build()
    l2_socket.send(pe)
    return 1

def sendpings_6():
    global l2_socket
    global pe_g
    #data = pe.build()
    l2_socket.send(pe_g)
    return 1

def sendpings_7():
    global l2_socket
    global pe_g
    pe_g[ICMP].code = 8
    #data = pe.build()
    l2_socket.send(pe_g)
    return 1

def eval_rate():
    i = 0
    for i in xrange(0, 7):
        t_end = time.time() + 1
        count = 0
        while time.time() < t_end:
            if i ==0:
                count += sendpings_1()
            if i ==1:
                count += sendpings_2()
            if i ==2:
                count += sendpings_3()
            if i ==3:
                count += sendpings_4()
            if i ==4:
                count += sendpings_5()
            if i ==5:
                count += sendpings_6()
            if i ==6:
                count += sendpings_7()
        print "Total Packets sent using sendpings_%d() in 1second: %d " %(i+1,count)
    
MAX_WAIT_TIME_FOR_RESP = 0.5

if __name__ == '__main__':
    getFunction()
    eval_rate()
