import geni.portal as portal
import geni.rspec.pg as PG
import geni.rspec.igext as IG

rspec = PG.Request()

pc = portal.Context()
pc.defineParameter("H", "Number of Host Nodes",portal.ParameterType.INTEGER, 2)

pc.defineParameter("S", "Number of swithces Nodes",portal.ParameterType.INTEGER, 2)

pc.defineParameter("H_S_L", "Host To Switch Links (csv format h1:s2,h2:s2..)",portal.ParameterType.STRING, 'h1:s1,h2:s2')

pc.defineParameter("S_S_L", "Switch To Switch Links (csv format s1:s2,s2:s3..)",portal.ParameterType.STRING, 's1:s2')

pc.defineParameter("CIP", "Controller_IP", portal.ParameterType.STRING, "134.76.249.236")

pc.defineParameter("CP", "Controller_Port", portal.ParameterType.INTEGER, 6633)

params = pc.bindParameters()

def setup_tour():
    global rspec
    tour = IG.Tour()
    tour.Description(IG.Tour.TEXT, "Dynamic topo with GeniLib with variable number of nodes and switches")
    tour.Instructions(IG.Tour.MARKDOWN, "Host to Switch and Switch to Switch Links must be manipulated in the python code. (all are 2 core, 1GB ram hosts: Isolate switch and hosts and conserve resources)")
    rspec.addTour(tour)

#List of Hosts and Switches <'h1'.... 'hn'> and <'s1'.... 'sn'>  
node_list = [] 
#Dictionary of Hosts and Switches key = 'h1' value = PG.node
node_dict = {}
#Dictionary of Hosts and Switches Interface List = 'h1' value = {ethx: iface_node}
node_if_dict ={}
#Infor for Links 
host_connections = [] # [['h1', 's1'], ['h2', 's2'], ['h3', 's3'], ['h4', 's4'], ['h5', 's5']]
switch_connections = [] #[['s1', 's2'], ['s2', 's3'], ['s3', 's4'], ['s4', 's5']]
link_list = []
iface_list = []
subnet_list= []
#base_ip_addr = "192.168.56."
base_ip_addr = "10.0.0."
base_host_mac_addr = "00:00:00:00:00:"
base_switch_mac_addr = ":00:00:00:00:00"

my_disk_image = "urn:publicid:IDN+wisc.cloudlab.us+image+kkprojects-PG0:Node_and_Switch"
my_disk_image = "urn:publicid:IDN+utah.cloudlab.us+image+kkprojects-PG0:Cluster_wtih_RawPC"
#my_disk_image = "urn:publicid:IDN+utah.cloudlab.us+image+kkprojects-PG0:MyCloudLab_OpenStackProfile"
#Drench Repository: https://bitbucket.org/sameergk_ugoe/drench/get/f94f08a23cb6.zip
#Only startup script: "#url=https://bitbucket.org/sameergk_ugoe/cloudlab_startup/get/8354ce39ac7f.zip

controller = None
def setup_controller():
    global controller
    cip = params.CIP
    cp  = params.CP
    controller = IG.OFController(cip,cp)
    return
# HELPER FUNCTIONS FOR CREATING NODES
def setup_host_info():
    global node_list
    for i in range(1, params.H+1):
        node_list.append('h%d' %i)
def setup_switch_info():
    global node_list
    for i in range(1, params.S+1):
        node_list.append('s%d' %i)
def create_all_nodes():
    global node_dict
    global rspec
    
    #Extract controller Information
    setup_controller()
    
    #Extract Host Node Details
    setup_host_info()
    
    #Extract Switch Node Details
    setup_switch_info()
    
    #Add host and switch nodes to rspec
    for node in node_list:
        h_node = IG.XenVM(node)
        h_node.cores = 2 #4
        #h_node.ram = 1024 #*4 
        #h_node.disk = 8
        h_node.disk_image = my_disk_image
        
        #h_node = PG.XenVM(node) #IG.XenVM(node)
        #h_node.disk_image = my_disk_image #h_node.disk = my_disk_image
        
        #h_node.addService(PG.Install(path="/local", url = "https://bitbucket.org/sameergk_ugoe/drench/get/f94f08a23cb6.zip"))
        #h_node.addService(PG.Install(path="/local", url = "https://bitbucket.org/sameergk_ugoe/cloudlab_startup/get/8354ce39ac7f.zip"))
        #h_node.addService(PG.Execute(shell="sh", command ="sudo git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git"))
        #h_node.addService(PG.Execute(shell="sh", command ="sudo cd /home/mininet; sudo git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git"))
        
        #On Each node install all the basic software tools
        h_node.addService(PG.Execute(shell="sh", command="sudo touch new_install.txt"))
        h_node.addService(PG.Execute(shell="sh", command="sudo mv /drench /drench_bkp"))
        h_node.addService(PG.Execute(shell="sh", command="sudo git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git"))
        h_node.addService(PG.Execute(shell="sh", command ="sudo chmod -R 666 /drench"))
        h_node.addService(PG.Execute(shell="sh", command ="sudo chmod -R +x /drench/*.sh"))
        h_node.addService(PG.Execute(shell="sh", command ="sudo chmod -R +x /drench/*.py"))
        h_node.addService(PG.Execute(shell="sh", command ="sudo chmod +x /drench/scripts/cloudlab/system_setup_script.sh"))
        h_node.addService(PG.Execute(shell="sh", command =". /drench/scripts/cloudlab/system_setup_script.sh"))
        
        
        h_node.addService(PG.Execute(shell="sh", command ="sudo mkdir /home/mininet"))
        h_node.addService(PG.Execute(shell="sh", command ="sudo cp -r /drench /home/mininet/"))
        
        #Configure the switches to start the openvSwitch bridges/ports and NF instances start the NFV services and end hosts start the stream services
        h_node.addService(PG.Execute(shell="sh", command ="sudo python /drench/scripts/cloudlab/node_configuration_scripts/configure_host_and_switch.py"))
        #h_node.addService(PG.Execute(shell="sh", command ="sudo python /drench/scripts/cloudlab/node_configuration_scripts/configure_ovs.py"))
        
        #h_node.addService(PG.Execute(shell="sh", command ="sudo python /local/drench/scripts/cloudlab/node_configuration_scripts/configure_ovs2.py"))
        #h_node.addService(PG.Execute(shell="sh", command ="sudo python /local/drench/scripts/cloudlab/node_configuration_scripts/configure_ovs.py"))
        #<TODO: End host services>
        
        node_dict[node] = h_node
        rspec.addResource(h_node)
    return

#PARSE LINKS INFORMATION
def setup_host_switch_links():
    global host_connections, link_list
    input_links_str = params.H_S_L
    input_links_str = input_links_str.rstrip('\n')
    h_links = input_links_str.split(',')
    host_connections = [h.split(':') for h in h_links]
    link_list.append(host_connections)
def setup_switch_switch_links():
    global switch_connections, link_list
    input_links_str = params.S_S_L
    input_links_str = input_links_str.rstrip('\n')
    s_links = input_links_str.split(',')
    switch_connections = [s.split(':') for s in s_links]
    link_list.append(switch_connections)

#INTERFACE FUNCTIONS
start_num = '01' 
def get_host_ip_and_mac_address(host_node):
    global start_num
    host_id = int(host_node[1:])
    mask = '255.255.255.0'
    host_ip  = base_ip_addr + str(host_id)
    host_mac = base_host_mac_addr + "{:0>2d}".format(host_id)
    return (host_ip, host_mac, mask)
def get_sw_ip_address_and_mask(switch_node):
    return
    
def setup_host_interface(host_node, iface):
    host_ip, host_mac, mask  = get_host_ip_and_mac_address(host_node)
    iface.addAddress(PG.IPv4Address(host_ip, mask))
    iface.mac_address = host_mac
    return
    
    # Specify the IPv4 address
    #host_if_ip_addr = base_ip_addr + host_node[1:]
    #iface.addAddress(PG.IPv4Address(host_if_ip_addr, "255.255.255.0"))
    #host_if_ip_addr, mask = get_host_ip_address_and_mask(host_node)
    #iface.addAddress(PG.IPv4Address(host_if_ip_addr, mask))
    #iface.mac_address = base_host_mac_addr + "{:0>2d}".format(int(node_id[1:]))
sw_intf_count = 0
def setup_new_interface(node_id, iface):
    global sw_intf_count
    if iface and node_id[0] == 'h':
        setup_host_interface(node_id, iface)
        
        #host_if_ip_addr = base_ip_addr + node_id[1:]
        #iface.addAddress(PG.IPv4Address(host_if_ip_addr, "255.255.255.0"))
        #iface.mac_address = base_host_mac_addr + "{:0>2d}".format(int(node_id[1:]))
    else:
        sw_intf_count+=1
        #setup_switch_interface(node_id, iface)
        #host_if_ip_addr = "10.0." + str(if_num) + '.' + node_id[1:]
        #host_if_ip_addr = base_ip_addr + str(254-sw_intf_count)
        #iface.addAddress(PG.IPv4Address(host_if_ip_addr, "255.255.255.0"))
        #iface.mac_address = "{:0>2d}".format(int(node_id[1:])) + base_switch_mac_addr
    return    
    

def create_interface_for_node(node_id):
    global node_if_dict, node_dict, sw_intf_count
    iface = None
        
    if node_if_dict.has_key(node_id):
        cur_node_if_map = node_if_dict[node_id]
        if_num = len(cur_node_if_map) + 1
        if_name = 'eth%d' %if_num
        iface = node_dict[node_id].addInterface(if_name)
        iface.component_id = if_name
        cur_node_if_map[if_name] = iface
    else:
        if_num = 1
        if_name = 'eth%d' %if_num
        iface = node_dict[node_id].addInterface(if_name)
        iface.component_id = if_name
        node_if_dict[node_id] = {if_name:iface}
        
    return iface
def create_full_link(link_num):
    global rspec
    link = PG.LAN('link%d' %link_num)
    for iface in iface_list:
        link.addInterface(iface)
    if controller:
        link.addChild(controller)
    rspec.addResource(link)
def create_link_num(link_num, iface1, iface2):
    global rspec
    link = PG.LAN('link%d' %link_num)
    link.addInterface(iface1)
    link.addInterface(iface2)
    if controller:
        link.addChild(controller)
    rspec.addResource(link)
def add_all_ifaces_to_link(iface_list):
    global rspec
    link_num = 0
    link = PG.LAN('link%d' %link_num)
    for iface in iface_list:
        link.addInterface(iface)
    if controller:
        link.addChild(controller)
    rspec.addResource(link)
def set_subnet_for_interface(ttl_subnets,i, iface1, iface2):
    
    subnet_mask = "255.255.255." + str((256-(256/ttl_subnets)))
    base_num = (i*(256/ttl_subnets))
    ip_addr1 = "192.168.56." + str(base_num)
    ip_addr2 = "192.168.56." + str(base_num+1)
    iface1.addAddress(PG.IPv4Address(ip_addr1, subnet_mask))
    iface2.addAddress(PG.IPv4Address(ip_addr2, subnet_mask))


def setup_new_subnet_link(link_num,iface1,iface2):
    mask = "255.255.255.0"
    base_ip_addr = "10.0."
    ip1 = base_ip_addr + str(link_num) +'.1'
    ip2 = base_ip_addr + str(link_num) +'.2'
    iface1.addAddress(PG.IPv4Address(ip1, mask))
    iface2.addAddress(PG.IPv4Address(ip2, mask))
    create_link_num(link_num,iface1,iface2)
    return
def create_link_pair(link_num, ifnode_1, ifnode_2):
    iface1 = create_interface_for_node(ifnode_1)
    iface2 = create_interface_for_node(ifnode_2)
    setup_new_subnet_link(link_num,iface1,iface2)
    
    return
def create_all_links():
    global iface_list, subnet_list
    total_links=0
    #Build Links Information from Input
    setup_host_switch_links()
    setup_switch_switch_links()
    
    #Setup the Switch Links
    for conn in switch_connections:
        
        #Approach 1
        #iface1 = create_interface_for_node(conn[0])
        #setup_new_interface(conn[0],iface1)
        #iface2 = create_interface_for_node(conn[1])
        #setup_new_interface(conn[1],iface2)
        #create_link_num(total_links, iface1, iface2)
        
        ##Approach 2 and 3
        #iface_list.append(iface1)
        #iface_list.append(iface2)
        #subnet_list.append((iface1, iface2))
        
        #Approach 4
        total_links+=1
        create_link_pair(total_links,conn[0], conn[1])
        
    
    #setup the Host Links
    for conn in host_connections:
        
        ##Approach 1
        #iface1 = create_interface_for_node(conn[0])
        #iface2 = create_interface_for_node(conn[1])
        #create_link_num(total_links, iface1, iface2)
        
        ##Approach 2 and 3
        #iface_list.append(iface1)
        #iface_list.append(iface2)
        #subnet_list.append([iface1, iface2])
        
        #Approach 4
        total_links+=1
        create_link_pair(total_links,conn[0], conn[1])
    
    #Second Approach: Add subnet per link
    #subnets_num = 1
    #while(subnets_num < total_links): subnets_num*=2
    
    #for i, x in enumerate(subnet_list):
    #    create_link_num(i,x[0],x[1])
    #    set_subnet_for_interface(subnets_num,i, x[0], x[1])
    
    #for x in xrange(0,total_links):
    #    create_full_link(x)
    
    #Third way: adding all interfaces to 1 link
    #add_all_ifaces_to_link(iface_list)
    
    return
def create_a_connection():
    nl = node_dict.keys()
    iface1 = create_interface_for_node('h3')
    iface2 = create_interface_for_node('s3')
    
    #iface1 = node_dict['h4'].addInterface("eth1")
    #iface1.component_id = "eth1"
    #iface2 = node_dict['s1'].addInterface("eth1")
    #iface2.component_id = "eth1"
    
    create_link_num(1, iface1, iface2)
    return
    
create_all_nodes()
create_all_links()
#create_a_connection()
setup_tour()
pc.printRequestRSpec(rspec)