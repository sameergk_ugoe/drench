import geni.portal as portal
import geni.rspec.pg as pg

pc = portal.Context()
rspec = pg.Request()

node1 = pg.XenVM("node1")
iface1 = node1.addInterface("if1")
# Specify the component id and the IPv4 address
iface1.component_id = "eth1"
iface1.addAddress(pg.IPv4Address("192.168.1.1", "255.255.255.0"))
rspec.addResource(node1)

node2 = pg.XenVM("node2")
iface2 = node2.addInterface("if2")
# Specify the component id and the IPv4 address
iface2.component_id = "eth2"
iface2.addAddress(pg.IPv4Address("192.168.1.2", "255.255.255.0"))
rspec.addResource(node2)

link = pg.LAN("lan")
link.addInterface(iface1)
link.addInterface(iface2)


# Specify the URL for the disk image
##node1.disk_image = "<URL to disk image>"

# Install and execute scripts on the VM
##node1.addService(pg.Install(url="<URL to the tarball file>",path="local"))
##node1.addService(pg.Execute(shell="bash", command="<Path to executable>"))

rspec.addResource(link)
pc.printRequestRSpec(rspec)