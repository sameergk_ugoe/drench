import geni.portal as portal
import geni.rspec.pg as PG
import geni.rspec.igext as IG

rspec = PG.Request()

pc = portal.Context()
pc.defineParameter("H", "Number of Host Nodes",portal.ParameterType.INTEGER, 5)

pc.defineParameter("S", "Number of swithces Nodes",portal.ParameterType.INTEGER, 5)

pc.defineParameter("H_S_L", "Host To Switch Links (csv format h1:s2,h2:s3..)",portal.ParameterType.STRING, 'h1:s1')

pc.defineParameter("S_S_L", "Switch To Switch Links (csv format s1:s2,s2:s3..)",portal.ParameterType.STRING, 's1:s2')
params = pc.bindParameters()

def setup_tour():
    global rspec
    tour = IG.Tour()
    tour.Description(IG.Tour.TEXT, "Dynamic topo with GeniLib with variable number of nodes and switches")
    tour.Instructions(IG.Tour.MARKDOWN, "Links must be manipulated in the python code. ")
    rspec.addTour(tour)

#List of Hosts and Switches <'h1'.... 'hn'> and <'s1'.... 'sn'>  
node_list = [] 
#Dictionary of Hosts and Switches key = 'h1' value = PG.node
node_dict = {}
#Dictionary of Hosts and Switches Interface List = 'h1' value = {ethx: iface_node}
node_if_dict ={}
#Infor for Links 
host_connections = [] # [['h1', 's1'], ['h2', 's2'], ['h3', 's3'], ['h4', 's4'], ['h5', 's5']]
switch_connections = [] #[['s1', 's2'], ['s2', 's3'], ['s3', 's4'], ['s4', 's5']]
link_list = []

def setup_host_info():
    global node_list
    for i in range(1, params.H+1):
        node_list.append('h%d' %i)
def setup_switch_info():
    global node_list
    for i in range(1, params.S+1):
        node_list.append('s%d' %i)
def create_all_nodes():
    global node_dict
    global rspec
    setup_host_info()
    setup_switch_info()
    for h in node_list:
        h_node = PG.XenVM(h)
        node_dict[h] = h_node
        rspec.addResource(h_node)

def setup_host_switch_links():
    global host_connections
    input_links_str = params.H_S_L
    h_links = input_links_str.split(',')
    host_connections = [h.split(':') for h in h_links]
def setup_switch_switch_links():
    global switch_connections
    input_links_str = params.S_S_L
    h_links = input_links_str.split(',')
    switch_connections = [h.split(':') for h in h_links]
    
def setup_host_interface(host_node, iface):
    # Specify the IPv4 address
    host_if_ip_addr = "10.0.0." + host_node[1:]
    iface.addAddress(PG.IPv4Address(host_if_ip_addr, "255.255.255.0"))
def create_interface_for_node(node_id):
    global node_if_dict, node_dict
    iface = None
        
    if node_if_dict.has_key(node_id):
        cur_node_if_map = node_if_dict[node_id]
        if_num = len(cur_node_if_map) + 1
        if_name = 'eth%d' %if_num
        iface = node_dict[node_id].addInterface(if_name)
        iface.component_id = if_name
        cur_node_if_map[if_name] = iface
    else:
        if_num = 1
        if_name = 'eth%d' %if_num
        iface = node_dict[node_id].addInterface(if_name)
        iface.component_id = if_name
        node_if_dict[node_id] = {if_name:iface}
        
    if iface and node_id[0] == 'h':
        #setup_host_interface(node_id, iface)
        host_if_ip_addr = "10.0.0." + node_id[1:]
        iface.addAddress(PG.IPv4Address(host_if_ip_addr, "255.0.0.0"))
    else:
        #setup_switch_interface(node_id, iface)
        host_if_ip_addr = "10.0." + str(if_num) + '.' + node_id[1:]
        iface.addAddress(PG.IPv4Address(host_if_ip_addr, "255.0.0.0"))
    return iface
def create_link_num(link_num, iface1, iface2):
    global rspec
    link = PG.LAN('link%d' %link_num)
    link.addInterface(iface1)
    link.addInterface(iface2)
    rspec.addResource(link)
def create_all_links():
    total_links=0
    setup_host_switch_links()
    setup_switch_switch_links()
    
    for conn in switch_connections:
        iface1 = create_interface_for_node(conn[0])
        iface2 = create_interface_for_node(conn[1])
        create_link_num(total_links, iface1, iface2)
        total_links+=1
    
    for conn in host_connections:
        iface1 = create_interface_for_node(conn[0])
        iface2 = create_interface_for_node(conn[1])
        create_link_num(total_links, iface1, iface2)
        total_links+=1
def create_a_connection():
    nl = node_dict.keys()
    iface1 = create_interface_for_node('h3')
    iface2 = create_interface_for_node('s3')
    
    #iface1 = node_dict['h4'].addInterface("eth1")
    #iface1.component_id = "eth1"
    #iface2 = node_dict['s1'].addInterface("eth1")
    #iface2.component_id = "eth1"
    
    create_link_num(1, iface1, iface2)
    return
    
create_all_nodes()
create_all_links()
#create_a_connection()

pc.printRequestRSpec(rspec)