import socket
import sys
import subprocess
import commands
import csv
import netifaces
import time
import sys
import os.path
import os
client_id=None
client_mac=None
if_dict = {}
if_name_ip_add_map = []
mfest = None

def get_from_manifest():
    start_string = 'emulab:openflow_controller url=\"'
    end_string = '\"'
    s_of = mfest.find(start_string)
    ln = len(start_string)
    ctrl_mode_ip_port = mfest[s_of+ln: mfest.find(end_string,s_of+ln)]
    if ctrl_mode_ip_port:
        ctrl_info= ctrl_mode_ip_port.split(':')
        ctrl_info.pop(0)
        ctrl_ip_port = ':'.join([str(s) for s in ctrl_info])
    return ctrl_ip_port

def get_controller_ip_and_port():
    if mfest:
        return get_from_manifest()
    return "127.0.0.1:6633"
    return "192.168.56.101:6633"
    
def get_node_info():
    global client_id, client_mac, mfest
    client_mac = os.popen('geni-get control_mac').read().rstrip('\n')
    client_id = os.popen('geni-get client_id').read().rstrip('\n')
    mfest = os.popen('geni-get manifest').read().rstrip('\n')
    if mfest is None:
        os.system('geni-get manifest > /local/manifest.xml')
        with open('/local/manifest.xml', "r") as f:
            mfest = f.read()
    print "client_id[%s] client_mac[%s]" %(client_id, client_mac)
    
    return
def get_if_details_from_sys_cmd():
    global if_dict
    ifaces = os.popen('ip -o link show').read()
    ifcs_list = ifaces.split('\n')
    for f in ifcs_list:
        of = f.find(': ')
        ln = len(': ')
        f_name = f[of +ln: f.find(': <')] 
  
        of = f.find('link/ether ')
        ln = len('link/ether ')
        f_addr = f[of+ln: f.find(' brd')]
        f_addr = ''.join(f_addr.split(':'))
        if f_addr == client_mac: continue
        if_dict[f_name] = f_addr
        
    print "IF_DICT:[%s]" %(if_dict)
def get_if_details_from_emulab_file():
    global if_dict, if_name_ip_add_map
    with open("/var/emulab/boot/ifmap", "r") as f:
        for line in f:
            line = line.rstrip('\n')
            map_data = line.split(' ')
            if_name_ip_add_map.append(map_data)
            if_dict[map_data[0]] = (map_data[1], map_data[2])
    return len(if_name_ip_add_map)
def get_interface_details():
    #get_if_details_from_sys_cmd()
    get_if_details_from_emulab_file()
    return len(if_dict.keys())

def route_del_and_add(br_name, f):
    #for a_map in if_name_ip_add_map:
    ip_addr, mc_addr = if_dict[f]
    ip_addr_b = ip_addr[0:ip_addr.rfind('.')] + '.0'
    
    cmd = 'ifconfig ' + f + ' 0'
    os.system(cmd)
    
    cmd = 'sudo ip route del ' + ip_addr_b + '/24 dev ' + f
    os.system(cmd)
    
    if br_name:
      cmd = 'sudo ifconfig ' + br_name + ' ' + ip_addr
      os.system(cmd)
      cmd = 'sudo ip route add ' + ip_addr_b + '/24 dev ' + br_name
      os.system(cmd)
    
    return
def configure_interface_details():
    #br_name = 'br' + client_id
    br_name = 'br0'
    cmd = 'sudo ovs-vsctl add-br ' + br_name 
    os.system(cmd)
    
    # add all interfaces as ports to ovs-switch
    for f in if_dict.keys():
        route_del_and_add(None, f)
        #cmd = 'sudo ifconfig ' + f + ' 0'
        #os.system(cmd)
        cmd = 'sudo  ovs-vsctl add-port ' + br_name + ' ' + f 
        os.system(cmd)

    #Update Bridge control settings:
    controller_ip_port = get_controller_ip_and_port()
    cmd = 'sudo ovs-vsctl set-controller ' + br_name + ' tcp:' + controller_ip_port 
    os.system(cmd)
    
    cmd = 'sudo ovs-vsctl set bridge ' + br_name + ' protocol=OpenFlow10'

    os.system(cmd)

    cmd = 'sudo ovs-vsctl set-fail-mode ' + br_name + ' secure '
    #cmd = 'sudo ovs-vsctl set-fail-mode ' + br_name + ' standalone '
    os.system(cmd)

    cmd = 'sudo ovs-vsctl show'
    os.system(cmd)

if __name__ == '__main__':
    get_node_info()
    if client_id and 's' in client_id:
        get_interface_details()
        configure_interface_details()
    
