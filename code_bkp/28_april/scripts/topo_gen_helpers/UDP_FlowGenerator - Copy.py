#!/usr/bin/python

#
# Program to generate cli command list
#

#from mininet.log import setLogLevel
from random import randint
import sys
import csv
import time
import random
import argparse
import SvcChainGenerator
#UDP_FlowGenerator.py -c 3 -n 100 -t 0 -o dummy.txt -mf 6 -b 10.0.0.0 -mh 5

iptos_to_svc_chain_dict = {}
aChain = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
max_functions = len(aChain)
chain_length = 3
num_flows = 10
flow_type = 0
out_file = "flows.txt"
distribution = 0
ecn_mode = 0
base_ip_addr = "10.0.0.0"
max_hosts = 30

def parse_named_args():
    global chain_length, num_flows, flow_type, out_file, base_ip_addr, distribution, ecn_mode, aChain, max_functions, max_hosts
    is_parsed = False
    
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--chain_length', '-c' ,'-l', required=True,type=int, help='Service Chain Length')
    parser.add_argument('--num_flows', '-n' ,'-f', required=True, type=int, help='Number of Flows')
    parser.add_argument('--flow_type', '-t', required=True, type=int, help='Type of Flows <Mice=0/Elephant=1/Mix=2>')
    parser.add_argument('--out_file', '-o', required=True, help='Output to File')
    
    parser.add_argument('--max_functions', '-mf', type=int, help='Maximum Number of Service Chain Functions')
    parser.add_argument('--base_ip_addr', '-b', help='base_ip_addr')
    parser.add_argument('--distribution', '-d', type=int, help='Distribution Mode')
    parser.add_argument('--max_hosts', '-mh', type=int, help='Maximum Hosts in the system')
    parser.add_argument('--ecn', '-e', type=int, help='ECN Mode')
    
    args = parser.parse_args()
    
    if args.chain_length:
        chain_length = args.chain_length
        is_parsed = True
    if args.num_flows:
        num_flows = args.num_flows
        is_parsed = True
    if args.out_file:
        out_file = args.out_file
        is_parsed = True
    if args.out_file:
        out_file = args.out_file
        is_parsed = True
    
    if args.max_functions and args.max_functions > 0 and args.max_functions <15:
        aChain = [args.max_functions - x for x in xrange(0, args.max_functions)]
        max_functions = len(aChain)
    if args.base_ip_addr:
        base_ip_addr = args.base_ip_addr    
    if args.distribution:
        distribution = args.distribution
    if args.max_hosts:
        max_hosts = args.max_hosts
    if args.ecn:
        ecn_mode = args.ecn
    return is_parsed

def returnZipfDistribution(zipfExponent,numberOfServices):
  probabilityDistribution  = [0.0]*numberOfServices
  cdfOfDistribution        = 0.0
  for serviceIndex in xrange(numberOfServices):
    nominator  = 1/(pow(float(serviceIndex+1),zipfExponent))
    probabilityDistribution[serviceIndex]  = nominator
    cdfOfDistribution  += nominator
  for serviceIndex in xrange(numberOfServices):
    probabilityDistribution[serviceIndex]/=cdfOfDistribution
  #probability distribution
  #print 'Zipf distribution'
  #print probabilityDistribution
  return probabilityDistribution
def serviceChainChoice(serviceChainLength,probabilityDistribution):
  probability  = {}
  for index in xrange(len(probabilityDistribution)):
    probability[index]  = probabilityDistribution[index]
  serviceChain  = [None]*serviceChainLength
  for serviceIndex in xrange(serviceChainLength):
    randomNumber  = random.random()
    #print '---------'
    #print 'service choice iteration '+str(serviceIndex)
    #print 'random number: '+str(randomNumber)
    #print 'updated probability space'
    #print probability
    serviceCDF     =  0.0
    serviceChosen  = -1
    for element in probability:
      if serviceCDF>randomNumber:
        break
      serviceCDF    += probability[element]
      serviceChosen  = element
    serviceChain[serviceIndex]  = serviceChosen
    #print 'chosen service: '+str(serviceChosen)
    updateProbabilitySpace(probability,serviceChosen)
  print 'Service chain selected'
  print serviceChain
  return serviceChain
def updateProbabilitySpace(probability,serviceChosen):
  del probability[serviceChosen]
  sumOfProbabilitiesOfNewSpace  = 0.0
  for element in probability:
    sumOfProbabilitiesOfNewSpace  += probability[element]
  #estimate new probabilities
  for element in probability:
    probability[element]/=sumOfProbabilitiesOfNewSpace
#select service chain
def getServiceChain(min_len=1, max_len=2, fixed_len=2):
  numberOfServices         = len(aChain)
  serviceChainLength       =  fixed_len
  zipfExponent             =  0.3 #0.5
  
  #zipf distribution
  probabilityDistribution  = returnZipfDistribution(zipfExponent,numberOfServices)
  sc_index = serviceChainChoice(serviceChainLength,probabilityDistribution)
  sc = [aChain[x] for x in sc_index]
  return sc
def write_dict_to_csv(dict_file_name='dict.csv'):
    writer = csv.writer(open(dict_file_name, 'wb'))
    for key, value in iptos_to_svc_chain_dict.items():
        writer.writerow([key, value])
def read_to_dict_from_csv(dict_file_name='dict.csv'):
    reader = csv.reader(open('dict.csv', 'rb'))
    mydict = dict(reader)
    print mydict.keys()
    print "\n\n value for tos=8: ", mydict[str(8)]
def construct_iptos_to_svc_chain_dict(dict_file_name="svc_chain_dict.csv"):
    global iptos_to_svc_chain_dict
    for i in xrange(1,256):
        #if  i == 0 or i&0x01 or i&0x02==2: continue
        schain = getServiceChain(fixed_len=chain_length)
        iptos_to_svc_chain_dict[i] = schain
    #print iptos_to_svc_chain_dict
    write_dict_to_csv(dict_file_name)
    #read_to_dict_from_csv()
    return
def setup_svc_chain_paramsand_generate_dict(ch_len=chain_length, max_funcs=max_functions, out_file="svc_chain_dict.csv"):
    global aChain, max_functions, chain_length
    aChain = [max_funcs - x for x in xrange(0, max_funcs)]
    max_functions = len(aChain)
    construct_iptos_to_svc_chain_dict(out_file)
    return
    
def get_flow_size_inKB():
    flow_size = 10 
    if flow_type == 0:
        rand_multiplier = randint(1,10)
    elif flow_type == 1:
        rand_multiplier = randint(10,100)
    else:
        # change to adapt to mix of traffic pattern
        rand_multiplier = randint(1,100)
    flow_size = flow_size*rand_multiplier 
    return flow_size
def get_packet_size():
    pkt_size = 10 
    if flow_type == 0:
        rand_multiplier = 50
    elif flow_type == 1:
        rand_multiplier = 150
    else:
        # change to adapt to mix of traffic pattern
        rand_multiplier = randint(10,150)
    pkt_size = pkt_size*rand_multiplier 
    return pkt_size
def get_packets_per_sec():
    return 1000
def generate_flows_for_host(host_id, host_ip, host_file, max_flows_per_host=0):
    hf = open(host_file, "wb")
    
    nflows = 0
    tos_keys = iptos_to_svc_chain_dict.keys()
    hosts = [h for h in xrange(1,max_hosts+1) if h!= host_id]
    random.shuffle(hosts)
    for h in hosts: #xrange(1,max_hosts+1):
        if h == host_id: continue
        d_ip = base_ip_addr[0:base_ip_addr.rfind('.')] + '.' + str(h)
        rport = 9000 + host_id*100 + h
        cmd = '-T UDP -a ' + str(d_ip) + ' -rp ' + str(rport)
        random.shuffle(tos_keys)
        for key in tos_keys[0:1]:
            tos=int(key)
            fsize = get_flow_size_inKB()
            psize = get_packet_size()
            pktps = get_packets_per_sec()
            cmd += ' -b ' + str(tos) + ' -k ' + str(fsize) + ' -C ' + str(pktps) + ' -c ' + str(psize) + '\n'
            hf.write(cmd)
            nflows +=1
            if max_flows_per_host and nflows >= max_flows_per_host:
                break
        if max_flows_per_host and nflows >= max_flows_per_host: break
    hf.close()
    return
def generate_all_flows():
    global max_hosts
    max_flows_per_host = 0
    if num_flows <= max_hosts:
        max_flows_per_host = 1
    else:
        max_flows_per_host = int(num_flows/max_hosts)
    for h in xrange(1,max_hosts+1):
        h_ip = base_ip_addr[0:base_ip_addr.rfind('.')] + '.' + str(h)
        h_file = str(h_ip) + "_flows.txt"
        generate_flows_for_host(h, h_ip, h_file,max_flows_per_host)
if __name__ == '__main__':
  parse_named_args()
  construct_iptos_to_svc_chain_dict()
  generate_all_flows()
  #setLogLevel( 'info' )
  #initiate()