#!/usr/bin/python

#
# Program to generate cli command list
#

#from mininet.log import setLogLevel
from random import randint
import sys
import time
import random

#aChain = ['1011', '1101', '1111']
#aChain = ['1011', '1010', '1101', '1110','1111']
#aChain = ['0110', '0111', '1000', '1001', '1010', '1011', '1100', '1101', '1110','1111']
aChain = ['1010', '1011', '1100', '1101', '1110','1111']
MAX_HOST = 30
FLOW = 10
PING = 10
INTERVAL = .01  # @10ms => 100 packets per second => 100*100*8 = 80Kbps~100Kbps
def returnZipfDistribution(zipfExponent,numberOfServices):
  probabilityDistribution  = [0.0]*numberOfServices
  cdfOfDistribution        = 0.0
  for serviceIndex in xrange(numberOfServices):
    nominator  = 1/(pow(float(serviceIndex+1),zipfExponent))
    probabilityDistribution[serviceIndex]  = nominator
    cdfOfDistribution  += nominator
  for serviceIndex in xrange(numberOfServices):
    probabilityDistribution[serviceIndex]/=cdfOfDistribution
  #probability distribution
  #print 'Zipf distribution'
  #print probabilityDistribution
  return probabilityDistribution
def serviceChainChoice(serviceChainLength,probabilityDistribution):
  probability  = {}
  for index in xrange(len(probabilityDistribution)):
    probability[index]  = probabilityDistribution[index]
  serviceChain  = [None]*serviceChainLength
  for serviceIndex in xrange(serviceChainLength):
    randomNumber  = random.random()
    #print '---------'
    #print 'service choice iteration '+str(serviceIndex)
    #print 'random number: '+str(randomNumber)
    #print 'updated probability space'
    #print probability
    serviceCDF     =  0.0
    serviceChosen  = -1
    for element in probability:
      if serviceCDF>randomNumber:
        break
      serviceCDF    += probability[element]
      serviceChosen  = element
    serviceChain[serviceIndex]  = serviceChosen
    #print 'chosen service: '+str(serviceChosen)
    updateProbabilitySpace(probability,serviceChosen)
  print 'Service chain selected'
  print serviceChain
  return serviceChain
def updateProbabilitySpace(probability,serviceChosen):
  del probability[serviceChosen]
  sumOfProbabilitiesOfNewSpace  = 0.0
  for element in probability:
    sumOfProbabilitiesOfNewSpace  += probability[element]
  #estimate new probabilities
  for element in probability:
    probability[element]/=sumOfProbabilitiesOfNewSpace

#select number of flows
def getNumFlows():
  try:
    print "No of flows: %s"  %str(sys.argv[1])
    nFlow = int(sys.argv[1])
    return nFlow
  except:
    print "Please enter valid number of flows"

#select number of pings per flow
def getPingLen():
  try:
    print "No of packets per flow: %s"  %str(sys.argv[2])
    pings = int(sys.argv[2])
    return pings
  except:
    print "Please enter valid number of packet per flow"

#select number of pings per flow
def getPingInterval():
  try:
    print "packet_rate: %s"  %str(sys.argv[3])
    interval = float(sys.argv[3])
    return interval
  except:
    print "Please enter valid number of packet per flow"

#select flow schedule rate
def getFlowInterval():
  try:
    print "flow_schedule_rate: %s"  %str(sys.argv[4])
    interval = float(sys.argv[4])
    return interval
  except:
    print "Please enter valid number of flow_schedule_Rate"
    
    
#select service chain
def getServiceChain(min_len=1, max_len=2, fixed_len=2):
  numberOfServices         = len(aChain)
  serviceChainLength       =  fixed_len
  zipfExponent             =  0.3 #0.5
  #zipf distribution
  probabilityDistribution  = returnZipfDistribution(zipfExponent,numberOfServices)
  sc_index = serviceChainChoice(serviceChainLength,probabilityDistribution)
  sc = [aChain[x] for x in sc_index]
  return sc
  
  lenth = randint(1,len(aChain)) #3
  sc = aChain[0:]
  sch = 0
  random.shuffle(sc)
  #sc = random.sample(aChain,lenth)
  #return aChain
  
  #sc.append(',servicelist')
  
  #for x in range(lenth):
  #  sc.append(aChain[randint(0,len(aChain)-1)])
  #print "service chain: %s" %sc
  
  if not sc:
    sc.append('1101')
  #return sc[0:randint(1,max_len)]
  #return sc[0:min(max_len,len(sc))]
  sc[0] = '1101'
  return sc[0:1]

#ft_dict{100:20*60}
TIME_FOR_100_FLOWS_IN_SEC = 20*60
def get_ttl_duration(nFlow=10, nPing=100,nInterval=.01, nFlowInterval=.1):
  nMaxLaunchLength = nFlow*nFlowInterval
  nEachFlowLenght = nPing*nInterval
  return (TIME_FOR_100_FLOWS_IN_SEC*nFlow)/(float(100))

BURST_FLOW_COUNT = 5
def get_ping_interval_for_flow(flow_num = 1, nFlows = 1, nPing=500,nInterval=.01, nFlowInterval=.1):
  flow_n = (flow_num)/BURST_FLOW_COUNT
  if flow_n == 0: flow_n = 1
  nPingInterval = "{0:.2f}".format(nInterval*nFlows*BURST_FLOW_COUNT/float(flow_n))
  print "For Flow", flow_num, "selected PingInterval", nPingInterval
  return nPingInterval

#select source and destination
def selectHosts(nFlow=10, nPing=100,nInterval=.01, nFlowInterval=.1):
  #num = randint(1, len(network.hosts))
  nElastic = randint(0,1)
  nInterval2 = 5*nFlowInterval
  ttl_duration_in_sec = get_ttl_duration(nFlow=nFlow, nPing=nPing,nInterval=nInterval, nFlowInterval=nFlowInterval)
  ttl_bursts = int(nFlow/float(BURST_FLOW_COUNT))
  duration_per_burst_in_sec = float("{0:.2f}".format(ttl_duration_in_sec/float(3*ttl_bursts)))
  first_burst_duration = float("{0:.2f}".format((duration_per_burst_in_sec/float(ttl_bursts))))
  for x in range (nFlow):
    src=randint(1,MAX_HOST)
    dst=''
    dstn=0
    if x == 0 or x%BURST_FLOW_COUNT == 0:
      nPingInterval = "{0:.2f}".format(nInterval*nFlow/float(x+1))
    #nPingInterval = get_ping_interval_for_flow(flow_num = x, nFlows = nFlow, nPing=nPing,nInterval=nInterval, nFlowInterval=nFlowInterval)
    if x and x%BURST_FLOW_COUNT == 0:
      #nInterval2 = BURST_FLOW_COUNT*nFlowInterval
      duration_per_burst_in_sec -= first_burst_duration
      nInterval2 = duration_per_burst_in_sec
    else:
      nInterval2 = nFlowInterval/float(BURST_FLOW_COUNT)
    #if x and x%10 == 0:
    #  nInterval2 = abs(min(nInterval2-nInterval_step, nInterval2_max))
    sChain=getServiceChain()
    sc=','.join(map(str, sChain))
    while (dstn==src or dstn==0):
      dstn=randint(1,MAX_HOST)
    
    print "src: h%s" %str(src)
    print "dst: %s" %str(dstn)
    print "schain: %s" %sc
    command = "py net.get('h"+str(src)+"').cmd('sudo python gn_sniff_sk.py "+ str(sc) + " " + str(src) + " " + str(dstn) + " " + str(nPing) + " " + str(nFlow) + " " + str(nPingInterval) + " " + str(nElastic) + " -q &')"
    print command
    #with open('/home/mininet/flowlist.py', 'ab') as flist:
    fname = 'n5_' + str(nFlow) + 'f_' + str(nPing)+ 'p_' + str(nInterval*1000) +'Pms_' + str(nFlowInterval*1000) + 'Fms.py'
    #with open('flowlist.py', 'ab') as flist:
    with open(fname, 'ab') as flist:
      if x == 0:
        flist.write("sh ./launch_flow_count_capture.sh &" + "\n")
      flist.write(command+'\n')
      flist.write("py time.sleep(" + str(nInterval2) + ")" + "\n")


def initiate():
  global FLOW
  global PING
  global INTERVAL
  FLOW = getNumFlows()
  PING = getPingLen()
  INTERVAL =getPingInterval()
  FLOWINTERVAL = getFlowInterval()
  selectHosts(FLOW, PING, INTERVAL, FLOWINTERVAL)
    
if __name__ == '__main__':
  #setLogLevel( 'info' )
  initiate()