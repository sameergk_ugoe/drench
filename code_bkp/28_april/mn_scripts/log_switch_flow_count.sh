#!/bin/bash
sudo ./l2_dump_sflows.sh "$1" >  total_flows.txt
egrep -v "Dumping|NXST_FLOW|socket" -c total_flows.txt > all_flowcount.txt
egrep "udp" total_flows.txt >filtered_vlan_flows.txt
grep -c "udp" filtered_vlan_flows.txt > all_vlan_flowcount.txt
#egrep "dl_vlan" total_flows.txt | grep -v  "dl_vlan=0" > only_vlan_flows.txt 
egrep -v  "cookie=0x0" filtered_vlan_flows.txt > only_vlan_flows.txt 
grep -c "cookie" only_vlan_flows.txt > only_vlan_flow_count.txt
rm -rf total_flows.txt filtered_vlan_flows.txt only_vlan_flows.txt
