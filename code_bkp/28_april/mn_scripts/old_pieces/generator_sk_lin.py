#!/usr/bin/python

#
# Program to generate packets with vlan
#

import threading
import sys
from scapy.all import send, sendp, IP, ICMP, Dot1Q, Ether
from random import randint

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
#nvlan = 251 #[1111 1011] (BC)
#nvlan = 4029 #[1111 1011 1101] (BCA)
nvlan = 3069 #[1011 1111 1101] (CBA)
#nvlan = 3579 #[1101 1111 1011] (ABC)
#nvlan = 13 #(A)
#nvlan = 253 #[1111 1101] (BA)
nid = 1
nseq = 1
interval = 10

def getFunction():
    global nvlan
    global nid
    global nseq
    global interval
    nid = randint(0,99999)
    nseq = 1
    #print "func %s" %str(sys.argv[1])
    #try:
    #    nvlan = int(sys.argv[1])
    #    print "Chain: %s" %s(str(nvlan))
    #except:
    #    print "Please enter valid service chain"
    if(len(sys.argv)>2):
        interval = int(sys.argv[2])
    #print("Interval is set to: [%d]",%interval)
    if(len(sys.argv)>1):
        nvlan = int(sys.argv[1])
    #print("nvlan is set to: [%d]",%nvlan)
    #else:
    #    nvlan = int(sys.argv[1])

def sendPings():
    global nseq
    global interval
    print("Interval is set to: [%d]",interval)
    threading.Timer(interval, sendPings).start()
    sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
    nseq = nseq+1
  
if __name__ == '__main__':
    getFunction()
    sendPings()
