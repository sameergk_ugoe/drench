#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.topolib import TreeTopo
from mininet.node import RemoteController
from mininet.log import setLogLevel
from mininet.cli import CLI
import csv
import os.path
import random

MAX_SWITCHES = 50 #319  #5
MAX_HOSTS    = 10 #46   #125
topo_file = '/home/mininet/rftopo.csv'

def demo():
  "Simple Demo for FCSC"
  topo = Topo()

  if(os.path.isfile(topo_file)):
    pass
  else:
    print "No File Found! Return!!"
    return

  # Add switches
  switchlist = [topo.addSwitch('s%i' %i) for i in range(1,MAX_SWITCHES+1)]
  
  # Add nodes
  hostlist = [topo.addHost('h%i' %i, ip='10.0.0.%i' %i, ) for i in range(1,MAX_HOSTS+1)]

  # Add Switch to Switch Links
  connlist = []
  with open(topo_file, 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    link_count = 0
    for row in reader:
      sw1 = int(row['Source'])
      sw2 = int(row['Destination'])
      wt  = int(row['Weight'])
      print "SW1:%i, SW2:%i, Wt:%i" %(sw1,sw2,wt)
      if( ('s%i' %sw1 in switchlist) and ('s%i' %sw2 in switchlist) and 
          (((sw1,sw2) not in connlist) and ((sw2,sw1) not in connlist))):
        topo.addLink('s%i'%sw1,'s%i' %sw2)
        #topo.addLink('s%i'%sw1,'s%i' %sw2, bw=wt)
        #topo.addLink('s%i'%sw1,'s%i' %sw2, delay='%ims' %wt)
        link_count = link_count+1;
        connlist.append((sw1,sw2))
        print(" Added: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
      else:
        #print(" Skipped: SW1:%d, SW2:%d, Wt:%d",sw1,sw2,wt)
        pass
    print "[%i] Number of Unique Links Created" %link_count
  
  
  #TODO: Identify the Disjoint set of switches and try to add the links for them. 
  #   This would be needed for scaled down version of switches
  #disjoint_switches = [sw for sw in connlist
  #                      if p.dpid != dpid or p.port_num != port_num]
  
  
  # Add Switch to Host Links
  sws = range(1,MAX_SWITCHES+1)
  random.shuffle(sws)
  for i in range(1,MAX_HOSTS+1):
    topo.addLink('s%i'%sws[i%MAX_SWITCHES],'h%i' %i)

  net = Mininet(topo=topo, controller=RemoteController, autoSetMacs = True, autoStaticArp = True)
  c1 = net.addController(name = 'c1', controller = RemoteController, port = 6633 )
  
  for i, h in enumerate(net.hosts):
    mac = '00:00:00:00:00:00'
    if i <9: 
      mac = '00:00:00:00:00:0%i' %(i+1) 
    elif i < 99: 
      mac = '00:00:00:00:00:%i' %(i+1)
    else:
      k = (i+1)/100
      j = ((i+1)%100)/10
      l = ((i+1)%100)%10
      mac = '00:00:00:00:0%i:%i%i' %(k,j,l)
    h.setMAC(mac)
    #print "Host ", h.name, "has IP address", h.IP(), "and MAC address", h.MAC(), mac
    
  # Start network
  #net.build()
  c1.start()
  for s in net.switches[0:6]:
    s.start([c1])
  
  net.start()
  net.pingAll()
  for h in net.hosts[0:MAX_HOSTS]:
    #h.pexec(['xterm', 'python', '-u', 'hostprocess.py'])
    h.cmdPrint('sudo python -u hostprocess.py &')
  CLI( net )
  net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    demo()