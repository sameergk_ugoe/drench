#!/usr/bin/python

import subprocess
import commands
from scapy.all import sendp, sniff, IP, ICMP, Dot1Q, Ether, Raw, srp, srp1
import csv
import netifaces
import time
import sys
ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
payload = ",svclist,"
svc_list = []
host_interface = "Hx-eth0"
host_mac_addr  = "00:00:00:00:00:00"
host_ip_addr   = "10.0.0.0"
svc_rx_file = '/home/mininet/my_code/svc_rx.csv'
svc_tx_file = '/home/mininet/my_code/svc_tx.csv'
rx_file = '/home/mininet/my_code/rx.csv'
xx_file = '/home/mininet/my_code/xx.csv'
xy_file = '/home/mininet/my_code/xy.csv'
pkt_counter = 0

def getdata_fromTcpHeader(pkt):
  vlan = 0
  id = 0
  seq =0 
  src =""#esrc
  dst=""#edst
  srcip=""#ipsrc
  dstip="" #ipdst
  try:
    of = pkt.find('vlan')
    if of >0:
      ln = len('vlan ')
      vlan = int(pkt[of+ln: pkt.find(',',of)])
    else:
      vlan = 0
    #vlan  = int(pkt[pkt.find('vlan')+len('vlan '): pkt.find(',', pkt.find('vlan'))])
    
    of = pkt.find('id')
    ln = len('id')
    id   = int(pkt[of+ln: pkt.find(',', of)])
    #id  = int(pkt[pkt.find('id')+len('id'): pkt.find(',', pkt.find('id'))])
    
    of = pkt.find('seq')
    ln = len('seq')
    seq  = int(pkt[of+ln: pkt.find(',', of)])
    #seq  = int(pkt[pkt.find('seq')+len('seq'): pkt.find(',', pkt.find('seq'))])
    
    of = pkt.find(' ')
    ln = len(' ')
    src  = pkt[of+ln: pkt.find(' (', of)]
    #src  = pkt[pkt.find(' ')+len(' '): pkt.find(' (', pkt.find(' '))]
    
    of =pkt.find('> ')
    ln = len('> ')
    dst  = pkt[of+ln: pkt.find(' (',of)]
    #dst  = pkt[pkt.find('> ')+len('> '): pkt.find(' (',pkt.find('> '))]
    
    of = pkt.find('IPv4, ')
    ln = len('IPv4, ')
    srcip = pkt[of+ln: pkt.find(' >', of)]
    #srcip = pkt[pkt.find('IPv4, ')+len('IPv4, '): pkt.find(' >', pkt.find('IPv4, '))]
    
    of = pkt.find(srcip+' > ')
    ln = len(srcip+' > ')
    dstip = pkt[of+ln: pkt.find(':', of)]
    #dstip = pkt[pkt.find(srcip+' > ')+len(srcip+' > '): pkt.find(':', pkt.find(srcip+' > '))]
  except:
    print " Parse for Header Data Failed!!"
  return vlan,id,seq,src,dst,srcip,dstip

def getdata_fromPayload(pkt):
  vlan = 0
  nvlan = 0
  npayload = ',svclist,end,'
  svlan = None
  snvlan = None
  try:
    pkt = pkt.rstrip()
    of = pkt.find(',svclist,')
    ln = len(',svclist,')
    svlan = pkt[of+ln: pkt.find(',',of+ln)]
    if svlan is None or len(svlan) == 0 or False == svlan.isdigit():
      vlan = 0
      nvlan = 0
      #sp = 20
      npayload = ',svclist,'+ pkt[of+ln:]
      #npayload = ',svclist,'+ pkt[of+ln:] + str(sp) +','
    else:
      vlan = int(svlan,2)
      snvlan = pkt[of+ln+len(svlan+','):pkt.find(',',of+ln+len(svlan+','))]
      SP = 35
      if snvlan is None or len(snvlan) == 0 or False == snvlan.isdigit():
        sp = 15
        nvlan = 0
      else:
        nvlan = int(snvlan,2)
        sp = 10
      npayload = ',svclist,'+ pkt[of+ln+len(svlan +','):] + str(sp) +','
  except Exception, e:
    print "Parse for Payload data Failed!!", str(e)
  return vlan, nvlan, npayload

def write_pkt(vlan=0, id=1, seq=1):
  svc_rx_file_func = svc_rx_file.split('.')[0] + '_' + str(vlan) +'.'+svc_rx_file.split('.')[1]
  with open(svc_rx_file_func, 'ab') as csvfile:
        rx1Writer = csv.writer(csvfile,delimiter=',')
        rx1Writer.writerows([[str(time.time()), str(id), str(seq), str(vlan), str(host_ip_addr), str(host_interface)]])
  return
def write_pkt2(vlan=0, id=1, seq=1):
  svc_tx_file_func = svc_tx_file.split('.')[0] + '_' + str(vlan) +'.'+svc_tx_file.split('.')[1]
  with open(svc_tx_file_func, 'ab') as csvfile1:
        tx1Writer = csv.writer(csvfile1,delimiter=',')
        tx1Writer.writerows([[str(time.time()), str(id), str(seq), str(vlan), str(host_ip_addr), str(host_interface)]])
  return

def log_pkt(id=1, seq=1,srcip=""):
  with open(rx_file, 'ab') as csvfile3:
    rxWriter = csv.writer(csvfile3,delimiter=',')
    rxWriter.writerows([[str(time.time()), str(id), str(seq), str(srcip), str(host_ip_addr), str(host_interface)]])
  return
  
def log_garbage_pkt(id=1, seq=1,srcip="",dstip=""):
  with open(xx_file, 'ab') as csvfile2:
    xxWriter = csv.writer(csvfile2,delimiter=',')
    xxWriter.writerows([[str(time.time()), str(id), str(seq), str(srcip), str(dstip),str(host_interface),str(host_ip_addr)]])
  return
def log_sniff_pkt(vlan=0, id=1, seq=1,srcip="",dstip="",payload=""):
  xy_file_func = xy_file.split('.')[0] + '_' + str(host_ip_addr[host_ip_addr.rfind('.'):]) +'.'+xy_file.split('.')[1]
  with open(xy_file_func, 'ab') as csvfile4:
    xyWriter = csv.writer(csvfile4,delimiter=',')
    xyWriter.writerows([[str(time.time()), str(id), str(seq), str(srcip), str(dstip),str(host_ip_addr), str(payload)]])
  return
  
def createPkt3(nvlan=0, nid=1, nseq=1, ipsrc=ipsrc, ipdst=ipdst, ndst=edst, nsrc=esrc, npayload=payload):
  #if nvlan>0:
    #sendp(Ether(src=nsrc,dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
    sendp(Ether(dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
  #else:
  #  #sendp(Ether(src=nsrc,dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
  #  sendp(Ether(dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)

def parseDump(op):
  
  #print "tcpdump is:", op
  vlan,id,seq,src,dst,srcip,dstip = getdata_fromTcpHeader(op)
  #print "vlan:%d, id:%d, seq:%d, src:%s, dst:%s, srcip:%s, dstip:%s, host_ip_addr:%s" %(vlan,id,seq,src,dst,srcip,dstip,host_ip_addr)
  if vlan>0:
    ovlan2,data = getdata_fromPayload(op)
    #print "current_vlan= %d, next_vlan= %d, data=%s" %(vlan,ovlan2,data)
    createPkt3(nvlan=ovlan2, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=host_mac_addr,npayload=data)
    write_pkt(vlan,id,seq)
  else:
    # Write only if this host is destination
    if dstip == host_ip_addr:
      with open(rx_file, 'ab') as csvfile:
        rxWriter = csv.writer(csvfile,delimiter=',')
        rxWriter.writerows([[str(time.time()), str(id), str(seq), str(srcip), str(host_ip_addr), str(host_interface)]])
    else: # unknown packet reached this host <loop or blackhole case>
      with open(xx_file, 'ab') as csvfile2:
        xxWriter = csv.writer(csvfile2,delimiter=',')
        xxWriter.writerows([[str(time.time()), str(id), str(seq), str(srcip), str(dstip), str(host_ip_addr)]])
    return


def isIncoming(pkt):
  a=""
  b = False
#print "Packet", pkt.show()
#print "Packet has: %s " %pkt.keys()
#print "FeildType Keys::%s" %pkt.fieldtype.keys()
#print "FieldKeys::%s" %pkt.fields.keys()
#print "Packet Fields: %s" %pkt.packetfields
#print "Packet Field Desc: %s" %pkt.fields_desc
# problem 1: No way to get the VLAN for incoming packets as Scapy strips Dot1Q on incoming packets <known scapy bug, no fix yet>
# problem 2: Sniff captures both Tx and Rx packets, Need some mechanism to skip the Tx packets sent from this host, <Dot1Q layer> 

  try:
    if pkt[IP].src == host_ip_addr:
      return False 
    if pkt.haslayer(Dot1Q):
      b = True
      #print "Packets Has Dot1Q:"
      #print "Dot1Q Pkt:", pkt.show()
      #print "Packets Dot1Q:%d" %pkt[Dot1Q].vlan
    else:
      #print "Packet does not have Dot1Q"
      pass
    
    #if pkt[ICMP].type==8 and host_ip_addr != pkt[IP].src and pkt[IP].dst != host_ip_addr:
      #log_sniff_pkt(vlan= 0, id=int(pkt[ICMP].id), seq=int(pkt[ICMP].seq),srcip=str(pkt[IP].src),dstip=str(pkt[IP].dst),payload=str(pkt[ICMP].payload))
      #pass
    # There is no way to know if the request is intened for this service or not, just assume True for all ICMP request packets sniffed anf ignore the (Tx)packets with Dot1Q!!
    if pkt[ICMP].type==8 and b is False: #pkt.haslayer(Dot1Q):
      #print " Regular ICMP Echo Request, Rx packet:", pkt.show()
      return True
  except:
    pass
  return False

def PacketHandler(pkt):
  id = int(pkt[ICMP].id)
  seq = int(pkt[ICMP].seq)
  cpayload = str(pkt[ICMP].payload)
  srcip = str(pkt[IP].src)
  dstip = str(pkt[IP].dst)
  src = str(pkt[Ether].src)
  dst = str(pkt[Ether].dst)
  vlan,nvlan,payload = getdata_fromPayload(cpayload)
  #print "vlan:%d, nvlan:%d id:%d, seq:%d, src:%s, dst:%s, srcip:%s, dstip:%s, host_ip_addr:%s" %(vlan,nvlan,id,seq,src,dst,srcip,dstip,host_ip_addr)
  #print "cpayload=",cpayload, "payload=",payload
  #log_sniff_pkt(vlan= vlan, id=id, seq=seq,srcip=srcip,dstip=dstip,payload=cpayload)
  
  if vlan > 0:
    createPkt3(nvlan=nvlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst, nsrc=src, npayload=payload)
    write_pkt(vlan,id,seq)
    #if nvlan > 0:
    #  write_pkt2(nvlan,id,seq)
    #  pass
  else:
    if dstip == host_ip_addr:
      log_pkt(id=id, seq=seq, srcip=srcip)
    else:
      # Send as this could be the last service in chain
      #createPkt3(nvlan=vlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst, nsrc=src, npayload=payload)
      log_garbage_pkt(id=id, seq=seq, srcip=srcip, dstip=dstip)
  
def getPacket():
  # Get interface name
  time.sleep(10)
  #print "start reading interface details!!"
  interface_list = netifaces.interfaces()
  interface = filter(lambda x: 'eth0' in x,interface_list)
  if len(interface) == 0:
    print "No interface found"
    return
  
  global host_interface
  global host_mac_addr
  global host_ip_addr
  
  addrs      = netifaces.ifaddresses(interface[0])
  link_addrs = addrs[netifaces.AF_LINK]
  net_addrs  = addrs[netifaces.AF_INET]
  host_interface  = interface[0]
  host_mac_addr   = link_addrs[0]['addr']
  host_ip_addr    = net_addrs[0]['addr']

  #print "host_interface:", host_interface, "host_mac_addr:", host_mac_addr, "host_ip_addr:", host_ip_addr
  #Send Dummy packet to help discover the host in the controller
  dnum =  int(host_ip_addr[(host_ip_addr.rfind('.')+1):])
  if dnum == 1 or dnum == 0: 
    dnum = 2
  else:
    dnum -= 1
  dst_ip_addr = host_ip_addr[0:host_ip_addr.rfind('.')] + '.' + str(dnum)
  dst_mac_addr = host_mac_addr[0:host_mac_addr.rfind(':')]+':'+ "{:0>2d}".format(dnum)
  #if dnum < 10:
  #  dst_mac_addr = host_mac_addr[0:host_mac_addr.rfind(':')]+':0'+str(dnum)
  #else:
  #  dst_mac_addr = host_mac_addr[0:host_mac_addr.rfind(':')]+':'+str(dnum)
  #print "sending Packet to Neighbour:", dst_ip_addr, dst_mac_addr
  #sendp(Ether(src=host_mac_addr,dst=dst_mac_addr)/Dot1Q(vlan=0)/IP(src=host_ip_addr,dst=dst_ip_addr))
  ans,unans=srp(Ether(src=host_mac_addr,dst=dst_mac_addr)/Dot1Q(vlan=0)/IP(src=host_ip_addr,dst=dst_ip_addr),timeout=1)
  #if ans is not None:
  #  print ans.show()
  #ans,unans=srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst="192.168.1.0/24"),timeout=2)
  #time.sleep(3)
  #Start the SNIFF and Forward/Log Sniffed Packets
  comTcpDump = sniff(iface=interface[0], lfilter = isIncoming, prn = PacketHandler)
  
if __name__ == '__main__':
    getPacket()

