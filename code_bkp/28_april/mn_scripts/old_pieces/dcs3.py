"""Custom topology example

######### works with notree data center
sudo mn --custom dcsimple.py --topo simple
or

sudo mn --custom dcsimple.py --topo simple --controller remote
and
./pox.py samples.spanning_tree

########

"""

from mininet.topo import Topo 

class Simple( Topo ):
    "Simple topology of depth 3 "

    def __init__( self, depth=3,fan_out=2,**kwargs ): #"Create custom topo."

        # Add default members to class.
        super( Simple, self ).__init__()
        
        #total_num_of_nodes+= [for i in range(depth+1) pow(fan_out,i)]
        total_num_of_nodes = sum(pow(fan_out,i) for i in range(depth+1) ) 
        num_of_hosts       = pow(fan_out,depth)
        num_of_switches    = total_num_of_nodes - num_of_hosts
        print "Input[Depth=%d, Fan_Out=%d] ==> (total_nodes=%d, num_hosts=%d, num_switches=%d)" %(depth, fan_out,total_num_of_nodes,num_of_hosts,num_of_switches)
        
        # Create hosts
        hostlist = [ self.addHost('h%d' %i)
                 for i in range(1,num_of_hosts+1)]

        # Create switches
        switchlist = [ self.addSwitch('s%d' %i)
                       for i in range(num_of_hosts+1,num_of_hosts+num_of_switches+1) ]

        k = 1                   # index for the number of hosts
        i = num_of_hosts+1      # index for the number of switches (start_from)
        j = num_of_hosts+1      # index for the number of switches (destination links)
        
        # Counter for the Total number of Connections setup by the below code
        total_count_of_links = 0
        #print "\n Outer Loop Iteration counts=%d" %(depth+1)
        for d in range(depth):
            j = (i+ pow(fan_out,d))
            cur_count_of_links = 0
            #print "\n Inner loop iteration counts= %d" %(pow(fan_out,d+1)) #%(fan_out*(d+1))
            for f in range(pow(fan_out,d+1)):
                print "\n At Depth=%d,and Fan_out=%d::" %(d,f)
                # Connect the switches to the leaf hosts
                if d==depth-1:
                    print "Adding Switch to Host link, sw_num=%d,ho_num=%d" %(i,k)
                    self.addLink('s%i'%i,'h%i'%k )
                    cur_count_of_links+=1
                    total_count_of_links+=1
                    k+=1
                    # Connect the switches to non-leaf switches
                else:
                    print"Adding Swith to Switch Link Sw%d Sw%d" %(i,j)
                    self.addLink('s%i'%i,'s%i'%j)
                    cur_count_of_links+=1
                    total_count_of_links+=1
                    j+=1
                if cur_count_of_links == fan_out:
                    cur_count_of_links = 0
                    if f < (pow(fan_out,d+1)-1):
                        #print "incrementing I to %d for f=%d less than %d" %(i,f, (fan_out*(d+1)-1))
                        i+=1
            i+=1
        print"\n Total_Links_Added=%d" %(total_count_of_links)


class Simple2( Topo ):
  "Simple topology of depth 3 "

  def __init__( self, depth=3,fan_out=2,**kwargs ): #"Create custom topo."

    # Add default members to class.
    super( Simple2, self ).__init__()

    # Create hosts
    hostlist = [ self.addHost('h%d' %i)
           for i in range(1,num_of_hosts+1)]

    # Create switches
    switchlist = [ self.addSwitch('s%d' %i)
                 for i in range(num_of_hosts+1,num_of_hosts+num_of_switches+1) ]

    print"\n Total_Links_Added=%d" %(total_count_of_links)


class STT(Topo):
  "Simple Test Topology "
  def __init__( self, num_of_hosts=6,num_of_switches=3,num_of_links =0,**kwargs ):
    
    # Add default members to class.
    super( STT, self ).__init__()
    
    # Create hosts
    hostlist = [ self.addHost('h%d' %i) for i in range(1,num_of_hosts+1)]

    # Create switches
    switchlist = [ self.addSwitch('s%d' %i) for i in range(1,num_of_switches+1) ]
        
    # Create Links
    self.addLink('s1','h1')
    self.addLink('s2','h2')
    self.addLink('s3','h3')
    self.addLink('s4','h4')
    self.addLink('s5','h5')
    self.addLink('s5','h6')
    #switch links
    self.addLink('s5','s1')
    self.addLink('s1','s2')
    self.addLink('s2','s4')
    self.addLink('s5','s4')
    self.addLink('s4','s3')


topos = { 'test': ( lambda: STT(num_of_hosts=6,num_of_switches=5,num_of_links=10) ),
         'test2': ( lambda: Simple2(depth=3,fan_out=2) ),
         'simple': ( lambda: Simple(depth=3,fan_out=2) ),
         'simple1_2': ( lambda: Simple(depth=1,fan_out=2) ),
         'simple2_2': ( lambda: Simple(depth=2,fan_out=2) ),
         'simple3_2': ( lambda: Simple(depth=3,fan_out=2) ),
         'simple4_2': ( lambda: Simple(depth=4,fan_out=2) ),
         'simple1_3': ( lambda: Simple(depth=1,fan_out=3) ),
         'simple2_3': ( lambda: Simple(depth=2,fan_out=3) ),
         'simple3_3': ( lambda: Simple(depth=3,fan_out=3) ),
         'simple4_3': ( lambda: Simple(depth=4,fan_out=3) )}
