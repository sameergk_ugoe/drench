#!/usr/bin/python

#
# Program to generate packets with vlan
#
import subprocess
import commands
from scapy.all import sendp, sniff, IP, ICMP, Dot1Q, Ether, Raw
import csv
import netifaces
import time
import sys
import threading
import sys
from random import randint

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
#payload = ",svclist="
payload = ",svclist,"
#nvlan = 251 #[1111 1011] (BC)
#nvlan = 4029 #[1111 1011 1101] (BCA)
nvlan = 3069 #[1011 1111 1101] (CBA)
#nvlan = 3579 #[1101 1111 1011] (ABC)
#nvlan = 13 #(A)
#nvlan = 253 #[1111 1101] (BA)
sChain = []
nid = 1
nseq = 1
interval = 0.5
count = 10
flow_id = 0
tx_file = '/home/mininet/my_code/tx.csv'
def writeData(id=nid, seq=nseq, idst=ipdst, dst=edst, flow=flow_id):
    with open(tx_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), id, seq, flow_id, ipsrc,esrc, idst,edst]])

def getFunction2():
    
    interface_list = netifaces.interfaces()
    interface = filter(lambda x: 'eth0' in x,interface_list)
    if len(interface) == 0:
        print "No interface found"
        return
  
    global ipsrc
    global esrc

    addrs      = netifaces.ifaddresses(interface[0])
    link_addrs = addrs[netifaces.AF_LINK]
    net_addrs  = addrs[netifaces.AF_INET]
    host_interface  = interface[0]
    host_mac_addr   = link_addrs[0]['addr']
    host_ip_addr    = net_addrs[0]['addr']
    esrc = host_mac_addr
    ipsrc = host_ip_addr
    
    global nvlan
    global sChain
    global nid
    global nseq
    global interval
    global flow_id
    global count
    global ipdst
    global edst

    nid = randint(0,99999)
    nseq = 1
    src_id = 0
    dst_id = 0
    try:
        #parse arguments <svc_chain, esrc, edst, count, flow_id, ping_interval>
        i = 6
        if(len(sys.argv)>i):
            ival = float(sys.argv[i])
            if ival > 0:
              interval = ival
        i -=1
        
        if(len(sys.argv)>i):
            flow_id = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            count = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            dst_id = int(sys.argv[i])
            ipdst = "10.0.0."+ sys.argv[i]
            if dst_id < 10:
                edst = "00:00:00:00:00:0"+sys.argv[i]
            else:
                edst = "00:00:00:00:00:"+sys.argv[i]
        i -=1
        
        if(len(sys.argv)>i):
            src_id = int(sys.argv[i])
            #print src
        i-=1
        
        if(len(sys.argv)>i):
            sChain = sys.argv[1].split(",")
            nvlan = int(sChain.pop(0),2)
            #print "vlan: %s" %str(nvlan)
        print "Generator Triggered on: %s with: From:%d To:%d, Service Chain:%s, First Service: %d , num_packets:%d and Flow_ID:%d, ping_interval:%f " %(host_ip_addr,src_id,dst_id,sChain,nvlan,count,flow_id,interval)
    except Exception,e:
        print str(e)
        exit()    
    
def sendPings2():
    global nseq
    global flow_id
    if not sChain:
        try:
            print " in send packets:"
            d= '0'
            payload = ",svclist," + d
            print "payload:", payload
            sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload)
        except Exception,e:
            exit()  
    else:
        d= ','.join(map(str, sChain))
        payload = ",svclist," + d
        print "payload with sChain:", payload
        try:
            sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload)
        except Exception,e:
            exit()
    writeData(id=str(nid), seq=str(nseq), idst=str(ipdst),dst=str(edst),flow=flow_id )
    nseq = nseq+1

def startPings2():
    global count
    print "In startPings2, %d" %count
    for x in range(count):
        try:
            sendPings2()
            time.sleep(interval)
        except Exception,e:
            print str(e)
            exit()

def getFunction():
    global nvlan
    global nid
    global nseq
    global interval
    nid = randint(0,99999)
    nseq = 1
    #print "func %s" %str(sys.argv[1])
    #try:
    #    nvlan = int(sys.argv[1])
    #    print "Chain: %s" %s(str(nvlan))
    #except:
    #    print "Please enter valid service chain"
    if(len(sys.argv)>2):
        interval = int(sys.argv[2])
    #print("Interval is set to: [%d]",%interval)
    if(len(sys.argv)>1):
        nvlan = int(sys.argv[1])
    #print("nvlan is set to: [%d]",%nvlan)
    #else:
    #    nvlan = int(sys.argv[1])

def sendPings():
    global nseq
    global interval
    global nvlan
    data = payload + str(nvlan)
    cur_vlan = nvlan & 0xF
    print("svc_chain:[%d], first vlan: [%d], Interval: [%d]",nvlan, cur_vlan,interval)
    #threading.Timer(interval, sendPings).start()
    #sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
    sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=cur_vlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq)/data)
    writeData(str(nid), str(nseq), str(ipdst))
    nseq = nseq+1
  
if __name__ == '__main__':
    #getFunction()
    #sendPings()
    getFunction2()
    startPings2()
