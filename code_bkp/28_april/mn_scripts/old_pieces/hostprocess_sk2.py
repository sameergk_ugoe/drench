#!/usr/bin/python

import subprocess
import netifaces
import commands
from scapy.all import sendp, IP, ICMP, Dot1Q, Ether

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"

def getID(list):
  id = filter(lambda x: 'id' in x, list)
  list = id[0].split('id ')
  return int(list[1])

def getVLAN(list):
  vlan = filter(lambda x: 'vlan' in x, list)
  list = vlan[0].split('vlan ')
  return int(list[1])
  
def modVLAN(ovlan):
  nvlan = ovlan >> 4
  if nvlan == 0:
    return 0
  else:
    return nvlan
  
def getSEQ(list):
  id = filter(lambda x: 'seq' in x, list)
  list = id[0].split('seq ')
  return int(list[1])

def createPkt(nvlan, nid, nseq):
  if nvlan>0:
    #sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
    sendp(Ether(dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
  else:
    #sendp(Ether(src=esrc, dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    sendp(Ether(dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
  
def parseDump(op):
  list = op.split(', ')
  vlan = getVLAN(list)
  if vlan>0:
    id = getID(list)
    seq = getSEQ(list)
    nvlan= modVLAN(vlan)
    createPkt(nvlan, id, seq)
  else:
    return

def getPacket():    
  # Get interface name
  interface_list = netifaces.interfaces()
  interface = filter(lambda x: 'eth0' in x,interface_list)
  if len(interface) == 0:
    print "No interface found"
    return
  
  addrs      = netifaces.ifaddresses(interface[0])
  link_addrs = addrs[netifaces.AF_LINK]
  net_addrs  = addrs[netifaces.AF_INET]
  mac_addr   = link_addrs[0]['addr']
  ip_addr    = net_addrs[0]['addr']
  
  global esrc
  global ipsrc
  
  esrc  = mac_addr
  ipsrc = ip_addr
  print "mac_addr", esrc, "ip_addr=", ipsrc
  
  # Create tcpdump command
  comTcpDump = "sudo tcpdump icmp[icmptype] == 8 -e -l -i " + interface[0]
  comTcpDump = "sudo tcpdump -l -i " + interface[0] + " -Uw - | tcpdump -l  -en -r - vlan"
  # Create subprocess to execute command
  p = subprocess.Popen(comTcpDump, stdout=subprocess.PIPE, shell=True)
  lines = iter(p.stdout.readline, b'')
  for line in lines:
    parseDump(line)
  
if __name__ == '__main__':
    getPacket()