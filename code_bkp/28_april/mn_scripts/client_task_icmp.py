import socket
import sys
import subprocess
import commands
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

#from scapy.all import conf
#conf.use_pcap = True
#import scapy.arch.pcapdnet

from scapy.all import sendp, sniff, IP, ICMP, Dot1Q, Ether, Raw, srp, srp1, sr1, send, TCP, UDP, wrpcap, conf, fragment
from scapy.utils import PcapWriter
import csv
import netifaces
import time
import sys
from os.path import expanduser
import os.path
from random import randint, normalvariate, gauss
from threading import Thread, Timer

host_interface = "Hx-eth0"
host_mac_addr  = "00:00:00:00:00:00"
host_ip_addr   = "10.0.0.0"
hdir_p = expanduser('~')
path_name = hdir_p + '/mptcp/results/'
tx_flows_file = path_name + 'tx_flows.csv'
rate_update_file = path_name + 'send_rate.csv'

l2_socket = None
l3_socket = None
SERVER_IP_ADDRESS = "10.0.0.0"
SERVER_MAC_ADDRESS = "00:00:00:00:00:00"

UDP_SERVER_PORT = 10001
TCP_SERVER_PORT = 10002
TCP_CLIENT_PORT = 10003
UDP_CLIENT_PORT = 10004

MAX_WAIT_TIME_FOR_RESP = 1

TCP_PACKET_SIZE = 4096
UDP_DGRAM_SIZE  = 4096

pktdumper = None
#Default Flow Rate in Kbps (10Mbps)
Flow_Rate = 1000*10
MAX_FLOW_RATE = 1000*100 #100Mbps to 1Gbps )1=1Kbps
#Default Flow Type [MICE/ELEPHANT]
Flow_Type     = 0
#Default flow Duration 10Seconds (3~10seconds for Mice and 15~30seconds for Elephant)
Flow_Duration = 30
#Default flow Mode is UDP[0] (TCP=1)
Flow_Mode = 0
Stop_Flag = 0

KILO_SIZE = 1024
BYTE_SIZE = 8
SEC_TO_MS = 1000
FIXED_INTERVAL_100ms = 100
FIXED_BSIZE_1400B = 1400
Rate_Update_Interval = 100
UDP_HEADER_SIZE = 8

sChain = []
schain_seq = 0
nid = 1
nseq = 1
interval = 0.5
count = 10
flow_id = 0
path_cost = 0
done = False
nElastic = 0
flow_rate = 0
ipdst = "10.0.0.0"
edst = "00:00:00:00:00:00"
payload = ",svclist,"
hdir_p = expanduser('~')
hdir_p = hdir_p + '/drench/results'
tx_file = hdir_p + '/tx.csv' #'/home/mininet/drench/results/tx.csv'
svc_tx_file = hdir_p + '/svc_tx.csv' # '/home/mininet/drench/results/svc_tx.csv'
svc_ts_file = hdir_p + '/svc_ts.csv' #'/home/mininet/drench/results/svc_ts.csv'
tx_flows_file = hdir_p + '/tx_flows.csv' #'/home/mininet/drench/results/tx.csv'

def writeRateData(rate_in_kbps):
    global rate_update_file
    with open(rate_update_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), rate_in_kbps]])


def getInterfaceInfo():
    # Get interface name
    #print "start reading interface details!!"
    interface_list = netifaces.interfaces()
    interface = filter(lambda x: 'eth0' in x,interface_list)
    if len(interface) == 0:
      print "No interface found"
      return
    
    global host_interface
    global host_mac_addr
    global host_ip_addr
    global l2_socket
    global l3_socket
    
    addrs      = netifaces.ifaddresses(interface[0])
    link_addrs = addrs[netifaces.AF_LINK]
    net_addrs  = addrs[netifaces.AF_INET]
    host_interface  = interface[0]
    host_mac_addr   = link_addrs[0]['addr']
    host_ip_addr    = net_addrs[0]['addr']
    
    l2_socket =  conf.L2socket(iface=host_interface)
    l3_socket =  conf.L3socket(iface=host_interface)
    #print "host_interface:", host_interface, "host_mac_addr:", host_mac_addr, "host_ip_addr:", host_ip_addr
    return host_interface, host_mac_addr, host_ip_addr

def writeFlowData(id=0, seq=0, idst=None, dst=None, flow=0, flow_rate=0, rtt=0,schain=[]):
    with open(tx_flows_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), id, seq, flow_id, host_ip_addr,str(schain),idst,flow_rate,rtt]])       
def writeData(id=0, seq=0, idst=None, dst=None, flow=0, flow_rate=0, rtt=0):
    with open(tx_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), id, seq, flow_id, flow_rate,rtt, host_ip_addr,idst]])
        #txWriter.writerows([[str(time.time()), id, seq, flow_id, host_ip_addr,esrc, idst,edst]])
MAX_WAIT_TIME_FOR_RESP = 0.5
icmp_pkt  = Ether(src=host_mac_addr)/Dot1Q(vlan=0)/IP(src=host_ip_addr)/ICMP(id=nid) #/payload
icmp_pkt2 = Ether(src=host_mac_addr)/Dot1Q(vlan=0)/IP(src=host_ip_addr)/ICMP(id=nid, seq=0)
partial_load = ""
def create_icmp_pkt():
    global icmp_pkt, payload, partial_load
    global nseq, nid, payload, flow_id, ipdst, edst, nvlan
    if not sChain:
        d=',end,'
        payload = ",svclist," + d
        #print "payload without Schain:", payload
    else:
        d= ','.join(map(str, sChain)) +',end,' + '{0:03d}'.format(0) + ','
        partial_load = ",svclist," + d 
    #Append the Origin Details <SVC_ID-IP:SP:Time>,
    partial_load += '{0:03d}'.format(0) + '-' + host_ip_addr + ":" + '{0:03d}'.format(0) + ":"
    payload = partial_load + str(time.time()) +','
    
    icmp_pkt[Ether].src = host_mac_addr
    icmp_pkt[Ether].dst = edst
    icmp_pkt[Dot1Q].vlan = nvlan
    icmp_pkt[IP].tos = schain_seq
    icmp_pkt[IP].src = host_ip_addr
    icmp_pkt[IP].dst = ipdst
    icmp_pkt[ICMP].id = nid
    icmp_pkt[ICMP].seq = nseq
    icmp_pkt[ICMP].payload=payload
    return
def estimate_ping_rate():
    ret = 0
    stime = time.time()
    icmp_pkt[Ether].dst = edst
    icmp_pkt[IP].src = host_ip_addr
    icmp_pkt[IP].dst = ipdst
    #resp = srp1(icmp_pkt2,timeout=MAX_WAIT_TIME_FOR_RESP,verbose=0)
    etime = time.time()
    rtt_s = int(etime-stime)
    rtt_ms = (etime - stime - rtt_s)*1000
    if rtt_s:
        ret = 0 #rtt_s
        if rtt_s >= MAX_WAIT_TIME_FOR_RESP:
            fr = 1/float(MAX_WAIT_TIME_FOR_RESP)
            MAX_WAIT_TIME_FOR_RESP +=1
    if rtt_ms < 100:
        ret = 0 #-int(rtt_ms)
    MAX_WAIT_TIME_FOR_RESP = 1
    return ret, rtt_s, rtt_ms
def sendPings(count=0):
    global nseq
    global nid
    global flow_id
    global payload
    global flow_rate
    global MAX_WAIT_TIME_FOR_RESP
    global l2_socket
    global icmp_pkt, partial_load, payload
    fr = flow_rate
    ret = 0
    rtt_ms = 25
    rtt_s = 0
  
    try:
        icmp_pkt[ICMP].seq = nseq
        payload = partial_load + str(time.time()) +','
        icmp_pkt[ICMP].payload = payload
        if l2_socket is not None:
            l2_socket.send(icmp_pkt)
            #print "Sent via L2 Socket!"
        else:
            sendp(icmp_pkt, verbose=0)
            print "Sent via Sendp()"
        
        #try with response for some packets:
        #if count == 0 or count%50 == 0:
        #    ret,rtt_s,rtt_ms = estimate_ping_rate()
    except Exception,e:
        print "Failed to Send Packets with Exception:", str(e)
        #exit()
        return -1
    writeData(id=str(nid), seq=str(nseq), idst=str(ipdst),dst=str(edst),flow=flow_id,flow_rate=fr,rtt=rtt_s*1000+rtt_ms )
    if nseq == 1:
        writeFlowData(id=str(nid), seq=str(nseq), idst=str(ipdst),dst=str(edst),flow=flow_id,flow_rate=fr,rtt=rtt_s*1000+rtt_ms, schain=sChain)
    nseq = nseq+1
    return ret
AVG_TOPOLOGY_LATENCY = .5 #500ms (6.6*87 =575ms)
def startPings():
    global count
    MAX_RETRY_COUNT = 0
    print "START: At[%s], StartPings[%d]" %(time.time(),count)
    ret = 0
    retry_count = 0
    create_icmp_pkt()
    while retry_count <= MAX_RETRY_COUNT:
        for x in range(count):
            new_interval = interval
            try:
                ret = sendPings(count=x)
                if ret == -1:
                    print "Aborting!!"
                    raise Exception('sendPing() Failed!!')
                    return
                if ret > 0:
                    new_interval = ret
                if nElastic:
                    #new_interval = abs(normalvariate(interval, interval))
                    #new_interval = abs(gauss(interval, interval))
                    delay_ms = randint(min(interval*1000,AVG_TOPOLOGY_LATENCY*1000), max(interval*1000,AVG_TOPOLOGY_LATENCY*1000))
                    new_interval = delay_ms/float(1000)
                time.sleep(new_interval)
            except Exception,e:
                print "SendPings() Exception:",str(e)
        break
        retry_count +=1
        if ret == 0:
            break
        else:
            time.sleep(max(3,interval*5))
            print "Retry attempt [%d]" %retry_count
    print "End At[%s], StartPings[%d]" %(time.time(),count)
    return
def sendPings_inloop(numpkts=count,rate=interval):
    global icmp_pkt
    try:
        print "Start Sending via Sendp(loop=%s,interval=%s)" %(numpkts,rate)   
        sendp(icmp_pkt, count=numpkts, inter=rate, verbose=0)
        print "Sent via Sendp(loop=%s,interval=%s)" %(numpkts,rate)
    except Exception,e:
        print "Failed to Send Packets with Exception:", str(e)
        #exit()
        return -1
    writeFlowData(id=str(nid), seq=str(nseq), idst=str(ipdst),dst=str(edst),flow=flow_id,flow_rate=flow_rate,rtt=.01, schain=sChain)
    return
    
def startPings_new():
    global count, interval, nElastic
    print "START: At[%s], StartPings_new[%d]" %(time.time(),count)
    create_icmp_pkt()
    if nElastic:
        startPings()
    else:
        startPings()
        #sendPings_inloop(numpkts=count,rate=interval)#do new way
    return
def parse_args_icmp():
    global nvlan
    global sChain
    global nid
    global nseq
    global interval
    global flow_id
    global count
    global ipdst
    global edst
    global nElastic
    global schain_seq
    nid = randint(0,99999)
    nseq = 1
    src_id = 0
    dst_id = 0
    try:
        #parse arguments <svc_chain, esrc, edst, count, flow_id, ping_interval, elastic_or_cbr>
        #i = 7
        nElastic = 0
        #if(len(sys.argv)>i) and sys.argv[i].isdigit() is True:
        #    nElastic = int(sys.argv[i])
        #i -=1
        i = 6
        if(len(sys.argv)>i):
            ival = float(sys.argv[i])
            if ival > 0:
              interval = ival
        i -=1
        
        if(len(sys.argv)>i):
            flow_id = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            count = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            dst_id = int(sys.argv[i])
            ipdst = "10.0.0."+ sys.argv[i]
            edst  = "00:00:00:00:00:" + "{:0>2d}".format(dst_id)
            #if dst_id < 10:
            #    edst = "00:00:00:00:00:0"+sys.argv[i]
            #else:
            #    edst = "00:00:00:00:00:"+sys.argv[i]
        i -=1
        
        if(len(sys.argv)>i):
            src_id = int(sys.argv[i])
            #print src
        i-=1
        
        if(len(sys.argv)>i):
            sChain = sys.argv[1].split(",")
            sChain = [x for x in sChain if x.isdigit() is True]
            #schain_seq = [ (schain_seq << 4 | int(x,2)) for x in sChain][0]
            for x in sChain:
                schain_seq = schain_seq << 4 | int(x,2)
            
            #nvlan = int(sChain.pop(0),2)
            nvlan = int(sChain[0],2)
            print "sChain:", sChain, "vlan: %d" %nvlan 
            #print "vlan: %s" %str(nvlan)
        print "Generator: %s to_Dst:%s(%d), Service Chain:%s, First Service: %d , num_packets:%d and Flow_ID:%d, ping_interval:%f, Elastic:%d " %(host_ip_addr,edst,dst_id,sChain,nvlan,count,flow_id,interval, nElastic)
        flow_rate = 1/float(interval) #packets per second
    except Exception,e:
        print "Exception parsing input args!!", str(e)
        exit()
    return

def parse_args():
    global SERVER_IP_ADDRESS
    global SERVER_MAC_ADDRESS
    global Flow_Type
    global Flow_Duration
    global Flow_Mode
    global Flow_Rate
    
    #parse arguments <host_number(%d), flow_rate[in Kbps](%d), flow_type[mice/elephant](%d), flow_duration[in seconds] (%d), flow_mode[udp(0)/tcp(1)/iperf_udp(2)/iperf_tcp(3)](%d)>
    try:
        i = 5
        if(len(sys.argv)>i):
            Flow_Mode = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            Flow_Duration = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            Flow_Type = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i) and int(sys.argv[i]) > 0 and int(sys.argv[i]) < MAX_FLOW_RATE:
            Flow_Rate = int(sys.argv[i])
        i -=1

        if(len(sys.argv)>i):
            dst_id = int(sys.argv[i])
            SERVER_IP_ADDRESS = "10.0.0."+ sys.argv[i]
            SERVER_MAC_ADDRESS = "00:00:00:00:00:" + "{:0>2d}".format(dst_id)
            print "ID:[%d] IP[%s] MAC[%s]" %(dst_id, SERVER_IP_ADDRESS, SERVER_MAC_ADDRESS)
        i -=1
        
        print "Parsed Arguments: server:%s FlowRate:%d, FlowType:%d, FlowDuration:%d and FlowMode:%d," %(SERVER_IP_ADDRESS,Flow_Rate,Flow_Type,Flow_Duration,Flow_Mode)
    except Exception,e:
        print "Exception parsing input args!!", str(e)
        exit()
    return

def advertize_bw_demand():
    global host_ip_addr
    global host_mac_addr
    global Flow_Rate
    global SERVER_IP_ADDRESS
    global SERVER_MAC_ADDRESS
    
    #Send Bandwitdh Information
    nid = Flow_Rate & 0xFFFF
    nseq = Flow_Rate >> 16
    sendp(Ether(src=host_mac_addr,dst=SERVER_MAC_ADDRESS)/IP(src=host_ip_addr,dst=SERVER_IP_ADDRESS)/ICMP(type=15, id=nid, seq=nseq))
    writeRateData(Flow_Rate)
    return

def disable_arp():
    
    #Drop ARP Packets:
    cmd = 'arptables -P INPUT DROP'
    os.system(cmd)
    cmd = 'arptables -P OUTPUT DROP'
    os.system(cmd)
    
    #Disable ARP
    #cmd = "sudo ip link set dev " + str(host_interface) + " arp off"
    #os.system(cmd)
    
    return
    
def configure_client():
    global host_interface
    global host_ip_addr
    global pktdumper
    global tx_flows_file
    global rate_update_file
    
    # Parse and setup the arguments
    parse_args()
    
    #Disable IPV6 Routing:
    cmd = "sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1"
    os.system(cmd)
    cmd = "sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1"
    os.system(cmd)
    cmd = "sudo sysctl -w net.ipv6.conf.lo.disable_ipv6=1"
    os.system(cmd)
    cmd = 'sudo sysctl -p'
    os.system(cmd)
    
    #Disable ARP
    #disable_arp()
    
    # Setup the Log Files 
    tx_flows_file = path_name + 'rx_flows' +  host_ip_addr + '.csv'
    rate_update_file = path_name + 'send_rate' +  host_ip_addr + '.csv'
    
    #PCAP DUMPER FOR CLIENT SIDE < comment below line to disable client side pcap >
    pcap_dump = path_name + 'cli_pcap_dump' + str(host_ip_addr) + '-' + str(host_interface) + '.pcap'
    #pktdumper = PcapWriter(pcap_dump, append=True, sync=True)
    
    Stop_Flag = 0
    pass
    return

def update_sending_rate(informed_rate_in_kbps):
    global Flow_Rate
    print  >>sys.stderr, ' At [%s] current_rate %s to new_rate %s' % (time.time(),Flow_Rate,informed_rate_in_kbps)
    Flow_Rate = informed_rate_in_kbps
    writeRateData(Flow_Rate)
    return

def get_vlan_id_for_rate(rate_in_kbps):
    if rate_in_kbps > 10:
        return rate_in_kbps/10
    return 1  #default 10kbps

def get_data_bytes(nBytes):
    
    if nBytes > 0:
        #return bytearray([x%256 for x in xrange(nBytes)])
        #reutn  'a'*nBytes
        mkstring = lambda(x): "".join(map(chr, (ord('a')+(y%26) for y in range(x))))
        return mkstring(nBytes)
        
    return '0'

MAX_TNX_SIZE = 1500
def send_bulk_packet(Packet, payload):
    global l2_socket
    s_time = time.time()
    print "IIn send_bulk_packet"
    pkt_size = len(Packet)
    unit_tnx_size = MAX_TNX_SIZE - pkt_size
    r_size = len(payload)
    start_offset = 0
    while r_size > 0:
        cur_chunk = min(r_size,unit_tnx_size)
        #frag_packet = Packet/payload[start_offset:cur_chunk]
        l2_socket.send(Packet/payload[start_offset:cur_chunk])
        start_offset += cur_chunk
        r_size -= cur_chunk
    return (time.time() - s_time)
    
    return
def get_data_rate_params(rate_in_kbps, bucket_size=0, send_interval_ms=0):
    nBytesPerSecond = (rate_in_kbps*KILO_SIZE)/BYTE_SIZE
    if bucket_size:
        send_interval_ms = float((bucket_size*SEC_TO_MS)/float(nBytesPerSecond))
    elif send_interval_ms:
        bucket_size = float((nBytesPerSecond*send_interval_ms)/float(SEC_TO_MS))
    else:
        #Fix send_interval = 100ms
        ##bucket_size = float((nBytesPerSecond*FIXED_INTERVAL_100ms)/float(SEC_TO_MS))
        ##send_interval_ms = FIXED_INTERVAL_100ms
        #Fix Bucket_Size = 1400
        bucket_size = FIXED_BSIZE_1400B
        send_interval_ms = float((bucket_size*SEC_TO_MS)/float(nBytesPerSecond))
        pass
    #print bucket_size, send_interval_ms
    return int(bucket_size),send_interval_ms
    
def start_udp_sock_client(dst_ip_addr="10.0.0.1", dst_mac_addr="00:00:00:00:00:00", rate_in_kbps=0):
    global host_ip_addr
    
    socket.setdefaulttimeout(Flow_Duration)
    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    #Bind the socket to client Port
    sock.bind((host_ip_addr, UDP_CLIENT_PORT))
    
    #server_address = ('localhost', 10000)
    server_address = (dst_ip_addr, UDP_SERVER_PORT)
    print >>sys.stderr, 'connecting to %s port %s' % server_address
    sock.connect(server_address)
    #sock.create_connection(server_address,timeout=2)
    
    #message = 'Hi!!, This is a dummy message.'
    bucket_size = 4*KILO_SIZE
    while True:
        if Stop_Flag : #or  (time.time() - start_time >= Flow_Duration) :
            print >>sys.stderr, "Stopping TCP SOCK client!"
            break
        try:
            # Fetch Data
            payload = get_data_bytes(bucket_size)
            
            # Send data
            sock.sendall(payload)
            
            #print >>sys.stderr, 'sending "%s"' % message
            #sent = sock.sendto(message, server_address)

            # Receive response
            #print >>sys.stderr, 'waiting to receive'
            #data, server = sock.recvfrom(UDP_DGRAM_SIZE)
            #print >>sys.stderr, 'received "%s"' % data
        
        except Exception,e:
            print " start_udp_sock_client() Exception:",str(e)
            pass

    #finally:
    print >>sys.stderr, 'Shutting down UDP Socket'
    sock.shutdown(socket.SHUT_RDWR)
    print >>sys.stderr, 'closing UDP Socket'
    sock.close()
    return
        
def start_udp_scapy_client(dst_ip_addr="10.0.0.1", dst_mac_addr="00:00:00:00:00:00", rate_in_kbps=0):
    global host_ip_addr
    global host_interface
    global host_mac_addr
    global l2_socket
    global Stop_Flag
    
    print "Start UDP connection with [%s, %s, %d Kbps]" %(dst_ip_addr,dst_mac_addr,rate_in_kbps)
    ipPacket = IP(src=host_ip_addr, dst=dst_ip_addr)
    udpPacket = UDP(sport=UDP_CLIENT_PORT, dport=UDP_SERVER_PORT)
    ethPacket = Ether(src=host_mac_addr, dst=dst_mac_addr)
    vlanPacket = Dot1Q(vlan=0)
    #vlanPacket.prio = 7
    #vlanPacket.type = 0x0800 # 0x8100
    bucket_size = 4*KILO_SIZE
    send_interval_ms = 0
    send_counter = 0
    start_time = time.time()
    while True:
        if Stop_Flag : #or  (time.time() - start_time >= Flow_Duration) :
            print >>sys.stderr, "Stopping UDP scapy client!"
            break
        def recalculate_rate():
            bucket_size, send_interval_ms = get_data_rate_params(rate_in_kbps)
        
        #Flow_Rate is Updated Asynchronously by controller, and we want to update to the specified rate as soon as possible!
        if rate_in_kbps != Flow_Rate:
            rate_in_kbps = Flow_Rate
            vlanPacket.vlan = get_vlan_id_for_rate(rate_in_kbps)
            bucket_size, send_interval_ms = get_data_rate_params(rate_in_kbps, bucket_size=1500-len(ethPacket/ipPacket/udpPacket)) # 
            interval = send_interval_ms/float(SEC_TO_MS)
            print  >>sys.stderr, 'rate_in_kbps: %s, vlan: %s, bucket_size:%s, time_interval:%s, interval: %s ' %(rate_in_kbps,vlanPacket.vlan, bucket_size, send_interval_ms, interval)
        
        try:
            #Fetch Data:
            #payload = 'Hi!!, This is a dummy message.'
            payload = get_data_bytes(bucket_size)
            time_spent = 0
            #Packet = ethPacket/vlanPacket/ipPacket/udpPacket
            #time_spent = send_bulk_packet(Packet, payload)
            
            #udpPacket.len = UDP_HEADER_SIZE + len(payload)
            #Packet = ethPacket/vlanPacket/ipPacket/udpPacket/payload
            Packet = ethPacket/ipPacket/udpPacket/payload
            # Send data
            #print >>sys.stderr, 'sending "%s"' % message
            if l2_socket is not None:
                l2_socket.send(Packet)
                #l2_socket.send(fragment(Packet,1400))
                sendp(Packet,verbose=0)
            # else:
                sendp(Packet,verbose=0)
                # sendp(fragment(Packet,1400),verbose=0)
            # #sendp(Packet,verbose=0)
            # #resp = srp1(Packet,timeout=MAX_WAIT_TIME_FOR_RESP,verbose=0)    
            
            # Receive response
            #print >>sys.stderr, 'waiting to receive'
            #data, server = sock.recvfrom(UDP_DGRAM_SIZE)
            #print >>sys.stderr, 'received "%s"' % data
            send_counter +=1
            #print >>sys.stderr, 'send_counter "%s: %s"' % (send_counter, time.time())
            wait_interval = 0
            if interval > time_spent:
                wait_interval = interval - time_spent
            else:
                print >>sys.stderr, 'underrunning in sending buffers "%s: %s"' % (wait_interval, interval)
            
            time.sleep(wait_interval)
            
        except Exception,e:
            print " start_udp_scapy_client() Exception:",str(e)
            break
        finally:
            #print >>sys.stderr, 'ending udp scapy client'
            #sock.close()
            pass
    return

def start_tcp_sock_client(dst_ip_addr="10.0.0.1", dst_mac_addr="00:00:00:00:00:00", rate_in_kbps=0):
    
    socket.setdefaulttimeout(Flow_Duration)
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Iterim Option to enable Address Reuse: 
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    #Bind the socket to client Port
    sock.bind((host_ip_addr, TCP_CLIENT_PORT))

    # Connect the socket to the port where the server is listening
    server_address = (dst_ip_addr, TCP_SERVER_PORT)
    print >>sys.stderr, 'connecting to %s port %s' % server_address
    sock.connect(server_address)
    #sock.create_connection(server_address,timeout=2)
    
    bucket_size = 4*KILO_SIZE
    while True:
        if Stop_Flag : #or  (time.time() - start_time >= Flow_Duration) :
            print >>sys.stderr, "Stopping TCP SOCK client!"
            break
        try:
            # Fetch Data
            payload = get_data_bytes(bucket_size)
            
            # Send data
            #message = 'This is the message.  It will be repeated.'
            #print >>sys.stderr, 'sending "%s"' % message
            sock.sendall(payload)

            # # Look for the response
            # amount_received = 0
            # amount_expected = len(message)
            
            # while amount_received < amount_expected:
                # data = sock.recv(16)
                # amount_received += len(data)
                # print >>sys.stderr, 'received "%s"' % data

        except Exception,e:
            print " start_tcp_sock_client() Exception:",str(e)
            pass
            
    #finally:
    print >>sys.stderr, 'Shutting down TCP Socket'
    sock.shutdown(socket.SHUT_RDWR)
    print >>sys.stderr, 'closing TCP socket'
    sock.close()
    return

def start_tcp_scapy_client(dst_ip_addr="10.0.0.1", dst_mac_addr="00:00:00:00:00:00", rate_in_kbps=0):
    global host_ip_addr
    global host_interface
    global host_mac_addr
    global l2_socket
    global Stop_Flag
    
    ipPacket = IP(src=host_ip_addr, dst=dst_ip_addr)
    ethPacket = Ether(src=host_mac_addr, dst=dst_mac_addr)
    vlanPacket = Dot1Q(vlan=get_vlan_id_for_rate(rate_in_kbps))
    
    # Establish Three way Handshake
    tcpsynPkt = TCP(sport=TCP_CLIENT_PORT, dport=TCP_SERVER_PORT, flags="S", seq=0)
    synPacket = ethPacket/ipPacket/tcpsynPkt
    synackPkt=sr1(ipPacket/tcpsynPkt)
    #synackPkt= srp1(synPacket) #srp doesnt work for TCP

    new_ack = synackPkt[TCP].seq + 1
    new_seq = synackPkt[TCP].ack
    tcpackPkt = TCP(sport=TCP_CLIENT_PORT, dport=TCP_SERVER_PORT, flags="A", seq=1, ack=new_ack)
    ackPacket = ethPacket/ipPacket/tcpackPkt
    send(ipPacket/tcpackPkt)
    #sendp(ackPacket)

    start_seq = 2
    tcpPacket = TCP(sport=TCP_CLIENT_PORT, dport=TCP_SERVER_PORT, flags="", seq=start_seq, ack=new_ack)
    #message = 'Hi!!, This is a dummy message.'
    #payload = message
    #Packet = ethPacket/vlanPacket/ipPacket/tcpPacket/payload
    
    bucket_size = 4*KILO_SIZE
    send_interval_ms = 0
    send_counter = 0
    start_time = time.time()
    while True:
        if Stop_Flag : #or  (time.time() - start_time >= Flow_Duration) :
            print >>sys.stderr, "Stopping UDP scapy client!"
            break
        def recalculate_rate():
            bucket_size, send_interval_ms = get_data_rate_params(rate_in_kbps)
        #Flow_Rate is Updated Asynchronously by controller, and we want to update to the specified rate as soon as possible!
        if rate_in_kbps != Flow_Rate:
            rate_in_kbps = Flow_Rate
            vlanPacket.vlan = get_vlan_id_for_rate(rate_in_kbps)
            bucket_size, send_interval_ms = get_data_rate_params(rate_in_kbps)
            interval = send_interval_ms/float(SEC_TO_MS)
            print  >>sys.stderr, 'rate_in_kbps: %s, vlan: %s, bucket_size:%s, time_interval:%s, interval: %s ' %(rate_in_kbps,vlanPacket.vlan, bucket_size, send_interval_ms, interval)

        try:
            #Fetch Data
            payload = get_data_bytes(bucket_size)
            Packet = ethPacket/vlanPacket/ipPacket/udpPacket/payload
            
            # Send data
            print >>sys.stderr, 'sending "%s"' % message
            if l2_socket is not None:
                l2_socket.send(Packet)
            else:
                sendp(Packet,verbose=0)
                #send(Packet)
            start_seq +=1
            
            send_counter +=1
            print >>sys.stderr, 'send_counter "%s: %s"' % (send_counter, time.time())
            time.sleep(interval)
            
            # Receive response
            #print >>sys.stderr, 'waiting to receive'
            #data, server = sock.recvfrom(TCP_PACKET_SIZE)
            #print >>sys.stderr, 'received "%s"' % data

        except Exception,e:
            print " start_tcp_scapy_client() Exception:",str(e)
            return
        finally:
            pass
            
    #Perform 3 way handshake for  close (FIN, ACK,FIN, ACK)
    print >>sys.stderr, 'ending tcp scapy client'
    tcpfinPkt = TCP(sport=TCP_CLIENT_PORT, dport=TCP_SERVER_PORT, flags="F", seq=start_seq, ack=new_ack)
    Packet = ethPacket/ipPacket/tcpfinPkt
    #finackPkt = srp1(Packet,verbose=0)
    finackPkt=sr1(ipPacket/tcpsynPkt)
    
    fin_ack = finackPkt[TCP].seq + 1
    fin_seq = finackPkt[TCP].ack
    tcpackPkt = TCP(sport=TCP_CLIENT_PORT, dport=TCP_SERVER_PORT, flags="A", seq=fin_seq, ack=fin_ack)
    ackPacket = ethPacket/ipPacket/tcpackPkt
    send(ipPacket/tcpackPkt)
    #sendp(ackPacket)
    
    return

def start_iperf_sock_client(dst_ip_addr="10.0.0.1", dst_mac_addr="00:00:00:00:00:00", udp_mode=0):
    global host_interface
    global host_ip_addr
    
    IP_SERVER_PORT = TCP_SERVER_PORT + 20
    if udp_mode:
        IP_SERVER_PORT = UDP_SERVER_PORT + 20
    
    ncs_dump= path_name + 'ipfc_dump' + str(host_ip_addr) + '-' + str(host_interface)
    ncs_dump = ncs_dump + str(IP_SERVER_PORT) + '.rawtxt'

    cmd = 'iperf -c ' + str(dst_ip_addr) + ' -p ' + str(IP_SERVER_PORT) # + ' -b 10M '
    cmd = cmd + ' -t ' + str(Flow_Duration)
    
    if udp_mode:
        cmd = cmd + ' -u ' + ' -b 10M '
    cmd = cmd +  ' > ' + ncs_dump
    
    print cmd
    os.system(cmd)

    ncs_dump_tcp = ncs_dump + str(IP_SERVER_PORT) + '.rawtxt'
    
    print cmd
    os.system(cmd)
    print "End and Return from IPERF CLIENT! "
    return
    
def isIncoming(pkt):
    
    try:
        # Check if this is required 
        if pkt[IP].src == host_ip_addr and pkt[Ether].src == host_mac_addr:
            return False
        # Valid Packets: ICMP - Controller about Rate Updates, TCP/UDP client oriented messages     
        if pkt[IP].dst == host_ip_addr: #and pkt[Ether].dst == host_mac_addr:
            # ICMP Packets for Rate Updates 
            #if pkt.haslayer(ICMP) and (pkt[ICMP].type == 0 or pkt[ICMP].type == 16):
            if pkt.haslayer(ICMP) and pkt[ICMP].type == 16:
                print "Got ICMP rate update packet from Controller! "
                return True
            # FOR TCP
            if pkt.haslayer(TCP) and pkt[TCP].dport ==TCP_CLIENT_PORT: #pkt[TCP].dport ==7: # and not pkt.haslayer(Dot1Q):
                #print "Got packet for TCP Client! "
                return True
            if pkt.haslayer(UDP) and pkt[UDP].dport == UDP_CLIENT_PORT:
                #print "Got packet for UDP Client! "
                return True
    except:
        pass
    return False

def PacketHandler(pkt):
    global host_interface
    global host_ip_addr
    global pktdumper
    
    #if pkt.haslayer(ICMP) and (pkt[ICMP].type == 0 or pkt[ICMP].type == 16):
    if pkt.haslayer(ICMP) and pkt[ICMP].type == 16:
        rate =  pkt[ICMP].id | pkt[ICMP].seq << 16
        update_sending_rate(informed_rate_in_kbps = rate)
        return
        
    if (pkt.haslayer(TCP) and pkt[TCP].sport == TCP_CLIENT_PORT) or \
       (pkt.haslayer(UDP) and pkt[UDP].sport == UDP_CLIENT_PORT):
        #wrpcap(pcap_dump,pkt)
        if pktdumper is not None:
            pktdumper.write(pkt)
    
    #Regular TCP/UDP/IP packet 
    #Update the Rate
    pass
    
    return

def start_sniffer():
    global host_interface
    global host_ip_addr
    
    #time.sleep(1)
    #Start the SNIFF and Forward/Log Sniffed Packets
    #comTcpDump = sniff(offline=pcap_dump, iface=host_interface, lfilter = isIncoming, prn = PacketHandler) #,store=0
    comTcpDump = sniff(iface=host_interface, lfilter = isIncoming, prn = PacketHandler,timeout=Flow_Duration+2) #,store=0
    print "Terminating Sniffer Thread!!"
    pass
    return

def launch_sniffer():
    try:
        t = Thread(group=None,target=start_sniffer,name="sniffer", args=(), kwargs={})
        
        t.start()
        pass
    except Exception, e:
        print "Exception:", str(e)
        exit()
    return t

def signal_end():
    global Stop_Flag
    Stop_Flag = 1
    print "Set the Stop Flag True!"
    return
    
def launch_client():
    global Flow_Duration
    #Start Two Threads
    #   1. Sniffer and Rate Control
    t1 = launch_sniffer()
    #   2. Actual Data Transfer Control Loop
    t2 = Timer(Flow_Duration+1, signal_end)
    t2.start()
    advertize_bw_demand()
    if Flow_Mode == 1:
        #start_tcp_scapy_client(dst_ip_addr=SERVER_IP_ADDRESS, dst_mac_addr=SERVER_MAC_ADDRESS, rate_in_kbps=0)
        start_tcp_sock_client(dst_ip_addr=SERVER_IP_ADDRESS, dst_mac_addr=SERVER_MAC_ADDRESS, rate_in_kbps=0)
    elif Flow_Mode == 2:
        start_iperf_sock_client(dst_ip_addr=SERVER_IP_ADDRESS, dst_mac_addr=SERVER_MAC_ADDRESS, udp_mode = 1)
    elif Flow_Mode == 3:    
        start_iperf_sock_client(dst_ip_addr=SERVER_IP_ADDRESS, dst_mac_addr=SERVER_MAC_ADDRESS, udp_mode = 0)
    else:
        #start_udp_sock_client(dst_ip_addr=SERVER_IP_ADDRESS, dst_mac_addr=SERVER_MAC_ADDRESS, rate_in_kbps=0)
        start_udp_scapy_client(dst_ip_addr=SERVER_IP_ADDRESS, dst_mac_addr=SERVER_MAC_ADDRESS, rate_in_kbps=0)
        
    #print "Waiting for %d seconds" %(Flow_Duration)
    #time.sleep(Flow_Duration)
    
    if t2.isAlive():
        t2.cancel()
    signal_end()    
    t1.join(1)
    if t1.isAlive():
        #print "Force Stop the Thread!"
        t1._Thread__stop() #t1.cancel() 
        time.sleep(1)
    return 
if __name__ == '__main__':
    
    getInterfaceInfo()
    #configure_client()
    
    #launch_client()
    parse_args_icmp()
    startPings_new()
    
    
    print "Ending Process \n "
    #start_udp_sock_client()
    #start_tcp_sock_client()

