#!/bin/bash
# Measure Network Bandwidth on an Interface
INTERVAL="1"  # update interval in seconds

if [ -z "$1" ]; then
        echo
        echo usage: $0 [network-interface]
        echo
        echo e.g. $0 eth0
        echo
        exit
fi
 
IF=$1
#OUT_FILE='./'$1'.txt'
#OUT_FILE='/local/drench/results/'$1'_bwd.txt'
OUT_FILE='/home/mininet/drench/results/'$1'_bwd.txt'
CUR_TIME=0
while true
do
        R1=`cat /sys/class/net/$1/statistics/rx_bytes`
        T1=`cat /sys/class/net/$1/statistics/tx_bytes`
        sleep $INTERVAL
        R2=`cat /sys/class/net/$1/statistics/rx_bytes`
        T2=`cat /sys/class/net/$1/statistics/tx_bytes`
        TBPS=`expr $T2 - $T1`
        RBPS=`expr $R2 - $R1`
        TKbPS=`expr $TBPS*8 / 1024 / $INTERVAL`
        RKbPS=`expr $RBPS*8 / 1024 / $INTERVAL`
        OKbPS=`expr $TKbPS + $RKbPS`
        CUR_TIME=`expr $CUR_TIME + $INTERVAL`
        #echo "TX $1: $TKbPS kB/s RX $1: $RKbPS kB/s"
        #echo "TX $1: $TKbPS kB/s RX $1: $RKbPS kB/s" >> $OUT_FILE
        echo "[$1 at: $CUR_TIME], TTL=$OKbPS kB/s, TX=$TKbPS kB/s, RX=$RKbPS kB/s" >> $OUT_FILE
done
