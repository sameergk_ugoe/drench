#!/usr/bin/python

#
# Program to generate cli command list
#

#from mininet.log import setLogLevel
from random import randint
import sys
import csv
import time
import random
import argparse
from os.path import expanduser
hdir_p = expanduser('~')
hdir_p = hdir_p + '/drench/svc_chains'

iptos_to_svc_chain_dict = {}
aChain = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
max_functions = len(aChain)
chain_length = 3
out_csv_file = hdir_p + "/svc_chain_dict.csv"
#out_csv_file = "svc_chain_dict.csv"
def parse_named_args():
    ch_len = chain_length
    mf_fun = max_functions
    o_file = out_csv_file
    is_parsed = False
    
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--chain_length', '-c' ,'-l', required=True, type=int, help='Service Chain Length')
    parser.add_argument('--max_functions', '-mf', required=True, type=int, help='Maximum Number of Service Chain Functions')
    parser.add_argument('--out_file', '-o', help='Output to File')
    args = parser.parse_args()
    
    if args.chain_length:
        ch_len = args.chain_length
        is_parsed = True
    if args.max_functions and args.max_functions > 0 and args.max_functions <15:
        mf_fun = args.max_functions
        is_parsed = True
    if args.out_file:
        o_file = args.out_file
        is_parsed = True
    else:
        o_file = o_file.split('.')[0] + '_' + str(ch_len) + '_' + str(mf_fun) + '.' + o_file.split('.')[1]
    setup_svc_chain_params_and_generate_dict(ch_len=ch_len, max_funcs=mf_fun, out_file=o_file)
    return is_parsed

def returnZipfDistribution(zipfExponent,numberOfServices):
  probabilityDistribution  = [0.0]*numberOfServices
  cdfOfDistribution        = 0.0
  for serviceIndex in xrange(numberOfServices):
    nominator  = 1/(pow(float(serviceIndex+1),zipfExponent))
    probabilityDistribution[serviceIndex]  = nominator
    cdfOfDistribution  += nominator
  for serviceIndex in xrange(numberOfServices):
    probabilityDistribution[serviceIndex]/=cdfOfDistribution
  #probability distribution
  #print 'Zipf distribution'
  #print probabilityDistribution
  return probabilityDistribution
def serviceChainChoice(serviceChainLength,probabilityDistribution):
  probability  = {}
  for index in xrange(len(probabilityDistribution)):
    probability[index]  = probabilityDistribution[index]
  serviceChain  = [None]*serviceChainLength
  for serviceIndex in xrange(serviceChainLength):
    randomNumber  = random.random()
    #print '---------'
    #print 'service choice iteration '+str(serviceIndex)
    #print 'random number: '+str(randomNumber)
    #print 'updated probability space'
    #print probability
    serviceCDF     =  0.0
    serviceChosen  = -1
    for element in probability:
      if serviceCDF>randomNumber:
        break
      serviceCDF    += probability[element]
      serviceChosen  = element
    serviceChain[serviceIndex]  = serviceChosen
    #print 'chosen service: '+str(serviceChosen)
    updateProbabilitySpace(probability,serviceChosen)
  #print 'Service chain selected'
  #print serviceChain
  return serviceChain
def updateProbabilitySpace(probability,serviceChosen):
  del probability[serviceChosen]
  sumOfProbabilitiesOfNewSpace  = 0.0
  for element in probability:
    sumOfProbabilitiesOfNewSpace  += probability[element]
  #estimate new probabilities
  for element in probability:
    probability[element]/=sumOfProbabilitiesOfNewSpace
#select service chain
def getServiceChain(min_len=1, max_len=2, fixed_len=2):
  numberOfServices         = len(aChain)
  serviceChainLength       =  fixed_len
  zipfExponent             =  0.3 #0.5
  
  #zipf distribution
  probabilityDistribution  = returnZipfDistribution(zipfExponent,numberOfServices)
  sc_index = serviceChainChoice(serviceChainLength,probabilityDistribution)
  sc = [aChain[x] for x in sc_index]
  return sc
def write_dict_to_csv(dict_file_name='dict.csv'):
    
    writer = csv.writer(open(dict_file_name, 'wb'))
    for key, value in iptos_to_svc_chain_dict.items():
        writer.writerow([key, value])
    
    #spamwriter = csv.writer(open(dict_file_name, 'wb'), delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
    #for key, value in iptos_to_svc_chain_dict.items():
    #    spamwriter.writerow(key)
    #    for i in range(0, len(value)):
    #        spamwriter.writerow(value[i])
    #return

def get_list_of_nums_from_string(in_str):
    str_items = in_str.split(',')
    num_list = []
    for str in str_items:
        val = int(''.join(ele for ele in str if ele.isdigit())) # float(''.join(ele for ele in x if ele.isdigit() or ele == '.'))
        num_list.append(val)
    return num_list
def read_to_dict_from_csv(dict_file_name='dict.csv'):
    global iptos_to_svc_chain_dict
    if iptos_to_svc_chain_dict:
        print "overriding the existing dictionary!!"
    reader = csv.reader(open(dict_file_name, 'rb'))
    #iptos_to_svc_chain_dict = dict(reader)
    for row in reader:
        key = int(row[0])
        vals = str(row[1])
        chain = get_list_of_nums_from_string(vals) #[int(s) for s in vals.split(,) if s.isdigit() and s not in [\", \", \[, \]]
        iptos_to_svc_chain_dict[key] = chain
    #print iptos_to_svc_chain_dict
    #print iptos_to_svc_chain_dict.keys()
    #print "\n\n value for tos=8: ", iptos_to_svc_chain_dict[str(8)]
    return iptos_to_svc_chain_dict
def construct_iptos_to_svc_chain_dict(dict_file_name="svc_chain_dict.csv"):
    global iptos_to_svc_chain_dict
    for i in xrange(1,256):
        if  i == 0 or i&0x01 or i&0x02==2: continue
        schain = getServiceChain(fixed_len=chain_length)
        iptos_to_svc_chain_dict[i] = schain
    #print iptos_to_svc_chain_dict
    write_dict_to_csv(dict_file_name)
    #read_to_dict_from_csv()
    return

def setup_svc_chain_params_and_generate_dict(ch_len=chain_length, max_funcs=max_functions, out_file="svc_chain_dict.csv"):
    global aChain, max_functions, chain_length
    chain_length = ch_len
    aChain = [max_funcs - x for x in xrange(0, max_funcs)]
    max_functions = len(aChain)
    construct_iptos_to_svc_chain_dict(out_file)
    return

def get_svc_chain_dict():
    return iptos_to_svc_chain_dict
def get_svc_chain_for_tos(ip_tos=8):
    tos = str(ip_tos)
    if iptos_to_svc_chain_dict.has_key(tos):
        return iptos_to_svc_chain_dict[tos]
    elif iptos_to_svc_chain_dict.has_key(ip_tos):
        return iptos_to_svc_chain_dict[ip_tos]
    return None
def get_svc_chian_len_for_tos(ip_tos):
    schain = get_svc_chain_for_tos(ip_tos)
    if schain and len(schain) > 0:
        return len(schain)
    return None
def get_svc_at_index_for_tos(ip_tos, index):
    schain = get_svc_chain_for_tos(ip_tos)
    if schain and len(schain) > index:
        return int(schain[index])
    return None

if __name__ == '__main__':
  parse_named_args()
  setup_svc_chain_params_and_generate_dict()
