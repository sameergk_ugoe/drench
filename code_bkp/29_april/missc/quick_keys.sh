#To extract column 2 data and get the avg
awk '{ total += $2 } END { print total/NR }'
perl -F, -lane '$total += $F[1]; END{print $total/$.}' file

#To append a fixed column to the each line of all files in a directory
for file in *.txt; do gawk -i inplace '{print $0 " -m rttm"}' $file; done

#To swap a content of column with new value (say 10240) for each line of all files in a directory
for file in *.txt; do gawk -i inplace '{$10=10240}1' $file; done

#To truncate file to first x lines 
for file in *.txt; do sed -i '2, $max_lines p' $file; done

#To swap a content of column with new value (say 10240) for each line of all files in a directory
for file in *.txt; do gawk -i inplace '{$10=10240}1' $file; done
for file in *.txt; do gawk -i inplace '{$2="TCP"}1' $file; done

#To replace a content of column for every 8th row wih a new value for each line of all files in a directory
for file in *.txt; do gawk -i inplace '{!(NR%8) && $10=10240}1' $file; done

#count total lines in all files recursively
find . -type f -exec wc -l {} \; | awk '{total += $1} END{print total}'
or 
wc -l `find . -type f`

#Recursively find .bak files and delete
find . -name "*.bak" -type f -print | xargs /bin/rm -f

#Recursively find .bak files and move to home/.old.files directory
find . -name "*.bak" -print0 | xargs -0 -I {} mv {} ~/old.files
 
#Refer:
http://www.catonmat.net/blog/ten-awk-tips-tricks-and-pitfalls/
http://www.grymoire.com/Unix/Awk.html
http://lorance.freeshell.org/csvutils/

#some git and Awk quickies
#add all files that have status changed
git status -s|awk '{ print $2 }'|xargs git add
#reset changes
git status -s|awk '{ print $2 }'|xargs git reset HEAD
