# Reference: http://goodcode.io/articles/python-dict-object/

#Approach1 for viewing dictionary entry as objects
class objectview(object):
    def __init__(self, d):
        self.__dict__ = d

##Test
#d = {'a': 1, 'b': 2}
#o = objectview(d)
#assert o.a == 1
#assert o.b == 2
#d['c'] = 3
#assert o.c == 3
#del o.c
#assert 'c' not in d

#Approach2 to create Dictionary entries as class objects
class objdict(dict):
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)

import networkx as nx
TopoGraph = None
#Type=0 => switch, 1=>Host
#Switch node has dpid, host nodes have mac and IP.
node_info={} # { name: type: ('switch=0' or 'host=1')  ip:  mac:  cost: demand: }
edge_info={} # { type: 0 or 1 node1: node1_obj node2: node2_obj port1: port_on_node1, port2: port_on_node_2, capacity: Link_BW delay: Link_Delay weight: 1}
DEFAULT_SHADOW_PRICE = 0
class node_object:
    def __init__(self, type, id=None, name=None, ip=None, mac=None, cost=None, demand=None):
        setup_node_info(name, type, ip, mac, cost, demand)
    def setup_node_info(self, type, id=None, name=None, ip=None, mac=None, cost=None, demand=None):
        self.type = type
        self.id = id
        self.name = name
        self.ip = ip
        self.mac = mac
        self.cost = cost
        self.demand = demand
        return
def add_node(type=0, id=None, name=None, ip=None, mac=None, cost=None, demand=None):
    if not type and id is None:
        return False
    elif type and ip is none or mac is none:
        return False
    return True
    
class link_object:
    def __init__(self,type, node1_id, node2_id, node1_port, node2_port, capacity=None, delay=None, weight=None,shadow_price=None):
        self.setup_link_info(type, node1_id, node2_id, node1_port, node2_port, capacity=capacity, delay=delay, weight=weight,shadow_price=shadow_price)
    def setup_link_info(self, type, node1_id, node2_id, node1_port, node2_port, capacity=None, delay=None, weight=None,shadow_price=None):
        self.type = type
        self.node1_id = node1_id
        self.node2_id = node2_id
        self.node1_port = node1_port
        self.node2_port = node2_port
        self.capacity = capacity
        self.delay = delay
        self.weight = weight
        self.shadow_price = shadow_price
        return
    def set_link_shadow_price(self, shadow_price):
        self.shadow_price = shadow_price
        return
    def clear_link_shadow_price(self):
        self.shadow_price = DEFAULT_SHADOW_PRICE
        return

def add_link(type, node1_id, node2_id, node1_port, node2_port, capacity=None, delay=None, weight=None,shadow_price=None):
    global TopoGraph
    if not type and id is None:
        return False
    link_obj = link_object(type, node1_id, node2_id, node1_port, node2_port, capacity, delay, weight)
    link_objr = link_object(type, node2_id, node1_id, node2_port, node1_port, capacity, delay, weight)
    if TopoGraph is None:
        TopoGraph = nx.DiGraph() #nx.Graph()
    TopoGraph.add_edge(node1_id, node2_id,link_info=link_obj)
    TopoGraph.add_edge(node2_id, node1_id,link_info=link_objr)
    
    #print TopoGraph.nodes()
    #print TopoGraph.edges(data=True)
    return True

def remove_link(type, node1_id, node2_id, node1_port, node2_port):
    global TopoGraph
    if TopoGraph.has_edge(node1_id, node2_id):TopoGraph.remove_edge(node1_id, node2_id)
    if TopoGraph.has_edge(node2_id, node1_id):TopoGraph.remove_edge(node2_id, node1_id)
    #Update Paths??
    return
def add_or_del_link(type, node1_id, node2_id, node1_port, node2_port, status, capacity=None, delay=None, weight=None,shadow_price=None):    if status:
        add_link(type, node1_id, node2_id, node1_port, node2_port, capacity, delay, weight, shadow_price)    else:
        remove_link(type, node1_id, node2_id, node1_port, node2_port)    #print TopoGraph.edges(data=True)
    return
def update_link_shadow_price(type, node1_id, node2_id, node1_port, node2_port,shadow_price):
    return
def draw_graph():
    global TopoGraph
    import matplotlib.pyplot as plt
    nx.draw(TopoGraph)
    plt.show()
    plt.savefig("path.png")
    return
    
def get_all_paths_between_edge_switch(sw_dpid1,sw_dpid2,first_in_port=None,last_out_port=None):
    global TopoGraph
    if sw_dpid1==sw_dpid2: return [[(sw_dpid1,first_in_port,last_out_port)]]
    #paths = nx.all_simple_paths(TopoGraph,source=sw_dpid1,target=sw_dpid2)
    paths = nx.all_shortest_paths(TopoGraph,source=sw_dpid1,target=sw_dpid2) #[p for p in nx.all_shortest_paths(TopoGraph,source=sw_dpid1,target=sw_dpid2)]
    if not paths:
        #print("Failed to Find Networkx Path between (%d:%d, %d:%d) ",sw_dpid1,sw_dpid2,first_in_port,last_out_port)
        return None
    
    sw_paths_list = []
    plist = [path for path in paths]
    #print("Got  Networkx Path between (%d:%d, %d:%d)=(%s) ",sw_dpid1,sw_dpid2,first_in_port,last_out_port, plist)
    p = None
    for path in plist:
        p_elist = [(x,path[i+1]) for i,x in enumerate(path) if i+1 < len(path)]
        p_link_objs = [TopoGraph.get_edge_data(*e)['link_info'] for e in p_elist]
        ports_info = [(p.node1_port, p.node2_port) for p in p_link_objs]
        sw_path = []
        in_port_temp = first_in_port
        for p in p_link_objs:
            sw_path.append((p.node1_id,in_port_temp, p.node1_port)) 
            in_port_temp= p.node2_port
        if p:
            sw_path.append((p.node2_id,in_port_temp,last_out_port))
            sw_paths_list.append(sw_path)
        #print "current path_list: ", sw_paths_list
    return sw_paths_list

def get_all_paths_between_all_edge_switches(edge_switch_info_list):
    edge_switch_paths={} #key = (src_sw_dpid, src_port, dst_sw_dpid, dst_port)
    for (src_sw, src_port) in edge_switch_info_list:
        for (dst_sw,dst_port) in edge_switch_info_list:
            key = (src_sw, src_port,dst_sw,dst_port)
            if src_sw != dst_sw and src_port != dst_port:
                edge_switch_paths[key] = get_all_paths_between_edge_switch(sw_dpid1=src_sw, first_in_port=src_port,sw_dpid2=dst_sw,last_out_port=dst_port)
            else:
                edge_switch_paths[key] = [(src_sw,src_port,dst_port)] #get_all_paths_between_edge_switch(sw_dpid1=src_sw, first_in_port=src_port,sw_dpid2=dst_sw,last_out_port=dst_port)
    return edge_switch_paths

def get_path_cost(path):
    return 0
def get_economic_path_between_edge_swiches(sw_dpid1,sw_dpid2,first_in_port=None,last_out_port=None):
    sw_paths_list = get_all_paths_between_edge_switch(sw_dpid1,sw_dpid2,first_in_port,last_out_port)
    economic_path = None
    economic_path_cost = 0
    cur_path_cost = 0
    for path in sw_paths_list:
        cur_path_cost = get_path_cost(path)
        if economic_path is None or cur_path_cost < economic_path_cost:
            economic_path_cost = cur_path_cost
            economic_path = path
    return economic_path,path_cost

path_map = {}
def get_path_key(src_sw_dpid, src_sw_inport, dst_sw_dpid, dst_sw_outpport):
    return (src_sw_dpid, src_sw_inport, dst_sw_dpid, dst_sw_outpport)
    
def get_all_paths():
    return
#paths = nx.all_simple_paths(g,source=2,target=3)
#plist = [path for path in paths]
#for path in plist:
#...   elist = [(x,path[i+1]) for i,x in enumerate(path) if i+1 < len(path)]
#...   print elist
#      for e in elist: print g.get_edge_data(*e)

def test_simple_loop_graph():
    #<s1-S2, s1-s3, s1-s4>
    add_link(type=0,node1_id=1, node2_id=2, node1_port=2, node2_port=1, capacity=2, delay=2, weight=2)
    add_link(type=0,node1_id=1, node2_id=3, node1_port=3, node2_port=3, capacity=3, delay=3, weight=3)
    add_link(type=0,node1_id=1, node2_id=4, node1_port=4, node2_port=1, capacity=4, delay=4, weight=4)
    
    #<s2-S3, s4-s3>
    add_link(type=0,node1_id=2, node2_id=3, node1_port=2, node2_port=2, capacity=5, delay=5, weight=5)
    add_link(type=0,node1_id=4, node2_id=3, node1_port=2, node2_port=4, capacity=7, delay=7, weight=7)
    
    #<h1-s1>
    add_link(type=1,node1_id=10, node2_id=1, node1_port=1, node2_port=1, capacity=10, delay=10, weight=10)
    #<h3-s3>
    add_link(type=1,node1_id=30, node2_id=3, node1_port=1, node2_port=1, capacity=30, delay=30, weight=30)
    #<h2-S2>
    add_link(type=1,node1_id=20, node2_id=2, node1_port=1, node2_port=3, capacity=20, delay=20, weight=20)
    #<h4-s4>
    add_link(type=1,node1_id=40, node2_id=4, node1_port=1, node2_port=3, capacity=40, delay=40, weight=40)
    #<h5-s5>
    add_link(type=1,node1_id=50, node2_id=1, node1_port=1, node2_port=5, capacity=50, delay=50, weight=50)
    
    draw_graph()
    #edge_switch_info_list = [(1,1,3,1), (1,1,2,3), (1,1,4,3), (1,1,1,5), (4,3,3,3)]
    edge_switch_info_list = [(1,1), (2,3), (3,1), (4,3), (1,5)]
    all_paths = get_all_paths_between_all_edge_switches(edge_switch_info_list)
    for key,value in all_paths.items():
        print "Between Nodes (%s): total Paths = [%s]:%s" %(key,len(value),value)
    #print all_paths
    
if __name__ == '__main__':
    #test_L2FwdTables()
    test_simple_loop_graph()