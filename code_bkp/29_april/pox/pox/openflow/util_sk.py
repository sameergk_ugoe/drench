# Copyright 2011-2013 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is loosely based on the discovery component in NOX.

"""
This module sets up the Drench Utils.
"""

from pox.lib.revent import *
from pox.lib.recoco import Timer
from pox.lib.util import dpid_to_str, str_to_bool
from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt
from pox.lib.addresses import EthAddr, IPAddr
import csv
import os.path
import math
import struct
import time
import copy
from collections import namedtuple, OrderedDict
from random import shuffle, random, randint
from os.path import expanduser
hdir_p = os.path.join(expanduser('~'), 'drench')
rdir_p = os.path.join(hdir_p, 'results')
log = core.getLogger()
rsc_file = "list3.csv"
svc_ch_file = os.path.join(hdir_p, 'svc_chains', 'svc_chain_dict_3_6.csv')
nfv_loc_info_dirty_bit = False
####################################

class nfv_rsrcs():
    def __init__ (self, svc_id, ip_addr, sp=0, mac_addr=None, sw_id=None, port_id=None):
        self.set_nfv_info(svc_id, sp,ip_addr, mac_addr, sw_id, port_id)
        self.updated = False
        return
    def set_nfv_info(self, svc_id, ip_addr, sp=0, mac_addr=None, sw_id=None, port_id=None):
        self.svc_id = svc_id
        self.sp = sp
        self.ip_addr = ip_addr
        self.mac_addr = mac_addr
        self.sw_id = sw_id
        self.port_id = port_id
        self.updated = True
        return
    def get_nfv_rsrc_type(self): return self.svc_id
    def get_nfv_info(self):
        return (self.svc_id,[self.sp, self.mac_addr,self.ip_addr,self.sw_id,self.port_id])
    def getSP(self):
        return self.sp
    def getMac(self):
        return EthAddr(self.mac_addr).toStr()
    def getIP(self):
        return IPAddr(self.ip_addr).toStr()
    def getSW_and_PortId(self):
        return self.sw_id,self.port_id

nfv_rsrc_map_dict = {}
input_rsrc_dict = {}
def load_rsrc_file(rsrc_file=rsc_file):
    global input_rsrc_dict
    with open(rsrc_file, 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            sw_num = int(row['switch'])
            svc = int(row['function'],2)
            mac = row['mac'].lstrip(' ').rstrip(' ')
            ip  = row['ip'].lstrip(' ').rstrip(' ')
            sp  = int(row['sp'])
            fnc = str(row['function']).lstrip(' ').rstrip(' ')
            entry = [sp,mac,ip,sw_num,0]
            if input_rsrc_dict.has_key(svc):
                input_rsrc_dict[svc].append(entry)
            else:
                input_rsrc_dict[svc] = [entry]
                
            nfv_obj = nfv_rsrcs(svc_id=svc, sp=sp,ip_addr=ip, mac_addr=mac, sw_id=sw_num, port_id=None)
            if nfv_rsrc_map_dict.has_key(svc):nfv_rsrc_map_dict[svc].append(nfv_obj)
            else:nfv_rsrc_map_dict[svc] = [nfv_obj]
    return

def get_nfv_obj_map_dict():
    return nfv_rsrc_map_dict
def get_nfv_dict():
    #log.warning("Input NFV Dictionary: %s", input_rsrc_dict)
    return copy.deepcopy(input_rsrc_dict)
def update_nfv_rsrc_info():
    nfi_update_list = []
    nfi_obj_update_list = []
    for svc, nf_entries in input_rsrc_dict.iteritems():
        for entry in nf_entries:
           (ip_addr,mac_addr,sw_id,port_id) = get_addr_map_from_ip_addr(IPAddr(entry[2]))
           if ip_addr is None or mac_addr is None: continue
           entry[1] = EthAddr(mac_addr).toStr()
           entry[2] = IPAddr(ip_addr).toStr()
           entry[3] = sw_id
           entry[4] = port_id
           nfi_update_list.append(entry)
        #return entry
    
    for svc, nf_entries in nfv_rsrc_map_dict.iteritems():
        for entry in nf_entries:
           (ip_addr,mac_addr,sw_id,port_id) = get_addr_map_from_ip_addr(IPAddr(entry.ip_addr))
           if ip_addr is None or mac_addr is None: continue
           entry.set_nfv_info(entry.svc_id, ip_addr=IPAddr(ip_addr).toStr(), mac_addr=EthAddr(mac_addr).toStr(), sw_id=sw_id, port_id=port_id)
        nfi_obj_update_list.append(entry)
        #return entry.get_nfv_info()[1]
    
    return nfi_obj_update_list    
    #return nfi_update_list

#import pox.misc.svcgen_sk as SvcGen
import pox.misc.SvcChainGenerator as SvcGen
def load_svc_chain_info(svc_chain_file=svc_ch_file):
  SvcGen.read_to_dict_from_csv(svc_chain_file)
  log.info ("Loaded Service Chain with length [%d]!", get_svc_chain_len_for_key(8))

def get_svc_chain_for_key(key):
  svc = SvcGen.get_svc_chain_for_tos(ip_tos=key)
  if svc is None:
    log.error("get_svc_chain_for_key():: SVC for key[%d] is not Available!", key)
    svc = []
  log.info("get_svc_chain_for_key():: for key[%d] chain=[%s]!", key, svc)
  return svc

def get_svc_chain_len_for_key(key):
  svc = SvcGen.get_svc_chian_len_for_tos(ip_tos=key)
  if svc is None:
    log.error("get_svc_chain_len_for_key():: SVC for key[%d] is not Available!", key)
    svc = -1
  log.info("get_svc_chain_len_for_key():: for key[%d] len=[%d]!", key, svc)
  return svc
  
def get_svc_for_key_at_index(key,index):
  svc = SvcGen.get_svc_at_index_for_tos(ip_tos=key, index=index)
  if svc is None:
    log.error("SVC for key[%d], index[%d] is not Available!", key, index)
    svc = -1
  log.info("get_svc_for_key_at_index():: for key[%d] at index [%d] SVC=[%d]!", key, index, svc)
  return svc

#Helper functions for Path Settings
def get_base_dir():
    return hdir_p
def get_results_dir():
    return rdir_p
def get_rsrc_file():
    return rsc_file

#Helper Functions for IP/MAC/(Swid,port) map
ip_mac_sw_obj_list = []
class ip_mac_switch_map():
    #_list_count = 0
    def __init__ (self,ip_addr=None, mac_addr=None, sw_id=None, port_id=None):
        self.set_map_entry(ip_addr, mac_addr, sw_id, port_id)
        #_list_count+=1
    def set_map_entry(self,ip_addr, mac_addr, sw_id, port_id):
        self.ip_addr = ip_addr
        self.mac_addr = mac_addr
        self.sw_id = sw_id
        self.port_id = port_id
        #log.warning("Setup entry[%s:%s:%s:%s]", ip_addr, mac_addr,sw_id,port_id)
    def get_ip_from_mac(self, mac_addr):
        if self.mac_addr == mac_addr: return self.ip_addr
        return None
    def get_mac_from_ip(self, ip_addr):
        if self.ip_addr == ip_addr: return self.mac_addr
        return None
    def get_ip_from_sw_id_and_port_id(self, sw_id,port_id):
        if self.sw_id == sw_id and self.port_id == port_id: return self.ip_addr
        return None
    def get_mac_from_sw_id_and_port_id(self, sw_id,port_id):
        if self.sw_id == sw_id and self.port_id == port_id: return self.mac_addr
        return None
def validate_for_non_duplicate_entry(ip_addr,mac_addr, sw_id, port_id):
    non_duplicate = True
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.mac_addr == mac_addr or map_obj.ip_addr == ip_addr or (map_obj.sw_id == sw_id and map_obj.port_id == port_id):
            log.warning("Existing Entry:(%s,%s,(%s:%d)), New Request(%s:%s,(%s:%d))",map_obj.mac_addr,map_obj.ip_addr,map_obj.sw_id,map_obj.port_id,mac_addr,ip_addr,sw_id,port_id)
            map_obj.set_map_entry(ip_addr,mac_addr, sw_id, port_id)
            return False
    return non_duplicate
def add_to_address_map_table(ip_addr,mac_addr, sw_id, port_id):
    global ip_mac_sw_obj_list, nfv_loc_info_dirty_bit
    
    ip_addr = IPAddr(ip_addr).toStr()
    mac_addr = EthAddr(mac_addr).toStr()
    
    if len(ip_mac_sw_obj_list):
        if validate_for_non_duplicate_entry(ip_addr,mac_addr, sw_id, port_id) is False: return None
    ip_mac_sw_obj_list.append(ip_mac_switch_map(ip_addr,mac_addr, sw_id, port_id))
    nfv_loc_info_dirty_bit = True
    
def get_addr_map_from_mac_addr(mac_addr):
    mac_addr = EthAddr(mac_addr).toStr()
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.mac_addr == mac_addr:
            return(map_obj.ip_addr,map_obj.mac_addr,map_obj.sw_id,map_obj.port_id)
    return (None,None,None,None)
def get_addr_map_from_ip_addr(ip_addr):
    ip_addr = IPAddr(ip_addr).toStr()
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.ip_addr == ip_addr:
            return(map_obj.ip_addr,map_obj.mac_addr,map_obj.sw_id,map_obj.port_id)
    return (None,None,None,None)
def get_addr_map_from_sw_id_and_port_id(sw_id,port_id):
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.sw_id == sw_id and map_obj.port_id == port_id:
            return(map_obj.ip_addr,map_obj.mac_addr,map_obj.sw_id,map_obj.port_id)
    return (None,None,None,None)

class NfvLocEvent (Event):
  """
  NFV Location Update event
  """
  def __init__ (self, add, svc_instance):
    Event.__init__(self)
    self.svc_instance = svc_instance
    self.added = add
    self.removed = not add
    
def nfv_svc_update_function():
    global nfv_loc_info_dirty_bit
    nfi_list = None
    if nfv_loc_info_dirty_bit:
        nfi_list = update_nfv_rsrc_info()
        nfv_loc_info_dirty_bit = False
    return nfi_list

class DrenchUtil (EventMixin):
  """
  Component provides basic drench utilities
  1. Sets up base paths and directories for execution environment
  2. Loads the Service chain information
  3. Loads the NFV resource file information.
  4. TODO: Update the NFV resource file information based on IP and translate to Service Dictionary
  5. TODO: client/server to pull/push information from/to host NFVs
  """
  
  #_rsc_file = rsc_file     # Priority of LLDP-catching flow (if any)
  #_base_dir = hdir_p        # How long until we consider a link dead
  nfv_update_interval = 5  # How often to check for NFV Resource Updates
  _core_name = "drench_util" # we want to be core.openflow_discovery
  _eventMixin_events = set([NfvLocEvent,])
  
  def __init__ (self):
    
    # Listen with a high priority (mostly so we get PacketIns early)
    #core.listen_to_dependencies(self,
    #    listen_args={'openflow':{'priority':0xffffffff}})
    #log.warning("base_dir=[%s] and Rsrc File[%s]", self._base_dir, self._rsc_file)
    
    Timer(self.nfv_update_interval, self._nfv_svc_update_function, recurring=True)
    return
    
  def _nfv_svc_update_function(self):
    nfi_list = nfv_svc_update_function()
    if nfi_list:
        self.raiseEventNoErrors(NfvLocEvent, True, nfi_list)
        #log.warning("Raising NfvLocEvent with NFI_LIST[%s]!!", nfi_list)
    return
    
def launch (base_dir = None, rsrc_file = None, svc_chain_info_file = None):
  global hdir_p, rdir_p, rsc_file, svc_ch_file
  if base_dir is not None and os.path.isdir(base_dir):
    hdir_p = base_dir
    rdir_p = os.path.join(hdir_p, 'results')
    if not os.path.isdir(rdir_p):
      os.makedirs(rdir_p)
  else:
    log.error(" Directory [%s] not found!",base_dir)
  if rsrc_file is not None and os.path.isfile(rsrc_file) and len(rsrc_file) > 0: 
    rsc_file = rsrc_file
  else:
    log.error(" File [%s] not found!",rsrc_file)
  if svc_chain_info_file is not None and os.path.isfile(svc_chain_info_file):
    svc_ch_file = svc_chain_info_file
  else:
    log.info(" Invalid svc_chain_file [%s], using default [%s]!!", svc_chain_info_file, svc_ch_file)
    
  load_rsrc_file(rsrc_file=rsc_file)
  load_svc_chain_info(svc_chain_file=svc_ch_file)
  #Timer(nfv_update_interval, nfv_svc_update_function, recurring=True)
  core.registerNew(DrenchUtil)
