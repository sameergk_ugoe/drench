# Copyright 2012-2013 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
A shortest-path forwarding application.

This is a standalone L2 switch that learns ethernet addresses
across the entire network and picks short paths between them.

You shouldn't really write an application this way -- you should
keep more state in the controller (that is, your flow tables),
and/or you should make your topology more static.  However, this
does (mostly) work. :)

Depends on openflow.discovery
Works with openflow.spanning_tree
"""

from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.recoco import Timer
from collections import defaultdict, OrderedDict
from pox.openflow.discovery_sk import Discovery
from pox.lib.util import dpid_to_str
import time
from pox.lib.packet.vlan import vlan
from pox.lib.addresses import EthAddr, IPAddr
import pox.lib.packet as pkt
import pox.openflow.discovery_sk as disc
#import pox.samples.flow_stats_sk as fstat
from copy import deepcopy
import csv
log = core.getLogger()
from os.path import expanduser
hdir_p = expanduser('~')

dbg_flow_counter = 0
# Adjacency map.  [sw1][sw2] -> port from sw1 to sw2
adjacency = defaultdict(lambda:defaultdict(lambda:None))

# Switches we know of.  [dpid] -> Switch
switches = {}

# ethaddr -> (switch, port), e.g 00:00:00:00:00:01 at (00-00-00-00-00-01, 1)
mac_map = {}

# [sw1][sw2] -> (distance, intermediate)
path_map = defaultdict(lambda:defaultdict(lambda:(None,None)))

# Waiting path.  (dpid,xid)->WaitingPath
waiting_paths = {}

# Time to not flood in seconds
FLOOD_HOLDDOWN = 5

# Flow timeouts
FLOW_IDLE_TIMEOUT = 10 #100 #10
FLOW_HARD_TIMEOUT = 30 #300 #30

# How long is allowable to set up a path?
PATH_SETUP_TIME = 4

# Table of Flows Installed for Services
svc_flows_table = {}        #Key = (svc,mac_addr_svc):[ [src_ip1, path1, match1], [src_ip2, path2, match2], ...]
next_svc_flows_table = {}   #Key = (svc,mac_addr_path_src):[ [src_ip1, path1], [src_ip2, path2], ...]

def clear_flow_for_svc_on_path(svc_id=0, svc_mac_addr= None, path=None, match=None, src_ip=None):
  
  if path is None:
    log.warning("clear_flow_for_svc_on_path(No Path Specififed!!)")
    return False
  if match is None and src_ip is None:
    log.warning("clear_flow_for_svc_on_path(No MATCH or SCRIP Specififed!!)")
    return False
  
  msg     = None
  command = None
  if match is None:
    match = of.ofp_match(nw_src=src_ip)
    match.dl_type = 0x800
    match.dl_src = EthAddr("00:00:00:00:00:00") #match.dl_src 
    match.dl_dst = EthAddr("00:00:00:00:00:00") #match.dl_dst 
    command=of.OFPFC_DELETE
  else:
    command=of.OFPFC_DELETE_STRICT
    #command=of.OFPFC_DELETE
    
  msg = of.ofp_flow_mod(match=match,command=command)
  msg.match.dl_vlan = svc_id
  
  ### OLD WAY < DELETE ALL THE PATHS > 
  """
  for sw,in_port,out_port in path: 
    #msg.match.out_port = out_port
    msg.match.in_port = in_port
    log.debug("Clearing Flows on Switch:[%s] with match:[%s]", sw, match)
    sw.connection.send(msg)
  """
  
  #NEW APPROACH <DELETE ONLY ON FIRST SWITCH, Remaining are OK to be kept active>
  """
  vlan_instance_id = svc_id
  svc_switch =          path[-1][0]
  svc_switch_out_port = path[-1][2]
  svc_switch_dpid     = svc_switch.dpid
  svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=svc_switch_dpid, port_id=svc_switch_out_port)
  if svc_mac_addr is None:
    log.error("Failed to Find the SVC MAC ADDRESS FROM WHICH THE FLOW IS TO BE CLEARED!!")
  else:
    byte_mac_str =  str(svc_mac_addr)
    byte_mac = int(byte_mac_str[byte_mac_str.rfind(':')+1:])
    byte_mac = byte_mac << 4
    vlan_instance_id = ((svc_id & 0xF) | (byte_mac & 0xFF0)) & 0xFFF
  """
  index = 0
  for sw,in_port,out_port in path:
    if index == 0:
      msg.match.in_port = in_port
      #log.warning("Clearing Flows on Switch:[%s] with match:[%s]", sw, match)
      sw.connection.send(msg)
    else:
      #msg.match.dl_vlan
      pass
    index +=1
  
  # Commented here coz, it needs to be done after svc_flow_table_update in the caller
  #update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=svc_mac_addr,path=path,addition=False)
  
  rem_hopcount_for_installed_route(svc_id=svc_id, hop_count=len(path),path=path, redirected=True)
  
  global dbg_flow_counter
  dbg_flow_counter -=1
  
  return True

def clear_next_svc_flow_table():
  global next_svc_flows_table
  log.warning("Clearing Next Service Flow Table[%s]", next_svc_flows_table)
  next_svc_flows_table.clear()
  return

def update_svc_flow_table_on_svc_instance_bringdown(svc_id=0,svc_mac_addr=""):
  if svc_id is 0 or svc_mac_addr is None:
    log.warning("Invalid Service instance!! ")
    return False
    
  key = (svc_id,svc_mac_addr)
  if (False == svc_flows_table.has_key(key)):
    log.warning(" No Flows found in svc_flow_table for key[%s] !! ", key)
  else:
    if len(svc_flows_table[key]):
      log.warning ("Before Svc Termination for Key[%s] total flows seen = %d", key, len(svc_flows_table[key]))
    svc_flows_table.pop(key)
    return False
    
  #log.debug("Svc Flow Table: [%s]", svc_flows_table)
  return True
  
def update_svc_flow_table_on_flow_removal_event(sw_dpid=0, event=None):
  if event is None or event.ofp is None or event.ofp.match is None:
    return
  # Handle only the Timeout Rules,  Force Deleted will be handled internally <otherwise results in unnecessary re-updates of SP (making it fluctuate )>
  if event.ofp.reason == of.OFPRR_DELETE: # of.OFPRR_HARD_TIMEOUT or event.ofp.reason == of.OFPRR_IDLE_TIMEOUT:
    log.warning("Skipped processing the Explicitly Deleted rule on sw_dpid[%d]",sw_dpid)
    return
  
  # Handle only the Rules on Edge port Switches
  if core.openflow_discovery.is_edge_port(sw_dpid, event.ofp.match.in_port) is False:
    log.debug("Skipped processing Deleted rule on Non Edge Port!")
    return
  
  match = event.ofp.match
  if match.dl_vlan is None or match.dl_vlan == 0 or match.dl_vlan == 65535:
    log.debug("Skipped processing Deleted rule: No Matching DL_VLAN")
    #if match.dl_vlan == 0: # requires setting MASK for installed rule
    #  rem_hopcount_for_installed_route(svc_id=0, hop_count=0,path=None, redirected=False)
    return
  svc_id = match.dl_vlan
  #svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid, match.in_port)
  #if svc_mac_addr is None:
  #  log.warning("No Matching MAC")
  #  return
  #key = (svc_id,svc_mac_addr)
  #if (False == svc_flows_table.has_key(key)):
  #  log.warning("Key[%s] with SVC[%d] on MAC[%s] is not in the current set of flows! [%s]", key, svc_id, svc_mac_addr, svc_flows_table)
  #  return
  
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  found = False
  for ip,mac,sp in ip_mac_sp_list:
    svc_mac_addr = EthAddr(mac)
    key = (svc_id,svc_mac_addr)
    if (False == svc_flows_table.has_key(key)): continue

    cur_flows = svc_flows_table[key]
    update_flows = []
    path = None
    for flow in cur_flows:
      # Switch ID, Input Port and the Match should be exact Matches.
      c_path = flow[1]
      sw = c_path[0][0]
      in_port = c_path[0][1]
      #log.debug("Sw:[%s], in_port[%d]",sw,in_port)
      if sw_dpid == sw.dpid and match.in_port == in_port and match == flow[2]:
        found = True
        path = c_path
        log.debug("Found Exact Match for Input_Match:[%s], Table_Match:[%s]", match, flow[2])
      else:
        update_flows.append(flow)
    if found is True:
      if update_flows and len(update_flows) > 0:
        svc_flows_table[key] = update_flows
      else:
        svc_flows_table.pop(key)
      log.warning("Update SP on SVC Instance [%d, %s] On Flow Eviction! ", svc_id, svc_mac_addr)
      update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=svc_mac_addr,path=path,addition=False)
      rem_hopcount_for_installed_route(svc_id=svc_id, hop_count=len(path),path=path, redirected=False)
      break

  #if found: log.warning("After Removal on FlowRemoved Event Processing:%s", svc_flows_table)
  
  global dbg_flow_counter
  dbg_flow_counter -=1
  
  return

def add_path_to_next_svc_flows_table(svc_id=0,svc_mac_addr="", src_ip= "", path="", match=""):
  global next_svc_flows_table
  ret = False
  if svc_id is None or svc_mac_addr is None or src_ip is None or path is None or match is None:
    log.warning(" Incorrect data! Skipped to add to next_svc_flows_table")
    return False
  _match = deepcopy(match)
  
  src_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=path[0][0].dpid,port_id=path[0][1])
  if src_mac_addr is None:
    log.warning(" Incorrect SRC data! Skipped to add to next_svc_flows_table")
    return False
    
  key = (svc_id,src_mac_addr)
  #log.debug("Obtained Key:: (%d,%s): %s::[%d:%s]", svc_id, src_mac_addr, key, key[0], key[1])
  if (False == next_svc_flows_table.has_key(key)):
    next_svc_flows_table[key] = [[src_ip,path,_match]]
    ret = True
  else:
    if (False == (any(src_ip== x[0] and path == x[1] for x in next_svc_flows_table[key]))):
      next_svc_flows_table[key].append([src_ip,path,_match])
      ret = True
    pass
    
  log.debug("add_path_to_next_svc_flows_table():: Ret=[%d], NextSvcFlowTable: [%s]", ret, next_svc_flows_table)
  return ret

def get_path_from_next_svc_flows_table(svc_id=0,sw_dpid=0, in_port=0, match=None):
  global next_svc_flows_table
  src_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=sw_dpid,port_id=in_port)
  key = (svc_id,src_mac_addr)
  if (False == next_svc_flows_table.has_key(key)): return None
  
  path = None
  paths_list = next_svc_flows_table[key]
  for index, path_entry in enumerate(paths_list):
    iip,ipath,imatch = path_entry[0],path_entry[1],path_entry[2]
    log.debug(" Input: Key[%s], match[%s],, Table: ip:[%s], path[%s], match[%s]", key, match, iip, ipath,imatch)
    if iip == match.nw_src:
    #if imatch == match: 
      path = ipath
      next_svc_flows_table[key].pop(index)
      break
    else: continue
  if not next_svc_flows_table[key]:
    next_svc_flows_table.pop(key)
    
  log.debug("get_path_from_next_svc_flows_table():: SVC[%d] SRC[%d:%d] for IP[%s] retrieved path[%s] ", svc_id,sw_dpid,in_port,match.nw_src,path)
  return path
  
def get_new_path_to_service(svc_id=0,svc_mac_addr=None,src_ip=None, cur_path=None, cur_match=None):
  if svc_mac_addr is None or cur_path is None:
    return None,None
    
  src_sw = cur_path[0][0]
  src_port = cur_path[0][1]
  dest_addr = mac_map[svc_mac_addr]
  new_path = _get_path(src_sw, dest_addr[0], src_port, dest_addr[1])
  log.info(" Computed new path for SVC[%d] for scr_ip[%s] From[%s:%d] to svc_mac_addr[%s] as [%s]",svc_id, src_ip, new_path[0][0], new_path[0][1], svc_mac_addr, new_path)
  return new_path,cur_match

def proactive_install_path_for_svc_on_path(svc_id=0,svc_mac_addr=None,path=None,match=None,src_ip=None):
  if svc_mac_addr is None or path is None:
    return False
  byte_mac_str =  str(svc_mac_addr)
  byte_mac = int(byte_mac_str[byte_mac_str.rfind(':')+1:])
  byte_mac = byte_mac << 4
  vlan_instance_id = ((svc_id & 0xF) | (byte_mac & 0xFF0)) & 0xFFF
      
  if match is None:
    match = of.ofp_match(nw_src=src_ip)
    match.dl_type = 0x800
    match.dl_src = EthAddr("00:00:00:00:00:00")
    match.dl_dst = EthAddr("00:00:00:00:00:00")
    log.warning("Input Match is Empty! Generated new copy [%s] ", match)
  #wp = None
  wp = WaitingPath(path, None)
  log.warning("proactive_install_path_for_svc_on_path() At:[%s] Installing Path for Service[%d] as [%s] ", time.time(),svc_id,path)
  i = 0
  for sw,in_port,out_port in path:
    if wp:
      wp_msg = of.ofp_barrier_request()
    
    msg = of.ofp_flow_mod()
    msg.flags = of.OFPFF_SEND_FLOW_REM
    #msg.match = match

    msg.match = of.ofp_match()
    msg.match.in_port = in_port
    msg.idle_timeout = FLOW_HARD_TIMEOUT #FLOW_IDLE_TIMEOUT #0
    #msg.hard_timeout = FLOW_HARD_TIMEOUT #0
    
    msg.match.dl_vlan = svc_id
    #msg.buffer_id    = None 
    
    #log.warning("proactive_install_path_for_svc_on_path() At:[%s] Installing Rule for Service[%d] on Switch[%s:%d]-->[%d]", time.time(),svc_id,sw,in_port,out_port)
    if(i == 0):
      msg.match.nw_src = match.nw_src
      msg.match.nw_dst = None
      msg.match.dl_type = match.dl_type
      msg.match.dl_src = match.dl_src
      msg.match.dl_dst = match.dl_dst
      msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = vlan_instance_id))
      if(in_port == out_port):
        msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
      else:
        msg.actions.append(of.ofp_action_output(port = out_port))
      # to ensure that we clear the item for next_svc_flows_table: ideally do not push to next_svc_flows_table in case of proactive rule setup.
      get_path_from_next_svc_flows_table(svc_id=svc_id,sw_dpid=sw.dpid, in_port=in_port, match=match)
      add_install_route(svc_id=svc_id, path=path, match=msg.match, redirected = True)
    else:
      msg.match.dl_vlan = vlan_instance_id
      msg.match.in_port = None
      
      msg.match.nw_src = None
      msg.match.nw_dst = None
      msg.match.dl_type = None
      msg.match.dl_src = None
      msg.match.dl_dst = None
      
      #msg.match.nw_src = match.nw_src
      #msg.match.nw_dst = None
      #msg.match.dl_type = match.dl_type
      #msg.match.dl_src = match.dl_src
      #msg.match.dl_dst = match.dl_dst
      
      msg.actions.append(of.ofp_action_output(port = out_port))

    sw.connection.send(msg)
    if wp and wp_msg:
      sw.connection.send(wp_msg)
      wp.add_xid(sw.dpid,wp_msg.xid)
    i+=1
  
  return True
  
redirection_log = {} # {'svc_id':[redir_count, src_switch1, from_svc_mac1, to_svc_mac1,  src_switch2, from_svc_mac2, to_svc_mac2,]
def add_to_redirection_log(svc_id=0, orig_sw_dpid=None, old_svc_mac=None, new_mac_addr=None):
  if (False == redirection_log.has_key(svc_id)):
    redirection_log[svc_id] = [1, orig_sw_dpid, old_svc_mac, new_mac_addr]
  else:
    #redirection_log[svc_id] = [redirection_log[svc_id][0]+1, orig_sw_dpid, old_svc_mac, new_mac_addr]
    redirection_log[svc_id] = [redirection_log[svc_id][0]+1] + redirection_log[svc_id][1:] + [orig_sw_dpid, old_svc_mac, new_mac_addr]
    # For safety: check if the occurance is already in the log
    #for i in range (1, len(redirection_log[svc_id])-2):
    #  if orig_sw_dpid == redirection_log[svc_id][i] and  old_svc_mac == redirection_log[svc_id][i+1] and new_mac_addr== redirection_log[svc_id][i+2]:
    #    log.warning("Found Redirection for the same path to same DST performed again!!")
    #    break
    pass
  return

def redirect_path_for_svc(svc_id=0,cur_svc_mac_addr="",next_svc_mac_addr="", for_src_ip=""):
  global svc_flows_table, next_svc_flows_table
  if svc_id is None or cur_svc_mac_addr is None or next_svc_mac_addr is None:
    log.warning(" Incorrect data! Skipped to redirect flows!")
    return
  
  key = (svc_id,cur_svc_mac_addr)
  if (False == svc_flows_table.has_key(key)):
    #log.warning("Key[%s] with SVC[%d] on MAC[%s] is not in current set of flows![%s]", key, svc_id, cur_svc_mac_addr, svc_flows_table.keys())
    log.warning(" Incorrect Instance [%s] selection! Not found in the SVC Table: [%s]", cur_svc_mac_addr, svc_flows_table)
    return 
  
  log.warning("ReDirecting path for Service[%d] for IP[%s] from [%s] to [%s]",svc_id,for_src_ip, cur_svc_mac_addr,next_svc_mac_addr)
  cur_flows = svc_flows_table[key]
  cur_path = None
  ret = False
  update_flows = []
  for flow in cur_flows:
    if for_src_ip == flow[0] and ret is False:
      path = flow[1]
      match = flow[2]
      new_path,new_match = get_new_path_to_service(svc_id=svc_id,svc_mac_addr=next_svc_mac_addr,src_ip=for_src_ip, cur_path=path, cur_match=match)
      #ret = add_path_to_next_svc_flows_table(svc_id=svc_id,svc_mac_addr=next_svc_mac_addr, src_ip=for_src_ip, path=new_path, match=new_match)
      ret = True
      if ret is True:
        # Clear the rules for the redirected flows and setup new rules to enable correct redirection!
        cur_path = path
        clear_flow_for_svc_on_path(svc_id=svc_id, svc_mac_addr=cur_svc_mac_addr, path=path, match=match, src_ip=for_src_ip)
        proactive_install_path_for_svc_on_path(svc_id=svc_id,svc_mac_addr=next_svc_mac_addr,path=new_path,match=new_match,src_ip=for_src_ip)
        add_to_redirection_log(svc_id=svc_id,orig_sw_dpid=path[0][0], old_svc_mac=cur_svc_mac_addr, new_mac_addr=next_svc_mac_addr)
        pass  # No break here, since we  need to re-update the svc_flow_table
    else:
      update_flows.append(flow)
  if update_flows and len(update_flows) > 0:
    svc_flows_table[key] = update_flows
  #else:
    #svc_flows_table.pop(key)
  #log.debug("After Redirection: CUR_SVC_FLOW_TABLE: %s", svc_flows_table)
  
  # Also Update SP after flow redirection from the cur_svc_mac 
  if ret is True:
    update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=cur_svc_mac_addr,path=cur_path,addition=False)
  return

def add_path_to_svc_flows_table(svc_id=0,svc_mac_addr="", src_ip= "", path="", match=""):
  global svc_flows_table
  if svc_id is None or svc_mac_addr is None or src_ip is None or path is None or match is None:
    log.warning(" Incorrect data! Skipped to add to svc_flow_table")
    return False
  _match = deepcopy(match)
  key = (svc_id,svc_mac_addr)
  #log.debug("Obtained Key:: (%d,%s): %s::[%d:%s]", svc_id, svc_mac_addr, key, key[0], key[1])
  if (False == svc_flows_table.has_key(key)):
    svc_flows_table[key] = [[src_ip,path,_match]]
  else:
    if (False == (any(src_ip== x[0] and path == x[1] for x in svc_flows_table[key]))):
      svc_flows_table[key].append([src_ip,path,_match])
    else:
      log.warning("Duplicate Path Install for the already installed path! skip adding to flow table! [%d:%s:%s:%s]", svc_id,svc_mac_addr,src_ip, path )
      return False
    
  #log.debug("Svc Flow Table: [%s]", svc_flows_table)
  return True

test_count = 0
def test_redirect(svc_id=0,svc_mac_addr="", src_ip= "", path="", match=""):
  global test_count
  if test_count or svc_id != 13:
    return
    
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  mac_addr = None
  for ip,mac,sp in ip_mac_sp_list:
    _mac = EthAddr(mac)
    log.warning("svc_mac_addr[%s], mac[%s], _mac[%s]",svc_mac_addr,mac,_mac )
    if _mac != svc_mac_addr:
      mac_addr = _mac
      log.warning ("Testing to redirect flows for SVC[%d] from current MAC[%s] to new MAC[%s] ", svc_id,svc_mac_addr,mac_addr)
      break
  if mac_addr:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=svc_mac_addr,next_svc_mac_addr=mac_addr, for_src_ip=src_ip)
  else:
    log.warning("Failed to find alternate MAC for service[%d]", svc_id)
  
  test_count+=1

def get_srcip_for_optimal_flow_redirection(key=None,next_svc_mac_addr=None):
  if key is None or next_svc_mac_addr is None:
    return None
  #cur_mac_addr = key[1]
  if next_svc_mac_addr not in mac_map.keys():
    log.error(" MAC Addr:[%s] not found in mac_map [%s]",next_svc_mac_addr,mac_map.keys())
    return None

  optimal_srcip = None
  dst_addr = mac_map[next_svc_mac_addr]
  dst_sw,dst_port = dst_addr[0],dst_addr[1]
  optimal_path_len = -1
  if not svc_flows_table.has_key(key):
    return None
  for flow in svc_flows_table[key]:
    new_path = None
    path  = flow[1]
    src_sw  = path[0][0]
    src_port = path[0][1]
    init_out_port = path[0][2]
    new_path = _get_path(src_sw, dst_sw, src_port, dst_port)
    if not new_path: continue
    if len(new_path) > 2 and new_path[0][2] != init_out_port:
      #log.warning("New chosen path not in same direction as current svc! ")
      pass
    if optimal_path_len < 0:
      optimal_srcip = flow[0]
      optimal_path_len = len(new_path)
    else:
      if optimal_path_len > len(new_path):
        optimal_srcip = flow[0]
        optimal_path_len = len(new_path)
        if len(new_path) > 2 and new_path[0][2] != init_out_port:
          log.warning("New chosen path[%s] not in same direction as current svc path [%s]!", new_path,path)
  """"
  optimal_srcip = None
  src_addr = mac_map[next_svc_mac_addr]
  src_sw,src_port = src_addr[0],src_addr[1]
  optimal_path_len = -1
  for flow in svc_flows_table[key]:
    path  = flow[1]
    dst_sw  = path[-1][0]
    dst_port = path[-1][2]  
    path = _get_path(src_sw, dst_sw, src_port, dst_port)
    if optimal_path_len < 0:
      optimal_srcip = flow[0]
      optimal_path_len = len(path)
    else:
      if optimal_path_len > len(path):
        optimal_srcip = flow[0]
        optimal_path_len = len(path)
  """
  log.warning("Chose the Path with Len[%d] and SRCIP[%s]!",optimal_path_len, optimal_srcip)
  return optimal_srcip

def redirect_flows_from_this_instance(svc_id=0, svc_mac_addr=None, check_for_flows = False):
  #Move some flows from this service instance towards other lightly loaded instance
  #Which instance to offload the flow to: - Pick the best SP or MIN SP or MIN flows
  #Which flow to pick from this instance: - pick the one nearest to the new instance
  #How many flows? == 1
  key = (svc_id,svc_mac_addr)
  flows_on_cur_svc = 0
  if (False == svc_flows_table.has_key(key)):
    log.warning("redirect_flows_from_this_instance()::Key[%s] with SVC[%d] on MAC[%s] is not in the current set of flows!", key, svc_id, svc_mac_addr)
    return 
  else:
    flows_on_cur_svc = len(svc_flows_table[key])
  
  # Pick the alternate best possible Mac address of the Service: 
  mac_addr = None
  # Get the Current best MAC for the Service 
  b_ip, b_mac, b_sp = core.openflow_discovery.getIP_MAC_of_BestSP_sk(svc_id)
  _mac = EthAddr(b_mac)  
  if _mac != svc_mac_addr:
    mac_addr = _mac
  else: # Get any other alternative Mac
    ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
    for ip,mac,sp in ip_mac_sp_list:
      _mac = EthAddr(mac)
      if _mac != svc_mac_addr:
        mac_addr = _mac
        break
  if mac_addr is None:
    log.warning("redirect_flows_from_this_instance(SVC[%d]):: Failed to find alternate MAC!", svc_id)
    return
  log.warning ("redirect_flows_from_this_instance(SVC[%d]), Move from current MAC[%s] to new MAC[%s] ", svc_id,svc_mac_addr,mac_addr)
  
  #2 number of Flows:
  redir_flows_count = flows_on_cur_svc*0.5
  
  # Pick one of the flows (IP) from the Flow table: Selection Criteria: Of all the Flows Choose the Flow with best cost gain for the chosen Service instance
  src_ip = get_srcip_for_optimal_flow_redirection(key=key,next_svc_mac_addr=mac_addr)
  
  if mac_addr and src_ip:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=svc_mac_addr,next_svc_mac_addr=mac_addr, for_src_ip=src_ip)
  else:
    log.warning("Failed to find any flow for service[%d]", svc_id)
  return

MAX_FLOWS_FOR_ALL_REDIRECTIONS = 4
def redirect_all_flows_from_this_instance(svc_id=0, svc_mac_addr=None, check_for_flows = False):
  #Move all flows from this service instance towards other lightly loaded instances
  key = (svc_id,svc_mac_addr)
  if (False == svc_flows_table.has_key(key)):
    log.warning("redirect_all_flows_from_this_instance()::Key[%s] with SVC[%d] on MAC[%s] is not in the current set of flows!", key, svc_id, svc_mac_addr)
    return False
  ttl_flows  = len(svc_flows_table[key])
  flows_list = svc_flows_table[key][0:]
  if ttl_flows >= MAX_FLOWS_FOR_ALL_REDIRECTIONS:
    log.warning("redirect_all_flows_from_this_instance()::For SVC[%d] at MAC[%s] Total flows [%d] exceeds max limit[%d]!", svc_id, svc_mac_addr, ttl_flows,MAX_FLOWS_FOR_ALL_REDIRECTIONS)
    return False
    
  log.warning("redirect_all_flows_from_this_instance()::For SVC[%d] at MAC[%s] Total flows [%d]!", svc_id, svc_mac_addr, ttl_flows)
  for i in range(0,ttl_flows):
    redirect_flows_from_this_instance(svc_id=svc_id, svc_mac_addr=svc_mac_addr, check_for_flows=check_for_flows)

  return True

def redirect_flows_towards_this_instance(svc_id=0, new_svc_mac_addr=None, check_for_flows = False):
  """ This function is called by the Timer and will redirect routes based on entries in the next_svc_flow_table"""
  #Move some other flows using other service instance towards this instance
  #Which instance to offload the flow from: - Pick the one with MAX SP or MAX flows
  #Which flow to pick from that instance: - pick the one nearest to the new instance
  #How many flows? == 1 - just be conservative.
  
  mac_addr = None
  ip_mac_sp_list = []
  
  flows_on_cur_svc = 0
  
  #0 Check for Num of Flows before initiating redirection
  if check_for_flows:
    key = (svc_id,EthAddr(new_svc_mac_addr))
    if svc_flows_table.has_key(key):
      flows_on_cur_svc = len(svc_flows_table[key])
    ttl_flows, ip_mac_sp_list = get_all_flows_for_svc(svc_id=svc_id)
    fs = MAX_FLOWS_AGR_SERVICE/float(max(1,len(ip_mac_sp_list)))
    #if ttl_flows > len(ip_mac_sp_list): #and  ttl_flows > fs:
    #Temporaritly commenting the below code: <TODO: REVERT AFTER TEST for SVC SWAP
    if ttl_flows > len(ip_mac_sp_list) and  ttl_flows > MAX_FLOWS_PER_SERVICE/2:
      pass
    else:
      log.warning("For svc[%d]:: NumInstances=[%d], Flow Count=[%d] is not enough to proceed with Redirection towards this new instance[%s] ", svc_id,len(ip_mac_sp_list),ttl_flows, new_svc_mac_addr)
      return
  else:
    ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)

  #1 pick instance (with highest SP)
  min_sp = 0
  for ip,mac,sp in ip_mac_sp_list:
    _mac = EthAddr(mac)
    if _mac != new_svc_mac_addr and min_sp < int(sp):
      mac_addr = _mac
      min_sp = int(sp)
      break

  if mac_addr is None:
    log.warning("redirect_flows_towards_this_instance(SVC[%d]):: Failed to find alternate SOURCE MAC!", svc_id)
    return
  
  #2 number of Flows:
  redir_flows_count = flows_on_cur_svc*0.5
  
  #2 Pick the optimal flow
  src_ip = get_srcip_for_optimal_flow_redirection(key=(svc_id,mac_addr),next_svc_mac_addr=new_svc_mac_addr)
  
  if mac_addr and src_ip:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=mac_addr,next_svc_mac_addr=new_svc_mac_addr, for_src_ip=src_ip)
  else:
    log.warning("redirect_flows_towards_this_instance(SVC[%d])Failed to find any flows from MAC[%s]", svc_id, mac_addr)
  return
  
def redirect_routes(svc_id=0, sw_dpid=0,out_port=0):
  """ This function is called by the Timer and will redirect routes based on entries in the next_svc_flow_table """
  svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=sw_dpid, port_id=out_port)
  if svc_mac_addr is None:
    return
  # Logic:
  # 1. Pick the Service with least Cost Factor:
  # 2. Pick one of the flows (IP) from the Flow table: Criteria: Of all the Flows Choose the Flow with best cost gain for the chosen Service instance
  #log.debug("redirect_routes():: for SVC[%d] DPID:PORT [%d:%d]!", svc_id, sw_dpid, out_port)
  
  #July 10,2015, 12:30PM
  #TODO: Logic needs to be inserted first to decide the direction of redirection for this svc_mac_addr
  #How? if  SP of SVC_MAC_ADDR is LESS THAN MIN_THRESHOLD then intent is to redirect any ongoing flows at other instances towards this instance as a new flow
  # else: redirect ongoing flows from this service instance towards any other instance.
  #Check for the Validity of the Existing Route First: if not Return
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  for ip,mac,sp in ip_mac_sp_list:
    _mac = EthAddr(mac)
    if _mac == svc_mac_addr:
      if int(sp) <= disc.MIN_SP_THRESHOLD:
        redirect_flows_towards_this_instance(svc_id=svc_id, new_svc_mac_addr=svc_mac_addr)
      else:
        redirect_flows_from_this_instance(svc_id=svc_id, svc_mac_addr=svc_mac_addr)
      break
  return
  
  key = (svc_id,svc_mac_addr)
  if (False == svc_flows_table.has_key(key)):
    ### TO DO: This is possibly case where this instance is free and wants other flows at other instances to be redirected towards itself.!! <How to handle this??>
    #redirect_flows_towards(svc_id=svc_id, new_svc_mac_addr=mac_addr)
    log.warning("redirect_routes()::Key[%s] with SVC[%d] on MAC[%s] is not in the current set of flows!", key, svc_id, svc_mac_addr)
    #log.warning("redirect_routes()::Key[%s] with SVC[%d] on MAC[%s] is not in current set of flows![%s]", key, svc_id, svc_mac_addr, svc_flows_table.keys())
    #log.warning("SVC Table: [%s]", svc_flows_table)
    return 
  
  # Pick the alternate best possible Mac address of the Service: 
  mac_addr = None
  # Get the Current best MAC for the Service 
  b_ip, b_mac, b_sp = core.openflow_discovery.getIP_MAC_of_BestSP_sk(svc_id)
  _mac = EthAddr(b_mac)  
  if _mac != svc_mac_addr:
    mac_addr = _mac
  else: # Get any other alternative Mac
    ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
    for ip,mac,sp in ip_mac_sp_list:
      _mac = EthAddr(mac)
      if _mac != svc_mac_addr:
        mac_addr = _mac
        break
  
  if mac_addr is None:
    return
  log.warning ("redirect_routes():: Redirect flows for SVC[%d] from current MAC[%s] to new MAC[%s] ", svc_id,svc_mac_addr,mac_addr)
  
  # Pick one of the flows (IP) from the Flow table: Selection Criteria: Of all the Flows Choose the Flow with best cost gain for the chosen Service instance
  src_ip = get_srcip_for_optimal_flow_redirection(key=key,next_svc_mac_addr=mac_addr)
  #src_ip = svc_flows_table[key][0][0]
  #cur_flows = svc_flows_table[key]
  #update_flows = []
  #for flow in cur_flows:
  #  if for_src_ip == flow[0]:
  
  if mac_addr and src_ip:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=svc_mac_addr,next_svc_mac_addr=mac_addr, for_src_ip=src_ip)
  else:
    log.warning("Failed to find alternate MAC for service[%d]", svc_id)
  return

def get_all_flows_for_svc(svc_id=0):
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  if ip_mac_sp_list is None:
    return 0,None
  total_count = 0
  for ip,mac,sp in ip_mac_sp_list:
    svc_mac_addr = EthAddr(mac)
    key = (svc_id,svc_mac_addr)
    if (svc_flows_table.has_key(key)):
      total_count +=len(svc_flows_table[key])

  return total_count,ip_mac_sp_list
  
MAX_FLOWS_PER_SERVICE = 10
MAX_FLOWS_AGR_SERVICE = 12 #20
def update_sp_on_flow_add_or_del(svc_id=0,svc_mac_addr=None, path=None, addition= True, match=None):
  if svc_id is None or svc_mac_addr is None :
    log.warning(" Incorrect data! Skipped to update shadow price!")
    return
  key = (svc_id,svc_mac_addr)
  svc_num_of_flows = 1
  #log.debug("Obtained Key:: (%d,%s): %s::[%d:%s]", svc_id, svc_mac_addr, key, key[0], key[1])
  if (False == svc_flows_table.has_key(key) and addition is False):
    log.warning(" Key[%s] Not found in Svc_flow_table! Last Flow Removed[%d]! ",key, addition)
    svc_num_of_flows = 0
    #return
  else:
    svc_num_of_flows = len(svc_flows_table[key])
  ttl_num_of_flows,ip_mac_sp_list = get_all_flows_for_svc(svc_id=svc_id)
  total_svc_instances = len(ip_mac_sp_list)
  #if ttl_num_of_flows == 0: ttl_num_of_flows = 1
  
  #sp = (svc_num_of_flows*100)/float(max(1,ttl_num_of_flows))
  #sp = (sp*svc_num_of_flows)/float(MAX_FLOWS_PER_SERVICE)
  #fs = MAX_FLOWS_AGR_SERVICE/float(max(1,total_svc_instances))
  #sp = (sp*svc_num_of_flows)/float(fs)
  #sp = (100*svc_num_of_flows)/float(max(MAX_FLOWS_AGR_SERVICE, ttl_num_of_flows*total_svc_instances))
  
  # Most worked is the below one:
  #sp = (100*svc_num_of_flows)/float(max(MAX_FLOWS_AGR_SERVICE, ttl_num_of_flows))
  
  #Trying New to ensure that we run to keep SP fixed based on num of flows serviced.
  sp = (100*svc_num_of_flows)/float(MAX_FLOWS_AGR_SERVICE)
  
  if sp > disc.MAX_SHADOW_PRICE: 
    log.warning(" Shadow Price for the SVC instance[%d:%s] exceeded the Limit[%d] with #flows:[%d]",svc_id, svc_mac_addr,sp, svc_num_of_flows)
    pass #sp = disc.MAX_SHADOW_PRICE
  if sp < disc.MIN_SHADOW_PRICE: sp = disc.MIN_SHADOW_PRICE
  disc.update_shadow_price_2(mac_addr=svc_mac_addr,svc_id=svc_id,shadow_price= int(sp))
  #disc.update_shadow_price_2(mac_addr=svc_mac_addr,svc_id=svc_id,shadow_price= 90)
  
  #log.warning("### Flows Add/Remove[%d] at Service[%d:%d] Total Flows[%d] Shadow_Price:[%d]", addition,svc_id,svc_num_of_flows,ttl_num_of_flows,sp)
  
  return
  
  # Update for all the instances of that service.
  for ip,mac,sp in ip_mac_sp_list:
    svc_mac_addr = EthAddr(mac)
    key = (svc_id,svc_mac_addr)
    if (svc_flows_table.has_key(key)):
      svc_num_of_flows = len(svc_flows_table[key])
    else:
      svc_num_of_flows = 0
    #sp = (svc_num_of_flows*100)/float(max(1,ttl_num_of_flows))
    #sp = (sp*svc_num_of_flows)/float(max(MAX_FLOWS_PER_SERVICE, ttl_num_of_flows))
    #sp = (sp*svc_num_of_flows)/float(max(MAX_FLOWS_PER_SERVICE, ttl_num_of_flows)*max(1,total_svc_instances))
    
    #sp = (100*svc_num_of_flows)/float(max(MAX_FLOWS_AGR_SERVICE, ttl_num_of_flows)*max(1,total_svc_instances))
    sp = (100*svc_num_of_flows)/float(max(MAX_FLOWS_AGR_SERVICE, ttl_num_of_flows*total_svc_instances))
    
    #sp = (sp*svc_num_of_flows)/float(fs)
    #log.warning("### Re-adjust Flows Add/Remove[%d] at Service[%d:%d] Total Flows[%d] Shadow_Price:[%d]", addition,svc_id,svc_num_of_flows,ttl_num_of_flows,sp)
    disc.update_shadow_price_2(mac_addr=svc_mac_addr,svc_id=svc_id,shadow_price= int(sp))

  return 

hop_count_dict= {} # {svc_id:[flow_count, total_hop_count_of_all_flows], svc_id2:[]...}
hop_count_dict2={} # {svc_id,origin_sw_id,origin_sw_in_port: [hop_count, dst_sw_id, dst_sw_out_port]}

def rem_hopcount_for_installed_route(svc_id=0, hop_count=0,path=None, redirected=False):
  cur_flows = 0 
  cur_hop_count = 0
  if hop_count_dict.has_key(svc_id):
    cur_flows, cur_hop_count = hop_count_dict[svc_id][0], hop_count_dict[svc_id][1]
    cur_flows-=1
    cur_hop_count-=hop_count
    if cur_flows <=0:
     hop_count_dict.pop(svc_id)
    else:
      hop_count_dict[svc_id] = [cur_flows, cur_hop_count]
  #log.debug("Flow and Hop count Details for svc[%d]: is [%d,%d]", svc_id,hop_count_dict[svc_id][0], hop_count_dict[svc_id][1])
  
  if path is None:
    return
  key = (svc_id,path[0][0].dpid,path[0][1])
  if hop_count_dict2.has_key(key):
    if len(hop_count_dict2[key]) == 1: 
      hop_count_dict2.pop(key)
      #log.warning("Found the Only Item in hopdict2 to remove on flow deletion!")
      pass
    else:
      item = [hop_count,path[-1][0].dpid,path[-1][2]]
      for index, path_entry in enumerate(hop_count_dict2[key]):
        if path_entry == item:
          hop_count_dict2[key].pop(index)
          log.warning("Found multiple items in hopdict2 to remove on flow deletion!")
          break
  return
  
def add_hopcount_for_installed_route(svc_id=0, hop_count=0,path=None, redirected=False):
  cur_flows = 0 
  cur_hop_count = 0
  if hop_count_dict.has_key(svc_id):
    cur_flows, cur_hop_count = hop_count_dict[svc_id][0], hop_count_dict[svc_id][1] 
     
  hop_count_dict[svc_id] = [cur_flows+1, cur_hop_count+hop_count]
  #log.debug("Flow and Hop count Details for svc[%d]: is [%d,%d]", svc_id,hop_count_dict[svc_id][0], hop_count_dict[svc_id][1])
  
  if path is None or svc_id == 0:
    return
  key = (svc_id,path[0][0].dpid,path[0][1])
  if hop_count_dict2.has_key(key):
    #hop_count_dict2[key] = hop_count_dict2[key] + [hop_count,path[-1][0].dpid,path[-1][2]]
    if redirected is False and svc_id != 0:
      #log.warning("Duplicate Flow for the Same Key(%s)!!", key)
      hop_count_dict2[key].append([hop_count,path[-1][0].dpid,path[-1][2]])
    else:
      log.warning("Found Flow with Redirection update for key[%s]", str(key))
      #for i,x in enumerate(hop_count_dict2[key]):
        #if x[1] == 
      hop_count_dict2[key][0] = [hop_count,path[-1][0].dpid,path[-1][2]]
  else:
    hop_count_dict2[key] = [[hop_count,path[-1][0].dpid,path[-1][2]]] # + 
  
def add_install_route(svc_id=0, path=None, match=None, redirected= False):

  if svc_id == 0 or match is None or path is None or len(path) == 0:
    return 

  org_switch          = path[0][0]
  org_switch_in_port  = path[0][1]
  org_switch_dpid     = org_switch.dpid
  org_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=org_switch_dpid, port_id=org_switch_in_port)
  if org_mac_addr is None:
    log.warning("Packet-IN for svc:[%d] at Intermediate switch!! [%s]", svc_id, path)
    return
  
  svc_switch =          path[-1][0]
  svc_switch_out_port = path[-1][2]
  svc_switch_dpid     = svc_switch.dpid
  svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=svc_switch_dpid, port_id=svc_switch_out_port)
  if svc_mac_addr is None:
    log.warning("Invalid Path setup for svc:[%d] at Intermediate switch!! [%s]", svc_id, path)
    return
  
  ret = add_path_to_svc_flows_table(svc_id=svc_id,svc_mac_addr=svc_mac_addr, src_ip=match.nw_src, path=path, match=match)
  #test_redirect(svc_id=svc_id,svc_mac_addr=svc_mac_addr, src_ip=match.nw_src, path=path, match=match)
  
  if ret is True:
    update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=svc_mac_addr,path=path,addition=True)
    add_hopcount_for_installed_route(svc_id=svc_id, hop_count=len(path),path=path, redirected=redirected)
    global dbg_flow_counter
    dbg_flow_counter +=1
  return

def get_mac_addr_from_sw_and_port(sw_dpid=0, port_id=0):
  #find_value = (dpid_to_str(sw_dpid),port_id)
  find_value = (get_sw_from_dpid(sw_dpid),port_id)
  log.debug("Requested for sw_id%d, port_id:%d, find_value:%s", sw_dpid,port_id,find_value)
  for key, value in mac_map.iteritems():
    if value == find_value:
      #log.debug("key:%s, value:%s, find_value:%s, mac_map:[%s]", key,value,find_value, mac_map)
      return key
  log.debug("NO KEY for find_value:%s", find_value)
  return None

def check_and_swap_any_service_instance_for_svc(svc_id=0, cur_max_sp=0, sw_path_list=None):

  svc_dict = disc.get_svc_dict()
  if (svc_dict.has_key(svc_id) is False):
    log.error ('Key[%d] not found! skipp swapping for the requested service',svc_id)
    return False
  
  if (svc_dict.has_key(0) and len(svc_dict[0]) > 0) and len(svc_dict[0][0]):
    log.error ('Alternate Free Instances are still available! skipp swapping for the requested service [%d], %s',svc_id, svc_dict[0])
    return False
  
  def_list = disc.get_default_svc_list()
  
  swap_svc_id = 0
  swap_svc_instace = None
  for key,value in svc_dict.items():
    if key!= 0 and key != svc_id:
      for x in range(len(value)):
        s_sp,s_mac_id,s_ip,s_sw_id = value[x]
        if (key,s_mac_id) in def_list: continue    # Ensure not to swap for svc's created for default case
        if int(s_sp) == 0 or int(s_sp) < disc.MIN_SP_PEAK_THRESHOLD or int(s_sp) == disc.MAX_SP_FOR_BRINGDOWN:
          log.warning("Swap Underutilized service[ %d, at %s] to Over utilized service[%d, %d]", key,value[x],svc_id, cur_max_sp)
          swap_svc_id = key
          swap_svc_instace = value[x]
          break

  if swap_svc_id != 0 and swap_svc_instace is not None:
    mac = swap_svc_instace[1]
    _mac = EthAddr(mac)
    log.warning("Try to bringdown service Service[%d] at mac[%s] with [%s]", svc_id, _mac, swap_svc_instace)
    redirect_all_flows_from_this_instance(svc_id=swap_svc_id, svc_mac_addr=_mac, check_for_flows = False)
    ret = disc.bringdown_service_instace_for_svc(svc_id = swap_svc_id, svc_mac_addr =_mac, skip_wait_period=True)
    if ret is True:
      update_svc_flow_table_on_svc_instance_bringdown(svc_id = swap_svc_id, svc_mac_addr =_mac)
      ret = disc.instantiate_new_instance_for_svc(svc_id = svc_id, cur_max_sp= cur_max_sp,sw_path_list=sw_path_list,new_service=False)
      log.warning("result to Swap and Bring new svc[%d] instance at mac[%s] is [%d]", svc_id, _mac, ret)
      return ret
  else:
    log.warning("Failed to find any under utilized service instance for SWAP to svc[%d]", svc_id)
  return False
  
#Aggregate Flow Statistics Table <routing based on aggregate flow_counts observed per switch>
aggr_flow_dict = {} # {'dpid1':[pkt_count,byte_count,flow_count], 'dpid2':[pkt_count,byte_count,flow_count]}
def get_aggr_flow_stats(sw_id=0):
  global aggr_flow_dict
  if (False == aggr_flow_dict.has_key(sw_id)):
    return 0
  #log.debug("For switch:%d, %s current total of flows: %d", sw_id, aggr_flow_dict[sw_id], aggr_flow_dict[sw_id][2])
  return aggr_flow_dict[sw_id][2]
def handle_aggregate_stats(sw_id=0, aggr_info=None):
  if aggr_info:
    global aggr_flow_dict
    aggr_flow_dict[sw_id] = [aggr_info.packet_count,aggr_info.byte_count,aggr_info.flow_count, time.time()]
    #log.warning("aggr_flow_dict: %s", aggr_flow_dict)
    #log.error("For Switch[%d], Total Flows=[%d]", sw_id, aggr_info.flow_count)
  return
def get_flow_count_for_path(path=None, svc_mac_addr=None, svc_id=0):
  flows_count = 0
  #Approach 1 : For All the Switches in the path: get data from aggr_flow_dict
  #if svc_id == 0 or svc_mac_addr is None and path is not None:
  if path:
    for sw,out_port,in_port in path:
      flows_count+= get_aggr_flow_stats(sw.dpid)
    return flows_count
  #Approach#2: get the data for svc_id from  svc_flow_table.
  if svc_id and svc_mac_addr is not None:
    key = (svc_id,svc_mac_addr)
    if (svc_flows_table.has_key(key)):
      flows_count = len(svc_flows_table[key])
      log.warning("Flow Count for Key[%s] is [%d]", key, flows_count)
  
  log.debug("For Path:%s, current total of flow counts: %d", path, flows_count)
  return flows_count


def _calc_paths ():
  """
  Essentially Floyd-Warshall algorithm
  """

  def dump ():
    for i in sws:
      for j in sws:
        a = path_map[i][j][0]
        #a = adjacency[i][j]
        if a is None: a = "*"
        print a,
      print

  sws = switches.values()
  path_map.clear()
  for k in sws:
    for j,port in adjacency[k].iteritems():
      if port is None: continue
      path_map[k][j] = (1,None)
    path_map[k][k] = (0,None) # distance, intermediate
  
  #dump()

  for k in sws:
    for i in sws:
      for j in sws:
        if path_map[i][k][0] is not None:
          if path_map[k][j][0] is not None:
            # i -> k -> j exists
            ikj_dist = path_map[i][k][0]+path_map[k][j][0]
            if path_map[i][j][0] is None or ikj_dist < path_map[i][j][0]:
              # i -> k -> j is better than existing
              path_map[i][j] = (ikj_dist, k)

  #print "--------------------"
  #dump()


def _get_raw_path (src, dst):
  """
  Get a raw path (just a list of nodes to traverse)
  """
  if len(path_map) == 0: _calc_paths()
  if src is dst:
    # We're here!
    return []
  if path_map[src][dst][0] is None:
    return None
  intermediate = path_map[src][dst][1]
  if intermediate is None:
    # Directly connected
    return []
  return _get_raw_path(src, intermediate) + [intermediate] + \
         _get_raw_path(intermediate, dst)


def _check_path (p):
  """
  Make sure that a path is actually a string of nodes with connected ports

  returns True if path is valid
  """
  for a,b in zip(p[:-1],p[1:]):
    if adjacency[a[0]][b[0]] != a[2]:
      return False
    if adjacency[b[0]][a[0]] != b[1]:
      return False
  return True


def _get_path (src, dst, first_port, final_port):
  """
  Gets a cooked path -- a list of (node,in_port,out_port)
  """
  # Start with a raw path...
  if src == dst:
    path = [src]
  else:
    path = _get_raw_path(src, dst)
    if path is None: return None
    path = [src] + path + [dst]

  # Now add the ports
  r = []
  in_port = first_port
  for s1,s2 in zip(path[:-1],path[1:]):
    out_port = adjacency[s1][s2]
    r.append((s1,in_port,out_port))
    in_port = adjacency[s2][s1]
  r.append((dst,in_port,final_port))

  assert _check_path(r), "Illegal path!"

  return r

def _toBinary(n):
  """
  Converts an integer to binary
  """ 
  return ''.join(str(1 & int(n) >> i) for i in range(64)[::-1])
  #return '{0:12b}'.format(n)

def _get_vlan_id(event):
  pkt_vlan_id = None
  split_vlan_id = None
  vlan_packet = event.parsed.find("vlan")
  if vlan_packet is not None:
    vlan_id_str = None
    pkt_vlan_id = vlan_packet.id
    id = _toBinary(vlan_packet.id)
    vlan_id_str = id[-4:]
    split_vlan_id =  (vlan_packet.id & 15)
    #log.debug("Pkt VLAN_ID:[%d]::ID[%s]::Desired VLAN_ID[%d], Split VLAN_ID:[%d]",pkt_vlan_id,id,int(vlan_id_str,2),split_vlan_id)
  return pkt_vlan_id

def get_svc_list(vlan_id):
  if vlan_id != 0:
    svc_count = 0
    svc_list = []
    while(vlan_id & 0x0F):
      svc = vlan_id & 0x0F
      svc_list.append(svc)
      svc_count+=1
      vlan_id >>= 4
    log.debug("Number of Services:[%d], List of Services:[%s]",svc_count,svc_list) 
    return svc_list
  else:
    pass
  return None

class WaitingPath (object):
  """
  A path which is waiting for its path to be established
  """
  def __init__ (self, path, packet):
    """
    xids is a sequence of (dpid,xid)
    first_switch is the DPID where the packet came from
    packet is something that can be sent in a packet_out
    """
    self.expires_at = time.time() + PATH_SETUP_TIME
    self.path = path
    self.first_switch = path[0][0].dpid
    self.xids = set()
    self.packet = packet

    if len(waiting_paths) > 1000:
      WaitingPath.expire_waiting_paths()

  def add_xid (self, dpid, xid):
    self.xids.add((dpid,xid))
    waiting_paths[(dpid,xid)] = self

  @property
  def is_expired (self):
    return time.time() >= self.expires_at

  def notify (self, event):
    """
    Called when a barrier has been received
    """
    self.xids.discard((event.dpid,event.xid))
    if len(self.xids) == 0:
      # Done!
      if self.packet:
        log.debug("Sending delayed packet out %s"
                  % (dpid_to_str(self.first_switch),))
        msg = of.ofp_packet_out(data=self.packet,
            action=of.ofp_action_output(port=of.OFPP_TABLE))
        core.openflow.sendToDPID(self.first_switch, msg)

      core.l2_multi.raiseEvent(PathInstalled(self.path))


  @staticmethod
  def expire_waiting_paths ():
    packets = set(waiting_paths.values())
    killed = 0
    for p in packets:
      if p.is_expired:
        killed += 1
        for entry in p.xids:
          waiting_paths.pop(entry, None)
    if killed:
      log.error("%i paths failed to install" % (killed,))


class PathInstalled (Event):
  """
  Fired when a path is installed
  """
  def __init__ (self, path):
    Event.__init__(self)
    self.path = path

def get_svc_chain_from_parsed_payload_data(p_data = None):
  if not p_data:
    return None
  of = p_data.find('svclist,')
  ln = len('svclist,')
  of2 =  p_data.find(',end')
  if of2 < of+ln:
    return None
  schain = p_data[of+ln:of2]
  schain = schain.lstrip(',').rstrip(',')
  sch_l  = [ int(i,2) for i in schain.split(',') if len(i) and i.isdigit()]
  return sch_l

def print_icmp_packet_data(event):
  import pox.lib.packet as pkt
  svc_chain = None
  in_pkt= event.parsed
  data = in_pkt.pack()
  #if data:
  #  svc_chain = get_svc_chain_from_parsed_payload_data(p_data = data)
  #  log.warning("Retrieved Service Chain[%s]", svc_chain)
  
  #ipp = event.parsed.find("ipv4")
  #pd2 = ''
  #if ipp:
  #  pd2 = ipp.payload
  #  dt = ipp.pack()
  #  log.warning("IP PKT[%s] and PAYLOAD[%s], Dt[%s]", ipp, pd2, dt)
 
  icp = event.parsed.find("icmp")
  if icp and icp.type == pkt.TYPE_ECHO_REQUEST:
    type = icp.type
    code = icp.code
    payload = icp.payload
    #log.warning("Received the ICMP Echo request Packet with type=[%d], code=[%d], payload=[%s], in_pkt[%s], icp[%s]",type,code,payload,in_pkt, icp)
    seq = payload.seq
    nid = payload.id
    log.warning("NID[%d], SEQ[%d]", nid,seq)
    if data:
      svc_chain = get_svc_chain_from_parsed_payload_data(p_data = data)
      log.warning("Retrieved Service Chain[%s]", svc_chain)
    #dt = icp.pack()
    #log.warning("DT[%s]", dt)
  return svc_chain

def get_penalty_factor(self, out_port_dst=0, hop_count_dst=0, out_port_svc=0, hop_count_svc=0 ):
  penalty_factor = 0.0
  MAX_HOP_COUNT = len(switches)  #10
  # For cases where Destination is at the requested switch or service is at intented switch then ignore penalty (as this might reroute back and add to loop)
  if hop_count_dst <= 1 or hop_count_svc <=1:
    return 0
  if out_port_dst != out_port_svc:
    #penalty_factor = ((hop_count_dst+hop_count_svc)%MAX_HOP_COUNT)/MAX_HOP_COUNT
    if (2*hop_count_svc) >= MAX_HOP_COUNT: # hop_count_dst+hop_count_svc >= MAX_HOP_COUNT:
      penalty_factor = 1
    else:
      #penalty_factor = ((hop_count_dst+hop_count_svc)/float(MAX_HOP_COUNT))
      penalty_factor = ((2*hop_count_svc)/float(MAX_HOP_COUNT))
    
  log.debug("For Dst [port %d,len %d], Trhough Svc [port %d,len %d], Max_Hops %d results in penatly %f", out_port_dst,hop_count_dst,out_port_svc,hop_count_svc,MAX_HOP_COUNT,penalty_factor)
  return penalty_factor

def compute_path_cost(self, hop_count_dst = 1, shadow_price=100, hop_count_svc=0, penalty_factor = 0, Weight = 0):
  """
  Function to return the path cost based on the the hop_count, and shadow price.
  shadow_price    [0-100]       :: Lesser the better
  hop_count       [1-MAX_HOPS]  :: Lesser the better
  penalty_factor  [0 to 1]     :: Lesser the better
  Weight          [TBD]
  Args can be extended to incorporate aggregate b/w, max hops etc to factor for estimation.
  For now simply return the cost = ((hop_count)*100)+(SP)
  Actual cost estimation equation can be deduced later. 
  """
  MAX_HOP_COUNT = len(switches) #10
  if penalty_factor == 0:
    #hop_count =abs(hop_count_dst - hop_count_svc)
    hop_count = hop_count_svc
  else:
    #hop_count_dst + hop_count_svc
    hop_count = 2*hop_count_svc 
    
  #if shadow_price <= 0:
  #  return ((hop_count)*100)
  #return (((hop_count)*100)/(shadow_price))
  return (shadow_price + ((hop_count_svc*100)/MAX_HOP_COUNT))  #<Most working data>
  #return (shadow_price + ((hop_count*100)/MAX_HOP_COUNT))  # Note (This adds the Loops in the path)
#return ((1+penalty_factor)*(shadow_price + ((hop_count*100)/MAX_HOP_COUNT)))   # Note (This adds the Loops in the path)
  #return ((1+penalty_factor)*(shadow_price + ((hop_count_svc*100)/MAX_HOP_COUNT)))   # Note (This adds the Loops in the path)

def get_optimal_path(self, vlan_id, dst_sw, last_port, match, event):
  """
  Function to evaluate and retrieve the best possible path for the intended service
  """
  log.debug(" get_optimal_path::Start Time: %s", time.time())
  svc = (vlan_id & 0xF)
  # Check if the Path is pre-computed and available in the next_svc_flow_table
  p_to_dest = get_path_from_next_svc_flows_table(svc_id=svc,sw_dpid=self.dpid, in_port=event.port, match=match)
  if p_to_dest is not None:
    log.warning("Returning Pre-computed<redirection> path![%s]",p_to_dest)
    return p_to_dest
  
  # If no precomputed path: then continue with the optimal path selection
  p_to_dest = _get_path(self, dst_sw, event.port, last_port)
  if p_to_dest is None:
    log.error("get_optimal_path::Failed to compute path for svc[%d] from [%s:%d] to [%s:%d] path!",svc,self,event.port,dst_sw,last_port)
    return None
  hop_to_dst = len(p_to_dest)
  log.debug("Path from[%s:%d] to Intended Destination[%s:%d] is [%d] hops as:[%s]",self,event.port,dst_sw,last_port,len(p_to_dest),p_to_dest)
  
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc)
  if not ip_mac_sp_list:
    log.warning("No Available Service: Try to Instantiate a new service Instance!")
    #sw_dpid_list = get_sw_dpid_list_from_path(p_to_dest)
    sw_dpid_list = [sw.dpid for sw,in_port,out_port in p_to_dest]
    res = disc.instantiate_new_instance_for_svc(svc_id = svc, sw_path_list=sw_dpid_list, cur_max_sp=disc.MAX_SHADOW_PRICE, new_service=True)
    if res is True:
      ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc)
    
  hop_count_list = []
  best_out_port = None
  best_path = None
  best_instance_sp = disc.MAX_SHADOW_PRICE*10
  if ip_mac_sp_list:
    for ip,mac,sp in ip_mac_sp_list:
      dest_mac = EthAddr(mac)
      if( dest_mac and dest_mac in mac_map):
        dest_addr = mac_map[dest_mac]
        p = _get_path(self, dest_addr[0], event.port, dest_addr[1])
        if p:
          flow_count = get_flow_count_for_path(path=p,svc_mac_addr=dest_mac,svc_id=svc)
          hop_count = len(p)
          out_port =  p[0][2] #for sw,in_port,out_port in p:
          log.info("For svc[%d] at Mac:%s: with SP:%s:: Path is:[%s] with [HopCount:%d, FlowCount:%d] ", svc, mac,sp, p,hop_count,flow_count)
          hop_count_list.append([mac,sp,hop_count,out_port,p,flow_count])
          if int(sp) < best_instance_sp:
            best_instance_sp = int(sp)
    
    log.debug("List of len:%d is:%s", len(hop_count_list), hop_count_list)
    for x in range(0,len(hop_count_list)):
      cur_mac, cur_sp, cur_hc, cur_op, cur_path,cur_fc = hop_count_list[x]
      penalty_factor = get_penalty_factor(self,out_port_dst=p_to_dest[0][2], hop_count_dst=hop_to_dst, out_port_svc=cur_op, hop_count_svc=cur_hc )
      new_cost = compute_path_cost(self,hop_count_dst = hop_to_dst, shadow_price=int(cur_sp),hop_count_svc = cur_hc, penalty_factor=penalty_factor)
      #new_cost = cur_fc
      log.debug("computed cost [%d] via port [%d] path:%s", new_cost,cur_op,cur_path)
      if x == 0:
        best_out_port =  cur_op
        best_path     =  cur_path
        optimal_cost  = new_cost
      else:
        if( optimal_cost > new_cost):
          best_out_port = cur_op
          best_path     =  cur_path
          optimal_cost  = new_cost
    pass
  else:
    pass
  if best_out_port:
    #p = [(self,event.port,best_out_port)]
    #log.info("Send Optimal Path is: %s", p)
    #return (self,event.port,best_out_port)
    log.debug("Computed Optimal Path:%s through out port:%d ",best_path, best_out_port)
    #return best_out_port
    log.debug(" get_optimal_path::End Time: %s", time.time())
  if best_path and best_instance_sp >= disc.MAX_SP_PEAK_THRESHOLD:
    #log.error(" Time to Instantiate New Service Instance for Svc[%d]. best_instance_sp[%d]!",svc, best_instance_sp)
    sw_dpid_list = [sw.dpid for sw,in_port,out_port in best_path]
    res = disc.instantiate_new_instance_for_svc(svc_id = svc, sw_path_list=sw_dpid_list, cur_max_sp=best_instance_sp, new_service=False)
    log.warning("Result of on-Demand Instantiation of service[%d] is [%d]", svc,res)
    if res == -1:
      res = check_and_swap_any_service_instance_for_svc(svc_id=svc, cur_max_sp=best_instance_sp, sw_path_list=sw_dpid_list)
      log.warning("Result of Swap svc [%d] for any other svc Instance is [%d]", svc,res)
    # on success Can switch the path to this new instance
  return best_path

def find_and_install_optimal_path_to_svc(self, vlan_id, dst_sw, last_port, match, event):
  svc = (vlan_id & 0xF)
  best_path = get_optimal_path(self,vlan_id, dst_sw, last_port, match, event)
  if best_path:
    svc_switch =          best_path[-1][0]
    svc_switch_out_port = best_path[-1][2]
    svc_switch_dpid     = svc_switch.dpid
    svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=svc_switch_dpid, port_id=svc_switch_out_port)
    byte_mac_str =  str(svc_mac_addr)
    byte_mac = int(byte_mac_str[byte_mac_str.rfind(':')+1:])
    byte_mac = byte_mac << 4
    vlan_instance_id = ((svc & 0xF) | (byte_mac & 0xFF0)) & 0xFFF
    log.warning(" vlan_id = 0x%x, svc=0x%x byte_mac_str[%s], byte_mac[%d], vlan_instance_id [0x%x]", vlan_id,svc,byte_mac_str,byte_mac,vlan_instance_id)
    best_out_port = best_path[0][2]
    log.debug("From[%s:%d] Service:[%d] has the Best Path:%s",self,event.port,svc,best_path)
    
    # Install Barrier message to avoid packet drop and sequential data processing
    wp = None
    wp_msg = None
    if event and event.ofp:
      wp = WaitingPath(best_path, event.ofp)
    else:
      wp = WaitingPath(best_path, None)
    
    #<Install flow only for this switch and invalidate corresponding flows in other switches to get the packetIn Events??>
    log.warning("At:[%s] Installing Path for Service[%d] from Switch[%s:%d]-->[%s]", time.time(),vlan_id,self,event.port,best_path)
    #add_install_route(svc_id=vlan_id, path=best_path, match=match)
    for sw,in_port,out_port in best_path:
      if sw is None:
        log.error("Incorrect Path! Cannont have None Switch in the path!!")
        return None
      wp_msg = of.ofp_barrier_request()
      msg = of.ofp_flow_mod()
      msg.flags = of.OFPFF_SEND_FLOW_REM
      #msg.match = match
      
      msg.match = of.ofp_match()
      msg.match.dl_vlan = vlan_id #svc
      msg.match.in_port = in_port #event.port #None
      #msg.cookie = 0 
      #msg.priority = 32768
      
      #msg.buffer_id    = None 
      msg.idle_timeout = FLOW_HARD_TIMEOUT #FLOW_IDLE_TIMEOUT #0
      #msg.hard_timeout = FLOW_HARD_TIMEOUT #0
      #log.debug("At:[%s] Installing Rule for Service[%d] on Switch[%s:%d]-->[%d]", time.time(),vlan_id,sw,in_port,out_port)
      #if(self == sw): #if self in best_path:
      if self == sw:
        msg.match.nw_src = match.nw_src #None
        msg.match.nw_dst = None
        msg.match.dl_type = match.dl_type #None # dl_type, dl_src, dl_dst cannot be set to None if nw_src/nw_dst is needed for match
        msg.match.dl_src = match.dl_src #None
        msg.match.dl_dst = match.dl_dst #None
        add_install_route(svc_id=vlan_id, path=best_path, match=msg.match)
        msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = vlan_instance_id))
        if(in_port == out_port):
          msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
          #rvlan_id = of.of.ofp_action_strip_vlan ()
          #cvlan_id = of.ofp_action_vlan_vid (vlan_vid = vlan_instance_id)
          #cout = of.ofp_action_output (port = of.OFPP_IN_PORT)
          #msg.actions = [cvlan_id, cout]
        else:
          msg.actions.append(of.ofp_action_output(port = out_port))
        #log.debug("For Switch [%s]::%s Best Path:%s through output on port:%d", self, str(sw),best_path[i],out_port)
        #break;
      else:
        msg.match.dl_vlan = vlan_instance_id
        msg.match.in_port = None
        msg.match.nw_src = None
        msg.match.nw_dst = None
        msg.match.dl_type = None # dl_type, dl_src, dl_dst cannot be set to None if nw_src/nw_dst is needed for match
        msg.match.dl_src = None
        msg.match.dl_dst = None
        
        #msg.match.nw_src = match.nw_src #None
        #msg.match.nw_dst = None
        #msg.match.dl_type = match.dl_type #None # dl_type, dl_src, dl_dst cannot be set to None if nw_src/nw_dst is needed for match
        #msg.match.dl_src = match.dl_src #None
        #msg.match.dl_dst = match.dl_dst #None
        
        
        #continue
        #msg.actions.append(of.ofp_action_output(port = of.OFPP_CONTROLLER))
        #log.info("For Switch [%s]::Setting up the path out to controller Path:%s", str(sw),best_path[i])
        msg.actions.append(of.ofp_action_output(port = out_port))
        #log.debug("For Switch [%s]::%s Path:%s to output on port:%d", self, str(sw),best_path[i],out_port)
        #msg = of.ofp_flow_mod()
        #msg.command=of.OFPFC_DELETE
        #log.info("For Switch [%s]::Deleting all the flows!", str(sw))
        #sw.connection.send(msg)
      sw.connection.send(msg)
      #core.openflow.sendToDPID(sw.dpid,msg)
      if wp and wp_msg:
        sw.connection.send(wp_msg)
        wp.add_xid(sw.dpid,wp_msg.xid)
        #time.sleep(10)
      #i=i+1
    #add_install_route(svc_id=vlan_id, path=best_path, match=msg.match)
  else:
    log.warning("No Optimal Path found!!, Install any path to Service[%d] or Flood! ", svc)
  return best_path
  

def find_and_install_optimal_path_to_dst(self, vlan_id, dst_sw, last_port, match, event):
  sChain = print_icmp_packet_data(event)
  if not sChain:
    log.warning("Failed to retrieve the Service Chain! Just install path for vlan_id:%d",vlan_id)
    find_and_install_optimal_path_to_svc(self, vlan_id, dst_sw, last_port, match, event)
    return
  
  log.warning("<start> Installing Path End 2 End for SCHAIN[%s]", sChain)
  src_sw = self
  src_port = event.port
  for svc_id in sChain:
    log.warning("Installing path for SVC_ID:[%d] from SW [%d:%d] ",svc_id, src_sw.dpid, event.port )
    installed_path = find_and_install_optimal_path_to_svc(src_sw, svc_id, dst_sw, last_port, match, event)
    log.warning("Installed path for SVC_ID:[%d] from SW [%d:%d] as [%s]",svc_id, src_sw.dpid, event.port, installed_path )
    if not installed_path:
      break
    src_sw = installed_path[-1][0]
    event.port = installed_path[-1][2]

  event.port = src_port
  return
    
  return 
class Switch (EventMixin):
  def __init__ (self):
    self.connection = None
    self.ports = None
    self.dpid = None
    self._listeners = None
    self._connected_at = None

  def __repr__ (self):
    return dpid_to_str(self.dpid)

  def _install (self, switch, in_port, out_port, match, buf = None):
    msg = of.ofp_flow_mod()
    msg.match = match
    msg.match.in_port = in_port
    msg.idle_timeout = FLOW_IDLE_TIMEOUT
    #msg.hard_timeout = FLOW_HARD_TIMEOUT
    msg.actions.append(of.ofp_action_output(port = out_port))
    msg.buffer_id = buf
    #msg.flags=of.OFPFF_SEND_FLOW_REM
    switch.connection.send(msg)

  def _install_path (self, p, match, packet_in=None):
    wp = WaitingPath(p, packet_in)
    #log.info ("Installing Path for switch[%s] as [%s]", self,p)
    for sw,in_port,out_port in p:
      self._install(sw, in_port, out_port, match)
      msg = of.ofp_barrier_request()
      sw.connection.send(msg)
      wp.add_xid(sw.dpid,msg.xid)

  def install_path (self, dst_sw, last_port, match, event):
    """
    Attempts to install a path between this switch and some destination
    """
    import pox.lib.packet as pkt
    
    p = _get_path(self, dst_sw, event.port, last_port)
    if p is None:
      log.error("Can't get from %s to %s", match.dl_src, match.dl_dst)

      if (match.dl_type == pkt.ethernet.IP_TYPE and
          event.parsed.find('ipv4')):
        # It's IP -- let's send a destination unreachable
        log.debug("Dest unreachable (%s -> %s)",
                  match.dl_src, match.dl_dst)

        from pox.lib.addresses import EthAddr
        e = pkt.ethernet()
        e.src = EthAddr(dpid_to_str(self.dpid)) #FIXME: Hmm...
        e.dst = match.dl_src
        e.type = e.IP_TYPE
        ipp = pkt.ipv4()
        ipp.protocol = ipp.ICMP_PROTOCOL
        ipp.srcip = match.nw_dst #FIXME: Ridiculous
        ipp.dstip = match.nw_src
        icmp = pkt.icmp()
        icmp.type = pkt.ICMP.TYPE_DEST_UNREACH
        icmp.code = pkt.ICMP.CODE_UNREACH_HOST
        orig_ip = event.parsed.find('ipv4')

        d = orig_ip.pack()
        d = d[:orig_ip.hl * 4 + 8]
        import struct
        d = struct.pack("!HH", 0,0) + d #FIXME: MTU
        icmp.payload = d
        ipp.payload = icmp
        e.payload = ipp
        msg = of.ofp_packet_out()
        msg.actions.append(of.ofp_action_output(port = event.port))
        msg.data = e.pack()
        self.connection.send(msg)

      return

    log.info("Installing path for %s -> %s %04x (%i hops)",
        match.dl_src, match.dl_dst, match.dl_type, len(p))
    
    #vlan_id = _get_vlan_id(event)
    #if vlan_id is not None and vlan_id == 0:
      #Account for Echo Request Packets Only
       #icp = event.parsed.find("icmp")
       #if icp and icp.type == pkt.TYPE_ECHO_REQUEST:
        #add_hopcount_for_installed_route(svc_id=0, hop_count=len(p),path=p, redirected=False)
        #log.warning("Added the HopCount for SVC[0] of len[%d] for path[%s]", len(p),p)
        #pass
    # We have a path -- install it
    self._install_path(p, match, event.ofp)

    # Now reverse it and install it backwards
    # (we'll just assume that will work)
    p = [(sw,out_port,in_port) for sw,in_port,out_port in p]
    self._install_path(p, match.flip())

  
  def install_path_sk (self, vlan_id, dst_sw, last_port, match, event):
    """
    Check and Alter the Path to orient towards 'svc' with Best Shadow Price;
    """
    log.debug(" install_path_sk::Start Time: %s", time.time())
    svc = (vlan_id & 0xF)
    #log.info("install_path_sk for switch[%d:%d] svc:%d, Start Time: %s",self.dpid,event.port,svc,time.time())
    #if vlan_id != svc or False == core.openflow_discovery.is_edge_port(self.dpid,event.port):
    if False == core.openflow_discovery.is_edge_port(self.dpid,event.port):
      log.error("Received packet_in with vlan[%d] for svc[%d] at intermediate switch [%d:%d]", vlan_id,svc,event.connection.dpid,event.port )
      #Silently discard this packet <TRY and CHECK>
      return
    #find_and_install_optimal_path_to_svc(self, vlan_id, dst_sw, last_port, match, event)
    find_and_install_optimal_path_to_dst(self, vlan_id, dst_sw, last_port, match, event)
    log.debug(" install_path_sk::End Time: %s", time.time())
    return

  def _handle_PacketIn (self, event):
    
    def drop ():
      # Kill the buffer
      if event.ofp.buffer_id is not None:
        msg = of.ofp_packet_out()
        msg.buffer_id = event.ofp.buffer_id
        event.ofp.buffer_id = None # Mark is dead
        msg.in_port = event.port
        self.connection.send(msg)
    
    def flood ():
      """ Floods the packet """
      if self.is_holding_down:
        #log.warning("Not flooding -- holddown active")
        log.info("Not flooding -- holddown active")
        return
        #return drop()
      #return drop()
      msg = of.ofp_packet_out()
      # OFPP_FLOOD is optional; some switches may need OFPP_ALL
      msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
      msg.buffer_id = event.ofp.buffer_id
      msg.in_port = event.port
      self.connection.send(msg)
    
    packet = event.parsed

    loc = (self, event.port) # Place we saw this ethaddr
    oldloc = mac_map.get(packet.src) # Place we last saw this ethaddr
    #log.info("loc:%s, oldloc:%s", loc, oldloc)
    #log.warning("pcket.src:%s, oldloc:%s, ", packet.src, oldloc)
    

    if packet.effective_ethertype == packet.LLDP_TYPE:
      drop()
      return
    
    vlan_id = _get_vlan_id(event)
    
    def setup_path_sk():
      if packet.dst.is_multicast:
        log.warning("Flood!! setup_path_sk(svc=%d)::Dst[%s] is multicast!", vlan_id, packet.dst)
        return flood()
      if packet.dst not in mac_map:
        log.warning("Flood!! setup_path_sk(svc=%d)::Dst[%s] is not found in mac_map", vlan_id, packet.dst)
        return flood()
      
      dest = mac_map[packet.dst]
      match = of.ofp_match.from_packet(packet)
      self.install_path_sk(vlan_id, dest[0], dest[1], match, event)
      return

    if vlan_id is not None and vlan_id != 0 and vlan_id != 65535:
      return setup_path_sk()
 

    if oldloc is None:
      if packet.src.is_multicast == False and packet.src != EthAddr("00:00:00:00:00:00"):
        mac_map[packet.src] = loc # Learn position for ethaddr
        #log.debug("Learned %s at %s.%i", packet.src, loc[0], loc[1])
        log.info("Learned SRC %s at %s.%i", packet.src, loc[0], loc[1])
    elif oldloc != loc:
      # ethaddr seen at different place!
      if core.openflow_discovery.is_edge_port(loc[0].dpid, loc[1]):
        # New place is another "plain" port (probably)
        log.warning("%s moved from %s.%i to %s.%i?", packet.src,
                  dpid_to_str(oldloc[0].dpid), oldloc[1],
                  dpid_to_str(   loc[0].dpid),    loc[1])
        if packet.src.is_multicast == False and packet.src != EthAddr("00:00:00:00:00:00"):
          mac_map[packet.src] = loc # Learn position for ethaddr
          log.info("Learned SRC2 %s at %s.%i", packet.src, loc[0], loc[1])
      elif packet.dst.is_multicast == False:
        # New place is a switch-to-switch port!
        # Hopefully, this is a packet we're flooding because we didn't
        # know the destination, and not because it's somehow not on a
        # path that we expect it to be on.
        # If spanning_tree is running, we might check that this port is
        # on the spanning tree (it should be).
        if packet.dst in mac_map:
          # Unfortunately, we know the destination.  It's possible that
          # we learned it while it was in flight, but it's also possible
          # that something has gone wrong.
          log.info("Packet from %s to known destination %s arrived "
                      "at %s.%i without flow", packet.src, packet.dst,
                      dpid_to_str(self.dpid), event.port)


    if packet.dst.is_multicast:
      log.debug("Flood DST multicast %s from %s", packet.dst, packet.src)
      flood()
    else:
      if packet.dst not in mac_map:
        log.info("%s unknown -- flooding" % (packet.dst,))
        flood()
      else:
        dest = mac_map[packet.dst]
        match = of.ofp_match.from_packet(packet)
        self.install_path(dest[0], dest[1], match, event)

  def disconnect (self):
    if self.connection is not None:
      log.error("Disconnect %s" % (self.connection,))
      self.connection.removeListeners(self._listeners)
      self.connection = None
      self._listeners = None

  def connect (self, connection):
    if self.dpid is None:
      self.dpid = connection.dpid
    assert self.dpid == connection.dpid
    if self.ports is None:
      self.ports = connection.features.ports
    self.disconnect()
    log.debug("Connect %s" % (connection,))
    self.connection = connection
    self._listeners = self.listenTo(connection)
    self._connected_at = time.time()

  @property
  def is_holding_down (self):
    if self._connected_at is None: return True
    if time.time() - self._connected_at > FLOOD_HOLDDOWN:
      return False
    return True

  def _handle_ConnectionDown (self, event):
    self.disconnect()

def get_sw_from_dpid(dpid=0):
  global switches
  sw = switches.get(dpid)
  if sw is None:
    log.warning("DPID:%d has no switch info in the Dictionary[%s]!!", dpid, switches)
    pass
  return sw

def get_mac_map(host_eth_addr):
  if host_eth_addr in mac_map:
    return mac_map[host_eth_addr]
  return None

class l2_multi (EventMixin):

  _eventMixin_events = set([
    PathInstalled,
  ])

  def __init__ (self):
    # Listen to dependencies
    def startup ():
      core.openflow.addListeners(self, priority=0)
      core.openflow_discovery.addListeners(self)
    core.call_when_ready(startup, ('openflow','openflow_discovery'))

  def _handle_LinkEvent (self, event):
    def flip (link):
      return Discovery.Link(link[2],link[3], link[0],link[1])

    l = event.link
    sw1 = switches[l.dpid1]
    sw2 = switches[l.dpid2]

    # Invalidate all flows and path info.
    # For link adds, this makes sure that if a new link leads to an
    # improved path, we use it.
    # For link removals, this makes sure that we don't use a
    # path that may have been broken.
    #NOTE: This could be radically improved! (e.g., not *ALL* paths break)
    clear = of.ofp_flow_mod(command=of.OFPFC_DELETE)
    for sw in switches.itervalues():
      if sw.connection is None: continue
      #log.debug("Flows Cleared for Switch %s!!", sw.dpid)
      log.info("Flows Cleared for Switch %s!!", sw.dpid)
      sw.connection.send(clear)
    path_map.clear()

    if event.removed:
      # This link no longer okay
      if sw2 in adjacency[sw1]: del adjacency[sw1][sw2]
      if sw1 in adjacency[sw2]: del adjacency[sw2][sw1]

      # But maybe there's another way to connect these...
      for ll in core.openflow_discovery.adjacency:
        if ll.dpid1 == l.dpid1 and ll.dpid2 == l.dpid2:
          if flip(ll) in core.openflow_discovery.adjacency:
            # Yup, link goes both ways
            adjacency[sw1][sw2] = ll.port1
            adjacency[sw2][sw1] = ll.port2
            # Fixed -- new link chosen to connect these
            break
    else:
      # If we already consider these nodes connected, we can
      # ignore this link up.
      # Otherwise, we might be interested...
      if adjacency[sw1][sw2] is None:
        # These previously weren't connected.  If the link
        # exists in both directions, we consider them connected now.
        if flip(l) in core.openflow_discovery.adjacency:
          # Yup, link goes both ways -- connected!
          adjacency[sw1][sw2] = l.port1
          adjacency[sw2][sw1] = l.port2

      # If we have learned a MAC on this port which we now know to
      # be connected to a switch, unlearn it.
      bad_macs = set()
      for mac,(sw,port) in mac_map.iteritems():
        if sw is sw1 and port == l.port1: bad_macs.add(mac)
        if sw is sw2 and port == l.port2: bad_macs.add(mac)
      for mac in bad_macs:
        log.debug("Unlearned %s", mac)
        del mac_map[mac]

  def _handle_ConnectionUp (self, event):
    sw = switches.get(event.dpid)
    if sw is None:
      # New switch
      sw = Switch()
      switches[event.dpid] = sw
      sw.connect(event.connection)
    else:
      sw.connect(event.connection)

  def _handle_BarrierIn (self, event):
    wp = waiting_paths.pop((event.dpid,event.xid), None)
    if not wp:
      #log.info("No waiting packet %s,%s", event.dpid, event.xid)
      return
    #log.debug("Notify waiting packet %s,%s", event.dpid, event.xid)
    wp.notify(event)

hops_file = "hopcount_log.csv"
hops_file2 = "hopcount2_log.csv"
def log_flows_count(cur_time=0):
  
  global dbg_flow_counter
  global svc_flows_table
  global next_svc_flows_table
  active_flow_count = sum(map(len, svc_flows_table.itervalues())) # sum(len(v), for v in svc_flows_table.itervalues()))
  
  global hop_count_dict
  data = ""
  avg_count = 0
  overall_avg = 0
  items_count=0
  if cur_time == 0: cur_time = time.time()
  for key,value in hop_count_dict.iteritems():
    avg_count = value[1]/float(value[0])
    overall_avg += avg_count
    items_count += 1
    data += str(key) + ':' + str(avg_count) + '-' + str(value[0]) + '-' + str(value[1]) + ','
  
  overall_avg /=float(max(1,items_count))
  data.rstrip(',')
  with open(hops_file, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    data = data.rstrip(',')
    logWriter.writerows([[int(cur_time), str(overall_avg), str(data), str(active_flow_count), str(dbg_flow_counter) ]])
  #return
  
  global hop_count_dict2
  data = ""
  total_flows = 0
  total_hop_count= 0
  overall_avg = 0
  if cur_time == 0: cur_time = time.time()
  for key,vals in hop_count_dict2.iteritems():
    svc_id,src_sw_dpid,src_sw_port = key[0],key[1],key[2]
    for val in vals:
      hop_ct,dst_sw_dpid,dst_sw_port = val[0],val[1],val[2]
      #data += str(key) + '-' + str(val[1]) + '-' + str(val[2]) + ':' + str(val[0]) + ','
      #k1 = (src_sw_dpid,src_sw_port,dst_sw_dpid,dst_sw_port)
      #data += '{0:3d}'.format(svc_id) + '-' + '{0:3d}'.format(src_sw_dpid) + '-' + '{0:3d}'.format(src_sw_port) + '-' + '{0:3d}'.format(dst_sw_dpid) + '-' + '{0:3d}'.format(dst_sw_port) + ':' + str(hop_ct) + ','
      
      total_flows +=1
      total_hop_count += hop_ct
      data += str(svc_id) + '@' + str(src_sw_dpid) + ':' + str(src_sw_port) + '-' + str(dst_sw_dpid) + ':' + str(dst_sw_port) + '=' + str(hop_ct) + ','
  
  #hop_count_dict2.clear()
  overall_avg = total_hop_count/float(max(1,total_flows))
  data.rstrip(',')
  
  with open(hops_file2, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    data = data.rstrip(',')
    #logWriter.writerows([[int(cur_time), str(data), str(overall_avg) ]])
    logWriter.writerows([[int(cur_time), str(total_flows), str(total_hop_count), str(overall_avg), str(data)]])
  return
  
  #global dbg_flow_counter
  #global svc_flows_table
  #global next_svc_flows_table
  
  active_flow_count = 0
  next_flow_count = 0
  active_flow_count = sum(map(len, svc_flows_table.itervalues())) # sum(len(v), for v in svc_flows_table.itervalues()))
  next_flow_count   = sum(map(len, next_svc_flows_table.itervalues()))
  log.warning("At[%s]: dbg_flow_counter[%d] active_flow_count[%d], next_flow_count[%d]",cur_time, dbg_flow_counter, active_flow_count,next_flow_count)
  return
  
fupdate_interval = 5 #2 #5
flog_file = "flog.csv"
rdir_file = "rdir_log.csv"
def _svc_flog_function():
  global svc_flows_table
  #global next_svc_flows_table
  
  if not svc_flows_table:
    #return
    # Utlize this to reset the SP here to Zero: 
    svc_dir = core.openflow_discovery.getFunctionList_sk() # disc.get_svc_dict()
    for svc_id, value in svc_dir.iteritems():
      #log.warning("Svcid: [%d],m value[%s]", svc_id,value)
      for instance in value:
        if int(instance[0]) > 0 and int(instance[0]) < disc.MAX_SHADOW_PRICE:
          log.warning("_svc_flog_function(): Explicitly Setting SP to Zero for svc[%d] at [%s]", svc_id, instance[1])
          #disc.update_shadow_price_2(mac_addr=instance[1],svc_id=svc_id,shadow_price= 0)
          disc.update_shadow_price(mac_addr=instance[1],svc_id=svc_id,shadow_price= -20)
          pass
    return
  o_svc_flows_table = OrderedDict(sorted(svc_flows_table.items(), key=lambda t: t[0]))
  #o_next_svc_flows_table = OrderedDict(sorted(next_svc_flows_table.items(), key=lambda t: t[0]))
  
  log_file_func = flog_file
  keys_list = o_svc_flows_table.keys()
  keys_list.sort(key=lambda t: t[0])
  
  #log_file_func = flog_file.split('.')[0] + '_' + str(key) +'.'+flog_file.split('.')[1]
  #with open(log_file_func, 'ab') as csvfile:
  #  logWriter = csv.writer(csvfile,delimiter=',')
  #  for key in keys_list:
  #    data = len(o_svc_flows_table[key])
  #    logWriter.writerows([[int(time.time()), str(key), str(data)]])
  
  #with open(log_file_func, 'ab') as csvfile:
  #  logWriter = csv.writer(csvfile,delimiter=',')
  #  data = ""
  #  for key in keys_list:
  #    data += str(key) + str('-') + str(len(o_svc_flows_table[key])) + str(',')
  #  logWriter.writerows([[int(time.time()), str(data)]])
  
  c_svc_id = -1
  log_file_func = ""
  data = ""
  cur_time = time.time()
  for key in keys_list:
    svc_id, mac_id = key[0],key[1]
    #if c_svc_id == -1: c_svc_id = svc_id
    if c_svc_id != svc_id:
      if log_file_func and data:
        with open(log_file_func, 'ab') as csvfile:
          logWriter = csv.writer(csvfile,delimiter=',')
          data = data.rstrip(',')
          logWriter.writerows([[int(cur_time), str(c_svc_id),str(data)]])
      log_file_func = flog_file.split('.')[0] + '_' + str(svc_id) +'.'+flog_file.split('.')[1]
      data= ""
    c_svc_id = svc_id
    data += str(mac_id) + str('-') + str(len(o_svc_flows_table[key])) + str(',')
  
  # The Last Record
  if log_file_func and data:
    log_file_func = flog_file.split('.')[0] + '_' + str(svc_id) +'.'+flog_file.split('.')[1]
    with open(log_file_func, 'ab') as csvfile:
      logWriter = csv.writer(csvfile,delimiter=',')
      data = data.rstrip(',')
      logWriter.writerows([[int(cur_time), str(svc_id), str(data)]])
  
  #Also log the SP of each service
  disc._svc_log_function(cur_time)
  
  log_flows_count(cur_time)
  
  #with open(log_file_func, 'ab') as csvfile:
  #  logWriter = csv.writer(csvfile,delimiter=',')
  #  data = ""
  #  
  #    data += str(key) + str('-') + str(len(o_svc_flows_table[key])) + str(',')
  #  logWriter.writerows([[int(time.time()), str(data)]])
  
  data = ""
  for key,value in redirection_log.items():
    data += str(key) + '-' + str(value) + ','
  #if data is None or len(data) == 0:
  #  return
  data = data.rstrip(',')
  with open(rdir_file, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    data = data.rstrip(',')
    logWriter.writerows([[int(cur_time), str(data)]])
  return
  
def launch ():
  core.registerNew(l2_multi)

  timeout = min(max(PATH_SETUP_TIME, 5) * 2, 15)
  Timer(timeout, WaitingPath.expire_waiting_paths, recurring=True)
  Timer(fupdate_interval, _svc_flog_function, recurring=True)
