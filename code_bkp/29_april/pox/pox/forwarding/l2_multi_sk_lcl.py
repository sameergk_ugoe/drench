# Copyright 2012-2013 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
A shortest-path forwarding application.

This is a standalone L2 switch that learns ethernet addresses
across the entire network and picks short paths between them.

You shouldn't really write an application this way -- you should
keep more state in the controller (that is, your flow tables),
and/or you should make your topology more static.  However, this
does (mostly) work. :)

Depends on openflow.discovery
Works with openflow.spanning_tree
"""
#changes for Local:
# 1. No Timer thread
# 2. Rule setup changes 
#   a. intermediate Mbox => Change SRC_ETHER=?? and SRC PORT=SRC_PORT+1
#   b. End Host => Drop
# 
from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.recoco import Timer
from collections import defaultdict, OrderedDict
from pox.openflow.discovery_sk_lcl import Discovery
from pox.lib.util import dpid_to_str
import time
import os
from pox.lib.packet.vlan import vlan
from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.tcp import tcp
from pox.lib.addresses import EthAddr, IPAddr
import pox.lib.packet as pkt
import pox.openflow.discovery_sk_lcl as disc
import pox.openflow.util_sk as util_sk
import pox.openflow.nwroutehelper_sk as nwrt_sk
import pox.forwarding.flow_mapper_sk as fmap_sk
#import pox.samples.flow_stats_sk as fstat
from copy import deepcopy
import csv
import sys
log = core.getLogger()
from os.path import expanduser
hdir_p = expanduser('~')
base_dir = os.path.join(hdir_p, 'drench')
rdir_p = os.path.join(base_dir, 'results')
hops_file = os.path.join(rdir_p, "hopcount_log.csv")
hops_file2 = os.path.join(rdir_p,"hopcount2_log.csv")
rdir_file = os.path.join(rdir_p,"rdir_log.csv")
related_flow_info_file = os.path.join(rdir_p,"related_flows_info.csv")
flog_file = os.path.join(rdir_p, "flog.csv")
dbg_flow_counter = 0
# Adjacency map.  [sw1][sw2] -> port from sw1 to sw2
adjacency = defaultdict(lambda:defaultdict(lambda:None))

# Switches we know of.  [dpid] -> Switch
switches = {}

# ethaddr -> (switch, port), e.g 00:00:00:00:00:01 at (00-00-00-00-00-01, 1)
mac_map = {}

# [sw1][sw2] -> (distance, intermediate)
path_map = defaultdict(lambda:defaultdict(lambda:(None,None)))

# Waiting path.  (dpid,xid)->WaitingPath
waiting_paths = {}

# Time to not flood in seconds
FLOOD_HOLDDOWN = 5

# Flow timeouts
FLOW_IDLE_TIMEOUT = 10 #100 #10
FLOW_HARD_TIMEOUT = 30 #300 #30
FLOW_MINIMAL_TIMEOUT = 5

# How long is allowable to set up a path?
PATH_SETUP_TIME = 10

# Table of Flows Installed for Services
svc_flows_table = {}        #Key = (svc,mac_addr_svc):[ [src_ip1, path1, match1], [src_ip2, path2, match2], ...]

# Table containing the current placement information for all services: to compute optimal placement for a service periodically
new_placement_table = {} #[svc_id:[path_cost, mac_addr],]

def get_all_connected_switches():
    global switches
    return  switches
def clear_flow_for_svc_on_path(svc_id=0, svc_mac_addr= None, path=None, match=None, src_ip=None):
  
  if path is None or match is None:
    log.warning("clear_flow_for_svc_on_path(No Path Specififed!!)")
    return False
  
  command = of.OFPFC_DELETE_STRICT # of.OFPFC_DELETE
  msg = of.ofp_flow_mod(match=match,command=command)
  for sw,in_port,out_port in path:
    if sw and sw.connection:
      #log.warning("Clearing Flows on Switch:[%s] with match:[%s]", sw, match)
      sw.connection.send(msg)
      break
    
  
  #Explicitly delete the Flow from the Flow Info Table
  fmap_sk.del_flow_from_flow_info_table(match)
  
  #Commented here coz, it needs to be done after svc_flow_table_update in the caller
  update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=svc_mac_addr,path=path,addition=False)
  
  rem_hopcount_for_installed_route(svc_id=svc_id, hop_count=len(path),path=path, redirected=True)
  
  global dbg_flow_counter
  dbg_flow_counter -=1
  
  return True

def update_svc_flow_table_on_svc_instance_bringdown(svc_id=0,svc_mac_addr=""):
  if svc_id is 0 or svc_mac_addr is None:
    log.warning("Invalid Service instance!! ")
    return False
  fmap_sk.update_flow_map_on_svc_instance_removal(svc_id,svc_mac_addr)
  return True
def get_svc_id_from_flow_rem_event(event):
    svc_id = 0
    match = event.ofp.match
    if match.nw_proto and (match.nw_proto == ipv4.UDP_PROTOCOL or match.nw_proto == ipv4.TCP_PROTOCOL):
        svc_id = event.ofp.cookie & 0x0F
    elif match.dl_vlan is None or match.dl_vlan == 0 or match.dl_vlan == 65535:
        log.debug("Skipped processing Deleted rule: No Matching DL_VLAN")
    else:
        svc_id = match.dl_vlan
    return svc_id
    
def update_svc_flow_table_on_flow_removal_event(sw_dpid=0, event=None):
  
  if event is None or event.ofp is None or event.ofp.match is None:
    return
    
  ## Handle only the Timeout Rules,  Force Deleted will be handled internally <otherwise results in unnecessary re-updates of SP (making it fluctuate )>
  #if event.ofp.reason == of.OFPRR_DELETE: # of.OFPRR_HARD_TIMEOUT or event.ofp.reason == of.OFPRR_IDLE_TIMEOUT:
  #  log.warning("Skipped processing the Explicitly Deleted rule on sw_dpid[%d]",sw_dpid)
  #  return
  
  # Handle only the Rules on Edge port Switches (SRC/Ingress Switch)
  if event.ofp.match.in_port is None:
    log.debug("Intermediate or Egress switch flows got removed. Ignore!!")
    return
  
  #if core.openflow_discovery.is_edge_port(sw_dpid, event.ofp.match.in_port) is False:
  #  log.debug("Skipped processing Deleted rule on Non Edge Port!")
  #  return
  
  svc_id = get_svc_id_from_flow_rem_event(event)
  #log.warning("Retrieved svc_id from cookie [%s] at sw:port[%s:%s]", svc_id, sw_dpid,event.ofp.match.in_port)
  match = event.ofp.match
  
  #src_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=sw_dpid, port_id=event.ofp.match.in_port)
  flow_key = fmap_sk.get_flow_key_from_match(match)
  path = fmap_sk.get_path_of_flow_from_flow_info_table(flow_key)
  svc_mac_addr = None
  if path is None:
    log.warning("Could not retrieve path for the removed flow at sw[%d]< perhaps due to redirection case> ",sw_dpid)
    svc_mac_addr = fmap_sk.get_svc_mac_addr_of_flow_from_flow_info_table(flow_key)
    pass
  else:
    svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=path[-1][0].dpid, port_id=path[-1][2])
    
  if svc_mac_addr is None: return
  
  #***REMEMBER*** Delete flow before updating the ShadowPrice **** < may need to invalidate this call in the caller function >
  fmap_sk.del_flow_from_flow_info_table(match)
  
  #Update the Shadow Price and Hop Count Information!
  update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=svc_mac_addr,path=path,addition=False)
  rem_hopcount_for_installed_route(svc_id=svc_id, hop_count=len(path),path=path, redirected=False)
  
  global dbg_flow_counter
  dbg_flow_counter -=1
  
  return

def get_new_path_to_service(svc_id=0,svc_mac_addr=None,src_ip=None, cur_path=None, cur_match=None):
  if svc_mac_addr is None or cur_path is None:
    return None,cur_match
    
  src_sw = cur_path[0][0]
  src_port = cur_path[0][1]
  dest_addr = mac_map[svc_mac_addr]
  new_path = _get_path(src_sw, dest_addr[0], src_port, dest_addr[1])
  log.info(" Computed new path for SVC[%d] for scr_ip[%s] From[%s:%d] to svc_mac_addr[%s] as [%s]",svc_id, src_ip, new_path[0][0], new_path[0][1], svc_mac_addr, new_path)
  return new_path,cur_match
  #new_match = deepcopy(cur_match)
  #new_match.dl_dst = svc_mac_addr # EthAddr(svc_mac_addr)
  #return new_path,new_match

def proactive_install_path_for_svc_on_path(svc_id=0,svc_mac_addr=None,path=None,match=None,src_ip=None,p_to_dest=None):

  proto = ipv4.UDP_PROTOCOL
  if match and match.nw_proto:
    proto = match.nw_proto
  return install_path_to_svc(svc=svc_id, best_path=path, match=match, packet=None, path_cost=10, proto=proto, ttl=0,p_to_dest=p_to_dest)
  return False
  
redirection_log = {} # {'svc_id':[redir_count, src_switch1, from_svc_mac1, to_svc_mac1,  src_switch2, from_svc_mac2, to_svc_mac2,]
def add_to_redirection_log(svc_id=0, orig_sw_dpid=None, old_svc_mac=None, new_mac_addr=None):
  if (False == redirection_log.has_key(svc_id)):
    redirection_log[svc_id] = [[orig_sw_dpid, old_svc_mac, new_mac_addr]]
    #redirection_log[svc_id] = [1], orig_sw_dpid, old_svc_mac, new_mac_addr]
  else:
    redirection_log[svc_id].append([orig_sw_dpid, old_svc_mac, new_mac_addr])
    #redirection_log[svc_id] = [redirection_log[svc_id][0]+1] + redirection_log[svc_id][1:] + [orig_sw_dpid, old_svc_mac, new_mac_addr]
    
    # For safety: check if the occurance is already in the log
    #for i in range (1, len(redirection_log[svc_id])-2):
    #  if orig_sw_dpid == redirection_log[svc_id][i] and  old_svc_mac == redirection_log[svc_id][i+1] and new_mac_addr== redirection_log[svc_id][i+2]:
    #    log.warning("Found Redirection for the same path to same DST performed again!!")
    #    break
    pass
  return

def redirect_path_for_svc(svc_id=0,cur_svc_mac_addr=None,next_svc_mac_addr=None, for_src_ip=None):
  
  if svc_id is None or cur_svc_mac_addr is None or next_svc_mac_addr is None or for_src_ip is None:
    log.warning(" Incorrect data! Skipped to redirect flows!")
    return
  
  #log.warning("Ignore Redirection of svc [%d] from cur_svc_mac_addr [%s] to next_svc_mac_addr [%s] for flow src[%s]",svc_id,cur_svc_mac_addr,next_svc_mac_addr,for_src_ip)
  #return
  
  flows_at_svc_dict = fmap_sk.get_all_flows_for_svc_id_at_svc_mac(svc_id,cur_svc_mac_addr)
  if not flows_at_svc_dict:
    log.error("<Redirect Path for Service> No Flows for svc at [%d:%s]!", svc_id,cur_svc_mac_addr)
    return
  
  log.warning("ReDirecting path for Service[%d] for IP[%s] from [%s] to [%s]",svc_id,for_src_ip, cur_svc_mac_addr,next_svc_mac_addr)
  ret = False
  for k,v in flows_at_svc_dict.items():
    new_path = None
    mac,ip = fmap_sk.get_src_mac_and_src_ip_from_flow_key(k) #flow[0]
    if for_src_ip == ip:
      path  = fmap_sk.get_path_of_flow_from_flow_info_table(k) #flow[1]
      match = fmap_sk.get_match_of_flow_from_flow_info_table(k)#flow[2]
      p_to_dest = fmap_sk.get_dstination_path_of_flow_from_flow_info_table(k)
      new_path,new_match = get_new_path_to_service(svc_id=svc_id,svc_mac_addr=next_svc_mac_addr,src_ip=for_src_ip, cur_path=path, cur_match=match)
      if not new_path: continue
      clear_flow_for_svc_on_path(svc_id=svc_id, svc_mac_addr=cur_svc_mac_addr, path=path, match=match, src_ip=for_src_ip)
      proactive_install_path_for_svc_on_path(svc_id=svc_id,svc_mac_addr=next_svc_mac_addr,path=new_path,match=new_match,src_ip=for_src_ip,p_to_dest=p_to_dest)
      add_to_redirection_log(svc_id=svc_id,orig_sw_dpid=path[0][0].dpid, old_svc_mac=cur_svc_mac_addr, new_mac_addr=next_svc_mac_addr)
      ret = True
      break
  
  if ret is True:
    ## Remove this particular key flow entry from table:
    ##fmap_sk.del_flow_from_flow_info_table_by_key(k)
    ## Also Update SP after flow redirection from the cur_svc_mac 
    #update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=cur_svc_mac_addr,path=path,addition=False)
    pass
  return

test_count = 0
def test_redirect(svc_id=0,svc_mac_addr=None, src_ip=None, path=None, match=None):
  global test_count
  if test_count or svc_id != 13:
    return
    
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  mac_addr = None
  for ip,mac,sp in ip_mac_sp_list:
    _mac = EthAddr(mac)
    log.info("svc_mac_addr[%s], mac[%s], _mac[%s]",svc_mac_addr,mac,_mac )
    if _mac != svc_mac_addr:
      mac_addr = _mac
      log.info ("Testing to redirect flows for SVC[%d] from current MAC[%s] to new MAC[%s] ", svc_id,svc_mac_addr,mac_addr)
      break
  if mac_addr:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=svc_mac_addr,next_svc_mac_addr=mac_addr, for_src_ip=src_ip)
  else:
    log.warning("Failed to find alternate MAC for service[%d]", svc_id)
  
  test_count+=1

def get_srcip_for_optimal_flow_redirection(key=None,next_svc_mac_addr=None):
  if key is None or next_svc_mac_addr is None:
    return None
  svc_id = key[0]
  cur_svc_mac_addr = key[1]
  
  if next_svc_mac_addr not in mac_map.keys():
    log.error(" MAC Addr:[%s] not found in mac_map [%s]",next_svc_mac_addr,mac_map.keys())
    return None

  optimal_srcip = None
  dst_addr = mac_map[next_svc_mac_addr]
  dst_sw,dst_port = dst_addr[0],dst_addr[1]
  optimal_path_len = -1
  
  flows_at_svc_dict = fmap_sk.get_all_flows_for_svc_id_at_svc_mac(svc_id,cur_svc_mac_addr)
  if not flows_at_svc_dict:
    log.error("[Optimal Flow Redirection] No Flows for svc at [%d:%s]!", svc_id,cur_svc_mac_addr)
    return None

  for k,v in flows_at_svc_dict.items():
    new_path = None
    path  = fmap_sk.get_path_of_flow_from_flow_info_table(k) #flow[1]
    mac,ip = fmap_sk.get_src_mac_and_src_ip_from_flow_key(k) #flow[0]
    
    src_sw  = path[0][0]
    src_port = path[0][1]
    init_out_port = path[0][2]
    new_path = _get_path(src_sw, dst_sw, src_port, dst_port)
    if not new_path: continue
    #if len(new_path) > 2 and new_path[0][2] != init_out_port: log.warning("New chosen path not in same direction as current svc! ") 
    if optimal_path_len < 0:
      optimal_srcip = ip
      optimal_path_len = len(new_path)
    else:
      if optimal_path_len > len(new_path):
        optimal_srcip = ip
        optimal_path_len = len(new_path)
        #if len(new_path) > 2 and new_path[0][2] != init_out_port: log.warning("New chosen path[%s] not in same direction as current svc path [%s]!", new_path,path)
  
  log.info("Chose the Path with Len[%d] and SRCIP[%s]!",optimal_path_len, optimal_srcip)
  return optimal_srcip

def redirect_flows_from_this_instance(svc_id=0, svc_mac_addr=None, check_for_flows = False):
  #Move some flows from this service instance towards other lightly loaded instance
  #Which instance to offload the flow to: - Pick the best SP or MIN SP or MIN flows
  #Which flow to pick from this instance: - pick the one nearest to the new instance
  #How many flows? == 1
  flows_on_cur_svc = fmap_sk.get_total_flow_count_for_svc_id_at_svc_mac(svc_id, svc_mac_addr)
  if not flows_on_cur_svc:
    log.warning("Incorrect Redirection request!! No Flows at Svc[%d] at Mac [%s]", svc_id,svc_mac_addr)
    return
    
  # Pick the alternate best possible Mac address of the Service: 
  mac_addr = None
  # Get the Current best MAC for the Service 
  b_ip, b_mac, b_sp = core.openflow_discovery.getIP_MAC_of_BestSP_sk(svc_id)
  _mac = EthAddr(b_mac)  
  if _mac != svc_mac_addr:
    mac_addr = _mac
  else: # Get any other alternative Mac
    ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
    for ip,mac,sp in ip_mac_sp_list:
      _mac = EthAddr(mac)
      if _mac != svc_mac_addr:
        mac_addr = _mac
        break
  if mac_addr is None:
    log.warning("redirect_flows_from_this_instance(SVC[%d]):: Failed to find alternate MAC!", svc_id)
    return
  log.warning ("redirect_flows_from_this_instance(SVC[%d]), Move from current MAC[%s] to new MAC[%s] ", svc_id,svc_mac_addr,mac_addr)
  
  #2 number of Flows:
  redir_flows_count = min(MAX_FLOWS_FOR_ALL_REDIRECTIONS,flows_on_cur_svc*0.5)
  
  # Pick one of the flows (IP) from the Flow table: Selection Criteria: Of all the Flows Choose the Flow with best cost gain for the chosen Service instance
  src_ip = get_srcip_for_optimal_flow_redirection(key=(svc_id,svc_mac_addr),next_svc_mac_addr=mac_addr)
  
  if mac_addr and src_ip:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=svc_mac_addr,next_svc_mac_addr=mac_addr, for_src_ip=src_ip)
  else:
    log.warning("Failed to find any flow for service[%d]", svc_id)
  return

MAX_FLOWS_FOR_ALL_REDIRECTIONS = 5
def redirect_all_flows_from_this_instance(svc_id=0, svc_mac_addr=None, check_for_flows = False):
  ttl_flows = fmap_sk.get_total_flow_count_for_svc_id_at_svc_mac(svc_id, svc_mac_addr)
  
  if ttl_flows >= MAX_FLOWS_FOR_ALL_REDIRECTIONS:
    log.warning("redirect_all_flows_from_this_instance()::For SVC[%d] at MAC[%s] Total flows [%d] exceeds max limit[%d]!", svc_id, svc_mac_addr, ttl_flows,MAX_FLOWS_FOR_ALL_REDIRECTIONS)
    return False

  log.warning("redirect_all_flows_from_this_instance()::For SVC[%d] at MAC[%s] Total flows [%d]!", svc_id, svc_mac_addr, ttl_flows)
  for i in range(0,ttl_flows):
    redirect_flows_from_this_instance(svc_id=svc_id, svc_mac_addr=svc_mac_addr, check_for_flows=check_for_flows)

  return True

def redirect_flows_towards_this_instance(svc_id=0, new_svc_mac_addr=None, check_for_flows = False):
  """ This function is called by the Timer and will redirect routes based on entries in the next_svc_flow_table"""
  #Move some other flows using other service instance towards this instance
  #Which instance to offload the flow from: - Pick the one with MAX SP or MAX flows
  #Which flow to pick from that instance: - pick the one nearest to the new instance
  #How many flows? == 1 - just be conservative.
  
  mac_addr = None
  mac_list = []
  flows_on_cur_svc = 0
  
  #0 Check for Num of Flows before initiating redirection
  if check_for_flows:
    ttl_flows, mac_list = get_ttl_svc_flows_and_svc_instances(svc_id=svc_id)
    fs = MAX_FLOWS_AGR_SERVICE/float(max(1,len(mac_list)))
    #if ttl_flows > len(mac_list): #and  ttl_flows > fs:
    #Temporaritly commenting the below code: <TODO: REVERT AFTER TEST for SVC SWAP
    if ttl_flows > len(mac_list) and  ttl_flows > MAX_FLOWS_PER_SERVICE/2:
      pass
    else:
      log.warning("For svc[%d]:: NumInstances=[%d], Flow Count=[%d] is not enough to proceed with Redirection towards this new instance[%s] ", svc_id,len(mac_list),ttl_flows, new_svc_mac_addr)
      return
  
  
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  #1 pick instance (with highest SP)
  min_sp = 0
  for ip,mac,sp in ip_mac_sp_list:
    _mac = EthAddr(mac)
    if _mac != new_svc_mac_addr and min_sp < int(sp):
      mac_addr = _mac
      min_sp = int(sp)
      break

  if mac_addr is None:
    log.warning("redirect_flows_towards_this_instance(SVC[%d]):: Failed to find alternate SOURCE MAC!", svc_id)
    return
  
  #2 number of Flows:
  redir_flows_count = min(MAX_FLOWS_FOR_ALL_REDIRECTIONS,flows_on_cur_svc*0.5)
  
  #2 Pick the optimal flow
  src_ip = get_srcip_for_optimal_flow_redirection(key=(svc_id,mac_addr),next_svc_mac_addr=new_svc_mac_addr)
  
  if mac_addr and src_ip:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=mac_addr,next_svc_mac_addr=new_svc_mac_addr, for_src_ip=src_ip)
  else:
    log.warning("redirect_flows_towards_this_instance(SVC[%d])Failed to find any flows from MAC[%s]", svc_id, mac_addr)
  return
  
def redirect_routes(svc_id=0, sw_dpid=0,out_port=0):
  """ This function is called by the Timer and will redirect routes based on entries in the next_svc_flow_table """
  # Logic:
  # 1. Pick the Service with least Cost Factor:
  # 2. Pick one of the flows (IP) from the Flow table: Criteria: Of all the Flows Choose the Flow with best cost gain for the chosen Service instance
  #log.debug("redirect_routes():: for SVC[%d] DPID:PORT [%d:%d]!", svc_id, sw_dpid, out_port)
  
  #July 10,2015, 12:30PM
  #TODO: Logic needs to be inserted first to decide the direction of redirection for this svc_mac_addr
  #How? if  SP of SVC_MAC_ADDR is LESS THAN MIN_THRESHOLD then intent is to redirect any ongoing flows at other instances towards this instance as a new flow
  # else: redirect ongoing flows from this service instance towards any other instance.
  #Check for the Validity of the Existing Route First: if not Return
  
  svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=sw_dpid, port_id=out_port)
  if svc_mac_addr is None:
    return
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  for ip,mac,sp in ip_mac_sp_list:
    _mac = EthAddr(mac)
    if _mac == svc_mac_addr:
      if int(sp) <= disc.MIN_SP_THRESHOLD:
        redirect_flows_towards_this_instance(svc_id=svc_id, new_svc_mac_addr=svc_mac_addr)
      else:
        redirect_flows_from_this_instance(svc_id=svc_id, svc_mac_addr=svc_mac_addr)
      break
  return
  
  # Pick the alternate best possible Mac address of the Service: 
  mac_addr = None
  # Get the Current best MAC for the Service 
  b_ip, b_mac, b_sp = core.openflow_discovery.getIP_MAC_of_BestSP_sk(svc_id)
  _mac = EthAddr(b_mac)  
  if _mac != svc_mac_addr:
    mac_addr = _mac
  else: # Get any other alternative Mac
    ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
    for ip,mac,sp in ip_mac_sp_list:
      _mac = EthAddr(mac)
      if _mac != svc_mac_addr:
        mac_addr = _mac
        break  
  if mac_addr is None:
    return

  log.warning ("redirect_routes():: Redirect flows for SVC[%d] from current MAC[%s] to new MAC[%s] ", svc_id,svc_mac_addr,mac_addr)
  # Pick one of the flows (IP) from the Flow table: Selection Criteria: Of all the Flows Choose the Flow with best cost gain for the chosen Service instance
  src_ip = get_srcip_for_optimal_flow_redirection(key=(svc_id,svc_mac_addr),next_svc_mac_addr=mac_addr)
  
  if mac_addr and src_ip:
    redirect_path_for_svc(svc_id=svc_id,cur_svc_mac_addr=svc_mac_addr,next_svc_mac_addr=mac_addr, for_src_ip=src_ip)
  else:
    log.warning("Failed to find alternate MAC for service[%d]", svc_id)
  return

def get_ttl_svc_flows_and_svc_instances(svc_id=0):
  ttl_num_of_flows = fmap_sk.get_total_flow_count_for_svc_id(svc_id)
  svc_instance_list = fmap_sk.get_total_svc_instance_list_for_svc_id(svc_id)
  return ttl_num_of_flows,svc_instance_list
  
MAX_FLOWS_PER_SERVICE = 10 #10 <Remember: Note: Must scale this along with the Flow Numbers >
MAX_FLOWS_AGR_SERVICE = 10 #12
def update_sp_on_flow_add_or_del(svc_id=0,svc_mac_addr=None, path=None, addition= True, match=None):
  if svc_id is None or svc_mac_addr is None :
    log.warning(" Incorrect data! Skipped to update shadow price!")
    return
  
  svc_num_of_flows = fmap_sk.get_total_flow_count_for_svc_id_at_svc_mac(svc_id, svc_mac_addr)
  
  #Trying New to ensure that we run to keep SP fixed based on num of flows serviced.
  sp = (100*svc_num_of_flows)/float(MAX_FLOWS_AGR_SERVICE)
  
  if sp > disc.MAX_SHADOW_PRICE: 
    log.warning(" Shadow Price for the SVC instance[%d:%s] exceeded the Limit[%d] with #flows:[%d]",svc_id, svc_mac_addr,sp, svc_num_of_flows)
    pass #sp = disc.MAX_SHADOW_PRICE
  if sp < disc.MIN_SHADOW_PRICE: sp = disc.MIN_SHADOW_PRICE
  disc.update_shadow_price_2(mac_addr=svc_mac_addr,svc_id=svc_id,shadow_price= int(sp))
  
  return
  
  # Update for all the instances of that service.
  ttl_num_of_flows, svc_instance_list = get_ttl_svc_flows_and_svc_instances(svc_id)
  total_svc_instances = len(svc_instance_list)
  
  for mac in svc_instance_list:
    svc_mac_addr = EthAddr(mac)
    svc_num_of_flows = fmap_sk.get_total_flow_count_for_svc_id_at_svc_mac(svc_id, svc_mac_addr)
    
    sp = (100*svc_num_of_flows)/float(max(MAX_FLOWS_AGR_SERVICE, ttl_num_of_flows*total_svc_instances))
    disc.update_shadow_price_2(mac_addr=svc_mac_addr,svc_id=svc_id,shadow_price= int(sp))

  return 

hop_count_dict= {} # {svc_id:[flow_count, total_hop_count_of_all_flows], svc_id2:[]...}
hop_count_dict2={} # {svc_id,origin_sw_id,origin_sw_in_port: [hop_count, dst_sw_id, dst_sw_out_port]}

def rem_hopcount_for_installed_route(svc_id=0, hop_count=0,path=None, redirected=False):
  cur_flows = 0 
  cur_hop_count = 0
  if hop_count_dict.has_key(svc_id):
    cur_flows, cur_hop_count = hop_count_dict[svc_id][0], hop_count_dict[svc_id][1]
    cur_flows-=1
    cur_hop_count-=hop_count
    if cur_flows <=0:
     hop_count_dict.pop(svc_id)
    else:
      hop_count_dict[svc_id] = [cur_flows, cur_hop_count]
  #log.debug("Flow and Hop count Details for svc[%d]: is [%d,%d]", svc_id,hop_count_dict[svc_id][0], hop_count_dict[svc_id][1])
  
  if path is None:
    return
  key = (svc_id,path[0][0].dpid,path[0][1])
  if hop_count_dict2.has_key(key):
    if len(hop_count_dict2[key]) == 1: 
      hop_count_dict2.pop(key)
      #log.warning("Found the Only Item in hopdict2 to remove on flow deletion!")
      pass
    else:
      item = [hop_count,path[-1][0].dpid,path[-1][2]]
      for index, path_entry in enumerate(hop_count_dict2[key]):
        if path_entry == item:
          hop_count_dict2[key].pop(index)
          #log.warning("Found multiple items in hopdict2 to remove on flow deletion!")
          break
  return
  
def add_hopcount_for_installed_route(svc_id=0, hop_count=0,path=None, redirected=False):
  cur_flows = 0 
  cur_hop_count = 0
  if hop_count_dict.has_key(svc_id):
    cur_flows, cur_hop_count = hop_count_dict[svc_id][0], hop_count_dict[svc_id][1] 
     
  hop_count_dict[svc_id] = [cur_flows+1, cur_hop_count+hop_count]
  #log.debug("Flow and Hop count Details for svc[%d]: is [%d,%d]", svc_id,hop_count_dict[svc_id][0], hop_count_dict[svc_id][1])
  
  if path is None or svc_id == 0:
    return
  key = (svc_id,path[0][0].dpid,path[0][1])
  if hop_count_dict2.has_key(key):
    #hop_count_dict2[key] = hop_count_dict2[key] + [hop_count,path[-1][0].dpid,path[-1][2]]
    if redirected is False and svc_id != 0:
      #log.warning("Duplicate Flow for the Same Key(%s)!!", key)
      hop_count_dict2[key].append([hop_count,path[-1][0].dpid,path[-1][2]])
    else:
      log.warning("Found Flow with Redirection update for key[%s]", str(key))
      #for i,x in enumerate(hop_count_dict2[key]):
        #if x[1] == 
      hop_count_dict2[key][0] = [hop_count,path[-1][0].dpid,path[-1][2]]
  else:
    hop_count_dict2[key] = [[hop_count,path[-1][0].dpid,path[-1][2]]] # + 
  
def add_install_route(svc_id=0, path=None, match=None, redirected= False,p_to_dest=None):

  #if svc_id == 0:
  if match is None or path is None or len(path) == 0:
    return 

  org_switch          = path[0][0]
  org_switch_in_port  = path[0][1]
  org_switch_dpid     = org_switch.dpid
  org_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=org_switch_dpid, port_id=org_switch_in_port)
  if org_mac_addr is None:
    log.warning("Packet-IN for svc:[%d] at Intermediate switch!! [%s]", svc_id, path)
    return
  
  svc_switch =          path[-1][0]
  svc_switch_out_port = path[-1][2]
  svc_switch_dpid     = svc_switch.dpid
  svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=svc_switch_dpid, port_id=svc_switch_out_port)
  if svc_mac_addr is None:
    log.warning("Invalid Path setup for svc:[%d] at Intermediate switch!! [%s]", svc_id, path)
    return
  
  dst_path_len=0
  dst_mac_addr=None
  if p_to_dest: 
    dst_path_len = len(p_to_dest)
    dst_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=p_to_dest[-1][0].dpid, port_id=p_to_dest[-1][2])
  if False == fmap_sk.add_flow_to_flow_info_table(match=match, path=path, event=None, svc_id=svc_id, svc_mac_addr=svc_mac_addr, src_mac_addr=org_mac_addr,dst_mac_addr=dst_mac_addr,p_to_dest=p_to_dest,dst_path_len=dst_path_len):
    log.warning("Possible Duplicate packet processing..!")
  else:
    #log.warning("Added Flow from SRC[%s] for SVC[%d] at [%s] Path:[%s] to Dictionary",org_mac_addr,svc_id, svc_mac_addr, path)
    if svc_id:
        update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=svc_mac_addr,path=path,addition=True)
    add_hopcount_for_installed_route(svc_id=svc_id, hop_count=len(path),path=path, redirected=redirected)
    global dbg_flow_counter
    dbg_flow_counter +=1
  return

def get_mac_addr_from_sw_and_port(sw_dpid=0, port_id=0):
  #find_value = (dpid_to_str(sw_dpid),port_id)
  find_value = (get_sw_from_dpid(sw_dpid),port_id)
  log.debug("Requested for sw_id%d, port_id:%d, find_value:%s", sw_dpid,port_id,find_value)
  for key, value in mac_map.iteritems():
    if value == find_value:
      #log.debug("key:%s, value:%s, find_value:%s, mac_map:[%s]", key,value,find_value, mac_map)
      return key
  log.debug("NO KEY for find_value:%s", find_value)
  return None

def check_and_swap_any_service_instance_for_svc(svc_id=0, cur_max_sp=0, sw_path_list=None):

  svc_dict = disc.get_svc_dict()
  if (svc_dict.has_key(svc_id) is False):
    log.error ('Key[%d] not found! skipp swapping for the requested service',svc_id)
    return False
  
  if (svc_dict.has_key(0) and len(svc_dict[0]) > 0) and len(svc_dict[0][0]):
    log.error ('Alternate Free Instances are still available! skipp swapping for the requested service [%d], %s',svc_id, svc_dict[0])
    return False
  
  def_list = disc.get_default_svc_list()
  
  swap_svc_id = 0
  swap_svc_instace = None
  for key,value in svc_dict.items():
    if key!= 0 and key != svc_id:
      for x in range(len(value)):
        s_sp,s_mac_id,s_ip,s_sw_id,pt_id = value[x]
        if (key,s_mac_id) in def_list: continue    # Ensure not to swap for svc's created for default case
        if int(s_sp) == 0 or int(s_sp) < disc.MIN_SP_PEAK_THRESHOLD or int(s_sp) == disc.MAX_SP_FOR_BRINGDOWN:
          log.warning("Swap Underutilized service[ %d, at %s] to Over utilized service[%d, %d]", key,value[x],svc_id, cur_max_sp)
          swap_svc_id = key
          swap_svc_instace = value[x]
          break

  if swap_svc_id != 0 and swap_svc_instace is not None:
    mac = swap_svc_instace[1]
    _mac = EthAddr(mac)
    log.warning("Try to bringdown service Service[%d] at mac[%s] with [%s]", svc_id, _mac, swap_svc_instace)
    redirect_all_flows_from_this_instance(svc_id=swap_svc_id, svc_mac_addr=_mac, check_for_flows = False)
    ret = disc.bringdown_service_instace_for_svc(svc_id = swap_svc_id, svc_mac_addr =_mac, skip_wait_period=True)
    if ret is True:
      update_svc_flow_table_on_svc_instance_bringdown(svc_id = swap_svc_id, svc_mac_addr =_mac)
      ret = disc.instantiate_new_instance_for_svc(svc_id = svc_id, cur_max_sp= cur_max_sp,sw_path_list=sw_path_list,new_service=False)
      log.warning("result to Swap and Bring new svc[%d] instance at mac[%s] is [%d]", svc_id, _mac, ret)
      return ret
  else:
    log.warning("Failed to find any under utilized service instance for SWAP to svc[%d]", svc_id)
  return False
  
#Aggregate Flow Statistics Table <routing based on aggregate flow_counts observed per switch>
aggr_flow_dict = {} # {'dpid1':[pkt_count,byte_count,flow_count], 'dpid2':[pkt_count,byte_count,flow_count]}
def get_aggr_flow_stats(sw_id=0):
  global aggr_flow_dict
  if (False == aggr_flow_dict.has_key(sw_id)):
    return 0
  #log.debug("For switch:%d, %s current total of flows: %d", sw_id, aggr_flow_dict[sw_id], aggr_flow_dict[sw_id][2])
  return aggr_flow_dict[sw_id][2]
def handle_aggregate_stats(sw_id=0, aggr_info=None):
  if aggr_info:
    global aggr_flow_dict
    aggr_flow_dict[sw_id] = [aggr_info.packet_count,aggr_info.byte_count,aggr_info.flow_count, time.time()]
    #log.warning("aggr_flow_dict: %s", aggr_flow_dict)
    #log.error("For Switch[%d], Total Flows=[%d]", sw_id, aggr_info.flow_count)
  return
def get_flow_count_for_path(path=None, svc_mac_addr=None, svc_id=0):
  
  flows_count = 0
  
  ##Approach 1 : For All the Switches in the path: get data from aggr_flow_dict
  ##if svc_id == 0 or svc_mac_addr is None and path is not None: < Flows across entire path: problem is tht it includes duplicate counts>
  #if path:
  #  for sw,out_port,in_port in path:
  #    flows_count+= get_aggr_flow_stats(sw.dpid)
  #  return flows_count
  
  #Approach#2: get the data for svc_id from  svc_flow_table.
  if svc_id and svc_mac_addr is not None:
    flows_count = fmap_sk.get_total_flow_count_for_svc_id_at_svc_mac(svc_id,svc_mac_addr)
    #log.warning("Flow Count for Key[%d:%s] is [%d]", svc_id, svc_mac_addr, flows_count)
  
  log.debug("For Path:%s, current total of flow counts: %d", path, flows_count)
  return flows_count


def _calc_paths ():
  """
  Essentially Floyd-Warshall algorithm
  """

  def dump ():
    for i in sws:
      for j in sws:
        a = path_map[i][j][0]
        #a = adjacency[i][j]
        if a is None: a = "*"
        print a,
      print

  sws = switches.values()
  path_map.clear()
  for k in sws:
    for j,port in adjacency[k].iteritems():
      if port is None: continue
      path_map[k][j] = (1,None)
    path_map[k][k] = (0,None) # distance, intermediate
  
  #dump()

  for k in sws:
    for i in sws:
      for j in sws:
        if path_map[i][k][0] is not None:
          if path_map[k][j][0] is not None:
            # i -> k -> j exists
            ikj_dist = path_map[i][k][0]+path_map[k][j][0]
            if path_map[i][j][0] is None or ikj_dist < path_map[i][j][0]:
              # i -> k -> j is better than existing
              path_map[i][j] = (ikj_dist, k)

  #print "--------------------"
  #dump()


def _get_raw_path (src, dst):
  """
  Get a raw path (just a list of nodes to traverse)
  """
  if len(path_map) == 0: _calc_paths()
  if src is dst:
    # We're here!
    return []
  if path_map[src][dst][0] is None: _calc_paths()
  if path_map[src][dst][0] is None:  return None
  intermediate = path_map[src][dst][1]
  if intermediate is None:
    # Directly connected
    return []
  return _get_raw_path(src, intermediate) + [intermediate] + \
         _get_raw_path(intermediate, dst)


def _check_path (p):
  """
  Make sure that a path is actually a string of nodes with connected ports

  returns True if path is valid
  """
  for a,b in zip(p[:-1],p[1:]):
    if adjacency[a[0]][b[0]] != a[2]:
      return False
    if adjacency[b[0]][a[0]] != b[1]:
      return False
  return True

def get_networkx_path(src, dst, first_port, final_port):
    all_nw_paths = nwrt_sk.get_all_paths_between_edge_switch(src.dpid,dst.dpid,first_port,final_port)
    log.warning("Obtained Following paths from Network Helper [%s]", all_nw_paths)
    for path in all_nw_paths:
        for index, (sw_dpid, in_port,out_port) in enumerate(path):
            sw = switches[sw_dpid]
            path[index] = (sw,in_port,out_port)
    return all_nw_paths

def _get_path (src, dst, first_port, final_port):
  """
  Gets a cooked path -- a list of (node,in_port,out_port)
  """
  #all_nw_paths = get_networkx_path(src, dst, first_port, final_port)
  #log.warning("computed Networkx paths for [%s:%s:%s:%s]= %s", src, dst, first_port, final_port,all_nw_paths)

  # Start with a raw path...
  if src == dst:
    path = [src]
  else:
    path = _get_raw_path(src, dst)
    if path is None:
      log.warning("_get_path Failed!! Trying to get Networkx path between (%s:%s -- %s:%s)", src, dst, first_port, final_port)
      all_nw_paths = get_networkx_path(src, dst, first_port, final_port)
      if all_nw_paths and all_nw_paths[0]: 
        path = all_nw_paths[0]
        log.warning("computed Networkx paths for [%s:%s:%s:%s]= %s", src, dst, first_port, final_port,path)
      return path
    
    path = [src] + path + [dst]

  # Now add the ports
  r = []
  in_port = first_port
  for s1,s2 in zip(path[:-1],path[1:]):
    out_port = adjacency[s1][s2]
    r.append((s1,in_port,out_port))
    in_port = adjacency[s2][s1]
  r.append((dst,in_port,final_port))

  assert _check_path(r), "Illegal path!"
  
  if r is None:
    log.warning("_get_path Failed!! Trying to get Networkx path between (%s:%s -- %s:%s)", src, dst, first_port, final_port)
    all_nw_paths = get_networkx_path(src, dst, first_port, final_port)
    if all_nw_paths and all_nw_paths[0]: r = all_nw_paths[0]
    log.warning("computed Networkx paths for [%s:%s:%s:%s]= %s", src, dst, first_port, final_port,r)
  return r

def _toBinary(n):
  """
  Converts an integer to binary
  """ 
  return ''.join(str(1 & int(n) >> i) for i in range(64)[::-1])
  #return '{0:12b}'.format(n)

def _get_vlan_id(event):
  pkt_vlan_id = None
  split_vlan_id = None
  vlan_packet = event.parsed.find("vlan")
  if vlan_packet is not None:
    vlan_id_str = None
    pkt_vlan_id = vlan_packet.id
    id = _toBinary(vlan_packet.id)
    vlan_id_str = id[-4:]
    split_vlan_id =  (vlan_packet.id & 15)
    #log.debug("Pkt VLAN_ID:[%d]::ID[%s]::Desired VLAN_ID[%d], Split VLAN_ID:[%d]",pkt_vlan_id,id,int(vlan_id_str,2),split_vlan_id)
  return pkt_vlan_id

def get_svc_list(vlan_id):
  if vlan_id != 0:
    svc_count = 0
    svc_list = []
    while(vlan_id & 0x0F):
      svc = vlan_id & 0x0F
      svc_list.append(svc)
      svc_count+=1
      vlan_id >>= 4
    log.debug("Number of Services:[%d], List of Services:[%s]",svc_count,svc_list) 
    return svc_list
  else:
    pass
  return None

class WaitingPath (object):
  """
  A path which is waiting for its path to be established
  """
  def __init__ (self, path, packet, match=None, svc=None, vlan_instance_id=None, ttl=None):
    """
    xids is a sequence of (dpid,xid)
    first_switch is the DPID where the packet came from
    packet is something that can be sent in a packet_out
    """
    self.expires_at = time.time() + PATH_SETUP_TIME
    self.path = path
    self.first_switch = path[0][0].dpid
    self.xids = set()
    self.packet = packet
    self.match = match
    self.svc = svc
    self.vlan_instance_id = vlan_instance_id
    self.ttl = ttl
    self.retry = 0

    if len(waiting_paths) > 10000:
      log.warning("More than 1000 waiting paths! Forcefully Expiring the waiting paths!")
      #WaitingPath.expire_waiting_paths()

  def add_xid (self, dpid, xid):
    self.xids.add((dpid,xid))
    waiting_paths[(dpid,xid)] = self

  @property
  def is_expired (self):
    return time.time() >= self.expires_at

  def notify (self, event):
    """
    Called when a barrier has been received
    """
    self.xids.discard((event.dpid,event.xid))
    if len(self.xids) == 0:
      # Done!
      if self.packet:
        log.debug("Sending delayed packet out %s"
                  % (dpid_to_str(self.first_switch),))
        msg = of.ofp_packet_out(data=self.packet,
            action=of.ofp_action_output(port=of.OFPP_TABLE))
        core.openflow.sendToDPID(self.first_switch, msg)

      core.l2_multi.raiseEvent(PathInstalled(self.path))


  @staticmethod
  def timeout_waiting_paths():
    #log.warning("Timedout on waiting paths! Expire the waiting paths!")
    WaitingPath.expire_waiting_paths()
  
  @staticmethod
  def expire_waiting_paths ():
    packets = set(waiting_paths.values())
    killed = 0
    for p in packets:
      if p.is_expired:
        killed += 1
        for entry in p.xids:
          waiting_paths.pop(entry, None)
    if killed:
      log.error("%i paths failed to install" % (killed,))


class PathInstalled (Event):
  """
  Fired when a path is installed
  """
  def __init__ (self, path):
    Event.__init__(self)
    self.path = path

def get_svc_chain_from_parsed_payload_data(p_data = None):
  if not p_data:
    return None
  of = p_data.find('svclist,')
  ln = len('svclist,')
  of2 =  p_data.find(',end')
  if of2 < of+ln:
    return None
  schain = p_data[of+ln:of2]
  schain = schain.lstrip(',').rstrip(',')
  sch_l  = [ int(i,2) for i in schain.split(',') if len(i) and i.isdigit()]
  log.warning("Data:[%s], [%s]:[%s]",p_data, schain, sch_l)
  return sch_l

def verify_and_get_svc_chain_data(event):
  import pox.lib.packet as pkt
  
  icp = event.parsed.find("icmp")
  if icp and icp.type == pkt.TYPE_ECHO_REQUEST:
    type = icp.type
    code = icp.code
    payload = icp.payload
    #log.info("Received the ICMP Echo request Packet with type=[%d], code=[%d], payload=[%s], in_pkt[%s], icp[%s]",type,code,payload,in_pkt, icp)
    seq = payload.seq
    nid = payload.id
    log.warning("NID[%d], SEQ[%d]", nid,seq)
    in_pkt= event.parsed
    data = in_pkt.pack()
    if data:
      return get_svc_chain_from_parsed_payload_data(p_data = data)
  return None

def print_icmp_packet_data(event):
  import pox.lib.packet as pkt
  svc_chain = None
  in_pkt= event.parsed
  data = in_pkt.pack()
  if data:
    svc_chain = get_svc_chain_from_parsed_payload_data(p_data = data)
    log.debug("Retrieved Service Chain[%s]", svc_chain)
  
  ipp = event.parsed.find("ipv4")
  pd2 = ''
  if ipp:
    pd2 = ipp.payload
    dt = ipp.pack()
    log.debug("IP PKT[%s] and PAYLOAD[%s], Dt[%s]", ipp, pd2, dt)
 
  icp = event.parsed.find("icmp")
  if icp and icp.type == pkt.TYPE_ECHO_REQUEST:
    type = icp.type
    code = icp.code
    payload = icp.payload
    log.debug("Received the ICMP Echo request Packet with type=[%d], code=[%d], payload=[%s], in_pkt[%s], icp[%s]",type,code,payload,in_pkt, icp)
    seq = payload.seq
    nid = payload.id
    log.debug("NID[%d], SEQ[%d]", nid,seq)
    if data:
      svc_chain = get_svc_chain_from_parsed_payload_data(p_data = data)
      log.debug("Retrieved Service Chain[%s]", svc_chain)
    dt = icp.pack()
    log.debug("DT[%s]", dt)
  return svc_chain

UDP_PORT_LOW=10000
UDP_PORT_HIGH=15000
def get_ipp_packet_protocol(event):
    ipp = event.parsed.find("ipv4")
    if ipp:
        return ipp.protocol
    return 0
def check_valid_udp_packet(event):
    ipp = event.parsed.find("ipv4")
    udpp = event.parsed.find("udp")
    tcpp = event.parsed.find("tcp")
    vlpp = event.parsed.find("vlan")
    ttl = 0
    dst_port = 0
    
    #Incase of packet without valid TOS, we do not need to handle it
    if ipp and ipp.tos == 0: return False,0,0
    #Incase of TCP ignore the SYN, ACK and FIN, RST flag enabled packets
    if tcpp and (tcpp.FIN or tcpp.SYN  or tcpp.RST): # or tcpp.ACK
        log.warning("SYN:%d, ACK:%d, FIN:%d RST:%d", tcpp.SYN, tcpp.ACK, tcpp.FIN, tcpp.RST)
        return False,0,0

    if udpp: dst_port = udpp.dstport
    if tcpp: dst_port = tcpp.dstport
    
    #if ipp:
    #  log.warning(" IP PKT with IP_PROTOCOL= [%d], IP_TOS = [%d], IP_TTL=[%d]", ipp.protocol, ipp.tos, ipp.ttl)
    #unfortunately vlan is lost at intermediate nodes due to scapy :( and no way to determine the sequence in chain!!!
    #vlanp = event.parsed.find("vlan")
    #if vlanp:
    #    cur_vlan_pcp = vlanp.pcp
    #else:
    #   cur_vlan_pcp = 0
    
    #Rely on IPTOS to identify the service chain and IP TTL to identify the sequence in the chain. Hosts increment the TTL after processing data 
    if ipp and dst_port and  (dst_port == 8999 or (dst_port >= UDP_PORT_LOW and dst_port <= UDP_PORT_HIGH )): #udpp.srcport > 10000 and udpp.srcport < 20000:
        #log.info("valid UDP Packet with IP_PROTOCOL= [%d], IP_SRC=[%s] IP_DST=[%s] IP_TOS = [%d], IP_TTL=[%d], UDP_SPORT[%d] UDP_DPORT=[%d] ", ipp.protocol, ipp.srcip, ipp.dstip, ipp.tos, ipp.ttl, udpp.srcport, udpp.dstport)
        #Rely on DST Port Last  bits to determine the current svc 
        #ttl = (dst_port -1) & 0x0F
        ttl = (dst_port -1) %10
        tos = (ipp.tos & 0xFC)
        return True, tos, ttl
    
    #elif ipp and udpp:
    #  log.warning("Invalid UDP packet with IP_PROTOCOL= [%d], IP_TOS = [%d], IP_TTL=[%d], udp_sport[%d] udp_dport[%d]", ipp.protocol, ipp.tos, ipp.ttl, udpp.srcport, udpp.dstport)
    return False, 0, 0
def get_vlan_id_from_ip_tos_and_ttl(tos,ttl):
    #schain = [13,15]
    #if ttl >= len(schain):
    #    return 0
    #return schain[ttl]
    chain_len = util_sk.get_svc_chain_len_for_key(key=tos)
    if chain_len < 0:
        log.warning("Bad Request! key [%d], chain_len[%d], index [%d]", tos,chain_len,ttl)
        return None
    elif chain_len == ttl:
        log.info("End of Chain! key [%d], chain_len[%d], index [%d]", tos,chain_len,ttl)
        return 0
    if ttl > chain_len:
        ttl = 0
    svc_id = util_sk.get_svc_for_key_at_index(key=tos,index=ttl)
    if svc_id < 0:
        log.warning("Potential End of Chain?? key [%d], chain_len[%d], index [%d]", tos,chain_len,ttl)
        return 0
    return svc_id
def get_chain_length_from_tos(tos):
    chain_len = 0
    chain_len = util_sk.get_svc_chain_len_for_key(key=tos)
    return chain_len
def is_pkt_icmp_src_quench_type(packet):
  icp = packet.find("icmp")
  if icp and icp.type == pkt.TYPE_SRC_QUENCH:
    log.info("Received the ICMP SRC QUENCH Request Packet with type=[%d], code=[%d]",icp.type,icp.code)
    return True
  return False
def get_penalty_factor_2(self, out_port_dst=0, hop_count_dst=0, out_port_svc=0, hop_count_svc=0, hop_count_dst_frm_svc=0 ):
  penalty_factor = 0.0
  MAX_HOP_COUNT = len(switches)  #10
  
  if hop_count_dst_frm_svc > hop_count_dst: # Debate on whether it should be  hop_count_dst_frm_svc >= hop_count_dst?  scenario: what if svc is at src switch itself? will have overhead. if not then arbitrary path with same len will incur no penalty (now).
    penalty_factor = ((hop_count_svc + hop_count_dst_frm_svc - hop_count_dst)/float(hop_count_dst))
    
  log.debug("For Dst [%d] through svc [%d] = [%d] results in penatly %f", hop_count_dst,hop_count_svc,hop_count_dst_frm_svc,penalty_factor)
  #log.warning("For Switch [%d] Dst_hop [%d] svc_hop=[%d], svc_to_dst_hop=[%d] results in penalty %f", self.dpid, hop_count_dst,hop_count_svc,hop_count_dst_frm_svc,penalty_factor)
  return penalty_factor

def compute_path_cost_2(self, hop_count_dst = 1, shadow_price=100, hop_count_svc=0, penalty_factor = 0, Weight = 0):
  """
  Function to return the path cost based on the the hop_count, and shadow price.
  shadow_price    [0-100]       :: Lesser the better
  hop_count       [1-MAX_HOPS]  :: Lesser the better
  penalty_factor  [0 to 1]     :: Lesser the better
  Weight          [TBD]
  Args can be extended to incorporate aggregate b/w, max hops etc to factor for estimation.
  For now simply return the cost = ((hop_count)*100)+(SP)
  Actual cost estimation equation can be deduced later. 
  """
  MAX_HOP_COUNT = len(switches) #10  
  return (1+penalty_factor)*(shadow_price + ((hop_count_svc*100)/MAX_HOP_COUNT))  #<Most working data>
  
def get_penalty_factor(self, out_port_dst=0, hop_count_dst=0, out_port_svc=0, hop_count_svc=0 ):
  penalty_factor = 0.0
  MAX_HOP_COUNT = len(switches)  #10
  # For cases where Destination is at the requested switch or service is at intented switch then ignore penalty (as this might reroute back and add to loop)
  if hop_count_dst <= 1 or hop_count_svc <=1:
    return 0
  if out_port_dst != out_port_svc:
    #penalty_factor = ((hop_count_dst+hop_count_svc)%MAX_HOP_COUNT)/MAX_HOP_COUNT
    if (2*hop_count_svc) >= MAX_HOP_COUNT: # hop_count_dst+hop_count_svc >= MAX_HOP_COUNT:
      penalty_factor = 1
    else:
      #penalty_factor = ((hop_count_dst+hop_count_svc)/float(MAX_HOP_COUNT))
      penalty_factor = ((2*hop_count_svc)/float(MAX_HOP_COUNT))
    
  log.debug("For Dst [port %d,len %d], Trhough Svc [port %d,len %d], Max_Hops %d results in penatly %f", out_port_dst,hop_count_dst,out_port_svc,hop_count_svc,MAX_HOP_COUNT,penalty_factor)
  return penalty_factor

def compute_path_cost(self, hop_count_dst = 1, shadow_price=100, hop_count_svc=0, penalty_factor = 0, Weight = 0):
  """
  Function to return the path cost based on the the hop_count, and shadow price.
  shadow_price    [0-100]       :: Lesser the better
  hop_count       [1-MAX_HOPS]  :: Lesser the better
  penalty_factor  [0 to 1]     :: Lesser the better
  Weight          [TBD]
  Args can be extended to incorporate aggregate b/w, max hops etc to factor for estimation.
  For now simply return the cost = ((hop_count)*100)+(SP)
  Actual cost estimation equation can be deduced later. 
  """
  MAX_HOP_COUNT = len(switches) #10
  if penalty_factor == 0:
    #hop_count =abs(hop_count_dst - hop_count_svc)
    hop_count = hop_count_svc
  else:
    #hop_count_dst + hop_count_svc
    hop_count = 2*hop_count_svc 
    
  #if shadow_price <= 0:
  #  return ((hop_count)*100)
  #return (((hop_count)*100)/(shadow_price))
  return (shadow_price + ((hop_count_svc*100)/MAX_HOP_COUNT))  #<Most working data>
  #return (shadow_price + ((hop_count*100)/MAX_HOP_COUNT))  # Note (This adds the Loops in the path)
#return ((1+penalty_factor)*(shadow_price + ((hop_count*100)/MAX_HOP_COUNT)))   # Note (This adds the Loops in the path)
  #return ((1+penalty_factor)*(shadow_price + ((hop_count_svc*100)/MAX_HOP_COUNT)))   # Note (This adds the Loops in the path)

def get_optimal_path_and_cost_to_svc(self, svc, dst_sw, last_port, src_port):
  # First compute path to Destination
  p_to_dest = _get_path(self, dst_sw, src_port, last_port)
  if p_to_dest is None:
    log.error("get_optimal_path_and_cost_to_svc::Failed to compute path for svc[%d] from [%s:%d] to [%s:%d] path!",svc,self,src_port,dst_sw,last_port)
    return (None, None, None, None)
  hop_to_dst = len(p_to_dest)
  log.debug("Path from[%s:%d] to Intended Destination[%s:%d] is [%d] hops as:[%s]",self,src_port,dst_sw,last_port,len(p_to_dest),p_to_dest)
  
  #if svc = 0: only destination requested <skip further processing and return path to destination
  if svc == 0: return (None,None, None, p_to_dest)
  
  #Get List of Eligible Hosts for requested Service
  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc)
  if not ip_mac_sp_list:
    log.warning(" Requested SVC[%d] has no instances!!", svc)
    return (None, None, None, p_to_dest)
  
  hop_count_list = []
  best_out_port = None
  best_path = None
  best_cost = None
  best_instance_sp = disc.MAX_SHADOW_PRICE*10
  for ip,mac,sp in ip_mac_sp_list:
    dest_mac = EthAddr(mac)
    if( dest_mac and dest_mac in mac_map):
      dest_addr = mac_map[dest_mac]
      p = _get_path(self, dest_addr[0], src_port, dest_addr[1])
      p_svc_to_dst =  _get_path(dest_addr[0], p_to_dest[-1][0], dest_addr[1], p_to_dest[-1][2])
      if p:
        flow_count = get_flow_count_for_path(path=p,svc_mac_addr=dest_mac,svc_id=svc)
        hop_count = len(p)
        out_port =  p[0][2] #for sw,in_port,out_port in p:
        log.info("For svc[%d] at Mac:%s: with SP:%s:: Path is:[%s] with [HopCount:%d, FlowCount:%d] ", svc, mac,sp, p,hop_count,flow_count)
        hop_count_list.append([mac,sp,hop_count,out_port,p,flow_count, len(p_svc_to_dst)])
        if int(sp) < best_instance_sp:
          best_instance_sp = int(sp)
  
  log.debug("List of len:%d is:%s with best_instance_sp:%d", len(hop_count_list), hop_count_list, best_instance_sp)
  for x in range(0,len(hop_count_list)):
    cur_mac, cur_sp, cur_hc, cur_op, cur_path,cur_fc, p_svc_to_dst_len = hop_count_list[x]
    penalty_factor = get_penalty_factor_2(self,out_port_dst=p_to_dest[0][2], hop_count_dst=hop_to_dst, out_port_svc=cur_op, hop_count_svc=cur_hc, hop_count_dst_frm_svc=p_svc_to_dst_len )
    new_cost = compute_path_cost_2(self,hop_count_dst = hop_to_dst, shadow_price=int(cur_sp),hop_count_svc = cur_hc, penalty_factor=penalty_factor)
    #new_cost = cur_fc
    #penalty_factor = get_penalty_factor(self,out_port_dst=p_to_dest[0][2], hop_count_dst=hop_to_dst, out_port_svc=cur_op, hop_count_svc=cur_hc )
    #new_cost = compute_path_cost(self,hop_count_dst = hop_to_dst, shadow_price=int(cur_sp),hop_count_svc = cur_hc, penalty_factor=penalty_factor)
    #new_cost = cur_fc
    log.debug("computed cost [%d] via port [%d] path:%s", new_cost,cur_op,cur_path)
    if x == 0:
      best_out_port =  cur_op
      best_path     =  cur_path
      best_cost  = new_cost
    else:
      if( best_cost > new_cost):
        best_out_port = cur_op
        best_path     =  cur_path
        best_cost  = new_cost
  
  if best_out_port:
    log.debug("Computed Optimal Path:%s through out port:%d with path_cost[%d] ",best_path, best_out_port, best_cost)
    
  return (best_path, best_cost, best_instance_sp, p_to_dest)
    
def get_optimal_path(self, svc, dst_sw, last_port, match, src_port):
  """
  Function to evaluate and retrieve the best possible path for the intended service
  """
  log.debug(" get_optimal_path::Start Time: %s", time.time())

  
  best_path,best_cost,best_instance_sp, p_to_dest = get_optimal_path_and_cost_to_svc(self, svc, dst_sw, last_port, src_port)
  #log.debug("bp[%s],bc:[%s],bsp[%s],pd[%s]", best_path,best_cost,best_instance_sp, p_to_dest)
  
  #if svc = 0: only destination requested <skip further processing and return path to destination
  if svc == 0: return (p_to_dest,0,p_to_dest)
  
  if p_to_dest and best_instance_sp is None:
    log.warning("No Available Service: Try to Instantiate a new service Instance!")
    sw_dpid_list = [sw.dpid for sw,in_port,out_port in p_to_dest]
    res = disc.instantiate_new_instance_for_svc(svc_id = svc, sw_path_list=sw_dpid_list, cur_max_sp=disc.MAX_SHADOW_PRICE, new_service=True)
    if res is True:
      best_path,best_cost,best_instance_sp, p_to_dest = get_optimal_path_and_cost_to_svc(self, svc, dst_sw, last_port, src_port)
    
  if best_path and best_instance_sp >= disc.MAX_SP_PEAK_THRESHOLD:
    #log.error(" Time to Instantiate New Service Instance for Svc[%d]. best_instance_sp[%d]!",svc, best_instance_sp)
    #sw_dpid_list = [sw.dpid for sw,in_port,out_port in best_path]
    sw_dpid_list = [sw.dpid for sw,in_port,out_port in p_to_dest]
    res = disc.instantiate_new_instance_for_svc(svc_id = svc, sw_path_list=sw_dpid_list, cur_max_sp=best_instance_sp, new_service=False)
    log.warning("Result of on-Demand Instantiation of service[%d] is [%d]", svc,res)
    if res == -1:
      res = check_and_swap_any_service_instance_for_svc(svc_id=svc, cur_max_sp=best_instance_sp, sw_path_list=sw_dpid_list)
      log.warning("Result of Swap svc [%d] for any other svc Instance is [%d]", svc,res)
    # on success Can switch the path to this new instance
  return (best_path,best_cost,p_to_dest)

def install_path_to_svc_instance_for_udp(svc, vlan_instance_id, ttl, best_path, match, packet,p_to_dest=None):
  
  if not match:
    match = of.ofp_match(dl_type = 0x800)
  
  # Install Barrier message to avoid packet drop and sequential data processing
  wp = WaitingPath(best_path, packet, match, svc, vlan_instance_id, ttl)
  wp_msg = of.ofp_barrier_request()
  i = 0
  for sw,in_port,out_port in best_path:
    if sw is None:
      log.error("Incorrect Path! Cannot have None Switch in the path!!")
      return False
    if sw.connection is None:
      log.error("Broken Connection! Cannot have None for Switch connection!!")
      return False
    msg = of.ofp_flow_mod()
    flags = msg.flags
    msg.flags = of.OFPFF_SEND_FLOW_REM
    msg.cookie = vlan_instance_id
    msg.idle_timeout = FLOW_MINIMAL_TIMEOUT #FLOW_HARD_TIMEOUT
    #msg.hard_timeout = FLOW_HARD_TIMEOUT #0 hard_timeout=of.OFP_FLOW_PERMANENT
    
    msg.match = of.ofp_match()
    msg.match.in_port = in_port
    #msg.match.dl_vlan = svc
    
    msg.match.dl_type = match.dl_type #None # dl_type cannot be set to None if nw_src/nw_dst is needed for match. 
    msg.match.nw_proto = match.nw_proto
    
    #log.debug("At:[%s] Installing Rule for Service[%d] on Switch[%s:%d]-->[%d]", time.time(),svc,sw,in_port,out_port)
    if i == 0:
      i = 1
      
      msg.match.dl_src = match.dl_src
      msg.match.nw_src = match.nw_src
      msg.match.dl_dst = match.dl_dst #None < cannot ignore, its differntiator for redirection case  >
      msg.match.nw_dst = match.nw_dst #None < can ignore both dl_dst and nw_dst to reduce rule count, but we lose granularity of flow per service >
      
      msg.match.nw_tos = match.nw_tos
      msg.match.tp_src = match.tp_src
      msg.match.tp_dst = match.tp_dst
      
      
      #if match.dl_vlan:
      #  msg.match.dl_vlan = match.dl_vlan #svc # for test to differntiate flow coming from same src
      
      log.debug("MSG MATCH PARAMS: dl_src[%s], dl_dst[%s], dl_type[%s], nw_src[%s]", msg.match.dl_src, msg.match.dl_dst, msg.match.dl_type, msg.match.nw_src)
      add_install_route(svc_id=svc, path=best_path, match=msg.match,p_to_dest=p_to_dest)
      
      #To Tunnel based on L4 SRC PORT
      #msg.actions.append(of.ofp_action_tp_port(type=of.OFPAT_SET_TP_SRC, tp_port = vlan_instance_id))
      
      #To set PCP as index of service executed
      #msg.actions.append(of.ofp_action_set_vlan_pcp(vlan_pcp=ttl))
      #To Tunnel based on VLAN_ID
      msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = vlan_instance_id))
      
      if(in_port == out_port):
        msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
      else:
        msg.actions.append(of.ofp_action_output(port = out_port))
    else:
      msg.flags = flags #^= of.OFPFF_SEND_FLOW_REM
      msg.match.in_port = None
      
      #IF tunnel based on L4 SRC PORT
      #msg.match.tp_src  = vlan_instance_id
      
      #IF Tunnel based on VLAN ID
      msg.match.dl_vlan = vlan_instance_id
      
      #msg.match.dl_type = None # dl_type, dl_src, dl_dst cannot be set to None if nw_src/nw_dst is needed for match
      msg.match.dl_src = None
      msg.match.dl_dst = None
      msg.match.nw_src = None
      msg.match.nw_dst = None
      msg.match.nw_tos = None
      #msg.match.nw_proto = None
      msg.actions.append(of.ofp_action_output(port = out_port))
      
    sw.connection.send(msg) #core.openflow.sendToDPID(sw.dpid,msg)
    if wp :
      if not wp_msg: wp_msg = of.ofp_barrier_request()
      sw.connection.send(wp_msg)
      wp.add_xid(sw.dpid,wp_msg.xid)
    
    # Log packet size details
    disc.add_pkt_size_info(disc.PKT_LOG_TYPE_PKT_OUT, len(msg))
    
  return
  
def install_path_to_svc_instance_for_icmp(svc, vlan_instance_id, best_path, match, packet=None,p_to_dest=None):

  if not match:
    match = of.ofp_match(dl_type = 0x8100)
  
  # Install Barrier message to avoid packet drop and sequential data processing
  wp = WaitingPath(best_path, packet)
  wp_msg = of.ofp_barrier_request()
  i = 0
  for sw,in_port,out_port in best_path:
    if sw is None:
      log.error("Incorrect Path! Cannot have None Switch in the path!!")
      return False
    if sw.connection is None:
      log.error("Broken Connection! Cannot have None for Switch connection!!")
      return False
    
    msg = of.ofp_flow_mod()
    msg.flags = of.OFPFF_SEND_FLOW_REM

    
    msg.match = of.ofp_match()
    msg.match.dl_vlan = svc
    msg.match.in_port = in_port #in_port #None
    msg.idle_timeout = FLOW_HARD_TIMEOUT #FLOW_IDLE_TIMEOUT #0 
    #msg.hard_timeout = FLOW_HARD_TIMEOUT #0 hard_timeout=of.OFP_FLOW_PERMANENT
    #log.debug("At:[%s] Installing Rule for Service[%d] on Switch[%s:%d]-->[%d]", time.time(),svc,sw,in_port,out_port)
    if i == 0:
      i = 1
      msg.match.nw_src = match.nw_src #None
      msg.match.nw_dst = None
      msg.match.dl_type = match.dl_type #None # dl_type cannot be set to None if nw_src/nw_dst is needed for match. 
      #Refer: http://openvswitch.org/support/dist-docs/ovs-ofctl.8.txt or https://www.opennetworking.org/images/stories/downloads/sdn-resources/onf-specifications/openflow/openflow-spec-v1.0.1.pdf
      msg.match.dl_src = None #match.dl_src
      msg.match.dl_dst = None #match.dl_dst
      log.debug("MSG MATCH PARAMS: dl_src[%s], dl_dst[%s], dl_type[%s], nw_src[%s]", msg.match.dl_src, msg.match.dl_dst, msg.match.dl_type, msg.match.nw_src)
      add_install_route(svc_id=svc, path=best_path, match=msg.match,p_to_dest=p_to_dest)
      msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = vlan_instance_id))
      if(in_port == out_port):
        msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
        #rvlan_id = of.of.ofp_action_strip_vlan ()
        #cvlan_id = of.ofp_action_vlan_vid (vlan_vid = vlan_instance_id)
        #cout = of.ofp_action_output (port = of.OFPP_IN_PORT)
        #msg.actions = [cvlan_id, cout]
      else:
        msg.actions.append(of.ofp_action_output(port = out_port))
    else:
      msg.flags ^= of.OFPFF_SEND_FLOW_REM
      msg.match.dl_vlan = vlan_instance_id
      msg.match.in_port = None
      msg.match.nw_src = None
      msg.match.nw_dst = None
      msg.match.dl_type = None # dl_type, dl_src, dl_dst cannot be set to None if nw_src/nw_dst is needed for match
      msg.match.dl_src = None
      msg.match.dl_dst = None
      msg.actions.append(of.ofp_action_output(port = out_port))
      
    sw.connection.send(msg) #core.openflow.sendToDPID(sw.dpid,msg)
    disc.add_pkt_size_info(disc.PKT_LOG_TYPE_PKT_OUT, len(msg))

    if wp and wp_msg:
      sw.connection.send(wp_msg)
      wp.add_xid(sw.dpid,wp_msg.xid)
  
  return
  
def install_path_to_svc(svc, best_path, match, packet=None, path_cost=0, proto=ipv4.ICMP_PROTOCOL, ttl=0,p_to_dest=None):
  if not best_path:
    log.warning("No Optimal Path found!!, Install any path to Service[%d] or Flood! ", svc)
    return False
  
   
  log.warning("At:[%s] Installing Path for Service[%d] with Cost[%d] -->[%s]", time.time(),svc, path_cost, best_path)
  
  svc_switch =          best_path[-1][0]
  svc_switch_out_port = best_path[-1][2]
  svc_switch_dpid     = svc_switch.dpid
  svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=svc_switch_dpid, port_id=svc_switch_out_port)
  byte_mac_str =  str(svc_mac_addr)
  byte_mac = int(byte_mac_str[byte_mac_str.rfind(':')+1:])
  byte_mac = byte_mac << 4
  vlan_instance_id = ((svc & 0xF) | (byte_mac & 0xFF0)) & 0xFFF
  log.info("svc=0x%x byte_mac_str[%s], byte_mac[%d], vlan_instance_id [0x%x]", svc,byte_mac_str,byte_mac,vlan_instance_id)
  best_out_port = best_path[0][2]
  log.debug("From[%s:%d] Service:[%d] has the Best Path:%s",svc_switch,svc_switch_out_port,svc,best_path)
  
  #Update DST L2 address and L3 addresses
  #n_match = match
  #n_match.dl_dst = EthAddr(svc_mac_addr)
  #n_match.nw_dst = svc_ip_addr <How to get the IP address based on L2 address in controller>
  
  if proto == ipv4.ICMP_PROTOCOL:
    install_path_to_svc_instance_for_icmp(svc, vlan_instance_id, best_path, match, packet)
  elif proto == ipv4.UDP_PROTOCOL or proto == ipv4.TCP_PROTOCOL:
    install_path_to_svc_instance_for_udp(svc, vlan_instance_id, ttl, best_path, match, packet,p_to_dest)
  else:
    log.error("Got packet with incorrect proto [%d]!!", proto)
    return False
  add_to_placement_table(svc, best_path, path_cost, path_to_dest=p_to_dest,proto=proto, ttl=ttl)
  return True

def get_path_and_cost_to_svc_node(svc, src_sw,src_port, svc_node, p_to_dest):
  
  if p_to_dest is None:
    log.error("Cannot compute path cost to svc without Destination path!")
    return (None,None)

  c_path = None
  c_cost = None
  mac = svc_node[1]
  sp  = svc_node[2]
  log.info("MAC=[%s], SP=[%s], SVC_NODE[%s]", mac,sp, svc_node)
  hop_to_dst = 1
  dst_out_port = 0
  if p_to_dest:
    hop_to_dst = len(p_to_dest)
    dst_out_port = p_to_dest[0][2]
  dest_mac = EthAddr(mac)
  if( dest_mac and dest_mac in mac_map):
    dest_addr = mac_map[dest_mac]
    c_path = _get_path(src_sw, dest_addr[0], src_port, dest_addr[1])
    p_svc_to_dst = _get_path(dest_addr[0], p_to_dest[-1][0], dest_addr[1], p_to_dest[-1][2])
    if c_path:
      svc_flow_count = get_flow_count_for_path(path=c_path,svc_mac_addr=dest_mac,svc_id=svc)
      svc_hop_count = len(c_path)
      svc_out_port =  c_path[0][2] #for sw,in_port,out_port in c_path:
      
      #log.debug("For svc[%d] at Mac:%s: with SP:%s:: Path is:[%s] with [HopCount:%d, FlowCount:%d] ", svc, mac,sp, c_path,svc_hop_count,svc_flow_count)
      #log.debug("List of len:%d is:%s with best_instance_sp:%d", len(hop_count_list), hop_count_list, best_instance_sp)
      penalty_factor = get_penalty_factor_2(src_sw,out_port_dst=dst_out_port, hop_count_dst=hop_to_dst, out_port_svc=svc_out_port, hop_count_svc=svc_hop_count, hop_count_dst_frm_svc=len(p_svc_to_dst) )
      c_cost = compute_path_cost_2(src_sw,hop_count_dst = hop_to_dst, shadow_price=int(sp),hop_count_svc = svc_hop_count, penalty_factor=penalty_factor)
      
      #penalty_factor = get_penalty_factor(src_sw,out_port_dst=dst_out_port, hop_count_dst=hop_to_dst, out_port_svc=svc_out_port, hop_count_svc=svc_hop_count )
      #c_cost = compute_path_cost(src_sw,hop_count_dst = hop_to_dst, shadow_price=int(sp),hop_count_svc = svc_hop_count, penalty_factor=penalty_factor)
      #c_cost = svc_flow_count
  return(c_path,c_cost)
  
def find_and_install_optimal_path_to_svc(self, svc, dst_sw, last_port, match, src_port,event=None, proto=ipv4.ICMP_PROTOCOL, ttl=0):
  best_path,best_cost,p_to_dest = get_optimal_path(self, svc, dst_sw, last_port, match, src_port)
  packet=None
  if event and event.ofp:
     packet = event.ofp
  ret = install_path_to_svc(svc, best_path, match, packet,best_cost, proto,ttl,p_to_dest)
  return best_path, best_cost
  
def find_and_install_optimal_path_to_dst(self, vlan_id, dst_sw, last_port, match, event,proto=ipv4.ICMP_PROTOCOL, ttl=0):
  sChain = verify_and_get_svc_chain_data(event)
  if not sChain:
    log.warning("Failed to retrieve the Service Chain! Just install path for vlan_id:%d",vlan_id)
    find_and_install_optimal_path_to_svc(self, vlan_id, dst_sw, last_port, match, event.port,event,proto,ttl)
    return
  #if len(sChain) == 1:
  #  log.warning("Single SVCID in Service Chain:%d",vlan_id)
  #return
  
  log.debug("<start> Installing Path End 2 End for SCHAIN[%s]", sChain)
  src_sw = self
  src_port = event.port
  evt = event
  for svc_id in sChain:
    log.debug("<in_chain> Installing path for SVC_ID:[%d] from SW [%d:%d] ",svc_id, src_sw.dpid, event.port )
    installed_path, path_cost = find_and_install_optimal_path_to_svc(src_sw, svc_id, dst_sw, last_port, match, src_port,evt,proto,ttl)
    log.debug("<in_chain> Installed path for SVC_ID:[%d] from SW [%d:%d] as [%s] with cost[%d]",svc_id, src_sw.dpid, src_port, installed_path, path_cost )
    if not installed_path:
      log.error("Failed to find optimal path to svc[%d] from [%d:%d]", svc_id, src_sw.dpid, src_port)
      break
    src_sw = installed_path[-1][0]
    src_port = installed_path[-1][2]
    evt = None
  log.debug("<end> Installing Path End 2 End for SCHAIN[%s]", sChain)
  return

def find_and_install_optimal_path_to_dst_2(self, vlan_id, dst_sw, last_port, match, event,proto=ipv4.ICMP_PROTOCOL, ttl=0):
  sChain = verify_and_get_svc_chain_data(event)
  if not sChain:
    log.warning("Failed to retrieve the Service Chain! Just install path for vlan_id:%d",vlan_id)
    find_and_install_optimal_path_to_svc(self, vlan_id, dst_sw, last_port, match, event.port,event,proto,ttl)
    return
  
  if len(sChain) == 1:
    log.warning("Only 1 instance in Service Chain! Just install path for vlan_id:%d",vlan_id)
    find_and_install_optimal_path_to_svc(self, vlan_id, dst_sw, last_port, match, event.port,event,proto,ttl)
    return
  
  log.info("<start> Installing Path End 2 End for SCHAIN[%s]", sChain)
  #First: Construct the possible_path_list
  import itertools
  possible_path_list = []
  for x in range(len(sChain)):
    svc_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(sChain[x])
    if svc_list is None or len(svc_list) == 0:
      log.warning("No instances for current svc [%d]", sChain[x])
      get_optimal_path(self, sChain[x], dst_sw, last_port, match, event.port)
      svc_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(sChain[x])
      if svc_list is None or len(svc_list) == 0:
        log.error("Failed to find any instance for current svc [%d]", sChain[x])
        return
    if x == 0:
      possible_path_list = svc_list
      #possible_path_list.append([svc_list])
    else:
      possible_path_list = list(itertools.product(possible_path_list,svc_list))
  log.info("Total Possible Paths Found = [%d]", len(possible_path_list))
  if len(possible_path_list) == 0:
    log.error("Failed to Find any possible path!!")
    return
    
  log.info("Total Possible Paths Found = [%d]", len(possible_path_list))
  for x in range(len(possible_path_list)):
    log.info("Possible Path[%d]:[%s]", x, possible_path_list[x])
    if len(possible_path_list[x]) != len(sChain):
      log.warning("Error in Possible Path[%d]:[%s], expected path len [%d]", x, possible_path_list[x], len(sChain))
  
  # Also compute path to Destination
  p_to_dest = _get_path(self, dst_sw, event.port, last_port)
  if p_to_dest is None:
    log.error("Failed to find path to Destination from [%s:%d to %s:%d]",self,event.port,dst_sw,last_port)
    return

  #Second: Compute Path Cost Matrix for each of the computed possible_path_list
  path_cost_matrix = [[-1,None]]*len(possible_path_list)
  for index, candidate_path in enumerate(possible_path_list):
    path_cost = 0
    path = []
    ip_cost = []
    src_sw = self
    src_port = event.port
    for svc_index, svc_node in enumerate(candidate_path):
      log.warning("Trying to Find the Path to Svc Node [%d:%s] from SRC[%s:%d]!!", sChain[svc_index],svc_node, src_sw,src_port)
      path_to_svc, cost_to_svc = get_path_and_cost_to_svc_node(sChain[svc_index],src_sw,src_port,svc_node, p_to_dest)
      if path_to_svc is None:
        log.error("Failed to Find the Path to Svc Node [%d:%s] from SRC[%s:%d]!!", sChain[svc_index],svc_node, src_sw,src_port)
        break
      log.debug("For Svc[%d] Computed cost[%d] and Path to SVC [%s]", sChain[svc_index], cost_to_svc,path_to_svc)
      path_cost += cost_to_svc
      path += [path_to_svc]
      ip_cost += [cost_to_svc]
      src_sw = path_to_svc[-1][0]
      src_port = path_to_svc[-1][2]
      #log.warning("Till Svc[%d] Computed cost[%d] and Path to SVC [%s]", sChain[svc_index], path_cost,path)

    path_cost_matrix[index] = [path_cost,path, ip_cost]
    log.info("For Candidate Instances [%d:%s], Total Path cost [%d] for computed path [%s] len(%d)", index, candidate_path,path_cost,path, len(path))
  
  for x in range(len(path_cost_matrix)):
    log.debug("Possible Path[%d]:[%s]", x, path_cost_matrix[x])
    
  # Third: Get Optimal Chain 
  optimal_chain_path = min(path_cost_matrix, key=lambda t:t[0])
  optimal_path_index = path_cost_matrix.index(optimal_chain_path)
  optimal_path_cost = optimal_chain_path[0]
  optimal_paths = optimal_chain_path[1]
  optimal_paths_cost = optimal_chain_path[2]
  if len(optimal_paths) != len(sChain):
    log.error("Failed to compute Full Path of Chain!! lenghts differ [%d,%d]",len(optimal_paths), len(sChain) )
    return
  
  # Finally: Install Path for the entire chain in one go
  packet = event.ofp
  for index, svc_id in enumerate(sChain):
    path = optimal_paths[index]
    cost = optimal_paths_cost[index]
    src_sw = path[0][0]
    src_port = path[0][1]
    log.debug("<in_chain> Installing path for SVC_ID:[%d] from SW [%d:%d] with cost[%d]",svc_id, src_sw.dpid, src_port, cost )
    install_path_to_svc(svc_id, path, match, packet, cost,p_to_dest=p_to_dest)
    #src_sw = installed_path[-1][0]
    #src_port = installed_path[-1][2]
    packet = None
  
  log.debug("<end> Installing Path End 2 End for SCHAIN[%s]", sChain)
  return


def install_path_for_local_svc(self, vlan_id, dst_sw, last_port, match, in_port, event, proto, ttl):
    
    #if proto != ipv4.UDP_PROTOCOL: return
    
    if vlan_id is None: vlan_id = 0
    
    #vlan_instance_id = vlan_id | self.dpid
    
    msg = of.ofp_flow_mod()
    msg.flags = of.OFPFF_SEND_FLOW_REM
    msg.cookie = vlan_id
    msg.idle_timeout = FLOW_MINIMAL_TIMEOUT 
    
    msg.match = match
    msg.match.in_port = in_port
    
    msg.match.dl_type = match.dl_type
    msg.match.dl_src = match.dl_src
    #msg.match.tp_src = match.tp_src
    
    
    #vlan_id is not zero => is a middlebox
    if vlan_id >= 0:
        vlan_instance_id = match.tp_dst + 1 #ttl
        msg.actions.append(of.ofp_action_strip_vlan())
        #msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = vlan_id))
        ##msg.actions.append(of.ofp_action_dl_addr(type=of.OFPAT_SET_DL_SRC))
        msg.actions.append(of.ofp_action_tp_port(type=of.OFPAT_SET_TP_DST, tp_port = vlan_instance_id))
        msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
        #msg.actions.append(of.ofp_action_output(port = of.OFPP_ALL))
        #msg.actions.append(of.ofp_action_output(port = of.OFPP_LOCAL))
    
    #vlan id is 0 => it is a destination node
    else:
        #vlan_instance_id = match.tp_src - match.dl_vlan_pcp
        #msg.actions.append(of.ofp_action_tp_port(type=of.OFPAT_SET_TP_SRC, tp_port = vlan_instance_id))
        
        #drop => No action set or OFPP_LOCAL => let local n/w stack handle the packet
        #msg.actions.append(of.ofp_action_output(port = of.OFPP_LOCAL))
        pass

    #Install the Rule on switch
    if self.connection:
        log.warning("Installing path on local switch,port [%s:%s]  with match [%s] actions=[%s]", self.dpid, in_port, msg.match, msg.actions )
        self.connection.send(msg)
    else:
        log.warning("No Connection! Failed to Installing path on local switch,port [%s:%s]  with match [%s] outport=[%s]", self.dpid, in_port, msg.match, vlan_instance_id )
    return

class Switch (EventMixin):
  def __init__ (self):
    self.connection = None
    self.ports = None
    self.dpid = None
    self._listeners = None
    self._connected_at = None

  def __repr__ (self):
    return dpid_to_str(self.dpid)

  def _install (self, switch, in_port, out_port, match, buf = None,log_path=False):
    msg = of.ofp_flow_mod()
    msg.match = match
    msg.match.in_port = in_port
    if match.nw_proto == ipv4.TCP_PROTOCOL:
        log.warning("setting TOS field [%d] match", match.nw_tos)
        msg.match.nw_tos = match.nw_tos
    
    msg.idle_timeout = FLOW_IDLE_TIMEOUT
    if log_path: msg.flags = of.OFPFF_SEND_FLOW_REM
    #msg.hard_timeout = FLOW_HARD_TIMEOUT #hard_timeout=of.OFP_FLOW_PERMANENT
    #if(in_port == out_port):
    #  log.warning(" Common Path setup!! Dst path is same as src! instance specifically for in_port!")
    #  msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
    #else:
    msg.actions.append(of.ofp_action_output(port = out_port))
    #msg.actions.append(of.ofp_action_output(port = out_port))
    msg.buffer_id = buf
    #msg.flags=of.OFPFF_SEND_FLOW_REM
    if switch is None or switch.connection is None:
      log.warning("Failed to Install path due to invalid switch connection!!")
      return
    switch.connection.send(msg)

  def _install_path (self, p, match, packet_in=None, log_path=False):
    wp = WaitingPath(p, packet_in)
    #log.info ("Installing Path for switch[%s] as [%s]", self,p)
    for sw,in_port,out_port in p:
      if sw is None or sw.connection is None: continue
      self._install(sw, in_port, out_port, match, log_path=log_path)
      msg = of.ofp_barrier_request()
      sw.connection.send(msg)
      wp.add_xid(sw.dpid,msg.xid)
      if log_path: log_path=False

  def install_path (self, dst_sw, last_port, match, event, rev_path=True, log_path=False):
    """
    Attempts to install a path between this switch and some destination
    """
    import pox.lib.packet as pkt
    
    p = _get_path(self, dst_sw, event.port, last_port)
    if p is None:
      log.error("Can't get from %s to %s", match.dl_src, match.dl_dst)

      if (match.dl_type == pkt.ethernet.IP_TYPE and
          event.parsed.find('ipv4')):
        # It's IP -- let's send a destination unreachable
        log.debug("Dest unreachable (%s -> %s)",
                  match.dl_src, match.dl_dst)

        from pox.lib.addresses import EthAddr
        e = pkt.ethernet()
        e.src = EthAddr(dpid_to_str(self.dpid)) #FIXME: Hmm...
        e.dst = match.dl_src
        e.type = e.IP_TYPE
        ipp = pkt.ipv4()
        ipp.protocol = ipp.ICMP_PROTOCOL
        ipp.srcip = match.nw_dst #FIXME: Ridiculous
        ipp.dstip = match.nw_src
        icmp = pkt.icmp()
        icmp.type = pkt.ICMP.TYPE_DEST_UNREACH
        icmp.code = pkt.ICMP.CODE_UNREACH_HOST
        orig_ip = event.parsed.find('ipv4')

        d = orig_ip.pack()
        d = d[:orig_ip.hl * 4 + 8]
        import struct
        d = struct.pack("!HH", 0,0) + d #FIXME: MTU
        icmp.payload = d
        ipp.payload = icmp
        e.payload = ipp
        msg = of.ofp_packet_out()
        msg.actions.append(of.ofp_action_output(port = event.port))
        msg.data = e.pack()
        self.connection.send(msg)

      return

    #log.warning("Installing path for %s -> %s %04x np:%04x (%i hops)", match.dl_src, match.dl_dst, match.dl_type, match.nw_proto, len(p))
    
    self._install_path(p, match, event.ofp, log_path)

    # To add the entires for last flow from last service to destination
    if rev_path is False and log_path is True:
      org_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=p[0][0].dpid, port_id=p[0][1])
      svc_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=p[-1][0].dpid, port_id=p[-1][2])
      fmap_sk.add_flow_to_flow_info_table(match=match, path=p, event=None, svc_id=0, svc_mac_addr=svc_mac_addr, src_mac_addr=org_mac_addr,dst_mac_addr=svc_mac_addr,p_to_dest=p,dst_path_len=len(p))
    if log_path:
        #add_hopcount_for_installed_route(svc_id=0, hop_count=len(p),path=p, redirected=False)
        pass
    
    if match.nw_proto == ipv4.TCP_PROTOCOL: rev_path = False
    # Now reverse it and install it backwards
    # (we'll just assume that will work)
    if rev_path:
        p = [(sw,out_port,in_port) for sw,in_port,out_port in p]
        self._install_path(p, match.flip())

  
  def install_path_sk (self, vlan_id, dst_sw, last_port, match, event,proto=ipv4.ICMP_PROTOCOL, ttl=0):
    """
    Check and Alter the Path to orient towards 'svc' with Best Shadow Price;
    """
    log.debug(" install_path_sk::Start Time: %s", time.time())
    svc = (vlan_id & 0xF)
    #log.info("install_path_sk for switch[%d:%d] svc:%d, Start Time: %s",self.dpid,event.port,svc,time.time())
    #if vlan_id != svc or False == core.openflow_discovery.is_edge_port(self.dpid,event.port):
    if False == core.openflow_discovery.is_edge_port(self.dpid,event.port):
      log.warning("Received packet_in with vlan[%d] for svc[%d] at intermediate switch [%d:%d]", vlan_id,svc,event.connection.dpid,event.port )
      #Silently discard this packet <TRY and CHECK>
      return
    find_and_install_optimal_path_to_svc(self, vlan_id, dst_sw, last_port, match, event.port, event, proto, ttl)
    #find_and_install_optimal_path_to_dst(self, vlan_id, dst_sw, last_port, match, event, proto, ttl)
    #find_and_install_optimal_path_to_dst_2(self, vlan_id, dst_sw, last_port, match, event, proto, ttl)
    log.debug(" install_path_sk::End Time: %s", time.time())
    return
  def install_path_sk_local (self, vlan_id, dst_sw, last_port, match, event,proto=ipv4.ICMP_PROTOCOL, ttl=0):
    install_path_for_local_svc(self, vlan_id, dst_sw, last_port, match, event.port, event, proto, ttl)
    #log.debug(" install_path_sk_local::End Time: %s", time.time())
    return

  def _handle_PacketIn (self, event):
    
    def drop ():
      # Kill the buffer
      if event.ofp.buffer_id is not None:
        msg = of.ofp_packet_out()
        msg.buffer_id = event.ofp.buffer_id
        event.ofp.buffer_id = None # Mark is dead
        msg.in_port = event.port
        self.connection.send(msg)
    
    def flood ():
      """ Floods the packet """
      if self.is_holding_down:
        #log.warning("Not flooding -- holddown active")
        log.info("Not flooding -- holddown active")
        return
        #return drop()
      #return drop()
      msg = of.ofp_packet_out()
      # OFPP_FLOOD is optional; some switches may need OFPP_ALL
      msg.actions.append(of.ofp_action_output(port = of.OFPP_FLOOD))
      msg.buffer_id = event.ofp.buffer_id
      msg.in_port = event.port
      self.connection.send(msg)
    
    packet = event.parsed
    match = of.ofp_match.from_packet(packet)
    
    
    if fmap_sk.check_flow_in_flow_info_table(match) is True:
      #log.warning("Flow already in flow table! skip processing")
      return
    
    #log.error(" Packet_in at switch[%d] of size [%s]", self.dpid, len(packet.pack()))
    #disc.add_pkt_size_info(disc.PKT_LOG_TYPE_PKT_IN, len(packet.pack()))
    
    loc = (self, event.port) # Place we saw this ethaddr
    oldloc = mac_map.get(packet.src) # Place we last saw this ethaddr
    #log.info("loc:%s, oldloc:%s", loc, oldloc)
    #log.warning("pcket.src:%s, oldloc:%s, ", packet.src, oldloc)
    
    if packet.effective_ethertype == packet.LLDP_TYPE:
      drop()
      return
    if packet.effective_ethertype  == 0x86dd: # IPV6 Packets
       drop()
       return
    
    
    proto = 0
    proto = get_ipp_packet_protocol(event)
    
    #Check for IP/MAC Advertisement messages and update accordingly
    if proto == ipv4.ICMP_PROTOCOL and is_pkt_icmp_src_quench_type(packet):
      if oldloc is None or oldloc != loc:
        #pass
        mac_map[packet.src] = loc
        export_learnt_address_map_from_packet_in(mac_addr=packet.src, sw_id=loc[0].dpid, port_id=loc[1], match=match)
        log.warning("Learned SRC0 %s at %s.%i", packet.src, loc[0], loc[1])
        drop()
      return
      
    #Based on used protocol decode vlan and setup path
    vlan_id = None
    ttl = 0
    tos = 0
    if proto == ipv4.UDP_PROTOCOL or proto == ipv4.TCP_PROTOCOL:
      flag,tos,ttl = check_valid_udp_packet(event)
      if tos ==0: flag = False
      if flag:
        vlan_id = get_vlan_id_from_ip_tos_and_ttl(tos,ttl)
        ttl = 1
    elif proto == ipv4.ICMP_PROTOCOL:
      vlan_id = _get_vlan_id(event)
    
    
    def setup_path_sk_local(proto=ipv4.UDP_PROTOCOL, ttl=0):
        return self.install_path_sk_local(vlan_id, None, 0, match, event, proto, ttl)
        
    def setup_path_sk(proto=ipv4.ICMP_PROTOCOL, ttl=0):
      if packet.dst.is_multicast:
        log.warning("Flood!! setup_path_sk(svc=%d)::Dst[%s] is multicast!", vlan_id, packet.dst)
        return flood()
      if packet.dst not in mac_map:
        log.warning(" setup_path_sk::Dst[%s] is not found in mac_map!! Drop packet for service[%s]", vlan_id, packet.dst)
        return flood()
      
      return self.install_path_sk_local(vlan_id, None, 0, match, event, proto, ttl)
      
      #dest = mac_map[packet.dst]
      #match = of.ofp_match.from_packet(packet)
      if vlan_id > 0:
        # Installing path to Svc node:
        self.install_path_sk(vlan_id, dest[0], dest[1], match, event, proto, ttl)
      else:
        # Installing path to Dst node:
        log.info("Install path to Dst %s:%s", dest[0], dest[1])
        self.install_path(dest[0], dest[1], match, event, rev_path=False, log_path=False)
      return

    if vlan_id is not None and vlan_id != 0 and vlan_id != 65535:
      return setup_path_sk_local(proto,ttl)
    elif vlan_id is not None and vlan_id == 0 and (proto == ipv4.UDP_PROTOCOL or proto == ipv4.TCP_PROTOCOL):
      return setup_path_sk_local(proto,ttl)
    
    if oldloc is None:
      if packet.src.is_multicast == False and packet.src != EthAddr("00:00:00:00:00:00"):
        mac_map[packet.src] = loc # Learn position for ethaddr
        #Place to learn the IP address as well <TODO>
        #export_learnt_address_map_from_packet_in(mac_addr=packet.src, sw_id=loc[0].dpid, port_id=loc[1], match=match)
        log.warning("Learned SRC1 %s at %s.%i", packet.src, loc[0], loc[1])
        
    elif oldloc != loc:
      # Precarious! Handle Updates with caution!!
      # ethaddr seen at different place!
      if core.openflow_discovery.is_edge_port(loc[0].dpid, loc[1]):
        # New place is another "plain" port (probably)
        log.warning("%s moved from %s.%i to %s.%i?", packet.src,
                  dpid_to_str(oldloc[0].dpid), oldloc[1],
                  dpid_to_str(   loc[0].dpid),    loc[1])
        if packet.src.is_multicast == False and packet.src != EthAddr("00:00:00:00:00:00"):
          mac_map[packet.src] = loc # Learn position for ethaddr
          #Place to learn the IP address as well <TODO>
          export_learnt_address_map_from_packet_in(mac_addr=packet.src, sw_id=loc[0].dpid, port_id=loc[1], match=match)
          log.warning("Learned SRC2 %s at %s.%i", packet.src, loc[0], loc[1])
      elif packet.dst.is_multicast == False:
        # New place is a switch-to-switch port!
        # Hopefully, this is a packet we're flooding because we didn't
        # know the destination, and not because it's somehow not on a
        # path that we expect it to be on.
        # If spanning_tree is running, we might check that this port is
        # on the spanning tree (it should be).
        if packet.dst in mac_map:
          # Unfortunately, we know the destination.  It's possible that
          # we learned it while it was in flight, but it's also possible
          # that something has gone wrong.
          log.warning("Packet from %s to known destination %s arrived "
                      "at %s.%i without flow", packet.src, packet.dst,
                      dpid_to_str(self.dpid), event.port)


    if packet.dst.is_multicast:
      log.debug("Flood DST multicast %s from %s", packet.dst, packet.src)
      flood()
    else:
      if packet.dst not in mac_map:
        log.warning("%s unknown -- flooding" % (packet.dst,))
        flood()
      else:
        dest = mac_map[packet.dst]
        match = of.ofp_match.from_packet(packet)
        self.install_path(dest[0], dest[1], match, event)

  def disconnect (self):
    if self.connection is not None:
      log.error("Switch Disconnect %s" % (self.connection,))
      self.connection.removeListeners(self._listeners)
      self.connection = None
      self._listeners = None

  def connect (self, connection):
    if self.dpid is None:
      self.dpid = connection.dpid
    assert self.dpid == connection.dpid
    if self.ports is None:
      self.ports = connection.features.ports
    self.disconnect()
    log.debug("Switch Connect %s" % (connection,))
    self.connection = connection
    self._listeners = self.listenTo(connection)
    self._connected_at = time.time()

  @property
  def is_holding_down (self):
    if self._connected_at is None: return True
    if time.time() - self._connected_at > FLOOD_HOLDDOWN:
      return False
    return True

  def _handle_ConnectionDown (self, event):
    self.disconnect()

def get_sw_from_dpid(dpid=0):
  global switches
  sw = switches.get(dpid)
  if sw is None:
    #log.warning("DPID:%d has no switch info in the Dictionary[%s]!!", dpid, switches)
    pass
  return sw

def get_mac_map(host_eth_addr):
  if host_eth_addr in mac_map:
    return mac_map[host_eth_addr]
  return None

def export_learnt_address_map_from_packet_in(mac_addr, sw_id, port_id, match):
    if match and match.nw_src:
        #log.error("setting the packet with L3 Information!!")
        #edge_link = Discovery.Link(match.nw_src,mac_addr,sw_id,port_id)
        #disc.add_link_info_to_link_dict(edge_link)
        #nwrt_sk.add_or_del_link(type=1, node1_id=match.nw_src, node1_port=mac_addr, node2_id=sw_id, node2_port=port_id, status=1)
        return util_sk.add_to_address_map_table(ip_addr=match.nw_src,mac_addr=mac_addr, sw_id=sw_id, port_id=port_id)
    else: pass
        #log.error("Ignore the packet without L3 Information [%s]!!", match)
    return None
def handle_link_up_down_for_network_route(dpid1,port1,dpid2,port2,is_removed):
    nwrt_sk.add_or_del_link(type=0, node1_id=dpid1, node1_port=port1, node2_id=dpid2, node2_port=port2, status=not is_removed)
def update_all_file_paths():
  global hops_file, hops_file2, flog_file, rdir_file,related_flow_info_file
  global base_dir, hdir_p, rdir_p
  base_dir = util_sk.get_base_dir()
  rdir_p = util_sk.get_results_dir()
  hops_file = os.path.join(rdir_p, "hopcount_log.csv")
  hops_file2 = os.path.join(rdir_p, "hopcount2_log.csv")
  flog_file = os.path.join(rdir_p, "flog.csv")
  rdir_file = os.path.join(rdir_p,"rdir_log.csv")
  related_flow_info_file = os.path.join(rdir_p,"related_flows_info.csv")
  return
class l2_multi (EventMixin):

  _eventMixin_events = set([
    PathInstalled,
  ])
  
  def __init__ (self):
    
    # Listen to dependencies
    def startup ():
      core.openflow.addListeners(self, priority=0)
      core.openflow_discovery.addListeners(self)
      # setup path and files info
      update_all_file_paths()
      
      #Timer(fupdate_interval, _svc_flog_function, recurring=True)
      
      core.openflow.addListenerByName("FlowRemoved", _handle_FlowRemoved,priority=65000)
      #core.openflow.addListenerByName("PortStatus", _handle_openflow_PortStatus)
      return
      
    core.call_when_ready(startup, ('openflow','openflow_discovery','drench_util'))
    return

  def _handle_LinkEvent (self, event):
    def flip (link):
      return Discovery.Link(link[2],link[3], link[0],link[1])

    l = event.link
    sw1 = switches[l.dpid1]
    sw2 = switches[l.dpid2]

    # Bypass for host links converted to vswitch link
    if event.added:
      bad_macs = set()
      for mac,(sw,port) in mac_map.iteritems():
        if sw is sw1 and port == l.port1: bad_macs.add(mac)
        if sw is sw2 and port == l.port2: bad_macs.add(mac)
      for mac in bad_macs:
        log.warning("skip Unlearned %s", mac)
        core.openflow_discovery._discard_link(l)
        return
      #  del mac_map[mac]
      # < Commenting out bad mac learning >
    
    
    
    
    # Invalidate all flows and path info.
    # For link adds, this makes sure that if a new link leads to an
    # improved path, we use it.
    # For link removals, this makes sure that we don't use a
    # path that may have been broken.
    #NOTE: This could be radically improved! (e.g., not *ALL* paths break)
    clear = of.ofp_flow_mod(command=of.OFPFC_DELETE)
    for sw in switches.itervalues():
      if sw is None or sw.connection is None: continue
      #log.debug("Flows Cleared for Switch %s!!", sw.dpid)
      log.info("Flows Cleared for Switch %s!!", sw.dpid)
      sw.connection.send(clear)
    #Apr 7, 2016, commenting out the path_map.clear() to ensure earlier paths are not disrupted!
    path_map.clear()

    if event.removed:
      # This link no longer okay
      if sw2 in adjacency[sw1]: del adjacency[sw1][sw2]
      if sw1 in adjacency[sw2]: del adjacency[sw2][sw1]

      # But maybe there's another way to connect these...
      for ll in core.openflow_discovery.adjacency:
        if ll.dpid1 == l.dpid1 and ll.dpid2 == l.dpid2:
          if flip(ll) in core.openflow_discovery.adjacency:
            # Yup, link goes both ways
            adjacency[sw1][sw2] = ll.port1
            adjacency[sw2][sw1] = ll.port2
            # Fixed -- new link chosen to connect these
            break
    else:
      # If we already consider these nodes connected, we can
      # ignore this link up.
      # Otherwise, we might be interested...
      if adjacency[sw1][sw2] is None:
        # These previously weren't connected.  If the link
        # exists in both directions, we consider them connected now.
        if flip(l) in core.openflow_discovery.adjacency:
          # Yup, link goes both ways -- connected!
          adjacency[sw1][sw2] = l.port1
          adjacency[sw2][sw1] = l.port2

      # If we have learned a MAC on this port which we now know to
      # be connected to a switch, unlearn it.
      #Commenting out Unlearn MAC <next Seven lines>
      #bad_macs = set()
      #for mac,(sw,port) in mac_map.iteritems():
      #  if sw is sw1 and port == l.port1: bad_macs.add(mac)
      #  if sw is sw2 and port == l.port2: bad_macs.add(mac)
      #for mac in bad_macs:
      #  log.warning("Unlearned %s", mac)
      #  del mac_map[mac]
      # < Commenting out bad mac learning >
      
    #Add/Remove the link to the NWRouteHelper
    handle_link_up_down_for_network_route(l.dpid1, l.port1, l.dpid2, l.port2,event.removed)
    
    return
    
  def _handle_ConnectionUp (self, event):
    sw = switches.get(event.dpid)
    if sw is None:
      # New switch
      sw = Switch()
      switches[event.dpid] = sw
      sw.connect(event.connection)
    else:
      sw.connect(event.connection)

  def _handle_BarrierIn (self, event):
    wp = waiting_paths.pop((event.dpid,event.xid), None)
    if not wp:
      log.warning("No waiting packet %s,%s", event.dpid, event.xid)
      return
    #log.debug("Notify waiting packet %s,%s", event.dpid, event.xid)
    wp.notify(event)

def check_and_update_flow_count_info_for_all_svc():
    if fmap_sk.get_total_flow_entries_count(): return False
    
    svc_dict = disc.get_svc_dict() # core.openflow_discovery.getFunctionList_sk()
    for svc_id, value in svc_dict.iteritems():
        for instance in value:
            if int(instance[0]) > 0 and int(instance[0]) < disc.MAX_SHADOW_PRICE:
                log.warning("_svc_flog_function(): Explicitly Setting SP to Zero for svc[%d] at [%s]", svc_id, instance[1])
                #update_sp_on_flow_add_or_del(svc_id=svc_id,svc_mac_addr=instance[1], path=None, addition= False, match=None)
                disc.update_shadow_price_2(mac_addr=instance[1],svc_id=svc_id,shadow_price= 0)
    return True


def log_flows_hop_count(cur_time=0):
  
  active_flow_count = fmap_sk.get_total_flow_entries_count() 
  data = ""
  avg_count = 0
  overall_avg = 0
  items_count=0
  #if cur_time == 0: cur_time = time.time()
  for key,value in hop_count_dict.iteritems():
    avg_count = value[1]/float(max(1,value[0]))
    overall_avg += avg_count
    items_count += 1
    data += str(key) + ':' + str(avg_count) + '-' + str(value[0]) + '-' + str(value[1]) + ','
  
  overall_avg /=float(max(1,items_count))
  data.rstrip(',')
  with open(hops_file, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    data = data.rstrip(',')
    #logWriter.writerows([[int(cur_time), str(overall_avg), str(data), str(active_flow_count), str(dbg_flow_counter) ]])
    logWriter.writerows([[int(cur_time), str(active_flow_count), str(overall_avg)]])
  #return
  

  data = ""
  total_flows = 0
  total_hop_count= 0
  overall_avg = 0
  if cur_time == 0: cur_time = time.time()
  for key,vals in hop_count_dict2.iteritems():
    svc_id,src_sw_dpid,src_sw_port = key[0],key[1],key[2]
    for val in vals:
      hop_ct,dst_sw_dpid,dst_sw_port = val[0],val[1],val[2]
      #data += str(key) + '-' + str(val[1]) + '-' + str(val[2]) + ':' + str(val[0]) + ','
      #k1 = (src_sw_dpid,src_sw_port,dst_sw_dpid,dst_sw_port)
      #data += '{0:3d}'.format(svc_id) + '-' + '{0:3d}'.format(src_sw_dpid) + '-' + '{0:3d}'.format(src_sw_port) + '-' + '{0:3d}'.format(dst_sw_dpid) + '-' + '{0:3d}'.format(dst_sw_port) + ':' + str(hop_ct) + ','
      
      total_flows +=1
      total_hop_count += hop_ct
      data += str(svc_id) + '@' + str(src_sw_dpid) + ':' + str(src_sw_port) + '-' + str(dst_sw_dpid) + ':' + str(dst_sw_port) + '=' + str(hop_ct) + ','
  
  #hop_count_dict2.clear()
  overall_avg = total_hop_count/float(max(1,total_flows))
  data.rstrip(',')
  
  with open(hops_file2, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    data = data.rstrip(',')
    #logWriter.writerows([[int(cur_time), str(data), str(overall_avg) ]])
    #logWriter.writerows([[int(cur_time), str(total_flows), str(total_hop_count), str(overall_avg), str(data)]])
    logWriter.writerows([[int(cur_time), str(total_flows), str(total_hop_count), str(overall_avg)]])
  return
  
  log.info("At[%s]: dbg_flow_counter[%d] active_flow_count[%d]",cur_time, dbg_flow_counter, active_flow_count)
  return

def log_redirection_log(cur_time):
    data = ""
    for key,value in redirection_log.items():
        data += str(key) + ':' + str(len(value)) + ','
        #data += str(key) + ':' + str(len(value)) + '::' + str(value) + ','
        
    #if data is None or len(data) == 0:
    #  return
    data = data.rstrip(',')
    with open(rdir_file, 'ab') as csvfile:
        logWriter = csv.writer(csvfile,delimiter=',')
        #data = data.rstrip(',')
        logWriter.writerows([[int(cur_time), str(data)]])
    return  


def log_relate_flow_info(cur_time):
    return fmap_sk.log_relate_flow_info(cur_time,related_flow_info_file)

fupdate_interval = 1 #2 #5
placement_intval = 0
base_time = None
def _svc_flog_function():
    global base_time
    
    #opportunity to start onluy after registerd flows and stop after end of flows
    if check_and_update_flow_count_info_for_all_svc(): pass#return
  
    log_file_func = flog_file
    cur_time = time.time()
    svc_list = disc.get_svc_dict().keys()
    
    if base_time is None: base_time = cur_time
    cur_time-= base_time
    
    #Log flow count at each service
    for svc_id in svc_list:
        svc_instances = fmap_sk.get_total_svc_instance_list_for_svc_id(svc_id)
        data = ""
        for svc_inst in svc_instances:
            flow_at_svc_inst = fmap_sk.get_total_flow_count_for_svc_id_at_svc_mac(svc_id, svc_inst)
            data += str(svc_inst) + str('-') + str(flow_at_svc_inst) + str(',')
    
        if data == "": data = "0"
        log_file_func = flog_file.split('.')[0] + '_' + str(svc_id) +'.'+flog_file.split('.')[1]
        with open(log_file_func, 'ab') as csvfile:
            logWriter = csv.writer(csvfile,delimiter=',')
            data = data.rstrip(',')
            logWriter.writerows([[int(cur_time), str(svc_id), str(data)]])
    
    #Also log the SP of each service
    disc._svc_log_function(cur_time)
  
    #Log overall Hop Count Information
    log_flows_hop_count(cur_time)
  
    #Log Redirection Log
    log_redirection_log(cur_time)
    
    log_relate_flow_info(cur_time)
    
    #check_for_optimal_placement()
    return

#these Functions are for trying with Optimal Placement case test only
#placement_info = [cur_cost, cur_path, hop_count_to_dest, cur_svc_mac_addr, src_mac_addr, sw_list_in_cur_path, path_to_dest, dst_mac_addr]
def add_to_placement_table(svc, path, cost, path_to_dest=None,proto=ipv4.ICMP_PROTOCOL, ttl=0):
  global new_placement_table
  sw_dpid_list = [sw.dpid for sw,in_port,out_port in path]
  orig_src_mac = get_mac_addr_from_sw_and_port(sw_dpid=path[0][0].dpid, port_id=path[0][1])
  best_svc_mac = get_mac_addr_from_sw_and_port(sw_dpid=path[-1][0].dpid, port_id=path[-1][2])
  if orig_src_mac is None:
    if path and path[0][0] and path[0][1]:log.error("Cannot add route to placement table as src_mac is invalid [%s:]!!", path)
    else: log.error("Cannot add route to placement table as src_mac is invalid!!")
    return
  if best_svc_mac is None:
    if path and path[-1][0] and path[-1][2]:log.error("Cannot add route to placement table as svc_mac is invalid [%s:]!!", path)
    else: log.error("Cannot add route to placement table as svc_mac is invalid!!")
    return
  
  hop_to_dst = 0
  if path_to_dest:
    hop_to_dst = len(path_to_dest)
    dst_mac_addr = get_mac_addr_from_sw_and_port(sw_dpid=path_to_dest[-1][0].dpid, port_id=path_to_dest[-1][2])
  placement_info = [cost, path, hop_to_dst, best_svc_mac, orig_src_mac, sw_dpid_list,path_to_dest,dst_mac_addr]
  if new_placement_table.has_key(svc):
    new_placement_table[svc].append(placement_info)
    #new_placement_table[svc] = [placement_info]
  else:
    new_placement_table[svc] = [placement_info]
  return
#these Functions are for trying with Optimal Placement case test only
def check_for_optimal_placement():
  global new_placement_table
    
  keys_list = new_placement_table.keys()
  if not keys_list:
    return

  svc_dict = disc.get_svc_dict()
  if svc_dict.has_key(0):
    free_list = svc_dict[0]
  else:
    log.warning("No Free core Available! ")
    #return
  svc_flow_dict = disc.get_svc_flow_dict()
  instantiate_list = []
  
  if free_list is None or len(free_list) == 0 or len(free_list[0]) == 0:
    log.warning("Empty Free List! Run Amongst Exisitng Running Cores")
  
  #free_list = []
  #for key in svc_dict.keys():
  #  free_list += svc_dict[key]
  
  #log.debug("Free List[%s]", free_list)
  for key,vals in new_placement_table.items():
    log.debug("Check for key[%d] , vals [%s]! ", key,vals)
    if not svc_flow_dict.has_key(key): continue
    unchanged_vals_list = []
    for indx, value in enumerate(vals):
      log.debug("Index:[%d], value:[%s]", indx, value)
      cur_cost, cur_path, dst_hop, cur_svc_mac, src_mac, sw_path_list,p_to_dest, dst_mac = value[0], value[1], value[2], value[3], value[4], value[5], value[6],value[7]
      new_cost = cur_cost
      candidate_sw_list = svc_flow_dict[key]
    
      inst_list = []
      if candidate_sw_list:
        for i, (sw_id, count) in enumerate(candidate_sw_list):
          for index,instance in enumerate(free_list):
            if sw_id == instance[3] and EthAddr(instance[1]) != EthAddr(cur_svc_mac) and instance not in inst_list:
              inst_list.append(instance)
      
      if not inst_list:
        log.warning("Empty Instance List!! No Possible optimal placement!, ")
        continue
      inst_list = [x for x in free_list if x[1] != EthAddr(cur_svc_mac)]
      candidate_instance = None
      cur_cost2 = cur_cost
      cur_path2 = cur_path
      for index,instance in enumerate(inst_list):
        swid = instance[3]
        _mac = EthAddr(instance[1])
        if _mac is None: continue
        svc_addr = mac_map[_mac]
        if svc_addr is None: continue
        svc_sw,svc_port = svc_addr[0],svc_addr[1]
        #log.warning("Estimate Path and cost for [%s:%d] to [%s:%d]", cur_path[0][0], cur_path[0][1], svc_sw,svc_port)
        new_path = _get_path(cur_path[0][0], svc_sw, cur_path[0][1], svc_port)
        if new_path is None:
          log.warning("Found No Paths!!")
          continue
        ##### TO dO: Setup DST MAC ADDR GET DEST PATCH FROM NEW SVC AND COMPUTE THE PENALTY FACTOR AND COST ####
        if dst_mac is None: continue
        d_mac = EthAddr(dst_mac)
        dst_addr = mac_map[d_mac]
        if dst_addr is None: continue
        dst_sw,dst_port = dst_addr[0],dst_addr[1]
        new_path_to_dst_from_svc = _get_path(svc_sw, dst_sw, svc_port, dst_port)
        hop_count_dst_frm_svc = len(new_path_to_dst_from_svc)
        new_inst_sp = 0
        
        
        
        #pf = get_penalty_factor(svc_sw,out_port_dst=1, hop_count_dst=dst_hop, out_port_svc=1, hop_count_svc=len(new_path) )
        #new_cost = compute_path_cost(svc_sw,hop_count_dst = dst_hop, shadow_price=new_inst_sp, hop_count_svc=len(new_path), penalty_factor =pf, Weight = 0)
        
        
        #get_penalty_factor_2(self, out_port_dst=0, hop_count_dst=0, out_port_svc=0, hop_count_svc=0, hop_count_dst_frm_svc=0 )
        
        pf = get_penalty_factor_2(self=cur_path[0][0],out_port_dst=p_to_dest[0][2], hop_count_dst=dst_hop, out_port_svc=new_path[0][2], hop_count_svc=len(new_path), hop_count_dst_frm_svc=len(new_path_to_dst_from_svc))
        new_cost = compute_path_cost_2(cur_path[0][0],hop_count_dst =dst_hop, shadow_price=new_inst_sp,hop_count_svc=len(new_path), penalty_factor=pf)
        
        log.info("Estimated Path and cost from [%s:%d] to [%s:%d] is:[%d], current_svc_cost[%d]", cur_path[0][0], cur_path[0][1], svc_sw,svc_port, new_cost,cur_cost2)
        
        
        if cur_cost2 > new_cost :
          candidate_instance = instance
          cur_cost2 = new_cost
          cur_path2 = new_path
        
      if candidate_instance:
        
        log.info("Chose the Candidate instance for Service[%d] as [%s] with path:%s and path_cost[%d]", key,candidate_instance,cur_path2,cur_cost2 )
        
        # Instantiate new instance and redirect 
        ret = disc.instantiate_new_instance_for_svc(svc_id =key, cur_max_sp= cur_cost,sw_path_list=sw_path_list,new_service=False, prechosen_instance=candidate_instance)
        log.info("New Instantiation = [%d]", ret)
        if not ret: continue
        
        #Pop out/Remove the current chosen entry from placement list
        vals.remove(value)
        if len(new_placement_table[key]) == 0: new_placement_table.pop(key)
        
        # do the Redirection to this instance
        redirect_flows_towards_this_instance(svc_id=key, new_svc_mac_addr=EthAddr(candidate_instance[1]), check_for_flows = False)
        
        ## Bring down Old Service
        disc.bringdown_service_instace_for_svc(svc_id = key, svc_mac_addr =EthAddr(cur_svc_mac), skip_wait_period=True)
        update_svc_flow_table_on_svc_instance_bringdown(svc_id = key, svc_mac_addr =EthAddr(cur_svc_mac))
        ##Replace the Table
        ##new_placement_table[key] = [[cur_cost2, cur_path2, dst_hop, candidate_instance[1], src_mac, sw_path_list]]
        #value = [cur_cost2, cur_path2, dst_hop, candidate_instance[1], src_mac, sw_path_list]
        #break
        return
  return

def _handle_FlowRemoved(event):
    if event is None or event.ofp is None or event.ofp.match is None:
        return
    sw_dpid=event.connection.dpid
    
    # Update flow information
    update_svc_flow_table_on_flow_removal_event(sw_dpid=sw_dpid, event=event)
    
    #GateKeeper <Regardless of switch and type of deletion> <Reconsider this option!>
    #fmap_sk.del_flow_from_flow_info_table(event.ofp.match)
    
    ## Handle only the Timeout Rules,  Force Deleted will be handled internally <otherwise results in unnecessary re-updates of SP (making it fluctuate )>
    #if event.ofp.reason == of.OFPRR_DELETE: # of.OFPRR_HARD_TIMEOUT or event.ofp.reason == of.OFPRR_IDLE_TIMEOUT:
    #    log.debug("Skipped processing the Explicitly Deleted rule on sw_dpid[%d]",sw_dpid)
    #    return
  
    ## Handle only the Rules on Edge port Switches < This would mean flow handled at src/ingress switch only > 
    #if core.openflow_discovery.is_edge_port(sw_dpid, event.ofp.match.in_port) is False:
    #    log.debug("Skipped processing Deleted rule on Non Edge Port!")
    #    return
    #match = event.ofp.match

    return

def _handle_openflow_PortStatus (event):
    """
    Track changes to switch ports
    """
    switch = event.dpid
    port = event.port
    #print "LLDPSENDER::I heard a port status change from switch {} port {}".format(switch, port)
    #log.warning("Sw, Port [%s.%s] added[%s], deleted[%s] event[%s]!",event.dpid, event.port,event.added,event.deleted,event)
    if event.modified:
        #<added>
        if event.ofp.desc.config == 0: pass
            #log.warning("Modified config status [%s], Port Enabled! ",event.ofp.desc.config)
        #<deleted>
        if event.ofp.desc.config == 1: pass
            #log.warning("Modified config status [%s], Port Disabled! ",event.ofp.desc.config)
    #if event.added:
        #log.warning("Sw, Port [%s.%s] added!",event.dpid, event.port)
        #Discovery.Link(link.dpid2,link.port2,link.dpid1,link.port1)
        #self.add_port(event.dpid, event.port, event.ofp.desc.hw_addr)
    #elif event.deleted:
        #log.warning("Sw, Port [%s.%s] deleted!",event.dpid, event.port)
        #self.del_port(event.dpid, event.port)
    return
def launch ():

  #setup the missed send len <to avoid overhead of packet_in messages>
  #core.openflow.miss_send_len = 64 #65535
  
  #time.sleep(FLOOD_HOLDDOWN)
  #update_all_file_paths()
  core.registerNew(l2_multi)

  #timeout = min(max(PATH_SETUP_TIME, 5) * 2, 15)
  timeout = min(max(PATH_SETUP_TIME, 5) * 2, 60)
  Timer(timeout, WaitingPath.timeout_waiting_paths, recurring=True)
  