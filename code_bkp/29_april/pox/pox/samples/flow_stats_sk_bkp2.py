#!/usr/bin/python
# Copyright 2012 William Yu
# wyu@ateneo.edu
#
# This file is part of POX.
#
# POX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# POX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with POX. If not, see <http://www.gnu.org/licenses/>.
#

"""
This is a demonstration file created to show how to obtain flow 
and port statistics from OpenFlow 1.0-enabled switches. The flow
statistics handler contains a summary of web-only traffic.
"""

# standard includes
from pox.core import core
from pox.lib.util import dpid_to_str
import pox.openflow.libopenflow_01 as of
# include as part of the betta branch
from pox.openflow.of_json import *
import time
log = core.getLogger()
import csv
import os.path
# To access and update the shadow price for service dictionary entries
import pox.openflow.discovery_sk as disc

THRESHOLD_PACKETS = 50
stats_interval = 5
log_file = 'logsp.csv'
switch_flow_info = {}
last_logged_time = 0

def log_sp_and_flow_data():
  global last_logged_time
  global stats_interval
  cur_time = time.time()
  if ((cur_time - last_logged_time) < stats_interval):
    return
  last_logged_time = cur_time
  dict = disc.get_svc_dict()
  log.warning("At[%s]: Dictionary contents:%s", cur_time, dict)
  with open(log_file, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    for key,value in dict.items():
      for x in range(len(value)):
        sp,mac_id,ip,sw_id = value[x]
        logWriter.writerows([[str(time.time()), str(key), str(sw_id), str(mac_id), str(sp)]])

def clear_flows_on_switch(dpid=0, vlan_id=0):
  log.debug("Sending for clear on switch %s"% (dpid_to_str(dpid)))
  #msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE_STRICT)
  msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE)
  msg.match.dl_vlan = vlan_id
  msg.match.in_port = None #in_port #event.port #None
  msg.match.nw_src = None
  msg.match.nw_dst = None
  msg.match.dl_src = None
  msg.match.dl_dst = None
  
  #msg = of.ofp_flow_mod(command=of.OFPFC_DELETE)
  #core.openflow.sendToDPID(dpid, msg)
  for switch in core.openflow._connections.values():
    if switch.dpid == dpid:
      #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d] by msg=%s", switch.dpid,vlan_id, msg)
      log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
      switch.send(msg)
  
def handle_flow_stats(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  if (switch_flow_info.has_key(sw_id)):
    for x in range(0,len(switch_flow_info[sw_id])):
      old_vlan,old_pkts = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1]
      if (num_pkts == old_pkts):
        # Reduce the Shadow Price (no flows observed!)
        clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price= -10)
        if(clear_flag is True):
          clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
        #pass
      elif (num_pkts > old_pkts and (num_pkts-old_pkts) > THRESHOLD_PACKETS):
      #elif (num_pkts > old_pkts and num_pkts > THRESHOLD_PACKETS):
        # Check if needs to update the Shadow Price
        clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price= 10)
        if(clear_flag is True):
          clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
      else:
        # Print the number of Flows, if 0, no action!!
        pass
      switch_flow_info[sw_id][x][1] = num_pkts
  else:
    switch_flow_info[sw_id] = [[vlan_id,num_pkts]]
  
  #disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price)
  #log.info("handle_flow_stats: %s",switch_flow_info )
  log_sp_and_flow_data()
  
  return
  

# handler for timer function that sends the requests to all the
# switches connected to the controller.
def _timer_func ():
  for connection in core.openflow._connections.values():
    connection.send(of.ofp_stats_request(body=of.ofp_flow_stats_request()))
    #connection.send(of.ofp_stats_request(body=of.ofp_port_stats_request()))
  log.debug("Sent %i flow/port stats request(s)", len(core.openflow._connections))

# handler to display flow statistics received in JSON format
# structure of event.stats is defined by ofp_flow_stats()
def _handle_flowstats_received (event):
  stats = flow_stats_to_list(event.stats)
  #log.info("FlowStatsReceived from %s: %s", #event, event.stats)
  #  dpid_to_str(event.connection.dpid), stats)
  
  #val = disc.update_shadow_price(dpid=1,svc_id=13,shadow_price=10)
  #log.info("updated: FlowStatsReceived from [%d:%s]: %s", event.connection.dpid, #event, event.stats)
    #dpid_to_str(event.connection.dpid), stats)
  
  # Get number of bytes/packets in flows for matching vlan_id
  num_bytes = 0
  num_flows = 0
  num_packet = 0
  for f in event.stats:
    #log.info (" F in event.stats is: %s", f)
    #if f.match.dl_vlan !=0 and f.match.dl_vlan != 65535 and f.actions[0].port == 1:
    if f.match.dl_vlan !=0 and f.match.dl_vlan != 65535:
      log.info("At: %s For Switch [%d:%s] vlan_Id: %d, %s bytes (%s packets) over %s flows",
       time.time(),event.connection.dpid,dpid_to_str(event.connection.dpid),f.match.dl_vlan, f.byte_count, f.packet_count, num_flows)
      handle_flow_stats(sw_id=event.connection.dpid, port_id=f.actions[0].port,vlan_id=f.match.dl_vlan,num_pkts=f.packet_count)
      
      #log.info("FlowStatsReceived from %s: %s", dpid_to_str(event.connection.dpid), stats)
      #pass
# handler to display port statistics received in JSON format
def _handle_portstats_received (event):
  stats = flow_stats_to_list(event.stats)
  log.info("PortStatsReceived from %s: %s", #event, stats)
    dpid_to_str(event.connection.dpid), stats)
    
# main functiont to launch the module
def launch ():
  from pox.lib.recoco import Timer

  # attach handsers to listners
  core.openflow.addListenerByName("FlowStatsReceived", 
    _handle_flowstats_received) 
  #core.openflow.addListenerByName("PortStatsReceived", 
  #  _handle_portstats_received) 

  # timer set to execute every five seconds
  Timer(stats_interval, _timer_func, recurring=True)