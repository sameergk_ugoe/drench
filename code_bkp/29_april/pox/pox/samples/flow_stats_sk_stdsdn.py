#!/usr/bin/python
# Copyright 2012 William Yu
# wyu@ateneo.edu
#
# This file is part of POX.
#
# POX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# POX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with POX. If not, see <http://www.gnu.org/licenses/>.
#

"""
This is a demonstration file created to show how to obtain flow 
and port statistics from OpenFlow 1.0-enabled switches. The flow
statistics handler contains a summary of web-only traffic.
"""

# standard includes
from pox.core import core
from pox.lib.util import dpid_to_str
import pox.openflow.libopenflow_01 as of
from pox.lib.util import dpid_to_str, str_to_bool, str_to_dpid
from pox.lib.addresses import EthAddr, IPAddr
# include as part of the betta branch
from pox.openflow.of_json import *
import time
log = core.getLogger()
import csv
import os.path
from os.path import expanduser
# To access and update the shadow price for service dictionary entries
import pox.openflow.discovery_sk as disc
# To access the Switch and setup the flows proactively
import pox.forwarding.l2_multi_sk_stdsdn as l2m
import pox.forwarding.flow_mapper_sk as fmap_sk
#import core.openflow_discovery as Discovery
from sets import Set
from pox.lib.revent import EventRemove,EventHaltAndRemove
from copy import deepcopy
stats_interval = 0.25
import pox.openflow.util_sk as util_sk

vlan_set = Set([])
#sw_id_vlan_set = Set([])
THRESHOLD_PACKETS = 200 #200 #500 #50
stats_interval =  0.5 #5 #5 #2 #10 #1 
gEvent = None
switch_flow_info = {}   #{'dpid1':[[svc1,port1, packets],[svc1,port2, packets], [svc2, port1, packets]] 'dpid2':[[...]]}
#Ideal Table {'dpid1':[[svc1,port1,packets, time_stamp,clearflows_flag],[svc1,port2, packets, time_stamp,clearflows_flag], [svc2, port1, packets,time_stamp,clearflows_flag]] 'dpid2':[[...]]}
#Current {'dpid1':[[svc1,packets,time_stamp],[svc2, packets, time_stamp]] 'dpid2':[[...]]}
#aggr_flow_dict = {} # {'dpid1':[pkt_count,byte_count,flow_count], 'dpid2':[pkt_count,byte_count,flow_count]}
last_logged_time = 0
base_time = 0
ecn_mark_mode = True
selective_ecn_mark = False
flow_bw_utilization_dict = {} #Key=flow_key, Value: Last Observed BW Consumption by flow
link_bw_utilization_dict = {} #Key=Link, Value: Last Observed Link BW Utilization
port_stats_dict = {} # Key = (sw_dpid,port) Value = Object of PortStatsInfo [time.time(), total_bytes, total_packets, total_drops, total_errors %ofUtilization,]
flow_ststs_dict = {} # Key = (sw_dpid,port) Value = Object of FlowStatsInfo [time.time(), total_bytes, total_packets, %ofUtilization]
#class LinkStatsInfo():

import pox.openflow.util_sk as util_sk
hdir_p = expanduser('~')
rdir_p = hdir_p + '/drench/results/'
log_file = rdir_p + 'logsp.csv'
def update_all_file_paths():
  global log_file
  global hdir_p, rdir_p
  hdir_p = util_sk.get_base_dir()
  rdir_p = util_sk.get_results_dir() + '/'
  log_file = rdir_p + "hopcount_log.csv"
  return
  
KILO_BPS = 1024
BYTES_TO_BITS = 8
NANO_SEC_TO_SEC = 1000*1000*1000
LINK_UTLIZATION_THRESHOLD = 95 # 95%
class PortStatsInfo():
    def __init__(self, dpid, port, data):
        self.key = (dpid,port)
        self.data = data
        self.init_time = time.time()
        self.updt_time = 0
        self.interval = 0
        self.old_data = data
        self.drop_rate = 0
        self.bw_utilized = 0
        self.error_rate = 0
        self.link = None
        self.link_max_bw = 0
        self.percentage_of_link_bw = 0
        self.init_link_info()
        log.debug("Initialized the PortStatsInfo Object [%s], at [%s]", self.key, self.init_time)
    def get_key(self):
        return self.key
    def get_data(self):
        return self.data
        
    def print_data(self):
        #print "Port Info(%s:)" %(self.key)
        #print "Packets(Total:%s, Rx:%s, Tx:%s)" %(self.data.rx_packets+self.data.tx_packets, self.data.rx_packets, self.data.tx_packets)
        #print "Bytes(Total:%s, Rx:%s, Tx:%s)"  %(self.data.rx_bytes+self.data.tx_bytes, self.data.rx_bytes, self.data.tx_bytes)
        #print "Drops(Total:%s, Rx:%s, Tx:%s)"  %(self.data.rx_dropped+self.data.tx_dropped, self.data.rx_dropped, self.data.tx_dropped)
        
        log.info("Port Info: %s:", self.key)
        log.info("Packets(Total:%s, Rx:%s, Tx:%s)",self.data.rx_packets+self.data.tx_packets, self.data.rx_packets, self.data.tx_packets)
        log.info("Bytes(Total:%s, Rx:%s, Tx:%s)" ,self.data.rx_bytes+self.data.tx_bytes, self.data.rx_bytes, self.data.tx_bytes)
        log.info("Drops(Total:%s, Rx:%s, Tx:%s)",self.data.rx_dropped+self.data.tx_dropped, self.data.rx_dropped, self.data.tx_dropped)
        return

    def set_link_max_bandwidth(self, link, bw_in_kbps):
        self.link = link
        self.link_max_bw = bw_in_kbps
        log.debug("Initialized Link:[%s] and Link Bandwitdh[%s]", self.link, self.link_max_bw)
        return

    def init_link_info(self):
        link = disc.get_link_from_sw_and_port(self.key[0], self.key[1])
        link_bw = disc.get_bw_of_link(link)
        self.set_link_max_bandwidth(link, link_bw)
        
        global link_bw_utilization_dict
        if not link_bw_utilization_dict.has_key(link):
            link_bw_utilization_dict[link] = 0
           
        return
    
    def print_raw_data(self, new_data):
        log.debug("For Port[%s] and Link:[%s]", self.key, self.link)
        log.debug("Old Data [%s, %s, %s]", self.data.rx_bytes, self.data.tx_bytes, self.data.rx_bytes+self.data.tx_bytes)
        log.debug("New Data [%s, %s, %s]", new_data.rx_bytes, new_data.tx_bytes, new_data.rx_bytes+new_data.tx_bytes)
        
        return
    def update_data(self, new_data):
        cur_time = time.time()
        self.interval = float(cur_time - self.updt_time)
        self.print_raw_data(new_data)
        log.debug("Init Time:[%s], Last Update_Time:[%s], Current Time:[%s], Interval:[%s],", self.init_time, self.updt_time, cur_time, self.interval)
        self.updt_time = cur_time
        self.old_data = self.data
        self.data = new_data
        
        if self.interval == cur_time or self.interval <= 0:
            log.debug("SKIPPED due to incorrect interval[%s]!", self.interval)
            return
            
        #Compute Statistic params
        ttl_bytes = ((self.data.rx_bytes+self.data.tx_bytes) - (self.old_data.rx_bytes+self.old_data.tx_bytes)) 
        log.debug("Total Bytes [%s] in Total Time [%s] over Link[%s:%s]]", ttl_bytes, self.interval,self.link,self.link_max_bw)
        self.bw_utilized = ((BYTES_TO_BITS)*(ttl_bytes))/float(KILO_BPS*self.interval)
        self.drop_rate = (100*(self.data.rx_dropped+self.data.tx_dropped))/float(self.data.rx_packets+self.data.tx_packets)
        self.error_rate = (100*(self.data.rx_errors+self.data.tx_errors))/float(self.data.rx_packets+self.data.tx_packets)
        
        if self.link and self.link_max_bw:
            self.percentage_of_link_bw = (100*self.bw_utilized)/float(self.link_max_bw)
            if self.percentage_of_link_bw >= LINK_UTLIZATION_THRESHOLD:
                log.warning("Link[%s] exceeds marked utlization threshold percent [%s] ", self.percentage_of_link_bw, LINK_UTLIZATION_THRESHOLD)
            
            global link_bw_utilization_dict
            if link_bw_utilization_dict.has_key(self.link):
                link_bw_utilization_dict[self.link] = self.percentage_of_link_bw
            else:
                link_bw_utilization_dict[self.link] = self.percentage_of_link_bw
        log.info("Link[%s]::Bandwitdh_Utilized:%s, Drop Rate:%s, Error Rate:%s, percentage_ofLinkBandwidth=%s",self.link, self.bw_utilized, self.drop_rate, self.error_rate, self.percentage_of_link_bw)
        
        return
    def get_port_stats(self):
        log.info("Last Update at:%s, time_interval: %s", (time.time()- self.updt_time), self.interval)
        #self.print_data()
        log.info("BW_Utilized:%s, Drop Rate:%s, Error Rate:%s, percentage_ofLinkBandwidth=%s",self.bw_utilized, self.drop_rate, self.error_rate, self.percentage_of_link_bw)
        #print " BW_Utilized:%s, Drop Rate:%s, Error Rate:%s, percentage_ofLinkBandwidth=%s)"  %(self.bw_utilized, self.drop_rate, self.error_rate, self.percentage_of_link_bw)
        return (self.bw_utilized)

def _get_port_stat_info(sw_dpid, port):
    global port_stats_dict
    ps_key = (sw_dpid, port)
    if port_stats_dict.has_key(ps_key):
        return port_stats_dict[ps_key]
    return None
def _set_port_stat_info(sw_dpid, port, data):
    global port_stats_dict
    ps_key = (sw_dpid, port)
    if port_stats_dict.has_key(ps_key):
        port_stats_dict[ps_key] = data
    else:
        port_stats_dict[ps_key] = data
        return False
    return True

def _get_link_stat_info(link):
    global link_bw_utilization_dict
    if link_bw_utilization_dict.has_key(link):
        return link_bw_utilization_dict[link]
    return None
def _set_link_stat_info(link, data):
    global link_bw_utilization_dict
    if link_bw_utilization_dict.has_key(link):
        link_bw_utilization_dict[link] = data
    else:
        return False
    return True

BW_THRESHOLD_ERROR_MARGIN = -0.05   #5% error margin for computation
THRSHOLD_INTERVAL_OFFSET = 0.3      # Acceptable +/- offset interval
MAX_COUNT_BEFORE_WARN = 1           # Count/interval before setting ECN
class FlowStatsInfo():
    def __init__(self, sw_dpid, flow_key, data):
        self.key = flow_key
        self.sw_dpid = sw_dpid
        self.data = None
        self.init_time = time.time()
        self.updt_time = 0
        self.interval = 0.0
        self.bytes_in_cur_interval = 0
        self.cur_interval = 0.0
        self.total_flow_duration = 0.0
        self.total_flow_bytes = 0
        self.old_data = None
        self.bw_utilized = 0
        self.avg_bw_utlized = 0
        self.flow_max_bw = 0
        self.links = None
        self.bottleneck_link = None
        self.link_min_bw = 0
        self.percentage_of_link_bw = 0
        self.marked_for_ecn = False
        self.init_ecn_flag = False
        self.overutilization_counter = 0
        self.init_link_info()
        self.update_data(data)
        log.debug("Initialized the PortStatsInfo Object [%s], at [%s]", self.key, self.init_time)
    def get_key(self):
        return self.key
    def print_key(self):
        return (self.key[1])
    def get_data(self):
        return self.data
        
    def print_data(self):
        #print "Flow Info(%s:)" %(self.key)
        
        log.info("Flow Info: %s:", self.key)
        log.info("Count(Bytes:%s, Packets:%s)",self.data.byte_count, self.data.packet_count)
        log.info("Duration(Seconds:%s, n_seconds:%s)" ,self.data.duration_sec, self.data.duration_nsec)
        return

    def set_flow_max_bandwidth(self, bw_in_kbps):
        self.flow_max_bw = bw_in_kbps
        log.debug("Initialized Flow Max Bandwitdh[%s]", self.flow_max_bw)
        return

    def init_link_info(self):
    
        self.set_flow_max_bandwidth(l2m.get_flow_demand_for_flow(self.key))
        
        self.links = l2m.get_flow_links_from_flow_path(self.key)
        log.info("Links of Flow are: %s", self.links)
        
        if self.links is not None:
            self.bottleneck_link, self.link_min_bw = min([(link,bw) for link,bw in disc.link_bw_dict.iteritems() if link in self.links], key=lambda x: x[1])
            log.info("Bottleneck Link of Flow and its BW : %s, %s", self.bottleneck_link, self.link_min_bw)
        else:
            log.info("Flow with no Links!!")
        global flow_bw_utilization_dict
        if not flow_bw_utilization_dict.has_key(self.key):
            flow_bw_utilization_dict[self.key] = 0
        
        return
    
    def print_raw_data(self, new_data):
        #log.debug("For Port[%s] and Link:[%s]", self.key, self.link)
        #log.debug("Old Data [%s, %s, %s]", self.data.byte_count, self.data.duration_sec, self.data.duration_nsec)
        if self.old_data:
            log.info("Old Data [%d, %f::(%d, %d)]", self.old_data.byte_count, float(self.old_data.duration_sec + self.old_data.duration_nsec/float(NANO_SEC_TO_SEC)),self.old_data.duration_sec, self.old_data.duration_nsec)
        log.info("New Data [%d, %f::(%d, %d)]", new_data.byte_count, float(new_data.duration_sec + new_data.duration_nsec/float(NANO_SEC_TO_SEC)),new_data.duration_sec, new_data.duration_nsec)
        return
    def update_flow_counters(self, new_data):
        cur_time = time.time()
        self.interval =  cur_time - self.updt_time
        
        current_flow_bytes    = new_data.byte_count
        current_flow_duration = float(new_data.duration_sec + new_data.duration_nsec/float(NANO_SEC_TO_SEC))
        
        #if self.interval < stats_interval/2:
        #    log.warning("Update in short interval (%s)", self.interval)
        #    if new_data.byte_count == 0:
        #        log.warning("Update in short interval (%s) with zero byte count", self.interval)
        #        return
        self.updt_time = cur_time
        
        if self.interval == cur_time:
            self.interval = current_flow_duration
        #log.debug("SELF.INTERVAL=[%s], FLOW_BYTES=[%s]", self.interval, current_flow_bytes)
            
        if current_flow_bytes < self.total_flow_bytes:
            #Indicates flow was reset/modified!
            log.debug("At Time[%f]:: Current flow Bytes [%d] is less than Total Flow Bytes [%d]", (self.updt_time - self.init_time), current_flow_bytes, self.total_flow_bytes)
            #self.data = None
            #Possible that no data was trasnmitted in this interval.
            pass
        
        if current_flow_duration < self.total_flow_duration:
            #Indicates flow was reset/modified!
            log.debug("At Time[%f]:: Current flow Duration [%f] is less than Total Flow Duration [%f]", (self.updt_time - self.init_time), current_flow_duration, self.total_flow_duration)
            self.data = None
            #Only Chance is Flow is reset or rule modified with add command.
            pass
        self.total_flow_bytes  =  new_data.byte_count
        self.total_flow_duration = current_flow_duration
        
        #self.interval = current_flow_duration - self.total_flow_duration
        
        self.old_data = deepcopy(self.data)
        self.data = deepcopy(new_data)
        if self.old_data is None or new_data.byte_count == 0 or current_flow_duration <= 0:
            self.bytes_in_cur_interval = self.data.byte_count
            self.cur_interval = current_flow_duration
        else:
            self.bytes_in_cur_interval = ((self.data.byte_count) - (self.old_data.byte_count))
            self.cur_interval = float(((self.data.duration_sec - self.old_data.duration_sec)) + ((self.data.duration_nsec - self.old_data.duration_nsec)/float(NANO_SEC_TO_SEC)))
        log.info("At Time[%f]:: Total Bytes [%s] in Interval[%s]" ,(self.updt_time - self.init_time), self.bytes_in_cur_interval, self.cur_interval)
        
        #No Bytes in this period
        if new_data.byte_count == 0:
            return False    
        return True
        
    def update_data(self, new_data):
        
        #self.print_raw_data(new_data)
        if self.update_flow_counters(new_data) is False:
            log.warning("Skipped Flow Stats Evaluation![%s:%s]",self.cur_interval, self.bytes_in_cur_interval)
            return
        
        #if self.old_data is None:
        #    log.warning("First Sample at interval[%s]. SKIP evaluation!", self.interval)
        #    self.old_data = new_data
        #    return
        #self.interval = float(cur_time - self.updt_time)
        
        #if self.interval == cur_time or self.interval <= 0:
        #    log.warning("First Sample at interval[%s]. SKIP evaluation!", self.interval)
        #    return
        #if self.interval > 2*stats_interval:
        #    log.warning("Missed one sample of periodic update!!")
        
        #if ttl_flow_duration < self.total_flow_duration:
        #    log.warning("Invalid Flow Duration [%s] Reported [%s]! Reset counters!!!!", ttl_flow_duration, self.total_flow_duration)
        #    #self.total_flow_duration = 1
        #    #return
        
        
        #Compute Statistic params
        ttl_bytes = self.bytes_in_cur_interval
        #if ttl_bytes == 0 and self.data.byte_count > 0:
        #    ttl_bytes = self.data.byte_count
        #    time_interval = float(self.data.duration_sec + (self.data.duration_nsec)/float(NANO_SEC_TO_SEC))
        #    self.interval = time_interval
            
        #else:
        time_interval = self.cur_interval
        #if ( (time_interval < ((1-THRSHOLD_INTERVAL_OFFSET)*stats_interval)) or (time_interval > ((1+THRSHOLD_INTERVAL_OFFSET)*stats_interval)) ):
            #log.warning("Detected Inaccurate Interval[%s, %s]! Skip Evaluation!!", self.interval, time_interval)
            #return
        #else:
        #    log.warning("Changing self interval from [%s] to [%s]", self.interval, time_interval)
        #    self.interval = time_interval
        
        #Final check to avoid errors
        if time_interval <= 0 or ttl_bytes <=0 or time_interval < stats_interval/4 or time_interval > stats_interval*4 :
            log.error("Interval[%s] or Bytes [%s] is not correct! Avoid Rate Estimations for [%s].", time_interval, ttl_bytes, self.key)
            return
        
        #log.debug("Total Bytes [%s] in Total Time [%s] over Link[%s:%s]]", ttl_bytes, self.interval,self.bottleneck_link,self.link_min_bw)
        #log.debug("Total Bytes [%s] in Total Time [%s] ]", ttl_bytes, self.interval)
        self.bw_utilized = ((BYTES_TO_BITS)*(ttl_bytes))/float(KILO_BPS*time_interval)
        #self.avg_bw_utlized = ((BYTES_TO_BITS)*(self.data.byte_count))/float(KILO_BPS*self.total_flow_duration)
        log.warning("At Time[%s]:: Flow with Key[%s] has usage[%s]! ", (self.updt_time - self.init_time), self.print_key(), self.bw_utilized)
        
        #Check if this flow has exceeded the current alloted bw limit
        flow_cong_flag, cur_allot_bw = l2m.get_flow_alloted_bw_for_flow(self.key)  #l2m.get_flow_allot_bw_from_flow_allot_table(self.key)
        #log.warning("for flow[%s] Retrieved allot_bw[%s], cong_flag[%s]", self.print_key(),cur_allot_bw,flow_cong_flag)
        if flow_cong_flag == False:
            #log.warning("Skip Rate Control on Flow in Non Congested Path!")
            #return
            #in this case peak limit is the min link bandwidth in the path
            #cur_allot_bw = l2m.get_min_link_bw_of_flow_from_flow_key(self.key)
            
            cur_allot_bw =self.link_min_bw
            log.debug("Flow is not in Congestion Path. Set Flow's Limit as Link Bandwidth [%s]", cur_allot_bw)
        
        if cur_allot_bw is None or cur_allot_bw == 0:
            pass
        else:
            bwd_limit = ((cur_allot_bw)*(1+BW_THRESHOLD_ERROR_MARGIN))
            if self.bw_utilized <= bwd_limit: #cur_allot_bw:
                if self.overutilization_counter:
                    self.overutilization_counter -=1
                else:
                    if self.marked_for_ecn is True:
                        self.marked_for_ecn = False
                        #log.debug("At Time[%s]:: Flow with Key[%s] is cleared for ECN; Flow Usage[%s] within Fair Share[%s]", self.updt_time, self.key, self.bw_utilized, cur_allot_bw)
            else:
                #self.overutilization_counter +=1
                #log.warning("At Time[%s]:: Flow with Key[%s] has usage[%s] exceeding the Fair Share[%s::%s]", self.updt_time, self.key, self.bw_utilized, cur_allot_bw, bwd_limit)
                #if self.bw_utilized > ((cur_allot_bw)*(1+BW_THRESHOLD_ERROR_MARGIN)): # or self.overutilization_counter >= MAX_COUNT_BEFORE_WARN: #self.bw_utilized > cur_allot_bw 
                    #self.marked_for_ecn = True
                    #log.warning("At Time[%s]:: Flow with Key[%s] is marked for ECN; Flow Usage[%s] exceeds Fair Share[%s]", self.updt_time, self.key, self.bw_utilized, cur_allot_bw)
                if self.bw_utilized > bwd_limit: #((cur_allot_bw)*(1+BW_THRESHOLD_ERROR_MARGIN)): # or 30% more than fair share then signal
                    self.overutilization_counter +=1
                    if self.overutilization_counter >= MAX_COUNT_BEFORE_WARN: #self.bw_utilized > cur_allot_bw 
                        self.marked_for_ecn = True
                        log.warning("At Time[%s]:: Flow with Key[%s] is marked for ECN; Flow Usage[%s] exceeds Fair Share[%s::%s]", self.updt_time, self.print_key(), self.bw_utilized, cur_allot_bw, bwd_limit)
                pass
        
        #if self.bottleneck_link and self.link_min_bw:
        #    self.percentage_of_link_bw = (100*self.bw_utilized)/float(self.link_min_bw)
        #    
        #    global flow_bw_utilization_dict
        #    if flow_bw_utilization_dict.has_key(self.key):
        #        flow_bw_utilization_dict[self.key] = self.percentage_of_link_bw
        #    else:
        #        flow_bw_utilization_dict[self.key] = self.percentage_of_link_bw
        #log.info("Flow Bandwidth:%s, percentage_ofLinkBandwidth=%s",self.bw_utilized, self.percentage_of_link_bw)
        
        return
    def get_flow_stats(self):
        #log.debug("Last Update at:%s, time_interval: %s", (time.time()- self.updt_time), self.interval)
        #self.print_data()
        log.info("Flow_BW_Utilized:%s, percentage_ofLinkBandwidth=%s",self.bw_utilized, self.percentage_of_link_bw)
        #print " BW_Utilized:%s, Drop Rate:%s, Error Rate:%s, percentage_ofLinkBandwidth=%s)"  %(self.bw_utilized, self.drop_rate, self.error_rate, self.percentage_of_link_bw)
        return (self.bw_utilized)
    def get_ecn_mark_status(self):
        return self.marked_for_ecn
    def set_ecn_mark_status(self,ecn_status = False):
        self.marked_for_ecn = ecn_status
        return ecn_status
    def clear_ecn_mark_status(self):
        self.marked_for_ecn = False
        self.overutilization_counter = 0
        return
    def get_ecn_init_mark(self):
        return self.init_ecn_flag
    def set_ecn_init_mark(self, flag):
        self.init_ecn_flag = flag
        return self.init_ecn_flag
    
def _get_flow_stat_info(flow_key):
    global flow_ststs_dict
    fs_key = (flow_key)
    if flow_ststs_dict.has_key(fs_key):
        return flow_ststs_dict[fs_key]
    return None
def _set_flow_stat_info(flow_key, data):
    global flow_ststs_dict
    fs_key = (flow_key)
    if flow_ststs_dict.has_key(fs_key):
        flow_ststs_dict[fs_key] = data
    else:
        flow_ststs_dict[fs_key] = data
        return False
    return True
def process_mark_ecn_for_identified_flows():
    global flow_ststs_dict
    for fs_key, fs_record in flow_ststs_dict.iteritems():
        #l2m.craft_and_set_ecn_status_for_flow(fs_record.get_key(), ecn_flag_status = False)
        if fs_record and fs_record.get_ecn_mark_status() is True and ecn_mark_mode is True:
            l2m.craft_and_set_ecn_status_for_flow(fs_record.get_key(), ecn_flag_status = True)
            fs_record.clear_ecn_mark_status()
        elif fs_record and selective_ecn_mark is True and fs_record.get_ecn_init_mark() is False:
            l2m.craft_and_set_ecn_status_for_flow(fs_record.get_key(), ecn_flag_status = False)
            fs_record.set_ecn_init_mark(True)
            pass
    return
def log_sp_and_flow_data(vlan_id=0,out_port=0):
  global last_logged_time
  global stats_interval
  global base_time
  
  cur_time = time.time()
  if ((cur_time - last_logged_time) < stats_interval):
    return
  last_logged_time = cur_time

  if base_time == 0:
    base_time = cur_time
  time_diff = int(cur_time - base_time)
  
  dict = disc.get_svc_dict()
  #log.debug("At[%s]: Dictionary contents:%s", cur_time, dict)
  log_file_func = log_file.split('.')[0] + '_' + str(vlan_id) +'.'+log_file.split('.')[1]
  with open(log_file_func, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    for key,value in dict.items():
      if key == vlan_id:
        log.debug("At[%s]: Dictionary for Key[%d] is:%s", cur_time, key, value)
        for x in range(len(value)):
          sp,mac_id,ip,sw_id,pt_id = value[x]
          logWriter.writerows([[str(time_diff), str(key), str(sw_id), str(mac_id), str(sp), str(out_port)]])

def add_flows_proactively_on_switch(dpid=0, vlan_id=0,event=gEvent):
  sw = l2m.get_sw_from_dpid(dpid)
  host_eth_addr = "00:00:00:00:00:01"
  dest =l2m.get_mac_map(host_eth_addr)
  if dest is None:
    log.warning("Failed to install proactive path on switch%d for svc%d", dpid,vlan_id)
    return False
  if sw:
    match = of.ofp_match()
    #event.port = ???
    sw.install_path_sk(vlan_id=vlan_id, dst_sw=dest[0], last_port=dest[1], match=match, event=event)
  return True

def clear_flows_on_switch(dpid=0, vlan_id=0,out_port=0):
  #log.debug("Func[%d] Triggered Clear Flows on switch [%s]", vlan_id,(dpid_to_str(dpid)))
  
  #msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE_STRICT)
  msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE)
  msg.match.dl_vlan = vlan_id
  #msg.match.out_port = out_port
  msg.match.in_port = None #in_port #event.port #None
  msg.match.nw_src = None
  msg.match.nw_dst = None
  msg.match.dl_src = None
  msg.match.dl_dst = None
  
  #msg = of.ofp_flow_mod(command=of.OFPFC_DELETE)
  #core.openflow.sendToDPID(dpid, msg)
  for switch in core.openflow._connections.values():

    # Base method II: clear flows on all the switches < to ensure all get to see the new path > : This also is seen results in loops on some occasions (in flight packets fwd'ed with differernt rules at different switches.)
    #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
    
    # Initial Method I: Clear flows on only the matching switches: This results into Loops
    #switch.send(msg)
    #if switch.dpid == dpid:
      #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d] by msg=%s", switch.dpid,vlan_id, msg)
      #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
      #switch.send(msg)
    
    # Method III: (Can it result in loops? looks likely... )
    # Send clear on only those switches that have the flow defined for the vlan_id (regardless of out_port)
    if ( (switch_flow_info.has_key(switch.dpid)) and (True == (any(vlan_id== x[0] for x in switch_flow_info[switch.dpid]))) ):
      log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
      switch.send(msg)
      # Also try instantaneous proactive flow setup
      #add_flows_proactively_on_switch(dpid=switch.dpid, vlan_id=vlan_id)
    else:
      # Ignore this switch
      pass
  return True

def handle_clear_flows():
  #global sw_id_vlan_set
  #for i,x in enumerate(sw_id_vlan_set):
  #  sw_id,vlan_id = x[0],x[1]
  #  clear_flows_on_switch(dpid=sw_id, vlan_id=vlan_id)
  #sw_id_vlan_set.clear()
  
  global switch_flow_info
  svc_set = Set()
  for sw_id, svc_flow_list in switch_flow_info.items():
    #if True == (any( == x[0] for x in svc_flow_list))
    for i,x in enumerate(svc_flow_list):
      if True == x[4]:
        svc_set.add((sw_id,x[0],x[1]))
        #switch_flow_info[sw_id][i] = [x[0],x[1],0,time.time(), False]
        switch_flow_info[sw_id][i] = [x[0],x[1],x[2],time.time(), False]
  
  for i,x in enumerate(svc_set):
    #clear_flows_on_switch(dpid=x[0], vlan_id=x[1],out_port=x[2])
    log.warning("Initiate Redirect Routes for Svc_id[%d] swID=[%d] out_port=[%d]", x[1], x[0], x[2])
    l2m.redirect_routes(svc_id=x[1], sw_dpid=x[0],out_port=x[2])
    pass

  if svc_set:
    svc_set.clear()
    #l2m.clear_next_svc_flow_table()

  return
  
def check_and_update_switch_flow_table(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  if (False == switch_flow_info.has_key(sw_id)):
    switch_flow_info[sw_id] = [[vlan_id,port_id,0,time.time(),False]]
    return True
  else:
    if (False == (any(vlan_id== x[0] and port_id == x[1] for x in switch_flow_info[sw_id]))):
      switch_flow_info[sw_id].append([vlan_id, port_id, 0,time.time(),False])
      return True
    pass
  return False
  
def handle_flow_stats_1(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  #global sw_id_vlan_set
  #log.warning("handle_flow_stats_1(): SW:PORT[%d:%d], %d, %d :", sw_id,port_id, vlan_id, num_pkts)
  #if vlan_id:
    #log.debug("handle_flow_stats_1(): SW:PORT[%d:%d],vlan:%d :", sw_id,port_id, vlan_id)
  new_record = check_and_update_switch_flow_table(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  
  for x in range(0,len(switch_flow_info[sw_id])):
    old_vlan,old_port,old_pkts,o_time,clear_flag = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2],switch_flow_info[sw_id][x][3],switch_flow_info[sw_id][x][4]
    if(vlan_id == old_vlan and port_id == old_port):
      log.info("handle_flow_stats_1(vlan_id:%d): SW:PORT[%d:%d], #old_pkts[%d] #new_pkts[%d]", vlan_id,sw_id,port_id,old_pkts,num_pkts)
      switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
      mac_addr = l2m.get_mac_addr_from_sw_and_port(sw_dpid=sw_id,port_id=old_port)
      if mac_addr is None:
        log.warning("handle_flow_stats_1(): SW:PORT[%d:%d],%d got Invalid MAC Address:", sw_id,old_port, port_id)
        continue
      if (num_pkts == old_pkts and new_record is False):
        # Reduce the Shadow Price (no flows observed!)
        found_flag, clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price= -5)
        switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
        #if(clear_flag is True):
          #sw_id_vlan_set.add((sw_id,vlan_id))
          #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
          #pass
      elif (num_pkts > old_pkts and (num_pkts-old_pkts) > THRESHOLD_PACKETS):
        log.info("handle_flow_stats_1(VLAN=%d): SW:PORT[%d:%d]: setting SP=[%d]", vlan_id, sw_id, port_id, 5*((num_pkts-old_pkts)/THRESHOLD_PACKETS))
        found_flag,clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price= 5*((num_pkts-old_pkts)/THRESHOLD_PACKETS))
        switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
        if(clear_flag is True):
          #sw_id_vlan_set.add((sw_id,vlan_id))
          #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
          pass
      elif (num_pkts < old_pkts):
        log.warning("handle_flow_stats_1(VLAN=%d): SW:PORT[%d:%d]: Packet count Dropped from [%d] to [%d] ", vlan_id, sw_id, port_id, old_pkts, num_pkts)
        pass
      else:
        # Save the record and Print the number of Flows, if 0, no action!!
        pass
      break
    elif (vlan_id == old_vlan): # and port_id != old_port
      #same vlan serviced on different ports:
      pass
    else:
      continue
  #log_sp_and_flow_data(vlan_id=vlan_id,out_port=port_id)
  return

# Readdress this Function @Jun21  
def handle_flow_stats_2(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  #global sw_id_vlan_set
  
  if False == disc.check_svc_at_sw_dpid(svc_id = vlan_id, sw_id=sw_id):
    #log.warning("Skip!! As this switch[%d] doesn't host the service function[%d]", sw_id, vlan_id)
    return
  #log.warning("Process!! This switch[%d] also hosts the service function[%d]", sw_id, vlan_id)
  
  new_record = check_and_update_switch_flow_table(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  
  #if new_record is False: # Do not see any reason for this check. New or OLD, just update 
  for x in range(0,len(switch_flow_info[sw_id])):
    old_vlan,old_port,old_pkts,o_time,clear_flag = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2],switch_flow_info[sw_id][x][3],switch_flow_info[sw_id][x][4]
    if(vlan_id == old_vlan and port_id == old_port):
      switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
      break
  
  #tricky now, since the switch that is serving the instance might now be redirecting flows to some other switch's service instance
  # Soln: add new Function in Discovery or L2 Multi to check if the DPID also hosts the service instance
  return
  
  mac_addr=l2m.get_mac_addr_from_sw_and_port(sw_dpid=sw_id,port_id=old_port)
  if mac_addr is None:
    return
    
  found_flag,clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price=-10)
  switch_flow_info[sw_id][x] = [vlan_id,port_id,0,time.time(),clear_flag]
  if(found_flag is True): # Indicates Switch also hosts the function and currently may not be getting used!!
    #sw_id_vlan_set.add((sw_id,vlan_id))
    #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
    pass


  #log_sp_and_flow_data(vlan_id=vlan_id,out_port=port_id)
  return

def check_for_aged_flows(vlan_id=0):
  
  global switch_flow_info
  
  clear_flag = False
  #cur_time = time.time()

  for sw_id,value in switch_flow_info.items():
    for x in range(len(value)):
      old_vlan,old_port,old_pkts,o_time,clear_flag = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2],switch_flow_info[sw_id][x][3],switch_flow_info[sw_id][x][4]
      cur_time = time.time()
      if True == core.openflow_discovery.is_edge_port(sw_id,old_port):
        if(vlan_id == old_vlan and (cur_time - o_time) >= 2*stats_interval):
          mac_addr= l2m.get_mac_addr_from_sw_and_port(sw_dpid=sw_id,port_id=old_port)
          cur_isp= disc.get_sp_of_instance(svc=vlan_id, svc_mac_addr=mac_addr)
          if mac_addr is None or cur_isp <= 0:
            switch_flow_info[sw_id][x] = [vlan_id,old_port,0,time.time(),clear_flag]
            continue
          log.warning("At:%s Aging values for switch[%d], function[%d]", cur_time,sw_id,vlan_id)
          found_flag,clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price=-10)
          switch_flow_info[sw_id][x] = [vlan_id,old_port,0,time.time(),clear_flag]
          #if(clear_flag is True):
            #sw_id_vlan_set.add((sw_id,vlan_id))
            #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
            #pass
  return

def check_for_all_aged_flows():
  global vlan_set
  for vlan_id in vlan_set:
    check_for_aged_flows(vlan_id=vlan_id)
  #vlan_set.clear()

def get_and_update_valid_mac_addr_for_services():
  dict = disc.get_svc_dict()
  for key,value in dict.items():
    for x in range(len(value)):
      sp,mac_id,ip,sw_id,pt_id = value[x]
      a_ip,a_mac,a_sw_id,a_port = util_sk.get_addr_map_from_ip_addr(ip)
      if a_ip is None or a_mac is None or a_sw_id is None: continue
      #log.warning("Service IP has In Values [%s, %s %d] vs Retrieved info [%s, %s, %d:%d] ", mac_id,ip,sw_id, a_ip, a_mac, a_sw_id, a_port)
      
      a_mac = EthAddr(a_mac).toStr()
      a_ip = IPAddr(a_ip).toStr()
  
      if a_mac != mac_id:
        #log.info("Values [%s, %s, %s %d] to be changed to %d", sp,mac_id,ip,sw_id, sw_dpid)
        disc.update_svc_mac_and_sw_info_from_ip_addr(svc_id=key,ip_addr=a_ip,mac_addr=a_mac,sw_id=a_sw_id,port_id=a_port)
        
      #dest = l2m.get_mac_map(EthAddr(mac_id))
      #if dest is None or dest[0] is None: continue
      #sw,port_id = dest[0],dest[1]
      #sw_dpid = sw.dpid

  return

def get_and_update_valid_switch_dpid_for_services():
  return get_and_update_valid_mac_addr_for_services()
  
  dict = disc.get_svc_dict()
  for key,value in dict.items():
    for x in range(len(value)):
      sp,mac_id,ip,sw_id,pt_id = value[x]
      #log.warning("Values [%s, %s, %s %d] ", sp,mac_id,ip,sw_id)
      dest = l2m.get_mac_map(EthAddr(mac_id))
      if dest is None or dest[0] is None:
        #log.warning("No Destination for Values [%s, %s, %s %d] ", sp,mac_id,ip,sw_id)
        continue
      sw,port_id = dest[0],dest[1]
      sw_dpid = sw.dpid
      if sw_dpid != sw_id:
        #log.info("Values [%s, %s, %s %d] to be changed to %d", sp,mac_id,ip,sw_id, sw_dpid)
        disc.setdpid_for_svc(svc_id=key,mac_addr=mac_id,sw_id=sw_dpid,port_id=port_id)
  return

def check_for_redirection_2():
  svc_bringdown_list = disc.check_for_svc_bring_down()
  for svc_id,instance in svc_bringdown_list:
    mac = instance[1]
    _mac = EthAddr(mac)
    log.warning("Try to bringdown service Service[%d] at mac[%s] with [%s]", svc_id, _mac, instance)
    ret = l2m.redirect_all_flows_from_this_instance(svc_id=svc_id, svc_mac_addr=_mac, check_for_flows = False)
    if ret is True:
        ret = disc.bringdown_service_instace_for_svc(svc_id = svc_id, svc_mac_addr =_mac, skip_wait_period=True)
    if ret is True:
      l2m.update_svc_flow_table_on_svc_instance_bringdown(svc_id = svc_id, svc_mac_addr =_mac)
  return
    
  svc_redirection_list = disc.check_for_svc_redirections()
  for svc_id,instance in svc_redirection_list:
    #ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
    mac = instance[1]
    _mac = EthAddr(mac)
    if (svc_id,instance) not in svc_bringdown_list:
      if (int(instance[0]) < disc.MIN_SP_THRESHOLD):
        #log.warning("Try to Redirect flows for Service[%d] towards mac[%s]", svc_id, _mac)
        #l2m.redirect_flows_towards_this_instance(svc_id=svc_id, new_svc_mac_addr=_mac,check_for_flows=True)
        pass
      elif (int(instance[0]) > disc.MAX_SP_THRESHOLD):
        log.warning("Try to Redirect flows for Service[%d] away from this mac[%s]", svc_id, _mac)
        l2m.redirect_flows_from_this_instance(svc_id=svc_id, svc_mac_addr=_mac,check_for_flows=True)
    else:
      log.warning("Redirection skipped as the Instance is marked for Bringdown [%d:%s]", svc_id, instance)
    
  #for svc_id,mac_id in svc_redirection_list:
  #  ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
  #  for ip,mac,sp in ip_mac_sp_list:
  #    log.warning("check redirection for svc[%d] with mac,sp[%s:%s]", svc_id,mac,sp)
  #    if int(sp) == 0 or int(sp) < disc.MIN_SP_THRESHOLD:
  #      _mac = EthAddr(mac)
  #      log.warning("Try to Redirect flows for Service[%d] towards mac[%s]", svc_id, _mac)
  #      l2m.redirect_flows_towards_this_instance(svc_id=svc_id, new_svc_mac_addr=_mac,check_for_flows=True)
  #      break
  return
  
def update_routes():
  check_for_all_aged_flows()
  handle_clear_flows()
  check_for_redirection_2()
  return

def setup_stats_requests():
  msg_flow_stats = of.ofp_stats_request(body=of.ofp_flow_stats_request())
  #msg_aggr_stats = of.ofp_stats_request(body=of.ofp_aggregate_stats_request())
  #msg_port_stats = of.ofp_stats_request(body=of.ofp_port_stats_request())
  
  dict = disc.get_svc_dict()
  sw_list = []
  for key,value in dict.items():
    for x in range(len(value)):
      sp,mac_id,ip,sw_id,pt_id = value[x]
      sw = l2m.get_sw_from_dpid(sw_id)
      if sw is not None and sw.connection is not None and sw not in sw_list:
        sw_list.append(sw)
      else:
        if sw is None or sw.connection is None:
          log.error("Failed to Install Stats Requests at Switch [%d] as the Switch Connection is None! ", sw_id)
        #else:
        #  log.error("Failed to Install Stats Requests at Switch [%d] the Switch is not in the list! ", sw_id)
      
  
  #i = 0
  for sw in sw_list:  
    sw.connection.send(msg_flow_stats)
    #sw.connection.send(msg_aggr_stats)
    #sw.connection.send(msg_port_stats)
    #i +=1
    pass
  #log.warning("Sent %i flow/port stats request(s)", i)
  return
  
# handler for timer function that sends the requests to all the
# switches connected to the controller.
def _timer_func ():
  get_and_update_valid_switch_dpid_for_services()
  update_routes()
  
  #if sum(map(len, l2m.svc_flows_table.itervalues())) == 0:
    #log.error("Skip Setting stat requests!") #Note: could have adverse impact in case of flows terminating at the end of test case.
  #  return
  if fmap_sk.get_total_flow_entries_count() == 0: return
  setup_stats_requests()

# Function to evaluate and update SP based on the instantaneous queue size or num of packets processed in this interval.
def evaluate_and_update_sp_for_inst_queue_size(event):
  global vlan_set
  sw_flowstat_dict = {}
  def add_to_swflowstat_dict(port,svc_id,num_packets):
    #global sw_flowstat_dict
    key = (port,svc_id)
    if (False == sw_flowstat_dict.has_key(key)):
      sw_flowstat_dict[key] = num_packets
      return True
    else:
      cur_pkts = sw_flowstat_dict[key]
      sw_flowstat_dict[key] = cur_pkts+num_packets
      #log.warning("Key:%s updated with Packets:%d, Total:%d",key,num_packets, sw_flowstat_dict[key])
      return True
    return False
  
  for f in event.stats:
    if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535:
      index_lst = [i for i,x in enumerate(f.actions) if x.type == of.OFPAT_OUTPUT]
      index = 0
      if not index_lst: continue
      index = index_lst[0]
      out_port = f.actions[index].port
      if f.actions[index].port == of.OFPP_IN_PORT:
        out_port = f.match.in_port
      vlan_id=f.match.dl_vlan
      svc_id = vlan_id & 0x0F
      num_pkts=f.packet_count
      new_flag = add_to_swflowstat_dict(port=out_port,svc_id=svc_id,num_packets=num_pkts)
      vlan_set.add(svc_id)
  
  for (out_port,svc_id), num_packets in sw_flowstat_dict.items():
    if True == core.openflow_discovery.is_edge_port(event.connection.dpid,out_port):
      handle_flow_stats_1(sw_id=event.connection.dpid, port_id=out_port,vlan_id=svc_id,num_pkts=num_packets)
      #log.warning("[1]sw_id:%d port:%d, vlan:%d, TotalPackets:%d",event.connection.dpid,out_port,svc_id, num_packets)
    else:
      handle_flow_stats_2(sw_id=event.connection.dpid, port_id=out_port,vlan_id=svc_id,num_pkts=num_packets)
      #log.warning("[2]sw_id:%d port:%d, vlan:%d, TotalPackets:%d",event.connection.dpid,out_port,svc_id, num_packets)
  return

def check_non_edge_port_flow(dpid,f):
  index_lst = [i for i,x in enumerate(f.actions) if x.type == of.OFPAT_OUTPUT]
  if not index_lst: return False  #continue
  index = index_lst[0]
  out_port = f.actions[index].port
  if f.actions[index].port == of.OFPP_IN_PORT: out_port = f.match.in_port
  if False == core.openflow_discovery.is_edge_port(dpid,out_port):return True
  return False

def check_for_flow_svc(dpid,f,key):
    
    if f.match.nw_tos and f.match.nw_tos != 0:
        #if f.cookie: log.warning("Flow with Cookie set to [%d: %d]", f.cookie,key)
        return ((f.cookie & 0x0F) == key)
        #return (key in util_sk.get_svc_chain_for_key(f.match.nw_tos))
    
    if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535:
        return (f.match.dl_vlan&0xF == key)
    
    return False
# Function to evaluate flows per service type at each switch and update the count info: affinity of services at each switch
def evaluate_and_set_svc_flow_count_info(event):
  dpid = event.connection.dpid
  for key in disc.get_svc_dict().keys():
    count_vlan_flows_all = [f for f in event.stats if True == check_for_flow_svc(dpid,f,key) and True==check_non_edge_port_flow(dpid,f)]
    if count_vlan_flows_all:
        disc.set_flow_count_info_for_svc_on_sw(sw_dpid = event.connection.dpid, svc_id=key,flow_count=len(count_vlan_flows_all))
  return
def record_flow_ultilization_info(sw_dpid, flow_stats):
    
    flow_key = l2m.get_flow_key_from_match(flow_stats.match)
    if not flow_key in l2m.flow_info_tbl.keys():
        log.debug("FlowKey[%s] not found in Flow Info Table!",flow_key)
        return
    if (flow_stats.match.nw_proto != ipv4.TCP_PROTOCOL and flow_stats.match.nw_proto != ipv4.UDP_PROTOCOL):
        log.debug("Skipping Non TCP/UDP Flow Statistics!")
        return
        
    flow_stats_record = _get_flow_stat_info(flow_key)
    if flow_stats_record is None:
        flow_stats_record = FlowStatsInfo(sw_dpid, flow_key, flow_stats)
        _set_flow_stat_info(flow_key, flow_stats_record)
    else:
        flow_stats_record.update_data(flow_stats)
    #flow_stats_record.get_flow_stats()
        
    return
def update_flow_utiliztion_info(event):
  for f in event.stats:
    #log.warning (" port_stat in event.stats is: %s", p)
    index_lst = [i for i,x in enumerate(f.actions) if x.type == of.OFPAT_OUTPUT]
    out_port =  f.actions[index_lst[0]].port
    if True == core.openflow_discovery.is_edge_port(event.connection.dpid,out_port):#f.match.in_port
      record_flow_ultilization_info(event.connection.dpid, f)
    else:
      log.debug("SKIP Non Edge Switch!! For Sw:Port[%d:%d], BYTE,PKT Count[%s, %s], Time[%s,%s]", event.connection.dpid, f.match.in_port, f.byte_count, f.packet_count, f.duration_sec, f.duration_nsec)
      pass
  return
def _handle_flowstats_received_new (event):
  #log.error("FlowStatsReceived from %s:%s", dpid_to_str(event.connection.dpid), time.time())

  #Update local flow_utlization_info <Need support functions in l2_multi>
  #update_flow_utiliztion_info()
  
  # Update the switch flow affinity matrix
  evaluate_and_set_svc_flow_count_info(event)
  
  #Un-Comment this if want to use Shadow Price update based on Flow Count and Not Queue Size
  # If 'return' is commented, then sp is evaluated by below function. Also ensure the Shadow Price update based on FlowAdd/deletion is commented by ensuring NOP (return) in disc.update_shadow_price_2()
  return
  
  # logic for code below is to evaluate and update SP based on the instantaneous queue size or num of packets processed in this interval.
  evaluate_and_update_sp_for_inst_queue_size(event)
  return

def record_port_ultilization_info(sw_dpid, port_stats):
    port_stats_record = _get_port_stat_info(sw_dpid, port_stats.port_no)
    if port_stats_record is None:
        port_stats_record = PortStatsInfo(sw_dpid, port_stats.port_no, port_stats)
        _set_port_stat_info(sw_dpid, port_stats.port_no, port_stats_record)
    else:
        port_stats_record.update_data(port_stats)
    port_stats_record.get_port_stats()
        
    #link = disc.get_link_from_sw_and_port(sw_dpid, port_stats.port_no)
    #link_bw_record = _get_link_stat_info(link)
    #if link_bw_record is None:
        
    return
def _handle_portstats_received (event):
    """
    struct ofp_port_stats {
    uint16_t port_no;
    uint8_t pad[6];          /* Align to 64-bits. */
    uint64_t rx_packets;     /* Number of received packets. */
    uint64_t tx_packets;     /* Number of transmitted packets. */
    uint64_t rx_bytes;       /* Number of received bytes. */
    uint64_t tx_bytes;       /* Number of transmitted bytes. */
    uint64_t rx_dropped;     /* Number of packets dropped by RX. */
    uint64_t tx_dropped;     /* Number of packets dropped by TX. */
    uint64_t rx_errors;      /* Number of receive errors.  This is a super-set
                                of receive errors and should be great than or
                                equal to the sum of all rx_*_err values. */
    uint64_t tx_errors;      /* Number of transmit errors.  This is a super-set
                                of transmit errors. */
    uint64_t rx_frame_err;   /* Number of frame alignment errors. */
    uint64_t rx_over_err;    /* Number of packets with RX overrun. */
    uint64_t rx_crc_err;     /* Number of CRC errors. */
    uint64_t collisions;     /* Number of collisions. */
    };
    """
    #stats = flow_stats_to_list(event.stats)
    #log.info("PortStatsReceived from %s: %s", #event, stats) #dpid_to_str(event.connection.dpid), stats)
    log.info("PortStatsReceived from %s:", dpid_to_str(event.connection.dpid))
    for p in event.stats:
        #log.warning (" port_stat in event.stats is: %s", p)
        if True == core.openflow_discovery.is_edge_port(event.connection.dpid,p.port_no):
            log.debug("SKIP Edge Switch!! For Sw:Port[%d:%d], rx_packets:rx_bytes [%d:%d], tx_packets:tx_bytes [%d:%d]", event.connection.dpid, p.port_no, p.rx_packets, p.rx_bytes, p.tx_packets, p.tx_bytes)
            record_port_ultilization_info(event.connection.dpid, p)
        else:
            log.debug("SKIP Non Edge Switch!! For Sw:Port[%d:%d], rx_packets:rx_bytes [%d:%d], tx_packets:tx_bytes [%d:%d]", event.connection.dpid, p.port_no, p.rx_packets, p.rx_bytes, p.tx_packets, p.tx_bytes)
        pass

    return

def get_aggr_flow_stats(sw_id=0):
  global aggr_flow_dict
  if (False == aggr_flow_dict.has_key(sw_id)):
    return None
  return aggr_flow_dict[sw_id]
def handle_aggregate_stats(sw_id=0, aggr_info=None):
  if aggr_info:
    global aggr_flow_dict
    aggr_flow_dict[sw_id] = [aggr_info.packet_count,aggr_info.byte_count,aggr_info.flow_count, time.time()]
  return
def _handle_aggregate_flowstats_received(event):
  #log.info("AggregateStatsReceived from %s: %s, %d, %d, %d", dpid_to_str(event.connection.dpid), event.stats,
  #        event.stats.packet_count, event.stats.byte_count,event.stats.flow_count)
  if event.stats:
    #handle_aggregate_stats(sw_id=event.connection.dpid, aggr_info=event.stats)
    l2m.handle_aggregate_stats(sw_id=event.connection.dpid, aggr_info=event.stats)
  return

def _handle_packetin(event):
  global gEvent
  gEvent = event
  log.info("_handle_packetin from %s, %s", event.port, event.ofp)
  return EventRemove
  
def _handle_FlowRemoved(event):
    '''
    if event.ofp.reason == of.OFPRR_HARD_TIMEOUT or event.ofp.reason == of.OFPRR_IDLE_TIMEOUT:
        msg = of.ofp_flow_mod(command=0)
        msg.priority=3
        msg.match.in_port=3
        msg.match.dl_type=0x800
        msg.hard_timeout=30
        msg.flags=of.OFPFF_SEND_FLOW_REM
        flow_removed = event.ofp
  
    for entry in self.flow_table.entries:
        if (flow_removed.match == entry.match and flow_removed.priority == entry.priority):
            self.flow_table.remove_entry(entry)
            self.raiseEvent(FlowTableModification(removed=[entry]))
            return EventHalt
    return EventContinue
    '''
    return l2m.update_svc_flow_table_on_flow_removal_event(sw_dpid=event.connection.dpid, event=event)
    

# main functiont to launch the module
def launch (ecn_mode=False, sel_ecn=False):
  from pox.lib.recoco import Timer
  global ecn_mark_mode
  global selective_ecn_mark
  ecn_mark_mode = str_to_bool(ecn_mode)
  selective_ecn_mark = str_to_bool(sel_ecn)
  
  log.warning("1ecn_mark_mode is set to [%s], selective_ecn_mark is set to [%s]", ecn_mark_mode, selective_ecn_mark)
  # attach handsers to listners
  core.openflow.addListenerByName("FlowStatsReceived",
    _handle_flowstats_received_new)
    
  core.openflow.addListenerByName("PortStatsReceived", 
    _handle_portstats_received)
    
  core.openflow.addListenerByName("AggregateFlowStatsReceived", 
    _handle_aggregate_flowstats_received)
    
  #core.openflow.addListenerByName("FlowRemoved",
  #  _handle_FlowRemoved,priority=65000)
  
  core.openflow.addListenerByName("PacketIn", 
    _handle_packetin, 0)
  
  # timer set to execute every five seconds
  Timer(stats_interval, _timer_func, recurring=True)
  