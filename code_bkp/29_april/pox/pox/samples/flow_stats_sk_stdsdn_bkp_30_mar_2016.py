#!/usr/bin/python
# Copyright 2012 William Yu
# wyu@ateneo.edu
#
# This file is part of POX.
#
# POX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# POX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with POX. If not, see <http://www.gnu.org/licenses/>.
#

"""
This is a demonstration file created to show how to obtain flow 
and port statistics from OpenFlow 1.0-enabled switches. The flow
statistics handler contains a summary of web-only traffic.
"""

# standard includes
from pox.core import core
from pox.lib.util import dpid_to_str
import pox.openflow.libopenflow_01 as of
# include as part of the betta branch
from pox.openflow.of_json import *
import time
log = core.getLogger()
import csv
import os.path
# To access and update the shadow price for service dictionary entries
import pox.openflow.discovery_sk as disc
# To access the Switch and setup the flows proactively
import pox.forwarding.l2_multi_sk_stdsdn as l2m
#import core.openflow_discovery as Discovery
from sets import Set
from pox.lib.revent import EventRemove,EventHaltAndRemove

vlan_set = Set([])
#sw_id_vlan_set = Set([])
THRESHOLD_PACKETS = 200 #500 #50
stats_interval =  5 #5 #2 #10 #1 
log_file = 'logsp.csv'
gEvent = None
switch_flow_info = {}   #{'dpid1':[[svc1,port1, packets],[svc1,port2, packets], [svc2, port1, packets]] 'dpid2':[[...]]}
#Ideal Table {'dpid1':[[svc1,port1,packets, time_stamp,clearflows_flag],[svc1,port2, packets, time_stamp,clearflows_flag], [svc2, port1, packets,time_stamp,clearflows_flag]] 'dpid2':[[...]]}
#Current {'dpid1':[[svc1,packets,time_stamp],[svc2, packets, time_stamp]] 'dpid2':[[...]]}
#aggr_flow_dict = {} # {'dpid1':[pkt_count,byte_count,flow_count], 'dpid2':[pkt_count,byte_count,flow_count]}
last_logged_time = 0
base_time = 0
def log_sp_and_flow_data(vlan_id=0,out_port=0):
  global last_logged_time
  global stats_interval
  global base_time
  
  cur_time = time.time()
  if ((cur_time - last_logged_time) < stats_interval):
    return
  last_logged_time = cur_time

  if base_time == 0:
    base_time = cur_time
  time_diff = int(cur_time - base_time)
  
  dict = disc.get_svc_dict()
  #log.debug("At[%s]: Dictionary contents:%s", cur_time, dict)
  log_file_func = log_file.split('.')[0] + '_' + str(vlan_id) +'.'+log_file.split('.')[1]
  with open(log_file_func, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    for key,value in dict.items():
      if key == vlan_id:
        log.debug("At[%s]: Dictionary for Key[%d] is:%s", cur_time, key, value)
        for x in range(len(value)):
          sp,mac_id,ip,sw_id = value[x]
          logWriter.writerows([[str(time_diff), str(key), str(sw_id), str(mac_id), str(sp), str(out_port)]])

def add_flows_proactively_on_switch(dpid=0, vlan_id=0,event=gEvent):
  sw = l2m.get_sw_from_dpid(dpid)
  host_eth_addr = "00:00:00:00:00:01"
  dest =l2m.get_mac_map(host_eth_addr)
  if dest is None:
    log.warning("Failed to install proactive path on switch%d for svc%d", dpid,vlan_id)
    return False
  if sw:
    match = of.ofp_match()
    #event.port = ???
    sw.install_path_sk(vlan_id=vlan_id, dst_sw=dest[0], last_port=dest[1], match=match, event=event)
  return True

def clear_flows_on_switch(dpid=0, vlan_id=0,out_port=0):
  #log.debug("Func[%d] Triggered Clear Flows on switch [%s]", vlan_id,(dpid_to_str(dpid)))
  
  #msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE_STRICT)
  msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE)
  msg.match.dl_vlan = vlan_id
  #msg.match.out_port = out_port
  msg.match.in_port = None #in_port #event.port #None
  msg.match.nw_src = None
  msg.match.nw_dst = None
  msg.match.dl_src = None
  msg.match.dl_dst = None
  
  #msg = of.ofp_flow_mod(command=of.OFPFC_DELETE)
  #core.openflow.sendToDPID(dpid, msg)
  for switch in core.openflow._connections.values():

    # Base method II: clear flows on all the switches < to ensure all get to see the new path > : This also is seen results in loops on some occasions (in flight packets fwd'ed with differernt rules at different switches.)
    #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
    
    # Initial Method I: Clear flows on only the matching switches: This results into Loops
    #switch.send(msg)
    #if switch.dpid == dpid:
      #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d] by msg=%s", switch.dpid,vlan_id, msg)
      #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
      #switch.send(msg)
    
    # Method III: (Can it result in loops? looks likely... )
    # Send clear on only those switches that have the flow defined for the vlan_id (regardless of out_port)
    if ( (switch_flow_info.has_key(switch.dpid)) and (True == (any(vlan_id== x[0] for x in switch_flow_info[switch.dpid]))) ):
      log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
      switch.send(msg)
      # Also try instantaneous proactive flow setup
      #add_flows_proactively_on_switch(dpid=switch.dpid, vlan_id=vlan_id)
    else:
      # Ignore this switch
      pass
  return True

def handle_clear_flows():
  #global sw_id_vlan_set
  #for i,x in enumerate(sw_id_vlan_set):
  #  sw_id,vlan_id = x[0],x[1]
  #  clear_flows_on_switch(dpid=sw_id, vlan_id=vlan_id)
  #sw_id_vlan_set.clear()
  
  global switch_flow_info
  svc_set = Set()
  for sw_id, svc_flow_list in switch_flow_info.items():
    #if True == (any( == x[0] for x in svc_flow_list))
    for i,x in enumerate(svc_flow_list):
      if True == x[4]:
        svc_set.add((sw_id,x[0],x[1]))
        #switch_flow_info[sw_id][i] = [x[0],x[1],0,time.time(), False]
        switch_flow_info[sw_id][i] = [x[0],x[1],x[2],time.time(), False]
  
  for i,x in enumerate(svc_set):
    #clear_flows_on_switch(dpid=x[0], vlan_id=x[1],out_port=x[2])
    l2m.redirect_routes(svc_id=x[1], sw_dpid=x[0],out_port=x[2])
    pass

  if svc_set:
    svc_set.clear()
    #l2m.clear_next_svc_flow_table()

  return
  
def check_and_update_switch_flow_table(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  if (False == switch_flow_info.has_key(sw_id)):
    switch_flow_info[sw_id] = [[vlan_id,port_id,0,time.time(),False]]
    return True
  else:
    if (False == (any(vlan_id== x[0] and port_id == x[1] for x in switch_flow_info[sw_id]))):
      switch_flow_info[sw_id].append([vlan_id, port_id, 0,time.time(),False])
      return True
    pass
  return False
  
def handle_flow_stats_1(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  #global sw_id_vlan_set
  #log.warning("handle_flow_stats_1(): SW:PORT[%d:%d], %d, %d :", sw_id,port_id, vlan_id, num_pkts)
  #if vlan_id:
    #log.debug("handle_flow_stats_1(): SW:PORT[%d:%d],vlan:%d :", sw_id,port_id, vlan_id)
  new_record = check_and_update_switch_flow_table(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  
  for x in range(0,len(switch_flow_info[sw_id])):
    old_vlan,old_port,old_pkts,o_time,clear_flag = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2],switch_flow_info[sw_id][x][3],switch_flow_info[sw_id][x][4]
    if(vlan_id == old_vlan and port_id == old_port):
      #log.warning("handle_flow_stats_1(): SW:PORT[%d:%d], vlan_id:%d,#old_pkts[%d] #new_pkts[%d]", sw_id,port_id,vlan_id,old_pkts,num_pkts)
      switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
      mac_addr = l2m.get_mac_addr_from_sw_and_port(sw_dpid=sw_id,port_id=old_port)
      if mac_addr is None:
        log.warning("handle_flow_stats_1(): SW:PORT[%d:%d],%d got Invalid MAC Address:", sw_id,old_port, port_id)
      if (num_pkts == old_pkts and new_record is False):
        # Reduce the Shadow Price (no flows observed!)
        found_flag, clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price= -5)
        switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
        #if(clear_flag is True):
          #sw_id_vlan_set.add((sw_id,vlan_id))
          #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
          #pass
      elif (num_pkts > old_pkts and (num_pkts-old_pkts) > THRESHOLD_PACKETS):
        found_flag,clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price= 5*((num_pkts-old_pkts)/THRESHOLD_PACKETS))
        switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
        if(clear_flag is True):
          #sw_id_vlan_set.add((sw_id,vlan_id))
          #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
          pass
      else:
        # Save the record and Print the number of Flows, if 0, no action!!
        pass
      break
    elif (vlan_id == old_vlan): # and port_id != old_port
      #same vlan serviced on different ports:
      pass
    else:
      continue
  log_sp_and_flow_data(vlan_id=vlan_id,out_port=port_id)
  return

# Readdress this Function @Jun21  
def handle_flow_stats_2(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  #global sw_id_vlan_set
  
  if False == disc.check_svc_at_sw_dpid(svc_id = vlan_id, sw_id=sw_id):
    #log.warning("Skip!! As this switch[%d] doesn't host the service function[%d]", sw_id, vlan_id)
    return
  #log.warning("Process!! This switch[%d] also hosts the service function[%d]", sw_id, vlan_id)
  
  new_record = check_and_update_switch_flow_table(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  
  if new_record is False:
    for x in range(0,len(switch_flow_info[sw_id])):
      old_vlan,old_port,old_pkts,o_time,clear_flag = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2],switch_flow_info[sw_id][x][3],switch_flow_info[sw_id][x][4]
      if(vlan_id == old_vlan and port_id == old_port):
        switch_flow_info[sw_id][x] = [vlan_id,port_id,num_pkts,time.time(),clear_flag]
        break
  
  #tricky now, since the switch that is serving the instance might now be redirecting flows to some other switch's service instance
  # Soln: add new Function in Discovery or L2 Multi to check if the DPID also hosts the service instance
  return
  
  mac_addr=l2m.get_mac_addr_from_sw_and_port(sw_dpid=sw_id,port_id=old_port)
  if mac_addr is None:
    return
    
  found_flag,clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price=-10)
  switch_flow_info[sw_id][x] = [vlan_id,port_id,0,time.time(),clear_flag]
  if(found_flag is True): # Indicates Switch also hosts the function and currently may not be getting used!!
    #sw_id_vlan_set.add((sw_id,vlan_id))
    #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
    pass


  log_sp_and_flow_data(vlan_id=vlan_id,out_port=port_id)
  return

def check_for_aged_flows(vlan_id=0):
  
  global switch_flow_info
  
  clear_flag = False
  #cur_time = time.time()

  for sw_id,value in switch_flow_info.items():
    for x in range(len(value)):
      old_vlan,old_port,old_pkts,o_time,clear_flag = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2],switch_flow_info[sw_id][x][3],switch_flow_info[sw_id][x][4]
      cur_time = time.time()
      if True == core.openflow_discovery.is_edge_port(sw_id,old_port):
        if(vlan_id == old_vlan and (cur_time - o_time) >= 2*stats_interval):
          mac_addr= l2m.get_mac_addr_from_sw_and_port(sw_dpid=sw_id,port_id=old_port)
          if mac_addr is None:
            switch_flow_info[sw_id][x] = [vlan_id,old_port,0,time.time(),clear_flag]
            continue
          log.warning("At:%s Aging values for switch[%d], function[%d]", cur_time,sw_id,vlan_id)
          found_flag,clear_flag = disc.update_shadow_price(mac_addr=mac_addr,svc_id=vlan_id,shadow_price=-10)
          switch_flow_info[sw_id][x] = [vlan_id,old_port,0,time.time(),clear_flag]
          #if(clear_flag is True):
            #sw_id_vlan_set.add((sw_id,vlan_id))
            #clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
            #pass
  return

def check_for_all_aged_flows():
  global vlan_set
  for vlan_id in vlan_set:
    check_for_aged_flows(vlan_id=vlan_id)
  #vlan_set.clear()

def get_and_update_valid_switch_dpid_for_services():
  dict = disc.get_svc_dict()
  for key,value in dict.items():
    for x in range(len(value)):
      sp,mac_id,ip,sw_id = value[x]
      #log.warning("Values [%s, %s, %s %d] ", sp,mac_id,ip,sw_id)
      dest = l2m.get_mac_map(EthAddr(mac_id))
      if dest is None or dest[0] is None:
        #log.warning("No Destination for Values [%s, %s, %s %d] ", sp,mac_id,ip,sw_id)
        continue
      sw,port_id = dest[0],dest[1]
      sw_dpid = sw.dpid
      if sw_dpid != sw_id:
        #log.info("Values [%s, %s, %s %d] to be changed to %d", sp,mac_id,ip,sw_id, sw_dpid)
        disc.setdpid_for_svc(svc_id=key,mac_addr=mac_id,sw_id=sw_dpid,port_id=port_id)
  return

def check_for_redirection_2():
  svc_bringdown_list = disc.check_for_svc_bring_down()
  for svc_id,instance in svc_bringdown_list:
    mac = instance[1]
    _mac = EthAddr(mac)
    log.warning("Try to bringdown service Service[%d] at mac[%s] with [%s]", svc_id, _mac, instance)
    l2m.redirect_all_flows_from_this_instance(svc_id=svc_id, svc_mac_addr=_mac, check_for_flows = False)
    ret = disc.bringdown_service_instace_for_svc(svc_id = svc_id, svc_mac_addr =_mac, skip_wait_period=True)
    if ret is True:
      l2m.update_svc_flow_table_on_svc_instance_bringdown(svc_id = svc_id, svc_mac_addr =_mac)
  return
    
  svc_redirection_list = disc.check_for_svc_redirections()
  for svc_id in svc_redirection_list:
    ip_mac_sp_list = core.openflow_discovery.getIP_MAC_and_SP_List_sk(svc_id)
    for ip,mac,sp in ip_mac_sp_list:
      log.warning("check redirection for svc[%d] with mac,sp[%s:%s]", svc_id,mac,sp)
      if int(sp) == 0 or int(sp) < disc.MIN_SP_THRESHOLD:
        _mac = EthAddr(mac)
        log.warning("Try to Redirect flows for Service[%d] towards mac[%s]", svc_id, _mac)
        l2m.redirect_flows_towards_this_instance(svc_id=svc_id, new_svc_mac_addr=_mac,check_for_flows=True)
        break
  return
  
def update_routes():
  check_for_all_aged_flows()
  handle_clear_flows()
  check_for_redirection_2()
  return
# handler for timer function that sends the requests to all the
# switches connected to the controller.
def _timer_func ():
  get_and_update_valid_switch_dpid_for_services()
  update_routes()
  
  msg_flow_stats = of.ofp_stats_request(body=of.ofp_flow_stats_request())
  msg_port_stats = of.ofp_stats_request(body=of.ofp_port_stats_request())
  msg_aggr_stats = of.ofp_stats_request(body=of.ofp_aggregate_stats_request())
  
  dict = disc.get_svc_dict()
  sw_list = []
  for key,value in dict.items():
    for x in range(len(value)):
      sp,mac_id,ip,sw_id = value[x]
      sw = l2m.get_sw_from_dpid(sw_id)
      if sw is not None and sw.connection is not None and sw not in sw_list:
        sw_list.append(sw)
      else:
        if sw is None or sw.connection is None:
          log.error("Failed to Install Stats Requests at Switch [%d] as the Switch Connection is None! ", sw_id)
        #else:
        #  log.error("Failed to Install Stats Requests at Switch [%d] the Switch is not in the list! ", sw_id)
      
  
  #i = 0
  for sw in sw_list:  
    sw.connection.send(msg_flow_stats)
    sw.connection.send(msg_aggr_stats)
    #sw.connection.send(msg_port_stats)
    #i +=1
  #log.warning("Sent %i flow/port stats request(s)", i)
      
  #for connection in core.openflow._connections.values():
  #  connection.send(of.ofp_stats_request(body=of.ofp_flow_stats_request()))
  #  #connection.send(of.ofp_stats_request(body=of.ofp_port_stats_request()))
  #  connection.send(of.ofp_stats_request(body=of.ofp_aggregate_stats_request()))
  #  pass
  #log.debug("Sent %i flow/port stats request(s)", len(core.openflow._connections))
  
  # Test
  #sw = l2m.get_sw_from_dpid(1)
  #log.info ("Retrieved switch info for:%s, %d", sw.__repr__(),sw.is_holding_down)

# handler to display flow statistics received in JSON format
# structure of event.stats is defined by ofp_flow_stats()

def _handle_flowstats_received_new (event):
  log.info("FlowStatsReceived from %s:", dpid_to_str(event.connection.dpid))

  #stats = flow_stats_to_list(event.stats)
  for key in disc.get_svc_dict().keys():
    count_vlan_flows_all = [f for f in event.stats if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535 and f.match.dl_vlan&0xF == key]
    if count_vlan_flows_all:
      #log.error("Number of Flows on Switch [%d] for svc [%d] is [%d]", event.connection.dpid, key, len(count_vlan_flows_all)) #len(event.stats), len(stats))
      #disc.set_flow_count_info_for_svc_on_sw(sw_dpid = event.connection.dpid, svc_id=key,flow_count=len(count_vlan_flows_all))
      
      #Additionally now ensure that we log only for the flows that are on the non edge ports
      count_vlan_flows_rest = []
      for f in count_vlan_flows_all:
        index_lst = [i for i,x in enumerate(f.actions) if x.type == of.OFPAT_OUTPUT]
        if not index_lst: continue
        index = index_lst[0]
        # f.actions[0] can be 'ofp_action_vlan_vid' f.actions[0/1] can be 'ofp_action_output'
        out_port = f.actions[index].port
        if f.actions[index].port == of.OFPP_IN_PORT:
          out_port = f.match.in_port
        if False == core.openflow_discovery.is_edge_port(event.connection.dpid,out_port):
          count_vlan_flows_rest.append(f)
      if count_vlan_flows_rest: # Interetingly without the if case, the edge port switch would be written with value 0: which might also be useful
        disc.set_flow_count_info_for_svc_on_sw(sw_dpid = event.connection.dpid, svc_id=key,flow_count=len(count_vlan_flows_rest))
  return
  
  global vlan_set
  sw_flowstat_dict = {}
  def add_to_swflowstat_dict(port,svc_id,num_packets):
    #global sw_flowstat_dict
    key = (port,svc_id)
    if (False == sw_flowstat_dict.has_key(key)):
      sw_flowstat_dict[key] = num_packets
      return True
    else:
      cur_pkts = sw_flowstat_dict[key]
      sw_flowstat_dict[key] = cur_pkts+num_packets
      #log.warning("Key:%s updated with Packets:%d, Total:%d",key,num_packets, sw_flowstat_dict[key])
      return True
    return False
  
  for f in event.stats:
    if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535:
      index_lst = [i for i,x in enumerate(f.actions) if x.type == of.OFPAT_OUTPUT]
      index = 0
      if not index_lst: continue
      index = index_lst[0]
      out_port = f.actions[index].port
      if f.actions[index].port == of.OFPP_IN_PORT:
        out_port = f.match.in_port
      vlan_id=f.match.dl_vlan
      svc_id = vlan_id & 0x0F
      num_pkts=f.packet_count
      new_flag = add_to_swflowstat_dict(port=out_port,svc_id=svc_id,num_packets=num_pkts)
      vlan_set.add(svc_id)
  
  for (out_port,svc_id), num_packets in sw_flowstat_dict.items():
    if True == core.openflow_discovery.is_edge_port(event.connection.dpid,out_port):
      handle_flow_stats_1(sw_id=event.connection.dpid, port_id=out_port,vlan_id=svc_id,num_pkts=num_packets)
      #log.warning("[1]sw_id:%d port:%d, vlan:%d, TotalPackets:%d",event.connection.dpid,out_port,svc_id, num_packets)
    else:
      handle_flow_stats_2(sw_id=event.connection.dpid, port_id=out_port,vlan_id=svc_id,num_pkts=num_packets)
      #log.warning("[2]sw_id:%d port:%d, vlan:%d, TotalPackets:%d",event.connection.dpid,out_port,svc_id, num_packets)
  #return
  
def _handle_flowstats_received (event):
  #stats = flow_stats_to_list(event.stats)
  #log.info("FlowStatsReceived from %s: %s", #event, event.stats)
  #  dpid_to_str(event.connection.dpid), stats)

  global vlan_set
  for f in event.stats:
    #log.warning (" F in event.stats is: %s", f)
    #if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535 and f.actions[0].port == 1:
    if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535:
      #log.debug("[%d:%s] vlan_id:[%d]:: Bytes:%s, (%s packets), action:%s",event.connection.dpid,dpid_to_str(event.connection.dpid),f.match.dl_vlan, f.byte_count, f.packet_count, f.actions[0])
      vlan_set.add(f.match.dl_vlan)
      index_lst = [i for i,x in enumerate(f.actions) if x.type == of.OFPAT_OUTPUT]
      index = 0
      if not index_lst: continue
      index = index_lst[0]
      out_port = f.actions[index].port
      if f.actions[index].port == of.OFPP_IN_PORT:
        out_port = f.match.in_port
      if True == core.openflow_discovery.is_edge_port(event.connection.dpid,out_port):
        handle_flow_stats_1(sw_id=event.connection.dpid, port_id=out_port,vlan_id=f.match.dl_vlan,num_pkts=f.packet_count)
      else:
        handle_flow_stats_2(sw_id=event.connection.dpid, port_id=out_port,vlan_id=f.match.dl_vlan,num_pkts=f.packet_count)
      #log.info("FlowStatsReceived from %s: %s", dpid_to_str(event.connection.dpid), stats)
      #pass
    else:
      # Other flows: (ignore)
      pass
  
  #for vlan_id in vlan_set:
  #  check_for_aged_flows(vlan_id=vlan_id)
  #check_for_all_aged_flows()
  
# handler to display port statistics received in JSON format
def _handle_portstats_received (event):
  """
  struct ofp_port_stats {
    uint16_t port_no;
    uint8_t pad[6];          /* Align to 64-bits. */
    uint64_t rx_packets;     /* Number of received packets. */
    uint64_t tx_packets;     /* Number of transmitted packets. */
    uint64_t rx_bytes;       /* Number of received bytes. */
    uint64_t tx_bytes;       /* Number of transmitted bytes. */
    uint64_t rx_dropped;     /* Number of packets dropped by RX. */
    uint64_t tx_dropped;     /* Number of packets dropped by TX. */
    uint64_t rx_errors;      /* Number of receive errors.  This is a super-set
                                of receive errors and should be great than or
                                equal to the sum of all rx_*_err values. */
    uint64_t tx_errors;      /* Number of transmit errors.  This is a super-set
                                of transmit errors. */
    uint64_t rx_frame_err;   /* Number of frame alignment errors. */
    uint64_t rx_over_err;    /* Number of packets with RX overrun. */
    uint64_t rx_crc_err;     /* Number of CRC errors. */
    uint64_t collisions;     /* Number of collisions. */
  };
  """

  #stats = flow_stats_to_list(event.stats)
  #log.info("PortStatsReceived from %s: %s", #event, stats)
  #  dpid_to_str(event.connection.dpid), stats)
  
  for p in event.stats:
    #log.warning (" port_stat in event.stats is: %s", p)
    if True == core.openflow_discovery.is_edge_port(event.connection.dpid,p.port_no):
      log.warning("For Sw:Port[%d:%d], rx_packets:rx_bytes [%d:%d], tx_packets:tx_bytes [%d:%d]", event.connection.dpid, p.port_no, p.rx_packets, p.rx_bytes, p.tx_packets, p.tx_bytes)
    else:
      pass

  return

def get_aggr_flow_stats(sw_id=0):
  global aggr_flow_dict
  if (False == aggr_flow_dict.has_key(sw_id)):
    return None
  return aggr_flow_dict[sw_id]
def handle_aggregate_stats(sw_id=0, aggr_info=None):
  if aggr_info:
    global aggr_flow_dict
    aggr_flow_dict[sw_id] = [aggr_info.packet_count,aggr_info.byte_count,aggr_info.flow_count, time.time()]
  return
def _handle_aggregate_flowstats_received(event):
  #log.info("AggregateStatsReceived from %s: %s, %d, %d, %d", dpid_to_str(event.connection.dpid), event.stats,
  #        event.stats.packet_count, event.stats.byte_count,event.stats.flow_count)
  if event.stats:
    #handle_aggregate_stats(sw_id=event.connection.dpid, aggr_info=event.stats)
    l2m.handle_aggregate_stats(sw_id=event.connection.dpid, aggr_info=event.stats)
  return

def _handle_packetin(event):
  global gEvent
  gEvent = event
  log.info("_handle_packetin from %s, %s", event.port, event.ofp)
  return EventRemove
  
def _handle_FlowRemoved(event):
  #log.warning("_handle_flow_removed from %s, %s", event.connection.dpid, event.ofp)
  #log.warning("_handle_flow_removed from %s", event.connection.dpid)
  l2m.update_svc_flow_table_on_flow_removal_event(sw_dpid=event.connection.dpid, event=event)
  
  #if event.ofp.reason == of.OFPRR_HARD_TIMEOUT or event.ofp.reason == of.OFPRR_IDLE_TIMEOUT:
  #  msg = of.ofp_flow_mod(command=0)
  #  msg.priority=3
  #  msg.match.in_port=3
  #  msg.match.dl_type=0x800
  #  msg.hard_timeout=30
  #  msg.flags=of.OFPFF_SEND_FLOW_REM
  #  flow_removed = event.ofp
  
    #for entry in self.flow_table.entries:
    #  if (flow_removed.match == entry.match
    #      and flow_removed.priority == entry.priority):
    #    self.flow_table.remove_entry(entry)
    #    self.raiseEvent(FlowTableModification(removed=[entry]))
    #    return EventHalt
    #return EventContinue
  return

# main functiont to launch the module
def launch ():
  from pox.lib.recoco import Timer

  # attach handsers to listners
  core.openflow.addListenerByName("FlowStatsReceived",
    _handle_flowstats_received_new)
    #_handle_flowstats_received)
  core.openflow.addListenerByName("PortStatsReceived", 
    _handle_portstats_received) 
  core.openflow.addListenerByName("AggregateFlowStatsReceived", 
    _handle_aggregate_flowstats_received)
  #core.openflow.addListenerByName("FlowRemoved", 
  #  _handle_flow_removed)
  core.openflow.addListenerByName("FlowRemoved",
    _handle_FlowRemoved,priority=65000)
  core.openflow.addListenerByName("PacketIn", 
    _handle_packetin, 0)
  # timer set to execute every five seconds
  Timer(stats_interval, _timer_func, recurring=True)
  