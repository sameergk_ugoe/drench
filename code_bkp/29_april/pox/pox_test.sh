#!/bin/bash
echo " Arguments are:" $array, $len
echo "Args: $@"
echo "Number_of_args: $#"
echo " Argments are : $1, $2, $3, $4, $5"
rsrc_file="./csv/list3.csv"
if [ $# -ge 1 ];
then
  rsrc_file=$1
  echo "updated resource file: $rsrc_file"
fi
if [ $# -ge 2 ];
then
  ecn_mode=$2
  echo "updated ecn_mode: $ecn_mode"
fi
if [ $# -ge 3 ];
then
  sel_ecn=$3
  echo "updated sel_ecn: $sel_ecn"
fi

cleanup() {
#    sudo killall -9 python
#    sudo fuser -k 6633/tcp
    cp controller_logs.txt /home/mininet/drench/results/
    rm controller_logs.txt
    cp *.csv /home/mininet/drench/results/
    #rm *.csv
    #rm -rf /home/mininet/dre/results/* 
    echo "*** cleanup completed ****"
}
ctrlc() {
    cleanup
    echo "*** Exitting!! ****"
    exit
}
trap ctrlc SIGINT

sudo chmod +x pox.py
#sudo ./pox.py samples.topo_sk log.level --WARNING --openflow.discovery_sk=WARNING --forwarding.l2_multi_sk=WARNING --samples.flow_stats_sk=INFO log --file=controller_logs.txt,w
#sudo ./pox.py samples.topo_sk log.level --WARNING --openflow.discovery_sk=WARNING --forwarding.l2_multi_sk=WARNING --samples.flow_stats_sk=WARNING log --file=controller_logs.txt,w
#sudo ./pox.py samples.topo_sk log.level --WARNING --openflow.discovery_sk=WARNING --forwarding.l2_multi_sk=WARNING --samples.flow_stats_sk=INFO log --file=controller_logs.txt,w
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log log.level --INFO
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log log.level --ERROR
#sudo ./pox.py openflow.discovery_sk forwarding.l2_multi_sk samples.flow_stats_sk samples.pretty_log log.level --INFO

#cp -rf ~/pox_wm/pox ~/pox_d/
#rename "s/\.csv$/\_$(date +%H-%M-%S)\.csv/" *.csv
#mv *.csv bkp/
#sudo ./pox.py --verbose openflow.discovery_sk --rsrc_file=$rsrc_file forwarding.l2_multi_sk_test samples.flow_stats_sk samples.pretty_log openflow.keepalive log.level --WARNING log --file=controller_logs.txt,w
sudo ./pox.py --verbose openflow.discovery_sk_bt --rsrc_file=$rsrc_file forwarding.l2_multi_sk_bt samples.pretty_log log.level --DEBUG log --file=controller_logs.txt,w
#sudo python pox.py --verbose log.level --DEBUG openflow.discovery_sk forwarding.l2_multi_sk
