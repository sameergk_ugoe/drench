#!/usr/bin/python

import csv
import time
from threading import Thread

header_names = {}
if_monitor_interval = 1
if_monitor_log_file = "iface_usage_details.csv"
        
def log_if_usage_bytes(iface_name=None, ttl_bytes=0, rx_bytes=0, tx_bytes=0, ttl_pkts=0, rx_pkts=0, tx_pkts=0, cur_time=0, xy_file_func="iface_blog.csv"):
    #xy_file_func = xy_file.split('.')[0] + '_' + str(face_name) +'.'+xy_file.split('.')[1]
    with open(xy_file_func, 'ab') as csvfile4:
        xyWriter = csv.writer(csvfile4,delimiter=',')
        xyWriter.writerows([[str(cur_time), str(ttl_bytes), str(rx_bytes), str(tx_bytes), str(ttl_pkts),str(rx_pkts), str(tx_pkts)]])
    return

def log_if_usage_rate(iface_name=None, ttl_Kbps=0, rx_Kbps=0, tx_Kbps=0, xy_file_func="iface_rlog.csv",cur_time=0):
    #xy_file_func = xy_file.split('.')[0] + '_' + str(face_name) +'.'+xy_file.split('.')[1]
    with open(xy_file_func, 'ab') as csvfile4:
        xyWriter = csv.writer(csvfile4,delimiter=',')
        xyWriter.writerows([[str(cur_time), str(ttl_Kbps), str(rx_Kbps), str(tx_Kbps)]])
    return

#intf_usage_params = [rx_bytes, rx_packets, rx_errs, rx_drop, rx_fifo, rx_frame, rx_compressed, rx_multicast, tx_bytes, tx_packets, tx_errs, tx_drop, tx_fifo, tx_frame, tx_compressed, tx_multicast]
def parse_intf_usage_info(intf_usage_params):
    rxb = intf_usage_params[0]
    rxp = intf_usage_params[1]
    txb = intf_usage_params[8]
    txp = intf_usage_params[9]
    ttlb = rxb + txb
    ttlp = rxp + txp
    #print "ttlb=%d, rxb=%d, txb=%d, ttlp=%d, rxp=%d, txp=%d" %(ttlb,rxb,txb,ttlp,rxp,txp)
    return (ttlb,rxb,txb,ttlp,rxp,txp)
    
def get_if_usage_details(iface_name=None, Output_file=None, cur_time=0):
    global header_names
    
    dev = open("/proc/net/dev", "r").readlines()
    
    if header_names is None:
        header_line = dev[1]
        header_names = header_line[header_line.index("|")+1:].replace("|", " ").split()

    values={}
    for line in dev[2:]:
        intf = line[:line.index(":")].strip()
        values[intf] = [int(value) for value in line[line.index(":")+1:].split()]

        #print intf,values[intf]

    if iface_name and iface_name in values.keys():
        ttlb,rxb,txb,ttlp,rxp,txp = parse_intf_usage_info(values[iface_name])
        if Output_file is not None:
            #log_if_usage_bytes(iface_name=None, ttl_bytes=ttlb, rx_bytes=rxb, tx_bytes=txb, ttl_pkts=ttlp, rx_pkts=rxp, tx_pkts=txp, cur_time=cur_time)
            pass
        return [ttlb,rxb,txb,ttlp,rxp,txp]
    
    return values

def start_if_monitor(iface_name, interval_in_sec=0,log_file="usage_rlog.csv"):
    if iface_name is None:
        return 
    if interval_in_sec <= 0:
        interval_in_sec = if_monitor_interval
    if log_file is None:
        log_file = if_monitor_log_file
    ttl_interval = 0
    start_ttlb, start_rxb, start_txb, start_ttlp, start_rxp, start_txp = get_if_usage_details(iface_name=iface_name)
    while True:
        time.sleep(interval_in_sec)
        end_ttlb, end_rxb, end_txb, end_ttlp, end_rxp, end_txp = get_if_usage_details(iface_name=iface_name)
        
        rxb = (end_rxb - start_rxb)
        txb = (end_txb - start_txb)
        ttlb = txb + rxb
        #ttlb = (end_rxb - start_rxb) + (end_txb - start_txb)
        rxp = (end_rxp - start_rxp)
        txp = (end_txp - start_txp)
        ttlp = txp + rxp
        #ttlp = (end_rxp - start_rxp) + (end_txp - start_txp)
        
        # 1024/8 = 128
        rx_Kbps  = rxb/128/interval_in_sec 
        #rx_Kbps  = (end_rxb - start_rxb)*8/1024/interval_in_sec
        tx_Kbps  = txb/128/interval_in_sec
        #tx_Kbps  = (end_txb - start_txb)*8/1024/interval_in_sec
        ttl_Kbps = rx_Kbps + tx_Kbps
        ttl_interval += interval_in_sec
        
        #log params
        #log2_file = log_file.rsplit('.')[0] + '_blog.'+ log_file.rsplit('.')[1]
        log_if_usage_rate(iface_name=None, ttl_Kbps=ttl_Kbps, rx_Kbps=rx_Kbps, tx_Kbps=tx_Kbps, xy_file_func=log_file,cur_time=ttl_interval)
        #log_if_usage_bytes(iface_name=None, ttl_bytes=ttlb, rx_bytes=rxb, tx_bytes=txb, ttl_pkts=ttlp, rx_pkts=rxp, tx_pkts=txp, xy_file_func=log2_file, cur_time=ttl_interval)
        
        #Update params
        start_ttlb = end_ttlb
        start_rxb  = end_rxb
        start_txb  = end_txb
        start_ttlp = end_ttlp
        start_rxp  = end_rxp
        start_txp  = end_txp
    return
def launch_if_monitor(iface_name=None, interval = 1, log_file="usage_rlog.csv"):
    try:
        global if_monitor_interval
        global if_monitor_log_file
        if interval:
            if_monitor_interval =  interval
        if log_file:
            if_monitor_log_file =log_file

        #t = Thread(group=None,target=start_if_monitor,name="if_mon", args=(interval,log_file), kwargs=dict(iface_name,interval_in_sec=interval,log_file=log_file))
        t = Thread(group=None,target=start_if_monitor,name="if_mon", args=(iface_name, interval,log_file), kwargs={})
        t.start()
        #t.join()
        pass
    except Exception, e:
        print "Exception:", str(e)
    return
if __name__ == '__main__':
    launch_if_monitor(iface_name="h1-eth0", interval=2,log_file="usage_rlog.csv")
    #get_if_usage_details(iface_name="h1-eth0", Output_file="h1_eth0_usage.csv", cur_time=0)
    #get_if_usage_details(iface_name="h1-eth0", Output_file="h1_eth0_usage.csv", cur_time=0)
