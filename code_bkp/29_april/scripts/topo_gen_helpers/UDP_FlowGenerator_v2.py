#!/usr/bin/python

#
# Program to generate cli command list
#

#from mininet.log import setLogLevel
from random import randint
import sys
import csv
import time
import random
import argparse
#UDP_FlowGenerator.py -c 3 -n 100 -t 0 -b 10.0.0.0 -bh 21 -mh 16
#UDP_FlowGenerator.py -c 3 -n 100 -t 0 -o dummy.txt -mf 6 -b 10.0.0.0 -mh 5

num_flows = 10
flow_type = 0
out_file = "flows.txt"
distribution = 0
ecn_mode = 0
base_ip_addr = "10.0.0.0"
max_hosts = 30
base_host_id = 1
def parse_named_args():
    global num_flows, flow_type, out_file, base_ip_addr, distribution, ecn_mode, max_hosts, base_host_id
    is_parsed = False
    chain_length = 3
    max_functions = 6
    
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--chain_length', '-c' ,'-l', required=True,type=int, help='Service Chain Length')
    parser.add_argument('--num_flows', '-n' ,'-f', required=True, type=int, help='Number of Flows')
    parser.add_argument('--flow_type', '-t', required=True, type=int, help='Type of Flows <Mice=0/Elephant=1/Mix=2>')
    
    parser.add_argument('--base_ip_addr', '-b', help='base_ip_addr')
    parser.add_argument('--base_host_id', '-bh', type=int, help='Start ID of Host that acts as src/dst for traffic generation')
    parser.add_argument('--distribution', '-d', type=int, help='Distribution Mode')
    parser.add_argument('--max_hosts', '-mh', type=int, help='Maximum Hosts in the system')
    
    parser.add_argument('--max_functions', '-mf', type=int, help='Maximum Number of Service Chain Functions')
    parser.add_argument('--out_file', '-o', help='Output to File')
    parser.add_argument('--ecn', '-e', type=int, help='ECN Mode')
    
    args = parser.parse_args()
    
    if args.chain_length:
        chain_length = args.chain_length
        is_parsed = True
    if args.num_flows:
        num_flows = args.num_flows
        is_parsed = True
    if args.out_file:
        out_file = args.out_file
        is_parsed = True
    if args.out_file:
        out_file = args.out_file
        is_parsed = True
    
    if args.max_functions and args.max_functions > 0 and args.max_functions <15:
        max_functions = args.max_functions
    if args.base_ip_addr:
        base_ip_addr = args.base_ip_addr
    if args.base_host_id:
        base_host_id = args.base_host_id    
    if args.distribution:
        distribution = args.distribution
    if args.max_hosts:
        max_hosts = args.max_hosts
    if args.ecn:
        ecn_mode = args.ecn
    
    return is_parsed

def get_svc_chain_keys():
    keys = [ i for i in xrange(1,256) if  not (i == 0 or i&0x01 or i&0x02==2) ]
    return keys
def get_flow_size_inKB():
    flow_size = 10 
    if flow_type == 0:
        rand_multiplier = randint(1,400) # 10KB to 4 MB
    elif flow_type == 1:
        rand_multiplier = randint(500,50000) # 5MB to 500MB+
    else:
        # change to adapt to mix of traffic pattern
        #rand_multiplier = randint(1,10000) % provide 80% small and 20% large flows
        rand_multiplier = random.triangular(1, 400, 50000)
    flow_size = flow_size*rand_multiplier 
    return flow_size
def get_packet_size():
    pkt_size = 10 
    if flow_type == 0:
        rand_multiplier = 50
    elif flow_type == 1:
        rand_multiplier = 150
    else:
        # change to adapt to mix of traffic pattern
        rand_multiplier = randint(10,150)
    pkt_size = pkt_size*rand_multiplier 
    return pkt_size
def get_packets_per_sec():
    #return 1000
    rand_multiplier = randint(1,10)
    pps = 100*rand_multiplier
    return pps
def generate_flows_for_host(host_id, host_ip, host_file, max_flows_per_host=0):
    hf = open(host_file, "wb")
    
    nflows = 0
    tos_keys = get_svc_chain_keys()
    hosts = [h for h in xrange(base_host_id,base_host_id+max_hosts) if h!= host_id]
    random.shuffle(hosts)
    for h in hosts: #xrange(1,max_hosts+1):
        if h == host_id: continue
        d_ip = base_ip_addr[0:base_ip_addr.rfind('.')] + '.' + str(h)
        
        flows_per_dst = max(1, int(max_flows_per_host/len(hosts)))
        j = 0
        random.shuffle(tos_keys)
        for key in tos_keys:
            tos=int(key)
            fsize = get_flow_size_inKB()
            psize = get_packet_size()
            pktps = get_packets_per_sec()
            rport = 10000 + host_id*100 + h*10 + j 
            cmd = '-T UDP -a ' + str(d_ip) + ' -rp ' + str(rport)
            cmd += ' -b ' + str(tos) + ' -k ' + str(fsize) + ' -C ' + str(pktps) + ' -c ' + str(psize) +  ' -d 50 \n'
            hf.write(cmd)
            nflows +=1
            j+=1
            if j == flows_per_dst:
                break
            if max_flows_per_host and nflows >= max_flows_per_host:
                break
        if max_flows_per_host and nflows >= max_flows_per_host: break
    hf.close()
    return
def generate_all_flows():
    global max_hosts
    max_flows_per_host = 0
    if num_flows <= max_hosts:
        max_flows_per_host = 1
    else:
        max_flows_per_host = int(num_flows/max_hosts)
    for h in xrange(base_host_id,base_host_id+max_hosts):
        h_ip = base_ip_addr[0:base_ip_addr.rfind('.')] + '.' + str(h)
        h_file = str(h_ip) + "_flows.txt"
        generate_flows_for_host(h, h_ip, h_file,max_flows_per_host)
if __name__ == '__main__':
  parse_named_args()
  generate_all_flows()