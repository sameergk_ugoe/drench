#ifndef CONFIG_H
#define CONFIG_H
#define FILE_CLIENTS ".curvetun/clients"
#define FILE_SERVERS ".curvetun/servers"
#define FILE_PRIVKEY ".curvetun/priv.key"
#define FILE_PUBKEY ".curvetun/pub.key"
#define FILE_USERNAM ".curvetun/username"
#define GITVERSION "v0.6.0-71-gf634c74"
#define HAVE_TCPDUMP_LIKE_FILTER 1
#define HAVE_GEOIP 1
#define HAVE_LIBZ 1
#define HAVE_HARDWARE_TIMESTAMPING 1
#define HAVE_TPACKET3 1
#endif /* CONFIG_H */
