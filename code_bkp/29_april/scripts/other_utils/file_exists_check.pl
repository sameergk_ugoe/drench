#!/usr/bin/perl
 
my $ssh_host = "xmodulo@remote_server";
my $file = "/var/run/test.pid";
 
system "ssh", $ssh_host, "test", "-e", $file;
my $rc = $? >> 8;
if ($rc) {
    print "$file doesn't exist\n";
} else {
    print "$file exists\n";
}