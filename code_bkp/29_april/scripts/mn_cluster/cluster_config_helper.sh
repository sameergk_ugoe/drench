#!/bin/bash

# Act as root (assume password less sudo (doesn't work)
#sudo -i

#Add user 'mininet' and assign default password 'mininet'
sudo useradd -m mininet
echo mininet:mininet | sudo chpasswd

#Other alternatives:
#useradd -p \`openssl passwd -1 $PASS\` $USER
#echo -e 'password\npassword\n' | sudo passwd username
#echo -e 'mininet\nmininet\n' | sudo passwd mininet
#echo thePassword | passwd theUsername --stdin

#Edit user previliges to allow passwordless sudo acces
sudo sh -c "echo 'mininet ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers"

#Edit the SSHD and restart service to enable ETHERNET Tunnels
sudo sh -c "echo 'PermitTunnel yes' >> /etc/ssh/sshd_config"
sudo service ssh restart

#Copy the Emulab Login keys to mininet user directory
sudo mkdir -p /home/mininet/.sshd
sudo rm /home/mininet/.sshd/authorized_keys
sudo touch  /home/mininet/.sshd/authorized_keys
sudo chmod 664 /home/mininet/.sshd/authorized_keys
sudo cat /users/sameergk/.ssh/authorized_keys >> /home/mininet/.sshd/authorized_keys
sudo chown -R mininet /home/mininet/.sshd/

#Swith to user mininet (Unfortunately there is no way to get around this, have to manually run it on individual terminals)
#su - mininet

#Cluster setup (only after logging into user mininet)
#cluster_nodes="192.168.56.1 192.168.56.2 192.168.56.3 192.168.56.4"
#cd /local/mininet/util
#sudo python clustersetup.sh -p cluster_nodes


#Try1: using expect tool <not good! :(
##!/usr/bin/expect -f
#log_file /tmp/status.log
#set timeout 2
#spawn su - y
# log_user 0
#   expect "assword: "
#   send "password\r"
#   expect "*> "
# log_user 1
#   send "commandme\r"
#   expect "*> "
#   send "exit\r"

#Try2: with separate python script (push this python script to ~/ and try:( "No success!!")
#sudo python /local/drench/scripts/mn_cluster/autouserlogin.py
#sudo cp /local/drench/scripts/mn_cluster/autouserlogin.py ~/
#sudo python ~/autouserlogin.py

##!/usr/bin/python
#import pexpect
#import os
#passwd = "mininet"
#child = pexpect.spawn('su - mininet')
#child.expect('Password:')
#child.sendline(passwd)
#child.expect('$')
#child.interact()