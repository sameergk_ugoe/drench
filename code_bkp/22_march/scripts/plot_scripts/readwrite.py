#!/usr/bin/python
import matplotlib.pyplot as plt
import numpy as np
import csv
import os.path
import scipy as sp
import scipy.stats
from matplotlib.legend_handler import HandlerLine2D
from matplotlib import cm as CM
from os import walk

plt.rcParams.update({'font.size': 18})
params = {'legend.fontsize': 15,
          'legend.linewidth': 2}
plt.rcParams.update(params)




#from array import *
#http://stackoverflow.com/questions/10937918/loading-a-file-into-a-numpy-array-with-python 

########### will be set inside a function 
SDIR = ''
DDIR = ''
###########

#F = array('i',[5,10,15,25])
F = [5,10,16,25,50,75,100,125,150]
F = [100]
nF = len(F)

#SF = [6,7,8,9,10,11,12,13,14,15]
SF = [10,11,12,13,14,15]
nSF = len(SF)

T = [.5, 1, 2, 3, 4]

SUFFIX = "info_logs/"
SUFFIX1 = "tx_rx_logs/"
FNAME = [SUFFIX+"hopcount2_log.csv", "only_vlan_flow_count.txt", SUFFIX+"rdir_log.csv", SUFFIX+"splog_", SUFFIX1+"rx_"]

BASE_DIR = ''  
   
## OURS
SDIR_OURS = ''  
DDIR_OURS = ''  

## SDN
SDIR_SDN = ''  
DDIR_SDN = ''  

## OURS-tree
SDIR_OURS_TREE = ''  
DDIR_OURS_TREE = ''  

## SDN-tree
SDIR_SDN_TREE = ''  
DDIR_SDN_TREE = ''  

FILE_DIR = ['SDIR_OURS', 'SDIR_SDN','SDIR_OURS_TREE', 'SDIR_SDN_TREE']
FILE_TYPE = ['ours','sdn','ours_tree','sdn_tree']

def set_SDIR_DDIR(s,d):
   global SDIR
   global DDIR

   SDIR = s
   DDIR = d



def init():
   ensure_dir(DDIR)

def check_files():
   for f in F:
      for fname in FNAME:
         if "splog_" in fname:
            #print fname + "in splog_"
            for s in SF:
               fopen = SDIR + str(f)+"flows/"+fname+str(s)+".csv"
               if not os.path.isfile(fopen):
                  print "ERROR: %s file doesn't exist" %fopen

         else:
            #print fname + "NOT in splog_"
            fopen = SDIR + str(f)+"flows/"+fname
            if not os.path.isfile(fopen):
               print "ERROR: %s file doesn't exist" %fopen

def ensure_dir(f):
   d = os.path.dirname(f)
   if not os.path.exists(d):
      os.makedirs(d)



def simpleTest_perf():
   #print sourceFiles
   "Create and test a simple network"
   topo = DataCenterPerf(fanout=2, maxlevel=4)
   net = Mininet(topo, link=TCLink)

   net.start()
   print "Dumping host connections"
   dumpNodeConnections(net.hosts)
   print "Testing network connectivity"
   net.pingAll()
   print "Testing bandwidth between h1 and h7"
   h1,h7 = net.get('host1','host7')
   net.iperf((h1,h7))
   net.stop()


def simpleTest():
   #print sourceFiles
   "Create and test a simple network"
   topo = DataCenter(fanout=2, maxlevel=4)
   net = Mininet(topo)
   net.start()
   print "Dumping host connections"
   dumpNodeConnections(net.hosts)
   print "Testing network connectivity"
   net.pingAll()
   net.stop()


def fileread():
   file = open("newfile.txt", "r")
   #print file.readline()
   #print file.readlines()
   for line in file:
      print line

   file.close()

def filewrite():
   file = open("newfile.txt", "w")

   file.write("hello world, in the new file\n")

   file.write("and another line\n")

   file.close()

def filereadw(fname):

   with open(fname) as f:
      for line in f:
         val = line.split(',')
         print val

def plot():


   plt.plot([1,2,3,4])
   plt.ylabel('some numbers')
   #plt.show()
   plt.savefig("plotpdf")
   

def hist():
   mu, sigma = 100, 15
   x = mu + sigma * np.random.randn(10000)

   # the histogram of the data
   n, bins, patches = plt.hist(x, 50, normed=1, facecolor='g', alpha=0.75)


   plt.xlabel('Smarts')
   plt.ylabel('Probability')
   plt.title('Histogram of IQ')
   plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
   plt.axis([40, 160, 0, 0.03])
   plt.grid(True)
   plt.savefig('hist.pdf')

#if __name__ == '__main__':
   # 


def filewrite_val(x):
   fname = "mininet" + str(x) + ".txt"
   print fname
   #return
   file = open(fname, "w")

   file.write("1,2\n")
   file.write("3,4\n")

   file.close()




def test():
   # Read all the files
   val = [5,10,15]
   for i in val:
      fname = "mininet" + str(i) + ".txt"
      print fname
      
      filereadw(fname)
   

def fread_splog(dir,fname):
   fsave_arr = SDIR + dir + fname + ".npy"
   fsave_n_inst = SDIR + dir + fname + "_n_inst.pdf"
   fsave_avgsp = SDIR + dir + fname + "_avgsp.pdf"
   
   fopen = SDIR + dir + fname + ".csv"
   if not os.path.isfile(fopen):
      return

   #fopen = "splog_11.csv"
   
   file = open(fopen, 'rb')
   #data = csv.reader(file, delimiter='\t')
   data = csv.reader(file, delimiter=',') 
   table = [row for row in data]
   #print table.shape
   #a = table[0:10][:]
   #   print a
   
   # following works in num of cols is the same for all rows
   numrows = len(table)
   numcols = len(table[0])
   print numrows, numcols

   a = np.empty([numrows,4])
   
   # Trying to extract array of number of service instances
   for i in range(numrows):
      #col[0] is time from zero
      #col[1] is service instance
      #col[2] will be number of instances
      #col[3] will be avg SP
      a[i][0] = int(table[i][0]) - int(table[0][0]) 
      a[i][1] = table[i][1]
      
      # now to extract number of service instances
      b = table[i][2].split(',')
      a[i][2] = len(b)
      #print b

      # now to extract avg shadow price
      sp = 0
      for bi in range(len(b)):
         t = b[bi].split('-')
         #print t
         sp += float(t[1])
         #print sp

      #print sp/len(b)
      a[i][3] = float(sp/len(b))

   np.set_printoptions(threshold='nan', precision=3)
   #print a[:,0], a[:,2]
   #print a[:,0], a[:,3]
   
   # Saving the array
   np.save(fsave_arr,a)

   plt.plot(a[:,0], a[:,2])
   plt.xlabel('Time')
   plt.ylabel('# of instance')
   plt.savefig(fsave_n_inst)
   plt.close()


   plt.plot(a[:,0], a[:,3])
   plt.xlabel('Time')
   plt.ylabel('avg SP')
   plt.savefig(fsave_avgsp)
   plt.close()
   
   

def fread_rdir(dir,fname):
   #fname = "rdir_log.csv"
   fsave_arr = SDIR + dir + fname + ".npy"
   fsave_inst = SDIR + dir + fname + "_inst.pdf"
   fsave_total = SDIR + dir + fname + "_total.pdf"
   fsave_avg = SDIR + dir + fname + "_avg.pdf"
   
   fopen = SDIR + dir + fname + ".csv"
   if not os.path.isfile(fopen):
      return
   
   file = open(fopen, 'rb')
   #data = csv.reader(file, delimiter='\t')
   data = csv.reader(file, delimiter=',') 
   table = [row for row in data]
   #print table.shape
   a = table[0:10][:]
   #print a
   
   
   # following works in num of cols is the same for all rows
   numrows = len(table)
   #numrows = 10 # for debugging

   numcols = len(table[0])
   print numrows, numcols
   
   a = np.empty([numrows,5])
   
   debug = 0
   # Trying to extract array of number of service instances
   for i in range(numrows):
      debug += 1
      #col[0] is time from zero
      #col[1] is number of service instances
      #col[2] instantaneous number of redirections for all services
      #col[3] total number of redirections for all services
      #col[4] average number of redirections for all services
      a[i][0] = int(table[i][0]) - int(table[0][0]) 
            
      # now to extract number of service instances
      b = table[i][1].split('],')
      
      #print b

      #now to extract total redirections for all instances put together
      r = 0
      #print len(b)
      if len(b) == 1 and b[0] == '':
         # take care of empty values in the beginning, but should not exclude the case of single instance being present
         r = 0
         a[i][1] = 0
      else:
         a[i][1] = len(b)
         
         for bi in range(len(b)):
            #print b[bi]
            t = b[bi].split('-[')[1].split(',')
            #print t
            r += int(t[0])
         #print t

      #print "r,len-b=" ,r, len(b)

   #return

   #for i in range(numrows):
      if i == 0:
         #i.e first row, no redirections yet
         a[i][2] = 0
         a[i][3] = 0
         a[i][4] = 0
         
         #print a
         
      else:
         #print "sum =",i,r,  a[:i,2],a[:i,2].sum(), r-a[:i,2].sum()
         a[i][2] = r - a[:i,2].sum()   
         a[i][3] = r # overall total
         a[i][4] = float(r/len(b))# overall avg 

      
   np.set_printoptions(threshold='nan', precision=3)
   print a
   #print a[:,0], a[:,2]
   
   
   # Saving the array
   np.save(fsave_arr,a)

   plt.plot(a[:,0], a[:,2])
   plt.xlabel('Time')
   plt.ylabel('inst # of redirections')
   #plt.show()
   plt.savefig("redir_inst.pdf")
   plt.close()
   
   plt.plot(a[:,0], a[:,3])
   plt.xlabel('Time')
   plt.ylabel('Total # of redirections')
   #plt.show()
   plt.savefig("redir_total.pdf")
   plt.close()

   plt.plot(a[:,0], a[:,4])
   plt.xlabel('Time')
   plt.ylabel('avg # of redirections')
   #plt.show()
   plt.savefig("redir_avg.pdf")
   plt.close()
#def n_service_instance():

def fread_splog_all():
   fplot_inst = DDIR + "splog_inst.pdf"  
   fplot_sp =  DDIR + "splog_sp.pdf"

   farr_inst__max = DDIR + "inst__max.npy"
   farr_inst__avg = DDIR + "inst__avg.npy"
   farr_sp__max = DDIR + "sp__max.npy"
   farr_sp__avg = DDIR + "sp__avg.npy"
  
   
   #dir = "sp_logs/"
   for f in F:
      # 11, 13, 15 are SF
      dir = str(f) + 'flows/'
      
      for sf in SF: 
      
         #fname = "splog_" + str(f) + "f_" + str(sf)
         fname = "splog_" + str(sf)
         #print dir+fname

         #if f == 25 and sf == 11:
         ######################
         ######################fread_splog(dir,fname)
         #m#
         fread_splog(dir,fname)

   
   inst__max = np.empty([nF])
   inst__min = np.empty([nF])
   inst__avg = np.empty([nF])
   sp__max = np.empty([nF])
   sp__min = np.empty([nF])
   sp__avg = np.empty([nF])


   # Now open all arrays and calculate overall avg
   fi = -1
   for f in F:
      fi += 1
      inst_max = np.empty([nSF])
      inst_min = np.empty([nSF])
      inst_avg = np.empty([nSF])
      sp_max = np.empty([nSF])
      sp_min = np.empty([nSF])
      sp_avg = np.empty([nSF])

      ####### Time period when max flows are seen
      arr_fulltime = [] # list of all time
      arr_fullflows = [] # list of all flows
      arr_time = [] # time when max flows are present
      fopen_arr = SDIR + "time_maxflows_alive_" + str(f) + ".npy" 
      fopen_arr_flows = SDIR + "time_and_flows_" + str(f) + ".npy" 
      
      if os.path.isfile(fopen_arr):
         arr_time = np.load(fopen_arr)[:,0]
         arr_fulltime = np.load(fopen_arr_flows)[:,0]
         arr_fullflows = np.load(fopen_arr_flows)[:,1]
      #print "arr_time, arr_fullflows, arr_fulltime =" , arr_time, arr_fullflows, arr_fulltime
      
      #######
      isf = -1
      for sf in SF: 
         isf += 1
         #fname = "splog_" + str(f) + "f_" + str(sf)
         dir = str(f) + 'flows/'
         fname = "splog_" + str(sf)
         fopen = SDIR + dir+fname+".npy"
         if not os.path.isfile(fopen):
            inst_max[isf] = -1
            inst_min[isf] = -1
            inst_avg[isf] = -1
            sp_max[isf] = -1
            sp_min[isf] = -1
            sp_avg[isf] = -1
            continue

         a = np.load(fopen)
         #print a
         # getting max number of instances and SP; min number of instances and SP;average # of instances and SP per unit time
         
         inst_max[isf] = np.amax(a[:,2]) 
         inst_min[isf] = np.amin(a[:,2]) 
         sp_max[isf] = np.amax(a[:,3]) 
         sp_min[isf] = np.amin(a[:,3])
         
         ### time based average
         ######## example code
         ########### Now get all the hops only for those flows
         ###########aa = a[a[:, 1] == hops_flows_max[fi]]
         #test[numpy.logical_or.reduce([test[:,1] == x for x in wanted])]
         t_arr = a[np.logical_or.reduce([a[:,0] == x for x in arr_time])]
         #print "a, t_arr = ", a, t_arr
         inst_avg[isf] = np.mean(t_arr[:,2]) # avg only when max flows are present
         sp_avg[isf] = np.mean(t_arr[:,3]) # avg only when max flows are present
         ###

         print "sf=%s: "%sf , inst_max[isf], inst_min[isf], inst_avg[isf], sp_max[isf], sp_min[isf], sp_avg[isf]
      # ? what should I do
      inst__max[fi] = np.amax(inst_max)
      inst__min[fi] = np.amin(inst_min)
      inst__avg[fi] = np.mean(inst_avg)
      sp__max[fi] = np.amax(sp_max)
      sp__min[fi] = np.amin(sp_min)
      sp__avg[fi] = np.mean(sp_avg)
      print "overall for %s:"%f, inst__max, inst__min, inst__avg, sp__max, sp__min, sp__avg

   np.save(farr_inst__max, inst__max)
   np.save(farr_inst__avg, inst__avg)
   np.save(farr_sp__max, sp__max)
   np.save(farr_sp__avg, sp__avg)
   

   plt.plot(F, inst__max)
   plt.xlabel('# of flows')
   plt.ylabel('Maximum # of instants')
   #plt.axis(F)
   plt.savefig(fplot_inst)
   plt.close()

   plt.plot(F, sp__max)
   plt.xlabel('# of flows')
   plt.ylabel('Maximum SP')
   #plt.axis(F)
   plt.savefig(fplot_sp)
   plt.close()

      
def fread_splog_time(f,dst,src,file_type): 
   #file_dir = [sdir_ours_tree,sdir_sdn_tree]
   #file_type = ['ours_tree','sdn_tree'] 

   #rdir = ddir + str(f)
   #f_NFI = rdir + "_Services_with_redirection"  

   ### These are per service ###
   f_a = dst + "_inst_"     
   f_inst = dst + "_inst_total_" + str(file_type) # Note this is per service 
   f_avgsp = dst + "_inst_avgSP_" + str(file_type) # Note this is per service 
   ###
   f_tot = dst + "_inst_Alltotal_" + str(file_type) ## Note this is for all services put together, the array is saved for future plotting 
   ###

   # folder to plot for each service
   fp_dir = dst + "/rdir_vs_sp/"
   ensure_dir(fp_dir)
   ##

   #f_total = rdir + "_redir_total"     
   #f_avg = rdir + "_redir_avg"  
   
   max_len = -1
   #print dst, src
   for sf in SF: 
      fopen = src + str(sf) + ".csv"
      if not os.path.isfile(fopen):
         print "Error: %s Not present"%fopen 
         continue
         #fread_splog(dir,fname)
         
      file = open(fopen, 'rb')
      data = csv.reader(file, delimiter=',') 
      table = [row for row in data]
      
      # following works in num of cols is the same for all rows
      numrows = len(table)
      numcols = len(table[0])
      #print numrows, numcols

      a = np.empty([numrows,6])
   
      # Trying to extract array of number of service instances
      for i in range(numrows):
         #col[0] is time from zero
         #col[1] is service type
         #col[2] will be number of instances (NOTE: if SP = 0, consider it is down)
         #col[3] will be avg SP
         #col[4] Std-dev in SP among all instances
         #col[5] Max SP among all instances

         a[i][0] = int(table[i][0]) - int(table[0][0]) 
         a[i][1] = table[i][1]
      
         # now to extract number of service instances
         b = table[i][2].split(',')
         #a[i][2] = len(b)
         #print fopen, b, len(b)

         #continue

         # now to extract avg shadow price for all instance
         sp = 0
         len_b = 0 # For counting instances that have > 0 SP
         std_d = []
                  
         for bi in range(len(b)):
            t = b[bi].split('-')
            #print fopen, t,len_t, len(t), t[0]
            if float(t[1]) > 0:               
               len_b += 1
               sp += float(t[1])
               std_d.append(float(t[1]))
            #print sp

         #print sp/len(b)
         a[i][2] = float(len_b)
         if len_b > 0:
            a[i][3] = float(sp/len_b)
            a[i][4] = np.std(std_d)
            a[i][5] = np.max(std_d)
            #print "std_d ", std_d, a[i][4] 
         else:
            a[i][3] = 0
            a[i][4] = 0
            a[i][5] = 0
            
         
         #################################################

      np.set_printoptions(threshold='nan', precision=3)
      #print a[:,0], a[:,2]
      #print a[:,0], a[:,3]
      #print fopen
         
      # Saving the array
      np_a = f_a + "/_Stype_"+str(sf)+"_"+str(file_type)+ ".npy" 
      ensure_dir(f_a+"/")
      #print "np_a = ",np_a
      np.save(np_a,a)
      np.savetxt( f_a + "/_Stype_"+str(sf)+"_"+str(file_type) + ".csv", a, delimiter=",")
      if len(a) > max_len:
         max_len = len(a)


      #### Plot per Serivce 
      #### Plot number of instance vs avg SP vs max SP
      # file to plot
      fp = fp_dir + str(file_type) + "_"+ str(sf) + "_instVSsp" + ".pdf"
      

      ######### Plot
      fig, ax1 = plt.subplots()
      t = a[:,0]
   
      s1 = a[:,2]
      line1, = ax1.plot(t, s1, 'g*', label='inst')
      ax1.set_xlabel('Time')
      # Make the y-axis label and tick labels match the line color.
      ax1.set_ylabel('Total instances', color='g')
      for tl in ax1.get_yticklabels():
         tl.set_color('b')

      legend = plt.legend(loc='upper left', shadow=True)
      

      s2 = a[:,3]
      s2_ = a[:,5]
      ax2 = ax1.twinx()

      #ax2.plot(t, s2, 'b-')
      line2, = ax2.plot(t, s2, 'b-', label='sp_avg')
      #ax2.plot(t, s2_sdn, 'r-')
      line3, = ax2.plot(t, s2_, 'r-', label='sp_max')

      ax2.set_ylabel('SP', color='r')
      for tl in ax2.get_yticklabels():
         tl.set_color('r')

      legend = plt.legend(loc='upper right', shadow=True)
   

      plt.savefig(fp)
      plt.close()
      
      ################33
      #################

   #################################################################   
   # Now, we calculate the totals for all services put together 
   #################################################################   
   tot = np.empty([max_len,4])
   for i in range(len(tot)):
      tot[i,0] = 0
      tot[i,1] = 0
      tot[i,2] = 0
      tot[i,3] = 0

   for sf in SF:
      #print sf
      np_a = f_a + "/_Stype_"+str(sf)+"_"+str(file_type) + ".npy"
      if os.path.isfile(np_a):
         a = np.load(np_a)

         # Copying the time value
         if len(a) == max_len:
            tot[:,0] = a[:,0] 

         # Adding the number of instances
         tot[:,2] = tot[:,2] + a[:,2] 
         
         # Adding the average SP per service
         tot[:,3] = tot[:,3] + a[:,3] 

   for i in range(len(tot)):
      tot[i,3] = float(tot[i,3]/float(len(SF))) # in order to get average SP for all services put together 

         
   np.save(f_tot+".npy",tot)

   #################################################################   
   # plot number of instances per service
   #################################################################   
   for sf in SF:
      #print sf
      np_a = f_a + "/_Stype_"+str(sf)+"_"+str(file_type) + ".npy"
      if os.path.isfile(np_a):
         a = np.load(np_a)
         plt.plot(a[:,0],a[:,2],label=sf)
         #    line1, = plt.plot(ot_a[:,0], ot_a[:,1], 'b*', label='Our-Solution DataCenter')
   
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
   plt.ylabel('Total Number of Intstances per Service')
   plt.title('Total Number of instances per Services vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_inst + ".pdf")
   plt.close()
   #################################################################

   #################################################################   
   # plot Average SP per service
   #################################################################   
   for sf in SF:
      #print sf
      np_a = f_a + "/_Stype_"+str(sf)+"_"+str(file_type) + ".npy"
      if os.path.isfile(np_a):
         a = np.load(np_a)
         plt.plot(a[:,0],a[:,3],label=sf)
         #    line1, = plt.plot(ot_a[:,0], ot_a[:,1], 'b*', label='Our-Solution DataCenter')
   
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
   plt.ylabel('Instantaneous Average SP per Service')
   plt.title('Instantaneous average SP per Service vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_avgsp + ".pdf")
   plt.close()
   #################################################################

def fread_sp_perflow_vs(f,ddir):
   flowcount = f
   file_dir = FILE_DIR #[sdir_ours_tree,sdir_sdn_tree]
   # NOT PLOTTING TREE BASED RESULTS FOR THIS
   
   file_dir = [FILE_DIR[0],FILE_DIR[1]] 
   file_type = FILE_TYPE # ['ours_tree','sdn_tree']

   f_a = ddir + str(f) + "flows/"
   ensure_dir(f_a)

   fname = FNAME[4]
   
   ############# Plotting SDN vs OURS for avg MAX -SP observed
   #fd =   [FILE_DIR[0],FILE_DIR[1]] 
   #f_a = ddir + str(f) + "flows/"
   #f_a + fd + "_yta.npy" # Store yta to save time. Computation is time consuming
   

   f_our = f_a + FILE_DIR[0] +  "_yta.npy"    
   f_sdn = f_a + FILE_DIR[1] +  "_yta.npy"
   max_our = []
   max_sdn = [] 

   if os.path.isfile(f_our):
      max_our = np.load(f_our) 

   if os.path.isfile(f_sdn):
      max_sdn = np.load(f_sdn) 
      

   print "len(max_sdn), len(max_our) ",len(max_sdn), len(max_our)
   #f_a = ddir + str(f) + "flows/"
   f_plot = f_a + "_OURS_VS_SDN_TIME_vs_MAXSP_per_PKT"
   
      
   #Use stored arrays only 
   #Mayutan
   x1 = range(len(max_our))
   y1 = max_our
   x2 = range(len(max_sdn))
   y2 = max_sdn

   line1, = plt.plot(x1,y1, 'b.', label='DRENCH') # alpha=0.8, rasterized=True, 
   #line2, = plt.plot(range(len(y_t_a)), y_t_a, 'ro', alpha=0.5, rasterized=True, label='Avg') 
   line2, = plt.plot(x2,y2 , 'ro', label='D-SDN') #alpha=0.8, rasterized=True, 
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time (s)')
   #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
   plt.ylabel('Avg. Maximum SP')
   #plt.title('SP vs Time')
   #plt.show()
   #plt.axis(F)

   #http://www.astrobetter.com/blog/2014/01/17/slim-down-your-bloated-graphics/
   # py.plot(arr[:,0], arr[:,1], 'o', alpha=0.1, rasterized=True)
   # py.savefig('dots.pdf', dpi=400)

   plt.axis([0, 650, 0, 110])
   fig = plt.gcf()
   fig.tight_layout()
   fig.set_size_inches(10.5,4.4)
   plt.savefig(f_plot + ".pdf", dpi=800)
   plt.close()

   ############# Plotting SDN vs OURS for avg Min -SP observed
   #fd =   [FILE_DIR[0],FILE_DIR[1]] 
   #f_a = ddir + str(f) + "flows/"
   #f_a + fd + "_yta.npy" # Store yta to save time. Computation is time consuming
   

   f_our = f_a + FILE_DIR[0] +  "_zta.npy"    
   f_sdn = f_a + FILE_DIR[1] +  "_zta.npy"

   max_our = []
   max_sdn = [] 

   if os.path.isfile(f_our):
      max_our = np.load(f_our) 

   if os.path.isfile(f_sdn):
      max_sdn = np.load(f_sdn) 
      
   f_plot = f_a + "_OURS_VS_SDN_TIME_vs_MINSP_per_PKT"

      
   #Use stored arrays only 
   #Mayutan
   x1 = range(len(max_our))
   y1 = max_our
   x2 = range(len(max_sdn))
   y2 = max_sdn

   line1, = plt.plot(x1,y1, 'b.',label='DRENCH')# alpha=0.8, rasterized=True, 
   #line2, = plt.plot(range(len(y_t_a)), y_t_a, 'ro', alpha=0.5, rasterized=True, label='Avg') 
   line2, = plt.plot(x2,y2 , 'ro', label='D-SDN') #alpha=0.8, rasterized=True, 
   legend = plt.legend(loc='upper left', shadow=True)
   plt.xlabel('Time (s)')
   #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
   plt.ylabel('Avg. Minimum SP')
   #plt.title('SP vs Time')
   #plt.show()
   #plt.axis(F)

   #http://www.astrobetter.com/blog/2014/01/17/slim-down-your-bloated-graphics/
   # py.plot(arr[:,0], arr[:,1], 'o', alpha=0.1, rasterized=True)
   # py.savefig('dots.pdf', dpi=400)

   plt.axis([0, 650, 0, 110])
   fig = plt.gcf()
   fig.tight_layout()
   fig.set_size_inches(10.5,4.4)
   plt.savefig(f_plot + ".pdf", dpi=800)
   plt.close()



   return
   ###################################################
   ####################################################


def fread_sp_perflow(f,ddir):
   flowcount = f
   file_dir = FILE_DIR #[sdir_ours_tree,sdir_sdn_tree]
   # NOT PLOTTING TREE BASED RESULTS FOR THIS
   
   file_dir = [FILE_DIR[0],FILE_DIR[1]] 
   file_type = FILE_TYPE # ['ours_tree','sdn_tree']

   f_a = ddir + str(f) + "flows/"
   ensure_dir(f_a)

   fname = FNAME[4]

   fdiri = -1
   for fd in file_dir:

      #if "tree" in fd:
       
      #   continue
         

      fdiri += 1

      fdir = eval(fd) # Do this, when I do file_dir = FILE_DIR
      #fdir = fd


      #print "fdir =", fdir
      
      mypath = fdir + str(flowcount) + "flows/" + fname.split('/')[0] + "/" 
      fn_test = fname.split('/')[1]
 
      files = []
      #print "mypath = ", mypath #, fn_test
      for (dirpath, dirnames, filenames) in walk(mypath):
         files.extend(filenames) # Note that this has all file names. I will have to filter rx_ files later
         break
      #print files

      # Read the files. 
      np_dir = f_a + file_type[fdiri] + "/" 
      ensure_dir(np_dir)

      for file_ in files:

         #print "file_ =", file_
         if not fn_test in file_:
            #print "fn_test, file_ ",fn_test, file_
            continue

         fopen = mypath + file_
         if not os.path.isfile(fopen):
            print "Error: file not present : ", fopen
            continue

         np_a = np_dir + file_.split(".")[0] + ".npy"

         if not os.path.isfile(np_a):
            
            file_open = open(fopen, 'rb')
            data = csv.reader(file_open, delimiter=',')
            table = [row for row in data]
            # following works in num of cols is the same for all rows
            numrows = len(table)
            #numrows = 10 # for debugging
         
            numcols = len(table[0])
            #print numrows, numcols
         
            a = np.empty([numrows,6])
            
            #continue
            debug = 0
            # Trying to extract array of number of service instances
            if numrows < 5:
               # If less than x pkts were received, no point in processing this file
               continue

            for i in range(numrows):
               debug += 1
               ## Below is done per packet
               #col[0] is packed_id
               #col[1] is max SP observed
               #col[2] is min SP observed
               #col[3] is avg SP observed
               #col[4] is time when max SP was observed
               #col[5] is time when min SP was observed

               a[i][0] = int(table[i][1]) # Need to find missing packets, reordered packets later 
            
               # now to extract other values
               b = table[i][2].split(',')
               #print b[0]
               #continue
               #print " b[0],i, fopen " , b[0],i, fopen  
            
               a[i][1] = float(b[0]) 
            
               #b[1] cost at src
               #b[2] SP at first_service
               #b[3] SP at second_service
               #b[4] cost at dst
               #b[2], b[3] has SP i.e. important for us
            
               #print b, len(b)
               b_2 = float(b[2].split(':')[1])
               b_3 = float(b[3].split(':')[1])

               b_2_time = float(b[2].split(':')[2])
               b_3_time = float(b[3].split(':')[2])

               #print "b_0, b_2, b_3 ",b[0], b_2, b_3
               a[i][2] = np.amin([b_2,b_3])
               #if np.amax([b_2,b_3]) != float(b[0]):
               #   print file_open, b
               #   continue
               a[i][3] = (float(b_2) + float(b_3)) / 2.0 # NOTE: Modify 2.0 with number of services being executed for each flow
            
               if b_2 <= b_3: 
                  a[i][4] = b_3_time
                  a[i][5] = b_2_time
               else: #if b_3 < b_2: 
                  a[i][4] = b_2_time
                  a[i][5] = b_3_time
               

            np.set_printoptions(threshold='nan', precision=3)
            #print a
         
         
            #print "np_a ",np_a
            np.save(np_a,a)   

         else:
            #print "NPY file exists, skipping computation"
            continue # No need to load it. 
            #Since file exists, lets load it to save time
      # Now get maximum packet id and two arrays(a for time stamps and another for corresponding max_sp). Similarly for min_sp. avg_SP does not have a time stamp, therefore not possible.
     
      y_t = []
      y_sp = []


      z_t = []
      z_sp = []
      
      len_check = 0

      np_path =  f_a + file_type[fdiri] + "/"
      #print "np_path ",np_path 
      files = []
      max_pkt = 0 # take the max pkt id... if pkt id is missing, it means loss
      
      # Store following to save time. Computation is time consuming
      
      f_yt = f_a + fd + "_yt.npy"
      f_ysp = f_a + fd + "_ysp.npy"
      f_zt = f_a + fd + "_zt.npy"
      f_zsp = f_a + fd + "_zsp.npy"

      if os.path.isfile(f_yt) and  os.path.isfile(f_ysp) and  os.path.isfile(f_zt) and  os.path.isfile(f_zsp):
         #print "f_yt and etc exist"
         y_t = np.load(f_yt)
         y_sp =  np.load(f_ysp)
         
         z_t =  np.load(f_zt)
         z_sp =  np.load(f_zsp)
      
      else:

         for (dirpath, dirnames, filenames) in walk(np_path):
            files.extend(filenames) # Note that this has all file names. I will have to filter rx_ files later
            for file_ in files:
               fopen = np_path + file_ 
               a = np.load(fopen)
               max_pkt = np.amax( [np.max(a[:,0]), max_pkt] )
               
               len_check = len_check + len(a[:,0]) 
               y_t.extend(a[:,4])
               y_sp.extend(a[:,1])

               z_t.extend(a[:,5])
               z_sp.extend(a[:,2])

            break
         np.save(f_yt,y_t)
         np.save(f_ysp,y_sp)
         
         np.save(f_zt,z_t)
         np.save(f_zsp,z_sp)
      
      
         print "max_pkt =" , max_pkt
      print "len_check, len(y_t),len(y_sp),len(z_t),len(z_sp)", len_check, len(y_t),len(y_sp),len(z_t),len(z_sp)
      
      ################ Now I will work on when max SP is observed. Later, can do the same for min
      #f_a = ddir + str(f) + "flows/"
      f_plot = f_a + fd + "_TIME_vs_MAXSP_per_PKT"
      f_plot_h = f_a + fd + "_TIME_vs_MAXSP_per_PKT_HEATMAP"
      
      f_yta = f_a + fd + "_yta.npy" # Store yta to save time. Computation is time consuming
      

      print "f_plot" , f_plot
      #print np.min(y_t), np.max(y_t)
      y_t = y_t - np.min(y_t) 
      #print y_t
      
      y_t_a = np.empty([ np.max(y_t)+ 1, 1 ])

      if not os.path.isfile(f_yta):
         for g in range(int(np.max(y_t))+1):
            c = 0 # used to count how many SP values were there
            y_t_a[g] =  0 
            for h in range(len(y_t)):
               if y_t[h] == g:
                  # => time is the same, get the SP and increase c
                  c += 1
                  y_t_a[g] += y_sp[h]
            if c > 0:
               y_t_a[g] = float(y_t_a[g])/float(c)
            if y_t_a[g] ==  0:
               y_t_a[g] = float('nan')
            
         np.save(f_yta,y_t_a)

      else:
         #print "Loading existing file"
         y_t_a = np.load(f_yta)

      #print "len : ", len(y_t) #, len(y_t[0,:]),  len(y_t[:,0])
      #print y_t_a[g]
      
      
      #Mayutan
      x1 = y_t
      y1 = y_sp
      x2 = range(len(y_t_a))
      y2 = y_t_a
      line1, = plt.plot(x1,y1, 'b.', alpha=0.2, rasterized=True, label='Per Packet')
      #line2, = plt.plot(range(len(y_t_a)), y_t_a, 'ro', alpha=0.5, rasterized=True, label='Avg') 
      line2, = plt.plot(x2,y2 , 'ro', alpha=0.5, rasterized=True, label='Avg') 
      legend = plt.legend(loc='upper left', shadow=True)
      plt.xlabel('Time (s)')
      #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
      plt.ylabel('Maximum SP')
      #plt.title('SP vs Time')
      #plt.show()
      #plt.axis(F)

      #http://www.astrobetter.com/blog/2014/01/17/slim-down-your-bloated-graphics/
      # py.plot(arr[:,0], arr[:,1], 'o', alpha=0.1, rasterized=True)
      # py.savefig('dots.pdf', dpi=400)

      plt.axis([0, 650, 0, 110])
      fig = plt.gcf()
      fig.tight_layout()
      fig.set_size_inches(10.5,4.4)
      plt.savefig(f_plot + ".pdf", dpi=800)
      plt.close()

      ## Plot using HeatMap
      #xxx = y_t
      #yyy = y_sp
      # This makes a 50x50 heatmap. If you want, say, 512x384, you can put bins=(512, 384) in the call to histogram2d.
      #heatmap, xedges, yedges = np.histogram2d(xxx, yyy, bins=1000)
      #extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
      
      #plt.clf()
      #plt.imshow(heatmap.T, extent=extent, origin = 'lower')

      #cb = plt.colorbar()
      #cb.set_label('mean value')
      
      #      plt.savefig(f_plot_h + ".pdf")
      #plt.close()
   
      ###########################################################################################

      ################ Now I will work on when min SP is observed
      #f_a = ddir + str(f) + "flows/"
      f_plot = f_a + fd + "_TIME_vs_MINSP_per_PKT"
      f_plot_h = f_a + fd + "_TIME_vs_MINSP_per_PKT_HEATMAP"

      f_zta = f_a + fd + "_zta.npy" # Store yta to save time. Computation is time consuming

      print "f_plot" , f_plot
      #print np.min(y_t), np.max(y_t)
      z_t = z_t - np.min(z_t) 
      #print z_t
      
      z_t_a = np.empty([ np.max(z_t)+ 1, 1 ])

      if not os.path.isfile(f_zta):
         for g in range(int(np.max(z_t))+1):
            c = 0 # used to count how many SP values were there
            z_t_a[g] = 0
            for h in range(len(z_t)):
               if z_t[h] == g:
                  # => time is the same, get the SP and increase c
                  c += 1
                  z_t_a[g] += z_sp[h]
            if c > 0:
               z_t_a[g] = float(z_t_a[g])/float(c)
            if z_t_a[g] ==  0:
               z_t_a[g] = float('nan')

         np.save(f_zta,z_t_a)
      else:
         print "Loading existing file"
         z_t_a = np.load(f_zta)

      
      
      x1 = z_t
      y1 = z_sp
      x2 = range(len(z_t_a))
      y2 = z_t_a
      

      # Mayutan
      line1, = plt.plot(x1,y1, 'b.', alpha=0.3, rasterized=True, label='Per Packet')
      #line2, = plt.plot(range(len(z_t_a)), z_t_a, 'ro', alpha=0.8, rasterized=True, label='Avg')
      line2, = plt.plot(x2, y2, 'ro', alpha=0.8, rasterized=True, label='Avg')
      legend = plt.legend(loc='upper left', shadow=True)
      plt.xlabel('Time (s)')
      #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
      plt.ylabel('Minimum SP')
      #plt.title('SP vs Time')
      #plt.show()
      #plt.axis(F)


      plt.axis([0, 650, 0, 110])
      fig = plt.gcf()
      fig.tight_layout()
      fig.set_size_inches(10.5,4.4)      
      plt.savefig(f_plot + ".pdf", dpi=800)
      plt.close()
 
      ## Plot using HeatMap
      #xxx = z_t
      #yyy = z_sp
      
      # This makes a 50x50 heatmap. If you want, say, 512x384, you can put bins=(512, 384) in the call to histogram2d.
      # if 'bins=None', then color of each hexagon corresponds directly to its count
      #gridsize=30
      # if 'bins=None', then color of each hexagon corresponds directly to its count
      # 'C' is optional--it maps values to x-y coordinates; if 'C' is None (default) then 
      # the result is a pure 2D histogram 

      #plt.hexbin(xxx, yyy, C=z_t_a, gridsize=gridsize, cmap=CM.jet, bins=None)
      #plt.axis([xxx.min(),xxx.max(),yyy.min(), yyy.max()])

      #cb = plt.colorbar()
      #cb.set_label('mean value')      


      #heatmap, xedges, yedges = np.histogram2d(xxx, yyy, bins=None)
      #extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
      
      #plt.clf()
      #plt.imshow(heatmap, extent=extent)
      
      #cb = plt.colorbar()
      #cb.set_label('mean value')
      #plt.savefig(f_plot_h + ".pdf")
      #plt.close()

  
      ###########################################################################################

   


def fread_splog_Alltime(f,dst,src):
   file_dir = FILE_DIR #[sdir_ours_tree,sdir_sdn_tree]
   file_type = FILE_TYPE # ['ours_tree','sdn_tree']
  
   f_instAll = dst + str(f) + "_inst_Alltotal_" 
   f_avgspAll = dst + str(f) + "_inst_AllavgSP_" 

   

   ###################################3
   # Plot instances vs SP for service 10, OURS vs SDN
   # Plot instances vs SP for service 11, OURS_tree vs SDN_tree
   ###################################
   ### FILE NEEDED:    
   ##### f_a = dst + "_inst_"     
   ##### np_a = f_a + "/_Stype_"+str(sf)+"_"+str(file_type)+ ".npy" 
   fopen_o = dst + "_inst_" + "/_Stype_" + str(10)+"_"+ str(file_type[0])+ ".npy" 
   fopen_s = dst + "_inst_" + "/_Stype_" + str(10)+"_"+ str(file_type[1])+ ".npy" 
   fopen_ot = dst + "_inst_" + "/_Stype_" + str(10)+"_"+ str(file_type[2])+ ".npy" 
   fopen_st = dst + "_inst_" + "/_Stype_" + str(10)+"_"+ str(file_type[3])+ ".npy" 

   # file to plot: instances vs SP for service 10, OURS vs SDN and service 11 for OURS_tree vs SDN_tree
   fp_dir = dst + "/rdir_vs_sp/"
   fp = fp_dir + "OURS_vs_SDN" + "_"+ str(10) + "_instVSavgsp" + ".pdf"
   fp_ = fp_dir + "OURS_vs_SDN" + "_"+ str(10) + "_instVSmaxsp" + ".pdf"

   tfp = fp_dir + "TREE_OURS_vs_SDN" + "_"+ str(10) + "_instVSavgsp" + ".pdf"
   tfp_ = fp_dir + "TREE_OURS_vs_SDN" + "_"+ str(10) + "_instVSmaxsp" + ".pdf"


   if os.path.isfile(fopen_o):
      a_o = np.load(fopen_o)

   if os.path.isfile(fopen_s):
      a_s = np.load(fopen_s)

   if os.path.isfile(fopen_ot):
      a_ot = np.load(fopen_ot)

   if os.path.isfile(fopen_st):
      a_st = np.load(fopen_st)

   #print a_o, a_s
   #max_len = 1300 # for Infocom submission
   #a_o = a_o[:max_len][:]
   #a_s = a_s[:max_len][:]
   #   a_ot = a_ot[:max_len][:]
   #a_st = a_st[:max_len][:]

   if len(a_o) > len(a_s):
      a_o = a_o[:len(a_s)][:]
   elif len(a_s) > len(a_o):
      a_s = a_s[:len(a_o)][:]

   if len(a_ot) > len(a_st):
      a_ot = a_ot[:len(a_st)][:]
   elif len(a_st) > len(a_ot):
      a_st = a_st[:len(a_ot)][:]
   
   ######### Plot
   fig, ax1 = plt.subplots()
   t = a_o[:,0]
   
   s1 = a_o[:,2]
   s1_ = a_s[:,2]

   #print "len(t), len(s1), len(s1_)" , len(t), len(s1), len(s1_)

   line1, = ax1.plot(t, s1, 'b.', label='DRENCH')
   line2, = ax1.plot(t, s1_, 'ro', label='D-SDN')
   
   
   ax1.set_xlabel('Time (s)')
   # Make the y-axis label and tick labels match the line color.
   ax1.axis([0, 650, 0, 5.2])
   ax1.set_ylabel('Max # of Instances', color='k')
   for tl in ax1.get_yticklabels():
      tl.set_color('k')

   legend = plt.legend(loc='upper left', shadow=True)
   # Mayutan

   s2 = a_o[:,4]
   s2_ = a_s[:,4]
   ax2 = ax1.twinx()

   #ax2.plot(t, s2, 'b-')
   line3, = ax2.plot(t, s2, 'b-', label='DRENCH' , alpha=0.8, rasterized=True)
   #ax2.plot(t, s2_sdn, 'r-')
   line4, = ax2.plot(t, s2_, 'r--', label='D-SDN', alpha=0.8, rasterized=True)

   ax2.axis([0, 650, 0, 50])
   ax2.set_ylabel('Avg. Shadow Price', color='k')
   for tl in ax2.get_yticklabels():
      tl.set_color('k')
      
   legend = plt.legend(loc='upper right', shadow=True)

   
   fig = plt.gcf()
   fig.tight_layout()
   fig.set_size_inches(10.5,4.4)
   plt.savefig(fp)
   plt.close()
   
   ###
   fig, ax1 = plt.subplots()
   t = a_o[:,0]
   
   s1 = a_o[:,2]
   s1_ = a_s[:,2]

   line1, = ax1.plot(t, s1, 'b.', label='DRENCH')
   line2, = ax1.plot(t, s1_, 'ro', label='D-SDN')
   ax1.set_xlabel('Time')

   ax1.axis([0, 650, 0, 5.2])
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of Instances', color='k')
   for tl in ax1.get_yticklabels():
      tl.set_color('k')

   legend = plt.legend(loc='upper left', shadow=True)
   # Mayutan

   s2 = a_o[:,5]
   s2_ = a_s[:,5]
   ax2 = ax1.twinx()

   #ax2.plot(t, s2, 'b-')
   line3, = ax2.plot(t, s2, 'b-', label='DRENCH' , alpha=0.8, rasterized=True)
   #ax2.plot(t, s2_sdn, 'r-')
   line4, = ax2.plot(t, s2_, 'r--', label='D-SDN' , alpha=0.8, rasterized=True)

   ax2.axis([0, 650, 0, 110])
   ax2.set_ylabel('Max Shadow Price', color='k')
   for tl in ax2.get_yticklabels():
      tl.set_color('k')
      
   legend = plt.legend(loc='upper right', shadow=True)

   fig = plt.gcf()
   fig.tight_layout()
   fig.set_size_inches(10.5,4.4)
   plt.savefig(fp_)
   plt.close()
   
   ######### Plot Tree #####
   fig, ax1 = plt.subplots()
   t = a_ot[:,0]
   
   s1 = a_ot[:,2]
   s1_ = a_st[:,2]

   line1, = ax1.plot(t, s1, 'b.', label='DRENCH')
   line2, = ax1.plot(t, s1_, 'ro', label='D-SDN')
   ax1.set_xlabel('Time (s)')
   
   ax1.axis([0, 650, 0, 5.2])
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of Instances', color='k')
   for tl in ax1.get_yticklabels():
      tl.set_color('k')

   legend = plt.legend(loc='upper left', shadow=True)
   # Mayutan

   s2 = a_ot[:,4]
   s2_ = a_st[:,4]
   ax2 = ax1.twinx()

   #ax2.plot(t, s2, 'b-')
   line3, = ax2.plot(t, s2, 'b-', label='DRENCH' , alpha=0.8, rasterized=True)
   #ax2.plot(t, s2_sdn, 'r-')
   line4, = ax2.plot(t, s2_, 'r--', label='D-SDN', alpha=0.8, rasterized=True)

   ax2.axis([0, 650, 0, 50])
   ax2.set_ylabel('Avg. Shadow Price', color='k')
   for tl in ax2.get_yticklabels():
      tl.set_color('k')
      
   legend = plt.legend(loc='upper right', shadow=True)

   fig = plt.gcf()
   fig.tight_layout()
   fig.set_size_inches(10.5,4.4)
   plt.savefig(tfp)
   plt.close()
   
   ###
   fig, ax1 = plt.subplots()
   t = a_ot[:,0]
   
   s1 = a_ot[:,2]
   s1_ = a_st[:,2]

   line1, = ax1.plot(t, s1, 'b.', label='DRENCH')
   line2, = ax1.plot(t, s1_, 'ro', label='D-SDN')
   ax1.set_xlabel('Time')
   
   ax1.axis([0, 650, 0, 6])
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of Instances', color='k')
   for tl in ax1.get_yticklabels():
      tl.set_color('k')

   legend = plt.legend(loc='upper left', shadow=True)
   # Mayutan

   s2 = a_ot[:,5]
   s2_ = a_st[:,5]
   ax2 = ax1.twinx()

   #ax2.plot(t, s2, 'b-')
   line3, = ax2.plot(t, s2, 'b-', label='DRENCH' , alpha=0.8, rasterized=True)
   #ax2.plot(t, s2_sdn, 'r-')
   line4, = ax2.plot(t, s2_, 'r--', label='D-SDN' , alpha=0.8, rasterized=True)
   
   ax2.axis([0, 650, 0, 110])
   ax2.set_ylabel('Max Shadow Price', color='k')
   for tl in ax2.get_yticklabels():
      tl.set_color('k')
      
   legend = plt.legend(loc='upper right', shadow=True)

   fig = plt.gcf()
   fig.tight_layout()
   fig.set_size_inches(10.5,4.4)
   plt.savefig(tfp_)
   plt.close()
   

   ###################################3
   # Plot Total inst 
   ###################################

   fdiri = -1
   for fd in file_dir:
      fdiri += 1
      fdir = eval(fd)
   
      fopen = src + file_type[fdiri] + ".npy"
      if os.path.isfile(fopen):
         a = np.load(fopen)
         #print a
         plt.plot(a[:,0],a[:,2],label=fd)
         #    line1, = plt.plot(ot_a[:,0], ot_a[:,1], 'b*', label='Our-Solution DataCenter')
   
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
   plt.ylabel('Total Number of instances')
   plt.title('Total number of instances vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_instAll + ".pdf")
   plt.close()
   ###


   ###################################3
   # Plot avgSP 
   ###################################
   fdiri = -1
   for fd in file_dir:
      fdiri += 1
      fdir = eval(fd)
   
      fopen = src + file_type[fdiri] + ".npy"
      if os.path.isfile(fopen):
         a = np.load(fopen)
         #print a
         plt.plot(a[:,0],a[:,3],label=fd)
         #    line1, = plt.plot(ot_a[:,0], ot_a[:,1], 'b*', label='Our-Solution DataCenter')
   
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   #plt.ylim( (0,2 + np.max(np.max(ot_a[:,1]),np.max(st_a[:,1]))) )
   plt.ylabel('Average SP')
   plt.title('Average SP vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_avgspAll + ".pdf")
   plt.close()
   ###



def fread_rdir_time(f,ddir):
   #print "Hello"
   dir = str(f) + 'flows/'
   
   rdir = ddir + str(f)
   f_NFI = rdir + "_Services_with_redirection"  

   f_a = rdir + "_a_redir"     
   f_total = rdir + "_redir_total"     
   f_avg = rdir + "_redir_avg"  
   f_inst = rdir + "_redir_inst"  
   

   f_redir = rdir + "_redir_per_service_"     
   fname = FNAME[2] #SUFFIX + "rdir_log"
   
   file_dir = FILE_DIR #[sdir_ours_tree,sdir_sdn_tree]
   file_type = FILE_TYPE # ['ours_tree','sdn_tree']

   fdiri = -1
   for fd in file_dir:
      fdiri += 1
      fdir = eval(fd)

      fopen = fdir + dir + fname

      if not os.path.isfile(fopen):
         print "Error: file not present : ", fopen
         return

      file = open(fopen, 'rb')
      data = csv.reader(file, delimiter=',')
      table = [row for row in data]
      
      #a = table[0:10][:]
      #print a

      # following works in num of cols is the same for all rows
      numrows = len(table)
      #numrows = 10 # for debugging
      
      numcols = len(table[0])
      print numrows, numcols
      
      a = np.empty([numrows,5])
      
      ##########################################
      redir_s = np.empty([numrows, nSF + 1]) 
      # to store time and number of redirections per service
      #col[0] : time
      #col[1] : redirections for SF[0]
      #..
      #col[nSF] : redirections for SF[nSF-1]
      # initialize it to zero
      for x in range(numrows):
         for y in range(nSF+1):
            redir_s[x][y] = 0 # float('nan')
         
      ###########################################

      debug = 0
      # Trying to extract array of number of service instances
      for i in range(numrows):
         debug += 1
         #col[0] is time from zero
         #col[1] is number of SERVICES that see redirections
         #col[2] instantaneous number of redirections for all services
         #col[3] total number of redirections for all services
         #col[4] average number of redirections for all services that have redirection
         a[i][0] = int(table[i][0]) - int(table[0][0]) 
         redir_s[i][0]= a[i][0]

     
         # now to extract number of service instances
         b = table[i][1].split('],')
         #print b

         #now to extract total redirections for all instances put together
         r = 0
         #print len(b)
         if len(b) == 1 and b[0] == '':
            # take care of empty values in the beginning, but should not exclude the case of single instance being present
            r = 0
            

            a[i][1] = 0
         else:
            a[i][1] = len(b)
         
            for bi in range(len(b)):
               #print b[bi]
               t = b[bi].split('-[')[1].split(',')
               #print t
               r += int(t[0])
               #print t

               #print "r,len-b=" ,r, len(b)

               # Now go through every SF and see if there were any redirections
               for x in range(nSF):
                  sf = SF[x]
                  # Check if sf is present in b
                  sf_ = b[bi].split('-[')[0]
                  sf__ = b[bi].split('-[')[1].split(',')
                  #print "sf_", sf
                  if i > 0:
                     if int(sf) == int(sf_):
                        sum_redir__ = np.sum(redir_s[:i,x+1])
                        #if sum_redir__ > 0:
                        #   print "SUM ",sum_redir__ , i , x,  redir_s[:i, x+1]
                        #   return
                        redir_s[i][x+1] = int(sf__[0]) - sum_redir__ # to get redirections in that time period 
               
         #return
      

         #for i in range(numrows):
         if i == 0:
            #i.e first row, no redirections yet
            a[i][2] = 0
            a[i][3] = 0
            a[i][4] = 0
         
            #print a
         
         else:
            #print "sum =",i,r,  a[:i,2],a[:i,2].sum(), r-a[:i,2].sum()
            a[i][2] = r - a[:i,2].sum()   
            a[i][3] = r # overall total
            a[i][4] = float(r/len(b))# overall avg 

      np.set_printoptions(threshold='nan', precision=3)
      #print a 
      np.save(f_a+"_"+file_type[fdiri]+".npy",a) 

      for x in range(numrows):
         for y in range(nSF+1):
            if redir_s[x][y] == 0:
               redir_s[x][y] = float('nan')
  
      np.save(f_redir+"_"+file_type[fdiri]+".npy",redir_s)   
      #print "f_redir = ", f_redir
      #print redir_s
      
      #print "a[:,2], redir_s ",a[:,2], redir_s 
   #return
   o_a = np.load(f_a+"_"+file_type[0]+".npy")
   s_a = np.load(f_a+"_"+file_type[1]+".npy")
   ot_a = np.load(f_a+"_"+file_type[2]+".npy")
   st_a = np.load(f_a+"_"+file_type[3]+".npy")

   #print "np.max = ", 
   
   
   #print np.max([ np.max(o_a[:,1]) , np.max(s_a[:,1]) , np.max(ot_a[:,1]) , np.max(st_a[:,1])] )
   #print np.max(o_a[:,1]) , np.max(s_a[:,1]) , np.max(ot_a[:,1]) , np.max(st_a[:,1]) 
   #print o_a[:,0] , s_a[:,0] , ot_a[:,0] , st_a[:,0] 
   #print o_a[:,1] , s_a[:,1] , ot_a[:,1] , st_a[:,1] 

   line1, = plt.plot(o_a[:,0], o_a[:,1], 'b-', label='Our-Solution Rocketfuel')
   line2, = plt.plot(s_a[:,0], s_a[:,1], 'r-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(ot_a[:,0], ot_a[:,1], 'b*', label='Our-Solution DataCenter')
   line4, = plt.plot(st_a[:,0], st_a[:,1], 'r*', label='Standard-SDN DataCenter')
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   plt.ylim( (0,2 + np.max([ np.max(o_a[:,1]) , np.max(s_a[:,1]) , np.max(ot_a[:,1]) , np.max(st_a[:,1])] ) ) )
   plt.ylabel('Total Number of Services that have redirection')
   plt.title('Total Number of Services with redirection vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_NFI + ".pdf")
   plt.close()

   #return

   line1, = plt.plot(o_a[:,0], o_a[:,3], 'b-', label='Our-Solution Rocketfuel')
   line2, = plt.plot(s_a[:,0], s_a[:,3], 'r-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(ot_a[:,0], ot_a[:,3], 'b*', label='Our-Solution DataCenter')
   line4, = plt.plot(st_a[:,0], st_a[:,3], 'r*', label='Standard-SDN DataCenter')
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   plt.ylim( (0,2 + np.max([ np.max(o_a[:,3]) , np.max(s_a[:,3]) , np.max(ot_a[:,3]) , np.max(st_a[:,3])] ) ) )
   plt.ylabel('Total Number of Redirections')
   plt.title('Total Number of Redirections vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_total + ".pdf")
   plt.close()

   line1, = plt.plot(o_a[:,0], o_a[:,4], 'b-', label='Our-Solution Rocketfuel')
   line2, = plt.plot(s_a[:,0], s_a[:,4], 'r-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(ot_a[:,0], ot_a[:,4], 'b*', label='Our-Solution DataCenter')
   line4, = plt.plot(st_a[:,0], st_a[:,4], 'r*', label='Standard-SDN DataCenter')
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   plt.ylim( (0,2 + np.max([ np.max(o_a[:,4]) , np.max(s_a[:,4]) , np.max(ot_a[:,4]) , np.max(st_a[:,4])] ) ) )
   plt.ylabel('Average Number of Redirections')
   plt.title('Avg redirections per service that has redirections \n(usually 6 or 7 out of the 10) vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_avg + ".pdf")
   plt.close()

   line1, = plt.plot(o_a[:,0], o_a[:,2], 'b-', label='Our-Solution Rocketfuel')
   line2, = plt.plot(s_a[:,0], s_a[:,2], 'r-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(ot_a[:,0], ot_a[:,2], 'b*', label='Our-Solution DataCenter')
   line4, = plt.plot(st_a[:,0], st_a[:,2], 'r*', label='Standard-SDN DataCenter')
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   plt.ylim( (0,2 + np.max([ np.max(o_a[:,2]) , np.max(s_a[:,2]) , np.max(ot_a[:,2]) , np.max(st_a[:,2])] ) ) )
   plt.ylabel('Redirections observed in that time period')
   plt.title('Redirections Observed in a time period vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_inst + ".pdf")
   plt.close()


def fread_rdir_all():
   fplot_maxinst = DDIR + "rdir_maxinst.pdf"  
   fplot_total =  DDIR + "rdir_total.pdf"  
   fplot_avg =  DDIR + "rdir_avg.pdf"

   farr_inst_redir_max = DDIR + "inst_redir_max.npy"
   farr_total_redir = DDIR + "total_redir.npy"
   farr_avg_redir = DDIR + "avg_redir.npy" 
   
   #dir = "sp_logs/"
   for f in F:
      # 11, 13, 15 are SF
      dir = str(f) + 'flows/'
      fname = "rdir_log" 
      fread_rdir(dir,fname)


   # Now open all arrays and calculate overall avg
   
   inst_redir_max = np.empty([nF]) # maximum number of instantaneous redirections
   total_redir = np.empty([nF]) # Total number of redirections
   avg_redir = np.empty([nF]) # Average number of redirections ?? should I make it avg number of instantaneous redirections

   i  = -1
   for f in F:
      #print "start = ", inst_redir_max, total_redir, avg_redir
      i += 1
      dir = str(f) + 'flows/'
      fname = "rdir_log.npy" 
      fopen = SDIR + dir + fname
      if not os.path.isfile(fopen):
         inst_redir_max[i] = -1
         total_redir[i] = -1
         avg_redir[i] = -1
         continue

      a = np.load(fopen)
      inst_redir_max[i] = np.amax(a[:,2]) 
      total_redir[i] = a[-1,3]
      avg_redir[i] = a[-1,4]

   print "overall : inst_redir_max, total_redir, avg_redir = ", inst_redir_max, total_redir, avg_redir

   np.save(farr_inst_redir_max,inst_redir_max)
   np.save(farr_total_redir,total_redir)
   np.save(farr_avg_redir,avg_redir)

   plt.plot(F, inst_redir_max)
   plt.xlabel('# of flows')
   plt.ylabel('Maximum # of instantaneous redirections')
   #plt.show()
   #plt.axis(F)
   plt.savefig(fplot_maxinst)
   plt.close()


   plt.plot(F, total_redir)
   plt.xlabel('# of flows')
   plt.ylabel('Total # of redirections')
   #plt.show(
   #plt.axis(F)
   plt.savefig(fplot_total)
   plt.close()

   plt.plot(F, avg_redir)
   plt.xlabel('# of flows')
   plt.ylabel('avg # of redirections')
   #plt.show()
   #plt.axis(F)
   plt.savefig(fplot_avg)
   plt.close()

def fread_rules_all():
   fplot_total = DDIR + "total_rules.pdf"
   farr_total_rules = DDIR + "total_rules.npy"
   #print F, nF
   
   total_rules = np.empty([nF]) # Total number of rules

   i  = -1
   for f in F:
      i += 1
      dir = str(f) + 'flows/'
      fname = "only_vlan_flow_count.txt"
      
      fopen = ''
      if os.path.isfile(SDIR + dir + fname):
         fopen = SDIR + dir + fname
      elif os.path.isfile(SDIR + dir + "only_vlan_flow_count_1.txt"):
         fopen = SDIR + dir + "only_vlan_flow_count_1.txt"
         
      #print fopen
      with open(fopen) as f:
         for line in f:
            val = line
         
      total_rules[i] = int(val)

   print "overall = ", total_rules, F[0], F[-1], np.amin(total_rules), np.amax(total_rules)

   np.save(farr_total_rules,total_rules)


   plt.plot(F, total_rules)
   plt.xlabel('# of flows')
   plt.ylabel('Total # of rules')
   plt.axis([F[0], F[-1], np.amin(total_rules), np.amax(total_rules)] )
   plt.savefig(fplot_total)
   plt.close()


def fread_hops(f,ddir):
   #print "Hello"
   dir = str(f) + 'flows/'
   
   rdir = ddir + str(f)
   f_a = rdir + "_a_hops_and_total_"     
   f_flows = rdir + "_total_flows"     
   f_totalhops = rdir + "_hops_total"  
   f_avghops = rdir + "_hops_avg"  

   
   fname = FNAME[0] #SUFFIX + "hopcount2_log.csv"
   
   file_dir = FILE_DIR #[sdir_ours_tree,sdir_sdn_tree]
   file_type = FILE_TYPE # ['ours_tree','sdn_tree']
   
   #print "file_dir ",file_dir

   fdiri = -1
   for fd in file_dir:
      fdiri += 1
      fdir = eval(fd)
      #print "fdir ",fdir

      fopen = fdir + dir + fname
      #print "fopen, fdir, dir,fname ",fopen, fdir,dir,fname 

      if not os.path.isfile(fopen):
         print "Error: file not present : ", fopen
         return

      file = open(fopen, 'rb')
      data = csv.reader(file, delimiter=',')
      table = [row for row in data]
      
      # following works in num of cols is the same for all rows
      numrows = len(table)
      #numrows = 10 # for debugging

      numcols = len(table[0])
      #print "row,col = ", numrows, numcols
      
      a = np.empty([numrows,4])

      debug = 0
      # Trying to extract array of number of service instances
      for i in range(numrows):
         debug += 1
         #col[0] is time from zero
         #col[1] is total number of flows, i.e. total number of flows/3
         #col[2] is total number of hops: I divide by three since each flow has 3 sub-flows 
         #col[3] is avg number of hops

         a[i][0] = int(table[i][0]) - int(table[0][0]) 
         if table[i][1] != '':
            a[i][1] = float(table[i][1])/2.0
         else:
            a[i][1] = 0.0

         
         if table[i][2] != '':
            a[i][2] = float(table[i][2])/2.0
         else:
            a[i][2] = 0

         if table[i][3] != '':
            a[i][3] = float(table[i][3])
         else:
            a[i][3] = 0

      #print a, a[:,0], a[:,1], a[:,2]
      
      np.save(f_a+"_"+file_type[fdiri]+".npy",a)   
      
   #return
   o_a = np.load(f_a+"_"+file_type[0]+".npy")
   s_a = np.load(f_a+"_"+file_type[1]+".npy")
   ot_a = np.load(f_a+"_"+file_type[2]+".npy")
   st_a = np.load(f_a+"_"+file_type[3]+".npy")

   #print f_flows

   line1, = plt.plot(o_a[:,0], o_a[:,1], 'b*-', label='Our-Solution Rocketfuel')
   line2, = plt.plot(s_a[:,0], s_a[:,1], 'r*-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(ot_a[:,0], ot_a[:,1], 'b--', label='Our-Solution DataCenter')
   line4, = plt.plot(st_a[:,0], st_a[:,1], 'r--', label='Standard-SDN DataCenter')
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   plt.ylabel('Total flows observed')
   plt.title('Total Number of flows vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_flows + ".pdf")
   plt.close()

   line1, = plt.plot(o_a[:,0], o_a[:,2], 'b*-', label='Our-Solution Rocketfuel')
   line2, = plt.plot(s_a[:,0], s_a[:,2], 'r*-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(ot_a[:,0], ot_a[:,2], 'b--', label='Our-Solution DataCenter')
   line4, = plt.plot(st_a[:,0], st_a[:,2], 'r--', label='Standard-SDN DataCenter')
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   plt.ylabel('Total hops observed')
   plt.title('Total Number of hops for all flows vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_totalhops + ".pdf")
   plt.close()

   line1, = plt.plot(o_a[:,0], o_a[:,3], 'b*-', label='Our-Solution Rocketfuel')
   line2, = plt.plot(s_a[:,0], s_a[:,3], 'r*-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(ot_a[:,0], ot_a[:,3], 'b--', label='Our-Solution DataCenter')
   line4, = plt.plot(st_a[:,0], st_a[:,3], 'r--', label='Standard-SDN DataCenter')
   legend = plt.legend(loc='lower center', shadow=True)
   plt.xlabel('Time')
   plt.ylabel('Average hops observed')
   plt.title('Average number of hops (total hops/total flows)vs Time')
   #plt.show()
   #plt.axis(F)
   plt.savefig(f_avghops + ".pdf")
   plt.close()


def fread_hops_all():
   #fplot_hops_95avg = DDIR + "hops_overallavg.pdf"
   fplot_maxflows = DDIR + "maxflows.pdf"  
   fplot_hops_avg_maxflows = DDIR + "hops_avg_maxflows.pdf"  
   fplot_hops_max_inst_avg = DDIR + "hops_maxavg.pdf"  
   #fplot_hops_minavg = DDIR + "hops_minavg.pdf"

   

   farr_hops_flows_max = DDIR + "hops_flows_max.npy"
   farr_hops_avg_maxflows = DDIR + "hops_avg_maxflows.npy"
   farr_hops_max_inst_avg = DDIR + "hops_max_inst_avg.npy"

   #hops_95avg = np.empty([nF]) # 95% hop count seen by flows   

   hops_flows_max = np.empty([nF]) #max number of flows observed at an instance
   hops_avg_maxflows = np.empty([nF]) #avg taken only when max number of flows are present
   hops_max_inst_avg = np.empty([nF]) # max avg hop count seen by flows
   #hops_minavg = np.empty([nF]) # min avg hop count seen by the flows




   fi  = -1
   for f in F:
      fi += 1
      dir = str(f) + 'flows/'
      fname = "hopcount2_log"
      fopen = SDIR + dir + fname + ".csv"
   
      
      if not os.path.isfile(fopen):
         #print "Error: file not present : ", fopen
         hops_flows_max[fi] = -1
         hops_avg_maxflows[fi] = -1
         hops_max_inst_avg[fi] = -1
         continue


      file = open(fopen, 'rb')
      
      data = csv.reader(file, delimiter=',')
      table = [row for row in data]
      
      # following works in num of cols is the same for all rows
      numrows = len(table)
      #numrows = 10 # for debugging

      numcols = len(table[0])
      #print "row,col = ", numrows, numcols
      
      a = np.empty([numrows,4])
      
      debug = 0
      # Trying to extract array of number of service instances
      for i in range(numrows):
         debug += 1
         #col[0] is time from zero
         #col[1] is total number of flows
         #col[2] is totol number of hops
         #col[3] is avg number of hops

         a[i][0] = int(table[i][0]) - int(table[0][0]) 
         if table[i][1] != '':
            a[i][1] = float(table[i][1])
         else:
            a[i][1] = 0

         
         if table[i][2] != '':
            a[i][2] = float(table[i][2])
         else:
            a[i][2] = 0

         if table[i][3] != '':
            a[i][3] = float(table[i][3])
         else:
            a[i][3] = 0
   
      ############# Save time period when max flows are alive
      #Use col[0] for time period 
      farr_time_maxflows_alive = SDIR + "time_maxflows_alive_" + str(f) + ".npy" 
      farr_time_and_flows = SDIR + "time_and_flows_" + str(f) + ".npy" 
      ########################
         
      ########
      # calc avg when max flows are present      
      #######
      hops_flows_max[fi] = np.amax(a[:,1])
      # Now get all the hops only for those flows
      aa = a[a[:, 1] == hops_flows_max[fi]]
      # save it in array for usage by other functions
      np.save(farr_time_maxflows_alive,aa)
      np.save(farr_time_and_flows,a) 
      #print "aa= ", aa 
      hops_avg_maxflows[fi] = np.mean(aa[:,3])

      ###########


      hops_max_inst_avg[fi] = np.amax(a[:,3])
      
   #print "overall: hops_flows_max, hops_avg_maxflows, hops_max_inst_avg  = ", hops_flows_max, hops_avg_maxflows, hops_max_inst_avg  
   
   np.save(farr_hops_flows_max, hops_flows_max)
   np.save(farr_hops_avg_maxflows,hops_avg_maxflows)
   np.save(farr_hops_max_inst_avg,hops_max_inst_avg)


   plt.plot(F, hops_flows_max)
   plt.xlabel('# of flows')
   plt.ylabel('Max flows observed')
   #plt.show()
   #plt.axis(F)
   plt.savefig(fplot_maxflows)
   plt.close()

   plt.plot(F, hops_avg_maxflows)
   plt.xlabel('# of flows')
   plt.ylabel('Avg hop count when max flows are present')
   #plt.show()
   #plt.axis(F)
   plt.savefig(fplot_hops_avg_maxflows)
   plt.close()
   
   plt.plot(F, hops_max_inst_avg)
   plt.xlabel('# of flows')
   plt.ylabel('Maximum # of avg instantaneous hops')
   #plt.show()
   #plt.axis(F)
   plt.savefig(fplot_hops_max_inst_avg)
   plt.close()


def plot_flows_vs_maxflows_and_rules():
   fplot = DDIR + "flows_vs_maxflows_and_rules.pdf"

   if os.path.isfile(DDIR + "total_rules.npy" ) and os.path.isfile(DDIR + "hops_flows_max.npy") :
      print "All OK"
   else:
      print "ERROR: either total_rules.npy or hops_flows_max.npy does not exist"
      return

   rules = np.load(DDIR + "total_rules.npy" ) 
   flows = np.load(DDIR + "hops_flows_max.npy") 
   print "flows, rules = ", flows, rules
   
   fig, ax1 = plt.subplots()
   t = F
   
   s1 = flows
   ax1.plot(t, s1, 'b-')
   ax1.set_xlabel('Flows')
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of flows', color='b')
   for tl in ax1.get_yticklabels():
      tl.set_color('b')

   s2 = rules 
   ax2 = ax1.twinx()
   ax2.plot(t, s2, 'r-')
   ax2.set_ylabel('Total # of rules', color='r')
   for tl in ax2.get_yticklabels():
      tl.set_color('r')
   

   plt.savefig(fplot)
   plt.close()

def plot_flows_vs_maxflows_and_totalredir():
   # also plots max inst redir
   
   ###
   #based on files:
   #farr_inst_redir_max = DDIR + "inst_redir_max.npy"
   #farr_total_redir = DDIR + "total_redir.npy"
   ###

   fplot_total = DDIR + "flows_vs_maxflows_and_totalredir.pdf"
   fplot_maxinstredir = DDIR + "flows_vs_maxflows_and_maxinstredir.pdf"

   if os.path.isfile(DDIR + "total_rules.npy" ) and os.path.isfile(DDIR + "inst_redir_max.npy")and os.path.isfile(DDIR + "total_redir.npy") :
      print "All OK"
   else:
      print "ERROR: either total_rules.npy or inst_redir_max.npy or total_redir.npy does not exist"
      return

   flows = np.load(DDIR + "hops_flows_max.npy") 
   total_redir = np.load(DDIR + "total_redir.npy" ) 
   inst_redir_max = np.load(DDIR + "inst_redir_max.npy" ) 
   
   print "flows, total_redir, inst_redir_max = ", flows, total_redir, inst_redir_max
   

   ######### Total redir #######
   fig, ax1 = plt.subplots()
   t = F
   
   s1 = flows
   ax1.plot(t, s1, 'b-')
   ax1.set_xlabel('Flows')
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of flows', color='b')
   for tl in ax1.get_yticklabels():
      tl.set_color('b')

   s2 = total_redir
   ax2 = ax1.twinx()
   ax2.plot(t, s2, 'r-')
   ax2.set_ylabel('Total # of redirections', color='r')
   for tl in ax2.get_yticklabels():
      tl.set_color('r')
   
   plt.savefig(fplot_total)
   plt.close()
   ###########

   ######### Max inst redir #######
   fig, ax1 = plt.subplots()
   t = F
   
   s1 = flows
   ax1.plot(t, s1, 'b-')
   ax1.set_xlabel('Flows')
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of flows', color='b')
   for tl in ax1.get_yticklabels():
      tl.set_color('b')

   s2 = inst_redir_max
   ax2 = ax1.twinx()
   ax2.plot(t, s2, 'r-')
   ax2.set_ylabel('Maximum # of redirections in a time period', color='r')
   for tl in ax2.get_yticklabels():
      tl.set_color('r')
   

   plt.savefig(fplot_maxinstredir)
   plt.close()

def plot_flows_vs_maxflows_and_avgSP():
   # also plots max SP

   ###
   #based on files:
   #farr_sp__max = DDIR + "sp__max.npy"
   #farr_sp__avg = DDIR + "sp__avg.npy"
   ###

   fplot_spavg = DDIR + "flows_vs_maxflows_and_spavg.pdf"
   fplot_spmax = DDIR + "flows_vs_maxflows_and_spmax.pdf"

   if os.path.isfile(DDIR + "total_rules.npy" ) and os.path.isfile(DDIR + "sp__avg.npy")and os.path.isfile(DDIR + "sp__max.npy") :
      print "All OK"
   else:
      print "ERROR: either total_rules.npy or sp__avg.npy or sp__max.npy does not exist"
      return

   flows = np.load(DDIR + "hops_flows_max.npy") 
   spavg = np.load(DDIR + "sp__avg.npy" ) 
   spmax = np.load(DDIR + "sp__max.npy" ) 
   
   print "flows, spavg, spmax = ", flows, spavg, spmax
   
   ######### Total redir #######
   fig, ax1 = plt.subplots()
   t = F
   
   s1 = flows
   ax1.plot(t, s1, 'b-')
   ax1.set_xlabel('Flows')
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of flows', color='b')
   for tl in ax1.get_yticklabels():
      tl.set_color('b')

   s2 = spavg
   ax2 = ax1.twinx()
   ax2.plot(t, s2, 'r-')
   ax2.set_ylabel('Average SP value', color='r')
   for tl in ax2.get_yticklabels():
      tl.set_color('r')
   
   plt.savefig(fplot_spavg)
   plt.close()
   ###########

   ######### Max inst redir #######
   fig, ax1 = plt.subplots()
   t = F
   
   s1 = flows
   ax1.plot(t, s1, 'b-')
   ax1.set_xlabel('Flows')
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of flows', color='b')
   for tl in ax1.get_yticklabels():
      tl.set_color('b')

   s2 = spmax
   ax2 = ax1.twinx()
   ax2.plot(t, s2, 'r-')
   ax2.set_ylabel('Maximum value of SP', color='r')
   for tl in ax2.get_yticklabels():
      tl.set_color('r')
   

   plt.savefig(fplot_spmax)
   plt.close()

 
def plot_flows_vs_maxflows_and_avginst():
   # also plots max number of instances  

   ###
   #based on files:
   #farr_inst__max = DDIR + "inst__max.npy"
   #farr_inst__avg = DDIR + "inst__avg.npy"
   ###

   fplot_instavg = DDIR + "flows_vs_maxflows_and_instavg.pdf"
   fplot_instmax = DDIR + "flows_vs_maxflows_and_instmax.pdf"

   if os.path.isfile(DDIR + "total_rules.npy" ) and os.path.isfile(DDIR + "inst__avg.npy")and os.path.isfile(DDIR + "inst__max.npy") :
      print "All OK"
   else:
      print "ERROR: either total_rules.npy or inst__avg.npy or inst__max.npy does not exist"
      return

   flows = np.load(DDIR + "hops_flows_max.npy") 
   instavg = np.load(DDIR + "inst__avg.npy" ) 
   instmax = np.load(DDIR + "inst__max.npy" ) 
   
   print "flows, instavg, instmax = ", flows, instavg, instmax
   
   ######### Total redir #######
   fig, ax1 = plt.subplots()
   t = F
   
   s1 = flows
   ax1.plot(t, s1, 'b-')
   ax1.set_xlabel('Flows')
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of flows', color='b')
   for tl in ax1.get_yticklabels():
      tl.set_color('b')

   s2 = instavg
   ax2 = ax1.twinx()
   ax2.plot(t, s2, 'r-')
   ax2.set_ylabel('Average # of instances', color='r')
   for tl in ax2.get_yticklabels():
      tl.set_color('r')
   
   plt.savefig(fplot_instavg)
   plt.close()
   ###########

   ######### Max inst redir #######
   fig, ax1 = plt.subplots()
   t = F
   
   s1 = flows
   ax1.plot(t, s1, 'b-')
   ax1.set_xlabel('Flows')
   # Make the y-axis label and tick labels match the line color.
   ax1.set_ylabel('Max # of flows', color='b')
   for tl in ax1.get_yticklabels():
      tl.set_color('b')

   s2 = instmax
   ax2 = ax1.twinx()
   ax2.plot(t, s2, 'r-')
   ax2.set_ylabel('Maximum value of Instances', color='r')
   for tl in ax2.get_yticklabels():
      tl.set_color('r')
   

   plt.savefig(fplot_instmax)
   plt.close()

def run(sdir,ddir):
   set_SDIR_DDIR(sdir,ddir)
   init()
   check_files()
   
   #return
   # Number of flow rules
   ## Per # of flow for all service instances
   ## Plots number of rules per # of flows
   #m# 
   #fread_rules_all()

   #return


   # Number of hop count
   ## Per # of flow for all service instances
   ## Plots max number of flows observed, avg_hopcount per flow
   ## VIMP: It saves the time periods when max flow is observed: farr_time_maxflows_alive = SDIR + "time_maxflows_alive_" + str(f) + ".npy" 
   ## VIMP: It saves the time periods and number of flows observed: farr_time_and_flows = SDIR + "time_and_flows_" + str(f) + ".npy" 
   #m#   
   ## fread_hops_all()
   return
   
   #exit()
   ####
   # NOTES: Needs files from functions fread_rules_all (rule count) and fread_hops_all (max flow count) 
   #plot_flows_vs_maxflows_and_rules()
   ####
   
   ### Flow redirection
   ## Per instance
   #fread_rdir()
   ## Per # of flow for all service instances
   #m# 
   fread_rdir_all()

   return
   ####
   # NOTES: Needs files from functions fread_hops_all (max flow count) and fread_rdir_all (redirection count)
   plot_flows_vs_maxflows_and_totalredir() # also plots max inst redir
   ####
   
   ### Service instance and SP
   ## Per instance
   #fread_splog()
   ## Per # of flow for all service instances
   ## DEPENDENCY: fread_hops_all
   #m# 
   fread_splog_all()

   ####
   # NOTES: Needs files from functions fread_hops_all (max flow count) and fread_splog_all (# of instances and SP)
   plot_flows_vs_maxflows_and_avgSP() # also plots max SP 
   plot_flows_vs_maxflows_and_avginst()# also plots max number of instances  
   ####

def plot_flows_vs_rules(ddir, ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree):
   fplot = ddir + "flows_vs_rules.pdf"
   dirs = [ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree]
   
   rules = np.empty([4,len(F)])
   di = -1
   for d in dirs:
      di += 1
      fname = d + "total_rules.npy"
      if not os.path.isfile(fname):
         print "ERROR: %s/total_rules.npy is missing"%d
         continue
      
      #print np.load(fname)
      rules[di] = np.load(fname)

   print rules
   #print rules[0]
   x = F
   plt.plot(x,rules[0],'bs', x, rules[1], 'r--', x, rules[2], 'bs', x, rules[3], 'g^')
   plt.xlabel('# of flows')
   plt.ylabel('# of rules')

   plt.savefig(fplot)
   plt.close()
   

def plot_flows_vs_subflows(ddir, ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree):
   fplot = ddir + "flows_vs_subflows.pdf"

   if os.path.isfile(ddir_ours +  "hops_flows_max.npy") and os.path.isfile(ddir_sdn + "hops_flows_max.npy") :
      print "All OK"
   else:
      print "ERROR: probably one of hops_flows_max.npy is missing"
      return

   flows_o = np.load(ddir_ours + "hops_flows_max.npy") 
   flows_s = np.load(ddir_sdn + "hops_flows_max.npy") 
   
   x = F
   plt.plot(x,flows_o,'bs', x, flows_s, 'r--') #, t, t**2, 'bs', t, t**3, 'g^')
   plt.xlabel('# of flows')
   plt.ylabel('# of sub-flows')

   plt.savefig(fplot)
   plt.close()
   
def plot_flows_vs_(ddir,name_npy,name_out,ylabel, ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree):
   fplot = ddir + name_out
   dirs = [ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree]
   
   rules = np.empty([4,len(F)])
   di = -1
   for d in dirs:
      di += 1
      fname = d + name_npy
      if not os.path.isfile(fname):
         print "ERROR: %s is missing"%f
         continue
      
      #print np.load(fname)
      rules[di] = np.load(fname)

   print rules
   #print rules[0]
   x = F
   #plt.plot(x,rules[0],'bs-', x, rules[1], 'r--', x, rules[2], 'gs-', x, rules[3], 'r^-')


   line1, = plt.plot(x,rules[0], 'b^-', label='Our-Solution Rocketfuel' )
   line2, = plt.plot(x,rules[1], 'r^-', label='Standard-SDN Rocketfuel')
   line3, = plt.plot(x,rules[2], 'b--', label='Our-Solution DataCenter')
   line4, = plt.plot(x,rules[3], 'r--', label='Standard-SDN DataCenter')
   
   #http://matplotlib.org/1.3.0/examples/pylab_examples/legend_demo.html
   # Now add the legend with some customizations.
   legend = plt.legend(loc='upper center', shadow=True)





   plt.xlabel('# of flows')
   plt.ylabel(ylabel)

   plt.savefig(fplot)
   plt.close()


def run_sdn_vs_ours_plots(ddir, sdir_ours,ddir_ours,sdir_ours_tree,ddir_ours_tree, sdir_sdn,ddir_sdn,sdir_sdn_tree,ddir_sdn_tree):
   print "PLOTTING SDN vs OURS"

   ######### NOT NEEDED NOW
   # plot Number of sub-flows-observed
   #plot_flows_vs_subflows(ddir, ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)
   
   # plot Number of rules
   #plot_flows_vs_rules(ddir, ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)
   #############################
   
   # Plotting Number of subflows
   name_npy = 'hops_flows_max.npy'
   name_out = 'flows_vs_subflows.pdf'
   ylabel = '# of subflows'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)


   # Plotting Number of rules
   name_npy = 'total_rules.npy'
   name_out = 'flows_vs_rules.pdf'
   ylabel = '# of rules'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)

   
   # Plotting Total Number of redirections
   name_npy = 'total_redir.npy'
   name_out = 'flows_vs_totalredirections.pdf'
   ylabel = 'Total # of redirections'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)

   # Plotting Max Number of simultaneous redirections
   name_npy = 'inst_redir_max.npy'
   name_out = 'flows_vs_maxredirections.pdf'
   ylabel = 'Maximum # of simultaneous redirections'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)


   # Plotting Avg Shadow Price observed
   name_npy = 'sp__avg.npy'
   name_out = 'flows_vs_avgSP.pdf'
   ylabel = 'Avg. SP Observed'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)


   # Plotting Max Shadow Price observed
   name_npy = 'sp__max.npy'
   name_out = 'flows_vs_maxSP.pdf'
   ylabel = 'Max SP Observed'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)


   # Plotting Avg # of instances observed
   name_npy = 'inst__avg.npy'
   name_out = 'flows_vs_avginst.pdf'
   ylabel = 'Avg. # of instances observed'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)


   # Plotting Max # of instances observed
   name_npy = 'inst__max.npy'
   name_out = 'flows_vs_maxinst.pdf'
   ylabel = 'Max # of instances Observed'
   plot_flows_vs_(ddir, name_npy,name_out,ylabel,ddir_ours, ddir_sdn, ddir_ours_tree ,ddir_sdn_tree)




# Plot Instantaneous redirections vs std_dev in cost for that service
def plot_redir_vs_SPstd(f,dst,src,file_type):
# Needs Instantaneous redirections from fread_rdir_time(f,ddir)
## redir[i][nSF+1] from np.save(f_redir+"_"+file_type[fdiri]+".npy",redir_s), f_redir = rdir + "_redir_per_service_"           
   f_redir = dst + "_redir_per_service__" + str(file_type) + ".npy"

   if 'tree' in str(file_type):
      # This is data center. Get sdn's file for this
      file_sdn = FILE_TYPE[3]
   else:
      file_sdn = FILE_TYPE[1]

   
   #print f_redir
   
   # folder to plot
   fp_dir = dst + "/rdir_vs_sp/"
   ensure_dir(fp_dir)

   if not os.path.isfile(f_redir):
      print "ERROR-0 ", f_redir
      return
   else:
      redir = np.load(f_redir)

   
   max_redir = 0
   sf_max_redir = ''

   j = -1 
   for sf in SF:
      j += 1
      # Needs std_dev from fread_splog_time(f,dst,src,file_type[j])
      ##  a[i][4] from np_a = f_a + "/_Stype_"+str(sf)+"_"+str(file_type)+ ".npy"  , f_a = dst + "_inst_"        
      f_sp = dst + "_inst_" + "/_Stype_"+str(sf)+"_"+str(file_type)+ ".npy"        
      f_sp_sdn = dst + "_inst_" + "/_Stype_"+str(sf)+"_"+str(file_sdn)+ ".npy"        
      #print f_sp
      
      if not os.path.isfile(f_sp):
         print "ERROR-1 ", f_sp
         continue
      else:
         sp = np.load(f_sp)

      if not os.path.isfile(f_sp_sdn):
         print "ERROR-2, running it again might get the .npy files ", f_sp_sdn
         # using return, since if those files are not present, it causes error. 
         #Instead, when I return, the for loop in the calling function runs and
         # the required SDN .npy files are created         
         return 
         
      else:
         sp_sdn = np.load(f_sp_sdn)
      
      # Need to find SP with max redirections
      if (np.sum(redir[:,j+1])) > max_redir:
         max_redir = (np.sum(redir[:,j+1]))
         sf_max_redir = sf


      # file to plot
      fp = fp_dir + str(file_type) + "_"+ str(sf) + "_plot" + ".pdf"
      
      #print fp

      #print "len(sp_sdn),len(sp),len(redir) ",len(sp_sdn),len(sp), len(redir)
      min_len = np.amin([len(sp_sdn),len(sp),len(redir)])
      #max_len = 1300 # Setting it to 1300 for Infocom submission
      #print min_len

      #sp_sdn = sp_sdn[:max_len][:]
      #redir = redir[:max_len][:]
      #sp = sp[:max_len][:]

      if len(sp_sdn) > min_len:
         sp_sdn = sp_sdn[:min_len][:]
         #print "AFTER TRIMMING: len(sp_sdn),len(sp),len(redir) ",len(sp_sdn),len(sp), len(redir)


      if len(sp) > min_len:
         sp = sp[:min_len][:]
         
      if len(redir) > min_len:
         redir = redir[:min_len][:]
      
      
      ######### Plot
      fig, ax1 = plt.subplots()
      t = redir[:,0]
   
      s1 = redir[:,j+1]
      line1, = ax1.plot(t, s1, 'go', label='DRENCH Redirection')
      ax1.set_xlabel('Time (s)')
      # Make the y-axis label and tick labels match the line color.
      ax1.set_ylabel('# of Redirections', color='g')
      for tl in ax1.get_yticklabels():
         tl.set_color('g')
         
      ax1.axis([0, 650, 0, 4])
      #legend = plt.legend(loc='upper center', shadow=True)
      # Mayutan

      s2 = sp[:,4]
      s2_sdn = sp_sdn[:,4]
      ax2 = ax1.twinx()

      #ax2.plot(t, s2, 'b-')
      line2, = ax2.plot(t, s2, 'b-', label='DRENCH' , alpha=0.8, rasterized=True)
      #ax2.plot(t, s2_sdn, 'r-')
      
      #print "len(t), len(s2_sdn), len(s2)", len(t), len(s2_sdn), len(s2)
      line3, = ax2.plot(t, s2_sdn, 'r--', label='D-SDN'  , alpha=0.8, rasterized=True)

      ax2.set_ylabel('Standard Deviation in SP', color='k')
      for tl in ax2.get_yticklabels():
         tl.set_color('k')

      ax2.axis([0, 650, 0, 50])
      legend = plt.legend(loc='upper right', shadow=True)
   
      fig = plt.gcf()
      fig.tight_layout()
      fig.set_size_inches(10.5,4.4)
      plt.savefig(fp)
      plt.close()
      
   print "Service %s has maximum redirections of %s" %(sf_max_redir, str(max_redir))

def run_all_extraction_and_plots():
   global BASE_DIR  
   
   ## OURS
   global SDIR_OURS
   global DDIR_OURS

   ## SDN
   global SDIR_SDN
   global DDIR_SDN
   

   ## OURS-tree
   global SDIR_OURS_TREE
   global DDIR_OURS_TREE
   

   ## SDN-tree
   global SDIR_SDN_TREE
   global DDIR_SDN_TREE
   
      
   BASE_DIR = './' #'logs-----/'
   #print "BASE_DIR = ",BASE_DIR

   ## OURS
   SDIR_OURS = BASE_DIR + 'OURS/'
   DDIR_OURS = SDIR_OURS + 'results/'
   
   ## SDN
   SDIR_SDN = BASE_DIR + 'SDN/'
   DDIR_SDN = SDIR_SDN + 'results/'
   

   ## OURS-tree
   SDIR_OURS_TREE = BASE_DIR + 'OURS_tree/'
   DDIR_OURS_TREE = SDIR_OURS_TREE + 'results/'
   

   ## SDN-tree
   SDIR_SDN_TREE = BASE_DIR + 'SDN_tree/'
   DDIR_SDN_TREE = SDIR_SDN_TREE + 'results/'
   
   
   ################################
   #Individual runs (single flows):
   ################################
   ddir = BASE_DIR + "results_indv/"
   ensure_dir(ddir)

   #print SDIR_SDN, SDIR_SDN_TREE, SDIR_OURS_TREE  
   
   for f in F:
      ##Plotting SP observed by each flow
      fread_sp_perflow(f,ddir) # Note, if evalaution crashes while running next function, comment this out and run it. It is caused due to improper handling of np.load on to arrays and I suppose therefore running out of memory for the next function. 
      fread_sp_perflow_vs(f,ddir) # ours vs SDN
      #continue
      
      #m       fread_hops(f,ddir)
      #m
      print "Doing fread_rdir_time"
      fread_rdir_time(f,ddir)
      # Mayutan
 
      #continue
      ## plotting number of instances and SP per service for each of SDN and our solutions
      file_dir = FILE_DIR #[sdir_ours_tree,sdir_sdn_tree]
      file_type = FILE_TYPE # ['ours_tree','sdn_tree']
      dir = str(f) + 'flows/'
      dst = ddir + str(f)

      j = -1
      for fd in file_dir:
         j+= 1
         fdir = eval(fd)

         fname = FNAME[3] #SUFFIX + "splog_"

         src = fdir + dir + fname

         #m 
         fread_splog_time(f,dst,src,file_type[j])

         ######################################################
         # Plot Instantaneous redirections vs std_dev in cost for that service
         # Needs Instantaneous redirections from fread_rdir_time(f,ddir)
         ## redir[i][nSF+1] from np.save(f_redir+"_"+file_type[fdiri]+".npy",redir_s), f_redir = rdir + "_redir_per_service_"        
         # Needs std_dev from fread_splog_time(f,dst,src,file_type[j])
         ##  a[i][4] from np_a = f_a + "/_Stype_"+str(sf)+"_"+str(file_type)+ ".npy"  , f_a = dst + "_inst_"     
         
         # Just plot it for "ours" since SDN does not have redirections
         
         if "OURS" in fd:
            plot_redir_vs_SPstd(f,dst,src,file_type[j])
         ######################################################         

      print "Doing fread_splog_Alltime"
      src = dst +  "_inst_Alltotal_"  
      fread_splog_Alltime(f,dst,src)
      #######################################################################################
      
      

      
      ######################################################
      


   ##########################
   #Overall runs (all flows):
   ##########################
   #run(sdir_ours,ddir_ours)   
   #run(sdir_sdn,ddir_sdn)
   #run(sdir_ours_tree,ddir_ours_tree)
   #run(sdir_sdn_tree,ddir_sdn_tree)

   ############################################
   ddir = BASE_DIR + 'results/'
   ensure_dir(ddir)
   ###########################################


   ## plot SDN vs OURs
   #run_sdn_vs_ours_plots(ddir,sdir_ours,ddir_ours,sdir_ours_tree,ddir_ours_tree, sdir_sdn,ddir_sdn,sdir_sdn_tree,ddir_sdn_tree)


def plot_argyrios(sdir_lb,ddir_lb):
   fplot = ddir_lb + "argyrios.pdf"
   ensure_dir(ddir_lb)

   D = ['regular_without_redirection/', 'regular_with_redirection/', 'Probabilistic_without_redirection/', 'Probabilistic_with_redirection/']
   SD = ['500ms/','1sec/','2sec/','3sec/','4sec/']
   
   arr_m = np.empty([len(D),len(SD)])
   arr_ml = np.empty([len(D),len(SD)])
   arr_mh = np.empty([len(D),len(SD)])

   di = -1
   for d in D:
      di += 1
      sdi = -1
      for sd in SD:
         sdi += 1
         fopen = sdir_lb + d + sd + "flog_13.csv"
         #print fopen
         if not os.path.isfile(fopen):
            print "ERROR: %s not present"%fopen
            continue
         #else:
            #print fopen
            #continue
         
         file = open(fopen, 'rb')
         data = csv.reader(file, delimiter=',') 
         table = [row for row in data]
         # following works in num of cols is the same for all rows
         numrows = len(table)
         numcols = len(table[0])
         #print numrows, numcols
         


         #continue
         


         #a = np.empty([numrows,4])
         a = np.zeros([numrows,4])
   
         # Trying to extract array of number of service instances
         for i in range(numrows):
            #col[0] is time from zero
            #col[1] will be number of instances
            #col[2] will be # of flows to instance 1
            #col[3] will be # of flows to instance 2
            
            a[i][0] = int(table[i][0]) - int(table[0][0]) 
                  
            # now to extract number of service instances
            b = table[i][2].split(',')
            a[i][1] = len(b)
            #print len(b)
            #continue

            # now to extract # of flows 
            bii = 1 # will use this to put # of flows into array a
            for bi in range(len(b)):
               bii += 1
               t = b[bi].split('-')
               #print t
               if bii == 2 or bii == 3:
                  a[i][bii] = int(t[1])
               else:
                  # This is the case where there are three instances
                  continue
               #print sp
               
         np.set_printoptions(threshold='nan', precision=3)
         #print fopen, a[:,0], a[:,1], a[:,2], a[:,3]
         #print fopen, a[:,2], a[:,3]
         
         # Saving the array
         #np.save(fsave_arr,a)

         # Now to calcuate argyrios formula
         # Start only when both instances are present, i.e. when second instance comes up
         # End when number of instances goes below 2
         avg = np.empty([len(a),1])
         #avg1 = np.empty([len(a),1])
         for j in range(len(a)):
            #set avg array value  to -1
            avg[j,0] = -1.0
            # # of instances > 1
            if not a[j,1] > 1: continue
            
            ideal_flows = float((float(a[j,2]) + float( a[j,3] ))/2.0)
            if abs(a[j,2]-a[j,3]) == 1:
               # If the diff in # of flows is 1, it is fine
               diff_avg = 0
            else:
               diff_avg = float(abs(ideal_flows - a[j,2]) + abs(ideal_flows - a[j,3])) / float(ideal_flows * 2)
            
            avg[j,0] = diff_avg
            #avg1[j,0] = diff_avg
            #print "a[j,2], a[j,3], ideal_flows, diff_avg,avg[j,0] ", a[j,2], a[j,3], ideal_flows, diff_avg, avg[j,0]
         #print avg1
         
         # Now select non -1 values from avg and take average
         tavg = avg[avg[:, 0] != -1]
         
         #print tavg #, avg #, avg1 
         #m = np.mean(tavg)
         #v = np.var(tavg)
         #me = np.median(tavg)
         #m1 = np.nanmean(avg1)
         #m,ml,mh = mean_confidence_interval(tavg) 
         arr_m[di,sdi],arr_ml[di,sdi],arr_mh[di,sdi] = mean_confidence_interval(tavg) 
         #print m,v, me,a,b,c #  len(tavg)
   #print arr_m,arr_ml,arr_mh
   
   x = ['.5', '1', '2','3','4']
   #D = ['regular_without_redirection/', 'regular_with_redirection/', 'Probabilistic_without_redirection/', 'Probabilistic_with_redirection/']
  
   #plt.plot(x,arr_m[0,:],'r--', x, arr_m[1,:], 'r^-', x, arr_m[2,:], 'b--', x, arr_m[3,:], 'b^-')
   

   #plt.plot(x,arr_m[0,:], label="Line 1", linestyle='r--')
   #plt.plot(x,arr_m[1,:], label="Line 2", linestyle='r^-')
   #plt.plot(x,arr_m[2,:], label="Line 3", linestyle='b--')
   #plt.plot(x,arr_m[3,:], label="Line 4", linestyle='b^-')

   #plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, mode="expand", borderaxespad=0.)

   

   line1, = plt.plot(x,arr_m[0,:], 'r--', label='Regular w/o redirection' )
   line2, = plt.plot(x,arr_m[1,:], 'r^-', label='Regular w redirection')
   line3, = plt.plot(x,arr_m[2,:], 'b--', label='Probabilistic w/o redirection')
   line4, = plt.plot(x,arr_m[3,:], 'b^-', label='Probabilistic w redirection')
   #plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, mode="expand", borderaxespad=0.)
   #plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)})        
   #plt.legend(handles=[line1,line2,line3,line4]) 


   #http://matplotlib.org/1.3.0/examples/pylab_examples/legend_demo.html
   # Now add the legend with some customizations.
   legend = plt.legend(loc='upper center', shadow=True)


   plt.xlabel('Rate of arrival of flows')
   plt.ylabel('Average Argyrios')

   
   plt.savefig(fplot)
   plt.close()

   

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h


if __name__ == '__main__':
   


   # http://matplotlib.org/users/pyplot_tutorial.html
   run_all_extraction_and_plots()

   
   ## plot what argyrios wanted
   sdir_lb = 'logs--/LB2/'
   ddir_lb = 'logs--/LB2/results/'
   # plot_argyrios(sdir_lb,ddir_lb)





   # n_service_instance
   #filewrite_val(5)
   #filewrite_val(10)
   #filewrite_val(15)
   #test()
   #plot()
   #hist()
   #filewrite()   
   #filereadw() # file read with "with". This cleans up better
   #fileread()
