


def getPacket():
  # Get interface name
  interface_list = netifaces.interfaces()
  interface = filter(lambda x: 'eth0' in x,interface_list)
  if len(interface) == 0:
    print "No interface found"
    return
  
  global host_interface
  global host_mac_addr
  global host_ip_addr
  
  addrs      = netifaces.ifaddresses(interface[0])
  link_addrs = addrs[netifaces.AF_LINK]
  net_addrs  = addrs[netifaces.AF_INET]
  host_interface  = interface[0]
  host_mac_addr   = link_addrs[0]['addr']
  host_ip_addr    = net_addrs[0]['addr']
#!/usr/bin/python

import subprocess
import commands
import csv
import time
import sys
import os
from os.path import expanduser
hdir_p = expanduser('~')
MAX_ITERATIONS = 5
INTERVAL_IN_SECONDS = 1
MAX_SWITCHES = 87
flow_counter_logfile = hdir_p + '/drench/results/ovs_rules_count.csv'
def write_pkt(cur_time = 0, total_num_of_flows=0,total_vlan_flows=0,vlan_rule_count=0):
  with open(flow_counter_logfile, 'ab') as csvfile:
    rxWriter = csv.writer(csvfile,delimiter=',')
    rxWriter.writerows([[str(cur_time), str(total_num_of_flows), str(total_vlan_flows), str(vlan_rule_count)]])
  return

def process_output_and_log():
  with open('all_flowcount.txt', "r") as f:
    total_rules = int(f.readline().strip('\n'))
    os.remove('all_flowcount.txt')
  with open('all_vlan_flowcount.txt', "r") as f:
    total_vlan_rules = int(f.readline().strip('\n'))
    os.remove('all_vlan_flowcount.txt')
  with open('only_vlan_flow_count.txt', "r") as f:
    only_vlan_rules = int(f.readline().strip('\n'))
    os.remove('only_vlan_flow_count.txt')
  write_pkt(cur_time = int(time.time()), total_num_of_flows=total_rules,total_vlan_flows=total_vlan_rules,vlan_rule_count=only_vlan_rules)
  return
  
def startLogging(num_of_iterations=MAX_ITERATIONS, interval=INTERVAL_IN_SECONDS, num_of_switches=MAX_SWITCHES):
  
  # Create subprocess to execute command
  #comTcpDump = "./log_switch_flow_count.sh" + str(num_of_switches)
  #p = subprocess.Popen(comTcpDump, stdout=subprocess.PIPE, shell=True)
  for x in range(num_of_iterations):
    subprocess.Popen(['./log_switch_flow_count.sh', str(num_of_switches)], shell=False)
    time.sleep(interval)
    process_output_and_log()
  
  return
  
if __name__ == '__main__':
  if(len(sys.argv)>1):
    num_switches = int(sys.argv[1])
  else:
    num_switches = MAX_SWITCHES
  startLogging(num_of_iterations=100, interval=5, num_of_switches=num_switches)
