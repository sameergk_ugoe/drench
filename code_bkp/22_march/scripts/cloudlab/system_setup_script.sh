#!/bin/sh

sudo -i
#Check if already installed:
if [ -f ~/drench_install.txt ]; then
  echo " File: ~/drench_install.txt Exits!!"
  exit 0
fi

cwd = `pwd`

#set exec permisions for /drench
cd /drench
chmod u+x **/*.sh
chmod u+x **/*.bash
#find ./ -name "*.sh" -exec chmod +x {} \;
cd $cwd

#change direcotry to local
cd /local

#configure for silent updates
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -q
#apt-get install -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" apache2 mysql-server

# Install git
sudo apt-get install -y git

#Install curl and wget
sudo apt-get install -y curl
sudo apt-get install -y wget
sudo apt-get install -y unzip

# Install Python Dev Tools
sudo apt-get install -y python-pip python-dev build-essential

# Intall Scapy
sudo apt-get install -y scapy

# Upgrade PIP and Install netifaces
sudo -H pip install --upgrade pip 
sudo -H pip install --upgrade virtualenv 
sudo -H pip install netifaces

# Install maptplotlib numpy
#sudo -H pip install numpy
#sudo -H pip install scipy
#sudo -H pip install matplotlib

# Downlaod and Install D-ITG 
sudo wget http://traffic.comics.unina.it/software/ITG/codice/D-ITG-2.8.1-r1023-src.zip
sudo unzip D-ITG-2.8.1-r1023-src.zip
cd D-ITG-2.8.1-r1023/src
sudo make clean all
sudo make install PREFIX=/usr/local
cd ../../

# Downlaod and Install Iperf
sudo git clone https://github.com/esnet/iperf
cd iperf
sudo ./configure
sudo make
sudo make install
cd ..

#Download and Install Mininet and OpenvSwitch
sudo git clone git://github.com/mininet/mininet
cd mininet
sudo git tag
sudo git checkout -b 2.2.1 2.2.1
cd ..
sudo chmod +x -R *.sh *.py
sudo mininet/util/install.sh -a
sudo mn --test pingall > ~/mn_pingout.txt 2>&1

echo "DRENCH Installation Complete!" > ~/drench_install.txt

cd $cwd
#Downlaod and Install OpenvSwitch from GIT Repo
#part of mininet -a (no need to install again)

#Dowload and SYNC DRECN code from GIT Repo
#sudo git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git
#cd drench
#sudo chmod +x -R *.sh *.py
#cd ..
#sudo mkdir /home/mininet
#sudo cp -r drench /home/mininet/

