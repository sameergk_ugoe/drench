#!/usr/bin/python

#
# Program to generate packets with vlan
#
import subprocess
import commands
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import sendp, sniff, ICMP, Dot1Q, Ether, Raw, IP
#from scapy.layers.inet import IP
import csv
import netifaces
import time
import sys
import threading
import thread
import sys
from random import randint
from threading import Thread

host_interface = "hx-eth0"
host_mac_addr = "00:00:00:00:00:00"
host_ip_addr = "00.00.00.00"

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
payload = ",svclist,"
nvlan = 0
#nvlan = 251 #[1111 1011] (BC)
#nvlan = 4029 #[1111 1011 1101] (BCA)
#nvlan = 3069 #[1011 1111 1101] (CBA)
#nvlan = 3579 #[1101 1111 1011] (ABC)
#nvlan = 13 #(A)
#nvlan = 253 #[1111 1101] (BA)

sChain = []
nid = 1
nseq = 1
interval = 0.5
count = 10
flow_id = 0
path_cost = 0
done = False
tx_file = '/home/mininet/my_code/tx.csv'
svc_tx_file = '/home/mininet/my_code/svc_tx.csv'
svc_ts_file = '/home/mininet/my_code/svc_ts.csv'
def writeData(id=nid, seq=nseq, idst=ipdst, dst=edst, flow=flow_id):
    with open(tx_file, 'ab') as csvfile:
        txWriter = csv.writer(csvfile,delimiter=',')
        txWriter.writerows([[str(time.time()), id, seq, flow_id, ipsrc,idst]])
        #txWriter.writerows([[str(time.time()), id, seq, flow_id, ipsrc,esrc, idst,edst]])

def write_pkt(vlan=0, id=1, seq=1):
  svc_tx_file_func = svc_tx_file.split('.')[0] + '_' + str(vlan) +'.'+svc_tx_file.split('.')[1]
  with open(svc_tx_file_func, 'ab') as csvfile2:
        tx2Writer = csv.writer(csvfile2,delimiter=',')
        tx2Writer.writerows([[str(time.time()), str(id), str(seq), str(vlan), str(host_ip_addr), str(host_interface)]])
  return

def log_latency_data(id=0, seq=0,cost=0,ts_list=[]):
  try:
    for i, x in enumerate(ts_list):
      data = x.split(':')
      vlid,ipid = data[0].split('-')[0], data[0].split('-')[1]
      #vlan-IP = data[0], TS = data[1]
      svc_ts_file_func = svc_ts_file.split('.')[0] + '_' + str(data[0]) + '.' + svc_ts_file.split('.')[1]
      with open(svc_ts_file_func, 'ab') as csvfile3:
          tx3Writer = csv.writer(csvfile3,delimiter=',')
          #tx3Writer.writerows([[ str(id), str(seq), str(cost), str(data[0]), str(data[1]), str(host_ip_addr)]])
          tx3Writer.writerows([[ str(id), str(seq), str(cost), str(vlid), str(ipid), str(data[1]) ]])
  except Exception,e:
    print "Exception logging ping response packet!!", str(e)
  return

def getFunction():

    interface_list = netifaces.interfaces()
    interface = filter(lambda x: 'eth0' in x,interface_list)
    if len(interface) == 0:
        print "No interface found"
        return
  
    global ipsrc
    global esrc
    global host_interface
    global host_mac_addr
    global host_ip_addr
    addrs      = netifaces.ifaddresses(interface[0])
    link_addrs = addrs[netifaces.AF_LINK]
    net_addrs  = addrs[netifaces.AF_INET]
    host_interface  = interface[0]
    host_mac_addr   = link_addrs[0]['addr']
    host_ip_addr    = net_addrs[0]['addr']
    esrc = host_mac_addr
    ipsrc = host_ip_addr
    
    global nvlan
    global sChain
    global nid
    global nseq
    global interval
    global flow_id
    global count
    global ipdst
    global edst

    nid = randint(0,99999)
    nseq = 1
    src_id = 0
    dst_id = 0
    try:
        #parse arguments <svc_chain, esrc, edst, count, flow_id, ping_interval>
        i = 6
        if(len(sys.argv)>i):
            ival = float(sys.argv[i])
            if ival > 0:
              interval = ival
        i -=1
        
        if(len(sys.argv)>i):
            flow_id = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            count = int(sys.argv[i])
        i -=1
        
        if(len(sys.argv)>i):
            dst_id = int(sys.argv[i])
            ipdst = "10.0.0."+ sys.argv[i]
            edst  = "00:00:00:00:00:" + "{:0>2d}".format(dst_id)
            #if dst_id < 10:
            #    edst = "00:00:00:00:00:0"+sys.argv[i]
            #else:
            #    edst = "00:00:00:00:00:"+sys.argv[i]
        i -=1
        
        if(len(sys.argv)>i):
            src_id = int(sys.argv[i])
            #print src
        i-=1
        
        if(len(sys.argv)>i):
            sChain = sys.argv[1].split(",")
            sChain = [x for x in sChain if x.isdigit() is True]
            #nvlan = int(sChain.pop(0),2)
            nvlan = int(sChain[0],2)
            print "sChain:", sChain, "vlan: %d" %nvlan 
            #print "vlan: %s" %str(nvlan)
        print "Generator: %s to_Dst:%s(%d), Service Chain:%s, First Service: %d , num_packets:%d and Flow_ID:%d, ping_interval:%f " %(host_ip_addr,edst,dst_id,sChain,nvlan,count,flow_id,interval)
    except Exception,e:
        print "Exception parsing input args!!", str(e)
        exit()
    
def sendPings():
  global nseq
  global flow_id
  global payload
  if not sChain:
    d=',end,'
    payload = ",svclist," + d
    #print "payload without Schain:", payload
  else:
    d= ','.join(map(str, sChain)) +',end,0,'
    payload = ",svclist," + d 
    #print "payload with sChain:", payload
  payload += "0-" + host_ip_addr + ":" + str(time.time()) +','
  try:
    sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload, verbose=0)
    #sendp(Ether(dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload, verbose=0)
    #pkt = Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/payload
    #sendp(pkt,verbose=0,)
  except Exception,e:
    print "Failed to Send Packets with Exception:", str(e)
    #exit()
    return -1
  writeData(id=str(nid), seq=str(nseq), idst=str(ipdst),dst=str(edst),flow=flow_id )
  #if nvlan != 0:
    #write_pkt(vlan=nvlan, id=nid, seq=nseq)
    #pass
  nseq = nseq+1
  return 0

def startPings():
  global count
  MAX_RETRY_COUNT = 0
  print "START: At[%s], StartPings[%d]" %(time.time(),count)
  ret = 0
  retry_count = 0
  while retry_count <= MAX_RETRY_COUNT:
    for x in range(count):
      try:
        ret = sendPings()
        if ret:
          print "Aborting!!"
          raise Exception('sendPing() Failed!!')
          return
        time.sleep(interval)
      except Exception,e:
        print "Exception:",str(e)
        break
    retry_count +=1
    if ret == 0:
      break
    else:
      time.sleep(max(3,interval*5))
      print "Retry attempt [%d]" %retry_count
  print "End At[%s], StartPings[%d]" %(time.time(),count)
  return 

def isIncoming(pkt):
  try:
    if pkt[IP].src == host_ip_addr:
      return False 
    if pkt.haslayer(Dot1Q):
      return False
    if pkt.haslayer(ICMP) is False:
      return False
    if pkt[ICMP].payload is None:
      return False
    
    #print "Packet is:", pkt.show()
    #print a,pkt[ICMP].type
    if pkt[IP].dst==host_ip_addr and pkt[ICMP].type==0:
      if pkt[ICMP].payload and ',' == str(pkt[ICMP].payload)[-1]:
        return True
  except:
    pass
  return False

def PacketHandler(pkt):
  global path_cost
  payload = str(pkt[ICMP].payload)
  
  try:
    payload.rstrip()
    id=pkt[ICMP].id
    seq=pkt[ICMP].seq
    of = payload.find(',end,')
    if of < 0:
      raise Exception('Incorrect Payload', ',end, not found!!')
    ln = len(',end,')
    cost = payload[of+ln:payload.find(',', of+ln)]
    if cost is None or len(cost) == 0 or False == cost.isdigit():
      raise Exception('Incorrect Payload', 'cost is not a number!!')
      pass
    else:
      path_cost = int(cost)
      ln += len(cost) + len(',')
      ts_list = payload[of+ln:].split(',')
      ts_list=[x for x in ts_list if ':' in x] 
      log_latency_data(id=id, seq=seq, cost=path_cost, ts_list=ts_list )
  except Exception, e:
    print "Parse for Payload data Failed to find the Path cost!!", str(e)
    #time.sleep(10)
  return

def startSniffer(name=None,**args):
  #print " In Sniffer: for ", host_interface
  global done
  while not done:
    #time.sleep(5)
    comTcpDump = sniff(iface=host_interface, lfilter = isIncoming, prn = PacketHandler, store=0,count =1,timeout=max(1, interval*5))
  print "Exiting sniffer thread!!"
  
def startSnifferThread_and_Pings():
  global done
  try:
    #print "Launching Thread"
    #thread.start_new_thread( startSniffer, ("SNIFF", 10)) #OLD MODE
    #New Approach
    #t = Thread(group=None,target=startSniffer,name="SNIFF", args=(), kwargs={})
    #t.start()
    startPings()
    #time.sleep(max(5, interval*10))
    done = True
    #time.sleep(max(10, interval*10))
    #t.join()
  except Exception, e:
    print str(e)
    return

if __name__ == '__main__':
    getFunction()
    startSnifferThread_and_Pings()
    #startPings()
    #time.sleep(max(10, interval*10))
