#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.topolib import TreeTopo
from mininet.node import RemoteController
from mininet.log import setLogLevel
from mininet.cli import CLI

def demo():
  "Simple Demo for FCSC"
  topo = Topo()

  # Add switches
  s1 = topo.addSwitch('s1')
  s2 = topo.addSwitch('s2')
  s3 = topo.addSwitch('s3')
  s4 = topo.addSwitch('s4')
  s5 = topo.addSwitch('s5')
  s6 = topo.addSwitch('s6')
  
  # Add nodes
  h1 = topo.addHost ('h1', ip='10.0.0.1')
  h2 = topo.addHost ('h2', ip='10.0.0.2')
  h3 = topo.addHost ('h3', ip='10.0.0.3')
  h4 = topo.addHost ('h4', ip='10.0.0.4')
  h5 = topo.addHost ('h5', ip='10.0.0.5')
  h6 = topo.addHost ('h6', ip='10.0.0.6')
  h7 = topo.addHost ('h7', ip='10.0.0.7')
  
  # Add Hosts to Switch links
  topo.addLink(s1, h1)
  topo.addLink(s2, h2)
  topo.addLink(s3, h3)
  topo.addLink(s4, h4)
  topo.addLink(s5, h5)
  topo.addLink(s5, h6)

  # Add Switch to Switch links
  topo.addLink(s1, s2)
  topo.addLink(s1, s5)
  topo.addLink(s2, s4)
  topo.addLink(s3, s4)
  topo.addLink(s4, s5)

  net = Mininet(topo=topo, controller=RemoteController, autoSetMacs = True, autoStaticArp = True, cleanup = True)
  c1 = net.addController(name = 'c1', controller = RemoteController, port = 6633 )
  # Start network
  #net.build()
  c1.start()
  for s in net.switches[0:6]:
    s.start([c1])
  

  net.start()
  net.pingAll()
  for h in net.hosts[0:4]:
    h.pexec(['xterm', 'python', '-u', 'hostprocess_sk.py'])
    #h.cmdPrint('sudo python -u hostprocess_sk.py &')
  CLI( net )
  net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    demo()