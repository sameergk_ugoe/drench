#!/usr/bin/python

import subprocess
import commands
from scapy.all import sendp, sniff, IP, ICMP, Dot1Q, Ether, Raw
import csv
import netifaces
import time
import sys

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
payload = ",svclist,"
inter = ''
svc_list = []
ipaddr = ""
 
def getID(list):
  print "\n List:", list
  id = filter(lambda x: 'id' in x, list)
  print "\n ID:", id
  list = id[0].split('id ')
  return int(list[1])

def getVLAN(list):
  try:
    vlan = filter(lambda x: 'vlan' in x, list)
    list = vlan[0].split('vlan ')
    print "VLAN ID: %d" %int(list[1])
    return int(list[1])
  except:
    return 0
  
def modVLAN(ovlan):
  nvlan = ovlan >> 4
  if nvlan == 0:
    return 0
  else:
    return nvlan
  
def getSEQ(list):
  id = filter(lambda x: 'seq' in x, list)
  list = id[0].split('seq ')
  return int(list[1])

def getSrc(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[0].split(' ')
  print "SRC MAC: %s" %str(mac[1])
  return str(mac[1])
  
def getDst(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[1].split(' ')
  print "DST MAC: %s" %str(mac[0])
  return str(mac[0])
  
def getDestIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[1].split(':')
  print "DST IP: %s" %str(ip[0])
  return str(ip[0])
  
def getSrcIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[0].split(':')
  print "SRC IP: %s" %str(ip[0])
  return str(ip[0])

def getSVCList(list):
	gloabl svc_list
	print "\n Payload: %s", list
	id = filter(lambda x: 'svclist' in x, list)
	print "\n LIST ID: %s", id
	svc_list = id[0].split('svclist')
	print "\n SVC_LIST: %s", svc_list
	#sv = (sChain.pop(0))
	sc = ','.join(map(str, svc_list[1]))
	return sc

def getOwnMAC():
	mc = ""
	no=ipaddr.split(".")
	if int(no[3]) < 10:
		mc = "00:00:00:00:00:0"+no[3]	#############
	else:
		mc = "00:00:00:00:00:"+no[3]	#############
	if len(mc) > 0:
		return mc
	else:
		return "NONE"
  
def getNextVlan():
	global svc_list
	sv = 0
	if not svc_list:
		sv = 0
	else:
		sv = int(svc_list.pop(0))
	return sv



def createPkt(nvlan, nid, nseq, ipsrc, ipdst, ndst, ndata):
	if nvlan>0:
		#sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
		sendp(Ether(dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/ndata)

	else:
		#sendp(Ether(src=esrc, dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
		sendp(Ether(dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))

def dropPacket(nid, nseq):
	#print 'dropPacket'
	with open('/home/mininet/drop.csv', 'ab') as csvfile:
		txWriter = csv.writer(csvfile,delimiter=',')
		txWriter.writerows([[str(nid), str(nseq), str(time.time()), str(ipaddr)]])
	
	
def parseDump(op):
	global svc_list
	list = op[0]
	list = list.split(', ')
	print "List is:", list
	vlan = getVLAN(list)
	if vlan>0:
		id = getID(list)
		seq = getSEQ(list)
		#nvlan= modVLAN(vlan)
		dst = getDst(list)
		src = getSrc(list)
		dstip = getDestIP(list)
		srcip = getSrcIP(list)
		svc_list = getSVCList(op)
		nvlan = getNextVlan()
		data = payload+svc_list
		ownmac = getOwnMAC()
		with open('/home/mininet/list.csv', 'rb') as csvfile:							#EM030415
			reader = csv.DictReader(csvfile)											#EM030415
			for row in reader:															#EM030415
				if ownmac==row['mac'] or ownmac=="NONE":								#EM030415
					createPkt(nvlan, id, seq, srcip, dstip, dst, data)
					return
				dropPacket(id, seq)
	else:
		print str(list)
		id = getID(list)
		seq = getSEQ(list)
		timest = time.time()
		ip = getDestIP(list)
		# Write only if this host is destination
		if ip == host_ip_addr:
			with open('/home/mininet/rx.csv', 'ab') as csvfile:
				rxWriter = csv.writer(csvfile,delimiter=',')
				rxWriter.writerows([[str(id), str(seq), str(timest), str(host_interface)]])
	return

def getPacket():
	# Get interface name
	global ipaddr
	interface_list = netifaces.interfaces()
	interface = filter(lambda x: 'eth0' in x,interface_list)
	#############
	global inter
	inter = interface[0]
	if len(interface) == 0:
		print "No interface found"
		return	
	# Get own IP address
	netifaces.ifaddresses(interface[0])
	ipaddr = netifaces.ifaddresses(interface[0])[2][0]['addr']
	print 'IP Address: %s' %ipaddr
	# Create tcpdump command
	comTcpDump = "sudo tcpdump icmp[icmptype] == 8 -A -e -Ul -i " + interface[0]
	# Create subprocess to execute command
	p = subprocess.Popen(comTcpDump, stdout=subprocess.PIPE, shell=True)
	lines = iter(p.stdout.readline, b'')
	packet = []
	for line in lines:
		parseDump(line)
  
if __name__ == '__main__':
    getPacket()