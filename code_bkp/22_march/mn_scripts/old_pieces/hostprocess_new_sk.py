#!/usr/bin/python

import subprocess
import commands
from scapy.all import sendp, sniff, IP, ICMP, Dot1Q, Ether, Raw
import csv
import netifaces
import time
import pcap
conf.use_pcap=True

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"
payload = ",svclist="

host_interface = "Hx-eth0"
host_mac_addr  = "00:00:00:00:00:00"
host_ip_addr   = "10.0.0.0"
  
def getID(list):
  id = filter(lambda x: 'id' in x, list)
  list = id[0].split('id ')
  return int(list[1])

def getVLAN(list):
  try:
    vlan = filter(lambda x: 'vlan' in x, list)
    list = vlan[0].split('vlan ')
    return int(list[1])
  except:
    return 0
  
def modVLAN(ovlan):
  nvlan = ovlan >> 4
  if nvlan == 0:
    return 0
  else:
    return nvlan
  
def getSEQ(list):
  id = filter(lambda x: 'seq' in x, list)
  list = id[0].split('seq ')
  return int(list[1])

def getSrc(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[0].split(' ')
  print "SRC MAC: %s" %str(mac[1])
  return str(mac[1])
  
def getDst(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[1].split(' ')
  print "DST MAC: %s" %str(mac[0])
  return str(mac[0])
  
def getDestIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[1].split(':')
  print "DST IP: %s" %str(ip[0])
  return str(ip[0])
  
def getSrcIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[0].split(':')
  print "SRC IP: %s" %str(ip[0])
  return str(ip[0])

def getSVCList(list):
  print "\n LIST: %s", list
  id = filter(lambda x: 'svclist' in x, list)
  print "\n LIST ID: %s", id
  svc_list = id[0].split('svclist=')
  print "\n SVC_LIST: %s", svc_list
  return svc_list[1]
  
def getNextVlan(vlan = 0,svc_list=0):
  if vlan == 0 or svc_list == 0:
    return 0
  
  mask = 0x0F
  cur_vlan = svc_list & mask
  while cur_vlan != vlan:
    mask <<= 4
    cur_vlan = svc_list & mask
  return (svc_list & mask <<4)

def createPkt(nvlan=0, nid=1, nseq=1):
  if nvlan>0:
    #sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
    sendp(Ether(dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
  else:
    #sendp(Ether(src=esrc, dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    sendp(Ether(dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
  
def createPkt2(nvlan=0, nid=1, nseq=1, ipsrc=ipsrc, ipdst=ipdst, ndst=edst, nsrc=esrc):
  if nvlan>0:
    sendp(Ether(src=nsrc,dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    #sendp(Ether(dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
  else:
    sendp(Ether(src=nsrc,dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    #sendp(Ether(dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    
def createPkt3(nvlan=0, nid=1, nseq=1, ipsrc=ipsrc, ipdst=ipdst, ndst=edst, nsrc=esrc, npayload=""):
  if nvlan>0:
    sendp(Ether(src=nsrc,dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq)/npayload)
  else:
    sendp(Ether(src=nsrc,dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))

def parseDump(op):
  list = op.split(', ')
  vlan = getVLAN(list)
  if vlan>0:
    id = getID(list)
    seq = getSEQ(list)
    ovlan= modVLAN(vlan)
    dst = getDst(list)
    src = getSrc(list)
    dstip = getDestIP(list)
    srcip = getSrcIP(list)
    svc_list = getSVCList(list)
    data = payload+svc_list
    ovlan2 = getNextVlan(vlan,int(svc_list))
    print "ovlan= %d, ovlan2= %d" %(ovlan,ovlan2)
    #createPkt(ovlan, id, seq)
    #createPkt2(nvlan=ovlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=src)
    createPkt3(nvlan=ovlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=host_mac_addr,npayload=data)
  else:
    print str(list)
    id = getID(list)
    seq = getSEQ(list)
    timest = time.time()
    ip = getDestIP(list)
    # Write only if this host is destination
    if ip == host_ip_addr:
      with open('/home/mininet/rx.csv', 'ab') as csvfile:
        rxWriter = csv.writer(csvfile,delimiter=',')
        rxWriter.writerows([[str(id), str(seq), str(timest), str(host_interface)]])
    return

def processPacket(pkt): 
  #print'processPacket'
  #print "\n processPacket:", pkt
  print "\n Packet sniffed: %s" % (pkt.summary())
  nseq = int(pkt[ICMP].seq)
  nid = int(pkt[ICMP].id)
  load = str(pkt[ICMP].payload)
  print "\n Seq = ", str(nseq), "Nid:", str(nid), "Payload:", load
  
  # if load is not None and len(load) > 1:
    # ipChain = load.split(",")
    # ipdst=ipChain.pop(0)
    # with open('/home/mininet/listip.csv', 'rb') as csvfile:             #EM030415
      # reader = csv.DictReader(csvfile)                      #EM030415
      # for row in reader:                              #EM030415
        # if ipaddr == row['ip']:                       #EM030415
          # sendPings()
          # return
      # dropPacket()
  # else:
    # writeData(pkt)
    
def PacketHandler(pkt):
  #print 'PacketHandler'
  processPacket(pkt)
  
def isIncoming(pkt):
  if Dot1Q in pkt:
    print "Dot1Q:", pkt[Dot1Q]
  if IP in pkt:
    #print pkt[IP].src
    pass
  if ICMP in pkt and pkt[ICMP].type==8:
    #print "\n Packet is:",pkt
    #print "\n ICMP Type:", pkt[ICMP].type
    return True
  return False

def getPacket():
  # Get interface name
  interface_list = netifaces.interfaces()
  interface = filter(lambda x: 'eth0' in x,interface_list)
  if len(interface) == 0:
    print "No interface found"
    return
  
  global host_interface
  global host_mac_addr
  global host_ip_addr
  
  addrs      = netifaces.ifaddresses(interface[0])
  link_addrs = addrs[netifaces.AF_LINK]
  net_addrs  = addrs[netifaces.AF_INET]
  host_interface  = interface[0]
  host_mac_addr   = link_addrs[0]['addr']
  host_ip_addr    = net_addrs[0]['addr']

  print "host_interface:", host_interface, "host_mac_addr:", host_mac_addr, "host_ip_addr:", host_ip_addr
  comTcpDump = sniff(iface=host_interface, lfilter = isIncoming, prn = PacketHandler)
  
  
if __name__ == '__main__':
    getPacket()