#!/usr/bin/python

import subprocess
import netifaces
import commands
from scapy.all import sendp, IP, ICMP, Dot1Q, Ether
import time
import csv

ipsrc = "10.0.0.5"
ipdst = "10.0.0.6"
esrc = "00:00:00:00:00:05"
edst = "00:00:00:00:00:06"

host_interface = "Hx-eth0"
host_mac_addr  = "00:00:00:00:00:00"
host_ip_addr   = "10.0.0.0"
  
def getID(list):
  id = filter(lambda x: 'id' in x, list)
  list = id[0].split('id ')
  return int(list[1])

def getVLAN(list):
  try:
    vlan = filter(lambda x: 'vlan' in x, list)
    list = vlan[0].split('vlan ')
    return int(list[1])
  except:
    return 0
  
def modVLAN(ovlan):
  nvlan = ovlan >> 4
  if nvlan == 0:
    return 0
  else:
    return nvlan
  
def getSEQ(list):
  id = filter(lambda x: 'seq' in x, list)
  list = id[0].split('seq ')
  return int(list[1])

def getSrc(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[0].split(' ')
  print "SRC MAC: %s" %str(mac[1])
  return str(mac[1])
  
def getDst(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[0].split('> ')
  mac = list[1].split(' ')
  print "DST MAC: %s" %str(mac[0])
  return str(mac[0])
  
def getDestIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[1].split(':')
  print "DST IP: %s" %str(ip[0])
  return str(ip[0])
  
def getSrcIP(list):
  dst = filter(lambda x: '>' in x, list)
  list = dst[1].split('> ')
  ip = list[0].split(':')
  print "SRC IP: %s" %str(ip[0])
  return str(ip[0])

def createPkt(nvlan=0, nid=1, nseq=1):
  if nvlan>0:
    #sendp(Ether(src=esrc, dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
    sendp(Ether(dst=edst)/Dot1Q(vlan=nvlan)/IP(dst=ipdst)/ICMP(id=nid, seq=nseq))
  else:
    #sendp(Ether(src=esrc, dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    sendp(Ether(dst=edst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
  
def createPkt2(nvlan=0, nid=1, nseq=1, ipsrc=ipsrc, ipdst=ipdst, ndst=edst, nsrc=esrc):
  if nvlan>0:
    sendp(Ether(src=nsrc,dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    #sendp(Ether(dst=ndst)/Dot1Q(vlan=nvlan)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
  else:
    sendp(Ether(src=nsrc,dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))
    #sendp(Ether(dst=ndst)/IP(src=ipsrc, dst=ipdst)/ICMP(id=nid, seq=nseq))

def parseDump(op):
  list = op.split(', ')
  vlan = getVLAN(list)
  if vlan>0:
    id = getID(list)
    seq = getSEQ(list)
    ovlan= modVLAN(vlan)
    dst = getDst(list)
    src = getSrc(list)
    dstip = getDestIP(list)
    srcip = getSrcIP(list)
    #createPkt(ovlan, id, seq)
    #createPkt2(nvlan=ovlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=src)
    createPkt2(nvlan=ovlan, nid=id, nseq=seq, ipsrc=srcip, ipdst=dstip, ndst=dst,nsrc=host_mac_addr)
  else:
    print str(list)
    id = getID(list)
    seq = getSEQ(list)
    timest = time.time()
    ip = getDestIP(list)
    # Write only if this host is destination
    if ip == host_ip_addr:
      with open('/home/mininet/rx.csv', 'ab') as csvfile:
        rxWriter = csv.writer(csvfile,delimiter=',')
        rxWriter.writerows([[str(id), str(seq), str(timest), str(host_interface)]])
    return

def getPacket():    
  # Get interface name
  interface_list = netifaces.interfaces()
  interface = filter(lambda x: 'eth0' in x,interface_list)
  if len(interface) == 0:
    print "No interface found"
    return
  
  global host_interface
  global host_mac_addr
  global host_ip_addr
  
  addrs      = netifaces.ifaddresses(interface[0])
  link_addrs = addrs[netifaces.AF_LINK]
  net_addrs  = addrs[netifaces.AF_INET]
  host_interface  = interface[0]
  host_mac_addr   = link_addrs[0]['addr']
  host_ip_addr    = net_addrs[0]['addr']

  print "host_interface:", host_interface, "host_mac_addr:", host_mac_addr, "host_ip_addr:", host_ip_addr
  
  # Create tcpdump command
  comTcpDump = "sudo tcpdump icmp[icmptype] == 8 -e -l -i " + interface[0]
  #comTcpDump = "sudo tcpdump -l -i " + interface[0] + " -Uw - | tcpdump -l  -en -r - vlan"
  # Create subprocess to execute command
  p = subprocess.Popen(comTcpDump, stdout=subprocess.PIPE, shell=True)
  lines = iter(p.stdout.readline, b'')
  for line in lines:
    parseDump(line)
  
if __name__ == '__main__':
    getPacket()