import socket, struct, os, array
from scapy.all import ETH_P_ALL, ETH_P_IP
from scapy.all import select
from scapy.all import MTU
from scapy.all import Ether, ICMP, UDP

UDP_PORT_LOW=10000
def process_scapy_pkt(pkt):
    print len(pkt)
    pkt_e = Ether(pkt)
    if pkt_e.haslayer(ICMP) and pkt_e[ICMP].type == 8:
        #print pkt_e.show()
        return True, pkt_e
    if pkt_e.haslayer(UDP) and (pkt_e[UDP].dport == 8999 or pkt_e[UDP].dport >= UDP_PORT_LOW):
        #print pkt_e.show()
        return True, pkt_e
    return False, pkt_e
    
class IPSniff:
 
    def __init__(self, interface_name, on_ip_incoming, on_ip_outgoing):
 
        self.interface_name = interface_name
        self.on_ip_incoming = on_ip_incoming
        self.on_ip_outgoing = on_ip_outgoing
 
        # The raw in (listen) socket is a L2 raw socket that listens
        # for all packets going through a specific interface.
        self.ins = socket.socket(
            socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_IP))
        self.ins.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 2**30)
        self.ins.bind((self.interface_name, ETH_P_IP))
 
    def __process_ipframe(self, pkt_type, pkt):

        #eth_header = struct.unpack("!6s6sH", pkt[0:14])
        #dummy_eth_protocol = socket.ntohs(eth_header[2])
        #if eth_header[2] != 0x800 :
        #    continue
        #ip_header = pkt[14:34]
        #payload = pkt[14:]
        ## Extract the 20 bytes IP header, ignoring the IP options
        #fields = struct.unpack("!BBHHHBBHII", ip_header)
        #dummy_hdrlen = fields[0] & 0xf
        #iplen = fields[2]
        #ip_src = payload[12:16]
        #ip_dst = payload[16:20]
        #ip_frame = payload[0:iplen]

        #if pkt_type == socket.PACKET_OUTGOING:
        #    if self.on_ip_outgoing is not None:
        #        self.on_ip_outgoing(ip_src, ip_dst, ip_frame)
 
        #else:
        if self.on_ip_incoming is not None:
            process_sts,scapy_pkt = process_scapy_pkt(pkt)
            if process_sts is True:
                self.on_ip_incoming(scapy_pkt)
 
    def recv(self):
        while True:
 
            pkt, sa_ll = self.ins.recvfrom(MTU)
 
            #if type == socket.PACKET_OUTGOING and self.on_ip_outgoing is None:
            #    continue
            #elif self.on_ip_outgoing is None:
            #    continue
            
            if len(pkt) <= 0:
                break

            self.__process_ipframe(sa_ll[2], pkt )
            #process_scapy_pkt(pkt)

#Example code to use IPSniff
def test_incoming_callback(scapy_pkt):
    pass
    #print("incoming - src=%s, dst=%s, frame len = %d"
    #    %(socket.inet_ntoa(src), socket.inet_ntoa(dst), len(frame)))
    return

def test_outgoing_callback(scapy_pkt):
    pass
    #print("outgoing - src=%s, dst=%s, frame len = %d"
    #    %(socket.inet_ntoa(src), socket.inet_ntoa(dst), len(frame)))
    return

def setup_l2sniff(interface_name, callback_func):
    ip_sniff = IPSniff(interface_name, callback_func, None)
    ip_sniff.recv()
    return

if __name__ == '__main__':
    setup_l2sniff('h1-eth0', test_incoming_callback)
    #ip_sniff = IPSniff('eth0', test_incoming_callback, test_outgoing_callback)
    #ip_sniff = IPSniff('eth0', test_incoming_callback, test_outgoing_callback)
    #ip_sniff.recv()
    #return
