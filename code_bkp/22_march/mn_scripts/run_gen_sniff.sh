#!/bin/bash
# program arguments: <svcchain, SRC_HOST_ID, DST_HOST_ID, NUM_OF_PKTS, FLOW_ID>
#array=( $@ )
#len=${#array[@]}
echo " Arguments are:" $array, $len
echo "Args: $@"
echo "Number_of_args: $#"
echo " Argments are : $1, $2, $3, $4, $5"

svc_chain="1101,1111,1011"
src_id=6
dst_id=5
num_of_pkts=1
flow_id=1
interval=0.5

if [ $# -ge 1 ];
then
  svc_chain=$1
  echo "updated service chain: $svc_chain"
fi

if [ $# -ge 2 ];
then
  src_id=$2
  echo "updated source id: $src_id"
fi


if [ $# -ge 3 ];
then
  dst_id=$3
  echo "updated destination id: $dst_id"
fi

if [ $# -ge 4 ];
then
  num_of_pkts=$4
  echo "updated packets count: $num_of_pkts"
fi


if [ $# -ge 5 ];
then
  flow_id=$5
  echo "updated flow_id: $flow_id"
fi


if [ $# -ge 6 ];
then
  interval=$6
  echo "updated interval: $interval"
fi

#for i in "$@"; do
#    echo $i

sudo python gn_sniff_sk.py $svc_chain $src_id $dst_id $num_of_pkts $flow_id $interval
#sudo python generator_sk.py $svc_chain $src_id $dst_id $num_of_pkts $flow_id $interval
#sudo python generator_sk.py 1101,1111,1011 6 1 5 0 0.5
