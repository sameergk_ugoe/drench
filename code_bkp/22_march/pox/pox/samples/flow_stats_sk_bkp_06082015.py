#!/usr/bin/python
# Copyright 2012 William Yu
# wyu@ateneo.edu
#
# This file is part of POX.
#
# POX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# POX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with POX. If not, see <http://www.gnu.org/licenses/>.
#

"""
This is a demonstration file created to show how to obtain flow 
and port statistics from OpenFlow 1.0-enabled switches. The flow
statistics handler contains a summary of web-only traffic.
"""

# standard includes
from pox.core import core
from pox.lib.util import dpid_to_str
import pox.openflow.libopenflow_01 as of
# include as part of the betta branch
from pox.openflow.of_json import *
import time
log = core.getLogger()
import csv
import os.path
# To access and update the shadow price for service dictionary entries
import pox.openflow.discovery_sk as disc
# To access the Switch and setup the flows proactively
import pox.forwarding.l2_multi_sk as l2m
#import core.openflow_discovery as Discovery
from sets import Set
vlan_set = Set([])
THRESHOLD_PACKETS = 50 #100 #50
stats_interval = 5 #5 #10 #1 
log_file = 'logsp.csv'

switch_flow_info = {}   #{'dpid1':[[svc1,port1, packets],[svc1,port2, packets], [svc2, port1, packets]] 'dpid2':[[...]]}
#Ideal Table {'dpid1':[[svc1,port1,packets, time_stamp],[svc1,port2, packets, time_stamp], [svc2, port1, packets,time_stamp]] 'dpid2':[[...]]}
#Current {'dpid1':[[svc1,packets, time_stamp],[svc2, packets, time_stamp]] 'dpid2':[[...]]}

last_logged_time = 0
base_time = 0
def log_sp_and_flow_data(vlan_id=0,out_port=0):
  global last_logged_time
  global stats_interval
  global base_time
  
  cur_time = time.time()
  if ((cur_time - last_logged_time) < stats_interval):
    return
  last_logged_time = cur_time

  if base_time == 0:
    base_time = cur_time
  time_diff = int(cur_time - base_time)
  
  dict = disc.get_svc_dict()
  #log.warning("At[%s]: Dictionary contents:%s", cur_time, dict)
  log_file_func = log_file.split('.')[0] + '_' + str(vlan_id) +'.'+log_file.split('.')[1]
  with open(log_file_func, 'ab') as csvfile:
    logWriter = csv.writer(csvfile,delimiter=',')
    for key,value in dict.items():
      if key == vlan_id:
        log.warning("At[%s]: Dictionary for Key[%d] is:%s", cur_time, key, value)
        for x in range(len(value)):
          sp,mac_id,ip,sw_id = value[x]
          logWriter.writerows([[str(time_diff), str(key), str(sw_id), str(mac_id), str(sp), str(out_port)]])

def add_flows_proactively_on_switch(dpid=0, vlan_id=0):
  return True

def clear_flows_on_switch(dpid=0, vlan_id=0):
  log.warning("Func[%d] Triggered Clear Flows on switch [%s]", vlan_id,(dpid_to_str(dpid)))
  
  msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE_STRICT)
  #msg = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE)
  msg.match.dl_vlan = vlan_id
  msg.match.in_port = None #in_port #event.port #None
  msg.match.nw_src = None
  msg.match.nw_dst = None
  msg.match.dl_src = None
  msg.match.dl_dst = None
  
  #msg = of.ofp_flow_mod(command=of.OFPFC_DELETE)
  #core.openflow.sendToDPID(dpid, msg)
  for switch in core.openflow._connections.values():

    # Base method II: clear flows on all the switches < to ensure all get to see the new path > : This also is seen results in loops on some occasions (in flight packets fwd'ed with differernt rules at different switches.)
    #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
    
    # Initial Method I: Clear flows on only the matching switches: This results into Loops
    #switch.send(msg)
    #if switch.dpid == dpid:
      #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d] by msg=%s", switch.dpid,vlan_id, msg)
      #log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
      #switch.send(msg)
    
    # Method III: (Can it result in loops? looks likely... )
    # Send clear on only those switches that have the flow defined for the vlan_id
    if ( (switch_flow_info.has_key(switch.dpid)) and (True == (any(vlan_id== x[0] for x in switch_flow_info[switch.dpid]))) ):
      log.info("Clearing Flows on Switch[%d] for VLAN_ID[%d]", switch.dpid,vlan_id)
      switch.send(msg)
      # Also try instantaneous proactive flow setup
      #add_flows_proactively_on_switch(dpid=switch.dpid, vlan_id=vlan_id)
    else:
      # Ignore this switch
      pass
  return True

def check_and_update_switch_flow_table(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  if (False == switch_flow_info.has_key(sw_id)):
    switch_flow_info[sw_id] = [[vlan_id,0,time.time()]]
  else:
    if (False == (any(vlan_id== x[0] for x in switch_flow_info[sw_id]))):
      switch_flow_info[sw_id].append([vlan_id,0,time.time()])
    pass
  return
  
def handle_flow_stats_1(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  check_and_update_switch_flow_table(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  
  for x in range(0,len(switch_flow_info[sw_id])):
    old_vlan,old_pkts,o_time = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2]
    if(vlan_id == old_vlan):
      switch_flow_info[sw_id][x] = [vlan_id,num_pkts,time.time()]
      if (num_pkts == old_pkts):
        # Reduce the Shadow Price (no flows observed!)
        clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price= -10)
        #clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price= 0)
        if(clear_flag is True):
          clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
          switch_flow_info[sw_id][x] = [vlan_id,0,time.time()]
          #pass
      elif (num_pkts > old_pkts and (num_pkts-old_pkts) > THRESHOLD_PACKETS):
        clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price= 10)
        if(clear_flag is True):
          clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
          switch_flow_info[sw_id][x] = [vlan_id,0,time.time()]
      else:
        # Save the record and Print the number of Flows, if 0, no action!!
        #switch_flow_info[sw_id][x] = [vlan_id,num_pkts,time.time()]
        pass
      break
    else:
      continue
  log_sp_and_flow_data(vlan_id=vlan_id,out_port=port_id)
  return
  
def handle_flow_stats_2(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  check_and_update_switch_flow_table(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  
  for x in range(0,len(switch_flow_info[sw_id])):
    old_vlan,old_pkts,o_time = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2]
    if(vlan_id == old_vlan):
      switch_flow_info[sw_id][x] = [vlan_id,num_pkts,time.time()]
      break

  clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price=0)
  if(clear_flag is True):
    clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
    switch_flow_info[sw_id][x] = [vlan_id,0,time.time()]

  log_sp_and_flow_data(vlan_id=vlan_id,out_port=port_id)
  return

last_aged_time = 0
def check_for_aged_flows(vlan_id=0):
  global switch_flow_info
  global last_aged_time
  cur_time = time.time()
  if last_aged_time == 0:
    last_aged_time = cur_time
  if ((cur_time - last_aged_time) < stats_interval):
    return
  last_aged_time = cur_time
  for sw_id,value in switch_flow_info.items():
    for x in range(len(value)):
      ovlan,opkts,otime = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2]
      if(vlan_id == ovlan and (cur_time - otime) > stats_interval):
        log.info("At:%s Aging values for switch[%d], function[%d]", last_aged_time,sw_id,vlan_id)
        clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price=0)
        if(clear_flag is True):
          clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
          switch_flow_info[sw_id][x] = [vlan_id,0,time.time()]

  return

def check_for_all_aged_flows():
  global vlan_set
  for vlan_id in vlan_set:
    check_for_aged_flows(vlan_id=vlan_id)
  vlan_set.clear()
  
def process_flow_ststs_info(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  processed = False
  
  for x in range(0,len(switch_flow_info[sw_id])):  
    old_vlan,old_pkts = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1]
    
    if (num_pkts == old_pkts and port_id == 1):
      #Reduce the Shadow Price (no flows observed (but only on port1)!)
      processed = True
      clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price= -10)
      if(clear_flag is True):
        clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
    
    elif (num_pkts > old_pkts and port_id == 1):
      if ((num_pkts-old_pkts) > THRESHOLD_PACKETS and port_id == 1):
        processed = True
        #Check if needs to update the Shadow Price
        clear_flag = disc.update_shadow_price(dpid=sw_id,svc_id=vlan_id,shadow_price= 10)
        if(clear_flag is True):
          clear_flows_on_switch(dpid=sw_id,vlan_id=vlan_id)
      else:
        switch_flow_info[sw_id][x][2] = num_pkts
    
    else:
      # Print the number of Flows, if 0, no action!!
      switch_flow_info[sw_id][x][2] = num_pkts
      pass
  if processed is False:
  # Apply Ageing rule for other switches servicing the same function:: if the port_id 1 is present in the list
    pass
  
def handle_flow_stats2(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  if (switch_flow_info.has_key(sw_id)):
    pass
  else:
    switch_flow_info[sw_id] = [[vlan_id,0]]
  
  process_flow_ststs_info(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  log_sp_and_flow_data(vlan_id=vlan_id)
  
  return

def handle_flow_stats_x(sw_id=0,port_id=0,vlan_id=0,num_pkts=0):
  global switch_flow_info
  if (switch_flow_info.has_key(sw_id)):
    Found = False
    x = 0
    for x in range(0,len(switch_flow_info[sw_id])):
      old_port_id,old_vlan,old_pkts = switch_flow_info[sw_id][x][0],switch_flow_info[sw_id][x][1],switch_flow_info[sw_id][x][2]
      if(port_id == old_port_id):
        Found = True
        break
    if Found is False:
      switch_flow_info[sw_id].append([port_id,vlan_id,num_pkts])
  else:
    switch_flow_info[sw_id] = [[port_id,vlan_id,num_pkts]]
  
  process_flow_ststs_info(sw_id=sw_id,port_id=port_id,vlan_id=vlan_id,num_pkts=num_pkts)
  log_sp_and_flow_data(vlan_id=vlan_id)
  
  return

# handler for timer function that sends the requests to all the
# switches connected to the controller.
def _timer_func ():
  for connection in core.openflow._connections.values():
    connection.send(of.ofp_stats_request(body=of.ofp_flow_stats_request()))
    #connection.send(of.ofp_stats_request(body=of.ofp_port_stats_request()))
  log.debug("Sent %i flow/port stats request(s)", len(core.openflow._connections))
  
  #sw = l2m.get_sw_from_dpid(1)
  #log.info ("Retrieved switch info for:%s, %d", sw.__repr__(),sw.is_holding_down)

# handler to display flow statistics received in JSON format
# structure of event.stats is defined by ofp_flow_stats()
def _handle_flowstats_received (event):
  stats = flow_stats_to_list(event.stats)
  #log.info("FlowStatsReceived from %s: %s", #event, event.stats)
  #  dpid_to_str(event.connection.dpid), stats)
  
  #val = disc.update_shadow_price(dpid=1,svc_id=13,shadow_price=10)
  #log.info("updated: FlowStatsReceived from [%d:%s]: %s", event.connection.dpid, #event, event.stats)
    #dpid_to_str(event.connection.dpid), stats)
  
  # Get number of bytes/packets in flows for matching vlan_id
  num_bytes = 0
  num_flows = 0
  num_packet = 0
  #global vlan_set
  for f in event.stats:
    #log.info (" F in event.stats is: %s", f)
    #if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535 and f.actions[0].port == 1:
    if f.match.dl_vlan and f.match.dl_vlan !=0 and f.match.dl_vlan != 65535:
      log.info("[%d:%s] vlan_id:[%d]:: Bytes:%s, (%s packets)",event.connection.dpid,dpid_to_str(event.connection.dpid),f.match.dl_vlan, f.byte_count, f.packet_count)
      #vlan_set.add(f.match.dl_vlan)
      #if f.actions[0].port == 1:
      if True == core.openflow_discovery.is_edge_port(event.connection.dpid,f.actions[0].port):
        handle_flow_stats_1(sw_id=event.connection.dpid, port_id=f.actions[0].port,vlan_id=f.match.dl_vlan,num_pkts=f.packet_count)
      else:
        #vlan_set.add(f.match.dl_vlan)
        handle_flow_stats_2(sw_id=event.connection.dpid, port_id=f.actions[0].port,vlan_id=f.match.dl_vlan,num_pkts=f.packet_count)
      #log.info("FlowStatsReceived from %s: %s", dpid_to_str(event.connection.dpid), stats)
      #pass
    else:
      # Other flows: (ignore)
      pass
  
  #for vlan_id in vlan_set:
  #  check_for_aged_flows(vlan_id=vlan_id)
  #check_for_all_aged_flows()
  
# handler to display port statistics received in JSON format
def _handle_portstats_received (event):
  stats = flow_stats_to_list(event.stats)
  log.info("PortStatsReceived from %s: %s", #event, stats)
    dpid_to_str(event.connection.dpid), stats)
    
# main functiont to launch the module
def launch ():
  from pox.lib.recoco import Timer

  # attach handsers to listners
  core.openflow.addListenerByName("FlowStatsReceived", 
    _handle_flowstats_received) 
  #core.openflow.addListenerByName("PortStatsReceived", 
  #  _handle_portstats_received) 

  # timer set to execute every five seconds
  Timer(stats_interval, _timer_func, recurring=True)
  