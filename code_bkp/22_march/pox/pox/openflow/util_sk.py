# Copyright 2011-2013 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is loosely based on the discovery component in NOX.

"""
This module sets up the Drench Utils.
"""

from pox.lib.revent import *
from pox.lib.recoco import Timer
from pox.lib.util import dpid_to_str, str_to_bool
from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt
from pox.lib.addresses import EthAddr, IPAddr
import csv
import os.path
import math
import struct
import time
from collections import namedtuple, OrderedDict
from random import shuffle, random, randint
from os.path import expanduser
hdir_p = expanduser('~') + '/drench'
rdir_p = hdir_p + '/results'
log = core.getLogger()
rsc_file = "list3.csv"
svc_ch_file = hdir_p + '/svc_chains/' + 'svc_chain_dict_3_6.csv'
####################################


#import pox.misc.svcgen_sk as SvcGen
import pox.misc.SvcChainGenerator as SvcGen
def load_svc_chain_info(svc_chain_file=svc_ch_file):
  SvcGen.read_to_dict_from_csv(svc_chain_file)
  log.info ("Loaded Service Chain with length [%d]!", get_svc_chain_len_for_key(8))

def get_svc_chain_for_key(key):
  svc = SvcGen.get_svc_chain_for_tos(ip_tos=key)
  if svc is None:
    log.error("get_svc_chain_for_key():: SVC for key[%d] is not Available!", key)
    svc = []
  log.info("get_svc_chain_for_key():: for key[%d] chain=[%s]!", key, svc)
  return svc

def get_svc_chain_len_for_key(key):
  svc = SvcGen.get_svc_chian_len_for_tos(ip_tos=key)
  if svc is None:
    log.error("get_svc_chain_len_for_key():: SVC for key[%d] is not Available!", key)
    svc = -1
  log.info("get_svc_chain_len_for_key():: for key[%d] len=[%d]!", key, svc)
  return svc
  
def get_svc_for_key_at_index(key,index):
  svc = SvcGen.get_svc_at_index_for_tos(ip_tos=key, index=index)
  if svc is None:
    log.error("SVC for key[%d], index[%d] is not Available!", key, index)
    svc = -1
  log.info("get_svc_for_key_at_index():: for key[%d] at index [%d] SVC=[%d]!", key, index, svc)
  return svc

#Helper functions for Path Settings
def get_base_dir():
    return hdir_p
def get_results_dir():
    return rdir_p
def get_rsrc_file():
    return rsc_file


#Helper Functions for IP/MAC/(Swid,port) map
ip_mac_sw_obj_list = []
class ip_mac_switch_map():
    #_list_count = 0
    def __init__ (self,ip_addr=None, mac_addr=None, sw_id=None, port_id=None):
        self.set_map_entry(ip_addr, mac_addr, sw_id, port_id)
        #_list_count+=1
    def set_map_entry(self,ip_addr, mac_addr, sw_id, port_id):
        self.ip_addr = ip_addr
        self.mac_addr = mac_addr
        self.sw_id = sw_id
        self.port_id = port_id
        #log.warning("Setup entry[%s:%s:%s:%s]", ip_addr, mac_addr,sw_id,port_id)
    def get_ip_from_mac(self, mac_addr):
        if self.mac_addr == mac_addr: return self.ip_addr
        return None
    def get_mac_from_ip(self, ip_addr):
        if self.ip_addr == ip_addr: return self.mac_addr
        return None
    def get_ip_from_sw_id_and_port_id(self, sw_id,port_id):
        if self.sw_id == sw_id and self.port_id == port_id: return self.ip_addr
        return None
    def get_mac_from_sw_id_and_port_id(self, sw_id,port_id):
        if self.sw_id == sw_id and self.port_id == port_id: return self.mac_addr
        return None
def validate_for_non_duplicate_entry(ip_addr,mac_addr, sw_id, port_id):
    non_duplicate = True
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.mac_addr == mac_addr or map_obj.ip_addr == ip_addr or (map_obj.sw_id == sw_id and map_obj.port_id == port_id):
            log.warning("Existing Entry:(%s,%s,(%s:%d)), New Request(%s:%s,(%s:%d))",map_obj.mac_addr,map_obj.ip_addr,map_obj.sw_id,map_obj.port_id,mac_addr,ip_addr,sw_id,port_id)
            map_obj.set_map_entry(ip_addr,mac_addr, sw_id, port_id)
            return False
    return non_duplicate
def add_to_address_map_table(ip_addr,mac_addr, sw_id, port_id):
    global ip_mac_sw_obj_list
    if len(ip_mac_sw_obj_list):
        if validate_for_non_duplicate_entry(ip_addr,mac_addr, sw_id, port_id) is False: return None
    return ip_mac_sw_obj_list.append(ip_mac_switch_map(ip_addr,mac_addr, sw_id, port_id))
def get_addr_map_from_mac_addr(mac_addr):
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.mac_addr == mac_addr:
            return(map_obj.ip_addr,map_obj.mac_addr,map_obj.sw_id,map_obj.port_id)
    return (None,None,None,None)
def get_addr_map_from_ip_addr(ip_addr):
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.ip_addr == ip_addr:
            return(map_obj.ip_addr,map_obj.mac_addr,map_obj.sw_id,map_obj.port_id)
    return (None,None,None,None)
def get_addr_map_from_sw_id_and_port_id(sw_id,port_id):
    for map_obj in ip_mac_sw_obj_list:
        if map_obj.sw_id == sw_id and map_obj.port_id == port_id:
            return(map_obj.ip_addr,map_obj.mac_addr,map_obj.sw_id,map_obj.port_id)
    return (None,None,None,None)

class DrenchUtil (EventMixin):
  """
  Component provides basic drench utilities
  1. Sets up base paths and directories for execution environment
  2. Loads the Service chain information
  3. Loads the NFV resource file information.
  4. TODO: Update the NFV resource file information based on IP and translate to Service Dictionary
  5. TODO: client/server to pull/push information from/to host NFVs
  """
  
  _rsc_file = rsc_file     # Priority of LLDP-catching flow (if any)
  _base_dir = hdir_p        # How long until we consider a link dead
  _timeout_check_period = 5  # How often to check for timeouts
  _core_name = "drench_util" # we want to be core.openflow_discovery

  def __init__ (self):
    
    # Listen with a high priority (mostly so we get PacketIns early)
    #core.listen_to_dependencies(self,
    #    listen_args={'openflow':{'priority':0xffffffff}})
    log.warning("base_dir=[%s] and Rsrc File[%s]", self._base_dir, self._rsc_file)
    return

def launch (base_dir = None, rsrc_file = None, svc_chain_info_file = None):
  global hdir_p, rdir_p, rsc_file, svc_ch_file
  if base_dir is not None and os.path.isdir(base_dir):
    hdir_p = base_dir
    rdir_p = hdir_p + '/results'
    if not os.path.isdir(rdir_p):
      os.makedirs(rdir_p)
  else:
    log.error(" Directory [%s] not found!",base_dir)
  if rsrc_file is not None and os.path.isfile(rsrc_file) and len(rsrc_file) > 0: 
    rsc_file = rsrc_file
  else:
    log.error(" File [%s] not found!",rsrc_file)
  if svc_chain_info_file is not None and os.path.isfile(svc_chain_info_file):
    svc_ch_file = svc_chain_info_file
  else:
    log.info(" Invalid svc_chain_file [%s], using default [%s]!!", svc_chain_info_file, svc_ch_file)
  load_svc_chain_info(svc_chain_file=svc_ch_file)
  #Timer(update_interval, _svc_log_function, recurring=True)
  core.registerNew(DrenchUtil)
