# Copyright 2011-2013 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is loosely based on the discovery component in NOX.

"""
This module discovers the connectivity between OpenFlow switches by sending
out LLDP packets. To be notified of this information, listen to LinkEvents
on core.openflow_discovery.

It's possible that some of this should be abstracted out into a generic
Discovery module, or a Discovery superclass.
"""

from pox.lib.revent import *
from pox.lib.recoco import Timer
from pox.lib.util import dpid_to_str, str_to_bool
from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt
from pox.lib.addresses import EthAddr, IPAddr
import csv
import os.path
import math
import struct
import time
from collections import namedtuple, OrderedDict
from random import shuffle, random, randint
from os.path import expanduser
import pox.openflow.util_sk as util_sk
hdir_p = expanduser('~') + '/drench'
rdir_p = hdir_p + '/results/'
log = core.getLogger()
####################################
abc = 0
svc_dict = {}  #SK DictionaArray of {'Service name':[(sp1,mac1,ip1,originatorDPID1), (sp2,mac2,ip2,originatorDPID2)]}
svc_free_list = []
list_file_ln = 'list.csv'
list_file2_ln = 'list3.csv'
list_file_wn = 'list.csv'
list_file2_wn = 'list3.csv'
update_interval = 5
csv2_file = list_file2_ln
MIN_SHADOW_PRICE = 0
MAX_SHADOW_PRICE = 100
MIN_SP_THRESHOLD = 35   #52, for lb_argi #
MAX_SP_THRESHOLD = 70   #60, 50, #55, for lb_argi
MAX_SP_PEAK_THRESHOLD = 85
MIN_SP_PEAK_THRESHOLD = 15 #20 #30  #25  #20
MAX_SP_FOR_BRINGDOWN  = 501
#REMEMBER for reupdating dict in place mydict = { k : v for k,v in mydict.iteritems() if k != val }
default_svc_list = []
####################################
def get_svc_dict():
  global svc_dict
  return svc_dict

def get_sp_of_instance(svc, svc_mac_addr):
  if svc_mac_addr is None or svc ==0:
    return -1
  global svc_dict
  if (svc_dict.has_key(svc)):
    for x in range(0,len(svc_dict[svc])):
      isp,imac,iip = svc_dict[svc][x][0],svc_dict[svc][x][1],svc_dict[svc][x][2]
      if EthAddr(imac) == EthAddr(svc_mac_addr):
        return int(isp)
  return -1

def get_default_svc_list():
  global default_svc_list
  return default_svc_list

svc_flow_dict = {} # {(svc_id):[[sw_dpid,count]]}
def export_svc_dict_to_file():
    dict_file_name = rdir_p + 'inst_svc_info.csv'
    writer = csv.writer(open(dict_file_name, 'wb'))
    for key, value in svc_dict.items():
        writer.writerow([key, value])
    
    
    #mac_svc_dict = { v[1] : k for k,v in svc_dict.iteritems() if k != val }
    mac_svc_dict = {}
    for key,v in svc_dict.items():
        for value in v:
            if mac_svc_dict.has_key(value[1]): mac_svc_dict[value[1]].append(key)
            else: mac_svc_dict[value[1]] = [key]
    dict_file_name = rdir_p + 'mac_svc_info.csv'
    writer = csv.writer(open(dict_file_name, 'wb'))
    for key, value in mac_svc_dict.items():
        writer.writerow([key, value])
    
    ip_svc_dict = {}
    for key,v in svc_dict.items():
        for value in v:
            if ip_svc_dict.has_key(value[2]): ip_svc_dict[value[2]].append(key)
            else: ip_svc_dict[value[2]] = [key]
    dict_file_name = rdir_p + 'ip_svc_info.csv'
    writer = csv.writer(open(dict_file_name, 'wb'))
    for key, value in ip_svc_dict.items():
        writer.writerow([key, value])

    return
def get_svc_flow_dict():
  global svc_flow_dict
  return svc_flow_dict

def set_flow_count_info_for_svc_on_sw(sw_dpid = 0, svc_id = 0, flow_count=0):
  global svc_flow_dict
  found = False
  if svc_flow_dict.has_key(svc_id):
    for i, (sw,cnt) in enumerate(svc_flow_dict[svc_id]):
      if sw == sw_dpid:
        svc_flow_dict[svc_id][i] = [sw_dpid,flow_count]
        found = True
        break
    if found is False:
      svc_flow_dict[svc_id].append([sw_dpid,flow_count])
  else:
    svc_flow_dict[svc_id] = [[sw_dpid,flow_count]]
  keys_list = svc_flow_dict.keys()
  
  # Make sure the Switch with Highest Number of FLows is at the Top
  for key in keys_list:
    svc_flow_dict[key].sort(key = lambda x: x[1], reverse=True)

  return

def check_svc_at_sw_dpid(svc_id = 0, sw_id=0):
  """ This function assumes that svc_dict is updated and SW_DPID is correctly set"""
  global svc_dict
  if False == svc_dict.has_key(svc_id):
    log.warning(" svc_id(Wrong):%d, sw_id:%d", svc_id, sw_id)
    return False
  
  #return any(sw_id== x[3] for x in [svc_dict[svc_id]])
  for x in range(len(svc_dict[svc_id])):
    swid = svc_dict[svc_id][x][3]
    if sw_id == swid:
      return True
  
  return False
 
def setdpid_for_svc(svc_id=0,mac_addr=None,sw_id=0,port_id=0):
  global svc_dict, svc_free_list
  #func = int(bin(svc_id)[2:])
  found = False
  clear = False
  
  if mac_addr is None:
    log.warning(" For Mac_Addr(Wrong):%s, svc_id:%d, sw_id:%d", mac_addr, svc_id, sw_id)
    return False
  if False == svc_dict.has_key(svc_id):
    log.warning(" For Mac_Addr:%s, svc_id(Wrong):%d, value:%d", mac_addr, svc_id, sw_id)
    return False
  
  for x in range(len(svc_dict[svc_id])):
    #sp,mac_id,ip,swid = svc_dict[svc_id][x][0],svc_dict[svc_id][x][1],svc_dict[svc_id][x][2],svc_dict[svc_id][x][3]
    log.warning("TODO: setdpid_for_svc():: For Svc[%d] at MAC:[%s], updated the Switch DPID from [%d] to [%d]", svc_id, mac_addr,svc_dict[svc_id][x][3],sw_id)
    if(EthAddr(mac_addr) == EthAddr(svc_dict[svc_id][x][1]) and svc_dict[svc_id][x][3] != sw_id ):
      svc_dict[svc_id][x][3] = sw_id
      
    #Note: TODO: Have a separate function to update this detail (or atleast remove from inside if mac_update)
    addr_map = util_sk.get_addr_map_from_mac_addr(mac_addr)
    if addr_map and addr_map[0] and addr_map[0] != IPAddr(svc_dict[svc_id][x][2]):
      svc_dict[svc_id][x][2] = IPAddr(addr_map[0]).toStr()
      log.warning("DONE: setdpid_for_svc():: For Svc[%d] at MAC:[%s], updated the Switch DPID to [%d] and IpAddr to [%s]", svc_id, mac_addr,svc_dict[svc_id][x][3], svc_dict[svc_id][x][2])  
        
      #log.warning("DONE: setdpid_for_svc():: For Svc[%d] at MAC:[%s], updated the Switch DPID from [%d] to [%d]", svc_id, mac_addr,svc_dict[svc_id][x][3],sw_id)  
  
  if svc_id == 0:
    for x in range(len(svc_free_list)):
      if(EthAddr(mac_addr) == EthAddr(svc_free_list[x][1]) and svc_free_list[x][3] != sw_id ):
        svc_free_list[x][3] = sw_id
        
      #Note: TODO: Have a separate function to update this detail (or atleast remove from inside if mac_update)
      addr_map = util_sk.get_addr_map_from_mac_addr(mac_addr) 
      if addr_map and addr_map[0] and addr_map[0] != IPAddr(svc_free_list[2]):
          svc_free_list[2] = IPAddr(addr_map[0]).toStr()

  return

def rem_svc_for_switch(sw = None, dpid = 0):
  global svc_dict
  return
  rem_val_index = []
  log.debug("\n DPID: %d triggered Current: svc_dict: %s", dpid, svc_dict)
  for key,value in svc_dict.items():
    for x in range(len(value)):
      sp,mac_id,ip,sw_id = value[x]
      if(sw_id == dpid):
        rem_val_index.append((key,x))
  log.debug("\n DPID: %d triggered changes: %d:%s", dpid, len(rem_val_index),rem_val_index)

  for x in range(len(rem_val_index)):
    key, index = rem_val_index[x]
    if len(svc_dict[key]) > index:
      svc_dict[key].pop(index)
    if(len(svc_dict[key]) == 0):
      svc_dict.pop(key)
      
  export_svc_dict_to_file()
  
  log.debug("\n DPID: %d triggered Updated: svc_dict: %s", dpid, svc_dict)

# mode = 1 => remove matching entries 0 => remove non matching entries
def update_dict_as_in_file(in_file=list_file2_ln, mode = 0):
  if ( False == os.path.isfile(in_file)):
    return -1

  def get_key(item):
    return item[1]

  global svc_dict
  rem_val_index = []
  with open(in_file, 'rb') as csvfile:
    for key,value in svc_dict.items():
      for x in range(len(value)):
        s_sp,s_mac_id,s_ip,s_sw_id = value[x]
        csvfile.seek(0,0)
        reader = csv.DictReader(csvfile)
        found = False
        for row in reader:
          #print row
          sw_num = int(row['switch'])
          svc = int(row['function'], 2)
          mac = row['mac']
          ip = row['ip']
          sp = int(row['sp'])
          if key == svc and s_sw_id == sw_num and s_mac_id==mac:
            found = True
            break
        if found is False:
          if mode == 0:
            rem_val_index.append((key,x))
        else:
          #print "Key %d and value_list[%s] found!" %(key,value[x])
          if mode == 1:
            rem_val_index.append((key,x))

  if rem_val_index:
    #print "To Remove:"
    rem_val_index.sort(reverse=2,key=get_key)
    #print rem_val_index
    for x in range(len(rem_val_index)):
      key, index = rem_val_index[x]
      svc_dict[key].pop(index)
      #if(len(svc_dict[key]) == 0):
      #    svc_dict.pop(key)
  return 0

def get_availbale_mac_list_for_instantiation(in_file=csv2_file):
  if ( False == os.path.isfile(in_file)):
    return -1
    
  with open(in_file, 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    available_list = []
    for row in reader:
      sw_num = int(row['switch'])
      svc = int(row['function'], 2)
      mac = row['mac']
      ip = row['ip']
      sp = int(row['sp'])
      if svc == 0:
        available_list.append([sp,mac,ip,sw_num])
    return available_list

sp_to_flowcount_gradation_dict = {70:5, 75:4, 80:3, 85:2, 90:1, 95:0}#{50:15, 55:12, 60:10, 65:7, 70:5, 75:4, 80:3, 85:2, 90:0}
def get_min_flow_count_for_sp(cur_max_sp):
  global sp_to_flowcount_gradation_dict
  min_flow_count = -1
  keys_list = sorted(sp_to_flowcount_gradation_dict.keys())
  if cur_max_sp < keys_list[0]:
    log.warning ("Current MAX SP:%d is less than min of Gradation %d", cur_max_sp,keys_list[0])
    return -1
  
  for key in reversed(keys_list):
    if cur_max_sp >= key:
      min_flow_count = sp_to_flowcount_gradation_dict[key]
      break
  return min_flow_count
  
def get_correspoding_sp_for_flow_count(flow_count):
  global sp_to_flowcount_gradation_dict
  min_sp = MIN_SHADOW_PRICE
  flows_list = sp_to_flowcount_gradation_dict.values()
  keys_list = sp_to_flowcount_gradation_dict.keys()
  if flow_count > flows_list[0]:
    return keys_list[0]

  for key,value in sp_to_flowcount_gradation_dict.items():
    if flow_count >= value:
      return key
  return min_sp
  
def check_sp_to_flow_count_for_instantiation(cur_max_sp, candidate_sw_list=None):
  
  if not candidate_sw_list:
    log.warning(" Empty Candidate_SW_LIST!! ")
    return False,None
  
  min_flow_count = 0
  global sp_to_flowcount_gradation_dict
  min_flow_count = get_min_flow_count_for_sp(cur_max_sp=cur_max_sp)
  
  if min_flow_count < 0 or candidate_sw_list[0][1] < min_flow_count:
    log.warning ("min_flow_count:%d is more than Flows at this Gradation %d", cur_max_sp,candidate_sw_list[0][1])
    return False,None
  
  sel_sw_list = []
  for i, x in enumerate(candidate_sw_list):
    #(sw_id, count) = x[0], x[1]
    if x[1] >=min_flow_count:
      log.warning(" For Switch [%d] with flow count [%d] is greater than min_sel_flow_count[%d] at cur_sp[%d]",x[0], x[1],min_flow_count,cur_max_sp )
      sel_sw_list.append(x)
  log.info("For current_max_sp=%d, Selection Switch List is: %s", cur_max_sp, sel_sw_list)
  return True,sel_sw_list
  
MAX_INSTANCES_FOR_NEW_SERVICE = 1
MAX_INSTANCES_FOR_CUR_SERVICE = 2
def get_advertisable_SP_for_instance(svc_id=0, cur_max_sp=0, sw_flow_count=0,instnace=None):
  global svc_dict
  n_max_adv_sp_value = MIN_SHADOW_PRICE
  if svc_id == 0:
    return MIN_SHADOW_PRICE
  if not svc_dict.has_key(svc_id):
      return MIN_SHADOW_PRICE
  else:
    if len(svc_dict[svc_id]) >1: 
      n_min_inst =  min(svc_dict[svc_id], key=lambda x:x[0])
      if n_min_inst: 
        n_min_inst_sp = int(n_min_inst[0])
      else: 
        n_min_inst_sp = MIN_SP_THRESHOLD + sw_flow_count
    else: 
      n_min_inst_sp = MIN_SP_PEAK_THRESHOLD
    
    #n_max_adv_sp_value = get_correspoding_sp_for_flow_count(sw_flow_count)
    #n_max_adv_sp_value = (max(cur_max_sp,n_max_adv_sp_value) + n_min_inst_sp)/2
    n_max_adv_sp_value = min(MIN_SP_PEAK_THRESHOLD,max(n_min_inst_sp, abs(int(cur_max_sp*0.5)-MIN_SP_PEAK_THRESHOLD))) 
  log.warning("Advertising the SP value for the svc[%d] new instance sp as:%d: %s", svc_id, n_max_adv_sp_value,svc_dict[svc_id] )
  return n_max_adv_sp_value
  
def get_optimal_svc_location_for_instantiation_from_available_list(cur_max_sp, available_list=None, sw_path=None, svc_id=0, new_service=False):
  """ To DO: """
  if not available_list:
    return None
  # Precedence: Option 1 Get the Switch in the path of the request: to reduce the hop count., else get any free one
  
  # alternate: Get the switch that is seen to be servicing more flows for the requested service type:
  candidate_sw_list = []
  global svc_flow_dict
  if svc_id != 0:
    if svc_flow_dict.has_key(svc_id):
      candidate_sw_list = svc_flow_dict[svc_id]

  if new_service==True:
    return available_list[0:min(len(available_list),MAX_INSTANCES_FOR_NEW_SERVICE)]
    
  inst_flag, inst_list = check_sp_to_flow_count_for_instantiation(cur_max_sp=cur_max_sp, candidate_sw_list=candidate_sw_list)
  if False ==inst_flag or inst_list is None:
    return None
  
  instance_list = []
  if inst_list:
    for i, (sw_id, count) in enumerate(inst_list):
      for index,instance in enumerate(available_list):
        if sw_id == instance[3] and instance not in instance_list: #[int(sp),mac,ip,sw_num]
          # Adversise SP based on cur_max_sp and flow count, more count => More SP, less count => less SP
          adv_sp = get_advertisable_SP_for_instance(svc_id=svc_id, cur_max_sp=cur_max_sp, sw_flow_count=count,instnace=instance)
          if adv_sp: instance[0] = adv_sp
          instance_list.append(instance)
  if instance_list:
    log.warning("For svc[%d] Picked the following instances: %s",svc_id, instance_list )
    temp_max = randint(1,MAX_INSTANCES_FOR_CUR_SERVICE)
    return instance_list[0:min(len(instance_list),MAX_INSTANCES_FOR_CUR_SERVICE)]
    #return instance_list[0:min(len(instance_list),MAX_INSTANCES_FOR_CUR_SERVICE)]

  
  # case where there is no suitable instance but SP is very high with some instance available
  if not instance_list and get_min_flow_count_for_sp(cur_max_sp) == 0:
    return available_list[0:min(len(available_list),MAX_INSTANCES_FOR_CUR_SERVICE)]
    
  return None
  
  #Get the SW from this Candidate list
  if candidate_sw_list:
    for i, (sw_id, count) in enumerate(candidate_sw_list):
      for index,instance in enumerate(available_list):
        if sw_id == instance[3]: #[int(sp),mac,ip,sw_num]
          return instance
  
  
  # Option 1: Get the Switch in the Requested Path
  if sw_path and available_list:
    # Priority: Find node in available_list that is in the sw_path <reverse order search => preference to the one closer to destination.
    for sw_id in reversed(sw_path):
      for index,instance in enumerate(available_list):
        if sw_id == instance[3]: #[int(sp),mac,ip,sw_num]
          return instance

  if available_list:
    return available_list[0]
  
  return None


svc_instantiation_log = {} #{key: time, instance_mac_id}
svc_instantiation_list = [] #[(svc_id,[svc_instance], time.time())]
THRESHOLD_TIME_FOR_NEW_INSTANTIATION = 2.5
INITIAL_SP_ON_INSTANTIATION = 25
def instantiate_new_instance_for_svc(svc_id = 0, cur_max_sp= 0,sw_path_list=None, new_service=False, prechosen_instance=None):
  """Currently tries to instantiate only  1 instance for the svc_id: 
     In order to Mimic distributed approach, need to instantiate on all potential candidates at once """
  global svc_dict
  global svc_instantiation_list
  global svc_free_list

  
  #log.error("instantiate_new_instance_for_svc: svc_id:%d, cur_max_sp=%d", svc_id,cur_max_sp)
  
  global svc_instantiation_log
  if svc_instantiation_log.has_key(svc_id):
    last_instantiated_time = svc_instantiation_log[svc_id]
    now = time.time()
    if now < last_instantiated_time + THRESHOLD_TIME_FOR_NEW_INSTANTIATION:
      log.warning("Ignore new Instantiation for Svc[%d] at time[%s]", svc_id,now)
      return False

  a_list = None
  if svc_dict.has_key(0):
    a_list = svc_dict[0]
  if a_list is None:
    log.warning("No Instance Available to instantiate the new service[%d], [%s]", svc_id, svc_dict)
    return -1 
  
  if prechosen_instance is not None:
    svc_loc_info_list = [prechosen_instance]
  else:
    svc_loc_info_list = get_optimal_svc_location_for_instantiation_from_available_list(cur_max_sp=cur_max_sp,available_list=a_list,sw_path=sw_path_list, svc_id=svc_id, new_service=new_service)
    if svc_loc_info_list is None and len(a_list):
      log.warning("Failed to find optimal Instance from Available list[%s] to instantiate the new service[%d]", a_list, svc_id)
      return False 
    elif len(a_list) == 0 or len(a_list[0]) == 0:
      log.warning("Empty Available list[%s] to instantiate the new service[%d]", a_list, svc_id)
      return -1 
    
  
  log.warning("Trying to instantiate [%d] instances for Service[%d], svc_loc_info_list[%s]", len(svc_loc_info_list),svc_id, svc_loc_info_list)
  for svc_loc_info in svc_loc_info_list:
    svc_instantiation_list.append((svc_id, svc_loc_info, time.time()))
    if svc_dict.has_key(svc_id):
      svc_dict[svc_id].append(svc_loc_info) #svc_dict[svc_id].append([sp,mac,ip,originatorDPID])
      log.warning ('New Instance [%s] instantiated for existing Service:[%d]',svc_loc_info, svc_id)
    else:
      svc_dict[svc_id] = [svc_loc_info]#[[sp,mac,ip,originatorDPID]]
      log.warning ('New Instance [%s] instantiated for New Service:[%d]',svc_loc_info, svc_id)

    if svc_dict.has_key(0):
      if svc_loc_info in svc_dict[0]:
        val = svc_dict[0].pop(svc_dict[0].index(svc_loc_info))
        log.warning("Popped from svc_dict[0]the Item[%s] for key[%d] ", val, svc_id)
        #svc_dict[0].pop(svc_loc_info) # fails needs index of svc_loc_info
      if svc_loc_info in svc_free_list:
        svc_free_list.pop(svc_free_list.index(svc_loc_info))
      svc_instantiation_log[svc_id] = time.time()
  export_svc_dict_to_file()
  return True


svc_de_instantiation_log = {}
THRESHOLD_TIME_FOR_NEW_DEINSTANTIATION = 2.5
MAX_DEINIT_REQUEST_COUNT = 3
def bringdown_service_instace_for_svc(svc_id = 0, svc_mac_addr = None,sw_path_list=None, skip_wait_period=False):
  global svc_free_list
  if svc_mac_addr is None:
    log.warning("bringdown_service_instace_for_svc[%d:%s] is invalid!", svc_id,svc_mac_addr)
    return False
  
  global svc_de_instantiation_log
  key = (svc_id,svc_mac_addr)
  now = time.time()
  if skip_wait_period is False:
    if svc_de_instantiation_log.has_key(key):
      last_de_instantiated_time,count = svc_de_instantiation_log[key]
      if now > last_de_instantiated_time + THRESHOLD_TIME_FOR_NEW_DEINSTANTIATION*MAX_DEINIT_REQUEST_COUNT:
        svc_de_instantiation_log[key] = [now, 1]
        log.info("bringdown_service_instace_for_svc():DeInstatiate request after long delay. Rebase the TimeStamp to current Time and reset count to 1")
        return False
      #if now < last_de_instantiated_time + THRESHOLD_TIME_FOR_NEW_DEINSTANTIATION and count < MAX_DEINIT_REQUEST_COUNT:
      #  svc_de_instantiation_log[key] = [now, count+1]
      #  log.info("bringdown_service_instace_for_svc():Subsequent DeInstatiate request for Key[%s]. count[%d]", key, count+1)
      #  return False
      if now < last_de_instantiated_time + THRESHOLD_TIME_FOR_NEW_DEINSTANTIATION: # and count == MAX_DEINIT_REQUEST_COUNT:
        log.warning("bringdown_service_instace_for_svc():De Instantiating Instance[%s]!!", key)
    else:
      # Add the request TimeStamp in the De-Instatioaton Log
      svc_de_instantiation_log[key] = [now,1]
      log.warning("bringdown_service_instace_for_svc():Added to Deinstantiate Queue")
      return False

  global svc_dict
  if (svc_dict.has_key(svc_id)):
    for index, value in enumerate(svc_dict[svc_id]):
      isp,imac = value[0],value[1]
      #log.warning("isp=%s,imac=%s, svc_mac_Addr=[%s]]", isp, imac, svc_mac_addr)
      if EthAddr(imac) == EthAddr(svc_mac_addr) and (int(isp) == 0 or int(isp) <= MIN_SP_PEAK_THRESHOLD or int(isp) > MAX_SHADOW_PRICE):
        pop_val = svc_dict[svc_id].pop(index)
        value[0] = 0
        #log.warning("Popped Value [%s], Svc_dict[%s]", pop_val, svc_dict[svc_id])
        if svc_dict.has_key(0):
          svc_dict[0].append(value)
        else:
          value[0] = 0 #str(0)
          svc_dict[0] = [value]
        svc_free_list.append(value)
        svc_de_instantiation_log[key] = [time.time(),0]
        log.warning("bringdown_service_instace_for_svc([%d:%s]):completed!", svc_id, value)
        export_svc_dict_to_file()
        return True
      else:
        pass 
        #log.warning("bringdown_service_instace_for_svc([%d:%s]):Failed!", svc_id, value)

  return False
  
def check_and_swap_service_instance_for_svc(svc_id=0):
  global svc_dict
  if (svc_dict.has_key(svc_id) is False):
    log.error ('Key[%d] not found! skipp swapping for the requested service',svc_id)
    return False
  
  for key,value in svc_dict.items():
    if key!= 0 and key != svc_id:
      for x in range(len(value)):
        s_sp,s_mac_id,s_ip,s_sw_id = value[x]
        if int(s_sp) == 0:
          log.warning("Swap Underutilized service[ %d, at %s] to Over utilized service[%d]", key,value[x],svc_id)
          return True
  return False

def log_inst_sp(svc_id=0, mac_addr="", sp = 0):
  fname = str(svc_id) + '_' + str(mac_addr[mac_addr.rfind(":")+1:])
  fname = rdir_p + fname + '.csv'  #e.g /home/mininet/drench/results/13_1.csv
  #if ( False == os.path.isfile(fname)):
  #  return 0

  with open(fname, 'wb') as csvfile0:
    csvfile0.write(str(sp))
  return
   
# old_sp = cur_sp == 0? (5) : 15*math.log10(cur_sp)
# ewma_new_sp = new_sp*0.7 + old_sp
def get_ewma_shadow_price(mac_addr= "", svc_id=0, cur_sp=0, new_sp=0):
  wt_factor = 0.70 #0.90 #0.80  # 0.9 0.94
  #Mathematically (val = sqrt( lambda*variance^2, + (1-lambda)*cur_sp^2 )):: variance^2 = (1/N)(SUM(SP1^2,SP2^2...SPnew^2))
  #ewma_new_sp = cur_sp + (new_sp*wt_factor + cur_sp*(1-wt_factor))
  if new_sp < 0:
    ewma_new_sp = cur_sp + new_sp
    if ewma_new_sp < 0: ewma_new_sp=0
  else:
    ewma_new_sp = cur_sp + (new_sp*wt_factor)
  #ewma_new_sp = math.fabs(math.ceil(ewma_new_sp-0.49))
  ewma_new_sp = int(math.fabs(math.ceil(ewma_new_sp-0.49)))
  if ewma_new_sp < MIN_SHADOW_PRICE: ewma_new_sp = MIN_SHADOW_PRICE
  if ewma_new_sp > MAX_SHADOW_PRICE: ewma_new_sp = MAX_SHADOW_PRICE
  return ewma_new_sp

def update_shadow_price(mac_addr= "", svc_id=0, shadow_price=0):
  ''' 
  This function is for updating SP, specifically for queue/PacketSize based SP updates only.
  Dependency: flow_stats_sk.py logic for updating SP based on Packets size
  '''
  global svc_dict
  #func = int(bin(svc_id)[2:])
  found = False
  clear = False
  
  if mac_addr is None:
    log.warning(" For Mac_Addr(Wrong):%s, svc_id: %d, value:%d", mac_addr, svc_id, shadow_price)
    return (False,False)
  if False == svc_dict.has_key(svc_id):
    log.warning(" For Mac_Addr:%s, svc_id(Wrong): %d, value:%d", mac_addr, svc_id, shadow_price)
    return (False,False)
  
  log.warning("update_shadow_price(): For Mac_Addr:%s, svc_id: %d, set_sp_value:%d", mac_addr, svc_id, shadow_price)
  
  min_sp = shadow_price
  cur_sp = 0
  new_sp = shadow_price
  #sw_id = 0
  for x in range(len(svc_dict[svc_id])):
    sp,mac_id,ip,sw_id = svc_dict[svc_id][x][0],svc_dict[svc_id][x][1],svc_dict[svc_id][x][2],svc_dict[svc_id][x][3]
    cur_sp = int(sp)
    if(EthAddr(mac_addr) == EthAddr(mac_id) or str(mac_addr) == str(mac_id) ):
      new_sp = get_ewma_shadow_price(mac_addr=mac_addr, svc_id=svc_id, cur_sp=cur_sp, new_sp=shadow_price)
      svc_dict[svc_id][x][0] = new_sp
      log.warning("DPID,FNCID[%d,%d],change_SP:[%d]: => Old value:[%d] New Value:[%d]:: Updated Dict:%s", sw_id, svc_id, shadow_price, cur_sp,new_sp,svc_dict[svc_id])
      found = True
      log_inst_sp(svc_id=svc_id, mac_addr=mac_id, sp = svc_dict[svc_id][x][0])
      break
  
  if found is False:
    log.warning(" Record Not found for Mac_Addr(Wrong):%s, svc_id: %d, value:%d", mac_addr, svc_id, shadow_price)
  
  #comment out for now: as the instatiation will be done on-demand and bringdown in the timer thread
  return (found,clear)
  
  # Find out clear flag: <More appropriate way>
  # if any SP is over-utilized or under utilized (the recent updated ) and the rest are in opposite phase then clear
  # if all SP in nominal range or below threshold or all above threshold then False
  check_for_min = False
  if new_sp <= MIN_SP_THRESHOLD: #and cur_sp > MIN_SP_THRESHOLD:
    check_for_min = False
    pass
  elif new_sp >= MAX_SP_THRESHOLD: #and cur_sp < MAX_SP_THRESHOLD:
    check_for_min = True 
  else: # Within operational range =>(clear = False)
    pass
    #check_for_min = True
    #  return(found,clear)

  for x in range(0,len(svc_dict[svc_id])):
    sp,mac_id = int(svc_dict[svc_id][x][0]),svc_dict[svc_id][x][1]
    if EthAddr(mac_addr) != EthAddr(mac_id):
      if check_for_min is True:     # Find ANY SERVICE WITH SP LESSER THAN MAX THRESHOLD, then set clear (to offload from this service to min loaded service)
        if sp < MAX_SP_THRESHOLD :#or ((sp+MIN_SP_THRESHOLD/2)<(new_sp)):
          clear = True
          break
      else:                         # Find ANY SERVICE WITH SP GREATER THAN MAX THRESHOLD, then set the clear (to redirect flows from overloaded to this new one)
        if sp > MAX_SP_THRESHOLD :#or (sp > (new_sp+MIN_SP_THRESHOLD/2)):
          clear = True
          break

  
  #for x in range(0,len(svc_dict[svc_id])):
    #if check_for_min is True:
  
  if check_for_min is True: #and clear is False:
    if (True == (all(int(x[0]) > MAX_SP_PEAK_THRESHOLD for x in svc_dict[svc_id]))):
      result = instantiate_new_instance_for_svc(svc_id=svc_id, cur_max_sp=new_sp)
      log.warning("All instances for svc_id[%d] are overloaded, new instantiation[%d]!!", svc_id,result)
      if result == -1:
        result = check_and_swap_service_instance_for_svc(svc_id=svc_id)
        log.warning("All instances for svc_id[%d] are overloaded, swap service instance[%d]!!", svc_id,result)
      # Need to remember the Time of instantiation and set threshold to avoid repeated instantiations
      
  #if check_for_min is False:
  #  if (True == (all(int(x[0]) < MIN_SP_THRESHOLD  or int(x[0]) < MIN_SP_THRESHOLD  for x in svc_dict[svc_id]))):
  #    log.warning("All instances for svc_id[%d] are Underloaded!!", svc_id)
  
  log.warning("sw:svc[%d,%d],set_sp:[%d]: => Old value:[%d] New Value:[%d]:: Found,Clear[%d:%d] Updated Dict:[%s]", sw_id, svc_id, shadow_price, cur_sp,new_sp,found,clear,svc_dict[svc_id])
  return(found,clear)

def update_shadow_price_2(mac_addr="",svc_id=0,shadow_price=0):
  ''' 
  This function is for updating SP, specifically for Flowcount based SP updates only.
  Dependency: flow_stats_sk.py logic for updating SP
  '''
  global svc_dict
  #func = int(bin(svc_id)[2:])
  found = False
  clear = False
  
  #Comment this if want to use Shadow Price update based on Flow Count and Not Queue Size ()
  # If uncommented, then ensure the Shadow Price update based on PacketCount is uncommented in flowstats._handle_flowstats_received_new()
  #return
  
  if mac_addr is None:
    log.warning(" For Mac_Addr(Wrong):%s, svc_id: %d, value:%d", mac_addr, svc_id, shadow_price)
    return (False,False)
  if False == svc_dict.has_key(svc_id):
    log.warning(" For Mac_Addr:%s, svc_id(Wrong): %d, value:%d", mac_addr, svc_id, shadow_price)
    return (False,False)
  
  #log.warning("update_shadow_price_2(): For Mac_Addr:%s, svc_id: %d, set_sp_value:%d", mac_addr, svc_id, shadow_price)
  
  cur_sp = 0
  new_sp = shadow_price
  if new_sp == MAX_SP_FOR_BRINGDOWN:
    log.warning("Svc Instance [%d: %s] is marked for Bringdown with Shadow Price[%d]!!",svc_id,mac_addr,shadow_price)
  elif new_sp > MAX_SHADOW_PRICE: pass #new_sp = MAX_SHADOW_PRICE
  elif new_sp < MIN_SHADOW_PRICE: new_sp = MIN_SHADOW_PRICE
  
  #Skip updating SP if the instance is in the svc_bringdown_candidate_instances = [] #[(svc,mac_addr),] List of candidates for Bringdown in this cycle 
  global svc_bringdown_candidate_instances
  if (svc_id,mac_addr) in svc_bringdown_candidate_instances :
    log.warning("svc[%d] at instance [%s] is in bringdown list! SKIP UPDATE OF SP!! ", svc_id, mac_addr)
    return
  
  #sw_id = 0
  for x in range(len(svc_dict[svc_id])):
    sp,mac_id,ip,sw_id = svc_dict[svc_id][x][0],svc_dict[svc_id][x][1],svc_dict[svc_id][x][2],svc_dict[svc_id][x][3]
    cur_sp = int(sp)
    if(EthAddr(mac_addr) == EthAddr(mac_id) or str(mac_addr) == str(mac_id) ):
      #new_sp = get_ewma_shadow_price(mac_addr=mac_addr, svc_id=svc_id, cur_sp=cur_sp, new_sp=shadow_price)
      #svc_dict[svc_id][x][0] = new_sp
      delta = new_sp - cur_sp
      if new_sp:
        svc_dict[svc_id][x][0] = int(cur_sp + delta*0.7)
      else:
        svc_dict[svc_id][x][0] = 0
      #log.warning("DPID,FNCID[%d,%d],change_SP:[%d]: => Old value:[%d] New Value:[%d]:: Updated Dict:%s", sw_id, svc_id, shadow_price, cur_sp,new_sp,svc_dict[svc_id])
      found = True
      log_inst_sp(svc_id=svc_id, mac_addr=mac_id, sp = svc_dict[svc_id][x][0])
      break

  if found is False:
    log.warning(" Record Not found for Mac_Addr(Wrong):%s, svc_id: %d, value:%d", mac_addr, svc_id, shadow_price)
    return(False,False)
  
  #comment out for now: as the instatiation will be done on-demand and bringdown in the timer thread
  return (found,clear)
  
  # Find out clear flag: <More appropriate way>
  # if any SP is over-utilized or under utilized (the recent updated ) and the rest are in opposite phase then clear
  # if all SP in nominal range or below threshold or all above threshold then False
  check_for_min = False
  if new_sp <= MIN_SP_THRESHOLD: #and cur_sp > MIN_SP_THRESHOLD:
    check_for_min = False
    pass
  elif new_sp >= MAX_SP_THRESHOLD: #and cur_sp < MAX_SP_THRESHOLD:
    check_for_min = True 
  else: # Within operational range =>(clear = False)
    pass
    #check_for_min = True
    #  return(found,clear)

  for x in range(0,len(svc_dict[svc_id])):
    sp,mac_id = int(svc_dict[svc_id][x][0]),svc_dict[svc_id][x][1]
    if EthAddr(mac_addr) != EthAddr(mac_id):
      if check_for_min is True:     # Find ANY SERVICE WITH SP LESSER THAN MAX THRESHOLD, then set clear (to offload from this service to min loaded service)
        if sp < MAX_SP_THRESHOLD :#or ((sp+MIN_SP_THRESHOLD/2)<(new_sp)):
          clear = True
          break
      else:                         # Find ANY SERVICE WITH SP GREATER THAN MAX THRESHOLD, then set the clear (to redirect flows from overloaded to this new one)
        if sp > MAX_SP_THRESHOLD :#or (sp > (new_sp+MIN_SP_THRESHOLD/2)):
          clear = True
          break

  if check_for_min is True:
    if (True == (all(int(x[0]) > MAX_SP_PEAK_THRESHOLD for x in svc_dict[svc_id]))):
      result = instantiate_new_instance_for_svc(svc_id=svc_id, cur_max_sp=new_sp)
      log.warning("All instances for svc_id[%d] are overloaded, new instantiation=[%d]!!", svc_id,result)
      if result == -1:
        result = check_and_swap_service_instance_for_svc(svc_id=svc_id)
        log.warning("No Free Instance available to instantiate for overloaded svc_id[%d]! swap with any other service instance[%d]!!", svc_id,result)
      # Need to remember the Time of instantiation and set threshold to avoid repeated instantiations
      
  if check_for_min is False:
    if (True == (all(int(x[0]) < MIN_SP_THRESHOLD or int(x[0]) ==  MAX_SP_FOR_BRINGDOWN for x in svc_dict[svc_id]))):
      #log.warning("All instances for svc_id[%d] are Underloaded!!", svc_id)
      for x in svc_dict[svc_id]:
        if int(x[0]) == 0 or int(x[0]) == MAX_SP_FOR_BRINGDOWN :
          result = bringdown_service_instace_for_svc(svc_id = svc_id, svc_mac_addr = x[1],sw_path_list=None, in_file=csv2_file)
          log.warning("Bring Down Service Instance[%d:%s]!,result[%d]!!", svc_id, x[1],result)

  log.warning("update_shadow_price_2(): DPID,FNCID[%d,%d],change_SP:[%d]: => Old value:[%d] New Value:[%d]:: Found,Clear [%d:%d] Updated Dict:%s", sw_id, svc_id, shadow_price, cur_sp,new_sp,found,clear,svc_dict[svc_id])
  return(found,clear)

svc_bringdown_candidate_instances = [] #[(svc,mac_addr),] List of candidates for Bringdown in this cycle 
def check_for_svc_redirections():
  svc_redirection_list = [] #[(svc_id,instance)]
  global svc_bringdown_candidate_instances
  global svc_dict
  keys_list = svc_dict.keys()
  for svc_id in keys_list:
    # check if the SVC's load is imbalanced. (1. more than 1 instance, 2. instance with 0 or less SP than MIN, 3.instance with max_sp and sp!=101)
    if ( (len(svc_dict[svc_id]) > 1) and (True == (any(int(x[0]) < MIN_SP_THRESHOLD for x in svc_dict[svc_id]))) and
         (True == (any(int(x[0]) > MAX_SP_THRESHOLD for x in svc_dict[svc_id])))):
      for i, x in enumerate(svc_dict[svc_id]):
        if ((svc_id,x[1]) in svc_bringdown_candidate_instances):
          log.warning("skip the svc_instance[%d:%s] from considering for redirections!", svc_id, x)
          continue
        if int(x[0]) > MAX_SP_THRESHOLD and int(x[0]) != MAX_SP_FOR_BRINGDOWN:
           svc_redirection_list.append((svc_id, x))
           #break
  """"
  for svc_id in keys_list:
    skip = True
    # Check if there are more than one service instance and at least one service instance with SP=0 and not all services with sp = 0 and more than one service is available.
    if ( (len(svc_dict[svc_id]) > 1) and (True == (any(int(x[0]) == 0 or int(x[0]) < MIN_SP_THRESHOLD for x in svc_dict[svc_id]))) and 
         (True == (any(int(x[0]) > MAX_SP_THRESHOLD and int(x[0]) < MAX_SHADOW_PRICE for x in svc_dict[svc_id]))) and
         (False == all(int(x[0]) == 0 or int(x[0]) < MIN_SP_THRESHOLD for x in svc_dict[svc_id]))):
      skip = False
      # Below condition is far restrictive but helps reduce the number of redirections
      # Additionally now ensure that all non zero sp (or SP below MIN THRESHOLD) instances are operating above max/min threshold, if not then skip.
      for i, x in enumerate(svc_dict[svc_id]):
        if ((svc_id,x[1]) in svc_bringdown_candidate_instances):
          skip = True
          break
        if int(x[0]) != 0 and int(x[0]) <  MIN_SP_THRESHOLD:
          # Exclude this instance from comparison!
          pass
        else:
          # Instance SP must be >= MAX SP
          if int(x[0]) <  MAX_SP_THRESHOLD:
            skip = True
            break
    if skip is False:
      svc_redirection_list.append((svc_id, x))
  """
  if svc_redirection_list:
      log.warning("Probable Redirection List:[%s]", svc_redirection_list)
  return svc_redirection_list

# this function checks and updates list of candidate instances for bringdown_service_instace_for_svc
SVC_BRINGDOWN_THRESHOLD_TIME = 2
def check_for_svc_bring_down():
  #conditions needed. 
  # 1. More than 1 instance
  # 2. Instance SP should be < MIN_SP_PEAK_THRESHOLD or 0
  # 3. At least one and Rest of instance SP for same key > MIN_SHADOW_PRICE
  # 4. Must avoid adding instances that were recently instantiated < 10 seconds.
  
  #Note: svc_bringdown_candidate_instances() is necessary to eliminate the chance of selecting the same candidates for bringdown/redirection.
  # This sort of puts restriction that the check_for_svc_bring_down() should be done first and then the call for check_for_svc_redirections()
  global svc_bringdown_candidate_instances
  svc_bringdown_candidate_instances = []
  svc_bringdown_list = []
  global svc_instantiation_list #[(svc_id,[svc_instance], time.time())]
  def get_svc_instantiation_time(svc, instance):
    for sid,inst,tm in svc_instantiation_list:
      if sid == svc and inst[1] == instance[1]:
        log.warning("Sid[%d], instance[%s] instantiated at time [%s]", sid,inst,tm)
        return tm
    return time.time()
    
  global svc_dict
  global default_svc_list
  keys_list = svc_dict.keys()
  for svc_id in keys_list:
    # Check if there are more than one service instance and at least one service instance with SP=0 and not all services with sp = 0 and more than one service is available.
    if ( (len(svc_dict[svc_id]) > 1) and (True == (any(int(x[0]) == 0 or int(x[0]) <= MIN_SP_PEAK_THRESHOLD for x in svc_dict[svc_id]))) and 
       #(True == (any(int(x[0]) > MIN_SP_THRESHOLD for x in svc_dict[svc_id]))) and 
       #(True == all(int(x[0]) > MIN_SP_THRESHOLD for x in svc_dict[svc_id] if int(x[0]) > MIN_SP_PEAK_THRESHOLD)) and # Ensure not all the rest are operating beyond peak SP threshold
       (False == all(int(x[0]) > MAX_SP_PEAK_THRESHOLD for x in svc_dict[svc_id] if int(x[0]) > MIN_SP_PEAK_THRESHOLD))):
      # Now choose the candidate instance.
      for i, x in enumerate(svc_dict[svc_id]):
        if ( ((int(x[0]) == 0 or int(x[0]) <= MIN_SP_PEAK_THRESHOLD or int(x[0]) ==  MAX_SP_FOR_BRINGDOWN)) and ((svc_id,x) not in svc_bringdown_list) and 
           ((get_svc_instantiation_time(svc=svc_id,instance=x) + SVC_BRINGDOWN_THRESHOLD_TIME) < (time.time())) and ((svc_id,x[1]) not in default_svc_list)):
          svc_bringdown_list.append((svc_id,x))
          #change SP of the instance to MAX_SP_FOR_BRINGDOWN
          #x[0] = MAX_SP_FOR_BRINGDOWN
          #update_shadow_price_2(mac_addr=x[1],svc_id=svc_id,shadow_price= MAX_SP_FOR_BRINGDOWN)
          svc_bringdown_candidate_instances.append((svc_id,x[1]))
          break #For now just select one instance for each service in every cycle.

  if svc_bringdown_list:
    log.warning("Probable SVC Bringdown List:[%s]", svc_bringdown_list)
  return svc_bringdown_list
  

class LLDPSender (object):
  """
  Sends out discovery packets
  """
  #n_cur_times = 0
  #n_max_times = 100

  SendItem = namedtuple("LLDPSenderItem", ('dpid','port_num','packet'))

  #NOTE: This class keeps the packets to send in a flat list, which makes
  #      adding/removing them on switch join/leave or (especially) port
  #      status changes relatively expensive. Could easily be improved.

  # Maximum times to run the timer per second
  #_sends_per_sec = 15
  _sends_per_sec = 15       #EM040415

  def __init__ (self, send_cycle_time, ttl = 120):
    """
    Initialize an LLDP packet sender

    send_cycle_time is the time (in seconds) that this sender will take to
      send every discovery packet.  Thus, it should be the link timeout
      interval at most.

    ttl is the time (in seconds) for which a receiving LLDP agent should
      consider the rest of the data to be valid.  We don't use this, but
      other LLDP agents might.  Can't be 0 (this means revoke).
    """
    # Packets remaining to be sent in this cycle
    self._this_cycle = []

    # Packets we've already sent in this cycle
    self._next_cycle = []

    # Packets to send in a batch
    self._send_chunk_size = 1

    self._timer = None
    self._ttl = ttl
    self._send_cycle_time = send_cycle_time
    self._timer2 = None
    core.listen_to_dependencies(self)

  def _handle_openflow_PortStatus (self, event):
    """
    Track changes to switch ports
    """
    switch = event.dpid
    port = event.port
    print "LLDPSENDER::I heard a port status change from switch {} port {}".format(switch, port)
    if event.added:
      print "LLDPSENDER::event.added"
      self.add_port(event.dpid, event.port, event.ofp.desc.hw_addr)
    elif event.deleted:
      print "LLDPSENDER::event.deleted"
      self.del_port(event.dpid, event.port)

  def _handle_openflow_ConnectionUp (self, event):
    self.del_switch(event.dpid, set_timer = False)
    log.info("_handle_openflow_ConnectionUp: [Switch:%s::ports[%s]]",event.dpid,event.ofp.ports )
    
    ports = [(p.port_no, p.hw_addr) for p in event.ofp.ports]

    for port_num, port_addr in ports:
      self.add_port(event.dpid, port_num, port_addr, set_timer = False)

    self._set_timer()

  def _handle_openflow_ConnectionDown (self, event):
    self.del_switch(event.dpid)

  def del_switch (self, dpid, set_timer = True):
    self._this_cycle = [p for p in self._this_cycle if p.dpid != dpid]
    self._next_cycle = [p for p in self._next_cycle if p.dpid != dpid]
    ##SK::TBD: Also clear this switch associated dictionary entries
    rem_svc_for_switch(sw=self,dpid=dpid)
    if set_timer: self._set_timer()

  def del_port (self, dpid, port_num, set_timer = True):
    if port_num > of.OFPP_MAX: return
    self._this_cycle = [p for p in self._this_cycle
                        if p.dpid != dpid or p.port_num != port_num]
    self._next_cycle = [p for p in self._next_cycle
                        if p.dpid != dpid or p.port_num != port_num]
    ##SK::TBD: Also clear this switch associated dictionary entries
    if (True == core.openflow_discovery.is_edge_port(dpid, port_num)):
      rem_svc_for_switch(sw=self,dpid=dpid) # Note: to do only if port num links to the host of service
    if set_timer: self._set_timer()

  def add_port (self, dpid, port_num, port_addr, set_timer = True):
    if port_num > of.OFPP_MAX: return
    self.del_port(dpid, port_num, set_timer = False)
    #SK
    self._next_cycle.append(LLDPSender.SendItem(dpid, port_num,
          self.create_discovery_packet(dpid, port_num, port_addr)))

    custom_lldp_pkts = []
    
    #custom_lldp_pkts = self.create_discovery_packet_sk(dpid, port_num, port_addr)
    if custom_lldp_pkts:
      for custom_lldp_pkt in custom_lldp_pkts:
        log.info("Custom LLD Packet: %s",custom_lldp_pkt )
        self._next_cycle.append(LLDPSender.SendItem(dpid, port_num,custom_lldp_pkt))

    if set_timer: self._set_timer()

  def _set_timer (self):
    if self._timer: self._timer.cancel()
    self._timer = None
    num_packets = len(self._this_cycle) + len(self._next_cycle)

    if num_packets == 0: return

    self._send_chunk_size = 1 # One at a time
    interval = self._send_cycle_time / float(num_packets)
    if interval < 1.0 / self._sends_per_sec:
      # Would require too many sends per sec -- send more than one at once
      interval = 1.0 / self._sends_per_sec
      chunk = float(num_packets) / self._send_cycle_time / self._sends_per_sec
      self._send_chunk_size = chunk

    self._timer = Timer(interval,
                        self._timer_handler, recurring=True)
                        
    #if self._timer2: self._timer2.cancel()
      #self._timer2 = None
    #if self._timer2 is None:
    #  log.info("Setting Timer2: %d", update_interval)
    #  self._timer2 =  Timer (update_interval, self.update_function_sk, recurring=True)


  def clearFlows(self):
      for c in core.openflow.connections:
        d = of.ofp_flow_mod(command = of.OFPFC_DELETE)
        c.send(d)
      log.warning("flows cleared on %s" % c)

  def function_handler_sk (self, dpid = 1, port_num = 1, port_addr = "xx:xx:xx:xx:xx:xx", mode = 0):
    """
    Function to lookup service info and create custom LLDP packet for existing services
    """
    ####TEST
    ####return self.create_discovery_packet(dpid,port_num,port_addr)

    global svc_free_list
    global svc_dict
    global csv2_file
    custom_packets_list = []
    log.debug("Gather packets for dpid=%d, port_num=%d, port_addr=%s",dpid,port_num,port_addr)
    csv2_file = 'none'
    if(os.path.isfile(list_file2_ln)):
      csv2_file = list_file2_ln
      log.debug("ListFile2 is:  %s ", csv2_file)
    elif (os.path.isfile(list_file2_wn)):
      csv2_file = list_file2_wn
      log.debug("ListFile2 is:  %s ", csv2_file)
    else:
      global abc
      if abc == 0:
        log.warn("ListFile2: %s is not Found ", csv2_file)
        abc = 1
      else:
        #pass
        return None

    if not os.path.isfile(csv2_file):
      log.error("Resource File[%s] not found!! No services!!",csv2_file)
      return None
    with open(csv2_file, 'rb') as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
        sw_num = int(row['switch'])
        if(sw_num != dpid):
          #log.info("##### sw_num:%d not same as DPID:%d", sw_num, dpid)
          continue
        svc = int(row['function'],2)
        #mac = row['mac'].lstrip(' ').rstrip(' ')
        mac = row['mac']
        if mac: mac = mac.lstrip(' ').rstrip(' ')
        else: continue
        ip  = row['ip']
        if ip: ip = ip.lstrip(' ').rstrip(' ')
        sp  = int(row['sp'])
        fnc = str(row['function']).lstrip(' ').rstrip(' ')
        #log.info("#####sw_num:%d Fnc:%s mac:%s sp:%d svc:%d", sw_num, fnc, mac, sp, svc)
        iFlag = 1
        #log.info("##### %s:%d, %d",svc_dict.keys(),len(svc_dict.keys()),svc)
        #log.info("#### svc_dict = {%s}",svc_dict)
        if svc == 0:
          if [int(sp),mac,ip,sw_num] not in svc_free_list:
            svc_free_list.append([int(sp),mac,ip,sw_num])
            if svc_dict.has_key(0) and [int(sp),mac,ip,sw_num] not in svc_dict[0]:
              svc_dict[0].append([int(sp),mac,ip,sw_num])
            else:
              svc_dict[0] = [[int(sp),mac,ip,sw_num]]
              #REMEMBER: TODO: revert change in rem_svc_for_switch and add back details for svc_free_list in instantiate/deinstantiate calls
          iFlag = 0
          continue
        if ( svc_dict.has_key(svc)):
        #if svc in svc_dict.keys:
          for x in range(0,len(svc_dict[svc])):
            isp,imac,iip = int(svc_dict[svc][x][0]),svc_dict[svc][x][1],svc_dict[svc][x][2]
            log.debug("##### isp:%d imac:%s sp:%d mac:%s", isp, imac, sp, mac)
            if(isp == sp and imac == mac and ip == iip):
              iFlag = 0
        else:
          pass
          #log.info("#### Key:[%d] not found in svc_dict:%s",svc,svc_dict)

        if iFlag == 1:
          custom_packets_list.append(self.insert_function_sk( svc=svc,macad=mac,sp=sp,ip=ip, dpid=dpid, port_num = port_num, port_addr = port_addr, mode=mode))
          #return self.insert_function_sk( svc=svc,macad=mac,sp=sp,ip=ip,dpid=dpid, port_num = port_num, port_addr = port_addr, mode=mode)
        else:
          log.debug("**** Skipped duplicate packet for Key:[%d] in svc_dict:%s ****",svc,svc_dict.keys())
          pass
          #return self.create_discovery_packet(dpid,port_num,port_addr)
          #return None
    log.debug('Number of Custom LLDP Packets to be sent: %d', len(custom_packets_list))
    return custom_packets_list

  def insert_function_sk(self,svc, macad,sp, ip, dpid = 1, port_num = 1, port_addr = "xx:xx:xx:xx:xx:xx", mode=0):

    chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)
    chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])

    port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(port_num))

    ttl = pkt.ttl(ttl = svc)
    log.debug("Initial TTL:%s",ttl)

    sysdesc = pkt.system_description()
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])

    discovery_packet = pkt.lldp()
    discovery_packet.tlvs.append(chassis_id)
    discovery_packet.tlvs.append(port_id)

    #sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1] +'\n mac:'+macad + '\n sp:'+sp) #EM240315
    #sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1] +'\n mac:'+macad + '\n sp:'+str(sp)) #EM240315
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1] +'\n ip:'+ip +'\n mac:'+macad + '\n sp:'+str(sp))
    #log.info("Setting packet with TTL: %s, val:%d", ttl,svc)

    discovery_packet.tlvs.append(ttl)
    discovery_packet.tlvs.append(sysdesc)

    discovery_packet.tlvs.append(pkt.end_tlv())

    eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)
    eth.src = port_addr
    eth.dst = pkt.ETHERNET.NDP_MULTICAST
    eth.payload = discovery_packet

    po = of.ofp_packet_out(action = of.ofp_action_output(port=port_num))
    po.data = eth.pack()
    if mode == 0:
      core.openflow.sendToDPID(dpid, po.pack())
      log.debug('SK: Send Specific LLDP for MAC:[%s] with SP:[%s] and TTL:[%s]', str(macad),str(sp),str(svc)) #SK
    else:
      log.debug('SK: Prepared and Return Specific LLDP for MAC:[%s] with SP:[%s] and TTL:[%s]', str(macad),str(sp),str(svc)) #SK
      pass
    add_pkt_size_info(PKT_LOG_TYPE_PKT_LLDP, len(po.pack())) # LLDP packet Size
    return po.pack()
  
  def update_function_sk(self):
    in_file = csv2_file
    mode = 0
    if ( False == os.path.isfile(in_file)):
      return -1
    #log.info ("Inside:update_function_sk@ %s", time.time())
    def get_key(item):
      return item[1]

    global svc_dict
    rem_val_index = []
    with open(in_file, 'rb') as csvfile:
      for key,value in svc_dict.items():
        for x in range(len(value)):
          s_sp,s_mac_id,s_ip,s_sw_id = value[x]
          csvfile.seek(0,0)
          reader = csv.DictReader(csvfile)
          found = False
          for row in reader:
            #print row
            sw_num = int(row['switch'])
            svc = int(row['function'], 2)
            mac = row['mac']
            ip = row['ip']
            sp = int(row['sp'])
            if key == svc and s_sw_id == sw_num and s_mac_id==mac:
              found = True
              break
          if found is False:
            if mode == 0:
              rem_val_index.append((key,x))
          else:
            #print "Key %d and value_list[%s] found!" %(key,value[x])
            if mode == 1:
              rem_val_index.append((key,x))

    #print "To Remove:"
    if rem_val_index:
      self.clearFlows()
      rem_val_index.sort(reverse=2,key=get_key)
      #print rem_val_index
      for x in range(len(rem_val_index)):
        key, index = rem_val_index[x]
        svc_dict[key].pop(index)
        #if(len(svc_dict[key]) == 0):
        #    svc_dict.pop(key)
    #log.info ("Leaving:update_function_sk@ %s", time.time())
    return 0
  
  def _timer_handler (self):
    """
    Called by a timer to actually send packets.

    Picks the first packet off this cycle's list, sends it, and then puts
    it on the next-cycle list.  When this cycle's list is empty, starts
    the next cycle.
    """
    #self.n_cur_times = self.n_cur_times+1
    #if (self.n_cur_times >= self.n_max_times):
    #  if(self.n_cur_times == 100):
    #    log.info("***** Reached Maximum Timer Limit ****")
    #  return

    num = int(self._send_chunk_size)
    fpart = self._send_chunk_size - num
    if random() < fpart: num += 1

    #self.function_handler_sk(mode=0)                    #SK

    for _ in range(num):
      if len(self._this_cycle) == 0:
        self._this_cycle = self._next_cycle
        self._next_cycle = []
        #shuffle(self._this_cycle)
      item = self._this_cycle.pop(0)
      self._next_cycle.append(item)
      core.openflow.sendToDPID(item.dpid, item.packet)

  def create_discovery_packet (self, dpid, port_num, port_addr):
    """
    Build discovery packet
    """
    chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)
    chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])
    # Maybe this should be a MAC.  But a MAC of what?  Local port, maybe?


    port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(port_num))

    ttl = pkt.ttl(ttl = self._ttl)

    sysdesc = pkt.system_description()
    sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])

    discovery_packet = pkt.lldp()
    discovery_packet.tlvs.append(chassis_id)
    discovery_packet.tlvs.append(port_id)

    discovery_packet.tlvs.append(ttl)
    discovery_packet.tlvs.append(sysdesc)

    discovery_packet.tlvs.append(pkt.end_tlv())

    eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)
    eth.src = port_addr
    eth.dst = pkt.ETHERNET.NDP_MULTICAST
    eth.payload = discovery_packet

    po = of.ofp_packet_out(action = of.ofp_action_output(port=port_num))
    po.data = eth.pack()
    return po.pack()

  def create_discovery_packet_sk (self, dpid, port_num, port_addr):
    return None
    """
    Build custom discovery packet
    """
    return self.function_handler_sk(dpid,port_num,port_addr,mode=1)


  def ChecksendCustomLLPD_sk(self, dpid, port_num, port_addr):
    """"
    Function checks if we need to send the custom LLDP packet
    returns None if not else returns the LLDP packet
    """
    return True
    #return False

class LinkEvent (Event):
  """
  Link up/down event
  """
  def __init__ (self, add, link):
    Event.__init__(self)
    self.link = link
    self.added = add
    self.removed = not add

  def port_for_dpid (self, dpid):
    if self.link.dpid1 == dpid:
      return self.link.port1
    if self.link.dpid2 == dpid:
      return self.link.port2
    return None


class Link (namedtuple("LinkBase",("dpid1","port1","dpid2","port2"))):
  @property
  def uni (self):
    """
    Returns a "unidirectional" version of this link

    The unidirectional versions of symmetric keys will be equal
    """
    pairs = list(self.end)
    pairs.sort()
    return Link(pairs[0][0],pairs[0][1],pairs[1][0],pairs[1][1])

  @property
  def end (self):
    return ((self[0],self[1]),(self[2],self[3]))

  def __str__ (self):
    return "%s.%s -> %s.%s" % (dpid_to_str(self[0]),self[1],
                               dpid_to_str(self[2]),self[3])

  def __repr__ (self):
    return "Link(dpid1=%s,port1=%s, dpid2=%s,port2=%s)" % (self.dpid1,
        self.port1, self.dpid2, self.port2)


def update_all_file_paths():
  global log_file, fsl_file, pktlog_file
  global hdir_p, rdir_p, list_file2_wn, list_file2_ln
  hdir_p = util_sk.get_base_dir()
  rdir_p = util_sk.get_results_dir() + '/'
  list_file2_ln = util_sk.get_rsrc_file()
  list_file2_wn = list_file2_ln
  log.warning("hdir_p[%s], rdir_p[%s] ist_file2_ln[%s], list_file2_wn [%s]", hdir_p, rdir_p,list_file2_ln,list_file2_wn)
  log_file = rdir_p + "splog.csv"
  fsl_file = rdir_p + "fslog.csv"
  pktlog_file = rdir_p + "comm_overhd_log.csv"
  return
class Discovery (EventMixin):
  """
  Component that attempts to discover network toplogy.

  Sends out specially-crafted LLDP packets, and monitors their arrival.
  """

  _flow_priority = 65000     # Priority of LLDP-catching flow (if any)
  _link_timeout = 10         # How long until we consider a link dead
  _timeout_check_period = 5  # How often to check for timeouts
  #_link_timeout = 100         # How long until we consider a link dead
  #_timeout_check_period = 50  # How often to check for timeouts

  _eventMixin_events = set([
    LinkEvent,
  ])

  _core_name = "openflow_discovery" # we want to be core.openflow_discovery

  Link = Link
  
  def __init__ (self, install_flow = True, explicit_drop = True,
                link_timeout = None, eat_early_packets = False):
    self._eat_early_packets = eat_early_packets
    self._explicit_drop = explicit_drop
    self._install_flow = install_flow
    if link_timeout: self._link_timeout = link_timeout
    
    # setup the directory and files from Utils
    update_all_file_paths()

    self.adjacency = {} # From Link to time.time() stamp
    self._sender = LLDPSender(self.send_cycle_time)

    # Listen with a high priority (mostly so we get PacketIns early)
    core.listen_to_dependencies(self,
        listen_args={'openflow':{'priority':0xffffffff}})

    #Timer(self._timeout_check_period, self._expire_links, recurring=True)

  @property
  def send_cycle_time (self):
    return self._link_timeout / 2.0

  def install_flow (self, con_or_dpid, priority = None):
    if priority is None:
      priority = self._flow_priority
    if isinstance(con_or_dpid, (int,long)):
      con = core.openflow.connections.get(con_or_dpid)
      if con is None:
        log.warn("Can't install flow for %s", dpid_to_str(con_or_dpid))
        return False
    else:
      con = con_or_dpid

    match = of.ofp_match(dl_type = pkt.ethernet.LLDP_TYPE,
                          dl_dst = pkt.ETHERNET.NDP_MULTICAST)
    msg = of.ofp_flow_mod()
    msg.priority = priority
    msg.match = match
    msg.actions.append(of.ofp_action_output(port = of.OFPP_CONTROLLER))
    con.send(msg)
    return True

  def _handle_openflow_ConnectionUp (self, event):
    if self._install_flow:
      # Make sure we get appropriate traffic
      log.debug("Got Connection from %s", dpid_to_str(event.dpid))
      self.install_flow(event.connection)

  def _handle_openflow_ConnectionDown (self, event):
    # Delete all links on this switch
    log.debug("_handle_openflow_ConnectionDown %s", dpid_to_str(event.dpid))
    self._delete_links([link for link in self.adjacency
                        if link.dpid1 == event.dpid
                        or link.dpid2 == event.dpid])

  def _handle_ConnectionDown (self, event):
    log.debug("_handle_ConnectionDown %s", dpid_to_str(event.dpid))

  def _handle_PortStatus(self, event):
    """Gather up some information about the event to print"""
    switch = event.dpid
    port = event.port
    state = ""
    #print "Discovery::I heard a port status change from switch {} port {}".format(switch, port)
    ofp_phy_port = event.ofp.desc
    name = ofp_phy_port.name
    if  ofp_phy_port.state & 1 == 0:
        state = "Up"
    else: #ofp_phy_port.state & OFPPS_LINK_DOWN == True
        state = "Down"
    #print "Port {} is {}".format(name, state)
    log.info("_handle_PortStatus: for switch: %d, port:%d, is %s", switch,port,state)
    
    #if((state == "Down")and (self.is_edge_port(event.dpid,event.port) == True) ):
      #log.info("_handle_PortStatus(rem_svc_for_switch): for switch: %d, port:%d, is %s", switch,port,state)
      #rem_svc_for_switch(sw = None, dpid = event.dpid)
    
  def _handle_openflow_PortStatus(self, event):
    return self._handle_PortStatus(event)

  def _expire_links (self):
    """
    Remove apparently dead links
    """
    now = time.time()

    expired = [link for link,timestamp in self.adjacency.iteritems()
               if timestamp + self._link_timeout < now]
    if expired:
      for link in expired:
        log.warning('link timeout: %s', link)

      self._delete_links(expired)

  def _handle_openflow_PacketIn (self, event):
    """
    Receive and process LLDP packets
    """
    packet = event.parsed

    if (packet.effective_ethertype != pkt.ethernet.LLDP_TYPE
        or packet.dst != pkt.ETHERNET.NDP_MULTICAST):
      if not self._eat_early_packets: return
      if not event.connection.connect_time: return
      enable_time = time.time() - self.send_cycle_time - 1
      if event.connection.connect_time > enable_time:
        log.debug("Halting PacketIn Event for Non LLDP packet %i", event.ofp.buffer_id)
        return EventHalt
      return

    if self._explicit_drop:
      if event.ofp.buffer_id is not None:
        #log.debug("Dropping LLDP packet %i", event.ofp.buffer_id)
        msg = of.ofp_packet_out()
        msg.buffer_id = event.ofp.buffer_id
        msg.in_port = event.port
        event.connection.send(msg)

    lldph = packet.find(pkt.lldp)
    if lldph is None or not lldph.parsed:
      log.error("LLDP packet could not be parsed")
      return EventHalt
    if len(lldph.tlvs) < 3:
      log.error("LLDP packet without required three TLVs")
      return EventHalt
    if lldph.tlvs[0].tlv_type != pkt.lldp.CHASSIS_ID_TLV:
      log.error("LLDP packet TLV 1 not CHASSIS_ID")
      return EventHalt
    if lldph.tlvs[1].tlv_type != pkt.lldp.PORT_ID_TLV:
      log.error("LLDP packet TLV 2 not PORT_ID")
      return EventHalt
    if lldph.tlvs[2].tlv_type != pkt.lldp.TTL_TLV:
      log.error("LLDP packet TLV 3 not TTL")
      return EventHalt

    def lookInSysDesc ():
      r = None
      for t in lldph.tlvs[3:]:
        if t.tlv_type == pkt.lldp.SYSTEM_DESC_TLV:
          # This is our favored way...
          for line in t.payload.split('\n'):
            if line.startswith('dpid:'):
              try:
                return int(line[5:], 16)
              except:
                pass
          if len(t.payload) == 8:
            # Maybe it's a FlowVisor LLDP...
            # Do these still exist?
            try:
              return struct.unpack("!Q", t.payload)[0]
            except:
              pass
          return None

    def getMAC ():
      r = None
      for t in lldph.tlvs[3:]:
        if t.tlv_type == pkt.lldp.SYSTEM_DESC_TLV:
          # This is our favored way...
          for line in t.payload.split('\n'):
            if line.startswith(' mac:'):
              try:
                return (line[5:])
                #return (line[6:])
              except:
                pass
          return None

    def getSP ():
      r = None
      for t in lldph.tlvs[3:]:
        if t.tlv_type == pkt.lldp.SYSTEM_DESC_TLV:
          # This is our favored way...
          for line in t.payload.split('\n'):
            #log.info("Line:[%s]", line)
            if line.startswith(' sp:'):
              try:
                log.debug("SP Value: [%s]", line[4:])
                return (line[4:])
              except:
                pass
          return None

    def getIP ():
      r = None
      for t in lldph.tlvs[3:]:
        if t.tlv_type == pkt.lldp.SYSTEM_DESC_TLV:
          # This is our favored way...
          for line in t.payload.split('\n'):
            #log.info("Line:[%s]", line)
            if line.startswith(' ip:'):
              try:
                #log.info("IP Value: [%s]", line[4:])
                return (line[4:])
              except:
                pass
          return None

    originatorDPID = lookInSysDesc()

    if originatorDPID == None:
      # We'll look in the CHASSIS ID
      if lldph.tlvs[0].subtype == pkt.chassis_id.SUB_LOCAL:
        if lldph.tlvs[0].id.startswith('dpid:'):
          # This is how NOX does it at the time of writing
          try:
            originatorDPID = int(lldph.tlvs[0].id[5:], 16)
          except:
            pass
      if originatorDPID == None:
        if lldph.tlvs[0].subtype == pkt.chassis_id.SUB_MAC:
          # Last ditch effort -- we'll hope the DPID was small enough
          # to fit into an ethernet address
          if len(lldph.tlvs[0].id) == 6:
            try:
              s = lldph.tlvs[0].id
              originatorDPID = struct.unpack("!Q",'\x00\x00' + s)[0]
            except:
              pass

    if originatorDPID == None:
      log.warning("Couldn't find a DPID in the LLDP packet")
      return EventHalt

    if originatorDPID not in core.openflow.connections:
      log.info('Received LLDP packet from unknown switch')
      return EventHalt

    def clearFlows():
        for c in core.openflow.connections:
            d = of.ofp_flow_mod(command = of.OFPFC_DELETE)
            c.send(d)
        log.warning("Flows cleared on %s" % c)


    def storeFunction_sk():
      """
      Stores svc code for correct dpid in a list of arrays
      """
      global default_svc_list
      func = lldph.tlvs[2].ttl
      iFlag = 1
      if func != 120:
        global svc_dict
        #svc = str(func)
        svc = func
        #mac = getMAC().lstrip(' ').rstrip(' ')
        mac = getMAC()
        if mac: mac = mac.lstrip(' ').rstrip(' ')
        sp = getSP()
        if sp: sp = int(sp)
        #ip = getIP().lstrip(' ').rstrip(' ')
        ip = getIP()
        if ip: ip = ip.lstrip(' ').rstrip(' ')
        iFlag = 1
        #log.debug ('Key of LLD Packet:[%d], MAC:[%s] Shadow Price:[%s]', svc, str(mac),str(sp))
        if (svc_dict.has_key(svc)):
          for x in range(0,len(svc_dict[svc])):
            isp,imac,iip = svc_dict[svc][x][0],svc_dict[svc][x][1],svc_dict[svc][x][2]
            #if(isp == sp and imac == mac and iip == ip):
            #if(imac == mac and iip == ip):
            if(imac == mac):
              iFlag = 0
              log.debug("SKIPPED!(sp=%s,mac=%s,ip=%s),already in Dict:%s", str(sp),mac,ip,svc_dict)
          if iFlag == 1:
            svc_dict[svc].append([sp,mac,ip,originatorDPID])
            if (svc,mac) not in default_svc_list: default_svc_list.append((svc,mac))
            log.warning ('Existing Service:%d Detected at %s, %s',svc,str(mac),str(sp))
            # JULY 24 21:30PM Commeneted to ensure the flow removal events are caught and cleared up correctly. < There is a bug of bringing down the service that is advertised through LLDP, as in next cycle it will be caught here and forcefully deletes the rules.>
            #clearFlows() # New place for existing service, clear older flows to redirect accordingly
        else:
          svc_dict[svc] = [[sp,mac,ip,originatorDPID]]
          if (svc,mac) not in default_svc_list: default_svc_list.append((svc,mac))
          log.warning ('New Service:%s Detected at:%s, %s', svc,str(mac),str(sp))
          #clearFlows() # new service, doesnt require to reset the existing flows!!
        log.debug('Store_Func::ServiceDictionary is: %s', svc_dict)
        export_svc_dict_to_file()

    storeFunction_sk()

    # Get port number from port TLV
    if lldph.tlvs[1].subtype != pkt.port_id.SUB_PORT:
      log.warning("Thought we found a DPID, but packet didn't have a port")
      return EventHalt
    originatorPort = None
    if lldph.tlvs[1].id.isdigit():
      # We expect it to be a decimal value
      originatorPort = int(lldph.tlvs[1].id)
    elif len(lldph.tlvs[1].id) == 2:
      # Maybe it's a 16 bit port number...
      try:
        originatorPort  =  struct.unpack("!H", lldph.tlvs[1].id)[0]
      except:
        pass
    if originatorPort is None:
      log.warning("Thought we found a DPID, but port number didn't " +
                  "make sense")
      return EventHalt

    if (event.dpid, event.port) == (originatorDPID, originatorPort):
      log.warning("Port received its own LLDP packet; ignoring")
      return EventHalt

    link = Discovery.Link(originatorDPID, originatorPort, event.dpid,
                          event.port)

    if link not in self.adjacency:
      self.adjacency[link] = time.time()
      log.info('link detected: %s', link)
      self.raiseEventNoErrors(LinkEvent, True, link)
    else:
      # Just update timestamp
      self.adjacency[link] = time.time()

    return EventHalt # Probably nobody else needs this event

  def _delete_links (self, links):
    log.info("In _delete_links for Links:%s",links)
    for link in links:
      self.raiseEventNoErrors(LinkEvent, False, link)
    for link in links:
      self.adjacency.pop(link, None)

  def is_edge_port (self, dpid, port):
    """
    Return True if given port does not connect to another switch
    """
    for link in self.adjacency:
      if link.dpid1 == dpid and link.port1 == port:
        return False
      if link.dpid2 == dpid and link.port2 == port:
        return False
    return True

  def getFunctionList_sk (self):
    """
    Return SV_MAP
    """
    global svc_dict
    return svc_dict

  def getIP_MAC_and_SP_List_sk (self, svc=0):
    """
    Return List of [(MAC1,SP,IP1), (MAC2,SP2,IP2)] for Service #SK
    """
    mac_sp_ip_list = []
    global svc_dict

    if (svc_dict.has_key(svc)):
      for x in range(0,len(svc_dict[svc])):
        isp,imac,iip = svc_dict[svc][x][0],svc_dict[svc][x][1],svc_dict[svc][x][2]
        #Exclude the INSTNACE MARKED FOR DELETION
        if int(isp) == MAX_SP_FOR_BRINGDOWN or (svc,svc_dict[svc][x][1]) in svc_bringdown_candidate_instances:
          log.warning("skipped Picking the service instance [%d:%s] marked for Bringdown! ", svc, svc_dict[svc][x])
        else:
          mac_sp_ip_list.append((iip,imac,isp))
    return mac_sp_ip_list

  def getIP_MAC_of_BestSP_sk (self, svc=0):
    """
    Return mac of best shadow price from List of [(MAC1,SP,IP), (MAC2,SP2,IP)] for Service #SK
    """
    global svc_dict
    cur_sp = None
    cur_mac = None
    cur_ip = None

    if (svc_dict.has_key(svc)):
      cur_sp  = int(svc_dict[svc][0][0])
      cur_mac = svc_dict[svc][0][1]
      cur_ip = svc_dict[svc][0][2]
      for x in range(1,len(svc_dict[svc])):
        isp,imac,iip = int(svc_dict[svc][x][0]),svc_dict[svc][x][1], svc_dict[svc][x][2]
        if int(isp) == MAX_SP_FOR_BRINGDOWN or (svc,svc_dict[svc][x][1]) in svc_bringdown_candidate_instances:
          log.warning("skipped Picking the service instance [%d:%s] marked for Bringdown! ", svc, svc_dict[svc][x])
          continue
        #("Lesser the SP, it is better")
        if(isp < cur_sp):
          cur_sp  = isp
          cur_mac = imac
          cur_ip = iip
    return (cur_ip,cur_mac,cur_sp)

update_interval = 5 #2 #5
log_file = rdir_p + "splog.csv"
fsl_file = rdir_p + "fslog.csv"
def _svc_log_function(cur_time=None):
  global svc_dict
  global svc_flow_dict
  o_svc_dict = None
  o_svc_dict = OrderedDict(sorted(svc_dict.items(), key=lambda t: t[0]))
  log.debug(" Discovery_SK: In Svc Log Function [%s],[%s]!",svc_dict, o_svc_dict)
  keys_list = o_svc_dict.keys()
  for key in keys_list:
    log_file_func = log_file.split('.')[0] + '_' + str(key) +'.'+log_file.split('.')[1]
    fsl_file_func = fsl_file.split('.')[0] + '_' + str(key) +'.'+fsl_file.split('.')[1]
    with open(log_file_func, 'ab') as csvfile, open(fsl_file_func, 'ab') as csvfile2:
      logWriter = csv.writer(csvfile,delimiter=',')
      fslWriter = csv.writer(csvfile2,delimiter=',')
      sp_mac_list = []
      sw_cnt_list = []
      log_data = ""
      fsl_data = ""
      for value in o_svc_dict[key]:
        sp,mac_id = value[0],value[1]
        sp_mac_list.append(str(mac_id)+'-'+str(sp))    
      log_data = ','.join(map(str,sp_mac_list)).rstrip(',')
      if svc_flow_dict.has_key(key):
        for value in svc_flow_dict[key]:
          sw_id,count = value[0],value[1]
          sw_cnt_list.append(str(sw_id)+'-'+str(count))    
        fsl_data = ','.join(map(str,sw_cnt_list)).rstrip(',')
      
      if cur_time is None:
        cur_time=time.time()
      logWriter.writerows([[int(cur_time), str(key), str(log_data)]])
      fslWriter.writerows([[int(cur_time), str(key), str(fsl_data)]])
  
  log_pkt_size_data(cur_time)
  return

PKT_LOG_TYPE_PKT_IN    = 0
PKT_LOG_TYPE_PKT_OUT   = 1
PKT_LOG_TYPE_PKT_LLDP  = 2
PKT_LOG_TYPE_PKT_OTHER = 3
class pkt_logger:
  def __init__(self):
    self.pkt_size_list = [0,0,0,0] #index 0=pkt_in 1=pkt_out, 2=lldp 3=unused
  def update_pkt_counter(self, pkt_type, pkt_size):
    if pkt_type < len(self.pkt_size_list):
      self.pkt_size_list[pkt_type] += pkt_size
  def write_and_reset_at_time(self, cur_time=0):
    global pktlog_file
    with open(pktlog_file, 'ab') as csvfile:
      logWriter = csv.writer(csvfile,delimiter=',')
      overall_avg = sum(self.pkt_size_list)
      if overall_avg:
        data = ",".join(str(s) for s in self.pkt_size_list)
        logWriter.writerows([[int(cur_time), str(overall_avg), str(data)]])
pkt_log_obj = pkt_logger()
pktlog_file = rdir_p  + "comm_overhd_log.csv"
def add_pkt_size_info(pkt_type, pkt_size):
  global pkt_log_obj
  #pkt_log_obj.update_pkt_counter(pkt_type,pkt_size)
  return
def log_pkt_size_data(cur_time=0):
  global pkt_log_obj
  #pkt_log_obj.write_and_reset_at_time(cur_time)
  return

def launch (no_flow = False, explicit_drop = True, link_timeout = None,
            eat_early_packets = False, base_dir = None, rsrc_file = None):
  # global hdir_p, rdir_p, list_file2_wn, list_file2_ln
  # if base_dir is not None and os.path.isdir(base_dir):
    # hdir_p = base_dir
    # rdir_p = hdir_p + 'results'
  # if rsrc_file is not None and os.path.isfile(rsrc_file) and len(rsrc_file) > 0: 
    # list_file2_ln = rsrc_file
    # list_file2_wn = rsrc_file
  # else:
    # log.error(" File [%s] not found!",rsrc_file)
  explicit_drop = str_to_bool(explicit_drop)
  eat_early_packets = str_to_bool(eat_early_packets)
  install_flow = not str_to_bool(no_flow)
  if link_timeout: link_timeout = int(link_timeout)
  
  #Timer(update_interval, _svc_log_function, recurring=True)
  core.registerNew(Discovery, explicit_drop=explicit_drop,
                   install_flow=install_flow, link_timeout=link_timeout,
                   eat_early_packets=eat_early_packets)
